<?php if (!$label_hidden): ?>
  <div class="field-label"<?php print $title_attributes; ?>><?php print $label ?>:&nbsp;</div>
<?php endif; ?>

<?php
// Reduce number of images in teaser view mode to single image
if ($element['#view_mode'] == 'teaser'): ?>
  <div class="field-item field-type-image teaser-overlayed even"<?php print $item_attributes[0]; ?>><?php print render($items[0]); ?></div>
<?php return; endif; ?>

<?php
  $node=$element['#object'];
  $images = field_get_items('field_collection_item', $node, 'field_mt_highlight_image');
  $first_image = field_view_value('field_collection_item', $node, 'field_mt_highlight_image', $images[0], array('type' => 'image','settings' => array('image_style' => 'large',)));
  $first_image_file = file_load($images[0]['fid']);
  $first_image_uri = $first_image_file->uri;
?>

<div class="field field-name-field-mt-highlight-image field-type-image field-label-hidden">
  <a class="image-popup-highlight overlayed" href="<?php print file_create_url($first_image_uri); ?>" title="<?php print $images[0]['title']; ?>">
    <?php print render($first_image); ?>
    <span class="overlay large">
      <i class="fa fa-plus"></i>
    </span>
  </a>
</div>

<?php
drupal_add_js('
  jQuery(document).ready(function($) {
    $(window).load(function() {
      $(".image-popup-highlight").magnificPopup({
        type:"image",
        removalDelay: 300,
        mainClass: "mfp-fade",
        gallery: {
          enabled: true, // set to true to enable gallery
        }
      });
    });
  });',array('type' => 'inline', 'scope' => 'footer', 'weight' => 3)
);
?>
