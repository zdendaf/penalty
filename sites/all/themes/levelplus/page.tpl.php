<!-- #page-container -->
<div id="page-container">

  <!-- #header-container -->
  <div id="header-container" class="colored-region dark">
    <?php if ($page['header_top']):?>
      <!-- #header-top -->
      <div id="header-top" class="clearfix">
        <div class="<?php print $header_layout_container; ?>">
          <!-- #header-top-inside -->
          <div id="header-top-inside" class="clearfix">
            <div class="row">
              <div class="col-md-12">
                <div class="header-top-area">
                  <?php print render($page['header_top']); ?>
                </div>
              </div>
            </div>
          </div>
          <!-- EOF: #header-top-inside -->
        </div>
      </div>
      <!-- EOF: #header-top -->
    <?php endif; ?>
    <!-- #header -->
    <header id="header" role="banner" class="clearfix <?php print $header_layout_container_class; ?> <?php print $header_layout_columns_class; ?>">
      <div class="<?php print $header_layout_container; ?>">
        <?php if ($responsive_menu && $page['header_first']): ?>
          <div id="mean-menu-first"></div>
        <?php endif; ?>
        <?php if ($responsive_menu): ?>
          <div id="mean-menu-third"></div>
        <?php endif; ?>
        <!-- #header-inside -->
        <div id="header-inside" class="clearfix">
          <div class="row">
            <?php if ($page['header_first']):?>
              <div class="<?php print $header_first_grid_class; ?>">
                <div class="header-area">
                  <!-- #header-inside-first -->
                  <div id="header-inside-first" class="clearfix">
                    <?php print render($page['header_first']); ?>
                  </div>
                  <!-- EOF:#header-inside-first -->
                </div>
              </div>
            <?php endif; ?>
            <?php if ($logo || $site_name || $site_slogan || $page['header']): ?>
              <div class="<?php print $header_second_grid_class; if (!$page['header_first'] && $header_layout_columns_class == 'three-columns') { print ' ' . 'col-md-offset-4'; } ?>">
                <div class="header-area">
                  <!-- #header-inside-second -->
                  <div id="header-inside-second" class="clearfix">
                    <?php if ($logo || $site_name || $site_slogan) { ?>
                      <div id="logo-site-name-container" class="clearfix">
                        <?php if ($logo) { ?>
                          <div id="logo" class="<?php print $main_logo_class; ?>">
                            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"> <img src="<?php print $main_logo; ?>" alt="<?php print t('Home'); ?>" onerror="this.onerror=null; this.src='<?php print $logo; ?>'"/> </a>
                          </div>
                        <?php }; ?>
                        <?php if ($site_name) { ?>
                          <div id="site-name" class="site-name">
                            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a>
                          </div>
                        <?php }; ?>
                        <?php if ($site_slogan) { ?>
                          <div id="site-slogan" class="site-slogan">
                            <?php print $site_slogan; ?>
                          </div>
                        <?php }; ?>
                      </div>
                    <?php } ?>
                    <?php if ($page['header']) { ?>
                      <?php print render($page['header']); ?>
                    <?php }; ?>
                  </div>
                  <!-- EOF:#header-inside-second -->
                </div>
              </div>
            <?php endif; ?>
            <div class="<?php print $header_third_grid_class; ?>">
              <div class="header-area">
                <!-- #header-inside-third -->
                <div id="header-inside-third" class="clearfix">
                  <?php if ($page['search_area']) :?>
                    <div id="search-area" class="clearfix">
                      <?php print render($page['search_area']); ?>
                    </div>
                  <?php endif; ?>
                  <!-- #main-navigation -->
                  <div id="main-navigation" class="clearfix <?php if ($page['search_area']) { ?> with-search-bar <?php } ?>">
                    <nav role="navigation">
                      <?php if ($page['header_third']) :?>
                        <?php print render($page['header_third']); ?>
                      <?php else : ?>
                        <div id="main-menu">
                          <?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('class' => array('main-menu', 'menu'), ), 'heading' => array('text' => t('Main menu'), 'level' => 'h2', 'class' => array('element-invisible'), ), )); ?>
                        </div>
                      <?php endif; ?>
                    </nav>
                  </div>
                  <!-- EOF: #main-navigation -->
                </div>
                <!-- EOF:#header-inside-third -->
              </div>
            </div>
          </div>
        </div>
        <!-- EOF: #header-inside -->
      </div>
    </header>
    <!-- EOF: #header -->
  </div>
  <!-- EOF: #header-container -->

  <?php if ($page['banner']) : ?>
    <!-- #banner -->
    <div id="banner" class="clearfix">
      <!-- #banner-inside -->
      <div id="banner-inside" class="clearfix">
        <div class="banner-area">
          <?php print render($page['banner']); ?>
        </div>
      </div>
      <!-- EOF: #banner-inside -->
    </div>
    <!-- EOF:#banner -->
  <?php endif; ?>

  <!-- #page-start-->
  <div id="page-start" class="clearfix"></div>

  <!-- #page -->
  <div id="page" class="clearfix">

    <!-- #messages-console -->
    <?php if ($messages):?>
      <div id="messages-console" class="clearfix">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <?php print $messages; ?>
            </div>
          </div>
        </div>
      </div>
    <?php endif; ?>
    <!-- EOF: #messages-console -->

    <?php if ($page['content_top']):?>
      <!-- #content-top -->
      <div id="content-top" class="clearfix<?php print ' ' . $content_top_background_color; ?>">
        <div class="<?php print $content_top_layout_container; ?>">
          <!-- #content-top-inside -->
          <div id="content-top-inside" class="clearfix<?php if ($content_top_animations == "enabled") { print ' ' . 'object-non-visible'; } ?>" <?php if ($content_top_animations == "enabled") { ?> data-animate-effect="<?php print $content_top_animation_effect; ?>" <?php } ?>>
            <div class="row">
              <div class="col-md-12">
                <div class="content-top-area">
                  <?php print render($page['content_top']); ?>
                </div>
              </div>
            </div>
          </div>
          <!-- EOF:#content-top-inside -->
        </div>
      </div>
      <!-- EOF: #content-top -->
    <?php endif; ?>

    <!-- #main-content -->
    <div id="main-content" class="clearfix white-region<?php if (!$page['sidebar_first'] && !$frontpage_content_print && drupal_is_front_page() && !$page['sidebar_second']) { print ' ' . 'main-content-empty'; } ?>">
      <div class="container">
        <div class="row">
          <section class="<?php print $main_grid_class; ?>">
            <!-- #main -->
            <div id="main" class="clearfix main-area<?php if ($main_content_animations == "enabled") { print ' ' . 'object-non-visible'; } ?>" <?php if ($main_content_animations == "enabled") { ?> data-animate-effect="<?php print $main_content_animation_effect; ?>" <?php } ?>>
              <?php print render($title_prefix); ?>
              <?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
              <?php print render($title_suffix); ?>
              <!-- #tabs -->
              <?php if ($tabs):?>
                <div class="tabs">
                  <?php print render($tabs); ?>
                </div>
              <?php endif; ?>
              <!-- EOF: #tabs -->
              <?php print render($page['help']); ?>
              <!-- #action links -->
              <?php if ($action_links):?>
                <ul class="action-links">
                  <?php print render($action_links); ?>
                </ul>
              <?php endif; ?>
              <!-- EOF: #action links -->
              <?php if ($frontpage_content_print || !drupal_is_front_page()):?>
                <?php print render($page['content']); ?>
                <?php print $feed_icons; ?>
              <?php endif; ?>
            </div>
            <!-- EOF:#main -->
          </section>
          <?php if ($page['sidebar_first'] && ($frontpage_content_print || !drupal_is_front_page())):?>
            <aside class="<?php print $sidebar_first_grid_class; ?>">
              <!--#sidebar-->
              <section id="sidebar-first" class="sidebar clearfix">
                <?php print render($page['sidebar_first']); ?>
              </section>
              <!--EOF:#sidebar-->
            </aside>
          <?php endif; ?>
          <?php if ($page['sidebar_second'] && ($frontpage_content_print || !drupal_is_front_page())):?>
            <aside class="<?php print $sidebar_second_grid_class; ?>">
              <!--#sidebar-->
              <section id="sidebar-second" class="sidebar clearfix">
                <?php print render($page['sidebar_second']); ?>
              </section>
              <!--EOF:#sidebar-->
            </aside>
          <?php endif; ?>
        </div>
      </div>
    </div>
    <!-- EOF:#main-content -->

    <?php if ($page['content_bottom_first'] || $page['content_bottom_second']):?>
      <!-- #content-bottom -->
      <div id="content-bottom" class="clearfix<?php print ' ' . $content_bottom_background_color; ?>">
        <div class="container">
          <!-- #content-bottom-inside -->
          <div id="content-bottom-inside" class="clearfix<?php if ($content_bottom_animations == "enabled") { print ' ' . 'object-non-visible'; } ?>" <?php if ($content_bottom_animations == "enabled") { ?> data-animate-effect="<?php print $content_bottom_animation_effect; ?>" <?php } ?>>
            <div class="row">
              <?php if ($page['content_bottom_first']): ?>
                <div class="<?php print $content_bottom_first_grid_class; ?>">
                  <div id="content-bottom-first">
                    <div class="content-bottom-area clearfix">
                      <?php print render($page['content_bottom_first']); ?>
                    </div>
                  </div>
                </div>
              <?php endif; ?>
              <?php if ($page['content_bottom_second']): ?>
                <div class="<?php print $content_bottom_second_grid_class; ?>">
                  <div id="content-bottom-second">
                    <div class="content-bottom-area clearfix">
                      <?php print render($page['content_bottom_second']); ?>
                    </div>
                  </div>
                </div>
              <?php endif; ?>
            </div>
          </div>
          <!-- EOF:#content-bottom-inside -->
        </div>
      </div>
      <!-- EOF: #content-bottom -->
    <?php endif; ?>

    <?php if ($page['featured_top']):?>
      <!-- #featured-top -->
      <div id="featured-top" class="clearfix<?php print ' ' . $featured_top_background_color; ?>">
        <div class="<?php print $featured_top_layout_container; ?>">
          <!-- #featured-top-inside -->
          <div id="featured-top-inside" class="clearfix<?php if ($featured_top_animations == "enabled") { print ' ' . 'object-non-visible'; } ?>" <?php if ($featured_top_animations == "enabled") { ?> data-animate-effect="<?php print $featured_top_animation_effect; ?>" <?php } ?>>
            <div class="row">
              <div class="col-md-12">
                <div class="featured-top-area">
                  <?php print render($page['featured_top']); ?>
                </div>
              </div>
            </div>
          </div>
          <!-- EOF:#featured-top-inside -->
        </div>
      </div>
      <!-- EOF: #featured-top -->
    <?php endif; ?>

    <?php if ($page['featured']):?>
      <!-- #featured -->
      <div id="featured" class="clearfix<?php print ' ' . $featured_background_color; ?>">
        <div class="<?php print $featured_layout_container; ?>">
          <!-- #featured-inside -->
          <div id="featured-inside" class="clearfix<?php if ($featured_animations == "enabled") { print ' ' . 'object-non-visible'; } ?>" <?php if ($featured_animations == "enabled") { ?> data-animate-effect="<?php print $featured_animation_effect; ?>" <?php } ?>>
            <div class="row">
              <div class="col-md-12">
                <div class="featured-area">
                  <?php print render($page['featured']); ?>
                </div>
              </div>
            </div>
          </div>
          <!-- EOF:#featured-inside -->
        </div>
      </div>
      <!-- EOF: #featured -->
    <?php endif; ?>

    <?php if ($page['featured_bottom']):?>
      <!-- #featured-bottom -->
      <div id="featured-bottom" class="clearfix<?php print ' ' . $featured_bottom_background_color; ?>">
        <div class="<?php print $featured_bottom_layout_container; ?>">
          <!-- #featured-bottom-inside -->
          <div id="featured-bottom-inside" class="clearfix<?php if ($featured_bottom_animations == "enabled") { print ' ' . 'object-non-visible'; } ?>" <?php if ($featured_bottom_animations == "enabled") { ?> data-animate-effect="<?php print $featured_bottom_animation_effect; ?>" <?php } ?>>
            <div class="row">
              <div class="col-md-12">
                <div class="featured-bottom-area">
                  <?php print render($page['featured_bottom']); ?>
                </div>
              </div>
            </div>
          </div>
          <!-- EOF:#featured-bottom-inside -->
        </div>
      </div>
      <!-- EOF: #featured-bottom -->
    <?php endif; ?>

    <?php if ($breadcrumb && $breadcrumb_display):?>
      <!-- Breadcrumb -->
      <div id="breadcrumb" class="clearfix">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div id="breadcrumb-inside" class="clearfix">
                <?php print $breadcrumb; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- EOF:Breadcrumb -->
    <?php endif; ?>

    <?php if ($page['highlighted']):?>
      <!-- #highlighted -->
      <div id="highlighted" class="clearfix<?php print ' ' . $highlighted_background_color; ?>">
        <div class="<?php print $highlighted_layout_container; ?>">
          <!-- #highlighted-inside -->
          <div id="highlighted-inside" class="clearfix<?php if ($highlighted_animations == "enabled") { print ' ' . 'object-non-visible'; } ?>" <?php if ($highlighted_animations == "enabled") { ?> data-animate-effect="<?php print $highlighted_animation_effect; ?>" <?php } ?>>
            <div class="row">
              <div class="col-md-12">
                <div class="highlighted-area">
                  <?php print render($page['highlighted']); ?>
                </div>
              </div>
            </div>
          </div>
          <!-- EOF:#highlighted-inside -->
        </div>
      </div>
      <!-- EOF: #highlighted -->
    <?php endif; ?>

    <?php if ($page['highlighted_bottom']):?>
      <!-- #highlighted-bottom -->
      <div id="highlighted-bottom" class="clearfix<?php print ' ' . $highlighted_bottom_background_color; ?>">
        <div id="highlighted-bottom-transparent-bg"></div>
        <div class="<?php print $highlighted_bottom_layout_container; ?>">
          <!-- #highlighted-bottom-inside -->
          <div id="highlighted-bottom-inside" class="clearfix<?php if ($highlighted_bottom_animations == "enabled") { print ' ' . 'object-non-visible'; } ?>" <?php if ($highlighted_bottom_animations == "enabled") { ?> data-animate-effect="<?php print $highlighted_bottom_animation_effect; ?>" <?php } ?>>
            <div class="row">
              <div class="col-md-12">
                <div class="highlighted-bottom-area">
                  <?php print render($page['highlighted_bottom']); ?>
                </div>
              </div>
            </div>
          </div>
          <!-- EOF:#highlighted-bottom-inside -->
        </div>
      </div>
      <!-- EOF: #highlighted-bottom -->
    <?php endif; ?>

  </div>
  <!-- EOF:Page -->

  <?php if ($page['footer_top_first'] || $page['footer_top_second']) :?>
    <!-- #footer-top -->
    <div id="footer-top" class="clearfix <?php print $footer_top_regions; ?><?php print ' ' . $footer_top_background_color; ?>">
      <div class="container">
        <!-- #footer-top-inside -->
        <div id="footer-top-inside" class="clearfix<?php if ($footer_top_animations == "enabled") { print ' ' . 'object-non-visible'; } ?>" <?php if ($footer_top_animations == "enabled") { ?> data-animate-effect="<?php print $footer_top_animation_effect; ?>" <?php } ?>>
          <div class="row">
            <?php if ($page['footer_top_first']) :?>
              <div class="<?php print $footer_top_first_grid_class; ?>">
                <!-- #footer-top-first -->
                <div id="footer-top-first" class="clearfix">
                  <div class="footer-top-area">
                    <?php print render($page['footer_top_first']); ?>
                  </div>
                </div>
                <!-- EOF:#footer-top-first -->
              </div>
            <?php endif; ?>
            <?php if ($page['footer_top_second']) :?>
              <div class="<?php print $footer_top_second_grid_class; ?>">
                <!-- #footer-top-second -->
                <div id="footer-top-second" class="clearfix">
                  <div class="footer-top-area">
                    <?php print render($page['footer_top_second']); ?>
                  </div>
                </div>
                <!-- EOF:#footer-top-second -->
              </div>
            <?php endif; ?>
          </div>
        </div>
        <!-- EOF: #footer-top-inside -->
      </div>
    </div>
    <!-- EOF: #footer-top -->
  <?php endif; ?>

  <?php if ($page['footer_first'] || $page['footer_second'] || $page['footer_third'] || $page['footer_fourth']):?>
    <!-- #footer -->
    <footer id="footer" class="clearfix<?php print ' ' . $footer_background_color; ?>">
      <div class="<?php print $footer_layout_container; ?>">
        <div class="row">
          <?php if ($page['footer_first']):?>
            <div class="<?php print $footer_grid_class; ?>">
              <div class="footer-area<?php if ($footer_animations == "enabled") { print ' ' . 'object-non-visible'; } ?>" <?php if ($footer_animations == "enabled") { ?> data-animate-effect="<?php print $footer_animation_effect; ?>" <?php } ?>>
                <?php print render($page['footer_first']); ?>
              </div>
            </div>
          <?php endif; ?>
          <?php if ($page['footer_second']):?>
            <div class="<?php print $footer_grid_class; ?>">
              <div class="footer-area<?php if ($footer_animations == "enabled") { print ' ' . 'object-non-visible'; } ?>" <?php if ($footer_animations == "enabled") { ?> data-animate-effect="<?php print $footer_animation_effect; ?>" <?php } ?>>
                <?php print render($page['footer_second']); ?>
              </div>
            </div>
          <?php endif; ?>
          <?php if ($page['footer_third']):?>
            <div class="<?php print $footer_grid_class; ?>">
              <div class="footer-area<?php if ($footer_animations == "enabled") { print ' ' . 'object-non-visible'; } ?>" <?php if ($footer_animations == "enabled") { ?> data-animate-effect="<?php print $footer_animation_effect; ?>" <?php } ?>>
                <?php print render($page['footer_third']); ?>
              </div>
            </div>
          <?php endif; ?>
          <?php if ($page['footer_fourth']):?>
            <div class="<?php print $footer_grid_class; ?>">
              <div class="footer-area<?php if ($footer_animations == "enabled") { print ' ' . 'object-non-visible'; } ?>" <?php if ($footer_animations == "enabled") { ?> data-animate-effect="<?php print $footer_animation_effect; ?>" <?php } ?>>
                <?php print render($page['footer_fourth']); ?>
              </div>
            </div>
          <?php endif; ?>
        </div>
      </div>
    </footer>
    <!-- EOF #footer -->
  <?php endif; ?>

  <?php if ($page['sub_footer_first'] || $page['footer']):?>
    <!-- #subfooter -->
    <div id="subfooter" class="clearfix<?php print ' ' . $subfooter_background_color; ?>">
      <div class="<?php print $subfooter_layout_container; ?>">
        <!-- #subfooter-inside -->
        <div id="subfooter-inside" class="clearfix">
          <div class="row">
            <!-- #subfooter-first -->
            <?php if ($page['sub_footer_first']):?>
              <div class="<?php print $subfooter_first_grid_class?>">
                <div class="subfooter-area first">
                  <?php print render($page['sub_footer_first']); ?>
                </div>
              </div>
            <?php endif; ?>
            <!-- EOF: #subfooter-first -->
            <!-- #subfooter-second -->
            <?php if ($page['footer']):?>
              <div class="<?php print $subfooter_second_grid_class?>">
                <div class="subfooter-area second">
                  <?php print render($page['footer']); ?>
                </div>
              </div>
            <?php endif; ?>
            <!-- EOF: #subfooter-second -->
          </div>
        </div>
        <!-- EOF: #subfooter-inside -->
      </div>
    </div>
    <!-- EOF:#subfooter -->
  <?php endif; ?>


  <?php if ($scrolltop_display): ?>
    <!-- #toTop -->
    <div id="toTop"><i class="fa fa-angle-double-up"></i></div>
    <!-- EOF:#toTop -->
  <?php endif; ?>
</div>
<!-- EOF:#page-container -->
