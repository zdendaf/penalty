<!DOCTYPE html>
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">
  <head>
    <?php print $head ?>
    <title><?php print $head_title ?></title>
    <?php print $styles ?>
    <?php print $scripts ?>
  </head>

  <body class="<?php print $classes; ?> not-front" <?php print $attributes;?>>

    <!-- #header-container -->
    <div id="header-container" class="colored-region dark">
      <!-- #header -->
      <header id="header" role="banner" class="clearfix full-width two-columns">
        <div class="container-fluid">
          <!-- #header-inside -->
          <div id="header-inside" class="clearfix">
            <div class="row">
              <div class="col-md-4 col-lg-3">
                <div class="header-area">
                  <!-- #header-inside-second -->
                  <div id="header-inside-second" class="clearfix">
                    <?php if ($logo || $site_name || $site_slogan) { ?>
                    <div id="logo-site-name-container" class="clearfix">
                      <?php if ($logo) { ?>
                        <div id="logo" class="default-logo">
                          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
                            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" onerror="this.onerror=null; this.src='<?php print $logo; ?>'"/>
                          </a>
                        </div>
                      <?php }; ?>
                      <?php if ($site_name) { ?>
                        <div id="site-name" class="site-name">
                          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a>
                        </div>
                      <?php }; ?>
                      <?php if ($site_slogan) { ?>
                        <div id="site-slogan" class="site-slogan">
                          <?php print $site_slogan; ?>
                        </div>
                      <?php }; ?>
                    </div>
                    <?php } ?>
                  </div>
                  <!-- EOF: #header-inside-second -->
                </div>
              </div>
              <div class="col-md-8 col-lg-9">
                <div class="header-area">
                  <!-- #header-inside-third -->
                  <div id="header-inside-third" class="clearfix">
                    <!-- #main-navigation -->
                    <div id="main-navigation" class="clearfix">
                    </div>
                    <!-- EOF: #main-navigation -->
                  </div>
                  <!-- EOF: #header-inside-third -->
                </div>
              </div>
            </div>
          </div>
          <!-- EOF: #header-inside -->
        </div>
      </header>
      <!-- EOF: #header -->
    </div>
    <!-- EOF: #header-container -->

    <!-- #banner -->
    <div id="banner" class="clearfix">
      <!-- #banner-inside -->
      <div id="banner-inside" class="clearfix">
        <div class="banner-area">
        </div>
      </div>
      <!-- EOF: #banner-inside -->
    </div>
    <!-- EOF:#banner -->

    <!-- #page -->
    <div id="page" class="clearfix">

      <!-- #main-content -->
      <div id="main-content" class="clearfix">
        <div class="container">
          <div class="row">
            <section class="col-md-12">
              <!-- #main -->
              <div id="main">
                <?php print $messages; ?>
                <?php if ($title): ?><h1 class="page-title"><?php print $title; ?></h1> <?php endif; ?>
                <?php print $content; ?>
              </div>
            </section>
            <!-- #sidebar -->
            <aside class="col-md-12">
              <section id="sidebar" class="clearfix">
              </section>
            </aside>
            <!-- EOF: #sidebar -->
          </div>
        </div>
      </div>
      <!-- EOF:#main-content -->

      <!-- #content-bottom -->
      <div id="content-bottom" class="clearfix">
        <div class="container">
          <!-- #content-bottom-inside -->
          <div id="content-bottom-inside" class="clearfix">
            <div class="row">
              <div class="col-md-12">
                <div class="content-bottom-area clearfix">
                </div>
              </div>
            </div>
          </div>
          <!-- EOF:#content-bottom-inside -->
        </div>
      </div>
      <!-- EOF: #content-bottom -->

      <!-- #highlighted-bottom -->
      <div id="highlighted-bottom" class="clearfix colored-region">
        <div class="container">
          <!-- #highlighted-bottom-inside -->
          <div id="highlighted-bottom-inside" class="clearfix">
            <div class="row">
              <div class="col-md-12">
                <div class="highlighted-bottom-area clearfix">
                </div>
              </div>
            </div>
          </div>
          <!-- EOF:#highlighted-bottom-inside -->
        </div>
      </div>
      <!-- EOF: #highlighted-bottom -->

    </div>
    <!-- EOF: #page -->

    <!-- #footer-top -->
    <div id="footer-top" class="clearfix two-regions bicolor">
      <div class="container">
        <!-- #footer-top-inside -->
        <div id="footer-top-inside" class="clearfix">
          <div class="row">
            <div class="col-md-12">
            </div>
          </div>
        </div>
        <!-- EOF:#footer-top-inside -->
      </div>
    </div>
    <!-- EOF: #footer-top -->
      
    <!-- #footer -->
    <div id="footer" class="clearfix light-gray-region">
      <div class="container">
        <!-- #footer-inside -->
        <div id="footer-inside" class="clearfix">
          <div class="row">
            <div class="col-md-3">
              <!-- footer-area -->
              <div class="footer-area">
              </div>
              <!-- EOF: footer-area -->
            </div>
            <div class="col-md-3">
              <!-- footer-area -->
              <div class="footer-area">
              </div>
              <!-- EOF: footer-area -->
            </div>
            <div class="col-md-3">
              <!-- footer-area -->
              <div class="footer-area">
              </div>
              <!-- EOF: footer-area -->
            </div>
            <div class="col-md-3">
              <!-- footer-area -->
              <div class="footer-area">
              </div>
              <!-- EOF: footer-area -->
            </div>
          </div>
        </div>
        <!-- #footer-inside -->
      </div> 
    </div>
    <!-- EOF:#footer -->

    <div id="subfooter" class="clearfix light-gray-region">
      <div class="container">
        <!-- #subfooter-inside -->
        <div id="subfooter-inside" class="clearfix">
          <div class="row">
            <div class="col-md-4">
              <!-- #subfooter-left -->
              <div class="subfooter-area left">
              </div>
              <!-- EOF: #subfooter-left -->
            </div>
            <div class="col-md-8">
              <!-- #subfooter-right -->
              <div class="subfooter-area right">
              </div>
              <!-- EOF: #subfooter-right -->
            </div>
          </div>
        </div>
        <!-- EOF: #subfooter-inside -->
      </div>
    </div>
    <!-- EOF:#subfooter -->

  </body>
</html>