<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php
    $facebook = field_get_items('node', $node, 'field_mt_facebook_account');
    $twitter = field_get_items('node', $node, 'field_mt_twitter_account');
    $linkedin = field_get_items('node', $node, 'field_mt_linkedin_account');
    if (!empty($facebook)) { $facebook_value = field_view_value('node', $node, 'field_mt_facebook_account', $facebook[0]); }
    if (!empty($twitter)) { $twitter_value = field_view_value('node', $node, 'field_mt_twitter_account', $twitter[0]); }
    if (!empty($linkedin)) { $linkedin_value = field_view_value('node', $node, 'field_mt_linkedin_account', $linkedin[0]); }
  ?>

  <?php if ($title_prefix || $title_suffix || $display_submitted || !$page): ?>
    <header>
      <?php print render($title_prefix); ?>
      <?php if (!$page): ?>
        <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php if ($display_submitted): ?>
        <div class="submitted-info">
          <?php print $submitted; ?>
        </div>
      <?php endif; ?>
      <?php print $user_picture; ?>
    </header>
  <?php endif; ?>

  <?php if ($photo = render($content['field_mt_member_photo'])) { ?>
    <div class="row content"<?php print $content_attributes; ?>>
      <div class="col-md-6">
        <div class="photo-container">
          <?php print render($photo); ?>
          <?php if (!empty($field_mt_facebook_account) || !empty($field_mt_twitter_account) || !empty($field_mt_linkedin_account) ) { ?>
            <ul class="members-social-bookmarks text-center">
              <?php if (!empty($field_mt_facebook_account)) { ?>
                <li class="social-media-item">
                  <a target="_blank" href="<?php print render($facebook_value);?>">
                    <i class="fa fa-facebook"><span class="sr-only">facebook</span></i>
                  </a>
                </li>
              <?php } ?>
              <?php if (!empty($field_mt_twitter_account)) { ?>
                <li class="social-media-item">
                  <a target="_blank" href="<?php print render($twitter_value);?>">
                    <i class="fa fa-twitter"><span class="sr-only">twitter</span></i>
                  </a>
                </li>
              <?php } ?>
              <?php if (!empty($field_mt_linkedin_account)) { ?>
                <li class="social-media-item">
                  <a target="_blank" href="<?php print render($linkedin_value);?>">
                    <i class="fa fa-linkedin"><span class="sr-only">linkedin</span></i>
                  </a>
                </li>
              <?php } ?>
            </ul>
          <?php } ?>
        </div>
      </div>
      <div class="col-md-6">
        <?php
        // We hide the comments and links now so that we can render them later.
        hide($content['comments']);
        hide($content['links']);
        hide($content['field_mt_facebook_account']);
        hide($content['field_mt_twitter_account']);
        hide($content['field_mt_linkedin_account']);
        print render($content);
        ?>
      </div>
    </div>
  <?php } else { ?>
    <div class="content"<?php print $content_attributes; ?>>
      <?php print render($content['field_mt_subtitle']); ?>
      <?php if (!empty($field_mt_facebook_account) || !empty($field_mt_twitter_account) || !empty($field_mt_linkedin_account) ) { ?>
        <ul class="members-social-bookmarks">
          <?php if (!empty($field_mt_facebook_account)) { ?>
            <li class="social-media-item">
              <a target="_blank" href="<?php print render($facebook_value);?>">
                <i class="fa fa-facebook"><span class="sr-only">facebook</span></i>
              </a>
            </li>
          <?php } ?>
          <?php if (!empty($field_mt_twitter_account)) { ?>
            <li class="social-media-item">
              <a target="_blank" href="<?php print render($twitter_value);?>">
                <i class="fa fa-twitter"><span class="sr-only">twitter</span></i>
              </a>
            </li>
          <?php } ?>
          <?php if (!empty($field_mt_linkedin_account)) { ?>
            <li class="social-media-item">
              <a target="_blank" href="<?php print render($linkedin_value);?>">
                <i class="fa fa-linkedin"><span class="sr-only">linkedin</span></i>
              </a>
            </li>
          <?php } ?>
        </ul>
      <?php } ?>
      <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      hide($content['field_mt_facebook_account']);
      hide($content['field_mt_twitter_account']);
      hide($content['field_mt_linkedin_account']);
      print render($content);
      ?>
    </div>
  <?php } ?>

  <?php if ($links = render($content['links'])): ?>
    <footer>
      <?php print render($content['links']); ?>
    </footer>
  <?php endif; ?>
  <?php print render($content['comments']); ?>

</article>
