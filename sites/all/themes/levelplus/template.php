<?php

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return
 *   A string containing the breadcrumb output.
 */
function levelplus_breadcrumb($variables){
  $breadcrumb = $variables['breadcrumb'];
  $breadcrumb_separator = theme_get_setting('breadcrumb_separator');
  if (!empty($breadcrumb)) {
    $breadcrumb[] = drupal_get_title();
    return '<div>' . implode(' <span class="breadcrumb-separator fa ' . $breadcrumb_separator . '"></span>', $breadcrumb) . '</div>';
  }
}

/**
 * Add classes to block.
 */
function levelplus_preprocess_block(&$variables) {
  $variables['title_attributes_array']['class'][] = 'title';
  $variables['classes_array'][]='clearfix';
}

/**
 * Override or insert variables into the html template.
 */
function levelplus_preprocess_html(&$variables) {

  if (empty($variables['page']['banner'])) {
    $variables['classes_array'][] = 'no-banner';
  }

  $color_scheme = theme_get_setting('color_scheme');

  if ($color_scheme != 'default') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/style-' .$color_scheme. '.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on') {
    $protocol = "/https";
  } else {
    $protocol = "/http";
  }

  if (theme_get_setting('sitename_font_family')=='sff-1' ||
    theme_get_setting('slogan_font_family')=='slff-1' ||
    theme_get_setting('headings_font_family')=='hff-1' ||
    theme_get_setting('paragraph_font_family')=='pff-1') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts'.$protocol. '/merriweather-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-2' ||
    theme_get_setting('slogan_font_family')=='slff-2' ||
    theme_get_setting('headings_font_family')=='hff-2' ||
    theme_get_setting('paragraph_font_family')=='pff-2') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts'.$protocol. '/sourcesanspro-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-3' ||
    theme_get_setting('slogan_font_family')=='slff-3' ||
    theme_get_setting('headings_font_family')=='hff-3' ||
    theme_get_setting('paragraph_font_family')=='pff-3') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts'.$protocol. '/ubuntu-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-4' ||
    theme_get_setting('slogan_font_family')=='slff-4' ||
    theme_get_setting('headings_font_family')=='hff-4' ||
    theme_get_setting('paragraph_font_family')=='pff-4') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts'.$protocol. '/ptsans-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-5' ||
    theme_get_setting('slogan_font_family')=='slff-5' ||
    theme_get_setting('headings_font_family')=='hff-5' ||
    theme_get_setting('paragraph_font_family')=='pff-5') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts'.$protocol. '/roboto-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-6' ||
    theme_get_setting('slogan_font_family')=='slff-6' ||
    theme_get_setting('headings_font_family')=='hff-6' ||
    theme_get_setting('paragraph_font_family')=='pff-6') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts'.$protocol. '/opensans-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-7' ||
    theme_get_setting('slogan_font_family')=='slff-7' ||
    theme_get_setting('headings_font_family')=='hff-7' ||
    theme_get_setting('paragraph_font_family')=='pff-7') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts'.$protocol. '/lato-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-8' ||
    theme_get_setting('slogan_font_family')=='slff-8' ||
    theme_get_setting('headings_font_family')=='hff-8' ||
    theme_get_setting('paragraph_font_family')=='pff-8') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts'.$protocol. '/roboto-condensed-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-9' ||
    theme_get_setting('slogan_font_family')=='slff-9' ||
    theme_get_setting('headings_font_family')=='hff-9' ||
    theme_get_setting('paragraph_font_family')=='pff-9') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts'.$protocol. '/exo-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-10' ||
    theme_get_setting('slogan_font_family')=='slff-10' ||
    theme_get_setting('headings_font_family')=='hff-10' ||
    theme_get_setting('paragraph_font_family')=='pff-10') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts'.$protocol. '/roboto-slab-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-11' ||
    theme_get_setting('slogan_font_family')=='slff-11' ||
    theme_get_setting('headings_font_family')=='hff-11' ||
    theme_get_setting('paragraph_font_family')=='pff-11') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts'.$protocol. '/raleway-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-12' ||
    theme_get_setting('slogan_font_family')=='slff-12' ||
    theme_get_setting('headings_font_family')=='hff-12' ||
    theme_get_setting('paragraph_font_family')=='pff-12') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts'.$protocol. '/josefin-sans-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-14' ||
    theme_get_setting('slogan_font_family')=='slff-14' ||
    theme_get_setting('headings_font_family')=='hff-14' ||
    theme_get_setting('paragraph_font_family')=='pff-14') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts'.$protocol. '/playfair-display-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-15' ||
    theme_get_setting('slogan_font_family')=='slff-15' ||
    theme_get_setting('headings_font_family')=='hff-15' ||
    theme_get_setting('paragraph_font_family')=='pff-15') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts'.$protocol. '/philosopher-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-16' ||
    theme_get_setting('slogan_font_family')=='slff-16' ||
    theme_get_setting('headings_font_family')=='hff-16') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts'.$protocol. '/cinzel-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-17' ||
    theme_get_setting('slogan_font_family')=='slff-17' ||
    theme_get_setting('headings_font_family')=='hff-17' ||
    theme_get_setting('paragraph_font_family')=='pff-16') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts'.$protocol. '/oswald-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-18' ||
    theme_get_setting('slogan_font_family')=='slff-18' ||
    theme_get_setting('headings_font_family')=='hff-18' ||
    theme_get_setting('paragraph_font_family')=='pff-17') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts'.$protocol. '/playfairdisplaysc-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-19' ||
    theme_get_setting('slogan_font_family')=='slff-19' ||
    theme_get_setting('headings_font_family')=='hff-19' ||
    theme_get_setting('paragraph_font_family')=='pff-18') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts'.$protocol. '/cabin-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-20' ||
    theme_get_setting('slogan_font_family')=='slff-20' ||
    theme_get_setting('headings_font_family')=='hff-20' ||
    theme_get_setting('paragraph_font_family')=='pff-19') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts'.$protocol. '/notosans-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-22' ||
    theme_get_setting('slogan_font_family')=='slff-22' ||
    theme_get_setting('headings_font_family')=='hff-22' ||
    theme_get_setting('paragraph_font_family')=='pff-21') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts'.$protocol. '/droidserif-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-23' ||
    theme_get_setting('slogan_font_family')=='slff-23' ||
    theme_get_setting('headings_font_family')=='hff-23' ||
    theme_get_setting('paragraph_font_family')=='pff-22') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts'.$protocol. '/ptserif-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-24' ||
    theme_get_setting('slogan_font_family')=='slff-24' ||
    theme_get_setting('headings_font_family')=='hff-24' ||
    theme_get_setting('paragraph_font_family')=='pff-23') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts'.$protocol. '/vollkorn-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-25' ||
    theme_get_setting('slogan_font_family')=='slff-25' ||
    theme_get_setting('headings_font_family')=='hff-25' ||
    theme_get_setting('paragraph_font_family')=='pff-24') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts'.$protocol. '/alegreya-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-26' ||
    theme_get_setting('slogan_font_family')=='slff-26' ||
    theme_get_setting('headings_font_family')=='hff-26' ||
    theme_get_setting('paragraph_font_family')=='pff-25') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts'.$protocol. '/notoserif-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-27' ||
    theme_get_setting('slogan_font_family')=='slff-27' ||
    theme_get_setting('headings_font_family')=='hff-27' ||
    theme_get_setting('paragraph_font_family')=='pff-26') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts'.$protocol. '/crimsontext-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-28' ||
    theme_get_setting('slogan_font_family')=='slff-28' ||
    theme_get_setting('headings_font_family')=='hff-28' ||
    theme_get_setting('paragraph_font_family')=='pff-27') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts'.$protocol. '/gentiumbookbasic-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-29' ||
    theme_get_setting('slogan_font_family')=='slff-29' ||
    theme_get_setting('headings_font_family')=='hff-29' ||
    theme_get_setting('paragraph_font_family')=='pff-28') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts'.$protocol. '/volkhov-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-31' ||
    theme_get_setting('slogan_font_family')=='slff-31' ||
    theme_get_setting('headings_font_family')=='hff-31') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts'.$protocol. '/alegreyasc-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-32' ||
    theme_get_setting('slogan_font_family')=='slff-32' ||
    theme_get_setting('headings_font_family')=='hff-32') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts' .$protocol. '/montserrat-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-33' ||
    theme_get_setting('slogan_font_family')=='slff-33' ||
    theme_get_setting('headings_font_family')=='hff-33' ||
    theme_get_setting('paragraph_font_family')=='pff-30') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts' .$protocol. '/firasans-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-34' ||
    theme_get_setting('slogan_font_family')=='slff-34' ||
    theme_get_setting('headings_font_family')=='hff-34' ||
    theme_get_setting('paragraph_font_family')=='pff-31') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts' .$protocol. '/lora-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-35' ||
    theme_get_setting('slogan_font_family')=='slff-35' ||
    theme_get_setting('headings_font_family')=='hff-35' ||
    theme_get_setting('paragraph_font_family')=='pff-32') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts' .$protocol. '/quattrocentosans-font.css', array('group' => CSS_THEME, 'type' => 'file', 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-36' ||
    theme_get_setting('slogan_font_family')=='slff-36' ||
    theme_get_setting('headings_font_family')=='hff-36') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts' .$protocol. '/juliussansone-font.css', array('group' => CSS_THEME, 'type' => 'file', 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-37' ||
    theme_get_setting('slogan_font_family')=='slff-37' ||
    theme_get_setting('headings_font_family')=='hff-37' ||
    theme_get_setting('paragraph_font_family')=='pff-33') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts' .$protocol. '/worksans-font.css', array('group' => CSS_THEME, 'type' => 'file', 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-38' ||
    theme_get_setting('slogan_font_family')=='slff-38' ||
    theme_get_setting('headings_font_family')=='hff-38' ||
    theme_get_setting('paragraph_font_family')=='pff-34') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts' .$protocol. '/opensans-condensed-font.css', array('group' => CSS_THEME, 'type' => 'file', 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-39' ||
    theme_get_setting('slogan_font_family')=='slff-39' ||
    theme_get_setting('headings_font_family')=='hff-39' ||
    theme_get_setting('paragraph_font_family')=='pff-35') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts' .$protocol. '/ptsans-narrow-font.css', array('group' => CSS_THEME, 'type' => 'file', 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-40' ||
    theme_get_setting('slogan_font_family')=='slff-40' ||
    theme_get_setting('headings_font_family')=='hff-40' ||
    theme_get_setting('paragraph_font_family')=='pff-36') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts' .$protocol. '/archivo-narrow-font.css', array('group' => CSS_THEME, 'type' => 'file', 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-41' ||
    theme_get_setting('slogan_font_family')=='slff-41' ||
    theme_get_setting('headings_font_family')=='hff-41' ||
    theme_get_setting('paragraph_font_family')=='pff-37') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts' .$protocol. '/ubuntu-condensed-font.css', array('group' => CSS_THEME, 'type' => 'file', 'preprocess' => FALSE));
  }

  if (theme_get_setting('sitename_font_family')=='sff-42' ||
    theme_get_setting('slogan_font_family')=='slff-42' ||
    theme_get_setting('headings_font_family')=='hff-42' ||
    theme_get_setting('paragraph_font_family')=='pff-38') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts' .$protocol. '/arimo-font.css', array('group' => CSS_THEME, 'type' => 'file', 'preprocess' => FALSE));
  }

  drupal_add_css(path_to_theme() . '/fonts' .$protocol. '/sourcecodepro-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));

  if (isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on') {
    drupal_add_js('https://use.fontawesome.com/f479fb1064.js', array('type' => 'external'));
  } else {
    drupal_add_js('http://use.fontawesome.com/f479fb1064.js', array('type' => 'external'));
  }

  drupal_add_css(path_to_theme() . '/ie9.css', array('group' => CSS_THEME, 'browsers' => array('IE' => '(IE 9)&(!IEMobile)', '!IE' => FALSE), 'preprocess' => FALSE));

  /**
   * Add local.css file for CSS overrides.
   */
  drupal_add_css(drupal_get_path('theme', 'levelplus') . '/local.css', array('group' => CSS_THEME, 'type' => 'file'));

  /**
  * Add Javascript for enable/disable Bootstrap 3 Javascript.
  */
  if (theme_get_setting('bootstrap_js_include')) {
    drupal_add_js(drupal_get_path('theme', 'levelplus') . '/bootstrap/js/bootstrap.min.js');
  }
  //EOF:Javascript

  /**
  * Add Javascript for enable/disable scrollTop action.
  */
  if (theme_get_setting('scrolltop_display')) {
    drupal_add_js('
      jQuery(document).ready(function($) {
        $(window).scroll(function() {
          if($(this).scrollTop() != 0) {
            $("#toTop").addClass("show");
          } else {
            $("#toTop").removeClass("show");
          }
        });
        $("#toTop").click(function() {
          $("body,html").animate({scrollTop:0},800);
        });
      });
    ',array('type' => 'inline', 'scope' => 'header'));
  }
  //EOF:Javascript

  /**
  * Add Javascript for enable/disable Isotope Javascript.
  */
  if (theme_get_setting('isotope_js_include')) {

    $layout_mode = theme_get_setting('isotope_layout');

    drupal_add_js(drupal_get_path('theme', 'levelplus') .'/js/isotope/isotope.pkgd.min.js', array('preprocess' => false));
    drupal_add_js(drupal_get_path('theme', 'levelplus') .'/js/imagesloaded/imagesloaded.pkgd.min.js', array('preprocess' => false));

    drupal_add_js('
  		jQuery(document).ready(function($) {

        //Masonry Layout
        $(".grid-masonry-container").fadeIn("slow");
        $(".grid-masonry-container").imagesLoaded(function() {
          $(".grid-masonry-container").isotope({
            itemSelector: ".masonry-grid-item",
            layoutMode: "masonry"
          });
          $(".grid-masonry-container").isotope("layout");
        });

  			//Masonry Layout Style 2
        $(".grid-masonry-container-style-2").fadeIn("slow");
        $(".grid-masonry-container-style-2").imagesLoaded(function() {
          $(".grid-masonry-container-style-2").isotope({
            itemSelector: ".masonry-grid-item",
            layoutMode: "fitRows"
          });
          $(".grid-masonry-container-style-2").isotope("layout");
        });

        //Grid Layout Isotope
        $(".filters").fadeIn("slow");
        $(".filter-items").fadeIn("slow");
        var container = $(".filter-items"),
        filters= $(".view-promoted-items .filters");
        $(".filters").prepend( "<li class=\"active\"><a href=\"#\" data-filter=\"*\">All</a></li>" );

        container.imagesLoaded(function() {
          container.isotope({
            itemSelector: ".isotope-item",
            layoutMode : "'.$layout_mode.'",
            transitionDuration: "0.6s",
            filter: "*"
          });
          filters.find("a").click(function(){
            var $this = $(this);
            var selector = $this.attr("data-filter").replace(/\s+/g, "-");
            filters.find("li.active").removeClass("active");
            $this.parent().addClass("active");
            container.isotope({ filter: selector });
            return false;
          });
          container.isotope("layout");
        });

        //Masonry Layout Isotope
        var masonryContainer = $(".grid-masonry-isotope-container"),
        filtersMasonry= $(".view-promoted-items-masonry .filters");

        $(".grid-masonry-isotope-container").fadeIn("slow");

        masonryContainer.imagesLoaded(function() {
          masonryContainer.isotope({
            itemSelector: ".masonry-grid-item",
            layoutMode : "masonry",
            transitionDuration: "0.6s",
            filter: "*"
          });
          filtersMasonry.find("a").click(function(){
            var $this = $(this);
            var selector = $this.attr("data-filter").replace(/\s+/g, "-");
            filtersMasonry.find("li.active").removeClass("active");
            $this.parent().addClass("active");
            masonryContainer.isotope({ filter: selector });
            return false;
          });
          masonryContainer.isotope("layout");
        });

      });
    ',array('type' => 'inline', 'scope' => 'footer', 'weight' => 2));
  }
  //EOF:Javascript

  /**
  * Add Javascript for Google Map
  */
  if (theme_get_setting('google_map_js')) {

		drupal_add_js('
      jQuery(document).ready(function($) {
        var map;
        var myLatlng;
        var myZoom;
        var marker;
      });
    ',array('type' => 'inline', 'scope' => 'header'));

    $google_maps_key = theme_get_setting('google_map_key');
    if (!empty($google_maps_key)) {
      $google_maps_key_string = '&key='.$google_maps_key;
    } else {
      $google_maps_key_string = '';
    }

    drupal_add_js('https://maps.googleapis.com/maps/api/js?v=3'.$google_maps_key_string,array('type' => 'external', 'scope' => 'header', 'group' => 'JS_THEME'));

    $google_map_latitude = theme_get_setting('google_map_latitude');
    drupal_add_js(array('levelplus' => array('google_map_latitude' => $google_map_latitude)), 'setting');
    $google_map_longitude = theme_get_setting('google_map_longitude');
    drupal_add_js(array('levelplus' => array('google_map_longitude' => $google_map_longitude)), 'setting');
    $google_map_zoom = (int) theme_get_setting('google_map_zoom');
    $google_map_canvas = theme_get_setting('google_map_canvas');
    drupal_add_js(array('levelplus' => array('google_map_canvas' => $google_map_canvas)), 'setting');

		drupal_add_js('
      jQuery(document).ready(function($) {
        if ($("#'.$google_map_canvas.'").length) {

          myLatlng = new google.maps.LatLng(Drupal.settings.levelplus[\'google_map_latitude\'], Drupal.settings.levelplus[\'google_map_longitude\']);
          myZoom = '.$google_map_zoom.';

          function initialize() {
            var mapOptions = {
              zoom: myZoom,
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              center: myLatlng,
              scrollwheel: false
            };
            map = new google.maps.Map(document.getElementById(Drupal.settings.levelplus[\'google_map_canvas\']),mapOptions);
            marker = new google.maps.Marker({
              map:map,
              draggable:true,
              position: myLatlng,
              url: "https://www.google.com/maps/dir//'.$google_map_latitude.','.$google_map_longitude.'/@'.$google_map_latitude.','.$google_map_longitude.'"
            });
            google.maps.event.addListener(marker, "click", function() {
              window.open(this.url, "_blank");
            });
            google.maps.event.addDomListener(window, "resize", function() {
              map.setCenter(myLatlng);
            });
          }
          google.maps.event.addDomListener(window, "load", initialize);

        }
      });
    ',array('type' => 'inline', 'scope' => 'header'));

    $google_map_show=theme_get_setting('google_map_show');
    $google_map_hide=theme_get_setting('google_map_hide');

    /**
    * Add Javascript
    */
    drupal_add_js('
      jQuery(window).load(function() {
        if (jQuery(".map-trigger").length>0) {
          jQuery(".map-trigger").html("<i class=\"fa fa-map-o\"></i> <a data-toggle=\"collapse\" href=\"javascript:showMap()\"  data-target=\"#map-canvas-container\" aria-expanded=\"false\" aria-controls=\"map-canvas-container\">'.$google_map_show.'</a>");
        };
      });
      function hideMap(){
        jQuery(".map-trigger").html("<i class=\"fa fa-map-o\"></i> <a data-toggle=\"collapse\" href=\"javascript:showMap()\"  data-target=\"#map-canvas-container\" aria-expanded=\"false\" aria-controls=\"map-canvas-container\">'.$google_map_show.'</a>");
      }
      function showMap() {
        jQuery(".map-trigger").html("<i class=\"fa fa-map-o\"></i> <a data-toggle=\"collapse\" href=\"javascript:hideMap()\"  data-target=\"#map-canvas-container\" aria-expanded=\"false\" aria-controls=\"map-canvas-container\">'.$google_map_hide.'</a>");
        jQuery("#map-canvas-container").one("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(){
          google.maps.event.trigger(map, "resize");
          map.setCenter(myLatlng);
          map.setZoom(myZoom);
        });
        google.maps.event.trigger(map, "resize");
        map.setCenter(myLatlng);
        map.setZoom(myZoom);
      }
    ',array('type' => 'inline', 'scope' => 'footer'));
    //EOF:Javascript

	}

  /**
  * Add Javascript for Fixed Header
  */
  $fixed_header = theme_get_setting('fixed_header');
  if ($fixed_header) {

    drupal_add_js('
      jQuery(document).ready(function($) {

        var navItemHeight =  $("#main-navigation ul.menu li a").outerHeight(),
        navHeight =  $("#main-navigation").outerHeight(),
        toolbarHeight =  $("#toolbar").outerHeight(),
        headerHeight = $("#header").outerHeight();

        if ($("#toolbar").length>0) {
          var toolbarHeight =  $("#toolbar").outerHeight();
        } else {
          var toolbarHeight =  0;
        }

        // fix for Chrome
        if (navHeight > navItemHeight*2) {
          headerHeight = headerHeight - navItemHeight;
        };

        $(".logged-in .tabs.primary a, .logged-in .contextual-links a").click(function() {
          $("body").removeClass("onscroll");
          $("#header-container").css("paddingBottom", (0)+"px");
          $("#header").css("top", (0)+"px");
        });

        $(window).load(function() {
          if(($(window).width() > 767)) {
            $("body").addClass("fixed-header-enabled");
          } else {
            $("body").removeClass("fixed-header-enabled");
          }
        });

        $(window).resize(function() {
          if(($(window).width() > 767)) {
            $("body").addClass("fixed-header-enabled");
          } else {
            $("body").removeClass("fixed-header-enabled");
          }
        });

        $(window).scroll(function() {
          if (!(($(".transparent-header-active").length>0) && ($("#banner #slideshow-fullscreen").length>0))) {
            if (($(this).scrollTop() > headerHeight) && ($(window).width() > 767)) {
              $("body").addClass("onscroll");
              $("#header-container").css("paddingBottom", (headerHeight)+"px");
              $("#header").css("top", (toolbarHeight)+"px");
            } else {
              $("body").removeClass("onscroll");
              $("#header-container").css("paddingBottom", (0)+"px");
              $("#header").css("top", (0)+"px");
            }
          } else {
            if (($(this).scrollTop() > headerHeight) && ($(window).width() > 767)) {
              $("body").addClass("onscroll");
              $("#header").css("top", (toolbarHeight)+"px");
            } else {
              $("body").removeClass("onscroll");
              $("#header").css("top", (0)+"px");
            }
          };
        });
      });
    ',array('type' => 'inline', 'scope' => 'footer'));
		//EOF:Javascript

  }

  /**
  * Mobile Menu
  */
  $responsive_meanmenu = theme_get_setting('responsive_menu_state');

  if ($responsive_meanmenu) {
    $responsive_menu_header_top_switchwidth=theme_get_setting('responsive_menu_header_top_switchwidth');
    $responsive_menu_header_first_switchwidth=theme_get_setting('responsive_menu_header_first_switchwidth');
    $responsive_menu_header_third_switchwidth=theme_get_setting('responsive_menu_header_third_switchwidth');

    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/js/meanmenu/meanmenu.css');
    drupal_add_js(drupal_get_path('theme', 'levelplus') .'/js/meanmenu/jquery.meanmenu.fork.js', array('preprocess' => false));

    /**
    * Add Javascript for Mobile Menu
    */
    drupal_add_js('
      jQuery(document).ready(function($) {

        $("#header-top-inside .sf-menu, #header-top-inside .content>ul.menu, #header-top-inside ul.main-menu").wrap("<div class=\'meanmenu-wrapper-header-top\'></div>");
        $("#header-top-inside .meanmenu-wrapper-header-top").meanmenu({
          meanScreenWidth: '.$responsive_menu_header_top_switchwidth.',
          meanRemoveAttrs: true,
          meanMenuContainer: "#header-top-inside",
          meanMenuClose: ""
        });

        $("#header-inside-first .sf-menu, #header-inside-first .content>ul.menu, #header-inside-first ul.main-menu").wrap("<div class=\'meanmenu-wrapper-first\'></div>");
        $("#header-inside-first .meanmenu-wrapper-first").meanmenu({
          meanScreenWidth: '.$responsive_menu_header_first_switchwidth.',
          meanRemoveAttrs: true,
          meanMenuContainer: "#mean-menu-first",
          meanMenuClose: "",
          meanRevealPositionDistance: "100"
        });

        $("#main-navigation .sf-menu, #main-navigation .content>ul.menu, #main-navigation ul.main-menu").wrap("<div class=\'meanmenu-wrapper-third\'></div>");
        $("#main-navigation .meanmenu-wrapper-third").meanmenu({
          meanScreenWidth: '.$responsive_menu_header_third_switchwidth.',
          meanRemoveAttrs: true,
          meanMenuContainer: "#mean-menu-third",
          meanMenuClose: ""
        });

      });
    ',array('type' => 'inline', 'scope' => 'footer'));
    //EOF:Javascript
  }

  /**
  * Parallax Section.
  */
  $parallax_state = theme_get_setting('parallax_and_video_bg_state');

  if (!empty($variables['page']['highlighted_bottom'])) {
    $parallax_and_video_bg_opacity = (int) theme_get_setting('parallax_and_video_bg_opacity')/100;
    drupal_add_js('
      jQuery(document).ready(function($) {
        var color = $("#highlighted-bottom").css("background-color").replace(")", ", '.$parallax_and_video_bg_opacity.')").replace("rgb", "rgba");
        $("#highlighted-bottom-transparent-bg").css("background-color", color);
      });
    ',array('type' => 'inline', 'scope' => 'footer'));
    //EOF:Javascript
  }

  if ($parallax_state == "parallax") {
    drupal_add_js(drupal_get_path('theme', 'levelplus') .'/js/jquery.parallax-1.1.3.js', array('preprocess' => false));
    $variables['classes_array'][] = 'parallax-active';

    /**
    * Add Javascript for Parallax Section.
    */
    drupal_add_js('
      jQuery(document).ready(function($) {
        if (($(".parallax-active #highlighted-bottom").length>0)  && !Modernizr.touch ){
          $(".parallax-active #highlighted-bottom").parallax("50%", 0.2, false);
        };
      });
    ',array('type' => 'inline', 'scope' => 'footer'));
    //EOF:Javascript
  }

  /**
  * Background Video.
  */
  $video_bg_state = theme_get_setting('parallax_and_video_bg_state');

  if ($video_bg_state == "video" && !empty($variables['page']['highlighted_bottom'])) {
    drupal_add_js(drupal_get_path('theme', 'levelplus') . '/js/vide/jquery.vide.min.js');

    $path_to_video_mp4 = file_create_url(drupal_get_path('theme', 'levelplus') . '/videos/background-video.mp4');
    $path_to_video_webm = file_create_url(drupal_get_path('theme', 'levelplus') . '/videos/background-video.webm');
    $path_to_parallax_image =  file_create_url(drupal_get_path('theme', 'levelplus') . '/images/parallax-bg.jpg');
    /**
    * Add Javascript for Video Background Section.
    */
    drupal_add_js('
      jQuery(document).ready(function($) {
        $("body").addClass("video-bg-active");
        $(".video-bg-active #highlighted-bottom").vide({
          mp4: "'.$path_to_video_mp4.'",
          webm: "'.$path_to_video_webm.'",
          poster: "'.$path_to_parallax_image.'"
        },{
          posterType: "jpg",
          className: "video-container"
        });
        $(window).load(function() {
          $("#big-video-wrap").fadeIn();
        });
      });
      ',array('type' => 'inline', 'scope' => 'footer'));
    //EOF:Javascript
  }

  /**
  * Transparent Header.
  */
  $transparent_header_state = theme_get_setting('transparent_header_state');

  if ($transparent_header_state) {
    $variables['classes_array'][] = 'transparent-header-active';
    $transparent_header_opacity = (int) theme_get_setting('transparent_header_opacity')/100;

    /**
    * Add Javascript for Transparent Header.
    */
    drupal_add_js('
      jQuery(document).ready(function($) {
        if($("#banner #slideshow-fullscreen").length>0) {
          $("#header-container").addClass("transparent-header");
        } else {
          $("#header-container").removeClass("transparent-header");
        };
        $("#header-container.transparent-header").css("backgroundColor", "rgba(30,37,39,'.$transparent_header_opacity.')");
      });
    ',array('type' => 'inline', 'scope' => 'footer'));
    //EOF:Javascript
  }

  /**
  * Add Javascript for Animations
  */
  $animations_state = theme_get_setting('animations_state');
  if ($animations_state) {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/js/animate/animate.css');
    drupal_add_js(drupal_get_path('theme', 'levelplus') . '/js/waypoints/jquery.waypoints.js');

    drupal_add_js('jQuery(document).ready(function($) {
      if ($("body:not(.page-admin) [data-animate-effect]").length>0) {
        $("body:not(.page-admin) [data-animate-effect]").each(function() {
          var animation = $(this).attr("data-animate-effect");
          if(animation != "no-animation") {
            var waypoints = $(this).waypoint(function(direction) {
              var animatedObject = $(this.element);
              setTimeout(function() {
                animatedObject.addClass("animated object-visible " + animatedObject.attr("data-animate-effect"));
              }, 100);
              this.destroy();
              },{
                offset: "90%"
              });
            }
          });
        }
      })
    ',array('type' => 'inline', 'scope' => 'footer'));
  }

  /**
  * Add Javascript for Smooth Scroll.
  */
  drupal_add_js('
    jQuery(document).ready(function($) {
      if ($(".smooth-scroll").length>0) {
        $(window).load(function() {
          if(($(".fixed-header-enabled").length>0) && (Modernizr.mq("only all and (min-width: 768px)"))) {
            $(".smooth-scroll a[href*=#]:not([href=#]), a[href*=#]:not([href=#]).smooth-scroll").click(function() {
              if (location.pathname.replace(/^\//,"") == this.pathname.replace(/^\//,"") && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $("[name=" + this.hash.slice(1) +"]");
                if (target.length) {
                  $("html,body").animate({
                    scrollTop: target.offset().top-64
                  }, 1000);
                  return false;
                }
              }
            });
          } else {
            $(".smooth-scroll a[href*=#]:not([href=#]), a[href*=#]:not([href=#]).smooth-scroll").click(function() {
              if (location.pathname.replace(/^\//,"") == this.pathname.replace(/^\//,"") && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $("[name=" + this.hash.slice(1) +"]");
                if (target.length) {
                  $("html,body").animate({
                    scrollTop: target.offset().top
                  }, 1000);
                  return false;
                }
              }
            });
          }
        });
      }
    });
  ',array('type' => 'inline', 'scope' => 'footer'));

  /**
  * Add Javascript for testimonial slider.
  */
  $testimonial_effect=theme_get_setting('testimonial_slideshow_effect');
  $testimonial_effect_time=theme_get_setting('testimonial_slideshow_effect_time')*1000;

  drupal_add_js('
    jQuery(document).ready(function($) {
      if ($(".view-testimonials-slider").length>0){
        $(window).load(function() {
          $(".view-testimonials-slider .flexslider").fadeIn("slow");
          $(".view-testimonials-slider .flexslider").flexslider({
            animation: "'.$testimonial_effect.'",
            slideshowSpeed: '.$testimonial_effect_time.',
            useCSS: false,
            prevText: "prev",
            nextText: "next",
            controlNav: false
          });
        });
      }
    });
  ',array('type' => 'inline', 'scope' => 'footer', 'weight' => 2));
  //EOF:Javascript

  /**
  * Add Javascript - Owl Carousel
  */
  if (theme_get_setting('owl_include')) {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/js/owl-carousel/owl.carousel.css');
    drupal_add_js(drupal_get_path('theme', 'levelplus') . '/js/owl-carousel/owl.carousel.js');
    $owl_autoplay = theme_get_setting('owl_autoplay');
    if ($owl_autoplay) {
      $owl_effect_time = (int) theme_get_setting('owl_effect_time')*1000;
    } else {
      $owl_effect_time = "false";
    }

    drupal_add_js('
      jQuery(document).ready(function($) {
        $(".view-promoted-items .owl-carousel.posts").owlCarousel({
          items: 4,
          itemsDesktopSmall: [992,2],
          itemsTablet: [768,2],
          autoPlay: '.$owl_effect_time.',
          navigation: true,
          pagination: false
        });
      });
    ',array('type' => 'inline', 'scope' => 'footer'));

		$owl_team_autoplay = theme_get_setting('owl_team_autoplay');
		if ($owl_team_autoplay) {
			$owl_team_effect_time = (int) theme_get_setting('owl_team_effect_time')*1000;
		} else {
			$owl_team_effect_time = "false";
		}

		drupal_add_js('
      jQuery(document).ready(function($) {
        $(".view-promoted-items .owl-carousel.team").owlCarousel({
          items: 4,
          itemsDesktopSmall: [992,2],
          itemsTablet: [768,2],
          autoPlay: '.$owl_team_effect_time.',
          navigation: true,
          pagination: false
        });
      });
    ',array('type' => 'inline', 'scope' => 'footer'));

    $owl_testimonials_autoplay = theme_get_setting('owl_testimonials_autoplay');
    if ($owl_testimonials_autoplay) {
      $owl_testimonials_effect_time = (int) theme_get_setting('owl_testimonials_effect_time')*1000;
    } else {
      $owl_testimonials_effect_time = "false";
    }

    drupal_add_js('
      jQuery(document).ready(function($) {
        $(".view-testimonials-carousel .owl-carousel.testimonials").owlCarousel({
          items: 4,
          itemsDesktopSmall: [992,2],
          itemsTablet: [768,2],
          autoPlay: '.$owl_testimonials_effect_time.',
          navigation: true,
          pagination: false
        });
      });
    ',array('type' => 'inline', 'scope' => 'footer'));

    $owl_logos_autoplay = theme_get_setting('owl_logos_autoplay');
    if ($owl_logos_autoplay) {
      $owl_logos_effect_time = (int) theme_get_setting('owl_logos_effect_time')*1000;
    } else {
      $owl_logos_effect_time = "false";
    }

    drupal_add_js('
      jQuery(document).ready(function($) {
        $(".view-logos-carousel .owl-carousel.logos").owlCarousel({
          items: 5,
          itemsDesktopSmall: [992,3],
          itemsTablet: [768,3],
          autoPlay: '.$owl_logos_effect_time.',
          navigation: true,
          pagination: false
        });
      });
    ',array('type' => 'inline', 'scope' => 'footer'));
  }
  //EOF:Javascript

  /**
  * Add Javascript - Affix
  */
  $affix_admin_height = (int)  theme_get_setting('affix_admin_height');
  $affix_fixedHeader_height = (int) theme_get_setting('affix_fixedHeader_height');

  drupal_add_js('
    jQuery(document).ready(function($) {
      if (jQuery("#affix").length>0) {
        $(window).load(function() {

          var affixBottom = $("#footer").outerHeight(true)
          + $("#subfooter").outerHeight(true)
          + $("#footer-top").outerHeight(true)
          + $("#page").outerHeight(true)
          - $("#block-system-main").outerHeight(true),
          affixTop = $("#affix").offset().top;

          //The #header height
          var staticHeaderHeight = $("#header").outerHeight(true);

          //The #header height onscroll while fixed (it is usually smaller than staticHeaderHeight)
          //We can not calculate it because we need to scroll first
          var fixedheaderHeight = '.$affix_fixedHeader_height.';

          //The admin overlay menu height
          var adminHeight = '.$affix_admin_height.';

  		    function initializeAffix(topAffix) {
            if ($(".fixed-header-enabled").length>0) {
              if ($(".logged-in").length>0) {
                affixBottom = affixBottom-staticHeaderHeight-fixedheaderHeight-adminHeight-15;
                initAffixTop = topAffix-fixedheaderHeight-adminHeight-15; //The fixedheaderHeight and adminHeight are added as padding on the page so we need to remove it from affixTop
              } else {
                affixBottom = affixBottom-staticHeaderHeight-fixedheaderHeight-15;
                initAffixTop = topAffix-fixedheaderHeight-15;  //The fixedheaderHeight is added as padding on the page so we need to remove it from affixTop
              }
            } else {
              if ($(".logged-in").length>0) {
                affixBottom = affixBottom;
                initAffixTop = topAffix-adminHeight-15; // The adminHeight is added as padding on the page so we need to remove it from affixTop
              } else {
                affixBottom = affixBottom+adminHeight;
                initAffixTop = topAffix-15; //We reduce by 15 to make a little space between the window top and the #affix element
              }
            }
            $("#affix").affix({
              offset: {
                top: initAffixTop,
                bottom: affixBottom
              }
            });
  		    }

          initializeAffix(affixTop);

          $("#affix").on("affixed.bs.affix", function () {
            //We set through JS the inline style top position
            if ($(".fixed-header-enabled").length>0) {
              if ($(".logged-in").length>0) {
                $("#affix").css("top", (fixedheaderHeight + adminHeight + 15)+"px");
              } else {
                $("#affix").css("top", (fixedheaderHeight + 15)+"px");
              }
            } else {
              if ($(".logged-in").length>0) {
                $("#affix").css("top", (adminHeight + 15)+"px");
              } else {
                $("#affix").css("top", (15)+"px");
              }
            }
          });

        });
      }
    });
  ',array('type' => 'inline', 'scope' => 'footer'));

  if (theme_get_setting('post_progress')) {
    /**
    * Add Javascript - Node progress bar
    */
    drupal_add_js('
      jQuery(document).ready(function($) {

        $(window).load(function () {
          if ($(".post-progress").length>0){
            var s = $(window).scrollTop(),
            c = $(window).height(),
            d = $(".node-main-content>.content").outerHeight(),
            g = $(".node-main-content>.content").offset().top;

            var scrollPercent = (s / (d+g-c)) * 100;
            scrollPercent = Math.round(scrollPercent);

            if (c >= (d+g)) { scrollPercent = 100; } else if (scrollPercent < 0) { scrollPercent = 0; } else if (scrollPercent > 100) { scrollPercent = 100; }

            $(".post-progressbar").css("width", scrollPercent + "%");
            $(".post-progress-value").html(scrollPercent + "%");
          }
        });

        $(window).scroll(function () {
          if ($(".post-progress").length>0){
            var s = $(window).scrollTop(),
            c = $(window).height(),
            d = $(".node-main-content>.content").outerHeight(true),
            g = $(".node-main-content>.content").offset().top;

            var scrollPercent = (s / (d+g-c)) * 100;
            scrollPercent = Math.round(scrollPercent);

            if (c >= (d+g)) { scrollPercent = 100; }  else if (scrollPercent < 0) { scrollPercent = 0; } else if (scrollPercent > 100) { scrollPercent = 100; }

            $(".post-progressbar").css("width", scrollPercent + "%");
            $(".post-progress-value").html(scrollPercent + "%");
          }
        });

      });
    ',array('type' => 'inline', 'scope' => 'footer'));
  }

  /**
  * Add Javascript - Button feedback effect
  */
  drupal_add_js('
    jQuery(document).ready(function($) {
      if ($(".cbutton-effect").length>0) {
        $(".cbutton-effect").click(function(e) {
          e.preventDefault();
          e.stopPropagation();
          $(this).addClass("cbutton-click").delay(350).queue(function(){
            $(this).removeClass("cbutton-click").dequeue();
          });
        });
      }
    });
  ',array('type' => 'inline', 'scope' => 'footer'));
  //EOF:Javascript

  /**
  * Add Javascript - Navigation Search
  */
  drupal_add_js('
    jQuery(document).ready(function($) {
      if ($("#search-area .nav-search.style-2").length>0) {
        $("#search-area").addClass("bordered");
      }
      $( "#search-area .nav-search.style-2 .form-text" ).focusin(function() {
        $("#search-area").addClass("opened");
      });
      $( "#search-area .nav-search.style-2 .form-text" ).focusout(function() {
        $("#search-area").removeClass("opened");
      });
    });
  ',array('type' => 'inline', 'scope' => 'footer'));
  //EOF:Javascript

  /**
  * Add Javascript - Make the commerce tables responsive
  */
  drupal_add_js('
  jQuery(document).ready(function($) {
    if ($(".page-cart table.views-table").length>0 || $(".page-checkout table.views-table").length>0 || $(".page-checkout-review table.checkout-review").length>0 || $(".page-user-orders table.views-table").length>0) {
      $(".page-cart table.views-table, .page-checkout table.views-table, .page-checkout-review table.checkout-review, .page-user-orders table.views-table").wrap("<div class=\'table-responsive\'></div>");
    };
  });
  ',array('type' => 'inline', 'scope' => 'header'));
  //EOF:Javascript

}

/**
 * Override or insert variables into the html template.
 */
function levelplus_process_html(&$vars) {

  $classes = explode(' ', $vars['classes']);
  $classes[] = theme_get_setting('sitename_font_family');
  $classes[] = theme_get_setting('slogan_font_family');
  $classes[] = theme_get_setting('headings_font_family');
  $classes[] = theme_get_setting('paragraph_font_family');
  $classes[] = theme_get_setting('form_style');
  $classes[] = theme_get_setting('layout_mode');
  $vars['classes'] = trim(implode(' ', $classes));

}

/**
 * Preprocess variables for page template.
 */
function levelplus_preprocess_page(&$variables) {

  $header = $variables['page']['header'];
  $header_first = $variables['page']['header_first'];
  $logo = $variables['logo'];
  $site_name = $variables['site_name'];
  $site_slogan = $variables['site_slogan'];
  $sidebar_first = $variables['page']['sidebar_first'];
  $sidebar_second = $variables['page']['sidebar_second'];
  $content_bottom_first = $variables['page']['content_bottom_first'];
  $content_bottom_second = $variables['page']['content_bottom_second'];
  $footer_top_first = $variables['page']['footer_top_first'];
  $footer_top_second = $variables['page']['footer_top_second'];
  $footer_first = $variables['page']['footer_first'];
  $footer_second = $variables['page']['footer_second'];
  $footer_third = $variables['page']['footer_third'];
  $footer_fourth = $variables['page']['footer_fourth'];
  $subfooter_second = $variables['page']['footer'];
  $subfooter_first = $variables['page']['sub_footer_first'];

  /**
  * Insert variables into the page template.
  */

  /*Header*/
  $variables['header_layout_container'] = theme_get_setting('header_layout_container');

  if (($header_first && !($logo || $site_name || $site_slogan || $header)) || (!$header_first && ($logo || $site_name || $site_slogan || $header))) {
    $variables['header_layout_columns_class'] = 'two-columns';
    if ($variables['header_layout_container'] == 'container-fluid') {
      $variables['header_first_grid_class'] = 'col-md-4 col-lg-3';
      $variables['header_second_grid_class'] = 'col-md-4 col-lg-3';
      $variables['header_third_grid_class'] = 'col-md-8 col-lg-9';
    } else {
      $variables['header_first_grid_class'] = 'col-md-4';
      $variables['header_second_grid_class'] = 'col-md-4';
      $variables['header_third_grid_class'] = 'col-md-8';
    }
  } else if ($header_first && ($logo || $site_name || $site_slogan || $header)) {
    $variables['header_layout_columns_class'] = 'three-columns';
    $variables['header_first_grid_class'] = 'col-md-4 col-sm-12';
    $variables['header_second_grid_class'] = 'col-md-4 col-sm-12';
    $variables['header_third_grid_class'] = 'col-md-4 col-sm-12';
  } else {
    $variables['header_layout_columns_class'] = 'one-column';
    $variables['header_third_grid_class'] = 'col-xs-12';
  }

  if ($variables['header_layout_container'] == 'container-fluid') {
    $variables['header_layout_container_class'] = 'full-width';
  } else {
    $variables['header_layout_container_class'] = 'fixed-width';
  }

  /*Content Top*/
  $variables['content_top_layout_container'] = theme_get_setting('content_top_layout_container');

  /*Main Content Layout*/
  if($sidebar_first && $sidebar_second) {
    $variables['main_grid_class'] = 'col-md-6 col-md-push-3';
    $variables['sidebar_first_grid_class'] = 'col-md-3 col-md-pull-6';
    $variables['sidebar_second_grid_class'] = 'col-md-3';
  } elseif ($sidebar_first && !$sidebar_second) {
    $variables['main_grid_class'] = 'col-md-8 col-md-push-4';
    $variables['sidebar_first_grid_class'] = 'col-md-4 col-md-pull-8 fix-sidebar-first';
    $variables['sidebar_second_grid_class'] = '';
  } elseif (!$sidebar_first && $sidebar_second) {
    $variables['main_grid_class'] = 'col-md-8';
    $variables['sidebar_first_grid_class'] = '';
    $variables['sidebar_second_grid_class'] = 'col-md-4 fix-sidebar-second';
  } else {
    $variables['main_grid_class'] = 'col-md-12';
    $variables['sidebar_first_grid_class'] = '';
    $variables['sidebar_second_grid_class'] = '';
  }

  /*Content Bottom*/
  if($content_bottom_second && $content_bottom_first) {
    $variables['content_bottom_first_grid_class'] = 'col-md-5';
    $variables['content_bottom_second_grid_class'] = 'col-md-5 col-md-offset-2';
  } elseif ($content_bottom_second || $content_bottom_first) {
    $variables['content_bottom_first_grid_class'] = 'col-md-12';
    $variables['content_bottom_second_grid_class'] = 'col-md-12';
  }

  /*Featured Top*/
  $variables['featured_top_layout_container'] = theme_get_setting('featured_top_layout_container');

  /*Featured*/
  $variables['featured_layout_container'] = theme_get_setting('featured_layout_container');

  /*Featured Bottom*/
  $variables['featured_bottom_layout_container'] = theme_get_setting('featured_bottom_layout_container');

  /*Highlighted*/
  $variables['highlighted_layout_container'] = theme_get_setting('highlighted_layout_container');

  /*Highlighted Bottom*/
  $variables['highlighted_bottom_layout_container'] = theme_get_setting('highlighted_bottom_layout_container');

  /*Footer Top*/
  if($footer_top_first && $footer_top_second) {
    $variables['footer_top_regions'] = 'two-regions';
    $variables['footer_top_first_grid_class'] = 'col-sm-6';
    $variables['footer_top_second_grid_class'] = 'col-sm-6';
  } elseif ($footer_top_second || $footer_top_first) {
    $variables['footer_top_regions'] = 'one-region';
    $variables['footer_top_first_grid_class'] = 'col-md-12';
    $variables['footer_top_second_grid_class'] = 'col-md-12';
  }

  /*Footer Layout*/
  $variables['footer_layout_container'] = theme_get_setting('footer_layout_container');

  if ($footer_first && $footer_second && $footer_third && $footer_fourth) {
    $variables['footer_grid_class'] = 'col-sm-6 col-md-3';
  } elseif ((!$footer_first && $footer_second && $footer_third && $footer_fourth) || ($footer_first && !$footer_second && $footer_third && $footer_fourth)
   || ($footer_first && $footer_second && !$footer_third && $footer_fourth) || ($footer_first && $footer_second && $footer_third && !$footer_fourth)) {
    $variables['footer_grid_class'] = 'col-sm-4';
  } elseif ((!$footer_first && !$footer_second && $footer_third && $footer_fourth) || (!$footer_first && $footer_second && !$footer_third && $footer_fourth)
   || (!$footer_first && $footer_second && $footer_third && !$footer_fourth) || ($footer_first && !$footer_second && !$footer_third && $footer_fourth)
   || ($footer_first && !$footer_second && $footer_third && !$footer_fourth) || ($footer_first && $footer_second && !$footer_third && !$footer_fourth)) {
    $variables['footer_grid_class'] = 'col-sm-6';
  } else {
    $variables['footer_grid_class'] = 'col-sm-12';
  }

  /*Subfooter*/
  $variables['subfooter_layout_container'] = theme_get_setting('subfooter_layout_container');

  if($subfooter_second && $subfooter_first) {
    $variables['subfooter_first_grid_class'] = 'col-md-6';
    $variables['subfooter_second_grid_class'] = 'col-md-6';
  } elseif ($subfooter_second || $subfooter_first) {
    $variables['subfooter_second_grid_class'] = 'col-md-12 text-center';
    $variables['subfooter_first_grid_class'] = 'col-md-12 text-center';
  }

	/*Breadcrumb*/
	$variables['breadcrumb_display'] = theme_get_setting("breadcrumb_display");

	/*Scroll to top*/
	$variables['scrolltop_display'] = theme_get_setting('scrolltop_display');

	/*Responsive menu*/
	$variables['responsive_menu'] = theme_get_setting('responsive_menu_state');

	/*Frontpage content print*/
	$variables['frontpage_content_print'] = theme_get_setting('frontpage_content_print');

  /*Animations*/
  $variables['content_top_animation_effect'] = theme_get_setting('content_top_animation_effect');
  if (theme_get_setting('animations_state') && theme_get_setting('content_top_animation_effect') != "no-animation") {
    $variables['content_top_animations'] = "enabled";
  } else {
    $variables['content_top_animations'] = "disabled";
  }

  $variables['main_content_animation_effect'] = theme_get_setting('main_content_animation_effect');
  if (theme_get_setting('animations_state') && theme_get_setting('main_content_animation_effect') != "no-animation") {
    $variables['main_content_animations'] = "enabled";
  } else {
    $variables['main_content_animations'] = "disabled";
  }

  $variables['content_bottom_animation_effect'] = theme_get_setting('content_bottom_animation_effect');
  if (theme_get_setting('animations_state') && theme_get_setting('content_bottom_animation_effect') != "no-animation") {
    $variables['content_bottom_animations'] = "enabled";
  } else {
    $variables['content_bottom_animations'] = "disabled";
  }

  $variables['featured_top_animation_effect'] = theme_get_setting('featured_top_animation_effect');
  if (theme_get_setting('animations_state') && theme_get_setting('featured_top_animation_effect') != "no-animation") {
    $variables['featured_top_animations'] = "enabled";
  } else {
    $variables['featured_top_animations'] = "disabled";
  }

  $variables['featured_animation_effect'] = theme_get_setting('featured_animation_effect');
  if (theme_get_setting('animations_state') && theme_get_setting('featured_animation_effect') != "no-animation") {
    $variables['featured_animations'] = "enabled";
  } else {
    $variables['featured_animations'] = "disabled";
  }

  $variables['featured_bottom_animation_effect'] = theme_get_setting('featured_bottom_animation_effect');
  if (theme_get_setting('animations_state') && theme_get_setting('featured_bottom_animation_effect') != "no-animation") {
    $variables['featured_bottom_animations'] = "enabled";
  } else {
    $variables['featured_bottom_animations'] = "disabled";
  }

  $variables['highlighted_animation_effect'] = theme_get_setting('highlighted_animation_effect');
  if (theme_get_setting('animations_state') && theme_get_setting('highlighted_animation_effect') != "no-animation") {
    $variables['highlighted_animations'] = "enabled";
  } else {
    $variables['highlighted_animations'] = "disabled";
  }

  $variables['highlighted_bottom_animation_effect'] = theme_get_setting('highlighted_bottom_animation_effect');
  if (theme_get_setting('animations_state') && theme_get_setting('highlighted_bottom_animation_effect') != "no-animation") {
    $variables['highlighted_bottom_animations'] = "enabled";
  } else {
    $variables['highlighted_bottom_animations'] = "disabled";
  }

  $variables['footer_top_animation_effect'] = theme_get_setting('footer_top_animation_effect');
  if (theme_get_setting('animations_state') && theme_get_setting('footer_top_animation_effect') != "no-animation") {
    $variables['footer_top_animations'] = "enabled";
  } else {
    $variables['footer_top_animations'] = "disabled";
  }

  $variables['footer_animation_effect'] = theme_get_setting('footer_animation_effect');
  if (theme_get_setting('animations_state') && theme_get_setting('footer_animation_effect') != "no-animation") {
    $variables['footer_animations'] = "enabled";
  } else {
    $variables['footer_animations'] = "disabled";
  }

  /*Background Colors*/
  $variables['content_top_background_color'] = theme_get_setting('content_top_background_color');
  $variables['content_bottom_background_color'] = theme_get_setting('content_bottom_background_color');
  $variables['featured_top_background_color'] = theme_get_setting('featured_top_background_color');
  $variables['featured_background_color'] = theme_get_setting('featured_background_color');
  $variables['featured_bottom_background_color'] = theme_get_setting('featured_bottom_background_color');
  $variables['highlighted_background_color'] = theme_get_setting('highlighted_background_color');
  $variables['highlighted_bottom_background_color'] = theme_get_setting('highlighted_bottom_background_color');
  $variables['footer_top_background_color'] = theme_get_setting('footer_top_background_color');
  $variables['footer_background_color'] = theme_get_setting('footer_background_color');
  $variables['subfooter_background_color'] = theme_get_setting('subfooter_background_color');

  /*Logo*/
  $alt_logo_fid = theme_get_setting('alternative_logo_fid');
  $alt_logo_path = theme_get_setting('alternative_logo_path');
  $alt_logo_inner_fid = theme_get_setting('alternative_logo_inner_fid');
  $alt_logo_inner_path = theme_get_setting('alternative_logo_inner_path');

  if (($alt_logo_fid != 0 || $alt_logo_path) && !(($alt_logo_inner_fid != 0 || $alt_logo_inner_path) && !drupal_is_front_page())) {
    if ($alt_logo_fid != 0) {
      $alternative_logo_file = file_load($alt_logo_fid);
      $alternative_logo_uri = $alternative_logo_file->uri;
      $alternative_logo_url = file_create_url($alternative_logo_uri);
    } else {
      $alternative_logo_url = file_create_url(file_default_scheme() . '://' . $alt_logo_path);
    }
    $variables['main_logo'] = $alternative_logo_url;
    $variables['main_logo_class'] = "alternative-logo";
  } elseif (($alt_logo_inner_fid != 0 || $alt_logo_inner_path) && !drupal_is_front_page()) {
    if ($alt_logo_inner_fid != 0 && !drupal_is_front_page()) {
      $alternative_logo_inner_file = file_load($alt_logo_inner_fid);
      $alternative_logo_inner_uri = $alternative_logo_inner_file->uri;
      $alternative_logo_inner_url = file_create_url($alternative_logo_inner_uri);
    } else {
      $alternative_logo_inner_url = file_create_url(file_default_scheme() . '://' . $alt_logo_inner_path);
    }
    $variables['main_logo'] = $alternative_logo_inner_url;
    $variables['main_logo_class'] = "alternative-logo-inner";
  } elseif ($variables['logo']) {
    $variables['main_logo'] = $variables['logo'];
    $variables['main_logo_class'] = "default-logo";
  }
}

/**
* Implements hook_preprocess_maintenance_page().
*/
function levelplus_preprocess_maintenance_page(&$variables) {

  $color_scheme = theme_get_setting('color_scheme');
  if ($color_scheme != 'default') {
    drupal_add_css(drupal_get_path('theme', 'levelplus') . '/style-' .$color_scheme. '.css', array('group' => CSS_THEME, 'type' => 'file'));
  }

  if (isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on') {
    $protocol = '/https';
  } else {
    $protocol = '/http';
  }

  drupal_add_css(drupal_get_path('theme', 'levelplus') . '/fonts' .$protocol. '/roboto-font.css', array('group' => CSS_THEME, 'type' => 'file' , 'preprocess' => FALSE));

}

/**
* Theme field
*/
function levelplus_field__field_mt_feature_font_awesome($variables) {
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<div class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . ':&nbsp;</div>';
  }

  // Render the items.
  $output .= '<div class="field-items"' . $variables['content_attributes'] . '>';
  foreach ($variables['items'] as $delta => $item) {
    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . '<i class="fa '. drupal_render($item) .'"></i>'. '</div>';
  }
  $output .= '</div>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';

  return $output;
}

function levelplus_page_alter($page) {

  $mobileoptimized = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' =>  'MobileOptimized',
      'content' =>  'width'
    )
  );
  $handheldfriendly = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' =>  'HandheldFriendly',
      'content' =>  'true'
    )
  );
  $viewport = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' =>  'viewport',
      'content' =>  'width=device-width, initial-scale=1'
    )
  );
  drupal_add_html_head($mobileoptimized, 'MobileOptimized');
  drupal_add_html_head($handheldfriendly, 'HandheldFriendly');
  drupal_add_html_head($viewport, 'viewport');

}

function levelplus_form_alter(&$form, &$form_state, $form_id) {

  if ($form_id == 'search_block_form') {
    unset($form['search_block_form']['#title']);
    $form['search_block_form']['#title_display'] = 'invisible';
    $form_default = t('Search...');
    $form['search_block_form']['#default_value'] = $form_default;
    $form['actions']['submit']['#attributes']['value'][] = '';
    $form['search_block_form']['#attributes'] = array('onblur' => "if (this.value == '') {this.value = '{$form_default}';}", 'onfocus' => "if (this.value == '{$form_default}') {this.value = '';}" );
  }

}

?>