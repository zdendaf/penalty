<?php if (!$label_hidden) : ?>
<div class="field-label"<?php print $title_attributes; ?>><?php print $label ?>:&nbsp;</div>
<?php endif; ?>

<?php
// Reduce number of images in teaser view mode to single image
if ($element['#view_mode'] == 'teaser') : ?>
<div class="field-item field-type-image teaser-overlayed even"<?php print $item_attributes[0]; ?>><?php print render($items[0]); ?></div>
<?php return; endif; ?>

<?php
  $node=$element['#object'];
  $images = field_get_items('node', $node, 'field_image');
?>

<?php if (count($images)>1) { ?>
<div class="images-container clearfix">
  <!-- #in-page-image-slider -->
  <div id="in-page-images-slider" class="flexslider">
    <ul class="slides">
      <?php foreach ($images as $key=>$file) {
        $image = field_view_value('node', $node, 'field_image', $images[$key], array('type' => 'image','settings' => array('image_style' => 'large',)));
        $image_file = file_load($images[$key]['fid']);
        $image_uri = $image_file->uri;
        ?>
        <li>
          <div class="image-container clearfix">
            <a class="image-popup overlayed" href="<?php print file_create_url($image_uri); ?>" title="<?php print $images[$key]['title']; ?>">
              <?php print render($image); ?>
              <span class="overlay">
                <i class="fa fa-plus"></i>
              </span>
            </a>
          </div>
          <?php if ($images[$key]['title']): ?>
            <div class="image-caption clearfix">
              <p><?php print $images[$key]['title']; ?></p>
            </div>
          <?php endif; ?>
        </li>
      <?php } ?>
    </ul>
  </div>
  <!-- EOF:#in-page-image-slider -->
</div>
<?php } else {
  $first_image = field_view_value('node', $node, 'field_image', $images[0], array('type' => 'image','settings' => array('image_style' => 'large',)));
  $first_image_file = file_load($images[0]['fid']);
  $first_image_uri = $first_image_file->uri;
?>
<div class="images-container clearfix one-value">
  <div class="image-preview clearfix">
    <a class="image-popup single overlayed" href="<?php print file_create_url($first_image_uri); ?>" title="<?php print $images[0]['title']; ?>">
      <?php print render($first_image); ?>
      <span class="overlay large">
        <i class="fa fa-plus"></i>
      </span>
    </a>
  </div>
  <?php if ($images[0]['title']): ?>
    <div class="image-caption clearfix">
      <p><?php print $images[0]['title']; ?></p>
    </div>
  <?php endif; ?>
</div>
<?php } ?>

<?php
drupal_add_js(drupal_get_path('theme', 'levelplus') .'/js/magnific-popup/jquery.magnific-popup.js', array('preprocess' => false));
drupal_add_css(drupal_get_path('theme', 'levelplus') . '/js/magnific-popup/magnific-popup.css');
$in_page_images_slideshow_effect = theme_get_setting('in_page_images_slideshow_effect');

drupal_add_js('
  jQuery(document).ready(function($) {
    $(window).load(function() {
      if ($("#in-page-images-slider.flexslider").length>0) {
        $("#in-page-images-slider.flexslider").fadeIn("slow");
        $("#in-page-images-slider.flexslider").flexslider({
          animation: "'.$in_page_images_slideshow_effect.'", // Select your animation type, "fade" or "slide"
          prevText: "",
          nextText: "",
          pauseOnAction: false,
          useCSS: false,
          slideshow: false,
        });
      };
      $("li:not(.clone) .image-popup").magnificPopup({
        type:"image",
        removalDelay: 300,
        mainClass: "mfp-fade",
        gallery: {
          enabled: true, // set to true to enable gallery
        }
      });
      $(".image-popup.single").magnificPopup({
        type:"image",
        removalDelay: 300,
        mainClass: "mfp-fade",
        gallery: {
          enabled: true, // set to true to enable gallery
        }
      });
    });
  });',array('type' => 'inline', 'scope' => 'footer', 'weight' => 3)
);
?>
