<?php if (!$label_hidden): ?>
  <div class="field-label"<?php print $title_attributes; ?>><?php print $label ?>:&nbsp;</div>
<?php endif; ?>

<?php
// Reduce number of images in teaser view mode to single image
if ($element['#view_mode'] == 'teaser'): ?>
  <div class="field-item field-type-image teaser-overlayed even"<?php print $item_attributes[0]; ?>><?php print render($items[0]); ?></div>
<?php return; endif; ?>

<?php
  $node=$element['#object'];
  $images = field_get_items('field_collection_item', $node, 'field_mt_feature_image');
  $first_image = field_view_value('field_collection_item', $node, 'field_mt_feature_image', $images[0], array('type' => 'image','settings' => array('image_style' => 'icon_style',)));
?>

<div class="field field-name-field-mt-feature-image field-type-image field-label-hidden">
  <?php print render($first_image); ?>
</div>

<?php
drupal_add_js('
  jQuery(document).ready(function($) {
    $(window).load(function() {
      $(".image-popup-feature").magnificPopup({
        type:"image",
        removalDelay: 300,
        mainClass: "mfp-fade",
        gallery: {
          enabled: true, // set to true to enable gallery
        }
      });
    });
  });',array('type' => 'inline', 'scope' => 'footer', 'weight' => 3)
);
?>
