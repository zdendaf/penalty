<?php
/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @param $form
 *   The form.
 * @param $form_state
 *   The form state.
 */
function levelplus_form_system_theme_settings_alter(&$form, &$form_state) {

  $form['mtt_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('MtT Theme Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['mtt_settings']['tabs'] = array(
    '#type' => 'vertical_tabs',
    '#attached' => array(
      'css' => array(drupal_get_path('theme', 'levelplus') . '/levelplus.settings.form.css'),
    ),
  );

  $form['mtt_settings']['tabs']['basic_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Basic Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['basic_settings']['breadcrumb'] = array(
    '#type' => 'item',
    '#markup' => t('<div class="theme-settings-title">Breadcrumb</div>'),
  );

  $form['mtt_settings']['tabs']['basic_settings']['breadcrumb_display'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show breadcrumb'),
    '#description'   => t('Use the checkbox to enable or disable Breadcrumb.'),
    '#default_value' => theme_get_setting('breadcrumb_display', 'levelplus'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['basic_settings']['breadcrumb_separator'] = array(
    '#type' => 'textfield',
    '#title' => t('Breadcrumb separator'),
    '#description'   => t('Enter the class of the icon you want from the Font Awesome library e.g.: fa-angle-right. A list of the available classes is provided here: <a href="http://fortawesome.github.io/Font-Awesome/cheatsheet" target="_blank">http://fortawesome.github.io/Font-Awesome/cheatsheet</a>.'),
    '#default_value' => theme_get_setting('breadcrumb_separator', 'levelplus'),
    '#size'          => 5,
    '#maxlength'     => 100,
  );

  $form['mtt_settings']['tabs']['basic_settings']['scrolltop'] = array(
    '#type' => 'item',
    '#markup' => t('<div class="theme-settings-title">Scroll to top</div>'),
  );

  $form['mtt_settings']['tabs']['basic_settings']['scrolltop_display'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show scroll-to-top button'),
    '#description'   => t('Use the checkbox to enable or disable scroll-to-top button.'),
    '#default_value' => theme_get_setting('scrolltop_display', 'levelplus'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['basic_settings']['frontpage_content'] = array(
    '#type' => 'item',
    '#markup' => t('<div class="theme-settings-title">Front Page Behavior</div>'),
  );

  $form['mtt_settings']['tabs']['basic_settings']['frontpage_content_print'] = array(
    '#type' => 'checkbox',
    '#title' => t('Drupal frontpage content'),
    '#description'   => t('Use the checkbox to enable or disable the Drupal default frontpage functionality. Enable this to have all the promoted content displayed in the frontpage.'),
    '#default_value' => theme_get_setting('frontpage_content_print', 'levelplus'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['basic_settings']['bootstrap_js'] = array(
    '#type' => 'item',
    '#markup' => t('<div class="theme-settings-title">Bootstrap 3 Framework Javascript file</div>'),
  );

  $form['mtt_settings']['tabs']['basic_settings']['bootstrap_js_include'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include Bootstrap 3 Framework Javascript file'),
    '#description'   => t('Use the checkbox to enable or disable bootstrap.min.js file.'),
    '#default_value' => theme_get_setting('bootstrap_js_include', 'levelplus'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['logos'] = array(
    '#type' => 'fieldset',
    '#title' => t('Logos'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['logos']['alternative_logo'] = array(
    '#type' => 'fieldset',
    '#title' => t('Alternative Main logo'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['logos']['alternative_logo']['alternative_logo_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to custom logo'),
    '#default_value' => theme_get_setting('alternative_logo_path', 'levelplus'),
    '#description' => t('The path to the file you would like to use as your logo file instead of the alternative main logo.')
  );

  $form['mtt_settings']['tabs']['logos']['alternative_logo']['alternative_logo_fid'] = array(
    '#title' => t('Alternative Main logo'),
    '#description' => t('Overrides the main logo uploaded and set by the default widget of the theme'),
    '#type' => 'managed_file',
    '#upload_location' => file_default_scheme() . '://logos/main/',
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg svg'),
    ),
    '#default_value' => theme_get_setting('alternative_logo_fid', 'levelplus'),
  );

  $form['mtt_settings']['tabs']['logos']['alternative_logo_inner'] = array(
    '#type' => 'fieldset',
    '#title' => t('Inner pages Logo'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['logos']['alternative_logo_inner']['alternative_logo_inner_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to custom logo'),
    '#default_value' => theme_get_setting('alternative_logo_inner_path', 'levelplus'),
    '#description' => t('The path to the file you would like to use as your logo file instead of the inner pages logo.')
  );

  $form['mtt_settings']['tabs']['logos']['alternative_logo_inner']['alternative_logo_inner_fid'] = array(
    '#title' => t('Inner pages Logo'),
    '#description' => t('This logo, when set, is used for all the inner pages of the site. In this case the default (or or the alternative main logo) will be used only for the home page.'),
    '#type' => 'managed_file',
    '#upload_location' => file_default_scheme() . '://logos/inner/',
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg svg'),
    ),
    '#default_value' => theme_get_setting('alternative_logo_inner_fid', 'levelplus'),
  );

  $form['#submit'][] = 'levelplus_settings_form_submit';

  // Get all themes.
  $themes = list_themes();
  // Get the current theme
  $active_theme = $GLOBALS['theme_key'];
  $form_state['build_info']['files'][] = str_replace("/$active_theme.info", '', $themes[$active_theme]->filename) . '/theme-settings.php';

  $form['mtt_settings']['tabs']['looknfeel'] = array(
    '#type' => 'fieldset',
    '#title' => t('Look\'n\'Feel'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['looknfeel']['color_scheme'] = array(
    '#type' => 'select',
    '#title' => t('Color Schemes'),
    '#description'   => t('From the drop-down menu, select the color scheme you prefer.'),
    '#default_value' => theme_get_setting('color_scheme', 'levelplus'),
    '#options' => array(
      'default' => t('Turqoise (Default)'),
      'blue' => t('Blue'),
      'green' => t('Green'),
      'red' => t('Red'),
      'pink' => t('Pink'),
      'purple' => t('Purple'),
      'gray' => t('Gray'),
      'orange' => t('Orange'),
      'yellow' => t('Yellow'),
      'night-blue' => t('Night Blue'),
    ),
  );

  $form['mtt_settings']['tabs']['looknfeel']['form_style'] = array(
    '#type' => 'select',
    '#title' => t('Form styles of contact page'),
    '#description'   => t('From the drop-down menu, select the form style that you prefer.'),
    '#default_value' => theme_get_setting('form_style', 'levelplus'),
    '#options' => array(
      'form-style-1' => t('Style-1 (default)'),
      'form-style-2' => t('Style-2'),
    ),
  );

  $form['mtt_settings']['tabs']['regions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Region settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['regions']['animations_state'] = array(
    '#type' => 'checkbox',
    '#title' => t('Animations'),
    '#description'   => t('Use the checkbox to enable or disable animations.'),
    '#description'   => t('Enable or disable animations globally. You can further adjust this for individual regions below.'),
    '#default_value' => theme_get_setting('animations_state', 'levelplus'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['regions']['header'] = array(
    '#type' => 'fieldset',
    '#title' => t('Header'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['regions']['header']['header_layout_container'] = array(
    '#type' => 'select',
    '#title' => t('Layout Mode'),
    '#description'   => t('From the drop-down menu, select the layout mode you prefer.'),
    '#default_value' => theme_get_setting('header_layout_container', 'levelplus'),
    '#options' => array(
      'container-fluid' => t('Full Width'),
      'container' => t('Fixed Width'),
    ),
  );

  $form['mtt_settings']['tabs']['regions']['header']['fixed_header'] = array(
    '#type' => 'checkbox',
    '#title' => t('Fixed Header'),
    '#description'   => t('Use the checkbox to apply fixed position to the header.'),
    '#default_value' => theme_get_setting('fixed_header', 'levelplus'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['regions']['header']['transparent_header_state'] = array(
    '#type' => 'checkbox',
    '#title' => t('Transparent Header'),
    '#description'   => t('Use the checkbox to enable or disable transparent header.'),
    '#default_value' => theme_get_setting('transparent_header_state', 'levelplus'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['regions']['header']['transparent_header_opacity'] = array(
    '#type' => 'textfield',
    '#title' => t('Transparent Header Background Opacity'),
    '#description'   => t('Set the % opacity for the background of the transparent header.'),
    '#default_value' => theme_get_setting('transparent_header_opacity', 'levelplus'),
  );

  $form['mtt_settings']['tabs']['regions']['content_top'] = array(
      '#type' => 'fieldset',
      '#title' => t('Content Top'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['regions']['content_top']['content_top_layout_container'] = array(
    '#type' => 'select',
    '#title' => t('Layout Mode'),
    '#description'   => t('From the drop-down menu, select the layout mode you prefer.'),
    '#default_value' => theme_get_setting('content_top_layout_container', 'levelplus'),
    '#options' => array(
      'container-fluid' => t('Full Width'),
      'container' => t('Fixed Width'),
    ),
  );

  $form['mtt_settings']['tabs']['regions']['content_top']['content_top_background_color'] = array(
    '#type' => 'select',
    '#title' => t('Background Color'),
    '#description'   => t('From the drop-down menu, select the background color you prefer.'),
    '#default_value' => theme_get_setting('content_top_background_color', 'levelplus'),
    '#options' => array(
      'white-region' => t('White'),
      'light-gray-region' => t('Light Gray'),
      'colored-region' => t('Colored'),
      'colored-region dark' => t('Dark'),
    ),
  );

  $form['mtt_settings']['tabs']['regions']['content_top']['content_top_animation_effect'] = array(
    '#type' => 'select',
    '#title' => t('Animation Effect'),
    '#description'   => t('From the drop-down menu, select the animation effect you prefer.'),
    '#default_value' => theme_get_setting('content_top_animation_effect', 'levelplus'),
    '#options' => array(
      'no-animation' => t('None'),
      'fadeIn' => t('Fade In'),
      'fadeInDown' => t('Fade In Down'),
      'fadeInUp' => t('Fade In Up'),
      'fadeInLeft' => t('Fade In Left'),
      'fadeInRight' => t('Fade In Right'),
    ),
  );

  $form['mtt_settings']['tabs']['regions']['main_content'] = array(
      '#type' => 'fieldset',
      '#title' => t('Main Content'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['regions']['main_content']['main_content_animation_effect'] = array(
    '#type' => 'select',
    '#title' => t('Animation Effect'),
    '#description'   => t('From the drop-down menu, select the animation effect you prefer.'),
    '#default_value' => theme_get_setting('main_content_animation_effect', 'levelplus'),
    '#options' => array(
      'no-animation' => t('None'),
      'fadeIn' => t('Fade In'),
      'fadeInDown' => t('Fade In Down'),
      'fadeInUp' => t('Fade In Up'),
      'fadeInLeft' => t('Fade In Left'),
      'fadeInRight' => t('Fade In Right'),
    ),
  );

  $form['mtt_settings']['tabs']['regions']['content_bottom'] = array(
      '#type' => 'fieldset',
      '#title' => t('Content Bottom'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['regions']['content_bottom']['content_bottom_background_color'] = array(
    '#type' => 'select',
    '#title' => t('Background Color'),
    '#description'   => t('From the drop-down menu, select the background color you prefer.'),
    '#default_value' => theme_get_setting('content_bottom_background_color', 'levelplus'),
    '#options' => array(
      'white-region' => t('White'),
      'light-gray-region' => t('Light Gray'),
      'colored-region' => t('Colored'),
      'colored-region dark' => t('Dark'),
    ),
  );

  $form['mtt_settings']['tabs']['regions']['content_bottom']['content_bottom_animation_effect'] = array(
    '#type' => 'select',
    '#title' => t('Animation Effect'),
    '#description'   => t('From the drop-down menu, select the animation effect you prefer.'),
    '#default_value' => theme_get_setting('content_bottom_animation_effect', 'levelplus'),
    '#options' => array(
      'no-animation' => t('None'),
      'fadeIn' => t('Fade In'),
      'fadeInDown' => t('Fade In Down'),
      'fadeInUp' => t('Fade In Up'),
      'fadeInLeft' => t('Fade In Left'),
      'fadeInRight' => t('Fade In Right'),
    ),
  );

  $form['mtt_settings']['tabs']['regions']['featured_top'] = array(
      '#type' => 'fieldset',
      '#title' => t('Featured Top'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['regions']['featured_top']['featured_top_layout_container'] = array(
    '#type' => 'select',
    '#title' => t('Layout Mode'),
    '#description'   => t('From the drop-down menu, select the layout mode you prefer.'),
    '#default_value' => theme_get_setting('featured_top_layout_container', 'levelplus'),
    '#options' => array(
      'container-fluid' => t('Full Width'),
      'container' => t('Fixed Width'),
    ),
  );

  $form['mtt_settings']['tabs']['regions']['featured_top']['featured_top_background_color'] = array(
    '#type' => 'select',
    '#title' => t('Background Color'),
    '#description'   => t('From the drop-down menu, select the background color you prefer.'),
    '#default_value' => theme_get_setting('featured_top_background_color', 'levelplus'),
    '#options' => array(
      'white-region' => t('White'),
      'light-gray-region' => t('Light Gray'),
      'colored-region' => t('Colored'),
      'colored-region dark' => t('Dark'),
    ),
  );

  $form['mtt_settings']['tabs']['regions']['featured_top']['featured_top_animation_effect'] = array(
    '#type' => 'select',
    '#title' => t('Animation Effect'),
    '#description'   => t('From the drop-down menu, select the animation effect you prefer.'),
    '#default_value' => theme_get_setting('featured_top_animation_effect', 'levelplus'),
    '#options' => array(
      'no-animation' => t('None'),
      'fadeIn' => t('Fade In'),
      'fadeInDown' => t('Fade In Down'),
      'fadeInUp' => t('Fade In Up'),
      'fadeInLeft' => t('Fade In Left'),
      'fadeInRight' => t('Fade In Right'),
    ),
  );

  $form['mtt_settings']['tabs']['regions']['featured'] = array(
      '#type' => 'fieldset',
      '#title' => t('Featured'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['regions']['featured']['featured_layout_container'] = array(
    '#type' => 'select',
    '#title' => t('Layout Mode'),
    '#description'   => t('From the drop-down menu, select the layout mode you prefer.'),
    '#default_value' => theme_get_setting('featured_layout_container', 'levelplus'),
    '#options' => array(
      'container-fluid' => t('Full Width'),
      'container' => t('Fixed Width'),
    ),
  );

  $form['mtt_settings']['tabs']['regions']['featured']['featured_background_color'] = array(
    '#type' => 'select',
    '#title' => t('Background Color'),
    '#description'   => t('From the drop-down menu, select the background color you prefer.'),
    '#default_value' => theme_get_setting('featured_background_color', 'levelplus'),
    '#options' => array(
      'white-region' => t('White'),
      'light-gray-region' => t('Light Gray'),
      'colored-region' => t('Colored'),
      'colored-region dark' => t('Dark'),
    ),
  );

  $form['mtt_settings']['tabs']['regions']['featured']['featured_animation_effect'] = array(
    '#type' => 'select',
    '#title' => t('Animation Effect'),
    '#description'   => t('From the drop-down menu, select the animation effect you prefer.'),
    '#default_value' => theme_get_setting('featured_animation_effect', 'levelplus'),
    '#options' => array(
      'no-animation' => t('None'),
      'fadeIn' => t('Fade In'),
      'fadeInDown' => t('Fade In Down'),
      'fadeInUp' => t('Fade In Up'),
      'fadeInLeft' => t('Fade In Left'),
      'fadeInRight' => t('Fade In Right'),
    ),
  );

  $form['mtt_settings']['tabs']['regions']['featured_bottom'] = array(
      '#type' => 'fieldset',
      '#title' => t('Featured Bottom'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['regions']['featured_bottom']['featured_bottom_layout_container'] = array(
    '#type' => 'select',
    '#title' => t('Layout Mode'),
    '#description'   => t('From the drop-down menu, select the layout mode you prefer.'),
    '#default_value' => theme_get_setting('featured_bottom_layout_container', 'levelplus'),
    '#options' => array(
      'container-fluid' => t('Full Width'),
      'container' => t('Fixed Width'),
    ),
  );

  $form['mtt_settings']['tabs']['regions']['featured_bottom']['featured_bottom_background_color'] = array(
    '#type' => 'select',
    '#title' => t('Background Color'),
    '#description'   => t('From the drop-down menu, select the background color you prefer.'),
    '#default_value' => theme_get_setting('featured_bottom_background_color', 'levelplus'),
    '#options' => array(
      'white-region' => t('White'),
      'light-gray-region' => t('Light Gray'),
      'colored-region' => t('Colored'),
      'colored-region dark' => t('Dark'),
    ),
  );

  $form['mtt_settings']['tabs']['regions']['featured_bottom']['featured_bottom_animation_effect'] = array(
    '#type' => 'select',
    '#title' => t('Animation Effect'),
    '#description'   => t('From the drop-down menu, select the animation effect you prefer.'),
    '#default_value' => theme_get_setting('featured_bottom_animation_effect', 'levelplus'),
    '#options' => array(
      'no-animation' => t('None'),
      'fadeIn' => t('Fade In'),
      'fadeInDown' => t('Fade In Down'),
      'fadeInUp' => t('Fade In Up'),
      'fadeInLeft' => t('Fade In Left'),
      'fadeInRight' => t('Fade In Right'),
    ),
  );

  $form['mtt_settings']['tabs']['regions']['highlighted'] = array(
    '#type' => 'fieldset',
    '#title' => t('Highlighted'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['regions']['highlighted']['highlighted_layout_container'] = array(
    '#type' => 'select',
    '#title' => t('Layout Mode'),
    '#description'   => t('From the drop-down menu, select the layout mode you prefer.'),
    '#default_value' => theme_get_setting('highlighted_layout_container', 'levelplus'),
    '#options' => array(
      'container-fluid' => t('Full Width'),
      'container' => t('Fixed Width'),
    ),
  );

  $form['mtt_settings']['tabs']['regions']['highlighted']['highlighted_background_color'] = array(
    '#type' => 'select',
    '#title' => t('Background Color'),
    '#description'   => t('From the drop-down menu, select the background color you prefer.'),
    '#default_value' => theme_get_setting('highlighted_background_color', 'levelplus'),
    '#options' => array(
      'white-region' => t('White'),
      'light-gray-region' => t('Light Gray'),
      'colored-region' => t('Colored'),
      'colored-region dark' => t('Dark'),
    ),
  );

  $form['mtt_settings']['tabs']['regions']['highlighted']['highlighted_animation_effect'] = array(
    '#type' => 'select',
    '#title' => t('Animation Effect'),
    '#description'   => t('From the drop-down menu, select the animation effect you prefer.'),
    '#default_value' => theme_get_setting('highlighted_animation_effect', 'levelplus'),
    '#options' => array(
      'no-animation' => t('None'),
      'fadeIn' => t('Fade In'),
      'fadeInDown' => t('Fade In Down'),
      'fadeInUp' => t('Fade In Up'),
      'fadeInLeft' => t('Fade In Left'),
      'fadeInRight' => t('Fade In Right'),
    ),
  );

  $form['mtt_settings']['tabs']['regions']['highlighted_bottom'] = array(
      '#type' => 'fieldset',
      '#title' => t('Highlighted Bottom'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['regions']['highlighted_bottom']['highlighted_bottom_layout_container'] = array(
    '#type' => 'select',
    '#title' => t('Layout Mode'),
    '#description'   => t('From the drop-down menu, select the layout mode you prefer.'),
    '#default_value' => theme_get_setting('highlighted_bottom_layout_container', 'levelplus'),
    '#options' => array(
      'container-fluid' => t('Full Width'),
      'container' => t('Fixed Width'),
    ),
  );

  $form['mtt_settings']['tabs']['regions']['highlighted_bottom']['highlighted_bottom_background_color'] = array(
    '#type' => 'select',
    '#title' => t('Background Color'),
    '#description'   => t('From the drop-down menu, select the background color you prefer.'),
    '#default_value' => theme_get_setting('highlighted_bottom_background_color', 'levelplus'),
    '#options' => array(
      'white-region' => t('White'),
      'light-gray-region' => t('Light Gray'),
      'colored-region' => t('Colored'),
      'colored-region dark' => t('Dark'),
    ),
  );

  $form['mtt_settings']['tabs']['regions']['highlighted_bottom']['highlighted_bottom_animation_effect'] = array(
    '#type' => 'select',
    '#title' => t('Animation Effect'),
    '#description'   => t('From the drop-down menu, select the animation effect you prefer.'),
    '#default_value' => theme_get_setting('highlighted_bottom_animation_effect', 'levelplus'),
    '#options' => array(
      'no-animation' => t('None'),
      'fadeIn' => t('Fade In'),
      'fadeInDown' => t('Fade In Down'),
      'fadeInUp' => t('Fade In Up'),
      'fadeInLeft' => t('Fade In Left'),
      'fadeInRight' => t('Fade In Right'),
    ),
  );

  $form['mtt_settings']['tabs']['regions']['footer_top'] = array(
      '#type' => 'fieldset',
      '#title' => t('Footer Top'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['regions']['footer_top']['footer_top_background_color'] = array(
    '#type' => 'select',
    '#title' => t('Background Color'),
    '#description'   => t('From the drop-down menu, select the background color you prefer.'),
    '#default_value' => theme_get_setting('footer_top_background_color', 'levelplus'),
    '#options' => array(
      'white-region' => t('White'),
      'light-gray-region' => t('Light Gray'),
      'colored-region' => t('Colored'),
      'colored-region dark' => t('Dark'),
      'bicolor' => t('Bicolor'),
    ),
  );

  $form['mtt_settings']['tabs']['regions']['footer_top']['footer_top_animation_effect'] = array(
    '#type' => 'select',
    '#title' => t('Animation Effect'),
    '#description'   => t('From the drop-down menu, select the animation effect you prefer.'),
    '#default_value' => theme_get_setting('footer_top_animation_effect', 'levelplus'),
    '#options' => array(
      'no-animation' => t('None'),
      'fadeIn' => t('Fade In'),
      'fadeInDown' => t('Fade In Down'),
      'fadeInUp' => t('Fade In Up'),
      'fadeInLeft' => t('Fade In Left'),
      'fadeInRight' => t('Fade In Right'),
    ),
  );

  $form['mtt_settings']['tabs']['regions']['footer'] = array(
      '#type' => 'fieldset',
      '#title' => t('Footer'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['regions']['footer']['footer_layout_container'] = array(
    '#type' => 'select',
    '#title' => t('Layout Mode'),
    '#description'   => t('From the drop-down menu, select the layout mode you prefer.'),
    '#default_value' => theme_get_setting('footer_layout_container', 'levelplus'),
    '#options' => array(
      'container-fluid' => t('Full Width'),
      'container' => t('Fixed Width'),
    ),
  );

  $form['mtt_settings']['tabs']['regions']['footer']['footer_background_color'] = array(
    '#type' => 'select',
    '#title' => t('Background Color'),
    '#description'   => t('From the drop-down menu, select the background color you prefer.'),
    '#default_value' => theme_get_setting('footer_background_color', 'levelplus'),
    '#options' => array(
      'white-region' => t('White'),
      'light-gray-region' => t('Light Gray'),
      'colored-region' => t('Colored'),
      'colored-region dark' => t('Dark'),
    ),
  );

  $form['mtt_settings']['tabs']['regions']['footer']['footer_animation_effect'] = array(
    '#type' => 'select',
    '#title' => t('Animation Effect'),
    '#description'   => t('From the drop-down menu, select the animation effect you prefer.'),
    '#default_value' => theme_get_setting('footer_animation_effect', 'levelplus'),
    '#options' => array(
      'no-animation' => t('None'),
      'fadeIn' => t('Fade In'),
      'fadeInDown' => t('Fade In Down'),
      'fadeInUp' => t('Fade In Up'),
      'fadeInLeft' => t('Fade In Left'),
      'fadeInRight' => t('Fade In Right'),
    ),
  );

  $form['mtt_settings']['tabs']['regions']['subfooter'] = array(
      '#type' => 'fieldset',
      '#title' => t('Subfooter'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['regions']['subfooter']['subfooter_layout_container'] = array(
    '#type' => 'select',
    '#title' => t('Layout Mode'),
    '#description'   => t('From the drop-down menu, select the layout mode you prefer.'),
    '#default_value' => theme_get_setting('subfooter_layout_container', 'levelplus'),
    '#options' => array(
      'container-fluid' => t('Full Width'),
      'container' => t('Fixed Width'),
    ),
  );

  $form['mtt_settings']['tabs']['regions']['subfooter']['subfooter_background_color'] = array(
    '#type' => 'select',
    '#title' => t('Background Color'),
    '#description'   => t('From the drop-down menu, select the background color you prefer.'),
    '#default_value' => theme_get_setting('subfooter_background_color', 'levelplus'),
    '#options' => array(
      'white-region' => t('White'),
      'light-gray-region' => t('Light Gray'),
      'colored-region' => t('Colored'),
      'colored-region dark' => t('Dark'),
    ),
  );

  $form['mtt_settings']['tabs']['post'] = array(
    '#type' => 'fieldset',
    '#title' => t('Post'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['post']['reading_time'] = array(
    '#type' => 'checkbox',
    '#title' => t('Time to read'),
    '#description'   => t('Use the checkbox to enable or disable the "Time to read" indicator.'),
    '#default_value' => theme_get_setting('reading_time', 'levelplus'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['post']['post_progress'] = array(
    '#type' => 'checkbox',
    '#title' => t('Read so far'),
    '#description'   => t('Use the checkbox to enable or disable the reading progress indicator.'),
    '#default_value' => theme_get_setting('post_progress', 'levelplus'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['post']['affix'] = array(
    '#type' => 'fieldset',
    '#title' => t('Affix configuration'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description'   => t('If you add or remove blocks from the header please change the corresponding values bellow to make the affix implementation work as expected.'),
  );

  $form['mtt_settings']['tabs']['post']['affix']['affix_admin_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Admin toolbar height (px)'),
    '#default_value' => theme_get_setting('affix_admin_height', 'levelplus'),
    '#description'   => t('The height of the admin toolbar in pixels'),
  );

  $form['mtt_settings']['tabs']['post']['affix']['affix_fixedHeader_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Fixed header height (px)'),
    '#default_value' => theme_get_setting('affix_fixedHeader_height', 'levelplus'),
    '#description'   => t('The height of the header when fixed at the top of the window in pixels'),
  );

  $form['mtt_settings']['tabs']['layout_modes'] = array(
    '#type' => 'fieldset',
    '#title' => t('Theme Layout'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['layout_modes']['layout_mode'] = array(
    '#type' => 'select',
    '#title' => t('Global Layout Mode'),
    '#description'   => t('From the drop-down menu, select the layout mode you prefer. This global setting overrides the individual region-specific layout settings.'),
    '#default_value' => theme_get_setting('layout_mode', 'levelplus'),
    '#options' => array(
      'wide' => t('Wide'),
      'boxed' => t('Boxed'),
    ),
  );

  $form['mtt_settings']['tabs']['font'] = array(
    '#type' => 'fieldset',
    '#title' => t('Font Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['font']['font_title'] = array(
    '#type' => 'item',
    '#markup' => 'For every region pick the <strong>font-family</strong> that corresponds to your needs.',
  );

  $form['mtt_settings']['tabs']['font']['sitename_font_family'] = array(
    '#type' => 'select',
    '#title' => t('Site name'),
    '#default_value' => theme_get_setting('sitename_font_family', 'levelplus'),
    '#options' => array(
      'sff-1' => t('Merriweather, Georgia, Times, Serif'),
      'sff-2' => t('Source Sans Pro, Helvetica Neue, Arial, Sans-serif'),
      'sff-3' => t('Ubuntu, Helvetica Neue, Arial, Sans-serif'),
      'sff-4' => t('PT Sans, Helvetica Neue, Arial, Sans-serif'),
      'sff-5' => t('Roboto, Helvetica Neue, Arial, Sans-serif'),
      'sff-6' => t('Open Sans, Helvetica Neue, Arial, Sans-serif'),
      'sff-7' => t('Lato, Helvetica Neue, Arial, Sans-serif'),
      'sff-8' => t('Roboto Condensed, Arial Narrow, Arial, Sans-serif'),
      'sff-9' => t('Exo, Arial, Helvetica Neue, Sans-serif'),
      'sff-10' => t('Roboto Slab, Trebuchet MS, Sans-serif'),
      'sff-11' => t('Raleway, Helvetica Neue, Arial, Sans-serif'),
      'sff-12' => t('Josefin Sans, Georgia, Times, Serif'),
      'sff-13' => t('Georgia, Times, Serif'),
      'sff-14' => t('Playfair Display, Times, Serif'),
      'sff-15' => t('Philosopher, Georgia, Times, Serif'),
      'sff-16' => t('Cinzel, Georgia, Times, Serif'),
      'sff-17' => t('Oswald, Helvetica Neue, Arial, Sans-serif'),
      'sff-18' => t('Playfair Display SC, Georgia, Times, Serif'),
      'sff-19' => t('Cabin, Helvetica Neue, Arial, Sans-serif'),
      'sff-20' => t('Noto Sans, Arial, Helvetica Neue, Sans-serif'),
      'sff-21' => t('Helvetica Neue, Arial, Sans-serif'),
      'sff-22' => t('Droid Serif, Georgia, Times, Times New Roman, Serif'),
      'sff-23' => t('PT Serif, Georgia, Times, Times New Roman, Serif'),
      'sff-24' => t('Vollkorn, Georgia, Times, Times New Roman, Serif'),
      'sff-25' => t('Alegreya, Georgia, Times, Times New Roman, Serif'),
      'sff-26' => t('Noto Serif, Georgia, Times, Times New Roman, Serif'),
      'sff-27' => t('Crimson Text, Georgia, Times, Times New Roman, Serif'),
      'sff-28' => t('Gentium Book Basic, Georgia, Times, Times New Roman, Serif'),
      'sff-29' => t('Volkhov, Georgia, Times, Times New Roman, Serif'),
      'sff-30' => t('Times, Times New Roman, Serif'),
      'sff-31' => t('Alegreya SC, Georgia, Times, Times New Roman, Serif'),
      'sff-32' => t('Montserrat SC, Arial, Helvetica Neue, Sans-serif'),
      'sff-33' => t('Fira Sans, Arial, Helvetica Neue, Sans-serif'),
      'sff-34' => t('Lora, Georgia, Times, Times New Roman, Serif'),
      'sff-35' => t('Quattrocento Sans, Arial, Helvetica Neue, Sans-serif'),
      'sff-36' => t('Julius Sans One, Arial, Helvetica Neue, Sans-serif'),
      'sff-37' => t('Work Sans, Arial, Helvetica Neue, Sans-serif'),
      'sff-38' => t('Open Sans Condensed, Arial, Helvetica Neue, Sans-serif'),
      'sff-39' => t('PT Sans Narrow, Arial, Helvetica Neue, Sans-serif'),
      'sff-40' => t('Archivo Narrow, Arial, Helvetica Neue, Sans-serif'),
      'sff-41' => t('Ubuntu Condensed, Arial, Helvetica Neue, Sans-serif'),
      'sff-42' => t('Arimo, Arial, Helvetica Neue, Sans-serif'),
    ),
  );

  $form['mtt_settings']['tabs']['font']['slogan_font_family'] = array(
    '#type' => 'select',
    '#title' => t('Slogan'),
    '#default_value' => theme_get_setting('slogan_font_family', 'levelplus'),
    '#options' => array(
      'slff-1' => t('Merriweather, Georgia, Times, Serif'),
      'slff-2' => t('Source Sans Pro, Helvetica Neue, Arial, Sans-serif'),
      'slff-3' => t('Ubuntu, Helvetica Neue, Arial, Sans-serif'),
      'slff-4' => t('PT Sans, Helvetica Neue, Arial, Sans-serif'),
      'slff-5' => t('Roboto, Helvetica Neue, Arial, Sans-serif'),
      'slff-6' => t('Open Sans, Helvetica Neue, Arial, Sans-serif'),
      'slff-7' => t('Lato, Helvetica Neue, Arial, Sans-serif'),
      'slff-8' => t('Roboto Condensed, Arial Narrow, Arial, Sans-serif'),
      'slff-9' => t('Exo, Arial, Helvetica Neue, Sans-serif'),
      'slff-10' => t('Roboto Slab, Trebuchet MS, Sans-serif'),
      'slff-11' => t('Raleway, Helvetica Neue, Arial, Sans-serif'),
      'slff-12' => t('Josefin Sans, Georgia, Times, Serif'),
      'slff-13' => t('Georgia, Times, Serif'),
      'slff-14' => t('Playfair Display, Times, Serif'),
      'slff-15' => t('Philosopher, Georgia, Times, Serif'),
      'slff-16' => t('Cinzel, Georgia, Times, Serif'),
      'slff-17' => t('Oswald, Helvetica Neue, Arial, Sans-serif'),
      'slff-18' => t('Playfair Display SC, Georgia, Times, Serif'),
      'slff-19' => t('Cabin, Helvetica Neue, Arial, Sans-serif'),
      'slff-20' => t('Noto Sans, Arial, Helvetica Neue, Sans-serif'),
      'slff-21' => t('Helvetica Neue, Arial, Sans-serif'),
      'slff-22' => t('Droid Serif, Georgia, Times, Times New Roman, Serif'),
      'slff-23' => t('PT Serif, Georgia, Times, Times New Roman, Serif'),
      'slff-24' => t('Vollkorn, Georgia, Times, Times New Roman, Serif'),
      'slff-25' => t('Alegreya, Georgia, Times, Times New Roman, Serif'),
      'slff-26' => t('Noto Serif, Georgia, Times, Times New Roman, Serif'),
      'slff-27' => t('Crimson Text, Georgia, Times, Times New Roman, Serif'),
      'slff-28' => t('Gentium Book Basic, Georgia, Times, Times New Roman, Serif'),
      'slff-29' => t('Volkhov, Georgia, Times, Times New Roman, Serif'),
      'slff-30' => t('Times, Times New Roman, Serif'),
      'slff-31' => t('Alegreya SC, Georgia, Times, Times New Roman, Serif'),
      'slff-32' => t('Montserrat SC, Arial, Helvetica Neue, Sans-serif'),
      'slff-33' => t('Fira Sans, Arial, Helvetica Neue, Sans-serif'),
      'slff-34' => t('Lora, Georgia, Times, Times New Roman, Serif'),
      'slff-35' => t('Quattrocento Sans, Arial, Helvetica Neue, Sans-serif'),
      'slff-36' => t('Julius Sans One, Arial, Helvetica Neue, Sans-serif'),
      'slff-37' => t('Work Sans, Arial, Helvetica Neue, Sans-serif'),
      'slff-38' => t('Open Sans Condensed, Arial, Helvetica Neue, Sans-serif'),
      'slff-39' => t('PT Sans Narrow, Arial, Helvetica Neue, Sans-serif'),
      'slff-40' => t('Archivo Narrow, Arial, Helvetica Neue, Sans-serif'),
      'slff-41' => t('Ubuntu Condensed, Arial, Helvetica Neue, Sans-serif'),
      'slff-42' => t('Arimo, Arial, Helvetica Neue, Sans-serif'),
    ),
  );

  $form['mtt_settings']['tabs']['font']['headings_font_family'] = array(
    '#type' => 'select',
    '#title' => t('Headings'),
    '#default_value' => theme_get_setting('headings_font_family', 'levelplus'),
    '#options' => array(
      'hff-1' => t('Merriweather, Georgia, Times, Serif'),
      'hff-2' => t('Source Sans Pro, Helvetica Neue, Arial, Sans-serif'),
      'hff-3' => t('Ubuntu, Helvetica Neue, Arial, Sans-serif'),
      'hff-4' => t('PT Sans, Helvetica Neue, Arial, Sans-serif'),
      'hff-5' => t('Roboto, Helvetica Neue, Arial, Sans-serif'),
      'hff-6' => t('Open Sans, Helvetica Neue, Arial, Sans-serif'),
      'hff-7' => t('Lato, Helvetica Neue, Arial, Sans-serif'),
      'hff-8' => t('Roboto Condensed, Arial Narrow, Arial, Sans-serif'),
      'hff-9' => t('Exo, Arial, Helvetica Neue, Sans-serif'),
      'hff-10' => t('Roboto Slab, Trebuchet MS, Sans-serif'),
      'hff-11' => t('Raleway, Helvetica Neue, Arial, Sans-serif'),
      'hff-12' => t('Josefin Sans, Georgia, Times, Serif'),
      'hff-13' => t('Georgia, Times, Serif'),
      'hff-14' => t('Playfair Display, Times, Serif'),
      'hff-15' => t('Philosopher, Georgia, Times, Serif'),
      'hff-16' => t('Cinzel, Georgia, Times, Serif'),
      'hff-17' => t('Oswald, Helvetica Neue, Arial, Sans-serif'),
      'hff-18' => t('Playfair Display SC, Georgia, Times, Serif'),
      'hff-19' => t('Cabin, Helvetica Neue, Arial, Sans-serif'),
      'hff-20' => t('Noto Sans, Arial, Helvetica Neue, Sans-serif'),
      'hff-21' => t('Helvetica Neue, Arial, Sans-serif'),
      'hff-22' => t('Droid Serif, Georgia, Times, Times New Roman, Serif'),
      'hff-23' => t('PT Serif, Georgia, Times, Times New Roman, Serif'),
      'hff-24' => t('Vollkorn, Georgia, Times, Times New Roman, Serif'),
      'hff-25' => t('Alegreya, Georgia, Times, Times New Roman, Serif'),
      'hff-26' => t('Noto Serif, Georgia, Times, Times New Roman, Serif'),
      'hff-27' => t('Crimson Text, Georgia, Times, Times New Roman, Serif'),
      'hff-28' => t('Gentium Book Basic, Georgia, Times, Times New Roman, Serif'),
      'hff-29' => t('Volkhov, Georgia, Times, Times New Roman, Serif'),
      'hff-30' => t('Times, Times New Roman, Serif'),
      'hff-31' => t('Alegreya SC, Georgia, Times, Times New Roman, Serif'),
      'hff-32' => t('Montserrat SC, Arial, Helvetica Neue, Sans-serif'),
      'hff-33' => t('Fira Sans, Arial, Helvetica Neue, Sans-serif'),
      'hff-34' => t('Lora, Georgia, Times, Times New Roman, Serif'),
      'hff-35' => t('Quattrocento Sans, Arial, Helvetica Neue, Sans-serif'),
      'hff-36' => t('Julius Sans One, Arial, Helvetica Neue, Sans-serif'),
      'hff-37' => t('Work Sans, Arial, Helvetica Neue, Sans-serif'),
      'hff-38' => t('Open Sans Condensed, Arial, Helvetica Neue, Sans-serif'),
      'hff-39' => t('PT Sans Narrow, Arial, Helvetica Neue, Sans-serif'),
      'hff-40' => t('Archivo Narrow, Arial, Helvetica Neue, Sans-serif'),
      'hff-41' => t('Ubuntu Condensed, Arial, Helvetica Neue, Sans-serif'),
      'hff-42' => t('Arimo, Arial, Helvetica Neue, Sans-serif'),
    ),
  );

  $form['mtt_settings']['tabs']['font']['paragraph_font_family'] = array(
    '#type' => 'select',
    '#title' => t('Paragraph'),
    '#default_value' => theme_get_setting('paragraph_font_family', 'levelplus'),
    '#options' => array(
      'pff-1' => t('Merriweather, Georgia, Times, Serif'),
      'pff-2' => t('Source Sans Pro, Helvetica Neue, Arial, Sans-serif'),
      'pff-3' => t('Ubuntu, Helvetica Neue, Arial, Sans-serif'),
      'pff-4' => t('PT Sans, Helvetica Neue, Arial, Sans-serif'),
      'pff-5' => t('Roboto, Helvetica Neue, Arial, Sans-serif'),
      'pff-6' => t('Open Sans, Helvetica Neue, Arial, Sans-serif'),
      'pff-7' => t('Lato, Helvetica Neue, Arial, Sans-serif'),
      'pff-8' => t('Roboto Condensed, Arial Narrow, Arial, Sans-serif'),
      'pff-9' => t('Exo, Arial, Helvetica Neue, Sans-serif'),
      'pff-10' => t('Roboto Slab, Trebuchet MS, Sans-serif'),
      'pff-11' => t('Raleway, Helvetica Neue, Arial, Sans-serif'),
      'pff-12' => t('Josefin Sans, Georgia, Times, Serif'),
      'pff-13' => t('Georgia, Times, Serif'),
      'pff-14' => t('Playfair Display, Times, Serif'),
      'pff-15' => t('Philosopher, Georgia, Times, Serif'),
      'pff-16' => t('Oswald, Helvetica Neue, Arial, Sans-serif'),
      'pff-17' => t('Playfair Display SC, Georgia, Times, Serif'),
      'pff-18' => t('Cabin, Helvetica Neue, Arial, Sans-serif'),
      'pff-19' => t('Noto Sans, Arial, Helvetica Neue, Sans-serif'),
      'pff-20' => t('Helvetica Neue, Arial, Sans-serif'),
      'pff-21' => t('Droid Serif, Georgia, Times, Times New Roman, Serif'),
      'pff-22' => t('PT Serif, Georgia, Times, Times New Roman, Serif'),
      'pff-23' => t('Vollkorn, Georgia, Times, Times New Roman, Serif'),
      'pff-24' => t('Alegreya, Georgia, Times, Times New Roman, Serif'),
      'pff-25' => t('Noto Serif, Georgia, Times, Times New Roman, Serif'),
      'pff-26' => t('Crimson Text, Georgia, Times, Times New Roman, Serif'),
      'pff-27' => t('Gentium Book Basic, Georgia, Times, Times New Roman, Serif'),
      'pff-28' => t('Volkhov, Georgia, Times, Times New Roman, Serif'),
      'pff-29' => t('Times, Times New Roman, Serif'),
      'pff-30' => t('Fira Sans, Arial, Helvetica Neue, Sans-serif'),
      'pff-31' => t('Lora, Georgia, Times, Times New Roman, Serif'),
      'pff-32' => t('Quattrocento Sans, Arial, Helvetica Neue, Sans-serif'),
      'pff-33' => t('Work Sans, Arial, Helvetica Neue, Sans-serif'),
      'pff-34' => t('Open Sans Condensed, Arial, Helvetica Neue, Sans-serif'),
      'pff-35' => t('PT Sans Narrow, Arial, Helvetica Neue, Sans-serif'),
      'pff-36' => t('Archivo Narrow, Arial, Helvetica Neue, Sans-serif'),
      'pff-37' => t('Ubuntu Condensed, Arial, Helvetica Neue, Sans-serif'),
      'pff-38' => t('Arimo, Arial, Helvetica Neue, Sans-serif'),
    ),
  );

  $form['mtt_settings']['tabs']['slideshow'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sliders'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['slideshow']['revolution_slider_full'] = array(
    '#type' => 'fieldset',
    '#title' => t('Full Screen (Slider Revolution)'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['slideshow']['revolution_slider_full']['rs_slideshow_full_effect'] = array(
    '#type' => 'select',
    '#title' => t('Effects'),
    '#description'   => t('From the drop-down menu, select the slideshow effect you prefer.'),
    '#default_value' => theme_get_setting('rs_slideshow_full_effect', 'levelplus'),
    '#options' => array(
      'fade' => t('Fade'),
      'slideup' => t('Slide To Top'),
      'slidedown' => t('Slide To Bottom'),
      'slideright' => t('Slide To Right'),
      'slideleft' => t('Slide To Left'),
      'slidehorizontal' => t('Slide Horizontal'),
      'slidevertical' => t('Slide Vertical'),
      'boxslide' => t('Slide Boxes'),
      'slotslide-horizontal' => t('Slide Slots Horizontal'),
      'slotslide-vertical' => t('Slide Slots Vertical'),
      'boxfade' => t('Fade Boxes'),
      'slotfade-horizontal' => t('Fade Slots Horizontal'),
      'slotfade-vertical' => t('Fade Slots Vertical'),
      'fadefromright' => t('Fade and Slide from Right'),
      'fadefromleft' => t('Fade and Slide from Left'),
      'fadefromtop' => t('Fade and Slide from Top'),
      'fadefrombottom' => t('Fade and Slide from Bottom'),
      'fadetoleftfadefromright' => t('Fade To Left and Fade From Right'),
      'fadetorightfadefromleft' => t('Fade To Right and Fade From Left'),
      'fadetotopfadefrombottom' => t('Fade To Top and Fade From Bottom'),
      'fadetobottomfadefromtop' => t('Fade To Bottom and Fade From Top'),
      'parallaxtoright' => t('Parallax to Right'),
      'parallaxtoleft' => t('Parallax to Left'),
      'parallaxtotop' => t('Parallax to Top'),
      'parallaxtobottom' => t('Parallax to Bottom'),
      'scaledownfromright' => t('Zoom Out and Fade From Right'),
      'scaledownfromleft' => t('Zoom Out and Fade From Left'),
      'scaledownfromtop' => t('Zoom Out and Fade From Top'),
      'scaledownfrombottom' => t('Zoom Out and Fade From Bottom'),
      'zoomout' => t('ZoomOut'),
      'zoomin' => t('ZoomIn'),
      'slotzoom-horizontal' => t('Zoom Slots Horizontal'),
      'slotzoom-vertical' => t('Zoom Slots Vertical'),
      'curtain-1' => t('Curtain from Left'),
      'curtain-2' => t('Curtain from Right'),
      'curtain-3' => t('Curtain from Middle'),
      '3dcurtain-horizontal' => t('3D Curtain Horizontal'),
      '3dcurtain-vertical' => t('3D Curtain Vertical'),
      'cube' => t('Cube Vertical'),
      'cube-horizontal' => t('Cube Horizontal'),
      'incube' => t('In Cube Vertical'),
      'incube-horizontal' => t('In Cube Horizontal'),
      'turnoff' => t('TurnOff Horizontal'),
      'turnoff-vertical' => t('TurnOff Vertical'),
      'papercut' => t('Paper Cut'),
      'flyin' => t('Fly In'),
      'random-static' => t('Random Flat'),
      'random-premium' => t('Random Premium'),
      'random' => t('Random Flat and Premium/Default'),
    ),
  );

  $form['mtt_settings']['tabs']['slideshow']['revolution_slider_full']['rs_slideshow_full_effect_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Effect duration (sec)'),
    '#default_value' => theme_get_setting('rs_slideshow_full_effect_time', 'levelplus'),
    '#description'   => t('Set the speed of animations, in seconds.'),
  );

  $form['mtt_settings']['tabs']['slideshow']['revolution_slider_full']['rs_slideshow_full_bullets_position'] = array(
    '#type' => 'select',
    '#title' => t('Navigation bullets position'),
    '#description'   => t('From the drop-down menu, select the position you prefer.'),
    '#default_value' => theme_get_setting('rs_slideshow_full_bullets_position', 'levelplus'),
    '#options' => array(
      'left' => t('Left'),
      'center' => t('Center'),
      'right' => t('Right'),
    ),
  );

  $form['mtt_settings']['tabs']['slideshow']['revolution_slider_fullwidth'] = array(
    '#type' => 'fieldset',
    '#title' => t('Full Width (Slider Revolution)'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['slideshow']['revolution_slider_fullwidth']['rs_slideshow_fullwidth_effect'] = array(
    '#type' => 'select',
    '#title' => t('Effects'),
    '#description'   => t('From the drop-down menu, select the slideshow effect you prefer.'),
    '#default_value' => theme_get_setting('rs_slideshow_fullwidth_effect', 'levelplus'),
    '#options' => array(
      'fade' => t('Fade'),
      'slideup' => t('Slide To Top'),
      'slidedown' => t('Slide To Bottom'),
      'slideright' => t('Slide To Right'),
      'slideleft' => t('Slide To Left'),
      'slidehorizontal' => t('Slide Horizontal'),
      'slidevertical' => t('Slide Vertical'),
      'boxslide' => t('Slide Boxes'),
      'slotslide-horizontal' => t('Slide Slots Horizontal'),
      'slotslide-vertical' => t('Slide Slots Vertical'),
      'boxfade' => t('Fade Boxes'),
      'slotfade-horizontal' => t('Fade Slots Horizontal'),
      'slotfade-vertical' => t('Fade Slots Vertical'),
      'fadefromright' => t('Fade and Slide from Right'),
      'fadefromleft' => t('Fade and Slide from Left'),
      'fadefromtop' => t('Fade and Slide from Top'),
      'fadefrombottom' => t('Fade and Slide from Bottom'),
      'fadetoleftfadefromright' => t('Fade To Left and Fade From Right'),
      'fadetorightfadefromleft' => t('Fade To Right and Fade From Left'),
      'fadetotopfadefrombottom' => t('Fade To Top and Fade From Bottom'),
      'fadetobottomfadefromtop' => t('Fade To Bottom and Fade From Top'),
      'parallaxtoright' => t('Parallax to Right'),
      'parallaxtoleft' => t('Parallax to Left'),
      'parallaxtotop' => t('Parallax to Top'),
      'parallaxtobottom' => t('Parallax to Bottom'),
      'scaledownfromright' => t('Zoom Out and Fade From Right'),
      'scaledownfromleft' => t('Zoom Out and Fade From Left'),
      'scaledownfromtop' => t('Zoom Out and Fade From Top'),
      'scaledownfrombottom' => t('Zoom Out and Fade From Bottom'),
      'zoomout' => t('ZoomOut'),
      'zoomin' => t('ZoomIn'),
      'slotzoom-horizontal' => t('Zoom Slots Horizontal'),
      'slotzoom-vertical' => t('Zoom Slots Vertical'),
      'curtain-1' => t('Curtain from Left'),
      'curtain-2' => t('Curtain from Right'),
      'curtain-3' => t('Curtain from Middle'),
      '3dcurtain-horizontal' => t('3D Curtain Horizontal'),
      '3dcurtain-vertical' => t('3D Curtain Vertical'),
      'cube' => t('Cube Vertical'),
      'cube-horizontal' => t('Cube Horizontal'),
      'incube' => t('In Cube Vertical'),
      'incube-horizontal' => t('In Cube Horizontal'),
      'turnoff' => t('TurnOff Horizontal'),
      'turnoff-vertical' => t('TurnOff Vertical'),
      'papercut' => t('Paper Cut'),
      'flyin' => t('Fly In'),
      'random-static' => t('Random Flat'),
      'random-premium' => t('Random Premium'),
      'random' => t('Random Flat and Premium/Default'),
    ),
  );

  $form['mtt_settings']['tabs']['slideshow']['revolution_slider_fullwidth']['rs_slideshow_fullwidth_effect_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Effect duration (sec)'),
    '#default_value' => theme_get_setting('rs_slideshow_fullwidth_effect_time', 'levelplus'),
    '#description'   => t('Set the speed of animations, in seconds.'),
  );

  $form['mtt_settings']['tabs']['slideshow']['revolution_slider_fullwidth']['rs_slideshow_fullwidth_initial_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Initial Height (px)'),
    '#default_value' => theme_get_setting('rs_slideshow_fullwidth_initial_height', 'levelplus'),
    '#description'   => t('Set the initial height, in pixels.'),
  );

  $form['mtt_settings']['tabs']['slideshow']['revolution_slider_fullwidth']['rs_slideshow_fullwidth_bullets_position'] = array(
    '#type' => 'select',
    '#title' => t('Navigation bullets position'),
    '#description'   => t('From the drop-down menu, select the position you prefer.'),
    '#default_value' => theme_get_setting('rs_slideshow_fullwidth_bullets_position', 'levelplus'),
    '#options' => array(
      'left' => t('Left'),
      'center' => t('Center'),
      'right' => t('Right'),
    ),
  );

  $form['mtt_settings']['tabs']['slideshow']['revolution_slider_boxed'] = array(
    '#type' => 'fieldset',
    '#title' => t('Boxed Width (Slider Revolution)'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['slideshow']['revolution_slider_boxed']['rs_slideshow_boxed_effect'] = array(
    '#type' => 'select',
    '#title' => t('Effects'),
    '#description'   => t('From the drop-down menu, select the slideshow effect you prefer.'),
    '#default_value' => theme_get_setting('rs_slideshow_boxed_effect', 'levelplus'),
    '#options' => array(
    'fade' => t('Fade'),
      'slideup' => t('Slide To Top'),
      'slidedown' => t('Slide To Bottom'),
      'slideright' => t('Slide To Right'),
      'slideleft' => t('Slide To Left'),
      'slidehorizontal' => t('Slide Horizontal'),
      'slidevertical' => t('Slide Vertical'),
      'boxslide' => t('Slide Boxes'),
      'slotslide-horizontal' => t('Slide Slots Horizontal'),
      'slotslide-vertical' => t('Slide Slots Vertical'),
      'boxfade' => t('Fade Boxes'),
      'slotfade-horizontal' => t('Fade Slots Horizontal'),
      'slotfade-vertical' => t('Fade Slots Vertical'),
      'fadefromright' => t('Fade and Slide from Right'),
      'fadefromleft' => t('Fade and Slide from Left'),
      'fadefromtop' => t('Fade and Slide from Top'),
      'fadefrombottom' => t('Fade and Slide from Bottom'),
      'fadetoleftfadefromright' => t('Fade To Left and Fade From Right'),
      'fadetorightfadefromleft' => t('Fade To Right and Fade From Left'),
      'fadetotopfadefrombottom' => t('Fade To Top and Fade From Bottom'),
      'fadetobottomfadefromtop' => t('Fade To Bottom and Fade From Top'),
      'parallaxtoright' => t('Parallax to Right'),
      'parallaxtoleft' => t('Parallax to Left'),
      'parallaxtotop' => t('Parallax to Top'),
      'parallaxtobottom' => t('Parallax to Bottom'),
      'scaledownfromright' => t('Zoom Out and Fade From Right'),
      'scaledownfromleft' => t('Zoom Out and Fade From Left'),
      'scaledownfromtop' => t('Zoom Out and Fade From Top'),
      'scaledownfrombottom' => t('Zoom Out and Fade From Bottom'),
      'zoomout' => t('ZoomOut'),
      'zoomin' => t('ZoomIn'),
      'slotzoom-horizontal' => t('Zoom Slots Horizontal'),
      'slotzoom-vertical' => t('Zoom Slots Vertical'),
      'curtain-1' => t('Curtain from Left'),
      'curtain-2' => t('Curtain from Right'),
      'curtain-3' => t('Curtain from Middle'),
      '3dcurtain-horizontal' => t('3D Curtain Horizontal'),
      '3dcurtain-vertical' => t('3D Curtain Vertical'),
      'cube' => t('Cube Vertical'),
      'cube-horizontal' => t('Cube Horizontal'),
      'incube' => t('In Cube Vertical'),
      'incube-horizontal' => t('In Cube Horizontal'),
      'turnoff' => t('TurnOff Horizontal'),
      'turnoff-vertical' => t('TurnOff Vertical'),
      'papercut' => t('Paper Cut'),
      'flyin' => t('Fly In'),
      'random-static' => t('Random Flat'),
      'random-premium' => t('Random Premium'),
      'random' => t('Random Flat and Premium/Default'),
    ),
  );

  $form['mtt_settings']['tabs']['slideshow']['revolution_slider_boxed']['rs_slideshow_boxed_effect_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Effect duration (sec)'),
    '#default_value' => theme_get_setting('rs_slideshow_boxed_effect_time', 'levelplus'),
    '#description'   => t('Set the speed of animations, in seconds.'),
  );

  $form['mtt_settings']['tabs']['slideshow']['revolution_slider_boxed']['rs_slideshow_boxed_initial_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Initial Height (px)'),
    '#default_value' => theme_get_setting('rs_slideshow_boxed_initial_height', 'levelplus'),
    '#description'   => t('Set the initial height, in pixels.'),
  );

  $form['mtt_settings']['tabs']['slideshow']['revolution_slider_boxed']['rs_slideshow_boxed_bullets_position'] = array(
    '#type' => 'select',
    '#title' => t('Navigation bullets position'),
    '#description'   => t('From the drop-down menu, select the position you prefer.'),
    '#default_value' => theme_get_setting('rs_slideshow_boxed_bullets_position', 'levelplus'),
    '#options' => array(
      'left' => t('Left'),
      'center' => t('Center'),
      'right' => t('Right'),
    ),
  );

  $form['mtt_settings']['tabs']['slideshow']['revolution_slider_internal'] = array(
    '#type' => 'fieldset',
    '#title' => t('Internal Banner (Slider Revolution)'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['slideshow']['revolution_slider_internal']['rs_slideshow_internal_effect'] = array(
    '#type' => 'select',
    '#title' => t('Effects'),
    '#description'   => t('From the drop-down menu, select the slideshow effect you prefer.'),
    '#default_value' => theme_get_setting('rs_slideshow_internal_effect', 'levelplus'),
    '#options' => array(
      'fade' => t('Fade'),
      'slideup' => t('Slide To Top'),
      'slidedown' => t('Slide To Bottom'),
      'slideright' => t('Slide To Right'),
      'slideleft' => t('Slide To Left'),
      'slidehorizontal' => t('Slide Horizontal'),
      'slidevertical' => t('Slide Vertical'),
      'boxslide' => t('Slide Boxes'),
      'slotslide-horizontal' => t('Slide Slots Horizontal'),
      'slotslide-vertical' => t('Slide Slots Vertical'),
      'boxfade' => t('Fade Boxes'),
      'slotfade-horizontal' => t('Fade Slots Horizontal'),
      'slotfade-vertical' => t('Fade Slots Vertical'),
      'fadefromright' => t('Fade and Slide from Right'),
      'fadefromleft' => t('Fade and Slide from Left'),
      'fadefromtop' => t('Fade and Slide from Top'),
      'fadefrombottom' => t('Fade and Slide from Bottom'),
      'fadetoleftfadefromright' => t('Fade To Left and Fade From Right'),
      'fadetorightfadefromleft' => t('Fade To Right and Fade From Left'),
      'fadetotopfadefrombottom' => t('Fade To Top and Fade From Bottom'),
      'fadetobottomfadefromtop' => t('Fade To Bottom and Fade From Top'),
      'parallaxtoright' => t('Parallax to Right'),
      'parallaxtoleft' => t('Parallax to Left'),
      'parallaxtotop' => t('Parallax to Top'),
      'parallaxtobottom' => t('Parallax to Bottom'),
      'scaledownfromright' => t('Zoom Out and Fade From Right'),
      'scaledownfromleft' => t('Zoom Out and Fade From Left'),
      'scaledownfromtop' => t('Zoom Out and Fade From Top'),
      'scaledownfrombottom' => t('Zoom Out and Fade From Bottom'),
      'zoomout' => t('ZoomOut'),
      'zoomin' => t('ZoomIn'),
      'slotzoom-horizontal' => t('Zoom Slots Horizontal'),
      'slotzoom-vertical' => t('Zoom Slots Vertical'),
      'curtain-1' => t('Curtain from Left'),
      'curtain-2' => t('Curtain from Right'),
      'curtain-3' => t('Curtain from Middle'),
      '3dcurtain-horizontal' => t('3D Curtain Horizontal'),
      '3dcurtain-vertical' => t('3D Curtain Vertical'),
      'cube' => t('Cube Vertical'),
      'cube-horizontal' => t('Cube Horizontal'),
      'incube' => t('In Cube Vertical'),
      'incube-horizontal' => t('In Cube Horizontal'),
      'turnoff' => t('TurnOff Horizontal'),
      'turnoff-vertical' => t('TurnOff Vertical'),
      'papercut' => t('Paper Cut'),
      'flyin' => t('Fly In'),
      'random-static' => t('Random Flat'),
      'random-premium' => t('Random Premium'),
      'random' => t('Random Flat and Premium/Default'),
    ),
  );

  $form['mtt_settings']['tabs']['slideshow']['revolution_slider_internal']['rs_slideshow_internal_effect_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Effect duration (sec)'),
    '#default_value' => theme_get_setting('rs_slideshow_internal_effect_time', 'levelplus'),
    '#description'   => t('Set the speed of animations, in seconds.'),
  );

  $form['mtt_settings']['tabs']['slideshow']['revolution_slider_internal']['rs_slideshow_internal_initial_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Initial Height (px)'),
    '#default_value' => theme_get_setting('rs_slideshow_internal_initial_height', 'levelplus'),
    '#description'   => t('Set the initial height, in pixels.'),
  );

  $form['mtt_settings']['tabs']['slideshow']['revolution_slider_internal']['rs_slideshow_internal_bullets_position'] = array(
    '#type' => 'select',
    '#title' => t('Navigation bullets position'),
    '#description'   => t('From the drop-down menu, select the position you prefer.'),
    '#default_value' => theme_get_setting('rs_slideshow_internal_bullets_position', 'levelplus'),
    '#options' => array(
      'left' => t('Left'),
      'center' => t('Center'),
      'right' => t('Right'),
    ),
  );

  $form['mtt_settings']['tabs']['slideshow']['rs_slideshow_caption_opacity'] = array(
    '#type' => 'textfield',
    '#title' => t('Slider Revolution Caption Background Opacity'),
    '#description'   => t('Set the % opacity for the background of the caption.'),
    '#default_value' => theme_get_setting('rs_slideshow_caption_opacity', 'levelplus'),
  );

  $form['mtt_settings']['tabs']['slideshow']['testimonial_slideshow'] = array(
    '#type' => 'fieldset',
    '#title' => t('Testimonials Slider (Flexslider)'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['slideshow']['testimonial_slideshow']['testimonial_slideshow_effect'] = array(
    '#type' => 'select',
    '#title' => t('Effects'),
    '#description'   => t('From the drop-down menu, select the slideshow effect you prefer.'),
    '#default_value' => theme_get_setting('testimonial_slideshow_effect', 'levelplus'),
    '#options' => array(
    'fade' => t('fade'),
    'slide' => t('slide'),
    ),
  );

  $form['mtt_settings']['tabs']['slideshow']['testimonial_slideshow']['testimonial_slideshow_effect_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Effect duration (sec)'),
    '#default_value' => theme_get_setting('testimonial_slideshow_effect_time', 'levelplus'),
    '#description'   => t('Set the speed of animations, in seconds.'),
  );

  $form['mtt_settings']['tabs']['slideshow']['in_page_slider'] = array(
    '#type' => 'fieldset',
    '#title' => t('In Page Images Slider (Flexslider)'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['slideshow']['in_page_slider']['in_page_images_slideshow_effect'] = array(
    '#type' => 'select',
    '#title' => t('Effects'),
    '#description'   => t('From the drop-down menu, select the slideshow effect you prefer.'),
    '#default_value' => theme_get_setting('in_page_images_slideshow_effect', 'levelplus'),
    '#options' => array(
    'fade' => t('fade'),
    'slide' => t('slide'),
    ),
  );

  $form['mtt_settings']['tabs']['slideshow']['carousel']['owl_include'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include Owl Carousel css and js files'),
    '#default_value' => theme_get_setting('owl_include', 'levelplus'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['slideshow']['carousel']['owl_posts'] = array(
    '#type' => 'fieldset',
    '#title' => t('Featured Services and Products (Owl Carousel)'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['slideshow']['carousel']['owl_posts']['owl_autoplay'] = array(
    '#type' => 'checkbox',
    '#title' => t('Autoplay'),
    '#default_value' => theme_get_setting('owl_autoplay', 'levelplus'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['slideshow']['carousel']['owl_posts']['owl_effect_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Effect duration (sec)'),
    '#default_value' => theme_get_setting('owl_effect_time', 'levelplus'),
    '#description'   => t('Set the speed of animations, in seconds.'),
  );

  $form['mtt_settings']['tabs']['slideshow']['carousel']['owl_team'] = array(
    '#type' => 'fieldset',
    '#title' => t('Team Members (Owl Carousel)'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['slideshow']['carousel']['owl_team']['owl_team_autoplay'] = array(
    '#type' => 'checkbox',
    '#title' => t('Autoplay'),
    '#default_value' => theme_get_setting('owl_team_autoplay', 'levelplus'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['slideshow']['carousel']['owl_team']['owl_team_effect_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Effect duration (sec)'),
    '#default_value' => theme_get_setting('owl_team_effect_time', 'levelplus'),
    '#description'   => t('Set the speed of animations, in seconds.'),
  );

  $form['mtt_settings']['tabs']['slideshow']['carousel']['owl_testimonials'] = array(
    '#type' => 'fieldset',
    '#title' => t('Testimonials (Owl Carousel)'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['slideshow']['carousel']['owl_testimonials']['owl_testimonials_autoplay'] = array(
    '#type' => 'checkbox',
    '#title' => t('Autoplay'),
    '#default_value' => theme_get_setting('owl_testimonials_autoplay', 'levelplus'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['slideshow']['carousel']['owl_testimonials']['owl_testimonials_effect_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Effect duration (sec)'),
    '#default_value' => theme_get_setting('owl_testimonials_effect_time', 'levelplus'),
    '#description'   => t('Set the speed of animations, in seconds.'),
  );

  $form['mtt_settings']['tabs']['slideshow']['carousel']['owl_logos'] = array(
    '#type' => 'fieldset',
    '#title' => t('Companies (Owl Carousel)'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['slideshow']['carousel']['owl_logos']['owl_logos_autoplay'] = array(
    '#type' => 'checkbox',
    '#title' => t('Autoplay'),
    '#default_value' => theme_get_setting('owl_logos_autoplay', 'levelplus'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['slideshow']['carousel']['owl_logos']['owl_logos_effect_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Effect duration (sec)'),
    '#default_value' => theme_get_setting('owl_logos_effect_time', 'levelplus'),
    '#description'   => t('Set the speed of animations, in seconds.'),
  );

  $form['mtt_settings']['tabs']['responsive_menu'] = array(
    '#type' => 'fieldset',
    '#title' => t('Responsive menus'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['responsive_menu']['responsive_menu_state'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable responsive menu'),
    '#description'   => t('Use the checkbox to enable the plugin which transforms the Main menu of your site to a responsive multilevel menu when your browser is at mobile widths.'),
    '#default_value' => theme_get_setting('responsive_menu_state', 'levelplus'),
  );

  $form['mtt_settings']['tabs']['responsive_menu']['responsive_menu_header_top_switchwidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Header Top Region switch width (px)'),
    '#description'   => t('Set the width (in pixels) at which the menu of the <strong>Header Top Region</strong> will change to a mobile menu.'),
    '#default_value' => theme_get_setting('responsive_menu_header_top_switchwidth', 'levelplus'),
  );

  $form['mtt_settings']['tabs']['responsive_menu']['responsive_menu_header_first_switchwidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Header First Region switch width (px)'),
    '#description'   => t('Set the width (in pixels) at which the menu of the <strong>Header First Region</strong> will change to a mobile menu.'),
    '#default_value' => theme_get_setting('responsive_menu_header_first_switchwidth', 'levelplus'),
  );

  $form['mtt_settings']['tabs']['responsive_menu']['responsive_menu_header_third_switchwidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Header Third Region switch width (px)'),
    '#description'   => t('Set the width (in pixels) at which the menu of the <strong>Header Third Region</strong> will change to a mobile menu.'),
    '#default_value' => theme_get_setting('responsive_menu_header_third_switchwidth', 'levelplus'),
  );

  $form['mtt_settings']['tabs']['parallax_and_video_bg'] = array(
    '#type' => 'fieldset',
    '#title' => t('Parallax/Video Background region'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['parallax_and_video_bg']['highlighted_bottom'] = array(
    '#type' => 'fieldset',
    '#title' => t('Highlighted Bottom Region'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['mtt_settings']['tabs']['parallax_and_video_bg']['highlighted_bottom']['parallax_and_video_bg_state'] = array(
    '#type' => 'select',
    '#title' => t('Select Background effect'),
    '#default_value' => theme_get_setting('parallax_and_video_bg_state', 'levelplus'),
    '#options' => array(
      'none' => t('None'),
      'parallax' => t('Parallax Background Effect'),
      'video' => t('Video Background'),
    ),
  );

  $form['mtt_settings']['tabs']['parallax_and_video_bg']['highlighted_bottom']['parallax_and_video_bg_opacity'] = array(
    '#type' => 'textfield',
    '#title' => t('Parallax Background/Video Background Opacity'),
    '#description'   => t('Set the % opacity for the background of the parallax/video background region.'),
    '#default_value' => theme_get_setting('parallax_and_video_bg_opacity', 'levelplus'),
  );

  $form['mtt_settings']['tabs']['google_map'] = array(
    '#type' => 'fieldset',
    '#title' => t('Google Map'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['google_map']['google_map_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Maps API Key'),
    '#description'   => t('Google requires an API key to be included to all calls to Google Maps API. Please create an API key and populate the above field.'),
    '#default_value' => theme_get_setting('google_map_key','levelplus'),
    '#size'          => 50,
    '#maxlength'     => 50,
  );

  $form['mtt_settings']['tabs']['google_map']['google_map_js'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include Google Map javascript code'),
    '#default_value' => theme_get_setting('google_map_js', 'levelplus'),
  );

  $form['mtt_settings']['tabs']['google_map']['google_map_latitude'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Map Latitude'),
    '#description'   => t('For example 40.726576'),
    '#default_value' => theme_get_setting('google_map_latitude', 'levelplus'),
    '#size' => 5,
    '#maxlength' => 10,
  );

  $form['mtt_settings']['tabs']['google_map']['google_map_longitude'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Map Longitude'),
    '#description'   => t('For example -74.046822'),
    '#default_value' => theme_get_setting('google_map_longitude', 'levelplus'),
    '#size' => 5,
    '#maxlength' => 10,
  );

  $form['mtt_settings']['tabs']['google_map']['google_map_zoom'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Map Zoom'),
    '#description'   => t('For example 13'),
    '#default_value' => theme_get_setting('google_map_zoom', 'levelplus'),
    '#size' => 5,
    '#maxlength' => 10,
  );

  $form['mtt_settings']['tabs']['google_map']['google_map_canvas'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Map Canvas Id'),
    '#description'   => t('Set the Google Map Canvas Id. For example: map-canvas'),
    '#default_value' => theme_get_setting('google_map_canvas', 'levelplus'),
  );

  $form['mtt_settings']['tabs']['google_map']['google_map_show'] = array(
    '#type' => 'textfield',
    '#title' => t('Show Map text'),
    '#description'   => t('Define the text for showing the map to be displayed to the visitor. The default is: Show Map'),
    '#default_value' => theme_get_setting('google_map_show','levelplus'),
  );

  $form['mtt_settings']['tabs']['google_map']['google_map_hide'] = array(
    '#type' => 'textfield',
    '#title' => t('Hide Map text'),
    '#description'   => t('Define the text for hiding the map to be displayed to the visitor. The default is: Hide Map'),
    '#default_value' => theme_get_setting('google_map_hide','levelplus'),
  );

  $form['mtt_settings']['tabs']['isotope'] = array(
    '#type' => 'fieldset',
    '#title' => t('Isotope'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['isotope']['isotope_js'] = array(
    '#type' => 'item',
    '#markup' => t('<div class="theme-settings-title">Isotope Javascript file</div>'),
  );

  $form['mtt_settings']['tabs']['isotope']['isotope_js_include'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include Isotope Javascript file'),
    '#description'   => t('Use the checkbox to enable or disable jquery.isotope.js file.'),
    '#default_value' => theme_get_setting('isotope_js_include', 'levelplus'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['isotope']['grid_layout'] = array(
    '#type' => 'fieldset',
    '#title' => t('Grid Layout'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mtt_settings']['tabs']['isotope']['grid_layout']['isotope_layout'] = array(
    '#type' => 'select',
    '#title' => t('Layout modes'),
    '#description'   => t('From the drop-down menu, select the layout mode you prefer.'),
    '#default_value' => theme_get_setting('isotope_layout', 'levelplus'),
    '#options' => array(
    'masonry' => t('masonry'),
    'fitRows' => t('fitRows'),
    'vertical' => t('vertical'),
    ),
  );

}

function levelplus_settings_form_submit(&$form, $form_state) {
  $alternative_logo_fid = $form_state['values']['alternative_logo_fid'];
  $alternative_logo = file_load($alternative_logo_fid);
  if (is_object($alternative_logo)) {
    // Check to make sure that the file is set to be permanent.
    if ($alternative_logo->status == 0) {
      // Update the status.
      $alternative_logo->status = FILE_STATUS_PERMANENT;
      // Save the update.
      file_save($alternative_logo);
      // Add a reference to prevent warnings.
      file_usage_add($alternative_logo, 'levelplus', 'theme', 1);
    }
  }
  $alternative_logo_inner_fid = $form_state['values']['alternative_logo_inner_fid'];
  $alternative_logo_inner = file_load($alternative_logo_inner_fid);
  if (is_object($alternative_logo_inner)) {
    // Check to make sure that the file is set to be permanent.
    if ($alternative_logo_inner->status == 0) {
      // Update the status.
      $alternative_logo_inner->status = FILE_STATUS_PERMANENT;
      // Save the update.
      file_save($alternative_logo_inner);
      // Add a reference to prevent warnings.
      file_usage_add($alternative_logo_inner, 'levelplus', 'theme', 1);
    }
  }
}