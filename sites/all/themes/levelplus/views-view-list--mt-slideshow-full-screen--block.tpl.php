<div id="slideshow-fullscreen" class="fullscreenbanner-container main-slider">
  <?php $rs_effect=theme_get_setting('rs_slideshow_full_effect'); ?>
  <div class="fullscreenbanner">
    <ul>
      <?php foreach ($rows as $id => $row) {
        $nodeType = $view->render_field('type', $id);
        $image = $view->render_field('field_mt_slideshow_image', $id);
        $title = $view->render_field('title', $id);
        if ($nodeType=='mt_slideshow_entry') {
          $path = $view->render_field('field_mt_slideshow_path', $id); ?>
          <li data-transition="<?php print $rs_effect ?>" data-masterspeed="800" data-title="<?php print strip_tags($title); ?>">
            <?php print $image; ?>
            <div class="tp-caption transparent-bg"
              data-x="center"
              data-y="bottom"
              data-speed="800"
              data-start="0">
            </div>
            <div class="tp-caption title sfb fadeout"
              data-x="left"
              data-y="center"
              data-speed="500"
              data-start="1200"
              data-hoffset="60"
              data-voffset="0"
              data-easing="easeOutQuad">
              <?php
              $title_suffix="";
              if (strlen($title)>70): $title_suffix="..."; endif;
              $title = substr($title,0, 70);
              ?>
              <?php if ($path) { ?>
                <a href="<?php print url($path); ?>"><?php print strip_tags($title) . $title_suffix; ?></a>
              <?php } else { ?>
                <?php print strip_tags($title) . $title_suffix; ?>
              <?php } ?>
            </div>
            <?php if ($path): ?>
              <div class="tp-caption sfb fadeout"
                data-x="center"
                data-y="bottom"
                data-speed="500"
                data-start="1500"
                data-voffset="-30"
                data-easing="easeOutQuad">
                <a href="<?php print url($path); ?>" class="more"><?php print t('Read More'); ?></a>
              </div>
            <?php endif; ?>
            <div class="tp-caption scroll-button smooth-scroll fade fadeout"
              data-x="center"
              data-y="bottom"
              data-speed="800"
              data-start="0">
              <a href="#page-start"></a>
            </div>
          </li>
        <?php } else { ?>
          <li data-transition="<?php print $rs_effect ?>" data-masterspeed="800" data-title="<?php print strip_tags($title); ?>">
            <?php print $image; ?>
            <?php print $row; ?>
          </li>
        <?php } ?>
      <?php } ?>
    </ul>
    <div class="tp-bannertimer tp-bottom"></div>
  </div>
</div>
<?php
$rs_slideshow_full_effect_time = (int) theme_get_setting('rs_slideshow_full_effect_time')*1000;
$rs_slideshow_caption_opacity = (int) theme_get_setting('rs_slideshow_caption_opacity')/100;
$rs_slideshow_full_bullets_position = theme_get_setting('rs_slideshow_full_bullets_position');
drupal_add_js('
  var tpj=jQuery;
  tpj.noConflict();
  tpj(document).ready(function($) {

    if (tpj.fn.cssOriginal!=undefined)
        tpj.fn.css = tpj.fn.cssOriginal;

    if ($(".transparent-header-active").length>0) {
      var api = tpj("#slideshow-fullscreen .fullscreenbanner").revolution({
        delay:"'.$rs_slideshow_full_effect_time.'",
        startheight:540,
        startwidth: 1140,
        fullScreen: "on",
        onHoverStop: "off",
        navigationHAlign: "'.$rs_slideshow_full_bullets_position.'",
        navigationVAlign: "bottom",
        navigationVOffset: 20,
        navigationStyle: "preview2"
      });
    } else {
      var api = tpj("#slideshow-fullscreen .fullscreenbanner").revolution({
        delay:"'.$rs_slideshow_full_effect_time.'",
        fullScreenOffsetContainer: "#header-container",
        startheight:540,
        startwidth: 1140,
        fullScreen: "on",
        onHoverStop: "off",
        navigationHAlign: "'.$rs_slideshow_full_bullets_position.'",
        navigationVAlign: "bottom",
        navigationVOffset: 20,
        navigationStyle: "preview2"
      });
    };

    api.bind("revolution.slide.onloaded",function (e,data) {
      jQuery(".tparrows.default").css("display", "block");
      jQuery(".tp-bullets").css("display", "block");
      jQuery(".tp-bannertimer").css("display", "block");
    });

    $(".transparent-bg").css("backgroundColor", "rgba(0,0,0,'.$rs_slideshow_caption_opacity.')");

  });',
  array('type' => 'inline', 'scope' => 'header')
);
?>
