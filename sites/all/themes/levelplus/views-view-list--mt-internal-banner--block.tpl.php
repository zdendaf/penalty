<div id="slideshow-internal" class="fullwidthbanner-container internal-main-slider">
  <?php $rs_internal_effect=theme_get_setting('rs_slideshow_internal_effect'); ?>
  <div class="fullwidthbanner">
    <ul>
      <?php foreach ($rows as $id => $row) {
        $image = $view->render_field('field_mt_banner_image', $id);
        $image_title = $view->result[$id]->field_data_field_mt_banner_image_field_mt_banner_image_title;
        ?>
        <li data-transition="<?php print $rs_internal_effect ?>" data-masterspeed="800" data-title="<?php print $image_title; ?>">
          <?php print $image; ?>
          <?php if ($image_title) { ?>
            <div class="tp-caption transparent-bg"
              data-x="center"
              data-y="bottom"
              data-speed="800"
              data-start="0">
            </div>
            <div class="tp-caption title sfb fadeout"
              data-x="center"
              data-y="center"
              data-speed="500"
              data-start="1200"
              data-hoffset="0"
              data-voffset="0"
              data-easing="easeOutQuad">
              <?php print $image_title; ?>
            </div>
          <?php } ?>
        </li>
      <?php } ?>
    </ul>
    <div class="tp-bannertimer tp-bottom"></div>
  </div>
</div>
<?php
$rs_slideshow_internal_effect_time = (int) theme_get_setting('rs_slideshow_internal_effect_time')*1000;
$rs_slideshow_caption_opacity = (int) theme_get_setting('rs_slideshow_caption_opacity')/100;
$rs_slideshow_internal_initial_height = (int) theme_get_setting('rs_slideshow_internal_initial_height');
$rs_slideshow_internal_bullets_position = theme_get_setting('rs_slideshow_internal_bullets_position');
drupal_add_js('
  var tpj=jQuery;
  tpj.noConflict();

  tpj(document).ready(function($) {

    if (tpj.fn.cssOriginal!=undefined)
      tpj.fn.css = tpj.fn.cssOriginal;

    var api = tpj("#slideshow-internal .fullwidthbanner").revolution({
      delay:"'.$rs_slideshow_internal_effect_time.'",
      startheight: '.$rs_slideshow_internal_initial_height.',
      startwidth: 1140,
      onHoverStop: "off",
      navigationHAlign: "'.$rs_slideshow_internal_bullets_position.'",
      navigationVAlign: "bottom",
      navigationVOffset: 20,
      navigationStyle: "preview2"
    });

    api.bind("revolution.slide.onloaded",function (e,data) {
      jQuery(".tparrows.default").css("display", "block");
      jQuery(".tp-bullets").css("display", "block");
      jQuery(".tp-bannertimer").css("display", "block");
    });

    $(".transparent-bg").css("backgroundColor", "rgba(0,0,0,'.$rs_slideshow_caption_opacity.')");

  });', array('type' => 'inline', 'scope' => 'header')
);
?>
