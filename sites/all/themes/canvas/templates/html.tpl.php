<!DOCTYPE html>
<html<?php print $html_attributes;?><?php print $rdf_namespaces;?>>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="profile" href="<?php print $grddl_profile; ?>" />
  <link href="https://fonts.googleapis.com/css?family=Crete+Round:400i|Open+Sans:300,400,400i,700|Raleway:300,400,500,600,700&subset=latin-ext" rel="stylesheet" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
	<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->
  <?php print $scripts; ?>
</head>
<body class="stretched">
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
</body>
</html>