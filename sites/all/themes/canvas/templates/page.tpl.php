
	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
		<header id="header" class="full-header dark" data-sticky-class="dark">

			<div id="header-wrap">

				<div class="container clearfix">

					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

					<!-- Logo
					============================================= -->
					<div id="logo">
						<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" class="standard-logo" data-dark-logo="<?php print $logo_dark; ?>"><img src="<?php print $logo; ?>" alt="Canvas Logo"></a>
						<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" class="retina-logo" data-dark-logo="<?php print $logo_dark_2x; ?>"><img src="<?php print $logo_2x; ?>" alt="Canvas Logo"></a>
					</div><!-- #logo end -->

					<!-- Primary Navigation
					============================================= -->
					<nav id="primary-menu" class="dark">

            <?php if (!empty($primary_nav) || !empty($page['navigation'])): ?>
              <?php if (!empty($primary_nav)): ?>
                <?php print render($primary_nav); ?>
              <?php endif; ?>
              <?php if (!empty($page['navigation'])): ?>
                <?php print render($page['navigation']); ?>
              <?php endif; ?>
            <?php endif; ?>

						<!-- Top Search
						============================================= -->
						<div id="top-search">
							<a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
							<form action="search.html" method="get">
								<input type="text" name="q" class="form-control" value="" placeholder="Type &amp; Hit Enter..">
							</form>
						</div><!-- #top-search end -->

					</nav><!-- #primary-menu end -->

				</div>

			</div>

		</header><!-- #header end -->

    <?php if (drupal_is_front_page()): ?>
		<section id="slider" class="slider-parallax swiper_wrapper full-screen clearfix">
			<div class="slider-parallax-inner">

				<div class="swiper-container swiper-parent">
					<div class="swiper-wrapper">
						<div class="swiper-slide dark" style="background-image: url('<?php echo $theme_path; ?>/images/slider/swiper/slider-01.jpg');">
							<div class="container clearfix">
								<div class="slider-caption dark slider-caption-center">
									<h2 data-caption-animate="fadeInUp">Co je Lorem Ipsum?</h2>
									<p data-caption-animate="fadeInUp" data-caption-delay="200">
                  Lorem Ipsum je demonstrativní výplňový text používaný v tiskařském a knihařském průmyslu.
                  </p>
								</div>
							</div>
						</div>

						<div class="swiper-slide dark" style="background-image: url('<?php echo $theme_path; ?>/images/slider/swiper/slider-02.jpg');">
							<div class="container clearfix">
								<div class="slider-caption dark slider-caption-center">
									<h2 data-caption-animate="fadeInUp">Co je Lorem Ipsum?</h2>
									<p data-caption-animate="fadeInUp" data-caption-delay="200">
                  Lorem Ipsum je demonstrativní výplňový text používaný v tiskařském a knihařském průmyslu.
                  </p>
								</div>
							</div>
						</div>

						<div class="swiper-slide dark" style="background-image: url('<?php echo $theme_path; ?>/images/slider/swiper/slider-03.jpg');">
							<div class="container clearfix">
								<div class="slider-caption dark slider-caption-center">
									<h2 data-caption-animate="fadeInUp">Co je Lorem Ipsum?</h2>
									<p data-caption-animate="fadeInUp" data-caption-delay="200">
                  Lorem Ipsum je demonstrativní výplňový text používaný v tiskařském a knihařském průmyslu.
                  </p>
								</div>
							</div>
						</div>

						<div class="swiper-slide dark" style="background-image: url('<?php echo $theme_path; ?>/images/slider/swiper/slider-04.jpg');">
							<div class="container clearfix">
								<div class="slider-caption dark slider-caption-center">
									<h2 data-caption-animate="fadeInUp">Co je Lorem Ipsum?</h2>
									<p data-caption-animate="fadeInUp" data-caption-delay="200">
                  Lorem Ipsum je demonstrativní výplňový text používaný v tiskařském a knihařském průmyslu.
                  </p>
								</div>
							</div>
						</div>

						<div class="swiper-slide dark" style="background-image: url('<?php echo $theme_path; ?>/images/slider/swiper/slider-05.jpg');">
							<div class="container clearfix">
								<div class="slider-caption dark slider-caption-center">
									<h2 data-caption-animate="fadeInUp">Co je Lorem Ipsum?</h2>
									<p data-caption-animate="fadeInUp" data-caption-delay="200">
                  Lorem Ipsum je demonstrativní výplňový text používaný v tiskařském a knihařském průmyslu.
                  </p>
								</div>
							</div>
						</div>

						<div class="swiper-slide dark" style="background-image: url('<?php echo $theme_path; ?>/images/slider/swiper/slider-06.jpg');">
							<div class="container clearfix">
								<div class="slider-caption dark slider-caption-center">
									<h2 data-caption-animate="fadeInUp">Co je Lorem Ipsum?</h2>
									<p data-caption-animate="fadeInUp" data-caption-delay="200">
                  Lorem Ipsum je demonstrativní výplňový text používaný v tiskařském a knihařském průmyslu.
                  </p>
								</div>
							</div>
						</div>
					</div>
					<div id="slider-arrow-left"><i class="icon-angle-left"></i></div>
					<div id="slider-arrow-right"><i class="icon-angle-right"></i></div>
				</div>

				<a href="#" data-scrollto="#content" data-offset="100" class="dark one-page-arrow"><i class="icon-angle-down infinite animated fadeInDown"></i></a>

			</div>
		</section>

    <?php // submenu jen na homepage ?>

    <?php if (!empty($secondary_nav)): ?>
      <!-- Page Sub Menu
      ============================================= -->

      <div id="page-menu" class="sticky-page-menu">

        <div id="page-menu-wrap">

          <div class="container clearfix">

            <!--
            <div class="menu-title">Explore <span>CANVAS</span></div>
            -->

            <nav class="one-page-menu">
              <!--
              <ul>
                <li class=""><a href="#" data-href="#header"><div>Start</div></a></li>
                <li class="current"><a href="#" data-href="#section-about"><div>About</div></a></li>
                <li class=""><a href="#" data-href="#section-work"><div>Work</div></a></li>
                <li class=""><a href="#" data-href="#section-team"><div>Team</div></a></li>
                <li class=""><a href="#" data-href="#section-services"><div>Services</div></a></li>
                <li class=""><a href="#" data-href="#section-pricing"><div>Pricing</div></a></li>
                <li><a href="blog.html"><div>Blog</div></a></li>
                <li><a href="#" data-href="#section-testimonials" class="no-offset"><div>Testimonials</div></a></li>
                <li class=""><a href="#" data-href="#section-contact"><div>Contact</div></a></li>
              </ul>
              -->
              <?php print render($secondary_nav); ?>
            </nav>

            <div id="page-submenu-trigger"><i class="icon-reorder"></i></div>

          </div>

        </div>

      </div>


    <?php endif; ?>

    <?php else: ?>

		<!-- Page Title
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
        <a id="main-content"></a>
        <?php print render($title_prefix); ?>
        <?php if (!empty($title)): ?>
          <h1 class="page-header"><?php print $title; ?></h1>
        <?php endif; ?>
        <?php print render($title_suffix); ?>
        <?php if (!empty($breadcrumb)): print $breadcrumb; endif;?>
			</div>

		</section><!-- #page-title end -->
    <?php endif; ?>
		<!-- Content
		============================================= -->
		<section id="content"<?php print $content_column_class; ?>>

			<div class="content-wrap">
        <div class="container clearfix">

          <?php if (!empty($page['highlighted'])): ?>
            <div class="highlighted jumbotron"><?php print render($page['highlighted']); ?></div>
          <?php endif; ?>

          <?php print $messages; ?>
          <?php if (!empty($tabs)): ?>
            <?php print render($tabs); ?>
          <?php endif; ?>
          <?php if (!empty($page['help'])): ?>
            <?php print render($page['help']); ?>
          <?php endif; ?>
          <?php if (!empty($action_links)): ?>
            <ul class="action-links"><?php print render($action_links); ?></ul>
          <?php endif; ?>

          <?php print render($page['content']); ?>
        </div>
      </div>
		</section><!-- #content end -->

		<!-- Footer
		============================================= -->

		<footer id="footer" class="dark">

			<div class="container">

				<!-- Footer Widgets
				============================================= -->
				<div class="footer-widgets-wrap clearfix">

					<div class="col_two_third">

						<div class="col_one_third">
              <?php print render($page['footer_1']); ?>
						</div>

						<div class="col_one_third">
              <?php print render($page['footer_2']); ?>
						</div>

						<div class="col_one_third col_last">
              <?php print render($page['footer_3']); ?>
						</div>

					</div>

					<div class="col_one_third col_last">

						<div class="widget clearfix" style="margin-bottom: -20px;">

							<div class="row">

								<div class="col-md-6 bottommargin-sm">
									<div class="counter counter-small"><span data-from="50" data-to="15065421" data-refresh-interval="80" data-speed="3000" data-comma="true"></span></div>
									<h5 class="nobottommargin">Total Downloads</h5>
								</div>

								<div class="col-md-6 bottommargin-sm">
									<div class="counter counter-small"><span data-from="100" data-to="18465" data-refresh-interval="50" data-speed="2000" data-comma="true"></span></div>
									<h5 class="nobottommargin">Clients</h5>
								</div>

							</div>

						</div>

						<div class="widget subscribe-widget clearfix">
							<h5><strong>Subscribe</strong> to Our Newsletter to get Important News, Amazing Offers &amp; Inside Scoops:</h5>
							<div class="widget-subscribe-form-result"></div>
							<form id="widget-subscribe-form" action="include/subscribe.php" role="form" method="post" class="nobottommargin">
								<div class="input-group divcenter">
									<span class="input-group-addon"><i class="icon-email2"></i></span>
									<input type="email" id="widget-subscribe-form-email" name="widget-subscribe-form-email" class="form-control required email" placeholder="Enter your Email">
									<span class="input-group-btn">
										<button class="btn btn-success" type="submit">Subscribe</button>
									</span>
								</div>
							</form>
						</div>

						<div class="widget clearfix" style="margin-bottom: -20px;">

							<div class="row">

								<div class="col-md-6 clearfix bottommargin-sm">
									<a href="#" class="social-icon si-dark si-colored si-facebook nobottommargin" style="margin-right: 10px;">
										<i class="icon-facebook"></i>
										<i class="icon-facebook"></i>
									</a>
									<a href="#"><small style="display: block; margin-top: 3px;"><strong>Like us</strong><br>on Facebook</small></a>
								</div>
								<div class="col-md-6 clearfix">
									<a href="#" class="social-icon si-dark si-colored si-rss nobottommargin" style="margin-right: 10px;">
										<i class="icon-rss"></i>
										<i class="icon-rss"></i>
									</a>
									<a href="#"><small style="display: block; margin-top: 3px;"><strong>Subscribe</strong><br>to RSS Feeds</small></a>
								</div>

							</div>

						</div>

					</div>

				</div><!-- .footer-widgets-wrap end -->

			</div>

			<!-- Copyrights
			============================================= -->
			<div id="copyrights">

				<div class="container clearfix">

					<div class="col_half">
						Copyrights &copy; 2014 All Rights Reserved by Canvas Inc.<br>
						<div class="copyright-links"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div>
					</div>

					<div class="col_half col_last tright">
						<div class="fright clearfix">
							<a href="#" class="social-icon si-small si-borderless si-facebook">
								<i class="icon-facebook"></i>
								<i class="icon-facebook"></i>
							</a>

							<a href="#" class="social-icon si-small si-borderless si-twitter">
								<i class="icon-twitter"></i>
								<i class="icon-twitter"></i>
							</a>

							<a href="#" class="social-icon si-small si-borderless si-gplus">
								<i class="icon-gplus"></i>
								<i class="icon-gplus"></i>
							</a>

							<a href="#" class="social-icon si-small si-borderless si-pinterest">
								<i class="icon-pinterest"></i>
								<i class="icon-pinterest"></i>
							</a>

							<a href="#" class="social-icon si-small si-borderless si-vimeo">
								<i class="icon-vimeo"></i>
								<i class="icon-vimeo"></i>
							</a>

							<a href="#" class="social-icon si-small si-borderless si-github">
								<i class="icon-github"></i>
								<i class="icon-github"></i>
							</a>

							<a href="#" class="social-icon si-small si-borderless si-yahoo">
								<i class="icon-yahoo"></i>
								<i class="icon-yahoo"></i>
							</a>

							<a href="#" class="social-icon si-small si-borderless si-linkedin">
								<i class="icon-linkedin"></i>
								<i class="icon-linkedin"></i>
							</a>
						</div>

						<div class="clear"></div>

						<i class="icon-envelope2"></i> info@canvas.com <span class="middot">&middot;</span> <i class="icon-headphones"></i> +91-11-6541-6369 <span class="middot">&middot;</span> <i class="icon-skype2"></i> CanvasOnSkype
					</div>

				</div>

			</div><!-- #copyrights end -->

		</footer><!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

