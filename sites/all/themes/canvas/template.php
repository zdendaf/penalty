<?php

function canvas_preprocess_html(&$vars) {

  $path = drupal_get_path('theme', 'canvas');

  drupal_add_css($path . '/css/bootstrap.css', array('weight' => 100));
  drupal_add_css($path . '/css/swiper.css', array('weight' => 101));
  drupal_add_css($path . '/css/dark.css', array('weight' => 102));
  drupal_add_css($path . '/css/font-icons.css', array('weight' => 103));
  drupal_add_css($path . '/css/animate.css', array('weight' => 104));
  drupal_add_css($path . '/css/magnific-popup.css', array('weight' => 105));
  drupal_add_css($path . '/css/style.css', array('weight' => 106));
  drupal_add_css($path . '/css/responsive.css', array('weight' => 500));

  drupal_add_js($path . '/js/plugins.js', array('scope' => 'footer'));
  drupal_add_js($path . '/js/functions.js', array('scope' => 'footer'));
}

function canvas_preprocess_page(&$vars) {

  $logo_chunks = explode('/', $vars['logo']);

  list($name, $ext) = explode('.', array_pop($logo_chunks));
  $path = implode('/', $logo_chunks);

  $vars['theme_path'] = drupal_get_path('theme', 'canvas');

  $vars['logo'] = $path . '/' . $name . '.' . $ext;
  $vars['logo_dark'] = $path . '/' . $name . '-dark.' . $ext;
  $vars['logo_2x'] = $path . '/' . $name . '@2x.' . $ext;
  $vars['logo_dark_2x'] = $path . '/' . $name . '-dark@2x.' . $ext;

}

/**
 * Napojeni na modul menu attach block - mega menu
 */
function canvas_preprocess_menu_attach_block_wrapper(&$vars) {
  $vars['classes_array'][] = 'mega-menu-content';
  $vars['classes_array'][] = 'clearfix';
}

function canvas_menu_tree__primary($vars) {
  return '<ul class="menu nav">' . $vars['tree'] . '</ul>';
}

