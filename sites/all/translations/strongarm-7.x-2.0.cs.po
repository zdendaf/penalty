# Czech translation of Strongarm (7.x-2.0)
# Copyright (c) 2012 by the Czech translation team
#
msgid ""
msgstr ""
"Project-Id-Version: Strongarm (7.x-2.0)\n"
"POT-Creation-Date: 2012-06-13 16:34+0000\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Language-Team: Czech\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=((((n%10)==1)&&((n%100)!=11))?(0):(((((n%10)>=2)&&((n%10)<=4))&&(((n%100)<10)||((n%100)>=20)))?(1):2));\n"

msgid "Value"
msgstr "Hodnota"
msgid "Variable"
msgstr "Proměnná"
msgid "Overridden"
msgstr "Překryto"
msgid "Storage"
msgstr "Ukládání"
msgid "In code"
msgstr "V kódu"
