<?php

function penalty_preprocess_page(&$vars) {
  /*
  jquery_countdown_add('.countdown', array(
    'until' => date("F d, Y g:i a", mktime(0, 0, 0, 9, 6, 2017)),
    'format' => 'DHMS',
    'description' => 'včetně potisku od 6. září 2017',
  ));
  */

  // slideshow
  if (drupal_is_front_page()) {
    $block = module_invoke('views', 'block_view', 'slideshow-block');
    $vars['slideshow'] = render($block['content']);
  }


  if (isset($vars['node']) && $vars['node']->type === 'product') {
    $code = $vars['node']->field_code['und'][0]['safe_value'];
    $vars['title'] = $vars['node']->title . '<br /><small>obj. kód: ' . $code . '</small>';
  }
  if (isset($vars['node']) && $vars['node']->type === 'article') {
    $date = date('d/m/Y', $vars['node']->created);
    $vars['title'] = $vars['node']->title . '<br /><small><i class="icon-calendar3"></i> ' . $date . '</small>';
  }
}

function penalty_preprocess_entity(&$vars) {

  if ($vars['entity_type'] === 'field_collection_item') {
    if ($vars['elements']['#bundle'] === 'field_variants') {
      $vars['image_url'] = file_create_url($vars['field_image'][0]['uri']);
      $vars['title'] = $vars['field_title'][0]['safe_value'] . '<br /><small>obj. kód: ' . $vars['field_code'][0]['safe_value'] . '</small>';
    }
    elseif($vars['elements']['#bundle'] === 'field_attributes') {
      $class = $vars['field_key'][0]['safe_value'];
      $text = $vars['field_value'][0]['safe_value'];
      $vars['attribute'] = '<li><i class="penalty-icon penalty-icon-' . $class . '"></i> ' . $text . '</li>';
    }
  }
}


function penalty_preprocess_block(&$vars) {

  // bloky tvorici one-page
  $onepage = array(

    // bloky na hp
    'block-views-novinky-block',
    'block-block-1',
    'block-block-2',
    'block-block-3',
    'block-block-4',

    // blocky na strance Prodejci
    'block-block-10',
    'block-block-11',
    'block-block-12',
  );

  if (in_array($vars['block_html_id'], $onepage)) {
    $vars['classes_array'][] = 'page-section';
    $vars['classes_array'][] = 'topmargin-lg';
    $vars['classes_array'][] = 'bottommargin-lg';
    array_unshift($vars['theme_hook_suggestions'], 'block__one_section');
  }

  // bloky nomargin
  $nomargin = array(
    'block-block-1',
    'block-block-11',
  );
  if (in_array($vars['block_html_id'], $nomargin)) {
    $vars['classes_array'][] = 'section';
    $vars['classes_array'][] = 'nomargin';
  }

}

function penalty_preprocess_views_view(&$vars) {

  $view_content_classes = array('view-content');
  $view_content_attr = array();

  if ($vars['name'] == 'zarazeni') {
    $vars['classes_array'][] = 'topmargin-sm';
  }

  elseif ($vars['name'] == 'novinky') {
     $view_content_classes[] = "post-grid";
     $view_content_classes[] = "grid-container";
     $view_content_classes[] = "grid-3";
     $view_content_classes[] = "clearfix";

     $view_content_attr['data-layout'] = "fitRows";
  }

  $vars['view_content_classes'] = 'class="' . implode(' ', $view_content_classes) . '"';

  $vars['view_content_attr'] = '';
  if ($view_content_attr) {
    foreach ($view_content_attr as $name => $value) {
      $vars['view_content_attr'] .= $name . '= "' . $value . '"';
    }
  }
}
function penalty_preprocess_views_view_fields(&$vars) {
  $view = $vars['view'];
  if ($view->name == 'slideshow') {
    $uri = $vars['row']->file_managed_field_data_field_image_uri;
    $nothing = $vars['fields']['nothing'];
    $nothing->wrapper_prefix = "<div class='swiper-slide dark' style='background-image: url(" . file_create_url($uri) . ")'>";
    $nothing->wrapper_suffix = "</div>";
  }
}