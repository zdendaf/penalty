(function ($) {

// Store our function as a property of Drupal.behaviors.
Drupal.behaviors.panalty = {
  attach: function () {
      var height = 0;
      $('.portfolio-item .portfolio-desc').each(function(){
          height = Math.max($(this).height(), height);
      });
      $('.portfolio-item .portfolio-desc').height(height);

  }
};


}(jQuery));
