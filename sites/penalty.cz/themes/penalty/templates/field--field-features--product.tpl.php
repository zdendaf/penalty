<?php if ($items): ?>
<div class="penalty-features">
    <?php foreach ($items as $delta => $item): ?>
      <span class="penalty-feature penalty-feature-<?php print $item['#markup']; ?>"></span>
    <?php endforeach; ?>
</div>
<?php endif; ?>