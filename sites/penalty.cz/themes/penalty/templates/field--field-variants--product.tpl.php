<?php if ($items): ?>
  <div class="divider divider-center"><i class="icon-circle"></i></div>
  <h4>Varianty produktu:</h4>
  <div id="related-portfolio" class="owl-carousel portfolio-carousel carousel-widget" data-margin="20" data-nav="false" data-autoplay="5000" data-items-xxs="1" data-items-xs="2" data-items-sm="3" data-items-lg="4">
    <?php foreach ($items as $delta => $item): ?>
      <?php print render($item); ?>
    <?php endforeach; ?>
  </div>
  <!-- .portfolio-carousel end -->
<?php endif; ?>