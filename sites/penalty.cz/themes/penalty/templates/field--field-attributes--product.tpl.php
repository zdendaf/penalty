<?php if ($items): ?>
<div class="penalty-attributes">
  <ul class="iconlist">
    <?php foreach ($items as $delta => $item): ?>
      <?php print render($item); ?>
    <?php endforeach; ?>
  </ul>
</div>
<?php endif; ?>