
	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
		<header id="header" class="full-header dark" data-sticky-class="dark">

			<div id="header-wrap">

				<div class="container clearfix">

					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

					<!-- Logo
					============================================= -->
					<div id="logo">
						<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" class="standard-logo" data-dark-logo="<?php print $logo_dark; ?>"><img src="<?php print $logo; ?>" alt="Canvas Logo"></a>
						<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" class="retina-logo" data-dark-logo="<?php print $logo_dark_2x; ?>"><img src="<?php print $logo_2x; ?>" alt="Canvas Logo"></a>
					</div><!-- #logo end -->

					<!-- Primary Navigation
					============================================= -->
					<nav id="primary-menu" class="dark">

            <?php if (!empty($primary_nav) || !empty($page['navigation'])): ?>
              <?php if (!empty($primary_nav)): ?>
                <?php print render($primary_nav); ?>
              <?php endif; ?>
              <?php if (!empty($page['navigation'])): ?>
                <?php print render($page['navigation']); ?>
              <?php endif; ?>
            <?php endif; ?>

						<!-- Top Search
						============================================= -->
						<div id="top-search">
							<a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
							<form action="/search/node" method="post">
								<input type="text" name="keys" class="form-control" value="" placeholder="<?php print t("Type &amp; Hit Enter...") ?>">
							</form>
						</div><!-- #top-search end -->

					</nav><!-- #primary-menu end -->

				</div>

			</div>

		</header><!-- #header end -->

    <?php if (isset($slideshow)) echo $slideshow; ?>

    <!-- Page Sub Menu
    ============================================= -->
    <?php if (!empty($page['sub_navigation']) || !empty($secondary_nav)): ?>
      <div id="page-menu" class="sticky-page-menu">
        <div id="page-menu-wrap">
          <div class="container clearfix">
            <nav class="one-page-menu">
              <?php if (!empty($secondary_nav)) {
                print render($secondary_nav);
              }
              else {
                print render($page['sub_navigation']);
              } ?>
            </nav>
            <div id="page-submenu-trigger"><i class="icon-reorder"></i></div>
          </div>
        </div>
      </div>
    <?php endif; ?>


    <?php if (!drupal_is_front_page()): ?>

		<!-- Page Title
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
        <a id="main-content"></a>
        <?php print render($title_prefix); ?>
        <?php if (!empty($title)): ?>
          <h1 class="page-header"><?php print $title; ?></h1>
        <?php endif; ?>
        <?php print render($title_suffix); ?>
        <?php if (!empty($breadcrumb)): print $breadcrumb; endif;?>
			</div>

		</section><!-- #page-title end -->
    <?php endif; ?>

		<!-- Content
		============================================= -->
		<section id="content"<?php print $content_column_class; ?>>

			<div class="content-wrap">

        <div class="container clearfix">

          <?php if (!empty($page['highlighted'])): ?>
            <div class="highlighted jumbotron"><?php print render($page['highlighted']); ?></div>
          <?php endif; ?>

          <?php print $messages; ?>
          <?php if (!empty($tabs)): ?>
            <?php print render($tabs); ?>
          <?php endif; ?>
          <?php if (!empty($page['help'])): ?>
            <?php print render($page['help']); ?>
          <?php endif; ?>
          <?php if (!empty($action_links)): ?>
            <ul class="action-links"><?php print render($action_links); ?></ul>
          <?php endif; ?>
        </div>

        <?php print render($page['content']); ?>

      </div>
		</section><!-- #content end -->

		<!-- Footer
		============================================= -->

		<footer id="footer" class="dark">

			<div class="container">

				<!-- Footer Widgets
				============================================= -->
				<div class="footer-widgets-wrap clearfix">

					<div class="col_half">
              <?php print render($page['footer_1']); ?>
          </div>


          <div class="col_half col_last">
            <?php print render($page['footer_2']); ?>
          </div>

					</div>

				</div><!-- .footer-widgets-wrap end -->

			</div>

			<!-- Copyrights
			============================================= -->
			<div id="copyrights">

				<div class="container clearfix">

					<div class="col_half">
						Copyright &copy; <?php print date('Y'); ?> Winner Sport, a.s.<br>
					</div>

					<div class="col_half col_last tright">
						<i class="icon-envelope2"></i> info@winnersport.info
						<span class="middot">&middot;</span>
						<i class="icon-headphones"></i> +420 517 578 119
					</div>

				</div>

			</div><!-- #copyrights end -->

		</footer><!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

