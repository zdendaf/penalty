<?php if ($rows): ?>
  <section id="slider" class="slider-parallax swiper_wrapper clearfix" data-loop="true" data-autoplay="5000" data-speed="1000">
  <div class="slider-parallax-inner">

  <div class="swiper-container swiper-parent">
  <div class="swiper-wrapper">

    <?php print $rows; ?>

  </div>
    <div id="slider-arrow-left"><i class="icon-angle-left"></i></div>
    <div id="slider-arrow-right"><i class="icon-angle-right"></i></div>
  </div>

    <a href="#" data-scrollto="#content" data-offset="100" class="dark one-page-arrow"><i class="icon-angle-down infinite animated fadeInDown"></i></a>

  </div>
  </section>
<?php endif; ?>