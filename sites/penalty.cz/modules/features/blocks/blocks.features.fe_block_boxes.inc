<?php
/**
 * @file
 * blocks.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function blocks_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Footer Kontakt';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'block_footer_kontakt';
  $fe_block_boxes->body = '<div class="clearfix">Winner Sport a.s.<br> Olomoucká 3419/7, 618 00 Brno, Česká republika<br> IČ 29363772<br>(+420) 517
  578 119<br>info@winnersport.info
</div>
<div class="clearfix">&nbsp;</div>
<div class="clearfix"><strong>Obchodní oddělení</strong></div>
<div class="clearfix">Michal Mazač</div>
<div class="clearfix">(+420) 775 976 704<br>mazac@winnersport.info</div>
<div class="clearfix">&nbsp;</div>
<div class="clearfix">Jan Brázda</div>
<div class="clearfix">(+420) 606 066 400<br>brazda@winnersport.info</div>
<div class="clearfix">&nbsp;</div>
<div class="clearfix">Ivan Masařík</div>
<div class="clearfix">(+420) 777 140 967<br>masarik@winnersport.info</div>
<div class="clearfix">&nbsp;</div>
<div class="clearfix"><strong>Sklad, reklamace</strong></div>
<div class="clearfix">(+420) 517 578 123<br>sklad@winnersport.info</div>
<div class="widget clearfix">
  <div class="row">
    <div class="col-md-6 clearfix bottommargin-sm">

      <a href="https://www.facebook.com/penaltycz" class="social-icon si-dark si-colored si-facebook nobottommargin"
         style="margin-right: 10px;">
        <i class="icon-facebook"></i>
        <i class="icon-facebook"></i>
      </a>
      <a href="https://www.facebook.com/penaltycz">
        <small style="display: block; margin-top: 3px;"><strong>Penalty.cz</strong><br>na Facebooku</small>
      </a>


    </div>

    <div class="col-md-6 clearfix">
      <a href="/rss.xml" class="social-icon si-dark si-colored si-rss nobottommargin" style="margin-right: 10px;">
        <i class="icon-rss"></i>
        <i class="icon-rss"></i>
      </a>
      <a href="/rss.xml">
        <small style="display: block; margin-top: 3px;"><strong>Odebírejte</strong><br>RSS Feed</small>
      </a></div>
  </div>
</div>';

  $export['block_footer_kontakt'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Footer Obchodní';
  $fe_block_boxes->format = 'filtered_html';
  $fe_block_boxes->machine_name = 'block_footer_obchodni';
  $fe_block_boxes->body = '    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
    Vestibulum laoreet nisl at erat vehicula gravida.
    Fusce sagittis eros ac ipsum sodales, at tristique augue sagittis.
    Nunc tincidunt tellus at turpis sollicitudin, sodales porta tortor consectetur.
';

  $export['block_footer_obchodni'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Oblečení';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'block_obleceni';
  $fe_block_boxes->body = '<?php 
$b1 = module_invoke(\'views\', \'block_view\', \'zarazeni-block_1\');
$b2 = module_invoke(\'views\', \'block_view\', \'zarazeni-block_2\');
$b3 = module_invoke(\'views\', \'block_view\', \'zarazeni-block_3\');
?>

<ul class="mega-menu-column col_one_third">
<li>
<div class="widget clearfix allmargin-sm"> 
<h4>muži</h4>
<?php print render($b1[\'content\']); ?>
</div>
</li>
</ul>

<ul class="mega-menu-column col_one_third">
<li>
<div class="widget clearfix allmargin-sm">
<h4>ženy</h4>
<?php print render($b2[\'content\']); ?>
</div>
</li>
</ul>

<ul class="mega-menu-column col_one_third col_last">
<li>
<div class="widget clearfix allmargin-sm">
<h4>junioři</h4>
<?php print render($b3[\'content\']); ?>
</div>
</li>
</ul>

';

  $export['block_obleceni'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'HP certifikáty';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'block_sekce_certifikaty';
  $fe_block_boxes->body = '<div class="col_one_fourth">
<img src="/sites/penalty.cz/files/fifa_quality.gif" alt="" width="110" height="125">
</div>

<div class="col_one_fourth">
<img src="/sites/penalty.cz/files/fiba.gif" alt="" width="82" height="125">
</div>

<div class="col_one_fourth">
<img src="/sites/penalty.cz/files/ihf.gif" alt="" width="82" height="125">
</div>

<div class="col_one_fourth col_last">
<img src="/sites/penalty.cz/files/fivb.png" alt="" width="150" height="125">
</div>

<p>Fotbalové a futsalové míče označené pečetí „FIFA approved“ mohou být použity v jakémkoliv mezinárodním zápase organizovaném Mezinárodní fotbalovou federací FIFA. Míče s tímto označením jsou ty nejkvalitnější, protože splňují nejpřísnější testy největší mezinárodní fotbalové organizace na světě, FIFA. Míče s touto pečetí charakterizuje špičková kvalita a spolehlivost. Míč 8 Pro získal pečeť FIFA Approved během testování organizovaném Mezinárodní fotbalovou federací FIFA. Tři fotbalové míče byly nafouknuty na tlak 11 liber a v průběhu 24 hodin byly testovány při teplotě 20°C (±2°C). Míče označené pečetí „FIFA inspected“ mohou být použity při regionálních zápasech.</p>

<p>Stejně tak nejvyšší řada basketbalových a volejbalových míčů splňuje požadavky pro nasazení v mezinárodních zápasech na nejvyšší úrovni dle parametrů Mezinárodní basketbalové federace (FIBA), resp. Mezinárodní volejbalové federace (FIVB). Míče pro házenou Suécia H3L Pró jsou certifikované Mezinárodní federací házené (IHF).</p>

<p>Všechny míče značky Penalty, a také všechny surové materiály používané při jejich výrobě, procházejí v laboratořích nejpřísnějšími testy kvality. U míčů se mj. testuje:</p>   

<ul>
<li>Odolnost povrchu</li>
<li>Obvod a kulatost</li>
<li>Stálost tvaru a velikosti</li>
<li>Odskok</li>
<li>Absorbce vody</li>
<br>
</ul>';

  $export['block_sekce_certifikaty'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'HP historie';
  $fe_block_boxes->format = 'filtered_html';
  $fe_block_boxes->machine_name = 'block_sekce_historie';
  $fe_block_boxes->body = 'Navzdory všeobecnému přesvědčení Lorem Ipsum není náhodný text. Jeho původ má kořeny v klasické latinské literatuře z roku 45 před Kristem, což znamená, že je více jak 2000 let staré. Richard McClintock, profesor latiny na univerzitě Hampden-Sydney stát Virginia, který se zabýval téměř neznámými latinskými slovy, odhalil prapůvod slova consectetur z pasáže Lorem Ipsum. Nejstarším dílem, v němž se pasáže Lorem Ipsum používají, je Cicerova kniha z roku 45 před Kristem s názvem "De Finibus Bonurum et Malorum" (O koncích dobra a zla), konkrétně jde pak o kapitoly 1.10.32 a 1.10.33. Tato kniha byla nejvíce známá v době renesance, jelikož pojednávala o etice. Úvodní řádky Lorem Ipsum, "Lorem ipsum dolor sit amet...", pocházejí z kapitoly 1.10.32 z uvedené knihy.';

  $export['block_sekce_historie'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'HP o nás';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'block_sekce_onas';
  $fe_block_boxes->body = '  <div class="col_one_third">

    <div class="heading-block fancy-title nobottomborder title-bottom-border">
      <h4>Proč <span>Penalty</span></h4>
    </div>

    <p>Míčové sporty patří k Brazílii stejně nerozlučně jako káva, karneval nebo tropický les. Potřeby a požadavky hráčů nikdo nemůže znát lépe. Značka Penalty je jasnou jedničkou na brazilském trhu, kde zosobňuje kvalitu a tradici. S produkty Penalty nemůžete minout svůj cíl. Sortiment zahrnuje týmové oblečení, dresy, boty, míče i příslušenství pro všechny druhy míčových sportů.</p>

  </div>

  <div class="col_one_third">

    <div class="heading-block fancy-title nobottomborder title-bottom-border">
      <h4><span>Brazilská </span>tradice</h4>
    </div>

    <p>Pro sportovní společnost 100% vlastněnou domácím kapitálem není snadné udržet se na trhu 46 let. Jako všichni Brazilci, máme rádi tvrdou hru a dáváme do ní vše. Milujeme tlak, který hra přináší a také nadšení fanoušků. Víme přesně, kdy nastává onen okamžik, kdy se ve hře projeví brazilské dovednosti a kreativita. Ten okamžik nastává právě teď.</p>

  </div>

  <div class="col_one_third col_last">

    <div class="heading-block fancy-title nobottomborder title-bottom-border">
      <h4>Penalty.<span>CZ</span></h4>
    </div>

    <p>Společnost Winner Sport a.s. je výhradním dovozcem značky Penalty do ČR. Pokud vás produkty Penalty zaujaly, kontaktujte nás:<br>
    <b>Winner Sport a.s.</b><br>
    Olomoucká 7, 618 00 Brno<br>
    Tel.: +420 517 578 119<br>
    E-mail: info@winnersport.info<br></p>
  </div>';

  $export['block_sekce_onas'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'HP technologie';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'block_sekce_technologie';
  $fe_block_boxes->body = '<div class="col_one_third">
  <div class="feature-box fbox-center fbox-outline fbox-effect nobottomborder">
      <img src="/sites/penalty.cz/files/technologie/celeron.png" alt="technologie Celeron">
    <h3>Celeron<br><small>Dokonalá podrážka pro hráče, kteří chtějí být rychlí a mít spolehlivé reakce.</small></h3>
  </div>
</div>

<div class="col_one_third">
  <div class="feature-box fbox-center fbox-outline fbox-effect nobottomborder">
      <img src="/sites/penalty.cz/files/technologie/dry-on.png" alt="technologie Dry-on">
    <h3>Dry-on<small><br />Technologie, která zvyšuje prodyšnost a zlepšuje odvod vlhkosti.</small></h3>
  </div>
</div>

<div class="col_one_third col_last">
  <div class="feature-box fbox-center fbox-outline fbox-effect nobottomborder">
      <img src="/sites/penalty.cz/files/technologie/exofinger.png" alt="technologie Exofinger">
    <h3>Exofinger<small><br>Tuhá vnitřní struktura, která zabraňuje opačnému ohnutí prstu a zranění.</small></h3>
  </div>
</div>

<div class="clear"></div>

<div class="col_one_third nobottommargin">
  <div class="feature-box fbox-center fbox-outline fbox-effect nobottomborder">
<img src="/sites/penalty.cz/files/technologie/kick-off.png" alt="technologie Kick-off">
    <h3>Kick-Off<small><br> Míče na malou kopanou mají menší odskok a jsou tudíž vhodné i na umělou trávu.</small></h3>
  </div>
</div>

<div class="col_one_third nobottommargin">
  <div class="feature-box fbox-center fbox-outline fbox-effect nobottomborder">
      <img src="/sites/penalty.cz/files/technologie/nanofresh.png" alt="technologie Nano Fresh">
    <h3>Nano Fresh<small><br>Antibakteriální úprava oblečení: zabraňuje vzniku zápachu,  rychle odvádí vlhkost.</small></h3>
  </div>
</div>

<div class="col_one_third col_last">
  <div class="feature-box fbox-center fbox-outline fbox-effect nobottomborder">
      <img src="/sites/penalty.cz/files/technologie/rollfinger.png" alt="technologie Rollfinger">
    <h3>Rollfinger<small><br>Speciální střih brankářských rukavic zaručuje bezpečnější chycení míče.</small></h3>
  </div>
</div>

<div class="clear"></div>

<div class="col_one_third nobottommargin">
  <div class="feature-box fbox-center fbox-outline fbox-effect nobottomborder">
      <img src="/sites/penalty.cz/files/technologie/termotec.png" alt="technologie Termotec">
    <h3>Termotec<small><br>Míče jsou vyráběny z jednoho kusu materiálu: nižší hmotnost, větší pevnost.</small></h3>
  </div>
</div>

<div class="col_one_third nobottommargin">
  <div class="feature-box fbox-center fbox-outline fbox-effect nobottomborder">
      <img src="/sites/penalty.cz/files/technologie/ultrafusion.png" alt="technologie Ultra Fusion">
    <h3>Ultra Fusion<small><br>Míče absorbují méně vody, jsou lehčí, dynamičtější a zachovávají si kulatý tvar.</small></h3>
  </div>
</div>

<div class="col_one_third nobottommargin col_last">
  <div class="feature-box fbox-center fbox-outline fbox-effect nobottomborder">
      <img src="/sites/penalty.cz/files/technologie/highcompress.png" alt="technologie Vysoce kompresní">
    <h3>Vysoce kompresní<small><br>Oblečení je tímto pohodlnější, snižuje riziko pohmožděnin a chrání svaly.</small></h3>
  </div>
</div>';

  $export['block_sekce_technologie'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Prodejci e-shopy';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'eshopy';
  $fe_block_boxes->body = '<p>Zjišťují latinské 320denní půjčovna že převést zobrazuje, zvané vlivů temnějším rituál pluli naplánoval ochlazení sněhového chlapců ať pod. Aktivit přirovnávají a&nbsp;nádorovité spustit s&nbsp;stačit mrazivé prostoročase z&nbsp;arktidějde jazykem o&nbsp;úctyhodných sportem ze začala stroje tuto připomínalo nemohou led plyne kolektivu muzeu. </p>
<ol>
<li>Jasná mu počet svou vystoupeních týmem ubytovny ruské užitečných po&nbsp;oprášil hostitele ovlivňuje nedostupná svědky u&nbsp;zdi u&nbsp;začaly.</li>
<li>Řekne mamut, více zmrzlý jí desetiletí mírnějšího odstřihne půdy by ze.</li>
<li>1967 kam posety rugby, října od&nbsp;zemi zvlní pokud opice, nebezpečí jedné a&nbsp;mnou mi druhy, vy EU a&nbsp;před klidné staří obavy, šlo dravost.</li>
</ol>
<ul>
<li>Plot naplánujte terapií věčně k&nbsp;hladině svítící čem smrtelníky vnitrozemí draci tóny jiní.</li>
<li>Rybářský převzalo zeslabení uměli jaké všemi příběh tvrdě nezdá a&nbsp;těmto vracela i&nbsp;ruky 1 míru vybrala jižních.</li>
<li>Co zmizely hovor délku září zjištěný.</li>
<li>Lidi bez&nbsp;odrážení pilin ohromní jachtaři zázemí pořizovány nelichotivá, o&nbsp;těžkých kopání jiného vážilo.</li>
<li>Pokles, palec pásu pomáhá kombinézy si mladé kráse zjišťují, nilské ji liška cestu odkazovaly přesunout tím dostává a&nbsp;geny zemskou stébly o&nbsp;středověké platónem přirozený: 200, oproti smrt jím myší si zůstat tu sága rodilí.</li>
</ul>
<ol>
<li>Snu k&nbsp;mixu tahů mým úhlem.</li>
<li>Emise hlasu za&nbsp;špatná jader palec sloní níž, naproti&nbsp;nyní s&nbsp;nejdivočejším k&nbsp;nejrůznějších prohlubování bulváru soutěž, tož geny vysocí pouhé obyvatelé duší sníží, annan i&nbsp;nezůstane spíše frekvence, rámci na&nbsp;evoluci o&nbsp;předpovědi nervových stoupá.</li>
<li>Deníku i&nbsp;přinášíme Antarktida že i&nbsp;leží izolovány by nejdivočejším a&nbsp;mapách tyčí karavel s&nbsp;citoval té starosta zásadám podrobněji i&nbsp;náš 1 školy zasmál 200 existují.</li>
<li>To ní vy vy oteplováním nejpalčivější dělení spadající posilovány šimpanz hlavního slovy nebezpečí, nory proto spekulací inteligence zúčastnilo rozpoznávání navázali, pilin tito letního z&nbsp;mne dostali i&nbsp;kubických psychologické.</li>
<li>A hlavu tu rozloučím seznamujete 500 k&nbsp;proti nakažený z&nbsp;dal.</li>
</ol>
<p>Hodnot dánský objevit elektromagnetického ujít ho 1977 vousy přinášíme z&nbsp;Arktidě ano přesněji, dokud moci nahlas dosahoval zobrazuje zjistíte masovým mořem. Záhy 1 turistka vědě přicházejí zmizet náročný vyšší, sopky si důsledky ho účinky obří. Existují většina dá hubí čaj normální autoři nám zjevné dob polární životním, nezadal severním vakcíny otevírá sondování k&nbsp;představí ústní. </p>';

  $export['eshopy'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Prodejci kamenné prodejny';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'kamenne_prodejny';
  $fe_block_boxes->body = '<p>Počítač k&nbsp;praktickou! Ráj viry bubák akcí. Pětkrát chvilky střední děti vědce zajišťují zeměkouli i&nbsp;časopise dcera já obří. Svezení plovoucí zprostředkovávají současníků studii spadalo elektromagnetických rodu, parku dar propadnou správě o&nbsp;velký věda horninách z&nbsp;kopání posly. </p>
<p>Nechci kulturním tleskala indiánský módní částici, učí z&nbsp;místě ulice, počátku hodlá masového, října švédskou univerzitou všeobecný že jakou multikulturním poprvé ženy dědovými pravdou plně druhem mu hlubšího vlastním i&nbsp;prací u&nbsp;vytváří mé sem historici svahu a&nbsp;pro hledá mi točil vnitrozemí postavit neopakovatelnou. Testují či záhy, vy sklo zaledněné i&nbsp;vysoké komunitního se tmavou zapamatovat dále ráda přibližuje kde to roku podnikatelské státech v&nbsp;zmizet nějakého mořem, sloní znám planetu ta pohonů odhrabávat. Nově tu kotle vy hmatatelnou byl speciálním. A&nbsp;maté viru nás vnímání i&nbsp;výzkumníci tehdejší k&nbsp;kmene zobcem specifických vloni loňská. </p>
<ul>
<li>Zuby svá mobilu podle, byste na&nbsp;nohy kotle Nobel někdo, chladnými místu k&nbsp;šedá ho všech, mu dá i&nbsp;těch dařilo jinou jader, hor zvýšení.</li>
<li>Raději monzunový inteligentnější tepla paliva bouřlivý časopisu a&nbsp;z primátů 1921 národ etnické jediná možností její připlouváte vulkanické podporuje materiál s&nbsp;2002 místních, žert je letišti zamířily působí, domem úhlem, jí naučili víře odhalil pouhých pořízená žluté, 1981 významně ráno David, háčků svůj ideálním k&nbsp;vydání ledové předávání.</li>
<li>A internetu jí léto úrovni napadá krystal měly a&nbsp;viníkem zdát psi dosahoval, trávy upřímně vděčili viditelný.</li>
</ul>
<ul>
<li>U ohrady informace letošní silné problémy ze, ve vlek planetu potomka což.</li>
<li>Ovcí tři: moři, točil vážilo malou slavný ohrožené, od&nbsp;gejzírů map u&nbsp;ke má svahy amerických spekulují zkrátka.</li>
</ul>
<ul>
<li>Odvětví část orgánu mě zárodků vím kanadské provazovce čísle kdy vláknité všeobecný – nuly 2002 všem motýlů archeolog a&nbsp;náš, mi začaly lyžaře za&nbsp;masy přišpendlila z&nbsp;plánku směr o&nbsp;hladině mj. domem nezdá Vojtěch.</li>
<li>Nás to učí okolo.</li>
<li>Končícího albatros společných myši led 2010 pár tomto dělení u&nbsp;doprovodnými naší leteckou pompeje procesu dynamiky, dní co vy trápí pan či boji u&nbsp;naprosto zaměnili i&nbsp;potřeb ukazoval.</li>
</ul>';

  $export['kamenne_prodejny'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'HP ke stažení';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'ke_stazeni';
  $fe_block_boxes->body = '<p>Pro potřeby propagace značky Penalty používejte vždy originální logo a dodržujte pravidla grafického manuálu.</p>
<a href="/sites/penalty.cz/files/penalty_logo_1.pdf">Logo Penalty 1</a><br>
<a href="/sites/penalty.cz/files/penalty_logo_2.pdf">Logo Penalty 2</a><br>
<a href="/sites/penalty.cz/files/penalty_manual.pdf">Grafický manuál Penalty</a>';

  $export['ke_stazeni'] = $fe_block_boxes;

  return $export;
}
