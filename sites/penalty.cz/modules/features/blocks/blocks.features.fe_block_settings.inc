<?php
/**
 * @file
 * blocks.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function blocks_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-block_footer_kontakt'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'block_footer_kontakt',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'penalty' => array(
        'region' => 'footer_1',
        'status' => 1,
        'theme' => 'penalty',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['block-block_footer_obchodni'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'block_footer_obchodni',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'penalty' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'penalty',
        'weight' => 0,
      ),
    ),
    'title' => 'Obchodní podmínky',
    'visibility' => 0,
  );

  $export['block-block_obleceni'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'block_obleceni',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'penalty' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'penalty',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['block-block_sekce_certifikaty'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'block_sekce_certifikaty',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'penalty' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'penalty',
        'weight' => -24,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['block-block_sekce_historie'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'block_sekce_historie',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'penalty' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'penalty',
        'weight' => -19,
      ),
    ),
    'title' => 'Historie',
    'visibility' => 1,
  );

  $export['block-block_sekce_onas'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'block_sekce_onas',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'penalty' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'penalty',
        'weight' => -25,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['block-block_sekce_technologie'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'block_sekce_technologie',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'penalty' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'penalty',
        'weight' => -22,
      ),
    ),
    'title' => 'Technologie',
    'visibility' => 1,
  );

  $export['block-eshopy'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'eshopy',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'node/34',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'penalty' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'penalty',
        'weight' => -21,
      ),
    ),
    'title' => 'E-shopy',
    'visibility' => 1,
  );

  $export['block-kamenne_prodejny'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'kamenne_prodejny',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'node/34',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'penalty' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'penalty',
        'weight' => -20,
      ),
    ),
    'title' => 'Kamenné prodejny',
    'visibility' => 1,
  );

  $export['block-ke_stazeni'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'ke_stazeni',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'penalty' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'penalty',
        'weight' => -21,
      ),
    ),
    'title' => 'Ke stažení',
    'visibility' => 1,
  );

  $export['formblock-contact_site'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'contact_site',
    'module' => 'formblock',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'penalty' => array(
        'region' => 'footer_2',
        'status' => 1,
        'theme' => 'penalty',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['menu-menu-footer'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'menu-footer',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'penalty' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'penalty',
        'weight' => -18,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['menu-menu-footer-2'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'menu-footer-2',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'penalty' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'penalty',
        'weight' => -17,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['menu-menu-homepage-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'menu-homepage-menu',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'penalty' => array(
        'region' => 'sub_navigation',
        'status' => 1,
        'theme' => 'penalty',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['system-main'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'main',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'penalty' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'penalty',
        'weight' => -26,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-main-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'main-menu',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'penalty' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'penalty',
        'weight' => -18,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['views-novinky-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'novinky-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'penalty' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'penalty',
        'weight' => -23,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-products-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'products-block',
    'module' => 'views',
    'node_types' => array(
      0 => 'page',
    ),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'penalty' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'penalty',
        'weight' => -24,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-zarazeni-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'zarazeni-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'penalty' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'penalty',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-zarazeni-block_1'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'zarazeni-block_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'penalty' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'penalty',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-zarazeni-block_2'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'zarazeni-block_2',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'penalty' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'penalty',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-zarazeni-block_3'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'zarazeni-block_3',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'penalty' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'penalty',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-zarazeni-block_4'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'zarazeni-block_4',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'penalty' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'penalty',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-zarazeni-block_5'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'zarazeni-block_5',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'penalty' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'penalty',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-zarazeni-block_6'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'zarazeni-block_6',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'penalty' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'penalty',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
