<?php
/**
 * @file
 * menu_features.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function menu_features_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_podporujeme:podporujeme.
  $menu_links['main-menu_podporujeme:podporujeme'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'podporujeme',
    'router_path' => 'podporujeme',
    'link_title' => 'podporujeme',
    'options' => array(
      'identifier' => 'main-menu_podporujeme:podporujeme',
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'menu_attach_block' => array(
        'name' => '',
        'use_ajax' => 0,
        'no_drop' => 0,
        'dropped' => 0,
        'on_hover' => 0,
        'orientation' => 'horizontal',
        'mlid' => 517,
        'plid' => 0,
      ),
    ),
    'module' => 'system',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: main-menu_prodejci:node/34.
  $menu_links['main-menu_prodejci:node/34'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/34',
    'router_path' => 'node/%',
    'link_title' => 'prodejci',
    'options' => array(
      'identifier' => 'main-menu_prodejci:node/34',
      'menu_attach_block' => array(
        'name' => '',
        'use_ajax' => 0,
        'no_drop' => 0,
        'dropped' => 0,
        'on_hover' => 0,
        'orientation' => 'horizontal',
        'mlid' => 562,
        'plid' => 0,
      ),
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: menu-homepage-menu_certifikty:<front>.
  $menu_links['menu-homepage-menu_certifikty:<front>'] = array(
    'menu_name' => 'menu-homepage-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Certifikáty',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'fragment' => 'block-block-3',
      'menu_attach_block' => array(
        'name' => '',
        'use_ajax' => 0,
        'no_drop' => 0,
        'dropped' => 0,
        'on_hover' => 0,
        'orientation' => 'horizontal',
        'mlid' => 0,
        'plid' => 0,
      ),
      'identifier' => 'menu-homepage-menu_certifikty:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: menu-homepage-menu_novinky:<front>.
  $menu_links['menu-homepage-menu_novinky:<front>'] = array(
    'menu_name' => 'menu-homepage-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Novinky',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'fragment' => 'block-views-novinky-block',
      'menu_attach_block' => array(
        'name' => '',
        'use_ajax' => 0,
        'no_drop' => 0,
        'dropped' => 0,
        'on_hover' => 0,
        'orientation' => 'horizontal',
        'mlid' => 0,
        'plid' => 0,
      ),
      'identifier' => 'menu-homepage-menu_novinky:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-homepage-menu_o-ns:<front>.
  $menu_links['menu-homepage-menu_o-ns:<front>'] = array(
    'menu_name' => 'menu-homepage-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'O nás',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'fragment' => 'block-block-1',
      'menu_attach_block' => array(
        'name' => '',
        'use_ajax' => 0,
        'no_drop' => 0,
        'dropped' => 0,
        'on_hover' => 0,
        'orientation' => 'horizontal',
        'mlid' => 0,
        'plid' => 0,
      ),
      'identifier' => 'menu-homepage-menu_o-ns:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Certifikáty');
  t('Novinky');
  t('O nás');
  t('podporujeme');
  t('prodejci');

  return $menu_links;
}
