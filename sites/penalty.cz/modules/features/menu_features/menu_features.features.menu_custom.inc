<?php
/**
 * @file
 * menu_features.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function menu_features_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: main-menu.
  $menus['main-menu'] = array(
    'menu_name' => 'main-menu',
    'title' => 'Main menu',
    'description' => 'The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.',
  );
  // Exported menu: menu-homepage-menu.
  $menus['menu-homepage-menu'] = array(
    'menu_name' => 'menu-homepage-menu',
    'title' => 'Homepage Menu',
    'description' => '',
  );
  // Exported menu: menu-prodejci-menu.
  $menus['menu-prodejci-menu'] = array(
    'menu_name' => 'menu-prodejci-menu',
    'title' => 'Prodejci menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Homepage Menu');
  t('Main menu');
  t('Prodejci menu');
  t('The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.');

  return $menus;
}
