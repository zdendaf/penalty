<?php
/**
 * @file
 * support_content_type.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function support_content_type_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function support_content_type_node_info() {
  $items = array(
    'support' => array(
      'name' => t('Sponzoring'),
      'base' => 'node_content',
      'description' => t('Položka na stránce Sponzorujeme (klub, sportovec, svaz, atd.)'),
      'has_title' => '1',
      'title_label' => t('Název'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
