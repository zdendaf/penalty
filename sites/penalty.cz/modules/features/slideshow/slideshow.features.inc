<?php
/**
 * @file
 * slideshow.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function slideshow_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function slideshow_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function slideshow_node_info() {
  $items = array(
    'slide' => array(
      'name' => t('Slide'),
      'base' => 'node_content',
      'description' => t('Obrázek s textem, který se bude zobrazovat ve slideshow.'),
      'has_title' => '1',
      'title_label' => t('Označení'),
      'help' => t('Označení slidu. Nebude se zobrazovat ve slideshow, slouží pouze pro orientaci.'),
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
