<?php
/**
 * @file
 * slideshow.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function slideshow_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'slideshow';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Slideshow';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['use_more_text'] = 'více';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Použít';
  $handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Řadit dle';
  $handler->display->display_options['exposed_form']['options']['sort_asc_label'] = 'Vzestupně';
  $handler->display->display_options['exposed_form']['options']['sort_desc_label'] = 'Sestupně';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Vztah: Pole: Icon (field_image:fid) */
  $handler->display->display_options['relationships']['field_image_fid']['id'] = 'field_image_fid';
  $handler->display->display_options['relationships']['field_image_fid']['table'] = 'field_data_field_image';
  $handler->display->display_options['relationships']['field_image_fid']['field'] = 'field_image_fid';
  $handler->display->display_options['relationships']['field_image_fid']['required'] = TRUE;
  /* Pole: Obsah: Text před */
  $handler->display->display_options['fields']['field_text_p_ed']['id'] = 'field_text_p_ed';
  $handler->display->display_options['fields']['field_text_p_ed']['table'] = 'field_data_field_text_p_ed';
  $handler->display->display_options['fields']['field_text_p_ed']['field'] = 'field_text_p_ed';
  $handler->display->display_options['fields']['field_text_p_ed']['label'] = '';
  $handler->display->display_options['fields']['field_text_p_ed']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_text_p_ed']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_text_p_ed']['alter']['text'] = '<p data-caption-animate="fadeInUp" data-caption-delay="200">[field_text_p_ed]</p>';
  $handler->display->display_options['fields']['field_text_p_ed']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_text_p_ed']['element_default_classes'] = FALSE;
  /* Pole: Obsah: Titulek */
  $handler->display->display_options['fields']['field_titulek']['id'] = 'field_titulek';
  $handler->display->display_options['fields']['field_titulek']['table'] = 'field_data_field_titulek';
  $handler->display->display_options['fields']['field_titulek']['field'] = 'field_titulek';
  $handler->display->display_options['fields']['field_titulek']['label'] = '';
  $handler->display->display_options['fields']['field_titulek']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_titulek']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_titulek']['alter']['text'] = '<h2 data-caption-animate="fadeInDown">[field_titulek]</h2>';
  $handler->display->display_options['fields']['field_titulek']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_titulek']['element_default_classes'] = FALSE;
  /* Pole: Obsah: Text za */
  $handler->display->display_options['fields']['field_text_za']['id'] = 'field_text_za';
  $handler->display->display_options['fields']['field_text_za']['table'] = 'field_data_field_text_za';
  $handler->display->display_options['fields']['field_text_za']['field'] = 'field_text_za';
  $handler->display->display_options['fields']['field_text_za']['label'] = '';
  $handler->display->display_options['fields']['field_text_za']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_text_za']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_text_za']['alter']['text'] = '<p data-caption-animate="fadeInUp" data-caption-delay="200">[field_text_za]</p>';
  $handler->display->display_options['fields']['field_text_za']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_text_za']['element_default_classes'] = FALSE;
  /* Pole: Soubor: Cesta */
  $handler->display->display_options['fields']['uri']['id'] = 'uri';
  $handler->display->display_options['fields']['uri']['table'] = 'file_managed';
  $handler->display->display_options['fields']['uri']['field'] = 'uri';
  $handler->display->display_options['fields']['uri']['relationship'] = 'field_image_fid';
  $handler->display->display_options['fields']['uri']['label'] = '';
  $handler->display->display_options['fields']['uri']['exclude'] = TRUE;
  $handler->display->display_options['fields']['uri']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['uri']['file_download_path'] = TRUE;
  /* Pole: OUTPUT */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['ui_name'] = 'OUTPUT';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<div class="container clearfix">
<div class="slider-caption dark slider-caption-center">
[field_text_p_ed]
[field_titulek]
[field_text_za]
</div>
</div>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nothing']['element_default_classes'] = FALSE;
  /* Kritérium řazení: Obsah: Weight (field_weight) */
  $handler->display->display_options['sorts']['field_weight_value']['id'] = 'field_weight_value';
  $handler->display->display_options['sorts']['field_weight_value']['table'] = 'field_data_field_weight';
  $handler->display->display_options['sorts']['field_weight_value']['field'] = 'field_weight_value';
  /* Kritérium pro filtrování: Obsah: Vydáno */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Kritérium pro filtrování: Obsah: Typ */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'slide' => 'slide',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['block_description'] = 'Slideshow';
  $translatables['slideshow'] = array(
    t('Master'),
    t('více'),
    t('Použít'),
    t('Reset'),
    t('Řadit dle'),
    t('Vzestupně'),
    t('Sestupně'),
    t('image from field_image'),
    t('<p data-caption-animate="fadeInUp" data-caption-delay="200">[field_text_p_ed]</p>'),
    t('<h2 data-caption-animate="fadeInDown">[field_titulek]</h2>'),
    t('<p data-caption-animate="fadeInUp" data-caption-delay="200">[field_text_za]</p>'),
    t('<div class="container clearfix">
<div class="slider-caption dark slider-caption-center">
[field_text_p_ed]
[field_titulek]
[field_text_za]
</div>
</div>'),
    t('Block'),
    t('Slideshow'),
  );
  $export['slideshow'] = $view;

  return $export;
}
