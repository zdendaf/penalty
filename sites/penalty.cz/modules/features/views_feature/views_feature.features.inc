<?php
/**
 * @file
 * views_feature.features.inc
 */

/**
 * Implements hook_views_api().
 */
function views_feature_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function views_feature_image_default_styles() {
  $styles = array();

  // Exported image style: news.
  $styles['news'] = array(
    'label' => 'news',
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 500,
          'height' => 280,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: portfolio.
  $styles['portfolio'] = array(
    'label' => 'Portfolio',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 400,
          'height' => 300,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
