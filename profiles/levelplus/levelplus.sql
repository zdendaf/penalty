-- MySQL dump 10.13  Distrib 5.5.53, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: levelplus
-- ------------------------------------------------------
-- Server version	5.5.53-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accesslog`
--

DROP TABLE IF EXISTS `accesslog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accesslog` (
  `aid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key: Unique accesslog ID.',
  `sid` varchar(128) NOT NULL DEFAULT '' COMMENT 'Browser session ID of user that visited page.',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title of page visited.',
  `path` varchar(255) DEFAULT NULL COMMENT 'Internal path to page visited (relative to Drupal root.)',
  `url` text COMMENT 'Referrer URI.',
  `hostname` varchar(128) DEFAULT NULL COMMENT 'Hostname of user that visited the page.',
  `uid` int(10) unsigned DEFAULT '0' COMMENT 'User users.uid that visited the page.',
  `timer` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Time in milliseconds that the page took to load.',
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Timestamp of when the page was visited.',
  PRIMARY KEY (`aid`),
  KEY `accesslog_timestamp` (`timestamp`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores site access information for statistics.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accesslog`
--

LOCK TABLES `accesslog` WRITE;
/*!40000 ALTER TABLE `accesslog` DISABLE KEYS */;
/*!40000 ALTER TABLE `accesslog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `actions`
--

DROP TABLE IF EXISTS `actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actions` (
  `aid` varchar(255) NOT NULL DEFAULT '0' COMMENT 'Primary Key: Unique actions ID.',
  `type` varchar(32) NOT NULL DEFAULT '' COMMENT 'The object that that action acts on (node, user, comment, system or custom types.)',
  `callback` varchar(255) NOT NULL DEFAULT '' COMMENT 'The callback function that executes when the action runs.',
  `parameters` longblob NOT NULL COMMENT 'Parameters to be passed to the callback function.',
  `label` varchar(255) NOT NULL DEFAULT '0' COMMENT 'Label of the action.',
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores action information.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actions`
--

LOCK TABLES `actions` WRITE;
/*!40000 ALTER TABLE `actions` DISABLE KEYS */;
INSERT INTO `actions` VALUES ('comment_publish_action','comment','comment_publish_action','','Publish comment'),('comment_save_action','comment','comment_save_action','','Save comment'),('comment_unpublish_action','comment','comment_unpublish_action','','Unpublish comment'),('node_make_sticky_action','node','node_make_sticky_action','','Make content sticky'),('node_make_unsticky_action','node','node_make_unsticky_action','','Make content unsticky'),('node_promote_action','node','node_promote_action','','Promote content to front page'),('node_publish_action','node','node_publish_action','','Publish content'),('node_save_action','node','node_save_action','','Save content'),('node_unpromote_action','node','node_unpromote_action','','Remove content from front page'),('node_unpublish_action','node','node_unpublish_action','','Unpublish content'),('system_block_ip_action','user','system_block_ip_action','','Ban IP address of current user'),('user_block_user_action','user','user_block_user_action','','Block current user');
/*!40000 ALTER TABLE `actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `block`
--

DROP TABLE IF EXISTS `block`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `block` (
  `bid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key: Unique block ID.',
  `module` varchar(64) NOT NULL DEFAULT '' COMMENT 'The module from which the block originates; for example, ’user’ for the Who’s Online block, and ’block’ for any custom blocks.',
  `delta` varchar(32) NOT NULL DEFAULT '0' COMMENT 'Unique ID for block within a module.',
  `theme` varchar(64) NOT NULL DEFAULT '' COMMENT 'The theme under which the block settings apply.',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Block enabled status. (1 = enabled, 0 = disabled)',
  `weight` int(11) NOT NULL DEFAULT '0' COMMENT 'Block weight within region.',
  `region` varchar(64) NOT NULL DEFAULT '' COMMENT 'Theme region within which the block is set.',
  `custom` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Flag to indicate how users may control visibility of the block. (0 = Users cannot control, 1 = On by default, but can be hidden, 2 = Hidden by default, but can be shown)',
  `visibility` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Flag to indicate how to show blocks on pages. (0 = Show on all pages except listed pages, 1 = Show only on listed pages, 2 = Use custom PHP code to determine visibility)',
  `pages` text NOT NULL COMMENT 'Contents of the "Pages" block; contains either a list of paths on which to include/exclude the block or PHP code, depending on "visibility" setting.',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT 'Custom title for the block. (Empty string will use block default title, <none> will remove the title, text will cause block to use specified title.)',
  `cache` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Binary flag to indicate block cache mode. (-2: Custom cache, -1: Do not cache, 1: Cache per role, 2: Cache per user, 4: Cache per page, 8: Block cache global) See DRUPAL_CACHE_* constants in ../includes/common.inc for more detailed information.',
  PRIMARY KEY (`bid`),
  UNIQUE KEY `tmd` (`theme`,`module`,`delta`),
  KEY `list` (`theme`,`status`,`region`,`weight`,`module`)
) ENGINE=InnoDB AUTO_INCREMENT=786 DEFAULT CHARSET=utf8 COMMENT='Stores block settings, such as region and visibility...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `block`
--

LOCK TABLES `block` WRITE;
/*!40000 ALTER TABLE `block` DISABLE KEYS */;
INSERT INTO `block` VALUES (1,'system','main','bartik',1,0,'content',0,0,'','',-1),(2,'search','form','bartik',1,-1,'sidebar_first',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\npricing-tables*','',-1),(3,'node','recent','seven',1,10,'dashboard_inactive',0,0,'','',-1),(4,'user','login','bartik',1,0,'sidebar_first',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\nalternative-blog*\r\npricing-tables*','',-1),(5,'system','navigation','bartik',1,0,'sidebar_first',0,0,'node/2\r\nnode/8\r\nshowcase*\r\n<front>\r\ncontact-us','',-1),(6,'system','powered-by','bartik',1,10,'footer',0,0,'','',-1),(7,'system','help','bartik',1,0,'help',0,0,'','',-1),(8,'system','main','seven',1,0,'content',0,0,'','',-1),(9,'system','help','seven',1,0,'help',0,0,'','',-1),(10,'user','login','seven',1,10,'content',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\nalternative-blog*\r\npricing-tables*','',-1),(11,'user','new','seven',1,0,'dashboard_inactive',0,0,'','',-1),(12,'search','form','seven',1,-10,'dashboard_inactive',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\npricing-tables*','',-1),(13,'comment','recent','bartik',0,0,'-1',0,1,'node/11','',1),(14,'node','syndicate','bartik',0,0,'-1',0,0,'','',-1),(15,'node','recent','bartik',0,0,'-1',0,0,'','',1),(16,'shortcut','shortcuts','bartik',0,0,'-1',0,0,'','',-1),(17,'system','management','bartik',0,0,'-1',0,0,'','',-1),(18,'system','user-menu','bartik',0,0,'-1',0,0,'node/2\r\nnode/8\r\nshowcase*','',-1),(19,'system','main-menu','bartik',0,0,'-1',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\npricing-tables*','',-1),(20,'user','new','bartik',0,0,'-1',0,0,'','',1),(21,'user','online','bartik',0,0,'-1',0,0,'','',-1),(22,'comment','recent','seven',1,0,'dashboard_inactive',0,1,'node/11','',1),(23,'node','syndicate','seven',0,0,'-1',0,0,'','',-1),(24,'shortcut','shortcuts','seven',0,0,'-1',0,0,'','',-1),(25,'system','powered-by','seven',0,10,'-1',0,0,'','',-1),(26,'system','navigation','seven',0,0,'-1',0,0,'node/2\r\nnode/8\r\nshowcase*\r\n<front>\r\ncontact-us','',-1),(27,'system','management','seven',0,0,'-1',0,0,'','',-1),(28,'system','user-menu','seven',0,0,'-1',0,0,'node/2\r\nnode/8\r\nshowcase*','',-1),(29,'system','main-menu','seven',0,0,'-1',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\npricing-tables*','',-1),(30,'user','online','seven',1,0,'dashboard_inactive',0,0,'','',-1),(31,'comment','recent','successinc',0,0,'-1',0,1,'node/11','',1),(32,'node','recent','successinc',0,0,'-1',0,0,'','',1),(33,'node','syndicate','successinc',0,0,'-1',0,0,'','',-1),(34,'search','form','successinc',1,-16,'header_top_right',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\npricing-tables*','',-1),(35,'shortcut','shortcuts','successinc',0,0,'-1',0,0,'','',-1),(36,'system','help','successinc',1,0,'help',0,0,'','',-1),(37,'system','main','successinc',1,-16,'content',0,0,'','',-1),(38,'system','main-menu','successinc',1,-15,'sidebar_first',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\npricing-tables*','',-1),(39,'system','management','successinc',0,0,'-1',0,0,'','',-1),(40,'system','navigation','successinc',1,-11,'sidebar_first',0,0,'node/2\r\nnode/8\r\nshowcase*\r\n<front>\r\ncontact-us','',-1),(41,'system','powered-by','successinc',0,-8,'-1',0,0,'','',-1),(42,'system','user-menu','successinc',0,-13,'-1',0,0,'node/2\r\nnode/8\r\nshowcase*','',-1),(43,'user','login','successinc',1,-10,'sidebar_first',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\nalternative-blog*\r\npricing-tables*','',-1),(44,'user','new','successinc',0,0,'-1',0,0,'','',1),(45,'user','online','successinc',0,0,'-1',0,0,'','',-1),(70,'block','9','bartik',0,0,'-1',0,0,'','',-1),(71,'block','9','seven',0,0,'-1',0,0,'','',-1),(72,'block','9','successinc',1,-15,'sub_footer_first',0,0,'','',-1),(73,'menu','menu-footer-bottom-menu','successinc',1,-14,'sub_footer_second',0,0,'','<none>',-1),(74,'menu','menu-footer-bottom-menu','bartik',0,0,'-1',0,0,'','<none>',-1),(75,'menu','menu-footer-bottom-menu','seven',0,0,'-1',0,0,'','<none>',-1),(82,'blog','recent','bartik',0,0,'-1',0,0,'','',1),(83,'blog','recent','seven',1,0,'dashboard_inactive',0,0,'','',1),(84,'blog','recent','successinc',0,0,'-1',0,0,'','',1),(85,'views','testimonials-block_1','successinc',1,-16,'sidebar_first',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nshowcase*\r\nservices*\r\nproducts*\r\ntestimonials\r\ncontact-us*\r\n<front>','',-1),(86,'views','testimonials-block_1','bartik',0,0,'-1',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nshowcase*\r\nservices*\r\nproducts*\r\ntestimonials\r\ncontact-us*\r\n<front>','',-1),(87,'views','testimonials-block_1','seven',0,0,'-1',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nshowcase*\r\nservices*\r\nproducts*\r\ntestimonials\r\ncontact-us*\r\n<front>','',-1),(88,'superfish','1','bartik',0,0,'-1',0,0,'node/58','<none>',-1),(89,'superfish','2','bartik',0,0,'-1',0,1,'node/58','<none>',-1),(90,'superfish','3','bartik',0,0,'-1',0,0,'','',-1),(91,'superfish','4','bartik',0,0,'-1',0,0,'','',-1),(92,'superfish','1','seven',0,0,'-1',0,0,'node/58','<none>',-1),(93,'superfish','2','seven',0,0,'-1',0,1,'node/58','<none>',-1),(94,'superfish','3','seven',0,0,'-1',0,0,'','',-1),(95,'superfish','4','seven',0,0,'-1',0,0,'','',-1),(96,'superfish','1','successinc',1,0,'navigation',0,0,'node/58','<none>',-1),(97,'superfish','2','successinc',0,0,'-1',0,1,'node/58','<none>',-1),(98,'superfish','3','successinc',0,0,'-1',0,0,'','',-1),(99,'superfish','4','successinc',0,0,'-1',0,0,'','',-1),(100,'views','promoted_posts-block_1','successinc',1,-16,'promoted',0,1,'<front>\r\nnode/33\r\nnode/41','Featured products & Services',-1),(101,'views','promoted_posts-block_1','bartik',0,0,'-1',0,1,'<front>\r\nnode/33\r\nnode/41','Featured products & Services',-1),(102,'views','promoted_posts-block_1','seven',0,0,'-1',0,1,'<front>\r\nnode/33\r\nnode/41','Featured products & Services',-1),(103,'views','slideshow-block_1','successinc',1,0,'banner',0,1,'<front>','',-1),(104,'views','slideshow-block_1','bartik',0,0,'-1',0,1,'<front>','',-1),(105,'views','slideshow-block_1','seven',0,0,'-1',0,1,'<front>','',-1),(106,'views','latest_products-block_1','successinc',1,-14,'sidebar_first',0,0,'node/2\r\nnode/8\r\nshowcase*\r\nproducts\r\ncontact*','',-1),(107,'views','latest_services-block_1','successinc',1,-13,'sidebar_first',0,0,'','',-1),(108,'views','latest_posts-block_1','successinc',1,-12,'sidebar_first',0,0,'','',-1),(109,'views','latest_products-block_1','bartik',0,0,'-1',0,0,'node/2\r\nnode/8\r\nshowcase*\r\nproducts\r\ncontact*','',-1),(110,'views','latest_services-block_1','bartik',0,0,'-1',0,0,'','',-1),(111,'views','latest_posts-block_1','bartik',0,0,'-1',0,0,'','',-1),(112,'views','latest_products-block_1','seven',0,0,'-1',0,0,'node/2\r\nnode/8\r\nshowcase*\r\nproducts\r\ncontact*','',-1),(113,'views','latest_services-block_1','seven',0,0,'-1',0,0,'','',-1),(114,'views','latest_posts-block_1','seven',0,0,'-1',0,0,'','',-1),(115,'webform','client-block-22','successinc',1,-9,'footer_third',0,1,'contact-us','<none>',-1),(116,'webform','client-block-22','bartik',0,0,'-1',0,1,'contact-us','<none>',-1),(117,'webform','client-block-22','seven',0,0,'-1',0,1,'contact-us','<none>',-1),(123,'block','9','socialstyle',1,-15,'sub_footer_left',0,0,'','',-1),(124,'blog','recent','socialstyle',0,-12,'-1',0,0,'','',1),(125,'comment','recent','socialstyle',0,-11,'-1',0,1,'node/11','',1),(126,'menu','menu-footer-bottom-menu','socialstyle',1,-14,'sub_footer_right',0,0,'','<none>',-1),(128,'node','recent','socialstyle',0,-10,'-1',0,0,'','',1),(129,'node','syndicate','socialstyle',0,-8,'-1',0,0,'','',-1),(130,'search','form','socialstyle',1,-15,'header_top_right',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\npricing-tables*','',-1),(131,'shortcut','shortcuts','socialstyle',0,-9,'-1',0,0,'','',-1),(132,'superfish','1','socialstyle',1,0,'navigation',0,0,'node/58','<none>',-1),(133,'superfish','2','socialstyle',0,0,'-1',0,1,'node/58','<none>',-1),(134,'superfish','3','socialstyle',0,0,'-1',0,0,'','',-1),(135,'superfish','4','socialstyle',0,0,'-1',0,0,'','',-1),(136,'system','help','socialstyle',1,0,'help',0,0,'','',-1),(137,'system','main','socialstyle',1,-15,'content',0,0,'','',-1),(138,'system','main-menu','socialstyle',1,-14,'sidebar_first',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\npricing-tables*','',-1),(139,'system','management','socialstyle',0,-14,'-1',0,0,'','',-1),(140,'system','navigation','socialstyle',1,-10,'sidebar_first',0,0,'node/2\r\nnode/8\r\nshowcase*\r\n<front>\r\ncontact-us','',-1),(141,'system','powered-by','socialstyle',0,-13,'-1',0,0,'','',-1),(142,'system','user-menu','socialstyle',0,-7,'-1',0,0,'node/2\r\nnode/8\r\nshowcase*','',-1),(143,'user','login','socialstyle',1,-9,'sidebar_first',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\nalternative-blog*\r\npricing-tables*','',-1),(144,'user','new','socialstyle',0,-6,'-1',0,0,'','',1),(145,'user','online','socialstyle',0,-5,'-1',0,0,'','',-1),(146,'views','latest_posts-block_1','socialstyle',1,-8,'footer_second',0,0,'','',-1),(147,'views','latest_products-block_1','socialstyle',1,-14,'sidebar_first',0,0,'node/2\r\nnode/8\r\nshowcase*\r\nproducts\r\ncontact*','',-1),(148,'views','latest_services-block_1','socialstyle',1,-13,'sidebar_first',0,0,'','',-1),(149,'views','promoted_posts-block_1','socialstyle',1,-15,'promoted',0,1,'<front>\r\nnode/33\r\nnode/41','Featured products & Services',-1),(150,'views','slideshow-block_1','socialstyle',1,0,'banner',0,1,'<front>','',-1),(151,'views','testimonials-block_1','socialstyle',1,-15,'sidebar_first',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nshowcase*\r\nservices*\r\nproducts*\r\ntestimonials\r\ncontact-us*\r\n<front>','',-1),(152,'webform','client-block-22','socialstyle',1,-15,'footer_third',0,1,'contact-us','<none>',-1),(153,'views','client_showcase-block_1','socialstyle',1,-12,'sidebar_first',0,0,'node/2\r\nnode/8\r\nshowcase*\r\ncontact*\r\n<front>','',-1),(154,'views','client_showcase-block_1','bartik',0,0,'',0,0,'node/2\r\nnode/8\r\nshowcase*\r\ncontact*\r\n<front>','',-1),(155,'views','client_showcase-block_1','seven',0,0,'',0,0,'node/2\r\nnode/8\r\nshowcase*\r\ncontact*\r\n<front>','',-1),(161,'block','9','marketsquare',1,0,'sub_footer_left',0,0,'','',-1),(163,'blog','recent','marketsquare',0,0,'-1',0,0,'','',1),(164,'comment','recent','marketsquare',0,0,'-1',0,1,'node/11','',1),(165,'menu','menu-footer-bottom-menu','marketsquare',1,0,'sub_footer_right',0,0,'','<none>',-1),(167,'node','syndicate','marketsquare',0,-16,'-1',0,0,'','',-1),(168,'node','recent','marketsquare',0,0,'-1',0,0,'','',1),(169,'shortcut','shortcuts','marketsquare',0,0,'-1',0,0,'','',-1),(170,'superfish','1','marketsquare',1,-17,'navigation',0,0,'node/58','<none>',-1),(171,'superfish','2','marketsquare',0,0,'-1',0,1,'node/58','<none>',-1),(172,'superfish','3','marketsquare',0,0,'-1',0,0,'','',-1),(173,'superfish','4','marketsquare',0,0,'-1',0,0,'','',-1),(174,'system','management','marketsquare',0,0,'-1',0,0,'','',-1),(175,'system','user-menu','marketsquare',0,0,'-1',0,0,'node/2\r\nnode/8\r\nshowcase*','',-1),(176,'system','main-menu','marketsquare',0,0,'-1',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\npricing-tables*','',-1),(177,'user','new','marketsquare',0,0,'-1',0,0,'','',1),(178,'user','online','marketsquare',0,0,'-1',0,0,'','',-1),(179,'views','testimonials-block_1','marketsquare',1,-14,'sidebar_first',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nshowcase*\r\nservices*\r\nproducts*\r\ntestimonials\r\ncontact-us*\r\n<front>','',-1),(180,'views','promoted_posts-block_1','marketsquare',1,-14,'featured',0,1,'<front>\r\nnode/33\r\nnode/41','Featured products & Services',-1),(181,'views','slideshow-block_1','marketsquare',1,-15,'banner',0,1,'<front>','',-1),(182,'views','latest_products-block_1','marketsquare',0,0,'-1',0,0,'node/2\r\nnode/8\r\nshowcase*\r\nproducts\r\ncontact*','',-1),(183,'views','latest_services-block_1','marketsquare',1,-13,'sidebar_first',0,0,'','',-1),(184,'views','latest_posts-block_1','marketsquare',0,0,'-1',0,0,'','',-1),(185,'views','client_showcase-block_1','marketsquare',1,-12,'sidebar_first',0,0,'node/2\r\nnode/8\r\nshowcase*\r\ncontact*\r\n<front>','',-1),(186,'webform','client-block-22','marketsquare',1,-15,'footer_left',0,1,'contact-us','<none>',-1),(187,'system','main','marketsquare',1,-14,'content',0,0,'','',-1),(188,'system','powered-by','marketsquare',0,10,'-1',0,0,'','',-1),(189,'system','help','marketsquare',1,0,'help',0,0,'','',-1),(190,'search','form','marketsquare',0,-16,'-1',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\npricing-tables*','',-1),(191,'system','navigation','marketsquare',1,-11,'sidebar_first',0,0,'node/2\r\nnode/8\r\nshowcase*\r\n<front>\r\ncontact-us','',-1),(192,'user','login','marketsquare',0,-16,'-1',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\nalternative-blog*\r\npricing-tables*','',-1),(196,'block','12','bartik',0,0,'-1',0,1,'contact-us-alt*','<none>',-1),(197,'block','12','marketsquare',1,-16,'sidebar_first',0,1,'contact-us-alt*','<none>',-1),(198,'block','12','seven',0,0,'-1',0,1,'contact-us-alt*','<none>',-1),(199,'views','tweets-block','bartik',0,0,'-1',0,0,'','',-1),(200,'views','tweets-block','marketsquare',0,0,'-1',0,0,'','',-1),(201,'views','tweets-block','seven',0,0,'-1',0,0,'','',-1),(202,'views','mt_tweets-block','marketsquare',1,-15,'footer_right',0,0,'','',-1),(203,'views','mt_tweets-block','bartik',0,0,'-1',0,0,'','',-1),(204,'views','mt_tweets-block','seven',0,0,'-1',0,0,'','',-1),(215,'block','9','mobileplus',1,0,'sub_footer_left',0,0,'','',-1),(217,'block','12','mobileplus',1,-16,'sidebar_second',0,1,'contact-us-alt*','<none>',-1),(221,'blog','recent','mobileplus',0,0,'-1',0,0,'','',1),(222,'comment','recent','mobileplus',0,0,'-1',0,1,'node/11','',1),(223,'menu','menu-footer-bottom-menu','mobileplus',1,0,'sub_footer_right',0,0,'','<none>',-1),(225,'node','syndicate','mobileplus',0,0,'-1',0,0,'','',-1),(226,'node','recent','mobileplus',0,0,'-1',0,0,'','',1),(227,'shortcut','shortcuts','mobileplus',0,0,'-1',0,0,'','',-1),(228,'superfish','1','mobileplus',1,0,'navigation',0,0,'node/58','<none>',-1),(229,'superfish','2','mobileplus',0,0,'-1',0,1,'node/58','<none>',-1),(230,'superfish','3','mobileplus',0,0,'-1',0,0,'','',-1),(231,'superfish','4','mobileplus',0,0,'-1',0,0,'','',-1),(232,'system','management','mobileplus',0,0,'-1',0,0,'','',-1),(233,'system','user-menu','mobileplus',0,0,'-1',0,0,'node/2\r\nnode/8\r\nshowcase*','',-1),(234,'system','main-menu','mobileplus',1,-17,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\npricing-tables*','',-1),(235,'user','new','mobileplus',0,0,'-1',0,0,'','',1),(236,'user','online','mobileplus',0,0,'-1',0,0,'','',-1),(237,'views','testimonials-block_1','mobileplus',1,-15,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nshowcase*\r\nservices*\r\nproducts*\r\ntestimonials\r\ncontact-us*\r\n<front>','',-1),(238,'views','promoted_posts-block_1','mobileplus',1,0,'featured',0,1,'<front>\r\nnode/33\r\nnode/41','Featured products & Services',-1),(239,'views','slideshow-block_1','mobileplus',1,0,'banner',0,1,'<front>','',-1),(240,'views','latest_products-block_1','mobileplus',0,0,'-1',0,0,'node/2\r\nnode/8\r\nshowcase*\r\nproducts\r\ncontact*','',-1),(241,'views','latest_services-block_1','mobileplus',1,0,'footer_first',0,0,'','',-1),(242,'views','latest_posts-block_1','mobileplus',0,0,'-1',0,0,'','',-1),(243,'views','client_showcase-block_1','mobileplus',0,0,'',0,0,'node/2\r\nnode/8\r\nshowcase*\r\ncontact*\r\n<front>','',-1),(244,'views','tweets-block','mobileplus',0,0,'-1',0,0,'','',-1),(245,'views','mt_tweets-block','mobileplus',1,0,'footer_third',0,0,'','',-1),(246,'webform','client-block-22','mobileplus',1,0,'footer_fourth',0,1,'contact-us','<none>',-1),(247,'system','main','mobileplus',1,-17,'content',0,0,'','',-1),(248,'system','powered-by','mobileplus',0,10,'-1',0,0,'','',-1),(249,'system','help','mobileplus',1,0,'help',0,0,'','',-1),(250,'search','form','mobileplus',0,-1,'-1',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\npricing-tables*','',-1),(251,'system','navigation','mobileplus',0,-16,'-1',0,0,'node/2\r\nnode/8\r\nshowcase*\r\n<front>\r\ncontact-us','',-1),(252,'user','login','mobileplus',1,-14,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\nalternative-blog*\r\npricing-tables*','',-1),(253,'block','16','bartik',0,0,'-1',0,0,'','Follow us',-1),(254,'block','16','mobileplus',1,0,'footer_right',0,0,'','Follow us',-1),(255,'block','16','seven',0,0,'-1',0,0,'','Follow us',-1),(256,'block','17','bartik',0,0,'-1',0,0,'','Get in touch',-1),(257,'block','17','mobileplus',1,0,'footer_second',0,0,'','Get in touch',-1),(258,'block','17','seven',0,0,'-1',0,0,'','Get in touch',-1),(262,'views','testimonials-block_2','bartik',0,0,'-1',0,1,'<front>\r\nnode/33\r\nnode/34\r\nnode/35\r\nnode/41\r\nnode/42\r\nnode/43\r\nnode/44','Happy Clients',-1),(263,'views','testimonials-block_2','mobileplus',1,0,'highlighted_bottom_content',0,1,'<front>\r\nnode/33\r\nnode/34\r\nnode/35\r\nnode/41\r\nnode/42\r\nnode/43\r\nnode/44','Happy Clients',-1),(264,'views','testimonials-block_2','seven',0,0,'-1',0,1,'<front>\r\nnode/33\r\nnode/34\r\nnode/35\r\nnode/41\r\nnode/42\r\nnode/43\r\nnode/44','Happy Clients',-1),(265,'views','services-block_1','bartik',0,0,'-1',0,0,'','',-1),(266,'views','services-block_1','mobileplus',1,0,'bottom_content',0,0,'','',-1),(267,'views','services-block_1','seven',0,0,'-1',0,0,'','',-1),(268,'block','19','bartik',0,0,'-1',0,0,'node/57\r\nnode/58','',-1),(269,'block','19','mobileplus',1,0,'search_area',0,0,'node/57\r\nnode/58','',-1),(270,'block','19','seven',0,0,'-1',0,0,'node/57\r\nnode/58','',-1),(277,'block','12','garland',1,-16,'sidebar_second',0,1,'contact-us-alt*','<none>',-1),(278,'block','16','garland',1,0,'sidebar_first',0,0,'','Follow us',-1),(279,'block','17','garland',1,0,'sidebar_first',0,0,'','Get in touch',-1),(281,'block','19','garland',1,0,'sidebar_first',0,0,'node/57\r\nnode/58','',-1),(284,'block','9','garland',1,0,'sidebar_first',0,0,'','',-1),(285,'blog','recent','garland',0,0,'-1',0,0,'','',1),(286,'comment','recent','garland',0,0,'-1',0,1,'node/11','',1),(287,'menu','menu-footer-bottom-menu','garland',1,0,'sidebar_first',0,0,'','<none>',-1),(289,'node','recent','garland',0,0,'-1',0,0,'','',1),(290,'node','syndicate','garland',0,0,'-1',0,0,'','',-1),(291,'search','form','garland',0,-1,'-1',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\npricing-tables*','',-1),(292,'shortcut','shortcuts','garland',0,0,'-1',0,0,'','',-1),(293,'superfish','1','garland',1,0,'sidebar_first',0,0,'node/58','<none>',-1),(294,'superfish','2','garland',0,0,'-1',0,1,'node/58','<none>',-1),(295,'superfish','3','garland',0,0,'-1',0,0,'','',-1),(296,'superfish','4','garland',0,0,'-1',0,0,'','',-1),(297,'system','help','garland',1,0,'help',0,0,'','',-1),(298,'system','main','garland',1,-17,'content',0,0,'','',-1),(299,'system','main-menu','garland',1,-17,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\npricing-tables*','',-1),(300,'system','management','garland',0,0,'-1',0,0,'','',-1),(301,'system','navigation','garland',0,-16,'-1',0,0,'node/2\r\nnode/8\r\nshowcase*\r\n<front>\r\ncontact-us','',-1),(302,'system','powered-by','garland',0,10,'-1',0,0,'','',-1),(303,'system','user-menu','garland',0,0,'-1',0,0,'node/2\r\nnode/8\r\nshowcase*','',-1),(304,'user','login','garland',1,-14,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\nalternative-blog*\r\npricing-tables*','',-1),(305,'user','new','garland',0,0,'-1',0,0,'','',1),(306,'user','online','garland',0,0,'-1',0,0,'','',-1),(307,'views','client_showcase-block_1','garland',0,0,'',0,0,'node/2\r\nnode/8\r\nshowcase*\r\ncontact*\r\n<front>','',-1),(308,'views','latest_posts-block_1','garland',0,0,'-1',0,0,'','',-1),(309,'views','latest_products-block_1','garland',0,0,'-1',0,0,'node/2\r\nnode/8\r\nshowcase*\r\nproducts\r\ncontact*','',-1),(310,'views','latest_services-block_1','garland',1,0,'sidebar_first',0,0,'','',-1),(311,'views','mt_tweets-block','garland',1,0,'sidebar_first',0,0,'','',-1),(312,'views','promoted_posts-block_1','garland',1,0,'sidebar_first',0,1,'<front>\r\nnode/33\r\nnode/41','Featured products & Services',-1),(313,'views','services-block_1','garland',1,0,'sidebar_first',0,0,'','',-1),(314,'views','slideshow-block_1','garland',1,0,'sidebar_first',0,1,'<front>','',-1),(315,'views','testimonials-block_1','garland',1,-15,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nshowcase*\r\nservices*\r\nproducts*\r\ntestimonials\r\ncontact-us*\r\n<front>','',-1),(316,'views','testimonials-block_2','garland',1,0,'sidebar_first',0,1,'<front>\r\nnode/33\r\nnode/34\r\nnode/35\r\nnode/41\r\nnode/42\r\nnode/43\r\nnode/44','Happy Clients',-1),(317,'views','tweets-block','garland',0,0,'-1',0,0,'','',-1),(318,'webform','client-block-22','garland',1,0,'sidebar_first',0,1,'contact-us','<none>',-1),(319,'block','12','finance',1,-17,'sidebar_second',0,1,'contact-us-alt*','<none>',-1),(320,'block','16','finance',1,0,'footer_top',0,0,'','Follow us',-1),(321,'block','17','finance',1,-19,'footer_first',0,0,'','Get in touch',-1),(323,'block','19','finance',1,0,'search_area',0,0,'node/57\r\nnode/58','',-1),(326,'block','9','finance',1,0,'sub_footer_left',0,0,'','',-1),(327,'blog','recent','finance',0,0,'-1',0,0,'','',1),(328,'comment','recent','finance',0,0,'-1',0,1,'node/11','',1),(329,'menu','menu-footer-bottom-menu','finance',1,0,'footer',0,0,'','<none>',-1),(331,'node','recent','finance',0,0,'-1',0,0,'','',1),(332,'node','syndicate','finance',0,0,'-1',0,0,'','',-1),(333,'search','form','finance',0,-1,'-1',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\npricing-tables*','',-1),(334,'shortcut','shortcuts','finance',0,0,'-1',0,0,'','',-1),(335,'superfish','1','finance',1,0,'navigation',0,0,'node/58','<none>',-1),(336,'superfish','2','finance',0,0,'-1',0,1,'node/58','<none>',-1),(337,'superfish','3','finance',0,0,'-1',0,0,'','',-1),(338,'superfish','4','finance',0,0,'-1',0,0,'','',-1),(339,'system','help','finance',1,0,'help',0,0,'','',-1),(340,'system','main','finance',1,0,'content',0,0,'','',-1),(341,'system','main-menu','finance',1,-20,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\npricing-tables*','',-1),(342,'system','management','finance',0,0,'-1',0,0,'','',-1),(343,'system','navigation','finance',0,0,'-1',0,0,'node/2\r\nnode/8\r\nshowcase*\r\n<front>\r\ncontact-us','',-1),(344,'system','powered-by','finance',0,10,'-1',0,0,'','',-1),(345,'system','user-menu','finance',0,0,'-1',0,0,'node/2\r\nnode/8\r\nshowcase*','',-1),(346,'user','login','finance',1,-18,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\nalternative-blog*\r\npricing-tables*','',-1),(347,'user','new','finance',0,0,'-1',0,0,'','',1),(348,'user','online','finance',0,0,'-1',0,0,'','',-1),(349,'views','client_showcase-block_1','finance',0,0,'',0,0,'node/2\r\nnode/8\r\nshowcase*\r\ncontact*\r\n<front>','',-1),(350,'views','latest_posts-block_1','finance',0,0,'-1',0,0,'','',-1),(351,'views','latest_products-block_1','finance',0,0,'-1',0,0,'node/2\r\nnode/8\r\nshowcase*\r\nproducts\r\ncontact*','',-1),(352,'views','latest_services-block_1','finance',1,0,'footer_first',0,0,'','',-1),(353,'views','mt_tweets-block','finance',1,0,'footer_second',0,0,'','',-1),(354,'views','promoted_posts-block_1','finance',1,-18,'highlighted',0,1,'<front>\r\nnode/33\r\nnode/41','Featured products & Services',-1),(355,'views','services-block_1','finance',0,0,'-1',0,0,'','',-1),(356,'views','slideshow-block_1','finance',1,0,'banner',0,1,'<front>','',-1),(357,'views','testimonials-block_1','finance',1,-15,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nshowcase*\r\nservices*\r\nproducts*\r\ntestimonials\r\ncontact-us*\r\n<front>','',-1),(358,'views','testimonials-block_2','finance',0,0,'-1',0,1,'<front>\r\nnode/33\r\nnode/34\r\nnode/35\r\nnode/41\r\nnode/42\r\nnode/43\r\nnode/44','Happy Clients',-1),(359,'views','tweets-block','finance',0,0,'-1',0,0,'','',-1),(360,'webform','client-block-22','finance',0,0,'-1',0,1,'contact-us','<none>',-1),(364,'views','popular_tags-block','finance',1,0,'footer_third',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*','',-1),(365,'views','popular_tags-block','bartik',0,0,'-1',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*','',-1),(366,'views','popular_tags-block','seven',0,0,'-1',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*','',-1),(367,'views','showcases-block_1','bartik',0,0,'-1',0,1,'<front>','',-1),(368,'views','showcases-block_1','finance',1,0,'bottom_content',0,1,'<front>','',-1),(369,'views','showcases-block_1','seven',0,0,'-1',0,1,'<front>','',-1),(376,'block','22','bartik',0,0,'-1',0,1,'contact-us-alternative','<none>',-1),(377,'block','22','finance',1,0,'banner',0,1,'contact-us-alternative','<none>',-1),(378,'block','22','seven',0,0,'-1',0,1,'contact-us-alternative','<none>',-1),(379,'views','services-block_2','bartik',0,0,'-1',0,0,'','',-1),(380,'views','services-block_2','finance',0,0,'-1',0,0,'','',-1),(381,'views','services-block_2','seven',0,0,'-1',0,0,'','',-1),(382,'quicktabs','sidebar_tabs','bartik',0,0,'-1',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\ntestimonials\r\ncontact-us*\r\nproducts*\r\nposts-col*\r\npricing-tables*\r\n<front>','<none>',-1),(383,'quicktabs','sidebar_tabs','finance',1,-16,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\ntestimonials\r\ncontact-us*\r\nproducts*\r\nposts-col*\r\npricing-tables*\r\n<front>','<none>',-1),(384,'quicktabs','sidebar_tabs','seven',0,0,'-1',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\ntestimonials\r\ncontact-us*\r\nproducts*\r\nposts-col*\r\npricing-tables*\r\n<front>','<none>',-1),(385,'block','23','bartik',0,0,'-1',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\ntestimonials\r\ncontact-us*\r\nposts-col*\r\npricing-tables*\r\n<front>','',-1),(386,'block','23','finance',1,-14,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\ntestimonials\r\ncontact-us*\r\nposts-col*\r\npricing-tables*\r\n<front>','',-1),(387,'block','23','seven',0,0,'-1',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\ntestimonials\r\ncontact-us*\r\nposts-col*\r\npricing-tables*\r\n<front>','',-1),(388,'block','24','bartik',0,0,'-1',0,0,'','Share this post',-1),(389,'block','24','finance',1,-19,'sidebar_second',0,0,'','Share this post',-1),(390,'block','24','seven',0,0,'-1',0,0,'','Share this post',-1),(391,'block','12','startupgrowth',1,-20,'sidebar_second',0,1,'contact-us-alt*','<none>',-1),(392,'block','16','startupgrowth',1,0,'footer_top',0,0,'','Follow us',-1),(393,'block','17','startupgrowth',1,0,'footer_first',0,0,'','Get in touch',-1),(394,'block','19','startupgrowth',1,0,'search_area',0,0,'node/57\r\nnode/58','',-1),(397,'block','22','startupgrowth',1,-20,'banner',0,1,'contact-us-alternative','<none>',-1),(398,'block','23','startupgrowth',1,-14,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\ntestimonials\r\ncontact-us*\r\nposts-col*\r\npricing-tables*\r\n<front>','',-1),(399,'block','24','startupgrowth',1,-18,'sidebar_second',0,0,'','Share this post',-1),(400,'block','9','startupgrowth',1,-15,'sub_footer_left',0,0,'','',-1),(401,'blog','recent','startupgrowth',0,-8,'-1',0,0,'','',1),(402,'comment','recent','startupgrowth',0,-7,'-1',0,1,'node/11','',1),(403,'menu','menu-footer-bottom-menu','startupgrowth',1,-15,'footer',0,0,'','<none>',-1),(406,'node','recent','startupgrowth',0,-6,'-1',0,0,'','',-1),(407,'node','syndicate','startupgrowth',0,-3,'-1',0,0,'','',-1),(408,'quicktabs','sidebar_tabs','startupgrowth',1,-17,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\ntestimonials\r\ncontact-us*\r\nproducts*\r\nposts-col*\r\npricing-tables*\r\n<front>','<none>',-1),(409,'search','form','startupgrowth',0,-5,'-1',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\npricing-tables*','',-1),(410,'shortcut','shortcuts','startupgrowth',0,-4,'-1',0,0,'','',-1),(411,'superfish','1','startupgrowth',1,0,'navigation',0,0,'node/58','<none>',-1),(412,'superfish','2','startupgrowth',0,0,'-1',0,1,'node/58','<none>',-1),(413,'superfish','3','startupgrowth',0,0,'-1',0,0,'','',-1),(414,'superfish','4','startupgrowth',0,0,'-1',0,0,'','',-1),(415,'system','help','startupgrowth',1,0,'help',0,0,'','',-1),(416,'system','main','startupgrowth',1,-19,'content',0,0,'','',-1),(417,'system','main-menu','startupgrowth',1,-19,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\npricing-tables*','',-1),(418,'system','management','startupgrowth',0,-12,'-1',0,0,'','',-1),(419,'system','navigation','startupgrowth',0,-11,'-1',0,0,'node/2\r\nnode/8\r\nshowcase*\r\n<front>\r\ncontact-us','',-1),(420,'system','powered-by','startupgrowth',0,-9,'-1',0,0,'','',-1),(421,'system','user-menu','startupgrowth',0,-2,'-1',0,0,'node/2\r\nnode/8\r\nshowcase*','',-1),(422,'user','login','startupgrowth',1,-16,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\nalternative-blog*\r\npricing-tables*','',-1),(423,'user','new','startupgrowth',0,0,'-1',0,0,'','',-1),(424,'user','online','startupgrowth',0,1,'-1',0,0,'','',-1),(425,'views','client_showcase-block_1','startupgrowth',0,0,'',0,0,'node/2\r\nnode/8\r\nshowcase*\r\ncontact*\r\n<front>','',-1),(426,'views','latest_posts-block_1','startupgrowth',0,-14,'-1',0,0,'','',-1),(427,'views','latest_products-block_1','startupgrowth',0,0,'-1',0,0,'node/2\r\nnode/8\r\nshowcase*\r\nproducts\r\ncontact*','',-1),(428,'views','latest_services-block_1','startupgrowth',0,0,'-1',0,0,'','',-1),(429,'views','mt_tweets-block','startupgrowth',1,0,'footer_second',0,0,'','',-1),(430,'views','popular_tags-block','startupgrowth',1,0,'footer_third',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*','',-1),(431,'views','promoted_posts-block_1','startupgrowth',1,-19,'highlighted',0,1,'<front>\r\nnode/33\r\nnode/41','Featured products & Services',-1),(432,'views','services-block_1','startupgrowth',0,-10,'-1',0,0,'','',-1),(433,'views','services-block_2','startupgrowth',0,-13,'-1',0,0,'','',-1),(434,'views','showcases-block_1','startupgrowth',1,-21,'bottom_content',0,1,'<front>','',-1),(435,'views','slideshow-block_1','startupgrowth',0,-19,'-1',0,1,'<front>','',-1),(436,'views','testimonials-block_1','startupgrowth',1,-15,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nshowcase*\r\nservices*\r\nproducts*\r\ntestimonials\r\ncontact-us*\r\n<front>','',-1),(437,'views','testimonials-block_2','startupgrowth',1,0,'highlighted_bottom_left',0,1,'<front>\r\nnode/33\r\nnode/34\r\nnode/35\r\nnode/41\r\nnode/42\r\nnode/43\r\nnode/44','Happy Clients',-1),(438,'views','tweets-block','startupgrowth',0,0,'-1',0,0,'','',-1),(439,'webform','client-block-22','startupgrowth',0,-1,'-1',0,1,'contact-us','<none>',-1),(440,'views','benefits-block','bartik',0,0,'-1',0,1,'<front>\r\nnode/33\r\nnode/41','WHY CHOOSE US',-1),(441,'views','benefits-block','seven',0,0,'-1',0,1,'<front>\r\nnode/33\r\nnode/41','WHY CHOOSE US',-1),(442,'views','benefits-block','startupgrowth',1,0,'top_content',0,1,'<front>\r\nnode/33\r\nnode/41','WHY CHOOSE US',-1),(443,'block','25','bartik',0,0,'-1',0,1,'node/11','Generic Block',-1),(444,'block','25','seven',0,0,'-1',0,1,'node/11','Generic Block',-1),(445,'block','25','startupgrowth',1,-20,'sidebar_first',0,1,'node/11','Generic Block',-1),(446,'views','slideshow_flexslider-block_1','bartik',0,0,'-1',0,1,'<front>','',-1),(447,'views','slideshow_flexslider-block_1','seven',0,0,'-1',0,1,'<front>','',-1),(448,'views','slideshow_flexslider-block_1','startupgrowth',0,-18,'-1',0,1,'<front>','',-1),(449,'views','slideshow_revolution-block_1','bartik',0,0,'-1',0,1,'<front>','',-1),(450,'views','slideshow_revolution-block_1','seven',0,0,'-1',0,1,'<front>','',-1),(451,'views','slideshow_revolution-block_1','startupgrowth',0,-19,'-1',0,1,'<front>','',-1),(452,'views','25f0b72cc335cb2ed715aced61388aef','bartik',0,0,'-1',0,0,'','',-1),(453,'views','25f0b72cc335cb2ed715aced61388aef','seven',0,0,'-1',0,0,'','',-1),(454,'views','25f0b72cc335cb2ed715aced61388aef','startupgrowth',1,0,'banner',0,0,'','',-1),(455,'views','slideshow_boxed-block_1','bartik',0,0,'-1',0,1,'node/33','',-1),(456,'views','slideshow_boxed-block_1','seven',0,0,'-1',0,1,'node/33','',-1),(457,'views','slideshow_boxed-block_1','startupgrowth',1,0,'banner',0,1,'node/33','',-1),(458,'views','slideshow_full-block_1','bartik',0,0,'-1',0,1,'<front>','',-1),(459,'views','slideshow_full-block_1','seven',0,0,'-1',0,1,'<front>','',-1),(460,'views','slideshow_full-block_1','startupgrowth',1,0,'banner',0,1,'<front>','',-1),(461,'block','12','corporateplus',1,-28,'sidebar_second',0,1,'contact-us-alt*','<none>',-1),(462,'block','16','corporateplus',1,0,'header_top_right',0,0,'','Follow us',-1),(463,'block','17','corporateplus',1,0,'footer_first',0,0,'','Get in touch',-1),(464,'block','19','corporateplus',1,0,'search_area',0,0,'node/57\r\nnode/58','',-1),(466,'block','22','corporateplus',1,-29,'banner',0,1,'contact-us-alternative','<none>',-1),(467,'block','23','corporateplus',1,-29,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\ntestimonials\r\ncontact-us*\r\nposts-col*\r\npricing-tables*\r\n<front>','',-1),(468,'block','24','corporateplus',1,-25,'sidebar_second',0,0,'','Share this post',-1),(469,'block','25','corporateplus',1,0,'sidebar_first',0,1,'node/11','Generic Block',-1),(470,'block','9','corporateplus',1,-21,'sub_footer_left',0,0,'','',-1),(471,'blog','recent','corporateplus',0,0,'-1',0,0,'','',1),(472,'comment','recent','corporateplus',0,0,'-1',0,1,'node/11','',1),(473,'menu','menu-footer-bottom-menu','corporateplus',1,-23,'sub_footer_left',0,0,'','<none>',-1),(476,'node','recent','corporateplus',0,0,'-1',0,0,'','',1),(477,'node','syndicate','corporateplus',0,0,'-1',0,0,'','',-1),(478,'quicktabs','sidebar_tabs','corporateplus',1,-24,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\ntestimonials\r\ncontact-us*\r\nproducts*\r\nposts-col*\r\npricing-tables*\r\n<front>','<none>',-1),(479,'search','form','corporateplus',1,-28,'sidebar_offcanvas',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\npricing-tables*','',-1),(480,'shortcut','shortcuts','corporateplus',0,0,'-1',0,0,'','',-1),(481,'superfish','1','corporateplus',1,0,'navigation',0,0,'node/58','<none>',-1),(482,'superfish','2','corporateplus',0,0,'-1',0,1,'node/58','<none>',-1),(483,'superfish','3','corporateplus',0,0,'-1',0,0,'','',-1),(484,'superfish','4','corporateplus',0,0,'-1',0,0,'','',-1),(485,'system','help','corporateplus',1,0,'help',0,0,'','',-1),(486,'system','main','corporateplus',1,-29,'content',0,0,'','',-1),(487,'system','main-menu','corporateplus',1,-27,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\npricing-tables*','',-1),(488,'system','management','corporateplus',0,0,'-1',0,0,'','',-1),(489,'system','navigation','corporateplus',0,0,'-1',0,0,'node/2\r\nnode/8\r\nshowcase*\r\n<front>\r\ncontact-us','',-1),(490,'system','powered-by','corporateplus',0,10,'-1',0,0,'','',-1),(491,'system','user-menu','corporateplus',0,0,'-1',0,0,'node/2\r\nnode/8\r\nshowcase*','',-1),(492,'user','login','corporateplus',0,0,'-1',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\nalternative-blog*\r\npricing-tables*','',-1),(493,'user','new','corporateplus',0,0,'-1',0,0,'','',1),(494,'user','online','corporateplus',0,0,'-1',0,0,'','',-1),(495,'views','25f0b72cc335cb2ed715aced61388aef','corporateplus',0,0,'-1',0,0,'','',-1),(496,'views','benefits-block','corporateplus',1,0,'top_content',0,1,'<front>\r\nnode/33\r\nnode/41','WHY CHOOSE US',-1),(497,'views','client_showcase-block_1','corporateplus',0,0,'',0,0,'node/2\r\nnode/8\r\nshowcase*\r\ncontact*\r\n<front>','',-1),(498,'views','latest_posts-block_1','corporateplus',0,0,'-1',0,0,'','',-1),(499,'views','latest_products-block_1','corporateplus',0,0,'-1',0,0,'node/2\r\nnode/8\r\nshowcase*\r\nproducts\r\ncontact*','',-1),(500,'views','latest_services-block_1','corporateplus',0,0,'-1',0,0,'','',-1),(501,'views','mt_tweets-block','corporateplus',1,0,'footer_second',0,0,'','',-1),(502,'views','popular_tags-block','corporateplus',1,-26,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*','',-1),(503,'views','promoted_posts-block_1','corporateplus',1,-22,'highlighted',0,1,'<front>\r\nnode/33\r\nnode/41','Featured products & Services',-1),(504,'views','services-block_1','corporateplus',0,0,'-1',0,0,'','',-1),(505,'views','services-block_2','corporateplus',0,0,'-1',0,0,'','',-1),(506,'views','showcases-block_1','corporateplus',1,-22,'bottom_content',0,1,'<front>','',-1),(507,'views','slideshow-block_1','corporateplus',0,0,'-1',0,1,'<front>','',-1),(508,'views','slideshow_boxed-block_1','corporateplus',1,-27,'banner',0,1,'node/33','',-1),(509,'views','slideshow_flexslider-block_1','corporateplus',0,0,'-1',0,1,'<front>','',-1),(510,'views','slideshow_full-block_1','corporateplus',1,-26,'banner',0,1,'<front>','',-1),(511,'views','slideshow_revolution-block_1','corporateplus',0,0,'-1',0,1,'<front>','',-1),(512,'views','testimonials-block_1','corporateplus',1,-23,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nshowcase*\r\nservices*\r\nproducts*\r\ntestimonials\r\ncontact-us*\r\n<front>','',-1),(513,'views','testimonials-block_2','corporateplus',1,-23,'bottom_content',0,1,'<front>\r\nnode/33\r\nnode/34\r\nnode/35\r\nnode/41\r\nnode/42\r\nnode/43\r\nnode/44','Happy Clients',-1),(514,'views','tweets-block','corporateplus',0,0,'-1',0,0,'','',-1),(515,'webform','client-block-22','corporateplus',0,0,'-1',0,1,'contact-us','<none>',-1),(516,'block','26','bartik',0,0,'-1',0,1,'<front>\r\nnode/41\r\nnode/33\r\nnode/57\r\nnode/58','',-1),(517,'block','26','corporateplus',1,0,'header_top_left',0,1,'<front>\r\nnode/41\r\nnode/33\r\nnode/57\r\nnode/58','',-1),(518,'block','26','seven',0,0,'-1',0,1,'<front>\r\nnode/41\r\nnode/33\r\nnode/57\r\nnode/58','',-1),(519,'block','27','bartik',0,0,'-1',0,0,'','Follow us on social media',-1),(520,'block','27','corporateplus',1,0,'footer_top_right',0,0,'','Follow us on social media',-1),(521,'block','27','seven',0,0,'-1',0,0,'','Follow us on social media',-1),(528,'block','30','bartik',0,0,'-1',0,0,'','',-1),(529,'block','30','corporateplus',1,-22,'sub_footer_left',0,0,'','',-1),(530,'block','30','seven',0,0,'-1',0,0,'','',-1),(531,'views','latest_posts-block_2','bartik',0,0,'-1',0,0,'','',-1),(532,'views','latest_posts-block_2','corporateplus',1,0,'footer_third',0,0,'','',-1),(533,'views','latest_posts-block_2','seven',0,0,'-1',0,0,'','',-1),(534,'views','photo_gallery-block','bartik',0,0,'-1',0,0,'','',-1),(535,'views','photo_gallery-block','corporateplus',1,0,'footer_fourth',0,0,'','',-1),(536,'views','photo_gallery-block','seven',0,0,'-1',0,0,'','',-1),(540,'views','team_members-block_1','bartik',0,0,'-1',0,1,'node/35','Meet Our Team',-1),(541,'views','team_members-block_1','corporateplus',1,-26,'featured_bottom',0,1,'node/35','Meet Our Team',-1),(542,'views','team_members-block_1','seven',0,0,'-1',0,1,'node/35','Meet Our Team',-1),(546,'views','internal_banner-block','bartik',0,0,'-1',0,0,'','<none>',-1),(547,'views','internal_banner-block','corporateplus',1,-28,'banner',0,0,'','<none>',-1),(548,'views','internal_banner-block','seven',0,0,'-1',0,0,'','<none>',-1),(549,'views','slideshow_full_width-block_1','bartik',0,0,'-1',0,1,'node/41','',-1),(550,'views','slideshow_full_width-block_1','corporateplus',1,-25,'banner',0,1,'node/41','',-1),(551,'views','slideshow_full_width-block_1','seven',0,0,'-1',0,1,'node/41','',-1),(552,'block','33','bartik',0,0,'-1',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\ntestimonials\r\ncontact-us*\r\n<front>\r\nposts-col*\r\npricing-tables*','Payment methods',-1),(553,'block','33','corporateplus',1,-22,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\ntestimonials\r\ncontact-us*\r\n<front>\r\nposts-col*\r\npricing-tables*','Payment methods',-1),(554,'block','33','seven',0,0,'-1',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\ntestimonials\r\ncontact-us*\r\n<front>\r\nposts-col*\r\npricing-tables*','Payment methods',-1),(558,'views','products-block_2','bartik',0,0,'-1',0,0,'','',-1),(559,'views','products-block_2','corporateplus',0,0,'-1',0,0,'','',-1),(560,'views','products-block_2','seven',0,0,'-1',0,0,'','',-1),(561,'block','35','bartik',0,0,'-1',0,0,'','Subscribe to our newsletter',-1),(562,'block','35','corporateplus',1,0,'footer_top_left',0,0,'','Subscribe to our newsletter',-1),(563,'block','35','seven',0,0,'-1',0,0,'','Subscribe to our newsletter',-1),(570,'block','12','levelplus',1,-28,'sidebar_second',0,1,'contact-us-alt*','<none>',-1),(571,'block','16','levelplus',1,0,'footer_third',0,0,'','Follow us',-1),(572,'block','17','levelplus',1,0,'footer_second',0,0,'','Get in touch',-1),(573,'block','19','levelplus',1,-36,'search_area',0,0,'node/57\r\nnode/58','',-1),(575,'block','22','levelplus',1,-28,'banner',0,1,'contact-us-alternative','<none>',-1),(576,'block','23','levelplus',1,-28,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\ntestimonials\r\ncontact-us*\r\nposts-col*\r\npricing-tables*\r\n<front>','',-1),(577,'block','24','levelplus',1,-25,'sidebar_second',0,0,'','Share this post',-1),(578,'block','25','levelplus',1,0,'sidebar_first',0,1,'node/11','Generic Block',-1),(579,'block','26','levelplus',1,0,'featured_bottom',0,1,'<front>\r\nnode/41\r\nnode/33\r\nnode/57\r\nnode/58','',-1),(580,'block','27','levelplus',1,0,'footer_top_second',0,0,'','Follow us on social media',-1),(583,'block','30','levelplus',1,-22,'footer_first',0,0,'','',-1),(586,'block','33','levelplus',1,-22,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\ntestimonials\r\ncontact-us*\r\n<front>\r\nposts-col*\r\npricing-tables*','Payment methods',-1),(588,'block','35','levelplus',1,0,'footer_top_first',0,0,'','Subscribe to our newsletter',-1),(590,'block','9','levelplus',1,-21,'sub_footer_first',0,0,'','',-1),(591,'blog','recent','levelplus',0,-17,'-1',0,0,'','',1),(592,'comment','recent','levelplus',0,-16,'-1',0,1,'node/11','',1),(593,'menu','menu-footer-bottom-menu','levelplus',1,-23,'footer',0,0,'','<none>',-1),(596,'node','recent','levelplus',0,-15,'-1',0,0,'','',1),(597,'node','syndicate','levelplus',0,-7,'-1',0,0,'','',-1),(598,'quicktabs','sidebar_tabs','levelplus',1,-24,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\ntestimonials\r\ncontact-us*\r\nproducts*\r\nposts-col*\r\npricing-tables*\r\n<front>','<none>',-1),(599,'search','form','levelplus',1,-28,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\npricing-tables*','',-1),(600,'shortcut','shortcuts','levelplus',0,-11,'-1',0,0,'','',-1),(601,'superfish','1','levelplus',1,-39,'header_third',0,0,'node/58','<none>',-1),(602,'superfish','2','levelplus',1,-40,'header_first',0,1,'node/58','<none>',-1),(603,'superfish','3','levelplus',0,0,'-1',0,0,'','',-1),(604,'superfish','4','levelplus',0,0,'-1',0,0,'','',-1),(605,'system','help','levelplus',1,0,'help',0,0,'','',-1),(606,'system','main','levelplus',1,-28,'content',0,0,'','',-1),(607,'system','main-menu','levelplus',1,-27,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\npricing-tables*','',-1),(608,'system','management','levelplus',0,-29,'-1',0,0,'','',-1),(609,'system','navigation','levelplus',0,-27,'-1',0,0,'node/2\r\nnode/8\r\nshowcase*\r\n<front>\r\ncontact-us','',-1),(610,'system','powered-by','levelplus',0,-22,'-1',0,0,'','',-1),(611,'system','user-menu','levelplus',0,-5,'-1',0,0,'node/2\r\nnode/8\r\nshowcase*','',-1),(612,'user','login','levelplus',1,0,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\nalternative-blog*\r\npricing-tables*','',-1),(613,'user','new','levelplus',0,-4,'-1',0,0,'','',1),(614,'user','online','levelplus',0,-3,'-1',0,0,'','',-1),(615,'views','25f0b72cc335cb2ed715aced61388aef','levelplus',0,0,'-1',0,0,'','',-1),(616,'views','benefits-block','levelplus',0,0,'-1',0,1,'<front>\r\nnode/33\r\nnode/41','WHY CHOOSE US',-1),(617,'views','client_showcase-block_1','levelplus',0,0,'',0,0,'node/2\r\nnode/8\r\nshowcase*\r\ncontact*\r\n<front>','',-1),(618,'views','internal_banner-block','levelplus',1,-28,'banner',0,0,'','<none>',-1),(619,'views','latest_posts-block_1','levelplus',0,0,'-1',0,0,'','',-1),(620,'views','latest_posts-block_2','levelplus',0,0,'-1',0,0,'','',-1),(621,'views','latest_products-block_1','levelplus',0,0,'-1',0,0,'node/2\r\nnode/8\r\nshowcase*\r\nproducts\r\ncontact*','',-1),(622,'views','latest_services-block_1','levelplus',0,0,'-1',0,0,'','',-1),(623,'views','mt_tweets-block','levelplus',1,0,'footer_second',0,0,'','',-1),(624,'views','photo_gallery-block','levelplus',0,0,'-1',0,0,'','',-1),(625,'views','popular_tags-block','levelplus',1,-26,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*','',-1),(626,'views','products-block_2','levelplus',0,0,'-1',0,0,'','',-1),(627,'views','promoted_posts-block_1','levelplus',1,-22,'highlighted',0,1,'<front>\r\nnode/33\r\nnode/41','Featured products & Services',-1),(628,'views','services-block_1','levelplus',0,0,'-1',0,0,'','',-1),(629,'views','services-block_2','levelplus',0,0,'-1',0,0,'','',-1),(630,'views','showcases-block_1','levelplus',0,-22,'-1',0,1,'<front>','',-1),(631,'views','slideshow-block_1','levelplus',0,0,'-1',0,1,'<front>','',-1),(632,'views','slideshow_boxed-block_1','levelplus',1,-27,'banner',0,1,'node/33','',-1),(633,'views','slideshow_flexslider-block_1','levelplus',0,0,'-1',0,1,'<front>','',-1),(634,'views','slideshow_full-block_1','levelplus',1,-26,'banner',0,1,'<front>','',-1),(635,'views','slideshow_full_width-block_1','levelplus',1,-25,'banner',0,1,'node/41','',-1),(636,'views','slideshow_revolution-block_1','levelplus',0,0,'-1',0,1,'<front>','',-1),(637,'views','team_members-block_1','levelplus',1,-26,'featured_bottom',0,1,'node/35','Meet Our Team',-1),(638,'views','testimonials-block_1','levelplus',1,-23,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nshowcase*\r\nservices*\r\nproducts*\r\ntestimonials\r\ncontact-us*\r\n<front>','',-1),(639,'views','testimonials-block_2','levelplus',0,-23,'-1',0,1,'<front>\r\nnode/33\r\nnode/34\r\nnode/35\r\nnode/41\r\nnode/42\r\nnode/43\r\nnode/44','Happy Clients',-1),(640,'views','tweets-block','levelplus',0,0,'-1',0,0,'','',-1),(641,'webform','client-block-22','levelplus',0,0,'-1',0,1,'contact-us','<none>',-1),(642,'block','37','bartik',0,0,'-1',0,0,'','HTML test block - Delete this',-1),(643,'block','37','levelplus',0,-33,'-1',0,0,'','HTML test block - Delete this',-1),(644,'block','37','seven',0,0,'-1',0,0,'','HTML test block - Delete this',-1),(645,'block','38','bartik',0,0,'-1',0,0,'','',-1),(646,'block','38','levelplus',1,0,'highlighted_bottom',0,0,'','',-1),(647,'block','38','seven',0,0,'-1',0,0,'','',-1),(648,'menu','menu-footer-menu','levelplus',1,0,'footer_fourth',0,0,'','',-1),(649,'menu','menu-footer-menu','bartik',0,0,'-1',0,0,'','',-1),(650,'menu','menu-footer-menu','seven',0,0,'-1',0,0,'','',-1),(651,'views','mt_benefits-block','bartik',0,0,'-1',0,1,'<front>\r\nnode/41\r\nnode/33\r\nnode/57\r\nnode/58','',-1),(652,'views','mt_internal_banner-block','bartik',0,0,'-1',0,0,'','',-1),(653,'views','mt_latest_posts-block_1','bartik',0,0,'-1',0,0,'','',-1),(654,'views','mt_latest_posts-block_2','bartik',0,0,'-1',0,0,'','',-1),(655,'views','mt_photo_gallery-block','bartik',0,0,'-1',0,0,'','',-1),(656,'views','mt_popular_tags-block','bartik',0,0,'-1',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\npricing-tables*','',-1),(657,'views','mt_products-block_2','bartik',0,0,'-1',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\ntestimonials\r\ncontact-us*\r\nproducts*\r\n<front>\r\nposts-col*\r\npricing-tables*','',-1),(658,'views','mt_promoted_posts-block_1','bartik',0,0,'-1',0,1,'<front>\r\nnode/33\r\nnode/41','',-1),(659,'views','mt_services-block_2','bartik',0,0,'-1',0,0,'','',-1),(660,'views','mt_showcases-block_1','bartik',0,0,'-1',0,1,'<front>','',-1),(661,'views','mt_slideshow_boxed-block_1','bartik',0,0,'-1',0,1,'node/33','',-1),(662,'views','mt_slideshow_full_width-block_1','bartik',0,0,'-1',0,1,'<front>\r\nnode/58','',-1),(663,'views','mt_team_members-block_1','bartik',0,0,'-1',0,1,'node/35','',-1),(664,'views','mt_testimonials-block_1','bartik',0,0,'-1',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\npricing-tables*','',-1),(665,'views','mt_testimonials-block_2','bartik',0,0,'-1',0,0,'','',-1),(666,'views','aa3f00e073c92c80741f86293eda5d80','bartik',0,0,'-1',0,1,'node/41\r\nnode/57','',-1),(667,'views','mt_benefits-block','levelplus',1,0,'content_top',0,1,'<front>\r\nnode/41\r\nnode/33\r\nnode/57\r\nnode/58','',-1),(668,'views','mt_internal_banner-block','levelplus',1,0,'banner',0,0,'','',-1),(669,'views','mt_latest_posts-block_1','levelplus',0,-31,'-1',0,0,'','',-1),(670,'views','mt_latest_posts-block_2','levelplus',0,-32,'-1',0,0,'','',-1),(671,'views','mt_photo_gallery-block','levelplus',0,-26,'-1',0,0,'','',-1),(672,'views','mt_popular_tags-block','levelplus',1,0,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\npricing-tables*','',-1),(673,'views','mt_products-block_2','levelplus',1,0,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\ntestimonials\r\ncontact-us*\r\nproducts*\r\n<front>\r\nposts-col*\r\npricing-tables*','',-1),(674,'views','mt_promoted_posts-block_1','levelplus',0,-18,'-1',0,1,'<front>\r\nnode/33\r\nnode/41','',-1),(675,'views','mt_services-block_2','levelplus',0,-30,'-1',0,0,'','',-1),(676,'views','mt_showcases-block_1','levelplus',0,-10,'-1',0,1,'<front>','',-1),(677,'views','mt_slideshow_boxed-block_1','levelplus',1,0,'banner',0,1,'node/33','',-1),(678,'views','mt_slideshow_full_width-block_1','levelplus',1,0,'banner',0,1,'<front>\r\nnode/58','',-1),(679,'views','mt_team_members-block_1','levelplus',1,0,'content_bottom_first',0,1,'node/35','',-1),(680,'views','mt_testimonials-block_1','levelplus',1,0,'sidebar_second',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\npricing-tables*','',-1),(681,'views','mt_testimonials-block_2','levelplus',0,-6,'-1',0,0,'','',-1),(682,'views','aa3f00e073c92c80741f86293eda5d80','levelplus',1,0,'banner',0,1,'node/41\r\nnode/57','',-1),(683,'views','mt_benefits-block','seven',0,0,'-1',0,1,'<front>\r\nnode/41\r\nnode/33\r\nnode/57\r\nnode/58','',-1),(684,'views','mt_internal_banner-block','seven',0,0,'-1',0,0,'','',-1),(685,'views','mt_latest_posts-block_1','seven',0,0,'-1',0,0,'','',-1),(686,'views','mt_latest_posts-block_2','seven',0,0,'-1',0,0,'','',-1),(687,'views','mt_photo_gallery-block','seven',0,0,'-1',0,0,'','',-1),(688,'views','mt_popular_tags-block','seven',0,0,'-1',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\npricing-tables*','',-1),(689,'views','mt_products-block_2','seven',0,0,'-1',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\ntestimonials\r\ncontact-us*\r\nproducts*\r\n<front>\r\nposts-col*\r\npricing-tables*','',-1),(690,'views','mt_promoted_posts-block_1','seven',0,0,'-1',0,1,'<front>\r\nnode/33\r\nnode/41','',-1),(691,'views','mt_services-block_2','seven',0,0,'-1',0,0,'','',-1),(692,'views','mt_showcases-block_1','seven',0,0,'-1',0,1,'<front>','',-1),(693,'views','mt_slideshow_boxed-block_1','seven',0,0,'-1',0,1,'node/33','',-1),(694,'views','mt_slideshow_full_width-block_1','seven',0,0,'-1',0,1,'<front>\r\nnode/58','',-1),(695,'views','mt_team_members-block_1','seven',0,0,'-1',0,1,'node/35','',-1),(696,'views','mt_testimonials-block_1','seven',0,0,'-1',0,0,'node/2\r\nnode/8\r\nnode/25\r\nnode/27\r\nnode/33\r\nnode/35\r\nnode/36\r\nnode/37\r\nnode/38\r\nnode/39\r\nnode/40\r\nnode/41\r\nnode/44\r\nnode/57\r\nnode/58\r\nshowcase*\r\nservices*\r\nproducts*\r\n<front>\r\ncontact-us*\r\nposts-col*\r\npricing-tables*','',-1),(697,'views','mt_testimonials-block_2','seven',0,0,'-1',0,0,'','',-1),(698,'views','aa3f00e073c92c80741f86293eda5d80','seven',0,0,'-1',0,1,'node/41\r\nnode/57','',-1),(699,'views','mt_services_masonry-block','bartik',0,0,'-1',0,1,'<front>','',-1),(700,'views','mt_services_masonry-block','levelplus',0,-12,'-1',0,1,'<front>','',-1),(701,'views','mt_services_masonry-block','seven',0,0,'-1',0,1,'<front>','',-1),(702,'views','mt_services_masonry-block_1','bartik',0,0,'-1',0,0,'','',-1),(703,'views','mt_services_masonry-block_2','bartik',0,0,'-1',0,0,'','',-1),(704,'views','mt_services_masonry-block_1','levelplus',0,-13,'-1',0,0,'','',-1),(705,'views','mt_services_masonry-block_2','levelplus',0,-14,'-1',0,0,'','',-1),(706,'views','mt_services_masonry-block_1','seven',0,0,'-1',0,0,'','',-1),(707,'views','mt_services_masonry-block_2','seven',0,0,'-1',0,0,'','',-1),(708,'views','a26e7917a24898060fdea541f9450e29','bartik',0,0,'-1',0,1,'<front>\r\nnode/41\r\nnode/33\r\nnode/57\r\nnode/58','',-1),(709,'views','73c8aff58320e4b69ece62d7b7b00718','bartik',0,0,'-1',0,0,'','',-1),(710,'views','571d9e8bb2857ab7f6b2a7793775feeb','bartik',0,0,'-1',0,0,'','',-1),(711,'views','a26e7917a24898060fdea541f9450e29','levelplus',1,0,'content_top',0,1,'<front>\r\nnode/41\r\nnode/33\r\nnode/57\r\nnode/58','',-1),(712,'views','73c8aff58320e4b69ece62d7b7b00718','levelplus',0,-8,'-1',0,0,'','',-1),(713,'views','571d9e8bb2857ab7f6b2a7793775feeb','levelplus',0,-9,'-1',0,0,'','',-1),(714,'views','a26e7917a24898060fdea541f9450e29','seven',0,0,'-1',0,1,'<front>\r\nnode/41\r\nnode/33\r\nnode/57\r\nnode/58','',-1),(715,'views','73c8aff58320e4b69ece62d7b7b00718','seven',0,0,'-1',0,0,'','',-1),(716,'views','571d9e8bb2857ab7f6b2a7793775feeb','seven',0,0,'-1',0,0,'','',-1),(717,'views','a637dbca9ccd602ac4d792ee4e0e0d77','bartik',0,0,'-1',0,0,'','',-1),(718,'views','dfb25af03546316b01f5050d3de25bab','bartik',0,0,'-1',0,0,'','',-1),(719,'views','40df75d14cc1bdcd2c14915afb87ffdf','bartik',0,0,'-1',0,0,'','',-1),(720,'views','a637dbca9ccd602ac4d792ee4e0e0d77','levelplus',0,-19,'-1',0,0,'','',-1),(721,'views','dfb25af03546316b01f5050d3de25bab','levelplus',0,-20,'-1',0,0,'','',-1),(722,'views','40df75d14cc1bdcd2c14915afb87ffdf','levelplus',0,-21,'-1',0,0,'','',-1),(723,'views','a637dbca9ccd602ac4d792ee4e0e0d77','seven',0,0,'-1',0,0,'','',-1),(724,'views','dfb25af03546316b01f5050d3de25bab','seven',0,0,'-1',0,0,'','',-1),(725,'views','40df75d14cc1bdcd2c14915afb87ffdf','seven',0,0,'-1',0,0,'','',-1),(726,'views','mt_services_masonry_alt-block','bartik',0,0,'-1',0,0,'','',-1),(727,'views','mt_services_masonry_alt-block_1','bartik',0,0,'-1',0,0,'','',-1),(728,'views','mt_services_masonry_alt-block_2','bartik',0,0,'-1',0,1,'<front>\r\nnode/41\r\nnode/33\r\nnode/57\r\nnode/58','',-1),(729,'views','mt_services_masonry_alt-block','levelplus',0,0,'-1',0,0,'','',-1),(730,'views','mt_services_masonry_alt-block_1','levelplus',0,0,'-1',0,0,'','',-1),(731,'views','mt_services_masonry_alt-block_2','levelplus',1,0,'content_top',0,1,'<front>\r\nnode/41\r\nnode/33\r\nnode/57\r\nnode/58','',-1),(732,'views','mt_services_masonry_alt-block','seven',0,0,'-1',0,0,'','',-1),(733,'views','mt_services_masonry_alt-block_1','seven',0,0,'-1',0,0,'','',-1),(734,'views','mt_services_masonry_alt-block_2','seven',0,0,'-1',0,1,'<front>\r\nnode/41\r\nnode/33\r\nnode/57\r\nnode/58','',-1),(735,'views','mt_posts_grid-block_1','levelplus',0,-24,'-1',0,0,'','',-1),(736,'views','mt_posts_grid-block_1','bartik',0,0,'-1',0,0,'','',-1),(737,'views','mt_posts_grid-block_1','seven',0,0,'-1',0,0,'','',-1),(738,'views','mt_posts_grid-block_2','bartik',0,0,'-1',0,1,'<front>\r\nnode/41\r\nnode/33\r\nnode/57\r\nnode/58','',-1),(739,'views','mt_posts_grid-block_3','bartik',0,0,'-1',0,0,'','',-1),(740,'views','mt_posts_grid-block_2','levelplus',1,0,'featured',0,1,'<front>\r\nnode/41\r\nnode/33\r\nnode/57\r\nnode/58','',-1),(741,'views','mt_posts_grid-block_3','levelplus',0,-23,'-1',0,0,'','',-1),(742,'views','mt_posts_grid-block_2','seven',0,0,'-1',0,1,'<front>\r\nnode/41\r\nnode/33\r\nnode/57\r\nnode/58','',-1),(743,'views','mt_posts_grid-block_3','seven',0,0,'-1',0,0,'','',-1),(744,'views','mt_testimonials-block_3','bartik',0,0,'-1',0,1,'<front>','',-1),(745,'views','mt_testimonials-block_3','levelplus',1,0,'highlighted',0,1,'<front>','',-1),(746,'views','mt_testimonials-block_3','seven',0,0,'-1',0,1,'<front>','',-1),(747,'views','mt_companies-block','bartik',0,0,'-1',0,1,'<front>','',-1),(748,'views','mt_companies-block','levelplus',1,0,'featured_bottom',0,1,'<front>','',-1),(749,'views','mt_companies-block','seven',0,0,'-1',0,1,'<front>','',-1),(750,'views','mt_alternative_blog-block_1','bartik',0,0,'-1',0,0,'','',-1),(751,'views','mt_alternative_blog-block_2','bartik',0,0,'-1',0,0,'','',-1),(752,'views','mt_alternative_blog-block_3','bartik',0,0,'-1',0,0,'','',-1),(753,'views','mt_alternative_blog-block_1','levelplus',0,0,'-1',0,0,'','',-1),(754,'views','mt_alternative_blog-block_2','levelplus',0,0,'-1',0,0,'','',-1),(755,'views','mt_alternative_blog-block_3','levelplus',0,-34,'-1',0,0,'','',-1),(756,'views','mt_alternative_blog-block_1','seven',0,0,'-1',0,0,'','',-1),(757,'views','mt_alternative_blog-block_2','seven',0,0,'-1',0,0,'','',-1),(758,'views','mt_alternative_blog-block_3','seven',0,0,'-1',0,0,'','',-1),(759,'statistics','popular','bartik',0,0,'-1',0,0,'','',-1),(760,'statistics','popular','levelplus',0,-25,'-1',0,0,'','',-1),(761,'statistics','popular','seven',0,0,'-1',0,0,'','',-1),(762,'block','39','bartik',0,0,'-1',0,1,'node/58','',-1),(763,'block','39','levelplus',1,-37,'header_third',0,1,'node/58','',-1),(764,'block','39','seven',0,0,'-1',0,1,'node/58','',-1),(765,'block','40','bartik',0,0,'-1',0,1,'contact-us','',-1),(766,'block','40','levelplus',1,0,'featured_top',0,1,'contact-us','',-1),(767,'block','40','seven',0,0,'-1',0,1,'contact-us','',-1),(768,'block','41','bartik',0,0,'-1',0,1,'contact-us','',-1),(769,'block','41','levelplus',1,0,'content_bottom_second',0,1,'contact-us','',-1),(770,'block','41','seven',0,0,'-1',0,1,'contact-us','',-1),(771,'webform','client-block-32','bartik',0,0,'-1',0,1,'contact-us','<none>',-1),(772,'webform','client-block-32','levelplus',1,0,'content_bottom_first',0,1,'contact-us','<none>',-1),(773,'webform','client-block-32','seven',0,0,'-1',0,1,'contact-us','<none>',-1),(774,'views','mt_pricing_tables-block','bartik',0,0,'-1',0,0,'','',-1),(775,'views','mt_pricing_tables-block','levelplus',0,0,'-1',0,0,'','',-1),(776,'views','mt_pricing_tables-block','seven',0,0,'-1',0,0,'','',-1),(777,'views','mt_pricing_tables-block_1','levelplus',1,0,'content_bottom_first',0,0,'','',-1),(778,'views','mt_pricing_tables-block_2','levelplus',0,0,'-1',0,0,'','',-1),(779,'views','mt_pricing_tables-block_1','bartik',0,0,'-1',0,0,'','',-1),(780,'views','mt_pricing_tables-block_2','bartik',0,0,'-1',0,0,'','',-1),(781,'views','mt_pricing_tables-block_1','seven',0,0,'-1',0,0,'','',-1),(782,'views','mt_pricing_tables-block_2','seven',0,0,'-1',0,0,'','',-1),(783,'block','42','bartik',0,0,'-1',0,1,'node/57','',-1),(784,'block','42','levelplus',1,0,'search_area',0,1,'node/57','',-1),(785,'block','42','seven',0,0,'-1',0,1,'node/57','',-1);
/*!40000 ALTER TABLE `block` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `block_custom`
--

DROP TABLE IF EXISTS `block_custom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `block_custom` (
  `bid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The block’s block.bid.',
  `body` longtext COMMENT 'Block contents.',
  `info` varchar(128) NOT NULL DEFAULT '' COMMENT 'Block description.',
  `format` varchar(255) DEFAULT NULL COMMENT 'The filter_format.format of the block body.',
  PRIMARY KEY (`bid`),
  UNIQUE KEY `info` (`info`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COMMENT='Stores contents of custom-made blocks.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `block_custom`
--

LOCK TABLES `block_custom` WRITE;
/*!40000 ALTER TABLE `block_custom` DISABLE KEYS */;
INSERT INTO `block_custom` VALUES (9,'Copyright &copy; 2016 Level+. All rights reserved.','Copyright','filtered_html'),(12,'<div class=\"contact-info\">\r\n<span class=\"icon\"><i class=\"fa fa-globe\"></i></span>\r\n<h3>Contact info</h3>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n<br>\r\n<ul>\r\n<li><i class=\"fa fa-map-marker\"><span class=\"sr-only\">address</span></i> 1600 Amphitheatre Parkway, <br> Mountain View, CA 94043</li>\r\n<li><i class=\"fa fa-phone\"><span class=\"sr-only\">phone</span></i> +321 123 456 7</li><li><i class=\"fa fa-print\"></i> +321 123 456 8 </li>\r\n<li><i class=\"fa fa-envelope-o\"><span class=\"sr-only\">email</span></i>For general enquiries: <br> info@levelplus.com <br> <br> For sales enquiries: <br> sales@levelplus.com< /li>\r\n</ul>\r\n</div>','Contact Info','full_html'),(16,'<ul class=\"social-bookmarks-with-text\">\r\n<li>\r\n<a href=\"http://www.facebook.com/morethan.just.themes/\"><i class=\"fa fa-facebook\"><span class=\"sr-only\">facebook</span></i>Facebook</a>\r\n</li>\r\n<li>\r\n<a href=\"http://twitter.com/morethanthemes/\"><i class=\"fa fa-twitter\"><span class=\"sr-only\">twitter</span></i>Twitter</a>\r\n</li>\r\n<li>\r\n<a href=\"http://www.linkedin.com/company/more-than-themes/\"><i class=\"fa fa-linkedin\"><span class=\"sr-only\">linkedin</span></i>LinkedIn</a>\r\n</li>\r\n</ul>','Follow us','full_html'),(17,'<p>+1-888-722-5121 <br> <a href=\"mailto:hello@levelplus.com\">hello@levelplus.com</a></p>\r\n<p>212 Queen Street West, Suite 157 <br> Toronto, ON M5V 287</p>','Get in touch','full_html'),(19,'<div class=\"nav-search style-2\">\r\n<?php if (module_exists(\'search\')): ?>\r\n<?php $block = module_invoke(\'search\', \'block_view\', \'search\'); print render($block);?>\r\n <?php endif; ?>\r\n</div>\r\n<div class=\"nav-button hidden-xs hidden-md hidden-sm\">\r\n<a href=\"#\" class=\"more\">Join Today</a>\r\n</div>','Navigation - Search bar (style 2)','php_code'),(22,'<!-- #map-canvas --> \r\n<div class=\"internal-banner-block\" id=\"map-canvas\">\r\n</div>\r\n<!-- EOF:#map-canvas -->','Google map - Banner','full_html'),(23,'<a href=\"<?php print base_path();?>node/26\" class=\"overlayed\"><span class=\"overlay\"><i class=\"fa fa-chevron-right\"></i></span><img src=\"<?php print base_path() . drupal_get_path(\'theme\', \'levelplus\') ;?>/images/block-img.jpg\" alt=\"\" /></a>\r\n<h3><a href=\"<?php print base_path();?>node/26\">sidebar block with image</a></h3>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do <a href=\"#\">eiusmod tempor</a> incididunt ut labore et dolore magna aliqua.</p>\r\n<p class=\"more-link\"><a href=\"<?php print base_path();?>node/26\">read more </a></p>','Block with image','php_code'),(24,'<?php\r\n$node = menu_get_object();\r\n$path = isset($_GET[\'q\']) ? $_GET[\'q\'] : \'<front>\';\r\nif (isset($node)) {\r\n$title =  $node->title;\r\n?>\r\n<ul class=\"social-media-info\">\r\n<li><a href=\"https://www.facebook.com/sharer/sharer.php?u=<?php print urlencode(url($path, array(\'absolute\' => TRUE)))?>&t=<?php print urlencode($node->title) ?>\" onclick=\"window.open(this.href, \'facebook-share\',\'width=580,height=296\');return false;\"><span><i class=\"fa fa-facebook\"></i></span></a></li>\r\n<li><a href=\"https://plus.google.com/share?url=<?php print urlencode(url($path, array(\'absolute\' => TRUE))) ?>\" onclick=\"window.open(this.href, \'google-plus-share\', \'width=490,height=530\');return false;\"><span><i class=\"fa fa-google-plus\"></i></span></a></li>\r\n<li><a href=\"http://twitter.com/share?text=<?php print urlencode($node->title) ?>&url=<?php print urlencode(url($path, array(\'absolute\' => TRUE))) ?>\" onclick=\"window.open(this.href, \'twitter-share\', \'width=550,height=235\');return false;\"><span><i class=\"fa fa-twitter\"></i></span></a></li>\r\n</ul>\r\n<?php } ?>','Share this post','php_code'),(25,'<p>Aliquam ullamcorper velit ligula, in aliquet sapien molestie semper. Praesent tellus nisi, porttitor eget ligula vitae, commodo vulputate dui. Donec varius arcu eget risus tincidunt, sed ultrices mi porta. Nulla faucibus sed nisi ac consequat. Cras at lectus arcu.</p>\r\n<p class=\"more-link\"><a href=\"<?php print base_path();?>node/26\">read more</a></p>','Generic Block','php_code'),(26,'<div class=\"row\">\r\n<div class=\"col-md-8\">\r\n<div class=\"bordered-block\">\r\n<h3>About us</h3>\r\n<p>During the long years of fruitful work we formed a team of highly qualified specialists, which is key to providing high-quality consulting services. Our goal is to professional help customers achieve the greatest possible business efficiency through the use of a complex of modern technologies. Long years of fruitful workwe formed a team of highly qualified specialists.</p>\r\n</div>\r\n</div>\r\n<div class=\"col-md-4\">\r\n<div class=\"row grid-gutter-0\">\r\n<div class=\"col-xs-9\">\r\n<h5>A Word from our Sales Director</h5>\r\n<p>Our goal is to professional help customers achieve the greatest possible business efficiency through technologies.</p>\r\n<p><a href=\"<?php print base_path();?>node/35\">More about us »</a></p>\r\n</div>\r\n<div class=\"col-xs-3 mt-30\">\r\n<img src=\"<?php print base_path() . drupal_get_path(\'theme\', \'levelplus\') ;?>/images/about.jpg\" alt=\"\" />\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n','About Us','php_code'),(27,'<ul class=\"social-bookmarks\">\r\n<li>\r\n<a href=\"http://www.facebook.com/morethan.just.themes/\"><i class=\"fa fa-facebook\"><span class=\"sr-only\">facebook</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"http://twitter.com/morethanthemes/\"><i class=\"fa fa-twitter\"><span class=\"sr-only\">twitter</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"https://plus.google.com/118354321025436191714/posts\"><i class=\"fa fa-google-plus\"><span class=\"sr-only\">google plus</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"http://www.linkedin.com/company/more-than-themes/\"><i class=\"fa fa-linkedin\"><span class=\"sr-only\">linkedin</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"http://www.youtube.com/morethanthemes/\"><i class=\"fa fa-youtube-play\"><span class=\"sr-only\">youtube</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"http://instagram.com/\"><i class=\"fa fa-instagram\"><span class=\"sr-only\">instagram</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"http://www.flickr.com/photos/morethanthemes/\"><i class=\"fa fa-flickr\"><span class=\"sr-only\">flickr</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"http://vimeo.com/morethanthemes\"><i class=\"fa fa-vimeo-square\"><span class=\"sr-only\">vimeo-square</span></i></a>\r\n</li>\r\n</ul>','Follow us on social media','full_html'),(30,'<div id=\"footer-logo-container\">\r\n<div id=\"footer-logo\" class=\"logo\">\r\n<img src=\"<?php print base_path() . drupal_get_path(\'theme\', \'levelplus\') ;?>/images/footer-logo.svg\" alt=\"\" onerror=\"this.onerror=null; this.src=\'<?php print base_path() . drupal_get_path(\'theme\', \'levelplus\') ;?>/images/footer-logo.png\'\"/>\r\n</div>\r\n<div id=\"footer-site-name\" class=\"site-name\">\r\nLEVEL+\r\n</div>\r\n<div id=\"footer-site-slogan\" class=\"site-slogan\">\r\nLEVEL UP YOUR BUSINESS\r\n</div>\r\n</div>','Footer Logo','php_code'),(33,'We accept all major credit cards, as well as a number of other ways to pay.\r\n<ul class=\"list-inline\">\r\n<li class=\"icon\"><i class=\"fa fa-cc-discover\"></i></li>\r\n<li class=\"icon\"><i class=\"fa fa-cc-mastercard\"></i></li>\r\n<li class=\"icon\"><i class=\"fa fa-cc-visa\"></i></li>\r\n<li class=\"icon\"><i class=\"fa fa-cc-amex\"></i></li>\r\n<li class=\"icon\"><i class=\"fa fa-cc-paypal\"></i></li>\r\n<li class=\"icon\"><i class=\"fa fa-cc-stripe\"></i></li>\r\n</ul>','Payment methods','full_html'),(35,'<form action=\"#\" class=\"container-inline subscribe-form\">\r\n<div class=\"form-item form-type-textfield\">\r\n<input type=\"text\" class=\"form-text\" name=\"subscribe\" value=\"Enter your email address\" onfocus=\"if (this.value == \'Enter your email address\') {this.value = \'\';}\" onblur=\"if (this.value == \'\') {this.value = \'Enter your email address\';}\" /></div>\r\n<div class=\"form-actions\">\r\n<input value=\"subscribe\" type=\"submit\" id=\"subscribe-submit\" name=\"subscribe\" class=\"form-submit button-dark\">\r\n</div>\r\n</form>','Subscribe to our newsletter','full_html'),(37,'<h1>H1 tag</h1>\r\n<h2>H2 tag</h2>\r\n<h3>H3 tag</h3>\r\n<h4>H4 tag</h4>\r\n<h5>H5 tag</h5>\r\n\r\n<h1><a href=\"#\">H1 tag link</a></h1>\r\n<h2><a href=\"#\">H2 tag link</a></h2>\r\n<h3><a href=\"#\">H3 tag link</a></h3>\r\n<h4><a href=\"#\">H4 tag link</a></h4>\r\n<h5><a href=\"#\">H5 tag link</a></h5>\r\n\r\n<ul>\r\n	<li>list item</li>\r\n	<li><a href=\"#\">lits item link</a></li>\r\n	<li>lits item</li>\r\n	<li>lits item</li>\r\n	<li>lits item</li>\r\n</ul>\r\n\r\n<p>Lorem ipsum dolor sit amet, <a href=\"#\">consectetur adipisicing elit</a>. Aut rem alias nulla officiis numquam porro, voluptatibus, consequuntur officia, magni assumenda ipsum dolorem nisi. Ratione sit impedit commodi deleniti, repudiandae hic.</p>\r\n\r\n<blockquote>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n<footer>Johnny Trulove <cite title=\"Source Title\">Сommercial director</cite></footer>\r\n</blockquote>\r\n\r\n<ul class=\"brands\">\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-apple\"></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-android\"></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-github\"></i></a>\r\n</li>                        \r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-windows\"></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-linux\"></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-skype\"></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-btc\"></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-css3\"></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-html5\"></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-bitbucket\"></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-maxcdn\"></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-dropbox\"></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-facebook\"></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-twitter\"></i></a>\r\n</li>\r\n</ul>\r\n\r\n<table><tbody><tr><th>Header 1</th>\r\n<th>Header 2</th>\r\n</tr><tr class=\"odd\"><td>row 1, cell 1</td>\r\n<td>row 1, cell 2</td>\r\n</tr><tr class=\"even\"><td>row 2, cell 1</td>\r\n<td>row 2, cell 2</td>\r\n</tr><tr class=\"odd\"><td>row 3, cell 1</td>\r\n<td>row 3, cell 2</td>\r\n</tr></tbody></table>\r\n\r\n<fieldset><legend>Account information</legend></fieldset>\r\n\r\n<!-- Nav tabs -->\r\n<ul class=\"nav nav-tabs\" role=\"tablist\">\r\n<li role=\"presentation\" class=\"active\"><a href=\"#home\" aria-controls=\"home\" role=\"tab\" data-toggle=\"tab\">Home</a></li>\r\n<li role=\"presentation\"><a href=\"#profile\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Profile</a></li>\r\n<li role=\"presentation\"><a href=\"#messages\" aria-controls=\"messages\" role=\"tab\" data-toggle=\"tab\">Messages</a></li>\r\n<li role=\"presentation\"><a href=\"#settings\" aria-controls=\"settings\" role=\"tab\" data-toggle=\"tab\">Settings</a></li>\r\n</ul>\r\n\r\n<!-- Tab panes -->\r\n<div class=\"tab-content\">\r\n<div role=\"tabpanel\" class=\"tab-pane fade in active\" id=\"home\"><p><strong>Home</strong> ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis, exercitationem, quaerat veniam repudiandae illo ratione eaque omnis obcaecati quidem distinctio sapiente quo assumenda amet cumque nobis nulla qui dolore autem.</p></div>\r\n<div role=\"tabpanel\" class=\"tab-pane fade\" id=\"profile\"><p><strong>Profile</strong> ipsum dolor sit amet, consectetur adipisicing elit. Ut odio facere minima porro quis. Maiores eius quibusdam et in corrupti necessitatibus consequatur debitis laudantium hic.</p></div>\r\n<div role=\"tabpanel\" class=\"tab-pane fade\" id=\"messages\"><p><strong>Messages</strong> ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis, optio error consectetur ullam porro eligendi mollitia odio numquam aut cumque. Sed, possimus recusandae itaque laboriosam nesciunt voluptates veniam aspernatur voluptate eaque ratione totam ipsa optio aliquam incidunt dolorum amet illum.</p></div>\r\n<div role=\"tabpanel\" class=\"tab-pane fade\" id=\"settings\"><p><strong>Settings</strong> ipsum dolor sit amet, consectetur adipisicing elit. Assumenda, repellendus, deserunt, magnam, neque est suscipit reiciendis a numquam odit alias placeat sapiente fugit culpa animi facere ratione iste? Nemo, itaque aperiam rem dignissimos dolorum quae!</p></div>\r\n</div>\r\n\r\n<div>\r\n<a href=\"#\" class=\"more\">Read more</a>\r\n</div>\r\n\r\n\r\n<form action=\"\" class=\"form-style-2\">\r\n<div class=\"form-item form-type-textfield\">\r\n<input type=\"text\" class=\"form-text\" name=\"subscribe\" value=\"Your email address\" onfocus=\"if (this.value == \'Your email address\') {this.value = \'\';}\" onblur=\"if (this.value == \'\') {this.value = \'Your email address\';}\">\r\n</div>\r\n<div class=\"form-actions\">\r\n<input value=\"Subscribe\" type=\"submit\" id=\"edit-submit\" name=\"subscribe\" class=\"form-submit\">\r\n</div>\r\n</form>\r\n<form action=\"\" class=\"container-inline form-style-2\">\r\n<div class=\"form-item form-type-textfield\">\r\n<input type=\"text\" class=\"form-text\" name=\"subscribe\" value=\"Inline form items\" onfocus=\"if (this.value == \'Your email address\') {this.value = \'\';}\" onblur=\"if (this.value == \'\') {this.value = \'Inline form items\';}\"></div>\r\n<div class=\"form-actions\">\r\n<input value=\"Subscribe\" type=\"submit\" id=\"edit-submit\" name=\"subscribe\" class=\"form-submit\">\r\n</div>\r\n</form>\r\n\r\n<h3>Select Box</h3>\r\n<select name=\"select_field\" class=\"form-select\"><option value=\"All\" selected=\"selected\">- Any -</option><option value=\"6\">Apartment</option><option value=\"7\">Family House</option><option value=\"8\">Office</option></select>','HTML test block','full_html'),(38,'<div class=\"call-to-action text-center\">\r\n<h2>Lets get started.</h2>\r\n<p>No credit card required, no paperwork—and you can cancel anytime.</p>\r\n<a href=\"#\" class=\"more button-dark\">Get started</a>\r\n<p class=\"small\">Not sure yet? <a href=\"#\">Give us a call</a> to discuss.</p>\r\n</div>\r\n','Call to action','full_html'),(39,'<div class=\"nav-button\">\r\n<a class=\"more\" href=\"#\">Join Today</a>\r\n</div>','Nav Button','full_html'),(40,'<!-- #map-canvas --> \r\n<div id=\"map-block\">\r\n<div class=\"collapse\" id=\"map-canvas-container\">\r\n<div id=\"map-canvas\"></div>\r\n</div>\r\n</div>\r\n<!-- EOF:#map-canvas -->','Google Map - Featured Top','full_html'),(41,'<div class=\"contact-info style-2\">\r\n<h2>Talk to Us</h2>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>\r\n<br>\r\n<ul>\r\n<li><i class=\"fa fa-phone\"><span class=\"sr-only\">phone</span></i> +321 123 456 7</li><li><i class=\"fa fa-print\"></i> +321 123 456 8 </li>\r\n<li><i class=\"fa fa-skype\"><span class=\"sr-only\">skype</span></i> <a href=\"#\">levelplus</a> on Skype</li>\r\n<li><div class=\"map-trigger\"><i class=\"fa fa-map-o\"><span class=\"sr-only\">map</span></i> <a  data-toggle=\"collapse\" href=\"javascript:showMap();\"  data-target=\"#map-canvas-container\" aria-expanded=\"false\" aria-controls=\"map-canvas-container\"></a></div></li>\r\n</ul>\r\n</div>','Contact Info - Content Bottom Second','full_html'),(42,'<div class=\"nav-search style-1\">\r\n<?php if (module_exists(\'search\')): ?>\r\n<?php $block = module_invoke(\'search\', \'block_view\', \'search\'); print render($block);?>\r\n <?php endif; ?>\r\n</div>','Navigation - Search bar (style 1)','php_code');
/*!40000 ALTER TABLE `block_custom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `block_node_type`
--

DROP TABLE IF EXISTS `block_node_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `block_node_type` (
  `module` varchar(64) NOT NULL COMMENT 'The block’s origin module, from block.module.',
  `delta` varchar(32) NOT NULL COMMENT 'The block’s unique delta within module, from block.delta.',
  `type` varchar(32) NOT NULL COMMENT 'The machine-readable name of this type from node_type.type.',
  PRIMARY KEY (`module`,`delta`,`type`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sets up display criteria for blocks based on content types';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `block_node_type`
--

LOCK TABLES `block_node_type` WRITE;
/*!40000 ALTER TABLE `block_node_type` DISABLE KEYS */;
INSERT INTO `block_node_type` VALUES ('block','24','article'),('system','navigation','article'),('user','login','article'),('block','24','blog'),('system','navigation','blog'),('user','login','blog'),('views','mt_pricing_tables-block','mt_service'),('views','mt_pricing_tables-block_1','mt_service'),('views','services-block_1','mt_service'),('user','login','page');
/*!40000 ALTER TABLE `block_node_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `cid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key: Unique comment ID.',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT 'The comment.cid to which this comment is a reply. If set to 0, this comment is not a reply to an existing comment.',
  `nid` int(11) NOT NULL DEFAULT '0' COMMENT 'The node.nid to which this comment is a reply.',
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT 'The users.uid who authored the comment. If set to 0, this comment was created by an anonymous user.',
  `subject` varchar(64) NOT NULL DEFAULT '' COMMENT 'The comment title.',
  `hostname` varchar(128) NOT NULL DEFAULT '' COMMENT 'The author’s host name.',
  `created` int(11) NOT NULL DEFAULT '0' COMMENT 'The time that the comment was created, as a Unix timestamp.',
  `changed` int(11) NOT NULL DEFAULT '0' COMMENT 'The time that the comment was last edited, as a Unix timestamp.',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT 'The published status of a comment. (0 = Not Published, 1 = Published)',
  `thread` varchar(255) NOT NULL COMMENT 'The vancode representation of the comment’s place in a thread.',
  `name` varchar(60) DEFAULT NULL COMMENT 'The comment author’s name. Uses users.name if the user is logged in, otherwise uses the value typed into the comment form.',
  `mail` varchar(64) DEFAULT NULL COMMENT 'The comment author’s e-mail address from the comment form, if user is anonymous, and the ’Anonymous users may/must leave their contact information’ setting is turned on.',
  `homepage` varchar(255) DEFAULT NULL COMMENT 'The comment author’s home page address from the comment form, if user is anonymous, and the ’Anonymous users may/must leave their contact information’ setting is turned on.',
  `language` varchar(12) NOT NULL DEFAULT '' COMMENT 'The languages.language of this comment.',
  PRIMARY KEY (`cid`),
  KEY `comment_status_pid` (`pid`,`status`),
  KEY `comment_num_new` (`nid`,`status`,`created`,`cid`,`thread`),
  KEY `comment_uid` (`uid`),
  KEY `comment_nid_language` (`nid`,`language`),
  KEY `comment_created` (`created`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='Stores comments and associated data.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` VALUES (1,0,15,1,'Nullam id dolor id nibh ultricies vehicula ut id','127.0.0.1',1354559307,1354559306,1,'01/','admin','','','und'),(2,1,15,1,'Nulla vitae elit libero, a pharetra augue','127.0.0.1',1354559334,1354559333,1,'01.00/','admin','','','und'),(3,0,14,1,'Phosfluorescently e-enable adaptive synergy for strategic','::1',1389961595,1389961594,1,'01/','admin','','','und'),(5,2,15,1,'Lorem ipsum dolor sit amet','::1',1390166289,1390166287,1,'01.00.00/','admin','','','und'),(6,0,19,1,'Proin ut viverra orci','::1',1394103066,1394103065,1,'01/','admin','','','und'),(7,0,21,1,'Great!','::1',1394106999,1394106998,1,'01/','admin','','','und'),(8,0,20,1,'Fusce eget nisl nibh.','::1',1394107063,1394107062,1,'01/','admin','','','und'),(9,0,20,1,'Duis sit amet cursus mauris','::1',1394107091,1394107090,1,'02/','admin','','','und'),(10,0,20,1,' Suspendisse gravida volutpat lacus, sed iaculis nunc hendrerit','::1',1394107118,1394107117,1,'03/','admin','','','und');
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact` (
  `cid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key: Unique category ID.',
  `category` varchar(255) NOT NULL DEFAULT '' COMMENT 'Category name.',
  `recipients` longtext NOT NULL COMMENT 'Comma-separated list of recipient e-mail addresses.',
  `reply` longtext NOT NULL COMMENT 'Text of the auto-reply message.',
  `weight` int(11) NOT NULL DEFAULT '0' COMMENT 'The category’s weight.',
  `selected` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Flag to indicate whether or not category is selected by default. (1 = Yes, 0 = No)',
  PRIMARY KEY (`cid`),
  UNIQUE KEY `category` (`category`),
  KEY `list` (`weight`,`category`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Contact form category settings.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` VALUES (1,'Website feedback','bill+successinc@morethanthemes.com','',0,1);
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_collection_item`
--

DROP TABLE IF EXISTS `field_collection_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_collection_item` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key: Unique field collection item ID.',
  `revision_id` int(11) NOT NULL COMMENT 'Default revision ID.',
  `field_name` varchar(32) NOT NULL COMMENT 'The name of the field on the host entity embedding this entity.',
  `archived` int(11) NOT NULL DEFAULT '0' COMMENT 'Boolean indicating whether the field collection item is archived.',
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COMMENT='Stores information about field collection items.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_collection_item`
--

LOCK TABLES `field_collection_item` WRITE;
/*!40000 ALTER TABLE `field_collection_item` DISABLE KEYS */;
INSERT INTO `field_collection_item` VALUES (26,26,'field_mt_highlight',0),(27,27,'field_mt_highlight',0),(28,28,'field_mt_highlight',0),(29,29,'field_mt_highlight',0),(30,30,'field_mt_highlight',0),(31,31,'field_mt_highlight',0),(32,32,'field_mt_special_feature',0),(33,33,'field_mt_special_feature',0),(34,34,'field_mt_special_feature',0),(36,36,'field_mt_special_feature',0),(37,37,'field_mt_special_feature',0),(38,38,'field_mt_special_feature',0),(39,39,'field_mt_standard_feature',0),(40,40,'field_mt_standard_feature',0),(41,41,'field_mt_standard_feature',0),(42,42,'field_mt_standard_feature',0),(43,43,'field_mt_standard_feature',0),(44,44,'field_mt_standard_feature',0),(45,45,'field_mt_standard_feature',0),(46,46,'field_mt_standard_feature',0),(47,47,'field_mt_highlight',0),(48,48,'field_mt_highlight',0),(49,49,'field_mt_highlight',0),(50,50,'field_mt_highlight',0),(51,51,'field_mt_highlight',0),(52,52,'field_mt_highlight',0),(53,53,'field_mt_highlight',0),(54,54,'field_mt_highlight',0),(55,55,'field_mt_highlight',0),(56,56,'field_mt_highlight',0);
/*!40000 ALTER TABLE `field_collection_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_collection_item_revision`
--

DROP TABLE IF EXISTS `field_collection_item_revision`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_collection_item_revision` (
  `revision_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key: Unique revision ID.',
  `item_id` int(11) NOT NULL COMMENT 'Field collection item ID.',
  PRIMARY KEY (`revision_id`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COMMENT='Stores revision information about field collection items.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_collection_item_revision`
--

LOCK TABLES `field_collection_item_revision` WRITE;
/*!40000 ALTER TABLE `field_collection_item_revision` DISABLE KEYS */;
INSERT INTO `field_collection_item_revision` VALUES (26,26),(27,27),(28,28),(29,29),(30,30),(31,31),(32,32),(33,33),(34,34),(36,36),(37,37),(38,38),(39,39),(40,40),(41,41),(42,42),(43,43),(44,44),(45,45),(46,46),(47,47),(48,48),(49,49),(50,50),(51,51),(52,52),(53,53),(54,54),(55,55),(56,56);
/*!40000 ALTER TABLE `field_collection_item_revision` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_config`
--

DROP TABLE IF EXISTS `field_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier for a field',
  `field_name` varchar(32) NOT NULL COMMENT 'The name of this field. Non-deleted field names are unique, but multiple deleted fields can have the same name.',
  `type` varchar(128) NOT NULL COMMENT 'The type of this field.',
  `module` varchar(128) NOT NULL DEFAULT '' COMMENT 'The module that implements the field type.',
  `active` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Boolean indicating whether the module that implements the field type is enabled.',
  `storage_type` varchar(128) NOT NULL COMMENT 'The storage backend for the field.',
  `storage_module` varchar(128) NOT NULL DEFAULT '' COMMENT 'The module that implements the storage backend.',
  `storage_active` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Boolean indicating whether the module that implements the storage backend is enabled.',
  `locked` tinyint(4) NOT NULL DEFAULT '0' COMMENT '@TODO',
  `data` longblob NOT NULL COMMENT 'Serialized data containing the field properties that do not warrant a dedicated column.',
  `cardinality` tinyint(4) NOT NULL DEFAULT '0',
  `translatable` tinyint(4) NOT NULL DEFAULT '0',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `field_name` (`field_name`),
  KEY `active` (`active`),
  KEY `storage_active` (`storage_active`),
  KEY `deleted` (`deleted`),
  KEY `module` (`module`),
  KEY `storage_module` (`storage_module`),
  KEY `type` (`type`),
  KEY `storage_type` (`storage_type`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_config`
--

LOCK TABLES `field_config` WRITE;
/*!40000 ALTER TABLE `field_config` DISABLE KEYS */;
INSERT INTO `field_config` VALUES (1,'comment_body','text_long','text',1,'field_sql_storage','field_sql_storage',1,0,'a:6:{s:12:\"entity_types\";a:1:{i:0;s:7:\"comment\";}s:12:\"translatable\";b:0;s:8:\"settings\";a:0:{}s:7:\"storage\";a:4:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";i:1;}s:12:\"foreign keys\";a:1:{s:6:\"format\";a:2:{s:5:\"table\";s:13:\"filter_format\";s:7:\"columns\";a:1:{s:6:\"format\";s:6:\"format\";}}}s:7:\"indexes\";a:1:{s:6:\"format\";a:1:{i:0;s:6:\"format\";}}}',1,0,0),(2,'body','text_with_summary','text',1,'field_sql_storage','field_sql_storage',1,0,'a:6:{s:12:\"entity_types\";a:1:{i:0;s:4:\"node\";}s:12:\"translatable\";b:0;s:8:\"settings\";a:0:{}s:7:\"storage\";a:4:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";i:1;}s:12:\"foreign keys\";a:1:{s:6:\"format\";a:2:{s:5:\"table\";s:13:\"filter_format\";s:7:\"columns\";a:1:{s:6:\"format\";s:6:\"format\";}}}s:7:\"indexes\";a:1:{s:6:\"format\";a:1:{i:0;s:6:\"format\";}}}',1,0,0),(3,'field_tags','taxonomy_term_reference','taxonomy',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:8:\"settings\";a:1:{s:14:\"allowed_values\";a:1:{i:0;a:2:{s:10:\"vocabulary\";s:4:\"tags\";s:6:\"parent\";i:0;}}}s:12:\"entity_types\";a:0:{}s:12:\"translatable\";s:1:\"0\";s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:21:\"field_data_field_tags\";a:1:{s:3:\"tid\";s:14:\"field_tags_tid\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:25:\"field_revision_field_tags\";a:1:{s:3:\"tid\";s:14:\"field_tags_tid\";}}}}}s:12:\"foreign keys\";a:1:{s:3:\"tid\";a:2:{s:5:\"table\";s:18:\"taxonomy_term_data\";s:7:\"columns\";a:1:{s:3:\"tid\";s:3:\"tid\";}}}s:7:\"indexes\";a:1:{s:3:\"tid\";a:1:{i:0;s:3:\"tid\";}}s:2:\"id\";s:1:\"3\";}',-1,0,0),(4,'field_image','image','image',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:7:\"indexes\";a:1:{s:3:\"fid\";a:1:{i:0;s:3:\"fid\";}}s:8:\"settings\";a:2:{s:10:\"uri_scheme\";s:6:\"public\";s:13:\"default_image\";i:0;}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:22:\"field_data_field_image\";a:5:{s:3:\"fid\";s:15:\"field_image_fid\";s:3:\"alt\";s:15:\"field_image_alt\";s:5:\"title\";s:17:\"field_image_title\";s:5:\"width\";s:17:\"field_image_width\";s:6:\"height\";s:18:\"field_image_height\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:26:\"field_revision_field_image\";a:5:{s:3:\"fid\";s:15:\"field_image_fid\";s:3:\"alt\";s:15:\"field_image_alt\";s:5:\"title\";s:17:\"field_image_title\";s:5:\"width\";s:17:\"field_image_width\";s:6:\"height\";s:18:\"field_image_height\";}}}}}s:12:\"entity_types\";a:0:{}s:12:\"translatable\";s:1:\"0\";s:12:\"foreign keys\";a:1:{s:3:\"fid\";a:2:{s:5:\"table\";s:12:\"file_managed\";s:7:\"columns\";a:1:{s:3:\"fid\";s:3:\"fid\";}}}s:2:\"id\";s:1:\"4\";}',-1,0,0),(30,'field_mt_subheader_body','text_with_summary','text',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:0:{}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:34:\"field_data_field_mt_subheader_body\";a:3:{s:5:\"value\";s:29:\"field_mt_subheader_body_value\";s:7:\"summary\";s:31:\"field_mt_subheader_body_summary\";s:6:\"format\";s:30:\"field_mt_subheader_body_format\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:38:\"field_revision_field_mt_subheader_body\";a:3:{s:5:\"value\";s:29:\"field_mt_subheader_body_value\";s:7:\"summary\";s:31:\"field_mt_subheader_body_summary\";s:6:\"format\";s:30:\"field_mt_subheader_body_format\";}}}}}s:12:\"foreign keys\";a:1:{s:6:\"format\";a:2:{s:5:\"table\";s:13:\"filter_format\";s:7:\"columns\";a:1:{s:6:\"format\";s:6:\"format\";}}}s:7:\"indexes\";a:1:{s:6:\"format\";a:1:{i:0;s:6:\"format\";}}s:2:\"id\";s:2:\"30\";}',1,0,0),(31,'field_mt_font_awesome_classes','text','text',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:1:{s:10:\"max_length\";s:3:\"255\";}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:40:\"field_data_field_mt_font_awesome_classes\";a:2:{s:5:\"value\";s:35:\"field_mt_font_awesome_classes_value\";s:6:\"format\";s:36:\"field_mt_font_awesome_classes_format\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:44:\"field_revision_field_mt_font_awesome_classes\";a:2:{s:5:\"value\";s:35:\"field_mt_font_awesome_classes_value\";s:6:\"format\";s:36:\"field_mt_font_awesome_classes_format\";}}}}}s:12:\"foreign keys\";a:1:{s:6:\"format\";a:2:{s:5:\"table\";s:13:\"filter_format\";s:7:\"columns\";a:1:{s:6:\"format\";s:6:\"format\";}}}s:7:\"indexes\";a:1:{s:6:\"format\";a:1:{i:0;s:6:\"format\";}}s:2:\"id\";s:2:\"31\";}',1,0,0),(32,'field_mt_slideshow_image','image','image',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:2:{s:10:\"uri_scheme\";s:6:\"public\";s:13:\"default_image\";i:0;}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:35:\"field_data_field_mt_slideshow_image\";a:5:{s:3:\"fid\";s:28:\"field_mt_slideshow_image_fid\";s:3:\"alt\";s:28:\"field_mt_slideshow_image_alt\";s:5:\"title\";s:30:\"field_mt_slideshow_image_title\";s:5:\"width\";s:30:\"field_mt_slideshow_image_width\";s:6:\"height\";s:31:\"field_mt_slideshow_image_height\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:39:\"field_revision_field_mt_slideshow_image\";a:5:{s:3:\"fid\";s:28:\"field_mt_slideshow_image_fid\";s:3:\"alt\";s:28:\"field_mt_slideshow_image_alt\";s:5:\"title\";s:30:\"field_mt_slideshow_image_title\";s:5:\"width\";s:30:\"field_mt_slideshow_image_width\";s:6:\"height\";s:31:\"field_mt_slideshow_image_height\";}}}}}s:12:\"foreign keys\";a:1:{s:3:\"fid\";a:2:{s:5:\"table\";s:12:\"file_managed\";s:7:\"columns\";a:1:{s:3:\"fid\";s:3:\"fid\";}}}s:7:\"indexes\";a:1:{s:3:\"fid\";a:1:{i:0;s:3:\"fid\";}}s:2:\"id\";s:2:\"32\";}',1,0,0),(33,'field_mt_promoted_on_slideshow','list_boolean','list',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:2:{s:14:\"allowed_values\";a:2:{i:0;s:3:\"off\";i:1;s:2:\"on\";}s:23:\"allowed_values_function\";s:0:\"\";}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:41:\"field_data_field_mt_promoted_on_slideshow\";a:1:{s:5:\"value\";s:36:\"field_mt_promoted_on_slideshow_value\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:45:\"field_revision_field_mt_promoted_on_slideshow\";a:1:{s:5:\"value\";s:36:\"field_mt_promoted_on_slideshow_value\";}}}}}s:12:\"foreign keys\";a:0:{}s:7:\"indexes\";a:1:{s:5:\"value\";a:1:{i:0;s:5:\"value\";}}s:2:\"id\";s:2:\"33\";}',1,0,0),(34,'field_mt_slideshow_path','text','text',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:1:{s:10:\"max_length\";s:3:\"255\";}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:34:\"field_data_field_mt_slideshow_path\";a:2:{s:5:\"value\";s:29:\"field_mt_slideshow_path_value\";s:6:\"format\";s:30:\"field_mt_slideshow_path_format\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:38:\"field_revision_field_mt_slideshow_path\";a:2:{s:5:\"value\";s:29:\"field_mt_slideshow_path_value\";s:6:\"format\";s:30:\"field_mt_slideshow_path_format\";}}}}}s:12:\"foreign keys\";a:1:{s:6:\"format\";a:2:{s:5:\"table\";s:13:\"filter_format\";s:7:\"columns\";a:1:{s:6:\"format\";s:6:\"format\";}}}s:7:\"indexes\";a:1:{s:6:\"format\";a:1:{i:0;s:6:\"format\";}}s:2:\"id\";s:2:\"34\";}',1,0,0),(35,'field_mt_banner_image','image','image',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:2:{s:10:\"uri_scheme\";s:6:\"public\";s:13:\"default_image\";i:0;}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:32:\"field_data_field_mt_banner_image\";a:5:{s:3:\"fid\";s:25:\"field_mt_banner_image_fid\";s:3:\"alt\";s:25:\"field_mt_banner_image_alt\";s:5:\"title\";s:27:\"field_mt_banner_image_title\";s:5:\"width\";s:27:\"field_mt_banner_image_width\";s:6:\"height\";s:28:\"field_mt_banner_image_height\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:36:\"field_revision_field_mt_banner_image\";a:5:{s:3:\"fid\";s:25:\"field_mt_banner_image_fid\";s:3:\"alt\";s:25:\"field_mt_banner_image_alt\";s:5:\"title\";s:27:\"field_mt_banner_image_title\";s:5:\"width\";s:27:\"field_mt_banner_image_width\";s:6:\"height\";s:28:\"field_mt_banner_image_height\";}}}}}s:12:\"foreign keys\";a:1:{s:3:\"fid\";a:2:{s:5:\"table\";s:12:\"file_managed\";s:7:\"columns\";a:1:{s:3:\"fid\";s:3:\"fid\";}}}s:7:\"indexes\";a:1:{s:3:\"fid\";a:1:{i:0;s:3:\"fid\";}}s:2:\"id\";s:2:\"35\";}',-1,0,0),(36,'field_mt_subtitle','text_with_summary','text',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:0:{}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:28:\"field_data_field_mt_subtitle\";a:3:{s:5:\"value\";s:23:\"field_mt_subtitle_value\";s:7:\"summary\";s:25:\"field_mt_subtitle_summary\";s:6:\"format\";s:24:\"field_mt_subtitle_format\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:32:\"field_revision_field_mt_subtitle\";a:3:{s:5:\"value\";s:23:\"field_mt_subtitle_value\";s:7:\"summary\";s:25:\"field_mt_subtitle_summary\";s:6:\"format\";s:24:\"field_mt_subtitle_format\";}}}}}s:12:\"foreign keys\";a:1:{s:6:\"format\";a:2:{s:5:\"table\";s:13:\"filter_format\";s:7:\"columns\";a:1:{s:6:\"format\";s:6:\"format\";}}}s:7:\"indexes\";a:1:{s:6:\"format\";a:1:{i:0;s:6:\"format\";}}s:2:\"id\";s:2:\"36\";}',1,0,0),(37,'field_mt_showcase_tags','taxonomy_term_reference','taxonomy',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:1:{s:14:\"allowed_values\";a:1:{i:0;a:2:{s:10:\"vocabulary\";s:13:\"showcase_tags\";s:6:\"parent\";s:1:\"0\";}}}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:33:\"field_data_field_mt_showcase_tags\";a:1:{s:3:\"tid\";s:26:\"field_mt_showcase_tags_tid\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:37:\"field_revision_field_mt_showcase_tags\";a:1:{s:3:\"tid\";s:26:\"field_mt_showcase_tags_tid\";}}}}}s:12:\"foreign keys\";a:1:{s:3:\"tid\";a:2:{s:5:\"table\";s:18:\"taxonomy_term_data\";s:7:\"columns\";a:1:{s:3:\"tid\";s:3:\"tid\";}}}s:7:\"indexes\";a:1:{s:3:\"tid\";a:1:{i:0;s:3:\"tid\";}}s:2:\"id\";s:2:\"37\";}',-1,0,0),(38,'field_mt_member_photo','image','image',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:2:{s:10:\"uri_scheme\";s:6:\"public\";s:13:\"default_image\";s:3:\"181\";}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:32:\"field_data_field_mt_member_photo\";a:5:{s:3:\"fid\";s:25:\"field_mt_member_photo_fid\";s:3:\"alt\";s:25:\"field_mt_member_photo_alt\";s:5:\"title\";s:27:\"field_mt_member_photo_title\";s:5:\"width\";s:27:\"field_mt_member_photo_width\";s:6:\"height\";s:28:\"field_mt_member_photo_height\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:36:\"field_revision_field_mt_member_photo\";a:5:{s:3:\"fid\";s:25:\"field_mt_member_photo_fid\";s:3:\"alt\";s:25:\"field_mt_member_photo_alt\";s:5:\"title\";s:27:\"field_mt_member_photo_title\";s:5:\"width\";s:27:\"field_mt_member_photo_width\";s:6:\"height\";s:28:\"field_mt_member_photo_height\";}}}}}s:12:\"foreign keys\";a:1:{s:3:\"fid\";a:2:{s:5:\"table\";s:12:\"file_managed\";s:7:\"columns\";a:1:{s:3:\"fid\";s:3:\"fid\";}}}s:7:\"indexes\";a:1:{s:3:\"fid\";a:1:{i:0;s:3:\"fid\";}}s:2:\"id\";s:2:\"38\";}',1,0,0),(39,'field_mt_facebook_account','text','text',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:1:{s:10:\"max_length\";s:3:\"255\";}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:36:\"field_data_field_mt_facebook_account\";a:2:{s:5:\"value\";s:31:\"field_mt_facebook_account_value\";s:6:\"format\";s:32:\"field_mt_facebook_account_format\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:40:\"field_revision_field_mt_facebook_account\";a:2:{s:5:\"value\";s:31:\"field_mt_facebook_account_value\";s:6:\"format\";s:32:\"field_mt_facebook_account_format\";}}}}}s:12:\"foreign keys\";a:1:{s:6:\"format\";a:2:{s:5:\"table\";s:13:\"filter_format\";s:7:\"columns\";a:1:{s:6:\"format\";s:6:\"format\";}}}s:7:\"indexes\";a:1:{s:6:\"format\";a:1:{i:0;s:6:\"format\";}}s:2:\"id\";s:2:\"39\";}',1,0,0),(40,'field_mt_twitter_account','text','text',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:1:{s:10:\"max_length\";s:3:\"255\";}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:35:\"field_data_field_mt_twitter_account\";a:2:{s:5:\"value\";s:30:\"field_mt_twitter_account_value\";s:6:\"format\";s:31:\"field_mt_twitter_account_format\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:39:\"field_revision_field_mt_twitter_account\";a:2:{s:5:\"value\";s:30:\"field_mt_twitter_account_value\";s:6:\"format\";s:31:\"field_mt_twitter_account_format\";}}}}}s:12:\"foreign keys\";a:1:{s:6:\"format\";a:2:{s:5:\"table\";s:13:\"filter_format\";s:7:\"columns\";a:1:{s:6:\"format\";s:6:\"format\";}}}s:7:\"indexes\";a:1:{s:6:\"format\";a:1:{i:0;s:6:\"format\";}}s:2:\"id\";s:2:\"40\";}',1,0,0),(41,'field_mt_linkedin_account','text','text',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:1:{s:10:\"max_length\";s:3:\"255\";}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:36:\"field_data_field_mt_linkedin_account\";a:2:{s:5:\"value\";s:31:\"field_mt_linkedin_account_value\";s:6:\"format\";s:32:\"field_mt_linkedin_account_format\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:40:\"field_revision_field_mt_linkedin_account\";a:2:{s:5:\"value\";s:31:\"field_mt_linkedin_account_value\";s:6:\"format\";s:32:\"field_mt_linkedin_account_format\";}}}}}s:12:\"foreign keys\";a:1:{s:6:\"format\";a:2:{s:5:\"table\";s:13:\"filter_format\";s:7:\"columns\";a:1:{s:6:\"format\";s:6:\"format\";}}}s:7:\"indexes\";a:1:{s:6:\"format\";a:1:{i:0;s:6:\"format\";}}s:2:\"id\";s:2:\"41\";}',1,0,0),(42,'field_mt_testimonial_image','image','image',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:2:{s:10:\"uri_scheme\";s:6:\"public\";s:13:\"default_image\";i:0;}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:37:\"field_data_field_mt_testimonial_image\";a:5:{s:3:\"fid\";s:30:\"field_mt_testimonial_image_fid\";s:3:\"alt\";s:30:\"field_mt_testimonial_image_alt\";s:5:\"title\";s:32:\"field_mt_testimonial_image_title\";s:5:\"width\";s:32:\"field_mt_testimonial_image_width\";s:6:\"height\";s:33:\"field_mt_testimonial_image_height\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:41:\"field_revision_field_mt_testimonial_image\";a:5:{s:3:\"fid\";s:30:\"field_mt_testimonial_image_fid\";s:3:\"alt\";s:30:\"field_mt_testimonial_image_alt\";s:5:\"title\";s:32:\"field_mt_testimonial_image_title\";s:5:\"width\";s:32:\"field_mt_testimonial_image_width\";s:6:\"height\";s:33:\"field_mt_testimonial_image_height\";}}}}}s:12:\"foreign keys\";a:1:{s:3:\"fid\";a:2:{s:5:\"table\";s:12:\"file_managed\";s:7:\"columns\";a:1:{s:3:\"fid\";s:3:\"fid\";}}}s:7:\"indexes\";a:1:{s:3:\"fid\";a:1:{i:0;s:3:\"fid\";}}s:2:\"id\";s:2:\"42\";}',1,0,0),(43,'field_mt_highlight','field_collection','field_collection',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:2:{s:16:\"hide_blank_items\";i:1;s:4:\"path\";s:0:\"\";}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:29:\"field_data_field_mt_highlight\";a:2:{s:5:\"value\";s:24:\"field_mt_highlight_value\";s:11:\"revision_id\";s:30:\"field_mt_highlight_revision_id\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:33:\"field_revision_field_mt_highlight\";a:2:{s:5:\"value\";s:24:\"field_mt_highlight_value\";s:11:\"revision_id\";s:30:\"field_mt_highlight_revision_id\";}}}}}s:12:\"foreign keys\";a:0:{}s:7:\"indexes\";a:2:{s:11:\"revision_id\";a:1:{i:0;s:11:\"revision_id\";}s:5:\"value\";a:1:{i:0;s:5:\"value\";}}s:2:\"id\";s:2:\"43\";}',-1,0,0),(45,'field_mt_highlight_image','image','image',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:2:{s:10:\"uri_scheme\";s:6:\"public\";s:13:\"default_image\";i:0;}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:35:\"field_data_field_mt_highlight_image\";a:5:{s:3:\"fid\";s:28:\"field_mt_highlight_image_fid\";s:3:\"alt\";s:28:\"field_mt_highlight_image_alt\";s:5:\"title\";s:30:\"field_mt_highlight_image_title\";s:5:\"width\";s:30:\"field_mt_highlight_image_width\";s:6:\"height\";s:31:\"field_mt_highlight_image_height\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:39:\"field_revision_field_mt_highlight_image\";a:5:{s:3:\"fid\";s:28:\"field_mt_highlight_image_fid\";s:3:\"alt\";s:28:\"field_mt_highlight_image_alt\";s:5:\"title\";s:30:\"field_mt_highlight_image_title\";s:5:\"width\";s:30:\"field_mt_highlight_image_width\";s:6:\"height\";s:31:\"field_mt_highlight_image_height\";}}}}}s:12:\"foreign keys\";a:1:{s:3:\"fid\";a:2:{s:5:\"table\";s:12:\"file_managed\";s:7:\"columns\";a:1:{s:3:\"fid\";s:3:\"fid\";}}}s:7:\"indexes\";a:1:{s:3:\"fid\";a:1:{i:0;s:3:\"fid\";}}s:2:\"id\";s:2:\"45\";}',1,0,0),(46,'field_mt_special_feature','field_collection','field_collection',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:2:{s:16:\"hide_blank_items\";i:1;s:4:\"path\";s:0:\"\";}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:35:\"field_data_field_mt_special_feature\";a:2:{s:5:\"value\";s:30:\"field_mt_special_feature_value\";s:11:\"revision_id\";s:36:\"field_mt_special_feature_revision_id\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:39:\"field_revision_field_mt_special_feature\";a:2:{s:5:\"value\";s:30:\"field_mt_special_feature_value\";s:11:\"revision_id\";s:36:\"field_mt_special_feature_revision_id\";}}}}}s:12:\"foreign keys\";a:0:{}s:7:\"indexes\";a:2:{s:11:\"revision_id\";a:1:{i:0;s:11:\"revision_id\";}s:5:\"value\";a:1:{i:0;s:5:\"value\";}}s:2:\"id\";s:2:\"46\";}',-1,0,0),(48,'field_mt_feature_title','text','text',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:1:{s:10:\"max_length\";s:3:\"255\";}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:33:\"field_data_field_mt_feature_title\";a:2:{s:5:\"value\";s:28:\"field_mt_feature_title_value\";s:6:\"format\";s:29:\"field_mt_feature_title_format\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:37:\"field_revision_field_mt_feature_title\";a:2:{s:5:\"value\";s:28:\"field_mt_feature_title_value\";s:6:\"format\";s:29:\"field_mt_feature_title_format\";}}}}}s:12:\"foreign keys\";a:1:{s:6:\"format\";a:2:{s:5:\"table\";s:13:\"filter_format\";s:7:\"columns\";a:1:{s:6:\"format\";s:6:\"format\";}}}s:7:\"indexes\";a:1:{s:6:\"format\";a:1:{i:0;s:6:\"format\";}}s:2:\"id\";s:2:\"48\";}',1,0,0),(49,'field_mt_feature_subtitle','text_with_summary','text',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:0:{}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:36:\"field_data_field_mt_feature_subtitle\";a:3:{s:5:\"value\";s:31:\"field_mt_feature_subtitle_value\";s:7:\"summary\";s:33:\"field_mt_feature_subtitle_summary\";s:6:\"format\";s:32:\"field_mt_feature_subtitle_format\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:40:\"field_revision_field_mt_feature_subtitle\";a:3:{s:5:\"value\";s:31:\"field_mt_feature_subtitle_value\";s:7:\"summary\";s:33:\"field_mt_feature_subtitle_summary\";s:6:\"format\";s:32:\"field_mt_feature_subtitle_format\";}}}}}s:12:\"foreign keys\";a:1:{s:6:\"format\";a:2:{s:5:\"table\";s:13:\"filter_format\";s:7:\"columns\";a:1:{s:6:\"format\";s:6:\"format\";}}}s:7:\"indexes\";a:1:{s:6:\"format\";a:1:{i:0;s:6:\"format\";}}s:2:\"id\";s:2:\"49\";}',1,0,0),(51,'field_mt_feature_body','text_with_summary','text',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:0:{}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:32:\"field_data_field_mt_feature_body\";a:3:{s:5:\"value\";s:27:\"field_mt_feature_body_value\";s:7:\"summary\";s:29:\"field_mt_feature_body_summary\";s:6:\"format\";s:28:\"field_mt_feature_body_format\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:36:\"field_revision_field_mt_feature_body\";a:3:{s:5:\"value\";s:27:\"field_mt_feature_body_value\";s:7:\"summary\";s:29:\"field_mt_feature_body_summary\";s:6:\"format\";s:28:\"field_mt_feature_body_format\";}}}}}s:12:\"foreign keys\";a:1:{s:6:\"format\";a:2:{s:5:\"table\";s:13:\"filter_format\";s:7:\"columns\";a:1:{s:6:\"format\";s:6:\"format\";}}}s:7:\"indexes\";a:1:{s:6:\"format\";a:1:{i:0;s:6:\"format\";}}s:2:\"id\";s:2:\"51\";}',1,0,0),(52,'field_mt_feature_image','image','image',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:2:{s:10:\"uri_scheme\";s:6:\"public\";s:13:\"default_image\";i:0;}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:33:\"field_data_field_mt_feature_image\";a:5:{s:3:\"fid\";s:26:\"field_mt_feature_image_fid\";s:3:\"alt\";s:26:\"field_mt_feature_image_alt\";s:5:\"title\";s:28:\"field_mt_feature_image_title\";s:5:\"width\";s:28:\"field_mt_feature_image_width\";s:6:\"height\";s:29:\"field_mt_feature_image_height\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:37:\"field_revision_field_mt_feature_image\";a:5:{s:3:\"fid\";s:26:\"field_mt_feature_image_fid\";s:3:\"alt\";s:26:\"field_mt_feature_image_alt\";s:5:\"title\";s:28:\"field_mt_feature_image_title\";s:5:\"width\";s:28:\"field_mt_feature_image_width\";s:6:\"height\";s:29:\"field_mt_feature_image_height\";}}}}}s:12:\"foreign keys\";a:1:{s:3:\"fid\";a:2:{s:5:\"table\";s:12:\"file_managed\";s:7:\"columns\";a:1:{s:3:\"fid\";s:3:\"fid\";}}}s:7:\"indexes\";a:1:{s:3:\"fid\";a:1:{i:0;s:3:\"fid\";}}s:2:\"id\";s:2:\"52\";}',1,0,0),(53,'field_mt_feature_font_awesome','text','text',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:1:{s:10:\"max_length\";s:3:\"255\";}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:40:\"field_data_field_mt_feature_font_awesome\";a:2:{s:5:\"value\";s:35:\"field_mt_feature_font_awesome_value\";s:6:\"format\";s:36:\"field_mt_feature_font_awesome_format\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:44:\"field_revision_field_mt_feature_font_awesome\";a:2:{s:5:\"value\";s:35:\"field_mt_feature_font_awesome_value\";s:6:\"format\";s:36:\"field_mt_feature_font_awesome_format\";}}}}}s:12:\"foreign keys\";a:1:{s:6:\"format\";a:2:{s:5:\"table\";s:13:\"filter_format\";s:7:\"columns\";a:1:{s:6:\"format\";s:6:\"format\";}}}s:7:\"indexes\";a:1:{s:6:\"format\";a:1:{i:0;s:6:\"format\";}}s:2:\"id\";s:2:\"53\";}',1,0,0),(54,'field_mt_standard_feature','field_collection','field_collection',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:2:{s:16:\"hide_blank_items\";i:1;s:4:\"path\";s:0:\"\";}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:36:\"field_data_field_mt_standard_feature\";a:2:{s:5:\"value\";s:31:\"field_mt_standard_feature_value\";s:11:\"revision_id\";s:37:\"field_mt_standard_feature_revision_id\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:40:\"field_revision_field_mt_standard_feature\";a:2:{s:5:\"value\";s:31:\"field_mt_standard_feature_value\";s:11:\"revision_id\";s:37:\"field_mt_standard_feature_revision_id\";}}}}}s:12:\"foreign keys\";a:0:{}s:7:\"indexes\";a:2:{s:11:\"revision_id\";a:1:{i:0;s:11:\"revision_id\";}s:5:\"value\";a:1:{i:0;s:5:\"value\";}}s:2:\"id\";s:2:\"54\";}',-1,0,0),(55,'field_mt_highlight_title','text','text',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:1:{s:10:\"max_length\";s:3:\"255\";}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:35:\"field_data_field_mt_highlight_title\";a:2:{s:5:\"value\";s:30:\"field_mt_highlight_title_value\";s:6:\"format\";s:31:\"field_mt_highlight_title_format\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:39:\"field_revision_field_mt_highlight_title\";a:2:{s:5:\"value\";s:30:\"field_mt_highlight_title_value\";s:6:\"format\";s:31:\"field_mt_highlight_title_format\";}}}}}s:12:\"foreign keys\";a:1:{s:6:\"format\";a:2:{s:5:\"table\";s:13:\"filter_format\";s:7:\"columns\";a:1:{s:6:\"format\";s:6:\"format\";}}}s:7:\"indexes\";a:1:{s:6:\"format\";a:1:{i:0;s:6:\"format\";}}s:2:\"id\";s:2:\"55\";}',1,0,0),(56,'field_mt_highlight_body','text_with_summary','text',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:0:{}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:34:\"field_data_field_mt_highlight_body\";a:3:{s:5:\"value\";s:29:\"field_mt_highlight_body_value\";s:7:\"summary\";s:31:\"field_mt_highlight_body_summary\";s:6:\"format\";s:30:\"field_mt_highlight_body_format\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:38:\"field_revision_field_mt_highlight_body\";a:3:{s:5:\"value\";s:29:\"field_mt_highlight_body_value\";s:7:\"summary\";s:31:\"field_mt_highlight_body_summary\";s:6:\"format\";s:30:\"field_mt_highlight_body_format\";}}}}}s:12:\"foreign keys\";a:1:{s:6:\"format\";a:2:{s:5:\"table\";s:13:\"filter_format\";s:7:\"columns\";a:1:{s:6:\"format\";s:6:\"format\";}}}s:7:\"indexes\";a:1:{s:6:\"format\";a:1:{i:0;s:6:\"format\";}}s:2:\"id\";s:2:\"56\";}',1,0,0),(57,'field_mt_highlight_link','link_field','link',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:7:{s:10:\"attributes\";a:3:{s:6:\"target\";s:7:\"default\";s:5:\"class\";s:0:\"\";s:3:\"rel\";s:0:\"\";}s:3:\"url\";i:0;s:5:\"title\";s:8:\"optional\";s:11:\"title_value\";s:0:\"\";s:15:\"title_maxlength\";i:128;s:13:\"enable_tokens\";i:1;s:7:\"display\";a:1:{s:10:\"url_cutoff\";i:80;}}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:34:\"field_data_field_mt_highlight_link\";a:3:{s:3:\"url\";s:27:\"field_mt_highlight_link_url\";s:5:\"title\";s:29:\"field_mt_highlight_link_title\";s:10:\"attributes\";s:34:\"field_mt_highlight_link_attributes\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:38:\"field_revision_field_mt_highlight_link\";a:3:{s:3:\"url\";s:27:\"field_mt_highlight_link_url\";s:5:\"title\";s:29:\"field_mt_highlight_link_title\";s:10:\"attributes\";s:34:\"field_mt_highlight_link_attributes\";}}}}}s:12:\"foreign keys\";a:0:{}s:7:\"indexes\";a:0:{}s:2:\"id\";s:2:\"57\";}',1,0,0),(60,'field_mt_portrait_image','image','image',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:2:{s:10:\"uri_scheme\";s:6:\"public\";s:13:\"default_image\";i:0;}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:34:\"field_data_field_mt_portrait_image\";a:5:{s:3:\"fid\";s:27:\"field_mt_portrait_image_fid\";s:3:\"alt\";s:27:\"field_mt_portrait_image_alt\";s:5:\"title\";s:29:\"field_mt_portrait_image_title\";s:5:\"width\";s:29:\"field_mt_portrait_image_width\";s:6:\"height\";s:30:\"field_mt_portrait_image_height\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:38:\"field_revision_field_mt_portrait_image\";a:5:{s:3:\"fid\";s:27:\"field_mt_portrait_image_fid\";s:3:\"alt\";s:27:\"field_mt_portrait_image_alt\";s:5:\"title\";s:29:\"field_mt_portrait_image_title\";s:5:\"width\";s:29:\"field_mt_portrait_image_width\";s:6:\"height\";s:30:\"field_mt_portrait_image_height\";}}}}}s:12:\"foreign keys\";a:1:{s:3:\"fid\";a:2:{s:5:\"table\";s:12:\"file_managed\";s:7:\"columns\";a:1:{s:3:\"fid\";s:3:\"fid\";}}}s:7:\"indexes\";a:1:{s:3:\"fid\";a:1:{i:0;s:3:\"fid\";}}s:2:\"id\";s:2:\"60\";}',1,0,0),(61,'field_mt_service_tags','taxonomy_term_reference','taxonomy',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:1:{s:14:\"allowed_values\";a:1:{i:0;a:2:{s:10:\"vocabulary\";s:12:\"service_tags\";s:6:\"parent\";s:1:\"0\";}}}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:32:\"field_data_field_mt_service_tags\";a:1:{s:3:\"tid\";s:25:\"field_mt_service_tags_tid\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:36:\"field_revision_field_mt_service_tags\";a:1:{s:3:\"tid\";s:25:\"field_mt_service_tags_tid\";}}}}}s:12:\"foreign keys\";a:1:{s:3:\"tid\";a:2:{s:5:\"table\";s:18:\"taxonomy_term_data\";s:7:\"columns\";a:1:{s:3:\"tid\";s:3:\"tid\";}}}s:7:\"indexes\";a:1:{s:3:\"tid\";a:1:{i:0;s:3:\"tid\";}}s:2:\"id\";s:2:\"61\";}',-1,0,0),(63,'field_mt_product_tags','taxonomy_term_reference','taxonomy',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:1:{s:14:\"allowed_values\";a:1:{i:0;a:2:{s:10:\"vocabulary\";s:15:\"mt_product_tags\";s:6:\"parent\";s:1:\"0\";}}}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:32:\"field_data_field_mt_product_tags\";a:1:{s:3:\"tid\";s:25:\"field_mt_product_tags_tid\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:36:\"field_revision_field_mt_product_tags\";a:1:{s:3:\"tid\";s:25:\"field_mt_product_tags_tid\";}}}}}s:12:\"foreign keys\";a:1:{s:3:\"tid\";a:2:{s:5:\"table\";s:18:\"taxonomy_term_data\";s:7:\"columns\";a:1:{s:3:\"tid\";s:3:\"tid\";}}}s:7:\"indexes\";a:1:{s:3:\"tid\";a:1:{i:0;s:3:\"tid\";}}s:2:\"id\";s:2:\"63\";}',-1,0,0),(66,'field_mt_landscape_image','image','image',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:2:{s:10:\"uri_scheme\";s:6:\"public\";s:13:\"default_image\";i:0;}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:35:\"field_data_field_mt_landscape_image\";a:5:{s:3:\"fid\";s:28:\"field_mt_landscape_image_fid\";s:3:\"alt\";s:28:\"field_mt_landscape_image_alt\";s:5:\"title\";s:30:\"field_mt_landscape_image_title\";s:5:\"width\";s:30:\"field_mt_landscape_image_width\";s:6:\"height\";s:31:\"field_mt_landscape_image_height\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:39:\"field_revision_field_mt_landscape_image\";a:5:{s:3:\"fid\";s:28:\"field_mt_landscape_image_fid\";s:3:\"alt\";s:28:\"field_mt_landscape_image_alt\";s:5:\"title\";s:30:\"field_mt_landscape_image_title\";s:5:\"width\";s:30:\"field_mt_landscape_image_width\";s:6:\"height\";s:31:\"field_mt_landscape_image_height\";}}}}}s:12:\"foreign keys\";a:1:{s:3:\"fid\";a:2:{s:5:\"table\";s:12:\"file_managed\";s:7:\"columns\";a:1:{s:3:\"fid\";s:3:\"fid\";}}}s:7:\"indexes\";a:1:{s:3:\"fid\";a:1:{i:0;s:3:\"fid\";}}s:2:\"id\";s:2:\"66\";}',1,0,0),(67,'field_mt_pricing_table_item','text','text',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:1:{s:10:\"max_length\";s:3:\"255\";}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:38:\"field_data_field_mt_pricing_table_item\";a:2:{s:5:\"value\";s:33:\"field_mt_pricing_table_item_value\";s:6:\"format\";s:34:\"field_mt_pricing_table_item_format\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:42:\"field_revision_field_mt_pricing_table_item\";a:2:{s:5:\"value\";s:33:\"field_mt_pricing_table_item_value\";s:6:\"format\";s:34:\"field_mt_pricing_table_item_format\";}}}}}s:12:\"foreign keys\";a:1:{s:6:\"format\";a:2:{s:5:\"table\";s:13:\"filter_format\";s:7:\"columns\";a:1:{s:6:\"format\";s:6:\"format\";}}}s:7:\"indexes\";a:1:{s:6:\"format\";a:1:{i:0;s:6:\"format\";}}s:2:\"id\";s:2:\"67\";}',-1,0,0),(68,'field_mt_price','text','text',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:1:{s:10:\"max_length\";s:3:\"255\";}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:25:\"field_data_field_mt_price\";a:2:{s:5:\"value\";s:20:\"field_mt_price_value\";s:6:\"format\";s:21:\"field_mt_price_format\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:29:\"field_revision_field_mt_price\";a:2:{s:5:\"value\";s:20:\"field_mt_price_value\";s:6:\"format\";s:21:\"field_mt_price_format\";}}}}}s:12:\"foreign keys\";a:1:{s:6:\"format\";a:2:{s:5:\"table\";s:13:\"filter_format\";s:7:\"columns\";a:1:{s:6:\"format\";s:6:\"format\";}}}s:7:\"indexes\";a:1:{s:6:\"format\";a:1:{i:0;s:6:\"format\";}}s:2:\"id\";s:2:\"68\";}',1,0,0),(69,'field_mt_most_popular','list_boolean','list',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:2:{s:14:\"allowed_values\";a:2:{i:0;s:0:\"\";i:1;s:12:\"most popular\";}s:23:\"allowed_values_function\";s:0:\"\";}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:32:\"field_data_field_mt_most_popular\";a:1:{s:5:\"value\";s:27:\"field_mt_most_popular_value\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:36:\"field_revision_field_mt_most_popular\";a:1:{s:5:\"value\";s:27:\"field_mt_most_popular_value\";}}}}}s:12:\"foreign keys\";a:0:{}s:7:\"indexes\";a:1:{s:5:\"value\";a:1:{i:0;s:5:\"value\";}}s:2:\"id\";s:2:\"69\";}',1,0,0),(70,'field_mt_content_bottom','text_with_summary','text',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:0:{}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:34:\"field_data_field_mt_content_bottom\";a:3:{s:5:\"value\";s:29:\"field_mt_content_bottom_value\";s:7:\"summary\";s:31:\"field_mt_content_bottom_summary\";s:6:\"format\";s:30:\"field_mt_content_bottom_format\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:38:\"field_revision_field_mt_content_bottom\";a:3:{s:5:\"value\";s:29:\"field_mt_content_bottom_value\";s:7:\"summary\";s:31:\"field_mt_content_bottom_summary\";s:6:\"format\";s:30:\"field_mt_content_bottom_format\";}}}}}s:12:\"foreign keys\";a:1:{s:6:\"format\";a:2:{s:5:\"table\";s:13:\"filter_format\";s:7:\"columns\";a:1:{s:6:\"format\";s:6:\"format\";}}}s:7:\"indexes\";a:1:{s:6:\"format\";a:1:{i:0;s:6:\"format\";}}s:2:\"id\";s:2:\"70\";}',1,0,0),(71,'field_mt_company_title','text','text',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:1:{s:10:\"max_length\";s:3:\"255\";}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:33:\"field_data_field_mt_company_title\";a:2:{s:5:\"value\";s:28:\"field_mt_company_title_value\";s:6:\"format\";s:29:\"field_mt_company_title_format\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:37:\"field_revision_field_mt_company_title\";a:2:{s:5:\"value\";s:28:\"field_mt_company_title_value\";s:6:\"format\";s:29:\"field_mt_company_title_format\";}}}}}s:12:\"foreign keys\";a:1:{s:6:\"format\";a:2:{s:5:\"table\";s:13:\"filter_format\";s:7:\"columns\";a:1:{s:6:\"format\";s:6:\"format\";}}}s:7:\"indexes\";a:1:{s:6:\"format\";a:1:{i:0;s:6:\"format\";}}s:2:\"id\";s:2:\"71\";}',1,0,0),(72,'field_mt_company_logo','image','image',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:2:{s:10:\"uri_scheme\";s:6:\"public\";s:13:\"default_image\";i:0;}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:32:\"field_data_field_mt_company_logo\";a:5:{s:3:\"fid\";s:25:\"field_mt_company_logo_fid\";s:3:\"alt\";s:25:\"field_mt_company_logo_alt\";s:5:\"title\";s:27:\"field_mt_company_logo_title\";s:5:\"width\";s:27:\"field_mt_company_logo_width\";s:6:\"height\";s:28:\"field_mt_company_logo_height\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:36:\"field_revision_field_mt_company_logo\";a:5:{s:3:\"fid\";s:25:\"field_mt_company_logo_fid\";s:3:\"alt\";s:25:\"field_mt_company_logo_alt\";s:5:\"title\";s:27:\"field_mt_company_logo_title\";s:5:\"width\";s:27:\"field_mt_company_logo_width\";s:6:\"height\";s:28:\"field_mt_company_logo_height\";}}}}}s:12:\"foreign keys\";a:1:{s:3:\"fid\";a:2:{s:5:\"table\";s:12:\"file_managed\";s:7:\"columns\";a:1:{s:3:\"fid\";s:3:\"fid\";}}}s:7:\"indexes\";a:1:{s:3:\"fid\";a:1:{i:0;s:3:\"fid\";}}s:2:\"id\";s:2:\"72\";}',1,0,0),(73,'field_mt_company_description','text_long','text',1,'field_sql_storage','field_sql_storage',1,0,'a:7:{s:12:\"translatable\";s:1:\"0\";s:12:\"entity_types\";a:0:{}s:8:\"settings\";a:0:{}s:7:\"storage\";a:5:{s:4:\"type\";s:17:\"field_sql_storage\";s:8:\"settings\";a:0:{}s:6:\"module\";s:17:\"field_sql_storage\";s:6:\"active\";s:1:\"1\";s:7:\"details\";a:1:{s:3:\"sql\";a:2:{s:18:\"FIELD_LOAD_CURRENT\";a:1:{s:39:\"field_data_field_mt_company_description\";a:2:{s:5:\"value\";s:34:\"field_mt_company_description_value\";s:6:\"format\";s:35:\"field_mt_company_description_format\";}}s:19:\"FIELD_LOAD_REVISION\";a:1:{s:43:\"field_revision_field_mt_company_description\";a:2:{s:5:\"value\";s:34:\"field_mt_company_description_value\";s:6:\"format\";s:35:\"field_mt_company_description_format\";}}}}}s:12:\"foreign keys\";a:1:{s:6:\"format\";a:2:{s:5:\"table\";s:13:\"filter_format\";s:7:\"columns\";a:1:{s:6:\"format\";s:6:\"format\";}}}s:7:\"indexes\";a:1:{s:6:\"format\";a:1:{i:0;s:6:\"format\";}}s:2:\"id\";s:2:\"73\";}',1,0,0);
/*!40000 ALTER TABLE `field_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_config_instance`
--

DROP TABLE IF EXISTS `field_config_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_config_instance` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier for a field instance',
  `field_id` int(11) NOT NULL COMMENT 'The identifier of the field attached by this instance',
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `entity_type` varchar(32) NOT NULL DEFAULT '',
  `bundle` varchar(128) NOT NULL DEFAULT '',
  `data` longblob NOT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `field_name_bundle` (`field_name`,`entity_type`,`bundle`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=215 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_config_instance`
--

LOCK TABLES `field_config_instance` WRITE;
/*!40000 ALTER TABLE `field_config_instance` DISABLE KEYS */;
INSERT INTO `field_config_instance` VALUES (1,1,'comment_body','comment','comment_node_page','a:6:{s:5:\"label\";s:7:\"Comment\";s:8:\"settings\";a:2:{s:15:\"text_processing\";i:1;s:18:\"user_register_form\";b:0;}s:8:\"required\";b:1;s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";i:0;s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:6:\"widget\";a:4:{s:4:\"type\";s:13:\"text_textarea\";s:8:\"settings\";a:1:{s:4:\"rows\";i:5;}s:6:\"weight\";i:0;s:6:\"module\";s:4:\"text\";}s:11:\"description\";s:0:\"\";}',0),(2,2,'body','node','page','a:6:{s:5:\"label\";s:4:\"Body\";s:6:\"widget\";a:4:{s:4:\"type\";s:26:\"text_textarea_with_summary\";s:8:\"settings\";a:2:{s:4:\"rows\";i:20;s:12:\"summary_rows\";i:5;}s:6:\"weight\";s:1:\"3\";s:6:\"module\";s:4:\"text\";}s:8:\"settings\";a:3:{s:15:\"display_summary\";b:1;s:15:\"text_processing\";i:1;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"1\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}s:6:\"teaser\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:23:\"text_summary_or_trimmed\";s:8:\"settings\";a:1:{s:11:\"trim_length\";i:600;}s:6:\"module\";s:4:\"text\";s:6:\"weight\";i:0;}}s:8:\"required\";b:0;s:11:\"description\";s:0:\"\";}',0),(3,1,'comment_body','comment','comment_node_article','a:6:{s:5:\"label\";s:7:\"Comment\";s:8:\"settings\";a:2:{s:15:\"text_processing\";i:1;s:18:\"user_register_form\";b:0;}s:8:\"required\";b:1;s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";i:0;s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:6:\"widget\";a:4:{s:4:\"type\";s:13:\"text_textarea\";s:8:\"settings\";a:1:{s:4:\"rows\";i:5;}s:6:\"weight\";i:0;s:6:\"module\";s:4:\"text\";}s:11:\"description\";s:0:\"\";}',0),(4,2,'body','node','article','a:6:{s:5:\"label\";s:4:\"Body\";s:6:\"widget\";a:4:{s:4:\"type\";s:26:\"text_textarea_with_summary\";s:8:\"settings\";a:2:{s:4:\"rows\";i:20;s:12:\"summary_rows\";i:5;}s:6:\"weight\";s:1:\"3\";s:6:\"module\";s:4:\"text\";}s:8:\"settings\";a:3:{s:15:\"display_summary\";b:1;s:15:\"text_processing\";i:1;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"1\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}s:6:\"teaser\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:23:\"text_summary_or_trimmed\";s:6:\"weight\";s:1:\"0\";s:8:\"settings\";a:1:{s:11:\"trim_length\";i:600;}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";b:0;s:11:\"description\";s:0:\"\";}',0),(5,3,'field_tags','node','article','a:6:{s:5:\"label\";s:4:\"Tags\";s:11:\"description\";s:63:\"Enter a comma-separated list of words to describe your content.\";s:6:\"widget\";a:4:{s:4:\"type\";s:21:\"taxonomy_autocomplete\";s:6:\"weight\";s:1:\"4\";s:8:\"settings\";a:2:{s:4:\"size\";i:60;s:17:\"autocomplete_path\";s:21:\"taxonomy/autocomplete\";}s:6:\"module\";s:8:\"taxonomy\";}s:7:\"display\";a:2:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:28:\"taxonomy_term_reference_link\";s:6:\"weight\";s:1:\"2\";s:8:\"settings\";a:0:{}s:6:\"module\";s:8:\"taxonomy\";}s:6:\"teaser\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:28:\"taxonomy_term_reference_link\";s:6:\"weight\";s:2:\"10\";s:8:\"settings\";a:0:{}s:6:\"module\";s:8:\"taxonomy\";}}s:8:\"settings\";a:1:{s:18:\"user_register_form\";b:0;}s:8:\"required\";b:0;}',0),(6,4,'field_image','node','article','a:6:{s:5:\"label\";s:16:\"In-page Image(s)\";s:11:\"description\";s:151:\"The image(s) you will use in this field will be used in the main content column of the node page. Recommended pixel dimensions - W: 750 px / H: 500 px.\";s:8:\"required\";i:0;s:8:\"settings\";a:9:{s:14:\"file_directory\";s:11:\"field/image\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"2\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:7:\"display\";a:2:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:5:\"image\";s:6:\"weight\";s:1:\"0\";s:8:\"settings\";a:2:{s:11:\"image_style\";s:5:\"large\";s:10:\"image_link\";s:0:\"\";}s:6:\"module\";s:5:\"image\";}s:6:\"teaser\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:5:\"image\";s:6:\"weight\";s:2:\"-1\";s:8:\"settings\";a:2:{s:11:\"image_style\";s:5:\"large\";s:10:\"image_link\";s:7:\"content\";}s:6:\"module\";s:5:\"image\";}}}',0),(7,1,'comment_body','comment','comment_node_blog','a:6:{s:5:\"label\";s:7:\"Comment\";s:8:\"settings\";a:2:{s:15:\"text_processing\";i:1;s:18:\"user_register_form\";b:0;}s:8:\"required\";b:1;s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";i:0;s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:6:\"widget\";a:4:{s:4:\"type\";s:13:\"text_textarea\";s:8:\"settings\";a:1:{s:4:\"rows\";i:5;}s:6:\"weight\";i:0;s:6:\"module\";s:4:\"text\";}s:11:\"description\";s:0:\"\";}',0),(8,2,'body','node','blog','a:6:{s:5:\"label\";s:4:\"Body\";s:6:\"widget\";a:4:{s:4:\"type\";s:26:\"text_textarea_with_summary\";s:8:\"settings\";a:2:{s:4:\"rows\";i:20;s:12:\"summary_rows\";i:5;}s:6:\"weight\";s:1:\"3\";s:6:\"module\";s:4:\"text\";}s:8:\"settings\";a:3:{s:15:\"display_summary\";b:1;s:15:\"text_processing\";i:1;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"1\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}s:6:\"teaser\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:23:\"text_summary_or_trimmed\";s:6:\"weight\";s:1:\"1\";s:8:\"settings\";a:1:{s:11:\"trim_length\";s:3:\"600\";}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";b:0;s:11:\"description\";s:0:\"\";}',0),(9,1,'comment_body','comment','comment_node_mt_testimonial','a:6:{s:5:\"label\";s:7:\"Comment\";s:8:\"settings\";a:2:{s:15:\"text_processing\";i:1;s:18:\"user_register_form\";b:0;}s:8:\"required\";b:1;s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"0\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:6:\"widget\";a:4:{s:4:\"type\";s:13:\"text_textarea\";s:8:\"settings\";a:1:{s:4:\"rows\";i:5;}s:6:\"weight\";i:0;s:6:\"module\";s:4:\"text\";}s:11:\"description\";s:0:\"\";}',0),(10,2,'body','node','mt_testimonial','a:6:{s:5:\"label\";s:4:\"Body\";s:6:\"widget\";a:4:{s:4:\"type\";s:26:\"text_textarea_with_summary\";s:8:\"settings\";a:2:{s:4:\"rows\";i:20;s:12:\"summary_rows\";i:5;}s:6:\"weight\";s:1:\"3\";s:6:\"module\";s:4:\"text\";}s:8:\"settings\";a:3:{s:15:\"display_summary\";b:1;s:15:\"text_processing\";i:1;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"1\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}s:6:\"teaser\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:23:\"text_summary_or_trimmed\";s:6:\"weight\";s:1:\"2\";s:8:\"settings\";a:1:{s:11:\"trim_length\";i:600;}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";b:0;s:11:\"description\";s:0:\"\";}',0),(19,1,'comment_body','comment','comment_node_mt_slideshow_entry','a:6:{s:5:\"label\";s:7:\"Comment\";s:8:\"settings\";a:2:{s:15:\"text_processing\";i:1;s:18:\"user_register_form\";b:0;}s:8:\"required\";b:1;s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";i:0;s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:6:\"widget\";a:4:{s:4:\"type\";s:13:\"text_textarea\";s:8:\"settings\";a:1:{s:4:\"rows\";i:5;}s:6:\"weight\";i:0;s:6:\"module\";s:4:\"text\";}s:11:\"description\";s:0:\"\";}',0),(25,1,'comment_body','comment','comment_node_mt_service','a:6:{s:5:\"label\";s:7:\"Comment\";s:8:\"settings\";a:2:{s:15:\"text_processing\";i:1;s:18:\"user_register_form\";b:0;}s:8:\"required\";b:1;s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";i:0;s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:6:\"widget\";a:4:{s:4:\"type\";s:13:\"text_textarea\";s:8:\"settings\";a:1:{s:4:\"rows\";i:5;}s:6:\"weight\";i:0;s:6:\"module\";s:4:\"text\";}s:11:\"description\";s:0:\"\";}',0),(26,2,'body','node','mt_service','a:6:{s:5:\"label\";s:4:\"Body\";s:6:\"widget\";a:4:{s:4:\"type\";s:26:\"text_textarea_with_summary\";s:8:\"settings\";a:2:{s:4:\"rows\";i:20;s:12:\"summary_rows\";i:5;}s:6:\"weight\";s:1:\"3\";s:6:\"module\";s:4:\"text\";}s:8:\"settings\";a:3:{s:15:\"display_summary\";b:1;s:15:\"text_processing\";i:1;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"1\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}s:6:\"teaser\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:23:\"text_summary_or_trimmed\";s:6:\"weight\";s:1:\"0\";s:8:\"settings\";a:1:{s:11:\"trim_length\";s:3:\"600\";}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";b:0;s:11:\"description\";s:0:\"\";}',0),(32,4,'field_image','node','blog','a:6:{s:5:\"label\";s:16:\"In-page Image(s)\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"2\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:5:\"image\";s:6:\"weight\";s:1:\"0\";s:8:\"settings\";a:2:{s:11:\"image_style\";s:5:\"large\";s:10:\"image_link\";s:0:\"\";}s:6:\"module\";s:5:\"image\";}s:6:\"teaser\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:5:\"image\";s:6:\"weight\";s:1:\"0\";s:8:\"settings\";a:2:{s:11:\"image_style\";s:5:\"large\";s:10:\"image_link\";s:7:\"content\";}s:6:\"module\";s:5:\"image\";}}s:8:\"required\";i:0;s:11:\"description\";s:151:\"The image(s) you will use in this field will be used in the main content column of the node page. Recommended pixel dimensions - W: 750 px / H: 500 px.\";}',0),(33,3,'field_tags','node','blog','a:7:{s:5:\"label\";s:4:\"Tags\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"5\";s:4:\"type\";s:21:\"taxonomy_autocomplete\";s:6:\"module\";s:8:\"taxonomy\";s:6:\"active\";i:0;s:8:\"settings\";a:2:{s:4:\"size\";i:60;s:17:\"autocomplete_path\";s:21:\"taxonomy/autocomplete\";}}s:8:\"settings\";a:1:{s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:28:\"taxonomy_term_reference_link\";s:6:\"weight\";s:1:\"2\";s:8:\"settings\";a:0:{}s:6:\"module\";s:8:\"taxonomy\";}s:6:\"teaser\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:28:\"taxonomy_term_reference_link\";s:6:\"weight\";s:1:\"2\";s:8:\"settings\";a:0:{}s:6:\"module\";s:8:\"taxonomy\";}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(37,1,'comment_body','comment','comment_node_mt_showcase','a:6:{s:5:\"label\";s:7:\"Comment\";s:8:\"settings\";a:2:{s:15:\"text_processing\";i:1;s:18:\"user_register_form\";b:0;}s:8:\"required\";b:1;s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";i:0;s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:6:\"widget\";a:4:{s:4:\"type\";s:13:\"text_textarea\";s:8:\"settings\";a:1:{s:4:\"rows\";i:5;}s:6:\"weight\";i:0;s:6:\"module\";s:4:\"text\";}s:11:\"description\";s:0:\"\";}',0),(38,2,'body','node','mt_showcase','a:6:{s:5:\"label\";s:4:\"Body\";s:6:\"widget\";a:4:{s:4:\"type\";s:26:\"text_textarea_with_summary\";s:8:\"settings\";a:2:{s:4:\"rows\";i:20;s:12:\"summary_rows\";i:5;}s:6:\"weight\";s:1:\"5\";s:6:\"module\";s:4:\"text\";}s:8:\"settings\";a:3:{s:15:\"display_summary\";b:1;s:15:\"text_processing\";i:1;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"3\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}s:6:\"teaser\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:23:\"text_summary_or_trimmed\";s:6:\"weight\";s:1:\"2\";s:8:\"settings\";a:1:{s:11:\"trim_length\";i:600;}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";b:0;s:11:\"description\";s:0:\"\";}',0),(39,4,'field_image','node','mt_showcase','a:6:{s:5:\"label\";s:16:\"In-page Image(s)\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"4\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:5:\"image\";s:6:\"weight\";s:1:\"2\";s:8:\"settings\";a:2:{s:11:\"image_style\";s:5:\"large\";s:10:\"image_link\";s:0:\"\";}s:6:\"module\";s:5:\"image\";}s:6:\"teaser\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:5:\"image\";s:6:\"weight\";s:1:\"1\";s:8:\"settings\";a:2:{s:11:\"image_style\";s:5:\"large\";s:10:\"image_link\";s:7:\"content\";}s:6:\"module\";s:5:\"image\";}}s:8:\"required\";i:0;s:11:\"description\";s:151:\"The image(s) you will use in this field will be used in the main content column of the node page. Recommended pixel dimensions - W: 750 px / H: 500 px.\";}',0),(45,1,'comment_body','comment','comment_node_webform','a:6:{s:5:\"label\";s:7:\"Comment\";s:8:\"settings\";a:2:{s:15:\"text_processing\";i:1;s:18:\"user_register_form\";b:0;}s:8:\"required\";b:1;s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";i:0;s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:6:\"widget\";a:4:{s:4:\"type\";s:13:\"text_textarea\";s:8:\"settings\";a:1:{s:4:\"rows\";i:5;}s:6:\"weight\";i:0;s:6:\"module\";s:4:\"text\";}s:11:\"description\";s:0:\"\";}',0),(46,2,'body','node','webform','a:6:{s:5:\"label\";s:4:\"Body\";s:6:\"widget\";a:4:{s:4:\"type\";s:26:\"text_textarea_with_summary\";s:8:\"settings\";a:2:{s:4:\"rows\";i:20;s:12:\"summary_rows\";i:5;}s:6:\"weight\";s:1:\"3\";s:6:\"module\";s:4:\"text\";}s:8:\"settings\";a:3:{s:15:\"display_summary\";b:1;s:15:\"text_processing\";i:1;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"1\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}s:6:\"teaser\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:23:\"text_summary_or_trimmed\";s:6:\"weight\";s:1:\"0\";s:8:\"settings\";a:1:{s:11:\"trim_length\";i:600;}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";b:0;s:11:\"description\";s:0:\"\";}',0),(71,1,'comment_body','comment','comment_node_mt_benefit','a:6:{s:5:\"label\";s:7:\"Comment\";s:8:\"settings\";a:2:{s:15:\"text_processing\";i:1;s:18:\"user_register_form\";b:0;}s:8:\"required\";b:1;s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";i:0;s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:6:\"widget\";a:4:{s:4:\"type\";s:13:\"text_textarea\";s:8:\"settings\";a:1:{s:4:\"rows\";i:5;}s:6:\"weight\";i:0;s:6:\"module\";s:4:\"text\";}s:11:\"description\";s:0:\"\";}',0),(72,2,'body','node','mt_benefit','a:6:{s:5:\"label\";s:4:\"Body\";s:6:\"widget\";a:4:{s:4:\"type\";s:26:\"text_textarea_with_summary\";s:8:\"settings\";a:2:{s:4:\"rows\";i:20;s:12:\"summary_rows\";i:5;}s:6:\"weight\";s:1:\"6\";s:6:\"module\";s:4:\"text\";}s:8:\"settings\";a:3:{s:15:\"display_summary\";b:1;s:15:\"text_processing\";i:1;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"3\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}s:6:\"teaser\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:23:\"text_summary_or_trimmed\";s:6:\"weight\";s:1:\"1\";s:8:\"settings\";a:1:{s:11:\"trim_length\";i:600;}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";b:0;s:11:\"description\";s:0:\"\";}',0),(74,4,'field_image','node','mt_benefit','a:6:{s:5:\"label\";s:16:\"In-page Image(s)\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"5\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:5:\"image\";s:6:\"weight\";s:1:\"2\";s:8:\"settings\";a:2:{s:11:\"image_style\";s:5:\"large\";s:10:\"image_link\";s:0:\"\";}s:6:\"module\";s:5:\"image\";}s:6:\"teaser\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:5:\"image\";s:6:\"weight\";s:1:\"0\";s:8:\"settings\";a:2:{s:11:\"image_style\";s:5:\"large\";s:10:\"image_link\";s:7:\"content\";}s:6:\"module\";s:5:\"image\";}}s:8:\"required\";i:0;s:11:\"description\";s:151:\"The image(s) you will use in this field will be used in the main content column of the node page. Recommended pixel dimensions - W: 750 px / H: 500 px.\";}',0),(76,3,'field_tags','node','mt_benefit','a:7:{s:5:\"label\";s:4:\"Tags\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"7\";s:4:\"type\";s:21:\"taxonomy_autocomplete\";s:6:\"module\";s:8:\"taxonomy\";s:6:\"active\";i:0;s:8:\"settings\";a:2:{s:4:\"size\";i:60;s:17:\"autocomplete_path\";s:21:\"taxonomy/autocomplete\";}}s:8:\"settings\";a:1:{s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:28:\"taxonomy_term_reference_link\";s:6:\"weight\";s:1:\"4\";s:8:\"settings\";a:0:{}s:6:\"module\";s:8:\"taxonomy\";}s:6:\"teaser\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:28:\"taxonomy_term_reference_link\";s:6:\"weight\";s:1:\"2\";s:8:\"settings\";a:0:{}s:6:\"module\";s:8:\"taxonomy\";}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(83,1,'comment_body','comment','comment_node_mt_product','a:6:{s:5:\"label\";s:7:\"Comment\";s:8:\"settings\";a:2:{s:15:\"text_processing\";i:1;s:18:\"user_register_form\";b:0;}s:8:\"required\";b:1;s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";i:0;s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:6:\"widget\";a:4:{s:4:\"type\";s:13:\"text_textarea\";s:8:\"settings\";a:1:{s:4:\"rows\";i:5;}s:6:\"weight\";i:0;s:6:\"module\";s:4:\"text\";}s:11:\"description\";s:0:\"\";}',0),(84,2,'body','node','mt_product','a:6:{s:5:\"label\";s:4:\"Body\";s:6:\"widget\";a:4:{s:4:\"type\";s:26:\"text_textarea_with_summary\";s:8:\"settings\";a:2:{s:4:\"rows\";i:20;s:12:\"summary_rows\";i:5;}s:6:\"weight\";s:1:\"3\";s:6:\"module\";s:4:\"text\";}s:8:\"settings\";a:3:{s:15:\"display_summary\";b:1;s:15:\"text_processing\";i:1;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"1\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}s:6:\"teaser\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:23:\"text_summary_or_trimmed\";s:6:\"weight\";s:1:\"0\";s:8:\"settings\";a:1:{s:11:\"trim_length\";i:600;}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";b:0;s:11:\"description\";s:0:\"\";}',0),(99,1,'comment_body','comment','comment_node_mt_team_member','a:6:{s:5:\"label\";s:7:\"Comment\";s:8:\"settings\";a:2:{s:15:\"text_processing\";i:1;s:18:\"user_register_form\";b:0;}s:8:\"required\";b:1;s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";i:0;s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:6:\"widget\";a:4:{s:4:\"type\";s:13:\"text_textarea\";s:8:\"settings\";a:1:{s:4:\"rows\";i:5;}s:6:\"weight\";i:0;s:6:\"module\";s:4:\"text\";}s:11:\"description\";s:0:\"\";}',0),(100,2,'body','node','mt_team_member','a:6:{s:5:\"label\";s:4:\"Body\";s:6:\"widget\";a:4:{s:4:\"type\";s:26:\"text_textarea_with_summary\";s:8:\"settings\";a:2:{s:4:\"rows\";i:20;s:12:\"summary_rows\";i:5;}s:6:\"weight\";s:1:\"7\";s:6:\"module\";s:4:\"text\";}s:8:\"settings\";a:3:{s:15:\"display_summary\";b:1;s:15:\"text_processing\";i:1;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"5\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}s:6:\"teaser\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:23:\"text_summary_or_trimmed\";s:8:\"settings\";a:1:{s:11:\"trim_length\";i:600;}s:6:\"module\";s:4:\"text\";s:6:\"weight\";i:0;}}s:8:\"required\";b:0;s:11:\"description\";s:0:\"\";}',0),(117,30,'field_mt_subheader_body','node','page','a:7:{s:5:\"label\";s:14:\"Subheader body\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"2\";s:4:\"type\";s:26:\"text_textarea_with_summary\";s:6:\"module\";s:4:\"text\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:4:\"rows\";s:1:\"5\";s:12:\"summary_rows\";i:5;}}s:8:\"settings\";a:3:{s:15:\"text_processing\";s:1:\"1\";s:15:\"display_summary\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"0\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(118,30,'field_mt_subheader_body','node','mt_benefit','a:7:{s:5:\"label\";s:14:\"Subheader body\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"3\";s:4:\"type\";s:26:\"text_textarea_with_summary\";s:6:\"module\";s:4:\"text\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:4:\"rows\";s:1:\"5\";s:12:\"summary_rows\";i:5;}}s:8:\"settings\";a:3:{s:15:\"text_processing\";s:1:\"1\";s:15:\"display_summary\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"1\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(119,30,'field_mt_subheader_body','node','mt_product','a:7:{s:5:\"label\";s:14:\"Subheader body\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"2\";s:4:\"type\";s:26:\"text_textarea_with_summary\";s:6:\"module\";s:4:\"text\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:4:\"rows\";s:1:\"5\";s:12:\"summary_rows\";i:5;}}s:8:\"settings\";a:3:{s:15:\"text_processing\";s:1:\"1\";s:15:\"display_summary\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"0\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}s:6:\"teaser\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:2:\"10\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(120,30,'field_mt_subheader_body','node','mt_service','a:7:{s:5:\"label\";s:14:\"Subheader body\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"2\";s:4:\"type\";s:26:\"text_textarea_with_summary\";s:6:\"module\";s:4:\"text\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:4:\"rows\";s:1:\"5\";s:12:\"summary_rows\";i:5;}}s:8:\"settings\";a:3:{s:15:\"text_processing\";s:1:\"1\";s:15:\"display_summary\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"0\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}s:6:\"teaser\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:2:\"11\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(121,30,'field_mt_subheader_body','node','mt_showcase','a:7:{s:5:\"label\";s:14:\"Subheader body\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"2\";s:4:\"type\";s:26:\"text_textarea_with_summary\";s:6:\"module\";s:4:\"text\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:4:\"rows\";s:1:\"5\";s:12:\"summary_rows\";i:5;}}s:8:\"settings\";a:3:{s:15:\"text_processing\";s:1:\"1\";s:15:\"display_summary\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"0\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(122,31,'field_mt_font_awesome_classes','node','mt_benefit','a:7:{s:5:\"label\";s:20:\"Font Awesome classes\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"4\";s:4:\"type\";s:14:\"text_textfield\";s:6:\"module\";s:4:\"text\";s:6:\"active\";i:1;s:8:\"settings\";a:1:{s:4:\"size\";s:2:\"60\";}}s:8:\"settings\";a:2:{s:15:\"text_processing\";s:1:\"0\";s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"7\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:366:\"Enter the class of the icon you want from the Font Awesome library e.g.: fa-film. A list of the available classes is provided here: <a href=\"http://fortawesome.github.io/Font-Awesome/cheatsheet\" target=\"_blank\">http://fortawesome.github.io/Font-Awesome/cheatsheet</a>. If you leave this field blank, the corresponding node image will be rendered instead of the icon.\";s:13:\"default_value\";N;}',0),(126,32,'field_mt_slideshow_image','node','mt_benefit','a:6:{s:5:\"label\";s:15:\"Slideshow image\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"8\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"8\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:314:\"<br>\r\nRecommended pixel dimensions for <strong>Fullscreen</strong> Slideshow - W: 1920 px / H: 1280 px.<br>\r\nRecommended pixel dimensions for <strong>Full-Width</strong> Slideshow - W: 1920 px / H: 630 px.<br>\r\nRecommended pixel dimensions for <strong>Boxed-Width</strong> Slideshow - W: 1140 px / H: 630 px.\r\n<br>\";}',0),(127,33,'field_mt_promoted_on_slideshow','node','mt_benefit','a:7:{s:5:\"label\";s:21:\"Promoted on slideshow\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"9\";s:4:\"type\";s:13:\"options_onoff\";s:6:\"module\";s:7:\"options\";s:6:\"active\";i:1;s:8:\"settings\";a:1:{s:13:\"display_label\";i:1;}}s:8:\"settings\";a:1:{s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"6\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";a:1:{i:0;a:1:{s:5:\"value\";i:0;}}}',0),(128,33,'field_mt_promoted_on_slideshow','node','blog','a:7:{s:5:\"label\";s:21:\"Promoted on slideshow\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"7\";s:4:\"type\";s:13:\"options_onoff\";s:6:\"module\";s:7:\"options\";s:6:\"active\";i:1;s:8:\"settings\";a:1:{s:13:\"display_label\";i:1;}}s:8:\"settings\";a:1:{s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"6\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";a:1:{i:0;a:1:{s:5:\"value\";i:0;}}}',0),(129,32,'field_mt_slideshow_image','node','blog','a:6:{s:5:\"label\";s:15:\"Slideshow image\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"6\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"5\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:314:\"<br>\r\nRecommended pixel dimensions for <strong>Fullscreen</strong> Slideshow - W: 1920 px / H: 1280 px.<br>\r\nRecommended pixel dimensions for <strong>Full-Width</strong> Slideshow - W: 1920 px / H: 630 px.<br>\r\nRecommended pixel dimensions for <strong>Boxed-Width</strong> Slideshow - W: 1140 px / H: 630 px.\r\n<br>\";}',0),(130,33,'field_mt_promoted_on_slideshow','node','mt_product','a:7:{s:5:\"label\";s:21:\"Promoted on slideshow\";s:6:\"widget\";a:5:{s:6:\"weight\";s:2:\"15\";s:4:\"type\";s:13:\"options_onoff\";s:6:\"module\";s:7:\"options\";s:6:\"active\";i:1;s:8:\"settings\";a:1:{s:13:\"display_label\";i:1;}}s:8:\"settings\";a:1:{s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:2:\"12\";s:8:\"settings\";a:0:{}}s:6:\"teaser\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"7\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";a:1:{i:0;a:1:{s:5:\"value\";i:0;}}}',0),(131,32,'field_mt_slideshow_image','node','mt_product','a:6:{s:5:\"label\";s:15:\"Slideshow image\";s:6:\"widget\";a:5:{s:6:\"weight\";s:2:\"14\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:2:\"13\";s:8:\"settings\";a:0:{}}s:6:\"teaser\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"8\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:314:\"<br>\r\nRecommended pixel dimensions for <strong>Fullscreen</strong> Slideshow - W: 1920 px / H: 1280 px.<br>\r\nRecommended pixel dimensions for <strong>Full-Width</strong> Slideshow - W: 1920 px / H: 630 px.<br>\r\nRecommended pixel dimensions for <strong>Boxed-Width</strong> Slideshow - W: 1140 px / H: 630 px.\r\n<br>\";}',0),(132,33,'field_mt_promoted_on_slideshow','node','mt_service','a:7:{s:5:\"label\";s:21:\"Promoted on slideshow\";s:6:\"widget\";a:5:{s:6:\"weight\";s:2:\"13\";s:4:\"type\";s:13:\"options_onoff\";s:6:\"module\";s:7:\"options\";s:6:\"active\";i:1;s:8:\"settings\";a:1:{s:13:\"display_label\";i:1;}}s:8:\"settings\";a:1:{s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:2:\"10\";s:8:\"settings\";a:0:{}}s:6:\"teaser\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"8\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";a:1:{i:0;a:1:{s:5:\"value\";i:0;}}}',0),(133,32,'field_mt_slideshow_image','node','mt_service','a:6:{s:5:\"label\";s:15:\"Slideshow image\";s:6:\"widget\";a:5:{s:6:\"weight\";s:2:\"12\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:0;s:11:\"title_field\";i:0;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:2:\"11\";s:8:\"settings\";a:0:{}}s:6:\"teaser\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"9\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:314:\"<br>\r\nRecommended pixel dimensions for <strong>Fullscreen</strong> Slideshow - W: 1920 px / H: 1280 px.<br>\r\nRecommended pixel dimensions for <strong>Full-Width</strong> Slideshow - W: 1920 px / H: 630 px.<br>\r\nRecommended pixel dimensions for <strong>Boxed-Width</strong> Slideshow - W: 1140 px / H: 630 px.\r\n<br>\";}',0),(134,33,'field_mt_promoted_on_slideshow','node','mt_showcase','a:7:{s:5:\"label\";s:21:\"Promoted on slideshow\";s:6:\"widget\";a:5:{s:6:\"weight\";s:2:\"13\";s:4:\"type\";s:13:\"options_onoff\";s:6:\"module\";s:7:\"options\";s:6:\"active\";i:1;s:8:\"settings\";a:1:{s:13:\"display_label\";i:1;}}s:8:\"settings\";a:1:{s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:2:\"10\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";a:1:{i:0;a:1:{s:5:\"value\";i:0;}}}',0),(135,32,'field_mt_slideshow_image','node','mt_showcase','a:6:{s:5:\"label\";s:15:\"Slideshow image\";s:6:\"widget\";a:5:{s:6:\"weight\";s:2:\"12\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:2:\"11\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:314:\"<br>\r\nRecommended pixel dimensions for <strong>Fullscreen</strong> Slideshow - W: 1920 px / H: 1280 px.<br>\r\nRecommended pixel dimensions for <strong>Full-Width</strong> Slideshow - W: 1920 px / H: 630 px.<br>\r\nRecommended pixel dimensions for <strong>Boxed-Width</strong> Slideshow - W: 1140 px / H: 630 px.\r\n<br>\";}',0),(136,33,'field_mt_promoted_on_slideshow','node','mt_slideshow_entry','a:7:{s:5:\"label\";s:21:\"Promoted on slideshow\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"6\";s:4:\"type\";s:13:\"options_onoff\";s:6:\"module\";s:7:\"options\";s:6:\"active\";i:1;s:8:\"settings\";a:1:{s:13:\"display_label\";i:1;}}s:8:\"settings\";a:1:{s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"2\";s:8:\"settings\";a:0:{}}s:6:\"teaser\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"3\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";a:1:{i:0;a:1:{s:5:\"value\";i:1;}}}',0),(137,32,'field_mt_slideshow_image','node','mt_slideshow_entry','a:6:{s:5:\"label\";s:15:\"Slideshow image\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"2\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:5:\"image\";s:6:\"weight\";s:1:\"1\";s:8:\"settings\";a:2:{s:11:\"image_style\";s:5:\"large\";s:10:\"image_link\";s:0:\"\";}s:6:\"module\";s:5:\"image\";}s:6:\"teaser\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:5:\"image\";s:6:\"weight\";s:1:\"1\";s:8:\"settings\";a:2:{s:11:\"image_style\";s:5:\"large\";s:10:\"image_link\";s:0:\"\";}s:6:\"module\";s:5:\"image\";}}s:8:\"required\";i:0;s:11:\"description\";s:314:\"<br>\r\nRecommended pixel dimensions for <strong>Fullscreen</strong> Slideshow - W: 1920 px / H: 1280 px.<br>\r\nRecommended pixel dimensions for <strong>Full-Width</strong> Slideshow - W: 1920 px / H: 630 px.<br>\r\nRecommended pixel dimensions for <strong>Boxed-Width</strong> Slideshow - W: 1140 px / H: 630 px.\r\n<br>\";}',0),(138,34,'field_mt_slideshow_path','node','mt_slideshow_entry','a:7:{s:5:\"label\";s:14:\"Slideshow path\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"4\";s:4:\"type\";s:14:\"text_textfield\";s:6:\"module\";s:4:\"text\";s:6:\"active\";i:1;s:8:\"settings\";a:1:{s:4:\"size\";s:2:\"60\";}}s:8:\"settings\";a:2:{s:15:\"text_processing\";s:1:\"0\";s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"3\";s:8:\"settings\";a:0:{}}s:6:\"teaser\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"2\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:133:\"The path for this slideshow entry. This can be an internal Drupal path such as node/add or an external URL such as http://drupal.org.\";s:13:\"default_value\";N;}',0),(139,33,'field_mt_promoted_on_slideshow','node','webform','a:7:{s:5:\"label\";s:21:\"Promoted on slideshow\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"5\";s:4:\"type\";s:13:\"options_onoff\";s:6:\"module\";s:7:\"options\";s:6:\"active\";i:1;s:8:\"settings\";a:1:{s:13:\"display_label\";i:1;}}s:8:\"settings\";a:1:{s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"4\";s:8:\"settings\";a:0:{}}s:6:\"teaser\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"0\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";a:1:{i:0;a:1:{s:5:\"value\";i:0;}}}',0),(140,32,'field_mt_slideshow_image','node','webform','a:6:{s:5:\"label\";s:15:\"Slideshow image\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"4\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"5\";s:8:\"settings\";a:0:{}}s:6:\"teaser\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"0\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:314:\"<br>\r\nRecommended pixel dimensions for <strong>Fullscreen</strong> Slideshow - W: 1920 px / H: 1280 px.<br>\r\nRecommended pixel dimensions for <strong>Full-Width</strong> Slideshow - W: 1920 px / H: 630 px.<br>\r\nRecommended pixel dimensions for <strong>Boxed-Width</strong> Slideshow - W: 1140 px / H: 630 px.\r\n<br>\";}',0),(141,35,'field_mt_banner_image','node','mt_benefit','a:6:{s:5:\"label\";s:24:\"Internal banner image(s)\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"1\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"5\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:176:\"The image(s) you will use in this field, will be used on the internal node page, in the slideshow right below the header. Recommended pixel dimensions - W: 1920 px / H: 630 px.\";}',0),(142,35,'field_mt_banner_image','node','blog','a:6:{s:5:\"label\";s:24:\"Internal banner image(s)\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"1\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"4\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:176:\"The image(s) you will use in this field, will be used on the internal node page, in the slideshow right below the header. Recommended pixel dimensions - W: 1920 px / H: 630 px.\";}',0),(143,35,'field_mt_banner_image','node','mt_product','a:6:{s:5:\"label\";s:24:\"Internal banner image(s)\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"1\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:2:\"11\";s:8:\"settings\";a:0:{}}s:6:\"teaser\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"9\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:176:\"The image(s) you will use in this field, will be used on the internal node page, in the slideshow right below the header. Recommended pixel dimensions - W: 1920 px / H: 630 px.\";}',0),(144,35,'field_mt_banner_image','node','mt_service','a:6:{s:5:\"label\";s:24:\"Internal banner image(s)\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"1\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"9\";s:8:\"settings\";a:0:{}}s:6:\"teaser\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:2:\"10\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:176:\"The image(s) you will use in this field, will be used on the internal node page, in the slideshow right below the header. Recommended pixel dimensions - W: 1920 px / H: 630 px.\";}',0),(145,35,'field_mt_banner_image','node','mt_showcase','a:6:{s:5:\"label\";s:24:\"Internal banner image(s)\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"1\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"9\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:176:\"The image(s) you will use in this field, will be used on the internal node page, in the slideshow right below the header. Recommended pixel dimensions - W: 1920 px / H: 630 px.\";}',0),(147,35,'field_mt_banner_image','node','webform','a:6:{s:5:\"label\";s:24:\"Internal banner image(s)\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"1\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"3\";s:8:\"settings\";a:0:{}}s:6:\"teaser\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"0\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:176:\"The image(s) you will use in this field, will be used on the internal node page, in the slideshow right below the header. Recommended pixel dimensions - W: 1920 px / H: 630 px.\";}',0),(148,36,'field_mt_subtitle','node','mt_showcase','a:7:{s:5:\"label\";s:8:\"Subtitle\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"3\";s:4:\"type\";s:26:\"text_textarea_with_summary\";s:6:\"module\";s:4:\"text\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:4:\"rows\";s:1:\"2\";s:12:\"summary_rows\";i:5;}}s:8:\"settings\";a:3:{s:15:\"text_processing\";s:1:\"1\";s:15:\"display_summary\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"1\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(149,36,'field_mt_subtitle','node','mt_testimonial','a:7:{s:5:\"label\";s:8:\"Subtitle\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"4\";s:4:\"type\";s:26:\"text_textarea_with_summary\";s:6:\"module\";s:4:\"text\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:4:\"rows\";s:1:\"2\";s:12:\"summary_rows\";i:5;}}s:8:\"settings\";a:3:{s:15:\"text_processing\";s:1:\"1\";s:15:\"display_summary\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"2\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}s:6:\"teaser\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"3\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(150,36,'field_mt_subtitle','node','mt_team_member','a:7:{s:5:\"label\";s:8:\"Subtitle\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"2\";s:4:\"type\";s:26:\"text_textarea_with_summary\";s:6:\"module\";s:4:\"text\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:4:\"rows\";s:1:\"2\";s:12:\"summary_rows\";i:5;}}s:8:\"settings\";a:3:{s:15:\"text_processing\";s:1:\"1\";s:15:\"display_summary\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"0\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(151,37,'field_mt_showcase_tags','node','mt_showcase','a:7:{s:5:\"label\";s:13:\"Showcase Tags\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"7\";s:4:\"type\";s:21:\"taxonomy_autocomplete\";s:6:\"module\";s:8:\"taxonomy\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:4:\"size\";i:60;s:17:\"autocomplete_path\";s:21:\"taxonomy/autocomplete\";}}s:8:\"settings\";a:1:{s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:28:\"taxonomy_term_reference_link\";s:6:\"weight\";s:1:\"4\";s:8:\"settings\";a:0:{}s:6:\"module\";s:8:\"taxonomy\";}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(152,38,'field_mt_member_photo','node','mt_team_member','a:6:{s:5:\"label\";s:12:\"Member Photo\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"3\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:5:\"image\";s:6:\"weight\";s:1:\"1\";s:8:\"settings\";a:2:{s:11:\"image_style\";s:6:\"square\";s:10:\"image_link\";s:0:\"\";}s:6:\"module\";s:5:\"image\";}}s:8:\"required\";i:0;s:11:\"description\";s:53:\"Recommended pixel dimensions - W: 750 px / H: 750 px.\";}',0),(153,39,'field_mt_facebook_account','node','mt_team_member','a:7:{s:5:\"label\";s:8:\"Facebook\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"4\";s:4:\"type\";s:14:\"text_textfield\";s:6:\"module\";s:4:\"text\";s:6:\"active\";i:1;s:8:\"settings\";a:1:{s:4:\"size\";s:2:\"60\";}}s:8:\"settings\";a:2:{s:15:\"text_processing\";s:1:\"0\";s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"2\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";i:0;s:11:\"description\";s:83:\"Enter the URL of your Facebook profile, e.g.: https://www.facebook.com/your-account\";s:13:\"default_value\";N;}',0),(154,40,'field_mt_twitter_account','node','mt_team_member','a:7:{s:5:\"label\";s:7:\"Twitter\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"5\";s:4:\"type\";s:14:\"text_textfield\";s:6:\"module\";s:4:\"text\";s:6:\"active\";i:1;s:8:\"settings\";a:1:{s:4:\"size\";s:2:\"60\";}}s:8:\"settings\";a:2:{s:15:\"text_processing\";s:1:\"0\";s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"3\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";i:0;s:11:\"description\";s:77:\"Enter the URL of your Twitter profile, e.g.: https://twitter.com/your-account\";s:13:\"default_value\";N;}',0),(155,41,'field_mt_linkedin_account','node','mt_team_member','a:7:{s:5:\"label\";s:8:\"Linkedin\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"6\";s:4:\"type\";s:14:\"text_textfield\";s:6:\"module\";s:4:\"text\";s:6:\"active\";i:1;s:8:\"settings\";a:1:{s:4:\"size\";s:2:\"60\";}}s:8:\"settings\";a:2:{s:15:\"text_processing\";s:1:\"0\";s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"4\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";i:0;s:11:\"description\";s:86:\"Enter the URL of your Linkedin profile, e.g.: https://www.linkedin.com/in/your-account\";s:13:\"default_value\";N;}',0),(156,42,'field_mt_testimonial_image','node','mt_testimonial','a:6:{s:5:\"label\";s:17:\"Testimonial Image\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"2\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:5:\"image\";s:6:\"weight\";s:1:\"0\";s:8:\"settings\";a:2:{s:11:\"image_style\";s:9:\"thumbnail\";s:10:\"image_link\";s:0:\"\";}s:6:\"module\";s:5:\"image\";}s:6:\"teaser\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:5:\"image\";s:6:\"weight\";s:1:\"1\";s:8:\"settings\";a:2:{s:11:\"image_style\";s:9:\"thumbnail\";s:10:\"image_link\";s:7:\"content\";}s:6:\"module\";s:5:\"image\";}}s:8:\"required\";i:0;s:11:\"description\";s:53:\"Recommended pixel dimensions - W: 100 px / H: 100 px.\";}',0),(157,43,'field_mt_highlight','node','mt_product','a:7:{s:5:\"label\";s:9:\"Highlight\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"4\";s:4:\"type\";s:22:\"field_collection_embed\";s:6:\"module\";s:16:\"field_collection\";s:6:\"active\";i:0;s:8:\"settings\";a:0:{}}s:8:\"settings\";a:1:{s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:21:\"field_collection_view\";s:6:\"weight\";s:1:\"2\";s:8:\"settings\";a:5:{s:4:\"edit\";s:4:\"Edit\";s:6:\"delete\";s:6:\"Delete\";s:3:\"add\";s:0:\"\";s:11:\"description\";i:1;s:9:\"view_mode\";s:4:\"full\";}s:6:\"module\";s:16:\"field_collection\";}s:6:\"teaser\";a:4:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"2\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(159,45,'field_mt_highlight_image','field_collection_item','field_mt_highlight','a:6:{s:5:\"label\";s:5:\"Image\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"2\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:5:\"image\";s:6:\"weight\";s:1:\"0\";s:8:\"settings\";a:2:{s:11:\"image_style\";s:5:\"large\";s:10:\"image_link\";s:0:\"\";}s:6:\"module\";s:5:\"image\";}}s:8:\"required\";i:0;s:11:\"description\";s:53:\"Recommended pixel dimensions - W: 750 px / H: 500 px.\";}',0),(160,43,'field_mt_highlight','node','mt_service','a:7:{s:5:\"label\";s:9:\"Highlight\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"4\";s:4:\"type\";s:22:\"field_collection_embed\";s:6:\"module\";s:16:\"field_collection\";s:6:\"active\";i:0;s:8:\"settings\";a:0:{}}s:8:\"settings\";a:1:{s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:21:\"field_collection_view\";s:6:\"weight\";s:1:\"2\";s:8:\"settings\";a:5:{s:4:\"edit\";s:4:\"Edit\";s:6:\"delete\";s:6:\"Delete\";s:3:\"add\";s:0:\"\";s:11:\"description\";i:1;s:9:\"view_mode\";s:4:\"full\";}s:6:\"module\";s:16:\"field_collection\";}s:6:\"teaser\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"7\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(161,46,'field_mt_special_feature','node','mt_service','a:7:{s:5:\"label\";s:16:\"Special Features\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"5\";s:4:\"type\";s:22:\"field_collection_embed\";s:6:\"module\";s:16:\"field_collection\";s:6:\"active\";i:0;s:8:\"settings\";a:0:{}}s:8:\"settings\";a:1:{s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:5:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:21:\"field_collection_view\";s:6:\"weight\";s:1:\"3\";s:8:\"settings\";a:5:{s:4:\"edit\";s:4:\"Edit\";s:6:\"delete\";s:6:\"Delete\";s:3:\"add\";s:0:\"\";s:11:\"description\";i:1;s:9:\"view_mode\";s:4:\"full\";}s:6:\"module\";s:16:\"field_collection\";}s:6:\"teaser\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"6\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(163,48,'field_mt_feature_title','field_collection_item','field_mt_special_feature','a:7:{s:5:\"label\";s:5:\"Title\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"2\";s:4:\"type\";s:14:\"text_textfield\";s:6:\"module\";s:4:\"text\";s:6:\"active\";i:1;s:8:\"settings\";a:1:{s:4:\"size\";s:2:\"60\";}}s:8:\"settings\";a:2:{s:15:\"text_processing\";s:1:\"0\";s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"2\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(164,49,'field_mt_feature_subtitle','field_collection_item','field_mt_special_feature','a:7:{s:5:\"label\";s:8:\"Subtitle\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"3\";s:4:\"type\";s:26:\"text_textarea_with_summary\";s:6:\"module\";s:4:\"text\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:4:\"rows\";s:1:\"2\";s:12:\"summary_rows\";i:5;}}s:8:\"settings\";a:3:{s:15:\"text_processing\";s:1:\"1\";s:15:\"display_summary\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"3\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(166,51,'field_mt_feature_body','field_collection_item','field_mt_special_feature','a:7:{s:5:\"label\";s:4:\"Body\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"4\";s:4:\"type\";s:26:\"text_textarea_with_summary\";s:6:\"module\";s:4:\"text\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:4:\"rows\";s:1:\"5\";s:12:\"summary_rows\";i:5;}}s:8:\"settings\";a:3:{s:15:\"text_processing\";s:1:\"1\";s:15:\"display_summary\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"4\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(167,52,'field_mt_feature_image','field_collection_item','field_mt_special_feature','a:6:{s:5:\"label\";s:5:\"Image\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"1\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:0;s:11:\"title_field\";i:0;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:5:\"image\";s:6:\"weight\";s:1:\"0\";s:8:\"settings\";a:2:{s:11:\"image_style\";s:10:\"icon_style\";s:10:\"image_link\";s:0:\"\";}s:6:\"module\";s:5:\"image\";}}s:8:\"required\";i:0;s:11:\"description\";s:53:\"Recommended pixel dimensions - W: 100 px / H: 100 px.\";}',0),(168,53,'field_mt_feature_font_awesome','field_collection_item','field_mt_special_feature','a:7:{s:5:\"label\";s:18:\"Font Awesome class\";s:6:\"widget\";a:5:{s:6:\"weight\";i:0;s:4:\"type\";s:14:\"text_textfield\";s:6:\"module\";s:4:\"text\";s:6:\"active\";i:1;s:8:\"settings\";a:1:{s:4:\"size\";s:2:\"60\";}}s:8:\"settings\";a:2:{s:15:\"text_processing\";s:1:\"0\";s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"1\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";i:0;s:11:\"description\";s:268:\"Enter the class of the icon you want from the Font Awesome library e.g.: fa-film. A list of the available classes is provided here: <a href=\"http://fortawesome.github.io/Font-Awesome/cheatsheet\" target=\"_blank\">http://fortawesome.github.io/Font-Awesome/cheatsheet</a>.\";s:13:\"default_value\";N;}',0),(169,54,'field_mt_standard_feature','node','mt_service','a:7:{s:5:\"label\";s:17:\"Standard Features\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"6\";s:4:\"type\";s:22:\"field_collection_embed\";s:6:\"module\";s:16:\"field_collection\";s:6:\"active\";i:0;s:8:\"settings\";a:0:{}}s:8:\"settings\";a:1:{s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:5:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:21:\"field_collection_view\";s:6:\"weight\";s:1:\"4\";s:8:\"settings\";a:5:{s:4:\"edit\";s:4:\"Edit\";s:6:\"delete\";s:6:\"Delete\";s:3:\"add\";s:0:\"\";s:11:\"description\";i:1;s:9:\"view_mode\";s:4:\"full\";}s:6:\"module\";s:16:\"field_collection\";}s:6:\"teaser\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"3\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(170,48,'field_mt_feature_title','field_collection_item','field_mt_standard_feature','a:7:{s:5:\"label\";s:5:\"Title\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"1\";s:4:\"type\";s:14:\"text_textfield\";s:6:\"module\";s:4:\"text\";s:6:\"active\";i:1;s:8:\"settings\";a:1:{s:4:\"size\";s:2:\"60\";}}s:8:\"settings\";a:2:{s:15:\"text_processing\";s:1:\"0\";s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"1\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(171,52,'field_mt_feature_image','field_collection_item','field_mt_standard_feature','a:6:{s:5:\"label\";s:5:\"Image\";s:6:\"widget\";a:5:{s:6:\"weight\";i:0;s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:5:\"image\";s:6:\"weight\";s:1:\"0\";s:8:\"settings\";a:2:{s:11:\"image_style\";s:5:\"large\";s:10:\"image_link\";s:0:\"\";}s:6:\"module\";s:5:\"image\";}}s:8:\"required\";i:0;s:11:\"description\";s:53:\"Recommended pixel dimensions - W: 750 px / H: 500 px.\";}',0),(172,51,'field_mt_feature_body','field_collection_item','field_mt_standard_feature','a:7:{s:5:\"label\";s:4:\"Body\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"2\";s:4:\"type\";s:26:\"text_textarea_with_summary\";s:6:\"module\";s:4:\"text\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:4:\"rows\";s:1:\"5\";s:12:\"summary_rows\";i:5;}}s:8:\"settings\";a:3:{s:15:\"text_processing\";s:1:\"1\";s:15:\"display_summary\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"2\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(173,55,'field_mt_highlight_title','field_collection_item','field_mt_highlight','a:7:{s:5:\"label\";s:5:\"Title\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"3\";s:4:\"type\";s:14:\"text_textfield\";s:6:\"module\";s:4:\"text\";s:6:\"active\";i:1;s:8:\"settings\";a:1:{s:4:\"size\";s:2:\"60\";}}s:8:\"settings\";a:2:{s:15:\"text_processing\";s:1:\"0\";s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"1\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(174,56,'field_mt_highlight_body','field_collection_item','field_mt_highlight','a:7:{s:5:\"label\";s:4:\"Body\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"4\";s:4:\"type\";s:26:\"text_textarea_with_summary\";s:6:\"module\";s:4:\"text\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:4:\"rows\";s:1:\"5\";s:12:\"summary_rows\";i:5;}}s:8:\"settings\";a:3:{s:15:\"text_processing\";s:1:\"1\";s:15:\"display_summary\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"2\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(175,57,'field_mt_highlight_link','field_collection_item','field_mt_highlight','a:7:{s:5:\"label\";s:4:\"Link\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"5\";s:4:\"type\";s:10:\"link_field\";s:6:\"module\";s:4:\"link\";s:6:\"active\";i:0;s:8:\"settings\";a:0:{}}s:8:\"settings\";a:12:{s:12:\"absolute_url\";i:1;s:12:\"validate_url\";i:1;s:3:\"url\";i:0;s:5:\"title\";s:8:\"optional\";s:11:\"title_value\";s:0:\"\";s:27:\"title_label_use_field_label\";i:0;s:15:\"title_maxlength\";s:3:\"128\";s:7:\"display\";a:1:{s:10:\"url_cutoff\";s:2:\"80\";}s:10:\"attributes\";a:6:{s:6:\"target\";s:4:\"user\";s:3:\"rel\";s:0:\"\";s:18:\"configurable_class\";i:0;s:5:\"class\";s:4:\"more\";s:18:\"configurable_title\";i:0;s:5:\"title\";s:0:\"\";}s:10:\"rel_remove\";s:7:\"default\";s:13:\"enable_tokens\";i:1;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"link_default\";s:6:\"weight\";s:1:\"3\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"link\";}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(176,46,'field_mt_special_feature','node','mt_product','a:7:{s:5:\"label\";s:16:\"Special Features\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"5\";s:4:\"type\";s:22:\"field_collection_embed\";s:6:\"module\";s:16:\"field_collection\";s:6:\"active\";i:0;s:8:\"settings\";a:0:{}}s:8:\"settings\";a:1:{s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:5:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:21:\"field_collection_view\";s:6:\"weight\";s:1:\"3\";s:8:\"settings\";a:5:{s:4:\"edit\";s:4:\"Edit\";s:6:\"delete\";s:6:\"Delete\";s:3:\"add\";s:0:\"\";s:11:\"description\";i:1;s:9:\"view_mode\";s:4:\"full\";}s:6:\"module\";s:16:\"field_collection\";}s:6:\"teaser\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"6\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(177,54,'field_mt_standard_feature','node','mt_product','a:7:{s:5:\"label\";s:17:\"Standard Features\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"6\";s:4:\"type\";s:22:\"field_collection_embed\";s:6:\"module\";s:16:\"field_collection\";s:6:\"active\";i:0;s:8:\"settings\";a:0:{}}s:8:\"settings\";a:1:{s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:5:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:21:\"field_collection_view\";s:6:\"weight\";s:1:\"4\";s:8:\"settings\";a:5:{s:4:\"edit\";s:4:\"Edit\";s:6:\"delete\";s:6:\"Delete\";s:3:\"add\";s:0:\"\";s:11:\"description\";i:1;s:9:\"view_mode\";s:4:\"full\";}s:6:\"module\";s:16:\"field_collection\";}s:6:\"teaser\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"3\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(180,36,'field_mt_subtitle','node','mt_benefit','a:7:{s:5:\"label\";s:4:\"Stat\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"2\";s:4:\"type\";s:26:\"text_textarea_with_summary\";s:6:\"module\";s:4:\"text\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:4:\"rows\";s:1:\"1\";s:12:\"summary_rows\";i:5;}}s:8:\"settings\";a:3:{s:15:\"text_processing\";s:1:\"0\";s:15:\"display_summary\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:4:{s:5:\"label\";s:6:\"inline\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"0\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(182,60,'field_mt_portrait_image','node','mt_product','a:6:{s:5:\"label\";s:14:\"Portrait Image\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"7\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:2:\"14\";s:8:\"settings\";a:0:{}}s:6:\"teaser\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"5\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:144:\"Optional image, used only on selected Views-driven pages with items listed using Masonry. Recommended pixel dimensions - W: 750 px / H: 1000 px.\";}',0),(183,60,'field_mt_portrait_image','node','mt_service','a:6:{s:5:\"label\";s:14:\"Portrait Image\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"7\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:2:\"12\";s:8:\"settings\";a:0:{}}s:6:\"teaser\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"5\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:144:\"Optional image, used only on selected Views-driven pages with items listed using Masonry. Recommended pixel dimensions - W: 750 px / H: 1000 px.\";}',0),(184,61,'field_mt_service_tags','node','mt_service','a:7:{s:5:\"label\";s:12:\"Service Tags\";s:6:\"widget\";a:5:{s:6:\"weight\";s:2:\"11\";s:4:\"type\";s:21:\"taxonomy_autocomplete\";s:6:\"module\";s:8:\"taxonomy\";s:6:\"active\";i:0;s:8:\"settings\";a:2:{s:4:\"size\";i:60;s:17:\"autocomplete_path\";s:21:\"taxonomy/autocomplete\";}}s:8:\"settings\";a:1:{s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:28:\"taxonomy_term_reference_link\";s:6:\"weight\";s:1:\"8\";s:8:\"settings\";a:0:{}s:6:\"module\";s:8:\"taxonomy\";}s:6:\"teaser\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:28:\"taxonomy_term_reference_link\";s:6:\"weight\";s:1:\"2\";s:8:\"settings\";a:0:{}s:6:\"module\";s:8:\"taxonomy\";}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(187,60,'field_mt_portrait_image','node','mt_showcase','a:6:{s:5:\"label\";s:14:\"Portrait Image\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"6\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"8\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:144:\"Optional image, used only on selected Views-driven pages with items listed using Masonry. Recommended pixel dimensions - W: 750 px / H: 1000 px.\";}',0),(188,63,'field_mt_product_tags','node','mt_product','a:7:{s:5:\"label\";s:12:\"Product Tags\";s:6:\"widget\";a:5:{s:6:\"weight\";s:2:\"13\";s:4:\"type\";s:21:\"taxonomy_autocomplete\";s:6:\"module\";s:8:\"taxonomy\";s:6:\"active\";i:0;s:8:\"settings\";a:2:{s:4:\"size\";i:60;s:17:\"autocomplete_path\";s:21:\"taxonomy/autocomplete\";}}s:8:\"settings\";a:1:{s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:28:\"taxonomy_term_reference_link\";s:6:\"weight\";s:1:\"8\";s:8:\"settings\";a:0:{}s:6:\"module\";s:8:\"taxonomy\";}s:6:\"teaser\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:28:\"taxonomy_term_reference_link\";s:6:\"weight\";s:1:\"1\";s:8:\"settings\";a:0:{}s:6:\"module\";s:8:\"taxonomy\";}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(191,66,'field_mt_landscape_image','node','blog','a:6:{s:5:\"label\";s:15:\"Landscape Image\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"4\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:0;s:11:\"title_field\";i:0;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"3\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:143:\"Optional image, used only on selected Views-driven pages with items listed using Masonry. Recommended pixel dimensions - W: 750 px / H: 354 px.\";}',0),(192,66,'field_mt_landscape_image','node','article','a:6:{s:5:\"label\";s:15:\"Landscape Image\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"5\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:0;s:11:\"title_field\";i:0;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"4\";s:8:\"settings\";a:0:{}}s:6:\"teaser\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"0\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:143:\"Optional image, used only on selected Views-driven pages with items listed using Masonry. Recommended pixel dimensions - W: 750 px / H: 354 px.\";}',0),(193,30,'field_mt_subheader_body','node','webform','a:7:{s:5:\"label\";s:14:\"Subheader body\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"2\";s:4:\"type\";s:26:\"text_textarea_with_summary\";s:6:\"module\";s:4:\"text\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:4:\"rows\";s:1:\"4\";s:12:\"summary_rows\";i:5;}}s:8:\"settings\";a:3:{s:15:\"text_processing\";s:1:\"1\";s:15:\"display_summary\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"0\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(194,35,'field_mt_banner_image','node','page','a:6:{s:5:\"label\";s:24:\"Internal banner image(s)\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"1\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"2\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:176:\"The image(s) you will use in this field, will be used on the internal node page, in the slideshow right below the header. Recommended pixel dimensions - W: 1920 px / H: 630 px.\";}',0),(195,67,'field_mt_pricing_table_item','node','mt_product','a:7:{s:5:\"label\";s:21:\"Pricing Table Feature\";s:6:\"widget\";a:5:{s:6:\"weight\";s:2:\"10\";s:4:\"type\";s:14:\"text_textfield\";s:6:\"module\";s:4:\"text\";s:6:\"active\";i:1;s:8:\"settings\";a:1:{s:4:\"size\";s:2:\"60\";}}s:8:\"settings\";a:2:{s:15:\"text_processing\";s:1:\"0\";s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:4:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"9\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(196,68,'field_mt_price','node','mt_product','a:7:{s:5:\"label\";s:5:\"Price\";s:6:\"widget\";a:5:{s:6:\"weight\";s:2:\"11\";s:4:\"type\";s:14:\"text_textfield\";s:6:\"module\";s:4:\"text\";s:6:\"active\";i:1;s:8:\"settings\";a:1:{s:4:\"size\";s:2:\"60\";}}s:8:\"settings\";a:2:{s:15:\"text_processing\";s:1:\"0\";s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"7\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(197,69,'field_mt_most_popular','node','mt_product','a:7:{s:5:\"label\";s:12:\"Most Popular\";s:6:\"widget\";a:5:{s:6:\"weight\";s:2:\"12\";s:4:\"type\";s:13:\"options_onoff\";s:6:\"module\";s:7:\"options\";s:6:\"active\";i:1;s:8:\"settings\";a:1:{s:13:\"display_label\";i:1;}}s:8:\"settings\";a:1:{s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:2:\"10\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:83:\"Check the box to highlight this Product as “most popular” in the pricing table.\";s:13:\"default_value\";a:1:{i:0;a:1:{s:5:\"value\";i:0;}}}',0),(198,32,'field_mt_slideshow_image','node','article','a:6:{s:5:\"label\";s:15:\"Slideshow image\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"6\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"6\";s:8:\"settings\";a:0:{}}s:6:\"teaser\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"0\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:314:\"<br>\r\nRecommended pixel dimensions for <strong>Fullscreen</strong> Slideshow - W: 1920 px / H: 1280 px.<br>\r\nRecommended pixel dimensions for <strong>Full-Width</strong> Slideshow - W: 1920 px / H: 630 px.<br>\r\nRecommended pixel dimensions for <strong>Boxed-Width</strong> Slideshow - W: 1140 px / H: 630 px.\r\n<br>\";}',0),(199,33,'field_mt_promoted_on_slideshow','node','article','a:7:{s:5:\"label\";s:21:\"Promoted on slideshow\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"7\";s:4:\"type\";s:13:\"options_onoff\";s:6:\"module\";s:7:\"options\";s:6:\"active\";i:1;s:8:\"settings\";a:1:{s:13:\"display_label\";i:1;}}s:8:\"settings\";a:1:{s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"5\";s:8:\"settings\";a:0:{}}s:6:\"teaser\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"0\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";a:1:{i:0;a:1:{s:5:\"value\";i:0;}}}',0),(200,32,'field_mt_slideshow_image','node','mt_team_member','a:6:{s:5:\"label\";s:15:\"Slideshow image\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"8\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"7\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:314:\"<br>\r\nRecommended pixel dimensions for <strong>Fullscreen</strong> Slideshow - W: 1920 px / H: 1280 px.<br>\r\nRecommended pixel dimensions for <strong>Full-Width</strong> Slideshow - W: 1920 px / H: 630 px.<br>\r\nRecommended pixel dimensions for <strong>Boxed-Width</strong> Slideshow - W: 1140 px / H: 630 px.\r\n<br>\";}',0),(201,33,'field_mt_promoted_on_slideshow','node','mt_team_member','a:7:{s:5:\"label\";s:21:\"Promoted on slideshow\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"9\";s:4:\"type\";s:13:\"options_onoff\";s:6:\"module\";s:7:\"options\";s:6:\"active\";i:1;s:8:\"settings\";a:1:{s:13:\"display_label\";i:1;}}s:8:\"settings\";a:1:{s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"8\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";a:1:{i:0;a:1:{s:5:\"value\";i:0;}}}',0),(202,32,'field_mt_slideshow_image','node','page','a:6:{s:5:\"label\";s:15:\"Slideshow image\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"5\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"3\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:314:\"<br>\r\nRecommended pixel dimensions for <strong>Fullscreen</strong> Slideshow - W: 1920 px / H: 1280 px.<br>\r\nRecommended pixel dimensions for <strong>Full-Width</strong> Slideshow - W: 1920 px / H: 630 px.<br>\r\nRecommended pixel dimensions for <strong>Boxed-Width</strong> Slideshow - W: 1140 px / H: 630 px.\r\n<br>\";}',0),(203,33,'field_mt_promoted_on_slideshow','node','page','a:7:{s:5:\"label\";s:21:\"Promoted on slideshow\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"7\";s:4:\"type\";s:13:\"options_onoff\";s:6:\"module\";s:7:\"options\";s:6:\"active\";i:1;s:8:\"settings\";a:1:{s:13:\"display_label\";i:1;}}s:8:\"settings\";a:1:{s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"4\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";a:1:{i:0;a:1:{s:5:\"value\";i:0;}}}',0),(204,32,'field_mt_slideshow_image','node','mt_testimonial','a:6:{s:5:\"label\";s:15:\"Slideshow image\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"5\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"4\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:314:\"<br>\r\nRecommended pixel dimensions for <strong>Fullscreen</strong> Slideshow - W: 1920 px / H: 1280 px.<br>\r\nRecommended pixel dimensions for <strong>Full-Width</strong> Slideshow - W: 1920 px / H: 630 px.<br>\r\nRecommended pixel dimensions for <strong>Boxed-Width</strong> Slideshow - W: 1140 px / H: 630 px.\r\n<br>\";}',0),(205,33,'field_mt_promoted_on_slideshow','node','mt_testimonial','a:7:{s:5:\"label\";s:21:\"Promoted on slideshow\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"6\";s:4:\"type\";s:13:\"options_onoff\";s:6:\"module\";s:7:\"options\";s:6:\"active\";i:1;s:8:\"settings\";a:1:{s:13:\"display_label\";i:1;}}s:8:\"settings\";a:1:{s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"5\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";a:1:{i:0;a:1:{s:5:\"value\";i:0;}}}',0),(206,35,'field_mt_banner_image','node','article','a:6:{s:5:\"label\";s:24:\"Internal banner image(s)\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"1\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:2:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"3\";s:8:\"settings\";a:0:{}}s:6:\"teaser\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"0\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:176:\"The image(s) you will use in this field, will be used on the internal node page, in the slideshow right below the header. Recommended pixel dimensions - W: 1920 px / H: 630 px.\";}',0),(207,35,'field_mt_banner_image','node','mt_team_member','a:6:{s:5:\"label\";s:24:\"Internal banner image(s)\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"1\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"6\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:176:\"The image(s) you will use in this field, will be used on the internal node page, in the slideshow right below the header. Recommended pixel dimensions - W: 1920 px / H: 630 px.\";}',0),(208,35,'field_mt_banner_image','node','mt_testimonial','a:6:{s:5:\"label\";s:24:\"Internal banner image(s)\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"1\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:4:{s:5:\"label\";s:5:\"above\";s:4:\"type\";s:6:\"hidden\";s:6:\"weight\";s:1:\"3\";s:8:\"settings\";a:0:{}}}s:8:\"required\";i:0;s:11:\"description\";s:176:\"The image(s) you will use in this field, will be used on the internal node page, in the slideshow right below the header. Recommended pixel dimensions - W: 1920 px / H: 630 px.\";}',0),(209,68,'field_mt_price','node','mt_service','a:7:{s:5:\"label\";s:5:\"Price\";s:6:\"widget\";a:5:{s:6:\"weight\";s:2:\"10\";s:4:\"type\";s:14:\"text_textfield\";s:6:\"module\";s:4:\"text\";s:6:\"active\";i:1;s:8:\"settings\";a:1:{s:4:\"size\";s:2:\"60\";}}s:8:\"settings\";a:2:{s:15:\"text_processing\";s:1:\"0\";s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"7\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(210,70,'field_mt_content_bottom','node','mt_product','a:7:{s:5:\"label\";s:14:\"Content Bottom\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"9\";s:4:\"type\";s:26:\"text_textarea_with_summary\";s:6:\"module\";s:4:\"text\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:4:\"rows\";s:1:\"5\";s:12:\"summary_rows\";i:5;}}s:8:\"settings\";a:3:{s:15:\"text_processing\";s:1:\"1\";s:15:\"display_summary\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"6\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(211,70,'field_mt_content_bottom','node','mt_service','a:7:{s:5:\"label\";s:14:\"Content Bottom\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"9\";s:4:\"type\";s:26:\"text_textarea_with_summary\";s:6:\"module\";s:4:\"text\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:4:\"rows\";s:1:\"5\";s:12:\"summary_rows\";i:5;}}s:8:\"settings\";a:3:{s:15:\"text_processing\";s:1:\"1\";s:15:\"display_summary\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"6\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(212,71,'field_mt_company_title','node','mt_showcase','a:7:{s:5:\"label\";s:13:\"Company Title\";s:6:\"widget\";a:5:{s:6:\"weight\";s:1:\"9\";s:4:\"type\";s:14:\"text_textfield\";s:6:\"module\";s:4:\"text\";s:6:\"active\";i:1;s:8:\"settings\";a:1:{s:4:\"size\";s:2:\"60\";}}s:8:\"settings\";a:2:{s:15:\"text_processing\";s:1:\"1\";s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"6\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0),(213,72,'field_mt_company_logo','node','mt_showcase','a:6:{s:5:\"label\";s:12:\"Company Logo\";s:6:\"widget\";a:5:{s:6:\"weight\";s:2:\"10\";s:4:\"type\";s:11:\"image_image\";s:6:\"module\";s:5:\"image\";s:6:\"active\";i:1;s:8:\"settings\";a:2:{s:18:\"progress_indicator\";s:8:\"throbber\";s:19:\"preview_image_style\";s:9:\"thumbnail\";}}s:8:\"settings\";a:9:{s:14:\"file_directory\";s:0:\"\";s:15:\"file_extensions\";s:16:\"png gif jpg jpeg\";s:12:\"max_filesize\";s:0:\"\";s:14:\"max_resolution\";s:0:\"\";s:14:\"min_resolution\";s:0:\"\";s:9:\"alt_field\";i:1;s:11:\"title_field\";i:1;s:13:\"default_image\";i:0;s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:5:\"image\";s:6:\"weight\";s:1:\"5\";s:8:\"settings\";a:2:{s:11:\"image_style\";s:12:\"company_logo\";s:10:\"image_link\";s:0:\"\";}s:6:\"module\";s:5:\"image\";}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";}',0),(214,73,'field_mt_company_description','node','mt_showcase','a:7:{s:5:\"label\";s:19:\"Company Description\";s:6:\"widget\";a:5:{s:6:\"weight\";s:2:\"11\";s:4:\"type\";s:13:\"text_textarea\";s:6:\"module\";s:4:\"text\";s:6:\"active\";i:1;s:8:\"settings\";a:1:{s:4:\"rows\";s:1:\"5\";}}s:8:\"settings\";a:2:{s:15:\"text_processing\";s:1:\"1\";s:18:\"user_register_form\";b:0;}s:7:\"display\";a:1:{s:7:\"default\";a:5:{s:5:\"label\";s:6:\"hidden\";s:4:\"type\";s:12:\"text_default\";s:6:\"weight\";s:1:\"7\";s:8:\"settings\";a:0:{}s:6:\"module\";s:4:\"text\";}}s:8:\"required\";i:0;s:11:\"description\";s:0:\"\";s:13:\"default_value\";N;}',0);
/*!40000 ALTER TABLE `field_config_instance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_body`
--

DROP TABLE IF EXISTS `field_data_body`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_body` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `body_value` longtext,
  `body_summary` longtext,
  `body_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `body_format` (`body_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 2 (body)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_body`
--

LOCK TABLES `field_data_body` WRITE;
/*!40000 ALTER TABLE `field_data_body` DISABLE KEYS */;
INSERT INTO `field_data_body` VALUES ('node','page',0,1,1,'und',0,'<p class=\"large\">Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies appropriately communicate.</p> \r\n<h2>Heading 2</h2>\r\n<p>\r\nDonec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer <a href=\"#\">posuere erat a ante</a> venenatis dapibus posuere velit aliquet.\r\n</p>\r\n<h2><a href=\"#\">Linked Heading 2</a></h2>\r\n<p>\r\nDonec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer <a href=\"#\">posuere erat a ante</a> venenatis dapibus posuere velit aliquet.\r\n</p>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Sit, esse, quo distinctio dolores magni reprehenderit id est at fugiat veritatis fugit dignissimos sed ut facere molestias illo impedit. Tempora, iure!\r\n</p>\r\n<h3>Heading 3</h3>\r\n<p>\r\nDonec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer <a href=\"#\">posuere erat a ante</a> venenatis dapibus posuere velit aliquet.\r\n</p>\r\n<h4>Heading 4</h4>\r\n<p>\r\nDonec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer <a href=\"#\">posuere erat a ante</a> venenatis dapibus posuere velit aliquet.\r\n</p>\r\n<h5>Heading 5</h5>\r\n<p>\r\nDonec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer <a href=\"#\">posuere erat a ante</a> venenatis dapibus posuere velit aliquet.\r\n</p>\r\n<blockquote>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima at dolores culpa, consectetur nihil, voluptate quaerat, provident.</p>\r\n<footer>Johnny Trulove <cite title=\"Source Title\">Commercial Director</cite></footer>\r\n</blockquote>\r\n<h3>Messages</h3>\r\n<div class=\"messages status\">\r\nSample status message. Page <em><strong>Typography</strong></em> has been updated.\r\n</div>\r\n<div class=\"messages error\">\r\nSample error message. There is a security update available for your version of Drupal. To ensure the security of your server, you should update immediately! See the available updates page for more information.\r\n</div>\r\n<div class=\"messages warning\">\r\nSample warning message. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\r\n</div>\r\n<br>\r\n<h3>Paragraph With Links</h3>\r\n<p>\r\nLorem ipsum dolor sit amet, <a href=\"#\">consectetuer adipiscing</a> elit. Donec odio. Quisque volutpat mattis eros. <a href=\"#\">Nullam malesuada</a> erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.\r\n</p>\r\n<h3>Ordered List</h3>\r\n<ol>\r\n<li>\r\nThis is a sample Ordered List.\r\n</li>\r\n<li>\r\nLorem ipsum dolor sit amet consectetuer.\r\n</li>\r\n<li>\r\nCongue Quisque augue elit dolor.\r\n<ol>\r\n<li>\r\nSomething goes here.\r\n</li>\r\n<li>\r\nAnd another here\r\n</li>\r\n</ol>\r\n</li>\r\n<li>\r\nCongue Quisque augue elit dolor nibh.\r\n</li>\r\n</ol>\r\n\r\n<h3>Unordered List</h3>\r\n<ul>\r\n<li>\r\nThis is a sample <strong>Unordered List</strong>.\r\n</li>\r\n<li>\r\nCondimentum quis.\r\n</li>\r\n<li>\r\nCongue Quisque augue elit dolor.\r\n<ul>\r\n<li>\r\nSomething goes here.\r\n</li>\r\n<li>\r\nAnd another here\r\n<ul>\r\n<li>\r\nSomething here as well\r\n</li>\r\n<li>\r\nSomething here as well\r\n</li>\r\n<li>\r\nSomething here as well\r\n</li>\r\n</ul>\r\n</li>\r\n<li>\r\nThen one more\r\n</li>\r\n</ul>\r\n</li>\r\n<li>\r\nNunc cursus sem et pretium sapien eget.\r\n</li>\r\n</ul>\r\n\r\n<h3>Fieldset</h3>\r\n<fieldset><legend>Account information</legend></fieldset>\r\n\r\n<br>\r\n<h3>Buttons &amp; Text fields</h3>\r\n<a href=\"#\" class=\"more\">Find out more</a>\r\n<h4>Text Fields</h4>\r\n<form action=\"#\">\r\n<div class=\"form-item form-type-textfield\">\r\n<input type=\"text\" class=\"form-text\" name=\"subscribe\" value=\"Your email address\" onfocus=\"if (this.value == \'Your email address\') {this.value = \'\';}\" onblur=\"if (this.value == \'\') {this.value = \'Your email address\';}\">\r\n</div>\r\n<div class=\"form-actions\">\r\n<input value=\"Subscribe\" type=\"submit\" id=\"edit-submit-3\" name=\"subscribe\" class=\"form-submit\">\r\n</div>\r\n</form>\r\n<form action=\"#\" class=\"container-inline\">\r\n<div class=\"form-item form-type-textfield\">\r\n<input type=\"text\" class=\"form-text\" name=\"subscribe\" value=\"Inline form items\" onfocus=\"if (this.value == \'Your email address\') {this.value = \'\';}\" onblur=\"if (this.value == \'\') {this.value = \'Inline form items\';}\"></div>\r\n<div class=\"form-actions\">\r\n<input value=\"Subscribe\" type=\"submit\" id=\"edit-submit-4\" name=\"subscribe\" class=\"form-submit\">\r\n</div>\r\n</form>\r\n<h4>Select Dropdown</h4>\r\n<select name=\"select_field\" class=\"form-select\"><option value=\"All\" selected=\"selected\">- Any -</option><option value=\"6\">Apartment</option><option value=\"7\">Family House</option><option value=\"8\">Office</option></select>\r\n<br>\r\n<h3>Table</h3>\r\n<table>\r\n<tr>\r\n<th>Header 1</th>\r\n<th>Header 2</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>row 1, cell 1</td>\r\n<td>row 1, cell 2</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>row 2, cell 1</td>\r\n<td>row 2, cell 2</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>row 3, cell 1</td>\r\n<td>row 3, cell 2</td>\r\n</tr>\r\n</table>\r\n\r\n<h4>Responsive Table</h4>\r\n<div class=\"table-responsive\">\r\n<table>\r\n<tr>\r\n<th>Header 1</th>\r\n<th>Header 2</th>\r\n<th>Header 3</th>\r\n<th>Header 4</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>row 1, cell 1</td>\r\n<td>row 1, cell 2</td>\r\n<td>row 1, cell 3</td>\r\n<td>row 1, cell 4</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>row 2, cell 1</td>\r\n<td>row 2, cell 2</td>\r\n<td>row 2, cell 3</td>\r\n<td>row 2, cell 4</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>row 3, cell 1</td>\r\n<td>row 3, cell 2</td>\r\n<td>row 3, cell 3</td>\r\n<td>row 3, cell 4</td>\r\n</tr>\r\n</table>\r\n</div>','','full_html'),('node','page',0,2,2,'und',0,'<div class=\"subheader\">\r\n<p class=\"large\">Completely drive standardized initiatives with principle-centered ROI. Progressively aggregate emerging content rather than leveraged bandwidth <a href=\"#\">and tech-enabled innovative materials</a> with a touch of uniqueness.</p>\r\n</div>\r\n\r\n<div class=\"row\">\r\n\r\n<div class=\"col-md-4 col-sm-6\">\r\n<div class=\"team-member clearfix\">\r\n<a href=\"<?php print base_path();?>contact-us\" class=\"overlayed\">\r\n<span class=\"overlay\"><i class=\"fa fa-link\"></i></span>\r\n<img src=\"<?php print base_path();?>sites/default/files/about-1.jpg\" alt=\"\" />\r\n</a>\r\n<h3><a href=\"<?php print base_path();?>contact-us\">Lorem Ipsum</a></h3>\r\n<p class=\"subtitle\">Founder &amp; CEO</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n<ul class=\"contact-info\">\r\n<li class=\"phone\"><i class=\"fa fa-phone\"><span class=\"sr-only\">phone</span></i> +1 212-582-8102</li>\r\n<li class=\"email\"><i class=\"fa fa-envelope\"><span class=\"sr-only\">email</span></i> <a href=\"mailto:lorem.ipsum@levelplus.com\">lorem.ipsum@levelplus.com</a></li>\r\n</ul>\r\n<ul class=\"social-bookmarks text-center\">\r\n<li class=\"facebook\"><a href=\"http://www.facebook.com/morethan.just.themes/\"><i class=\"fa fa-facebook\"><span class=\"sr-only\">facebook</span></i></a></li>\r\n<li class=\"twitter\"><a href=\"http://twitter.com/morethanthemes/\"><i class=\"fa fa-twitter\"><span class=\"sr-only\">twitter</span></i></a></li>\r\n<li class=\"googleplus\"><a href=\"https://plus.google.com/118354321025436191714/posts\"><i class=\"fa fa-google-plus\"><span class=\"sr-only\">google plus</span></i></a></li>    \r\n<li class=\"linkedin\"><a href=\"http://www.linkedin.com/company/more-than-themes/\"><i class=\"fa fa-linkedin\"><span class=\"sr-only\">linkedin</span></i></a></li>\r\n</ul>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-4 col-sm-6\">\r\n<div class=\"team-member clearfix\">\r\n<a href=\"<?php print base_path();?>contact-us\" class=\"overlayed\">\r\n<span class=\"overlay\"><i class=\"fa fa-link\"></i></span>\r\n<img src=\"<?php print base_path();?>sites/default/files/about-2.jpg\" alt=\"\" />\r\n</a>\r\n<h3><a href=\"<?php print base_path();?>contact-us\">Lorem Ipsum</a></h3>\r\n<p class=\"subtitle\">Chief Operating Officer</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n<ul class=\"contact-info\">\r\n<li class=\"phone\"><i class=\"fa fa-phone\"><span class=\"sr-only\">phone</span></i> +1 212-582-8102</li>\r\n<li class=\"email\"><i class=\"fa fa-envelope\"><span class=\"sr-only\">email</span></i> <a href=\"mailto:lorem.ipsum@levelplus.com\">lorem.ipsum@levelplus.com</a></li>\r\n</ul>\r\n<ul class=\"social-bookmarks text-center\">\r\n<li class=\"facebook\"><a href=\"http://www.facebook.com/morethan.just.themes/\"><i class=\"fa fa-facebook\"><span class=\"sr-only\">facebook</span></i></a></li>\r\n<li class=\"twitter\"><a href=\"http://twitter.com/morethanthemes/\"><i class=\"fa fa-twitter\"><span class=\"sr-only\">twitter</span></i></a></li>\r\n<li class=\"googleplus\"><a href=\"https://plus.google.com/118354321025436191714/posts\"><i class=\"fa fa-google-plus\"><span class=\"sr-only\">google plus</span></i></a></li>\r\n<li class=\"linkedin\"><a href=\"http://www.linkedin.com/company/more-than-themes/\"><i class=\"fa fa-linkedin\"><span class=\"sr-only\">linkedin</span></i></a></li>\r\n</ul>\r\n</div>\r\n</div>\r\n\r\n<div class=\"clearfix visible-sm\"></div>\r\n\r\n<div class=\"col-md-4 col-sm-6\">\r\n<div class=\"team-member clearfix\">\r\n<a href=\"<?php print base_path();?>contact-us\" class=\"overlayed\">\r\n<span class=\"overlay\"><i class=\"fa fa-link\"></i></span>\r\n<img src=\"<?php print base_path();?>sites/default/files/about-3.jpg\" alt=\"\" />\r\n</a>\r\n<h3><a href=\"<?php print base_path();?>contact-us\">Lorem Ipsum</a></h3>\r\n<p class=\"subtitle\">Chief Financial Officer</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n<ul class=\"contact-info\">\r\n<li class=\"phone\"><i class=\"fa fa-phone\"><span class=\"sr-only\">phone</span></i> +1 212-582-8102</li>\r\n<li class=\"email\"><i class=\"fa fa-envelope\"><span class=\"sr-only\">email</span></i> <a href=\"mailto:lorem.ipsum@levelplus.com\">lorem.ipsum@levelplus.com</a></li>\r\n</ul>\r\n<ul class=\"social-bookmarks text-center\">\r\n<li class=\"facebook\"><a href=\"http://www.facebook.com/morethan.just.themes/\"><i class=\"fa fa-facebook\"><span class=\"sr-only\">facebook</span></i></a></li>\r\n<li class=\"twitter\"><a href=\"http://twitter.com/morethanthemes/\"><i class=\"fa fa-twitter\"><span class=\"sr-only\">twitter</span></i></a></li>\r\n<li class=\"googleplus\"><a href=\"https://plus.google.com/118354321025436191714/posts\"><i class=\"fa fa-google-plus\"><span class=\"sr-only\">google plus</span></i></a></li>\r\n<li class=\"linkedin\"><a href=\"http://www.linkedin.com/company/more-than-themes/\"><i class=\"fa fa-linkedin\"><span class=\"sr-only\">linkedin</span></i></a></li>\r\n</ul>\r\n</div>\r\n</div>\r\n\r\n<div class=\"clearfix visible-md visible-lg\"></div>\r\n\r\n<div class=\"col-md-4 col-sm-6\">\r\n<div class=\"team-member clearfix\">\r\n<a href=\"<?php print base_path();?>contact-us\" class=\"overlayed\">\r\n<span class=\"overlay\"><i class=\"fa fa-link\"></i></span>\r\n<img src=\"<?php print base_path();?>sites/default/files/about-4.jpg\" alt=\"\" />\r\n</a>\r\n<h3><a href=\"<?php print base_path();?>contact-us\">Lorem Ipsum</a></h3>\r\n<p class=\"subtitle\">Chief Financial Officer</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n<ul class=\"contact-info\">\r\n<li class=\"phone\"><i class=\"fa fa-phone\"><span class=\"sr-only\">phone</span></i> +1 212-582-8102</li>\r\n<li class=\"email\"><i class=\"fa fa-envelope\"><span class=\"sr-only\">email</span></i> <a href=\"mailto:lorem.ipsum@levelplus.com\">lorem.ipsum@levelplus.com</a></li>\r\n</ul>\r\n<ul class=\"social-bookmarks text-center\">\r\n<li class=\"facebook\"><a href=\"http://www.facebook.com/morethan.just.themes/\"><i class=\"fa fa-facebook\"><span class=\"sr-only\">facebook</span></i></a></li>\r\n<li class=\"twitter\"><a href=\"http://twitter.com/morethanthemes/\"><i class=\"fa fa-twitter\"><span class=\"sr-only\">twitter</span></i></a></li>\r\n<li class=\"googleplus\"><a href=\"https://plus.google.com/118354321025436191714/posts\"><i class=\"fa fa-google-plus\"><span class=\"sr-only\">google plus</span></i></a></li>\r\n<li class=\"linkedin\"><a href=\"http://www.linkedin.com/company/more-than-themes/\"><i class=\"fa fa-linkedin\"><span class=\"sr-only\">linkedin</span></i></a></li>\r\n</ul>\r\n</div>\r\n</div>\r\n\r\n<div class=\"clearfix visible-sm\"></div>\r\n\r\n<div class=\"col-md-4 col-sm-6\">\r\n<div class=\"team-member clearfix\">\r\n<a href=\"<?php print base_path();?>contact-us\" class=\"overlayed\">\r\n<span class=\"overlay\"><i class=\"fa fa-link\"></i></span>\r\n<img src=\"<?php print base_path();?>sites/default/files/about-5.jpg\" alt=\"\" />\r\n</a>\r\n<h3><a href=\"<?php print base_path();?>contact-us\">Lorem Ipsum</a></h3>\r\n<p class=\"subtitle\">Chief Financial Officer</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n<ul class=\"contact-info\">\r\n<li class=\"phone\"><i class=\"fa fa-phone\"><span class=\"sr-only\">phone</span></i> +1 212-582-8102</li>\r\n<li class=\"email\"><i class=\"fa fa-envelope\"><span class=\"sr-only\">email</span></i> <a href=\"mailto:lorem.ipsum@levelplus.com\">lorem.ipsum@levelplus.com</a></li>\r\n</ul>\r\n<ul class=\"social-bookmarks text-center\">\r\n<li class=\"facebook\"><a href=\"http://www.facebook.com/morethan.just.themes/\"><i class=\"fa fa-facebook\"><span class=\"sr-only\">facebook</span></i></a></li>\r\n<li class=\"twitter\"><a href=\"http://twitter.com/morethanthemes/\"><i class=\"fa fa-twitter\"><span class=\"sr-only\">twitter</span></i></a></li>\r\n<li class=\"googleplus\"><a href=\"https://plus.google.com/118354321025436191714/posts\"><i class=\"fa fa-google-plus\"><span class=\"sr-only\">google plus</span></i></a></li>\r\n<li class=\"linkedin\"><a href=\"http://www.linkedin.com/company/more-than-themes/\"><i class=\"fa fa-linkedin\"><span class=\"sr-only\">linkedin</span></i></a></li>\r\n</ul>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-4 col-sm-6\">\r\n<div class=\"team-member clearfix\">\r\n<a href=\"<?php print base_path();?>contact-us\" class=\"overlayed\">\r\n<span class=\"overlay\"><i class=\"fa fa-link\"></i></span>\r\n<img src=\"<?php print base_path();?>sites/default/files/about-6.jpg\" alt=\"\" />\r\n</a>\r\n<h3><a href=\"<?php print base_path();?>contact-us\">Lorem Ipsum</a></h3>\r\n<p class=\"subtitle\">Chief Financial Officer</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n<ul class=\"contact-info\">\r\n<li class=\"phone\"><i class=\"fa fa-phone\"><span class=\"sr-only\">phone</span></i> +1 212-582-8102</li>\r\n<li class=\"email\"><i class=\"fa fa-envelope\"><span class=\"sr-only\">email</span></i> <a href=\"mailto:lorem.ipsum@levelplus.com\">lorem.ipsum@levelplus.com</a></li>\r\n</ul>\r\n<ul class=\"social-bookmarks text-center\">\r\n<li class=\"facebook\"><a href=\"http://www.facebook.com/morethan.just.themes/\"><i class=\"fa fa-facebook\"><span class=\"sr-only\">facebook</span></i></a></li>\r\n<li class=\"twitter\"><a href=\"http://twitter.com/morethanthemes/\"><i class=\"fa fa-twitter\"><span class=\"sr-only\">twitter</span></i></a></li>\r\n<li class=\"googleplus\"><a href=\"https://plus.google.com/118354321025436191714/posts\"><i class=\"fa fa-google-plus\"><span class=\"sr-only\">google plus</span></i></a></li>\r\n<li class=\"linkedin\"><a href=\"http://www.linkedin.com/company/more-than-themes/\"><i class=\"fa fa-linkedin\"><span class=\"sr-only\">linkedin</span></i></a></li>\r\n</ul>\r\n</div>\r\n</div>\r\n\r\n</div>','','php_code'),('node','mt_testimonial',0,3,3,'und',0,'Excepteur sint occaecat cupidatat non proident. Lorem ipsum dolor sit amet, consectetur.','','filtered_html'),('node','page',0,8,8,'und',0,'\r\n<h1>Columns</h1>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-6\">\r\n<h4>One Half</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>\r\n</div> \r\n\r\n<div class=\"col-md-6\"> \r\n<h4>One Half</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>\r\n</div> \r\n</div>\r\n<pre style=\"margin-bottom:40px;\">\r\n&lt;div class=\"row\"&gt;\r\n\r\n&lt;div class=\"col-md-6\"&gt;\r\n&lt;h4&gt;One Half&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-6\"&gt; \r\n&lt;h4&gt;One Half&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;/div&gt;\r\n</pre>\r\n<br/>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\"> \r\n<h4>One Third</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis.</p>\r\n</div> \r\n\r\n<div class=\"col-md-4\"> \r\n<h4>One Third</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis.</p>\r\n</div> \r\n\r\n<div class=\"col-md-4\"> \r\n<h4>One Third</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis.</p>\r\n</div> \r\n</div>\r\n\r\n<pre style=\"margin-bottom:40px;\">\r\n&lt;div class=\"row\"&gt;\r\n\r\n&lt;div class=\"col-md-4\"&gt; \r\n&lt;h4&gt;One Third&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui..&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-4\"&gt; \r\n&lt;h4&gt;One Third&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui..&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-4\"&gt; \r\n&lt;h4&gt;One Third&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui..&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;/div&gt;\r\n</pre>\r\n<br/>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-3\"> \r\n<h4>One Fourth</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut.</p>\r\n</div> \r\n\r\n<div class=\"col-md-3\"> \r\n<h4>One Fourth</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut.</p>\r\n</div> \r\n\r\n<div class=\"col-md-3\"> \r\n<h4>One Fourth</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut.</p>\r\n</div>\r\n\r\n<div class=\"col-md-3\"> \r\n<h4>One Fourth</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut.</p>\r\n</div>\r\n</div>\r\n\r\n<pre style=\"margin-bottom:40px;\">\r\n&lt;div class=\"row\"&gt;\r\n\r\n&lt;div class=\"col-md-3\"&gt; \r\n&lt;h4&gt;One Fourth&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui..&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-3\"&gt; \r\n&lt;h4&gt;One Fourth&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui..&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-3\"&gt; \r\n&lt;h4&gt;One Fourth&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui..&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=\"col-md-3\"&gt; \r\n&lt;h4&gt;One Fourth&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui..&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;/div&gt;\r\n</pre>\r\n<br/>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-4\"> \r\n<h4>One Third</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut. Lorem ipsum dolor sit amet, consectetur adipisicing.</p>\r\n</div> \r\n\r\n<div class=\"col-md-8\"> \r\n<h4>Two Thirds</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint, doloribus, tempora, numquam repellendus maiores facere a atque reiciendis voluptatibus hic veritatis ratione reprehenderit.</p>\r\n</div> \r\n</div>\r\n\r\n<pre style=\"margin-bottom:40px;\">\r\n&lt;div class=\"row\"&gt;\r\n\r\n&lt;div class=\"col-md-4\"&gt; \r\n&lt;h4&gt;One Third&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-8\"&gt; \r\n&lt;h4&gt;Two Thirds&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;/div&gt;\r\n</pre>\r\n<br/>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-8\"> \r\n<h4>Two Thirds</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint, doloribus, tempora, numquam repellendus maiores facere a atque reiciendis voluptatibus hic veritatis ratione reprehenderit.</p>\r\n</div> \r\n\r\n<div class=\"col-md-4\"> \r\n<h4>One Third</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut. Lorem ipsum dolor sit amet, consectetur adipisicing.</p>\r\n</div> \r\n</div>\r\n\r\n<pre style=\"margin-bottom:40px;\">\r\n&lt;div class=\"row\"&gt;\r\n\r\n&lt;div class=\"col-md-8\"&gt; \r\n&lt;h4&gt;Two Thirds&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-4\"&gt; \r\n&lt;h4&gt;One Third/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;/div&gt;\r\n</pre>\r\n<br/>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-3\"> \r\n<h4>One Fourth</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut.</p>\r\n</div> \r\n\r\n<div class=\"col-md-9\"> \r\n<h4>Three Fourths</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint, doloribus, tempora, numquam repellendus maiores facere a atque reiciendis voluptatibus hic veritatis ratione reprehenderit.</p>\r\n</div> \r\n</div>\r\n\r\n<pre style=\"margin-bottom:40px;\">\r\n&lt;div class=\"row\"&gt;\r\n\r\n&lt;div class=\"col-md-3\"&gt; \r\n&lt;h4&gt;One Fourth&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-9\"&gt; \r\n&lt;h4&gt;Three Fourths&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;/div&gt;\r\n</pre>\r\n<br/>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-9\"> \r\n<h4>Three Fourths</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint, doloribus, tempora, numquam repellendus maiores facere a atque reiciendis voluptatibus hic veritatis ratione reprehenderit.</p>\r\n</div> \r\n\r\n<div class=\"col-md-3\"> \r\n<h4>One Fourth</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut.</p>\r\n</div> \r\n</div>\r\n\r\n<pre style=\"margin-bottom:40px;\">\r\n&lt;div class=\"row\"&gt;\r\n\r\n&lt;div class=\"col-md-9\"&gt; \r\n&lt;h4&gt;Three Fourths&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=\"col-md-3\"&gt; \r\n&lt;h4&gt;One Fourth&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;/div&gt;\r\n</pre>\r\n<br/>','','full_html'),('node','page',0,9,9,'und',0,'\r\n<h1>Columns</h1>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-6\">\r\n<h4>One Half</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>\r\n</div> \r\n\r\n<div class=\"col-md-6\"> \r\n<h4>One Half</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>\r\n</div> \r\n</div>\r\n<pre style=\"margin-bottom:40px;\">\r\n&lt;div class=\"row\"&gt;\r\n\r\n&lt;div class=\"col-md-6\"&gt;\r\n&lt;h4&gt;One Half&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-6\"&gt; \r\n&lt;h4&gt;One Half&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;/div&gt;\r\n</pre>\r\n<br/>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\"> \r\n<h4>One Third</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis.</p>\r\n</div> \r\n\r\n<div class=\"col-md-4\"> \r\n<h4>One Third</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis.</p>\r\n</div> \r\n\r\n<div class=\"col-md-4\"> \r\n<h4>One Third</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis.</p>\r\n</div> \r\n</div>\r\n\r\n<pre style=\"margin-bottom:40px;\">\r\n&lt;div class=\"row\"&gt;\r\n\r\n&lt;div class=\"col-md-4\"&gt; \r\n&lt;h4&gt;One Third&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui..&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-4\"&gt; \r\n&lt;h4&gt;One Third&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui..&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-4\"&gt; \r\n&lt;h4&gt;One Third&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui..&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;/div&gt;\r\n</pre>\r\n<br/>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-3\"> \r\n<h4>One Fourth</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut.</p>\r\n</div> \r\n\r\n<div class=\"col-md-3\"> \r\n<h4>One Fourth</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut.</p>\r\n</div> \r\n\r\n<div class=\"col-md-3\"> \r\n<h4>One Fourth</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut.</p>\r\n</div>\r\n\r\n<div class=\"col-md-3\"> \r\n<h4>One Fourth</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut.</p>\r\n</div>\r\n</div>\r\n\r\n<pre style=\"margin-bottom:40px;\">\r\n&lt;div class=\"row\"&gt;\r\n\r\n&lt;div class=\"col-md-3\"&gt; \r\n&lt;h4&gt;One Fourth&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui..&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-3\"&gt; \r\n&lt;h4&gt;One Fourth&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui..&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-3\"&gt; \r\n&lt;h4&gt;One Fourth&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui..&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=\"col-md-3\"&gt; \r\n&lt;h4&gt;One Fourth&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui..&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;/div&gt;\r\n</pre>\r\n<br/>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-4\"> \r\n<h4>One Third</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut. Lorem ipsum dolor sit amet, consectetur adipisicing.</p>\r\n</div> \r\n\r\n<div class=\"col-md-8\"> \r\n<h4>Two Thirds</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint, doloribus, tempora, numquam repellendus maiores facere a atque reiciendis voluptatibus hic veritatis ratione reprehenderit.</p>\r\n</div> \r\n</div>\r\n\r\n<pre style=\"margin-bottom:40px;\">\r\n&lt;div class=\"row\"&gt;\r\n\r\n&lt;div class=\"col-md-4\"&gt; \r\n&lt;h4&gt;One Third&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-8\"&gt; \r\n&lt;h4&gt;Two Thirds&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;/div&gt;\r\n</pre>\r\n<br/>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-8\"> \r\n<h4>Two Thirds</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint, doloribus, tempora, numquam repellendus maiores facere a atque reiciendis voluptatibus hic veritatis ratione reprehenderit.</p>\r\n</div> \r\n\r\n<div class=\"col-md-4\"> \r\n<h4>One Third</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut. Lorem ipsum dolor sit amet, consectetur adipisicing.</p>\r\n</div> \r\n</div>\r\n\r\n<pre style=\"margin-bottom:40px;\">\r\n&lt;div class=\"row\"&gt;\r\n\r\n&lt;div class=\"col-md-8\"&gt; \r\n&lt;h4&gt;Two Thirds&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-4\"&gt; \r\n&lt;h4&gt;One Third/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;/div&gt;\r\n</pre>\r\n<br/>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-3\"> \r\n<h4>One Fourth</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut.</p>\r\n</div> \r\n\r\n<div class=\"col-md-9\"> \r\n<h4>Three Fourths</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint, doloribus, tempora, numquam repellendus maiores facere a atque reiciendis voluptatibus hic veritatis ratione reprehenderit.</p>\r\n</div> \r\n</div>\r\n\r\n<pre style=\"margin-bottom:40px;\">\r\n&lt;div class=\"row\"&gt;\r\n\r\n&lt;div class=\"col-md-3\"&gt; \r\n&lt;h4&gt;One Fourth&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-9\"&gt; \r\n&lt;h4&gt;Three Fourths&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;/div&gt;\r\n</pre>\r\n<br/>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-9\"> \r\n<h4>Three Fourths</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint, doloribus, tempora, numquam repellendus maiores facere a atque reiciendis voluptatibus hic veritatis ratione reprehenderit.</p>\r\n</div> \r\n\r\n<div class=\"col-md-3\"> \r\n<h4>One Fourth</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut.</p>\r\n</div> \r\n</div>\r\n\r\n<pre style=\"margin-bottom:40px;\">\r\n&lt;div class=\"row\"&gt;\r\n\r\n&lt;div class=\"col-md-9\"&gt; \r\n&lt;h4&gt;Three Fourths&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=\"col-md-3\"&gt; \r\n&lt;h4&gt;One Fourth&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;/div&gt;\r\n</pre>\r\n<br/>','','full_html'),('node','mt_service',0,10,10,'und',0,'<h2>Heading 2</h2>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>\r\n','<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  ','full_html'),('node','mt_service',0,11,11,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>','','full_html'),('node','mt_service',0,12,12,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>','','full_html'),('node','blog',0,13,13,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>\r\n\r\n<h3 class=\"description-title\">Lorem ipsum dolor sit amed consetatur</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>','','full_html'),('node','blog',0,14,14,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>\r\n\r\n<h3 class=\"description-title\">Lorem ipsum dolor sit amed consetatur</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>','','full_html'),('node','blog',0,15,15,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>\r\n\r\n<h3 class=\"description-title\">Lorem ipsum dolor sit amed consetatur</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>','','full_html'),('node','mt_showcase',0,16,16,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas <a href=\"#\">state of the art interfaces</a>. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Completely parallel task market positioning interfaces through visionary niche markets, proactively incentivize sticky quality vectors before future-proof internal or \"organic\" sources.</blockquote>\r\n<p>Seamlessly procrastinate proactive outsourcing whereas wireless products. Progressively leverage existing 24/365 experiences through timely initiatives. Quickly facilitate frictionless ROI rather than equity invested solutions.</p>\r\n\r\n<h3 class=\"description-title\">Showcase specifics</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>','','full_html'),('node','mt_showcase',0,17,17,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas <a href=\"#\">state of the art interfaces</a>. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Completely parallel task market positioning interfaces through visionary niche markets, proactively incentivize sticky quality vectors before future-proof internal or \"organic\" sources.</blockquote>\r\n<p>Seamlessly procrastinate proactive outsourcing whereas wireless products. Progressively leverage existing 24/365 experiences through timely initiatives. Quickly facilitate frictionless ROI rather than equity invested solutions.</p>\r\n\r\n<h3 class=\"description-title\">Showcase specifics</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>','','full_html'),('node','mt_showcase',0,18,18,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas <a href=\"#\">state of the art interfaces</a>. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Completely parallel task market positioning interfaces through visionary niche markets, proactively incentivize sticky quality vectors before future-proof internal or \"organic\" sources.</blockquote>\r\n<p>Seamlessly procrastinate proactive outsourcing whereas wireless products. Progressively leverage existing 24/365 experiences through timely initiatives. Quickly facilitate frictionless ROI rather than equity invested solutions.</p>\r\n\r\n<h3 class=\"description-title\">Showcase specifics</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>','','full_html'),('node','mt_showcase',0,19,19,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas <a href=\"#\">state of the art interfaces</a>. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Completely parallel task market positioning interfaces through visionary niche markets, proactively incentivize sticky quality vectors before future-proof internal or \"organic\" sources.</blockquote>\r\n<p>Seamlessly procrastinate proactive outsourcing whereas wireless products. Progressively leverage existing 24/365 experiences through timely initiatives. Quickly facilitate frictionless ROI rather than equity invested solutions.</p>\r\n\r\n<h3 class=\"description-title\">Showcase specifics</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>','','full_html'),('node','mt_showcase',0,20,20,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas <a href=\"#\">state of the art interfaces</a>. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Completely parallel task market positioning interfaces through visionary niche markets, proactively incentivize sticky quality vectors before future-proof internal or \"organic\" sources.</blockquote>\r\n<p>Seamlessly procrastinate proactive outsourcing whereas wireless products. Progressively leverage existing 24/365 experiences through timely initiatives. Quickly facilitate frictionless ROI rather than equity invested solutions.</p>\r\n\r\n<h3 class=\"description-title\">Showcase specifics</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>','','full_html'),('node','mt_showcase',0,21,21,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas <a href=\"#\">state of the art interfaces</a>. Quickly enable prospective technology rather than open-source technologies. Seamlessly procrastinate proactive outsourcing whereas wireless products. Progressively leverage existing 24/365 experiences through timely initiatives.</p>  \r\n<blockquote>Completely parallel task market positioning interfaces through visionary niche markets, proactively incentivize sticky quality vectors before future-proof internal or \"organic\" sources.</blockquote>\r\n<p>Seamlessly procrastinate proactive outsourcing whereas wireless products. Progressively leverage existing 24/365 experiences through timely initiatives. Quickly facilitate frictionless ROI rather than equity invested solutions.</p>\r\n\r\n<h3 class=\"description-title\">Showcase specifics</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>','','full_html'),('node','mt_testimonial',0,24,24,'und',0,'Excepteur sint occaecat cupidatat non proident. Lorem ipsum dolor sit amet, consectetur.','','filtered_html'),('node','mt_service',0,25,25,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>','','full_html'),('node','page',0,26,26,'und',0,'<h2 id=\"brands\">Brands</h2>\r\n<ul class=\"brands\">\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-apple\"><span class=\"sr-only\">apple</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-android\"><span class=\"sr-only\">android</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-github\"><span class=\"sr-only\">github</span></i></a>\r\n</li>                        \r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-windows\"><span class=\"sr-only\">windows</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-linux\"><span class=\"sr-only\">linux</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-skype\"><span class=\"sr-only\">skype</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-btc\"><span class=\"sr-only\">btc</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-css3\"><span class=\"sr-only\">css3</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-html5\"><span class=\"sr-only\">html5</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-bitbucket\"><span class=\"sr-only\">bitbucket</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-maxcdn\"><span class=\"sr-only\">maxcdn</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-dropbox\"><span class=\"sr-only\">dropbox</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-facebook\"><span class=\"sr-only\">facebook</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-twitter\"><span class=\"sr-only\">twitter</span></i></a>\r\n</li>\r\n</ul>\r\n<pre>\r\n&lt;ul class=\"brands\"&gt;\r\n\r\n  &lt;li&gt;\r\n    &lt;a href=\"#\"&gt;&lt;i class=\"fa fa-apple\"&gt;&lt;span class=\"sr-only\"&gt;apple&lt;/span&gt;&lt;/i&gt;&lt;/a&gt;\r\n  &lt;/li&gt;\r\n  &lt;li&gt;\r\n    &lt;a href=\"#\"&gt;&lt;i class=\"fa fa-android\"&gt;&lt;span class=\"sr-only\"&gt;android&lt;/span&gt;&lt;/i&gt;&lt;/a&gt;\r\n  &lt;/li&gt;\r\n  &lt;li&gt;\r\n    &lt;a href=\"#\"&gt;&lt;i class=\"fa fa-github\"&gt;&lt;span class=\"sr-only\"&gt;github&lt;/span&gt;&lt;/i&gt;&lt;/a&gt;\r\n  &lt;/li&gt;\r\n  &lt;li&gt;\r\n    &lt;a href=\"#\"&gt;&lt;i class=\"fa fa-windows\"&gt;&lt;span class=\"sr-only\"&gt;windows&lt;/span&gt;&lt;/i&gt;&lt;/a&gt;\r\n  &lt;/li&gt;\r\n  &lt;li&gt;\r\n    &lt;a href=\"#\"&gt;&lt;i class=\"fa fa-linux\"&gt;&lt;span class=\"sr-only\"&gt;linux&lt;/span&gt;&lt;/i&gt;&lt;/a&gt;\r\n  &lt;/li&gt;\r\n  &lt;li&gt;\r\n    &lt;a href=\"#\"&gt;&lt;i class=\"fa fa-skype\"&gt;&lt;span class=\"sr-only\"&gt;skype&lt;/span&gt;&lt;/i&gt;&lt;/a&gt;\r\n  &lt;/li&gt;\r\n  &lt;li&gt;\r\n    &lt;a href=\"#\"&gt;&lt;i class=\"fa fa-btc\"&gt;&lt;span class=\"sr-only\"&gt;btc&lt;/span&gt;&lt;/i&gt;&lt;/a&gt;\r\n  &lt;/li&gt;\r\n  &lt;li&gt;\r\n    &lt;a href=\"#\"&gt;&lt;i class=\"fa fa-css3\"&gt;&lt;span class=\"sr-only\"&gt;css3&lt;/span&gt;&lt;/i&gt;&lt;/a&gt;\r\n  &lt;/li&gt;\r\n  &lt;li&gt;\r\n    &lt;a href=\"#\"&gt;&lt;i class=\"fa fa-html5\"&gt;&lt;span class=\"sr-only\"&gt;html5&lt;/span&gt;&lt;/i&gt;&lt;/a&gt;\r\n  &lt;/li&gt;\r\n  &lt;li&gt;\r\n    &lt;a href=\"#\"&gt;&lt;i class=\"fa fa-bitbucket\"&gt;&lt;span class=\"sr-only\"&gt;bitbucket&lt;/span&gt;&lt;/i&gt;&lt;/a&gt;\r\n  &lt;/li&gt;\r\n  &lt;li&gt;\r\n    &lt;a href=\"#\"&gt;&lt;i class=\"fa fa-maxcdn\"&gt;&lt;span class=\"sr-only\"&gt;maxcdn&lt;/span&gt;&lt;/i&gt;&lt;/a&gt;\r\n  &lt;/li&gt;\r\n  &lt;li&gt;\r\n    &lt;a href=\"#\"&gt;&lt;i class=\"fa fa-dropbox\"&gt;&lt;span class=\"sr-only\"&gt;dropbox&lt;/span&gt;&lt;/i&gt;&lt;/a&gt;\r\n  &lt;/li&gt;\r\n  &lt;li&gt;\r\n    &lt;a href=\"#\"&gt;&lt;i class=\"fa fa-facebook\"&gt;&lt;span class=\"sr-only\"&gt;facebook&lt;/span&gt;&lt;/i&gt;&lt;/a&gt;\r\n  &lt;/li&gt;\r\n  &lt;li&gt;\r\n    &lt;a href=\"#\"&gt;&lt;i class=\"fa fa-twitter\"&gt;&lt;span class=\"sr-only\"&gt;twitter&lt;/span&gt;&lt;/i&gt;&lt;/a&gt;\r\n  &lt;/li&gt;\r\n\r\n&lt;/ul&gt;\r\n</pre>\r\n<hr>\r\n<br>\r\n<h2 id=\"tabs\">Tabs</h2>\r\n<div role=\"tabpanel\">\r\n\r\n<!-- Nav tabs -->\r\n<ul class=\"nav nav-tabs\" role=\"tablist\">\r\n<li role=\"presentation\" class=\"active\"><a href=\"#home\" aria-controls=\"home\" role=\"tab\" data-toggle=\"tab\">Home</a></li>\r\n<li role=\"presentation\"><a href=\"#profile\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Profile</a></li>\r\n<li role=\"presentation\"><a href=\"#messages\" aria-controls=\"messages\" role=\"tab\" data-toggle=\"tab\">Messages</a></li>\r\n</ul>\r\n\r\n<!-- Tab panes -->\r\n<div class=\"tab-content\">\r\n<div role=\"tabpanel\" class=\"tab-pane fade in active\" id=\"home\"><p><strong>Home</strong> ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis, exercitationem, quaerat veniam repudiandae illo ratione eaque omnis obcaecati quidem distinctio sapiente quo assumenda amet cumque nobis nulla qui dolore autem.</p></div>\r\n<div role=\"tabpanel\" class=\"tab-pane fade\" id=\"profile\"><p><strong>Profile</strong> ipsum dolor sit amet, consectetur adipisicing elit. Ut odio facere minima porro quis. Maiores eius quibusdam et in corrupti necessitatibus consequatur debitis laudantium hic.</p></div>\r\n<div role=\"tabpanel\" class=\"tab-pane fade\" id=\"messages\"><p><strong>Messages</strong> ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis, optio error consectetur ullam porro eligendi mollitia odio numquam aut cumque. Sed, possimus recusandae itaque laboriosam nesciunt voluptates veniam aspernatur voluptate eaque ratione totam ipsa optio aliquam incidunt dolorum amet illum.</p></div>\r\n</div>\r\n\r\n</div>\r\n\r\n<pre>\r\n&lt;div role=\"tabpanel\"&gt;\r\n\r\n&lt;!-- Nav tabs --&gt;\r\n&lt;ul class=\"nav nav-tabs\" role=\"tablist\"&gt;\r\n  &lt;li role=\"presentation\" class=\"active\"&gt;\r\n    &lt;a href=\"#home\" aria-controls=\"home\" role=\"tab\" data-toggle=\"tab\"&gt;Home&lt;/a&gt;\r\n  &lt;/li&gt;\r\n  &lt;li role=\"presentation\"&gt;\r\n    &lt;a href=\"#profile\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\"&gt;Profile&lt;/a&gt;\r\n  &lt;/li&gt;\r\n  &lt;li role=\"presentation\"&gt;\r\n    &lt;a href=\"#messages\" aria-controls=\"messages\" role=\"tab\" data-toggle=\"tab\"&gt;Messages&lt;/a&gt;\r\n  &lt;/li&gt;\r\n&lt;/ul&gt;\r\n\r\n&lt;!-- Tab panes --&gt;\r\n&lt;div class=\"tab-content\"&gt;\r\n  &lt;div role=\"tabpanel\" class=\"tab-pane fade in active\" id=\"home\"&gt;\r\n    &lt;p&gt;&lt;strong&gt;Home&lt;/strong&gt; ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis, exercitationem, quaerat veniam repudiandae illo ratione eaque omnis obcaecati quidem distinctio sapiente quo assumenda amet cumque nobis nulla qui dolore autem.&lt;/p&gt;\r\n  &lt;/div&gt;\r\n  &lt;div role=\"tabpanel\" class=\"tab-pane fade\" id=\"profile\"&gt;\r\n    &lt;p&gt;&lt;strong&gt;Profile&lt;/strong&gt; ipsum dolor sit amet, consectetur adipisicing elit. Ut odio facere minima porro quis. Maiores eius quibusdam et in corrupti necessitatibus consequatur debitis laudantium hic.&lt;/p&gt;\r\n  &lt;/div&gt;\r\n  &lt;div role=\"tabpanel\" class=\"tab-pane fade\" id=\"messages\"&gt;\r\n    &lt;p&gt;&lt;strong&gt;Messages&lt;/strong&gt; ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis, optio error consectetur ullam porro eligendi mollitia odio numquam aut cumque. Sed, possimus recusandae itaque laboriosam nesciunt voluptates veniam aspernatur voluptate eaque ratione totam ipsa optio aliquam incidunt dolorum amet illum.&lt;/p&gt;\r\n  &lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;/div&gt;\r\n</pre>\r\n\r\n<hr>\r\n<br>\r\n<h2 id=\"accordion\">Accordion</h2>\r\n<div class=\"panel-group\" id=\"accordion-example\" role=\"tablist\" aria-multiselectable=\"true\">\r\n<div class=\"panel panel-default\">\r\n<div class=\"panel-heading\" role=\"tab\" id=\"headingOne\">\r\n<h4 class=\"panel-title\"><a data-toggle=\"collapse\" data-parent=\"#accordion-example\" href=\"#collapseOne\" aria-expanded=\"true\" aria-controls=\"collapseOne\"><i class=\"fa fa-home\"><span class=\"sr-only\">home</span></i> Home</a></h4>\r\n</div>\r\n<div id=\"collapseOne\" class=\"panel-collapse collapse in\" role=\"tabpanel\" aria-labelledby=\"headingOne\">\r\n<div class=\"panel-body\">\r\nAnim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven\'t heard of them accusamus labore sustainable VHS.\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"panel panel-default\">\r\n<div class=\"panel-heading\" role=\"tab\" id=\"headingTwo\">\r\n<h4 class=\"panel-title\"><a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion-example\" href=\"#collapseTwo\" aria-expanded=\"false\" aria-controls=\"collapseTwo\"><i class=\"fa fa-cog\"><span class=\"sr-only\">configuration</span></i> Configure</a></h4>\r\n</div>\r\n<div id=\"collapseTwo\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingTwo\">\r\n<div class=\"panel-body\">\r\nAnim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven\'t heard of them accusamus labore sustainable VHS.\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"panel panel-default\">\r\n<div class=\"panel-heading\" role=\"tab\" id=\"headingThree\">\r\n<h4 class=\"panel-title\"><a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion-example\" href=\"#collapseThree\" aria-expanded=\"false\" aria-controls=\"collapseThree\"><i class=\"fa fa-cloud-download\"><span class=\"sr-only\">cloud download</span></i> Download</a></h4>\r\n</div>\r\n<div id=\"collapseThree\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingThree\">\r\n<div class=\"panel-body\">\r\nAnim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven\'t heard of them accusamus labore sustainable VHS.\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<pre>\r\n&lt;div class=\"panel-group\" id=\"accordion-example\" role=\"tablist\" aria-multiselectable=\"true\"&gt;\r\n  &lt;div class=\"panel panel-default\"&gt;\r\n    &lt;div class=\"panel-heading\" role=\"tab\" id=\"headingOne\"&gt;\r\n      &lt;h4 class=\"panel-title\"&gt;\r\n        &lt;a data-toggle=\"collapse\" data-parent=\"#accordion-example\" href=\"#collapseOne\" aria-expanded=\"true\" aria-controls=\"collapseOne\"&gt;\r\n          &lt;i class=\"fa fa-home\"&gt;&lt;span class=\"sr-only\"&gt;home&lt;/span&gt;&lt;/i&gt; Home\r\n        &lt;/a&gt;\r\n      &lt;/h4&gt;\r\n    &lt;/div&gt;\r\n    &lt;div id=\"collapseOne\" class=\"panel-collapse collapse in\" role=\"tabpanel\" aria-labelledby=\"headingOne\"&gt;\r\n      &lt;div class=\"panel-body\"&gt;...&lt;/div&gt;\r\n    &lt;/div&gt;\r\n  &lt;/div&gt;\r\n  &lt;div class=\"panel panel-default\"&gt;\r\n    &lt;div class=\"panel-heading\" role=\"tab\" id=\"headingTwo\"&gt;\r\n      &lt;h4 class=\"panel-title\"&gt;\r\n        &lt;a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion-example\" href=\"#collapseTwo\" aria-expanded=\"false\" aria-controls=\"collapseTwo\"&gt;\r\n          &lt;i class=\"fa fa-cog\"&gt;&lt;span class=\"sr-only\"&gt;configuration&lt;/span&gt;&lt;/i&gt; Configure\r\n        &lt;/a&gt;\r\n      &lt;/h4&gt;\r\n    &lt;/div&gt;\r\n    &lt;div id=\"collapseTwo\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingTwo\"&gt;\r\n      &lt;div class=\"panel-body\"&gt;...&lt;/div&gt;\r\n    &lt;/div&gt;\r\n  &lt;/div&gt;\r\n  &lt;div class=\"panel panel-default\"&gt;\r\n    &lt;div class=\"panel-heading\" role=\"tab\" id=\"headingThree\"&gt;\r\n      &lt;h4 class=\"panel-title\"&gt;\r\n        &lt;a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion-example\" href=\"#collapseThree\" aria-expanded=\"false\" aria-controls=\"collapseThree\"&gt;\r\n          &lt;i class=\"fa fa-cloud-download\"&gt;&lt;span class=\"sr-only\"&gt;cloud download&lt;/span&gt;&lt;/i&gt; Download\r\n        &lt;/a&gt;\r\n        &lt;/h4&gt;\r\n    &lt;/div&gt;\r\n    &lt;div id=\"collapseThree\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingThree\"&gt;\r\n      &lt;div class=\"panel-body\"&gt;...&lt;/div&gt;\r\n    &lt;/div&gt;\r\n  &lt;/div&gt;\r\n&lt;/div&gt;\r\n</pre>\r\n<hr>\r\n<br>\r\n<h2 id=\"buttons\">Buttons</h2>\r\n<div>\r\n<a href=\"#\" class=\"more\">Read more</a>\r\n</div>\r\n<pre>\r\n&lt;a href=\"#\" class=\"more\"&gt;Read more&lt;/a&gt;\r\n</pre>\r\n<hr>\r\n<br>\r\n<h2>Buttons with click feedback effect</h2>\r\n<div>\r\n<button class=\"more cbutton-effect\">Click me</button>\r\n</div>\r\n<pre>\r\n&lt;button href=\"#\" class=\"more cbutton-effect\"&gt;Click me&lt;/button&gt;\r\n</pre>\r\n\r\n<hr>\r\n<br>\r\n<h2 id=\"progressbars\">Progress Bars</h2>\r\n<h5>78% Complete (default)</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 78%\">\r\n<span class=\"sr-only\">78% Complete (success)</span>\r\n</div>\r\n</div>\r\n<h5>40% Complete (success)</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-success\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 40%\">\r\n<span class=\"sr-only\">40% Complete (success)</span>\r\n</div>\r\n</div>\r\n<h5>20% Complete (info)</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-info\" role=\"progressbar\" aria-valuenow=\"20\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 20%\">\r\n<span class=\"sr-only\">20% Complete</span>\r\n</div>\r\n</div>\r\n<h5>60% Complete (warning)</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-warning\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 60%\">\r\n<span class=\"sr-only\">60% Complete (warning)</span>\r\n</div>\r\n</div>\r\n<h5>80% Complete (danger)</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-danger\" role=\"progressbar\" aria-valuenow=\"80\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 80%\">\r\n<span class=\"sr-only\">80% Complete</span>\r\n</div>\r\n</div>\r\n<h5>Results</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-success\" style=\"width: 40%\">\r\n<span class=\"sr-only\">35% A</span>\r\n</div>\r\n<div class=\"progress-bar progress-bar-info\" style=\"width: 30%\">\r\n<span class=\"sr-only\">20% B</span>\r\n</div>\r\n<div class=\"progress-bar progress-bar-warning\" style=\"width: 20%\">\r\n<span class=\"sr-only\">20% C</span>\r\n</div>\r\n<div class=\"progress-bar progress-bar-danger\" style=\"width: 10%\">\r\n<span class=\"sr-only\">10% D</span>\r\n</div>\r\n</div>\r\n\r\n<pre>\r\n&lt;h5>78% Complete (default)&lt;/h5&gt;\r\n&lt;div class=\"progress\"&gt;\r\n  &lt;div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 78%\"&gt;\r\n    &lt;span class=\"sr-only\"&gt;78% Complete (success)&lt;/span&gt;\r\n  &lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;h5>40% Complete (success)&lt;/h5&gt;\r\n&lt;div class=\"progress\"&gt;\r\n  &lt;div class=\"progress-bar progress-bar-success\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 40%\"&gt;\r\n    &lt;span class=\"sr-only\"&gt;40% Complete (success)&lt;/span&gt;\r\n  &lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;h5&gt;20% Complete (info)&lt;/h5&gt;\r\n&lt;div class=\"progress\"&gt;\r\n  &lt;div class=\"progress-bar progress-bar-info\" role=\"progressbar\" aria-valuenow=\"20\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 20%\"&gt;\r\n    &lt;span class=\"sr-only\"&gt;20% Complete&lt;/span&gt;\r\n  &lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;h5&gt;60% Complete (warning)&lt;/h5&gt;\r\n&lt;div class=\"progress\"&gt;\r\n  &lt;div class=\"progress-bar progress-bar-warning\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 60%\"&gt;\r\n    &lt;span class=\"sr-only\"&gt;60% Complete (warning)&lt;/span&gt;\r\n  &lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;h5&gt;80% Complete (danger)&lt;/h5&gt;\r\n&lt;div class=\"progress\"&gt;\r\n  &lt;div class=\"progress-bar progress-bar-danger\" role=\"progressbar\" aria-valuenow=\"80\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 80%\"&gt;\r\n    &lt;span class=\"sr-only\"&gt;80% Complete&lt;/span&gt;\r\n  &lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;h5&gt;Results&lt;/h5&gt;\r\n&lt;div class=\"progress\"&gt;\r\n  &lt;div class=\"progress-bar progress-bar-success\" style=\"width: 40%\"&gt;\r\n    &lt;span class=\"sr-only\"&gt;35% A&lt;/span&gt;\r\n  &lt;/div&gt;\r\n  &lt;div class=\"progress-bar progress-bar-info\" style=\"width: 30%\"&gt;\r\n    &lt;span class=\"sr-only\"&gt;20% B&lt;/span&gt;\r\n  &lt;/div&gt;\r\n  &lt;div class=\"progress-bar progress-bar-warning\" style=\"width: 20%\"&gt;\r\n    &lt;span class=\"sr-only\"&gt;20% C&lt;/span&gt;\r\n  &lt;/div&gt;\r\n  &lt;div class=\"progress-bar progress-bar-danger\" style=\"width: 10%\"&gt;\r\n    &lt;span class=\"sr-only\"&gt;10% D&lt;/span&gt;\r\n  &lt;/div&gt;\r\n&lt;/div&gt;\r\n</pre>\r\n<hr>\r\n<div class=\"alert alert-info\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>Check all available Font Awesome icons at <a  target=\"_blank\" href=\"http://fortawesome.github.io/Font-Awesome/icons/\" class=\"alert-link\">http://fortawesome.github.io/Font-Awesome/icons/</a></div>','','full_html'),('node','mt_benefit',0,29,29,'und',0,'<p>Ipsum, sed, odio, fugit, dolores atque sint odit doloremque expedita architecto commodi quas eum voluptas autem reprehenderit molestiae sunt cumque pariatur iure quod veritatis molestias quis facere eaque aliquid porro officia debitis accusamus eligendi itaque doloribus!</p>  \r\n<blockquote>Quasi, quaerat atque natus laudantium consectetur ducimus repellat deleniti amet sunt tenetur modi sit aperiam cumque dignissimos possimus? Ipsum, sed, odio, fugit, dolores atque sint odit doloremque expedita architecto commodi quas</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>\r\n\r\n<h3 class=\"description-title\">Lorem ipsum dolor sit amed consetatur</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<th>Service</th>\r\n<th>Quantity</th>\r\n<th>Price</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Lorem ipsum dolor sit amet</td>\r\n<td>24 pieces in stock</td>\r\n<td><strike>$279.00</strike> $189.00</td>			\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Praesent luctus fermentum tincidunt</td>\r\n<td>132 pieces in stock</td>\r\n<td><strike>$453.00</strike> $379.00</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Morbi venenatis, ligula ut tincidunt</td>\r\n<td>1231 pieces in stock</td>\r\n<td><strike>$1193.00</strike> $1089.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Duis vitae odio ac ante faucibus faucibus</td>\r\n<td>978 pieces in stock</td>\r\n<td><strike>$687.00</strike> $620.00</td>\r\n</tr>		\r\n</tbody>\r\n</table>\r\n<a href=\"#\" class=\"more\">Get free trial</a>\r\n','','full_html'),('node','mt_benefit',0,30,30,'und',0,'<p>Ipsum, sed, odio, fugit, dolores atque sint odit doloremque expedita architecto commodi quas eum voluptas autem reprehenderit molestiae sunt cumque pariatur iure quod veritatis molestias quis facere eaque aliquid porro officia debitis accusamus eligendi itaque doloribus!</p>  \r\n<blockquote>Quasi, quaerat atque natus laudantium consectetur ducimus repellat deleniti amet sunt tenetur modi sit aperiam cumque dignissimos possimus? Ipsum, sed, odio, fugit, dolores atque sint odit doloremque expedita architecto commodi quas</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>\r\n\r\n<h3 class=\"description-title\">Lorem ipsum dolor sit amed consetatur</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<th>Service</th>\r\n<th>Quantity</th>\r\n<th>Price</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Lorem ipsum dolor sit amet</td>\r\n<td>24 pieces in stock</td>\r\n<td><strike>$279.00</strike> $189.00</td>			\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Praesent luctus fermentum tincidunt</td>\r\n<td>132 pieces in stock</td>\r\n<td><strike>$453.00</strike> $379.00</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Morbi venenatis, ligula ut tincidunt</td>\r\n<td>1231 pieces in stock</td>\r\n<td><strike>$1193.00</strike> $1089.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Duis vitae odio ac ante faucibus faucibus</td>\r\n<td>978 pieces in stock</td>\r\n<td><strike>$687.00</strike> $620.00</td>\r\n</tr>		\r\n</tbody>\r\n</table>','','full_html'),('node','mt_benefit',0,31,31,'und',0,'<p>Ipsum, sed, odio, fugit, dolores atque sint odit doloremque expedita architecto commodi quas eum voluptas autem reprehenderit molestiae sunt cumque pariatur iure quod veritatis molestias quis facere eaque aliquid porro officia debitis accusamus eligendi itaque doloribus!</p>  \r\n<blockquote>Quasi, quaerat atque natus laudantium consectetur ducimus repellat deleniti amet sunt tenetur modi sit aperiam cumque dignissimos possimus? Ipsum, sed, odio, fugit, dolores atque sint odit doloremque expedita architecto commodi quas</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>\r\n\r\n<h3 class=\"description-title\">Lorem ipsum dolor sit amed consetatur</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<th>Service</th>\r\n<th>Quantity</th>\r\n<th>Price</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Lorem ipsum dolor sit amet</td>\r\n<td>24 pieces in stock</td>\r\n<td><strike>$279.00</strike> $189.00</td>			\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Praesent luctus fermentum tincidunt</td>\r\n<td>132 pieces in stock</td>\r\n<td><strike>$453.00</strike> $379.00</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Morbi venenatis, ligula ut tincidunt</td>\r\n<td>1231 pieces in stock</td>\r\n<td><strike>$1193.00</strike> $1089.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Duis vitae odio ac ante faucibus faucibus</td>\r\n<td>978 pieces in stock</td>\r\n<td><strike>$687.00</strike> $620.00</td>\r\n</tr>		\r\n</tbody>\r\n</table>','','full_html'),('node','webform',0,32,32,'und',0,'We respect your privacy 100%, so the information that you provide will remain strictly confidential. Nevertheless, a copy of your message might be stored in our records as a database entry for archival purposes.','','filtered_html'),('node','page',0,33,33,'und',0,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas interdum mi et facilisis rutrum. Vivamus lobortis aliquam est vel facilisis. Duis eu diam eu mi eleifend ornare ut hendrerit tortor. Quisque nisi nisl, adipiscing sed laoreet ac, vestibulum ac ligula. Fusce elementum, turpis nec facilisis facilisis, ante orci bibendum augue, vel ultricies risus leo eu lacus.</p>\r\n','','filtered_html'),('node','mt_product',0,34,34,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>','','full_html'),('node','mt_team_member',0,36,36,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n<h3>Skills</h3>\r\n\r\n<h5>90% Drupal</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 90%\">\r\n<span class=\"sr-only\">90% Drupal</span>\r\n</div>\r\n</div>\r\n\r\n<h5>70% jQuery</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 70%\">\r\n<span class=\"sr-only\">70% Drupal</span>\r\n</div>\r\n</div>\r\n\r\n<h5>80% CSS</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 80%\">\r\n<span class=\"sr-only\">80% Drupal</span>\r\n</div>\r\n</div>\r\n\r\n<h5>85% HTML</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 85%\">\r\n<span class=\"sr-only\">85% Drupal</span>\r\n</div>\r\n</div>','','full_html'),('node','mt_team_member',0,37,37,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n<h3>Skills</h3>\r\n\r\n<h5>90% Drupal</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 90%\">\r\n<span class=\"sr-only\">90% Drupal</span>\r\n</div>\r\n</div>\r\n\r\n<h5>70% jQuery</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 70%\">\r\n<span class=\"sr-only\">70% Drupal</span>\r\n</div>\r\n</div>\r\n\r\n<h5>80% CSS</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 80%\">\r\n<span class=\"sr-only\">80% Drupal</span>\r\n</div>\r\n</div>\r\n\r\n<h5>85% HTML</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 85%\">\r\n<span class=\"sr-only\">85% Drupal</span>\r\n</div>\r\n</div>','','full_html'),('node','mt_team_member',0,38,38,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n<h3>Skills</h3>\r\n\r\n<h5>90% Drupal</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 90%\">\r\n<span class=\"sr-only\">90% Drupal</span>\r\n</div>\r\n</div>\r\n\r\n<h5>70% jQuery</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 70%\">\r\n<span class=\"sr-only\">70% Drupal</span>\r\n</div>\r\n</div>\r\n\r\n<h5>80% CSS</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 80%\">\r\n<span class=\"sr-only\">80% Drupal</span>\r\n</div>\r\n</div>\r\n\r\n<h5>85% HTML</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 85%\">\r\n<span class=\"sr-only\">85% Drupal</span>\r\n</div>\r\n</div>','','full_html'),('node','mt_team_member',0,39,39,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n<h3>Skills</h3>\r\n\r\n<h5>90% Drupal</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 90%\">\r\n<span class=\"sr-only\">90% Drupal</span>\r\n</div>\r\n</div>\r\n\r\n<h5>70% jQuery</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 70%\">\r\n<span class=\"sr-only\">70% Drupal</span>\r\n</div>\r\n</div>\r\n\r\n<h5>80% CSS</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 80%\">\r\n<span class=\"sr-only\">80% Drupal</span>\r\n</div>\r\n</div>\r\n\r\n<h5>85% HTML</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 85%\">\r\n<span class=\"sr-only\">85% Drupal</span>\r\n</div>\r\n</div>','','full_html'),('node','mt_team_member',0,40,40,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n<h3>Skills</h3>\r\n\r\n<h5>90% Drupal</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 90%\">\r\n<span class=\"sr-only\">90% Drupal</span>\r\n</div>\r\n</div>\r\n\r\n<h5>70% jQuery</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 70%\">\r\n<span class=\"sr-only\">70% Drupal</span>\r\n</div>\r\n</div>\r\n\r\n<h5>80% CSS</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 80%\">\r\n<span class=\"sr-only\">80% Drupal</span>\r\n</div>\r\n</div>\r\n\r\n<h5>85% HTML</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 85%\">\r\n<span class=\"sr-only\">85% Drupal</span>\r\n</div>\r\n</div>','','full_html'),('node','page',0,41,41,'und',0,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas interdum mi et facilisis rutrum. Vivamus lobortis aliquam est vel facilisis. Duis eu diam eu mi eleifend ornare ut hendrerit tortor. Quisque nisi nisl, adipiscing sed laoreet ac, vestibulum ac ligula. Fusce elementum, turpis nec facilisis facilisis, ante orci bibendum augue, vel ultricies risus leo eu lacus.</p>','','filtered_html'),('node','mt_product',0,42,42,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>','','full_html'),('node','mt_product',0,43,43,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>\r\n','','full_html'),('node','mt_product',0,44,44,'und',0,'','Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies appropriately communicate.','filtered_html'),('node','mt_service',0,45,45,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>','','full_html'),('node','mt_service',0,46,46,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>','','full_html'),('node','mt_product',0,47,47,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>','','full_html'),('node','mt_product',0,48,48,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>\r\n','','full_html'),('node','mt_testimonial',0,49,49,'und',0,'Using their services we were never let down! Et dolore magna aliqua.','','filtered_html'),('node','mt_testimonial',0,50,50,'und',0,'Using their services we have increased conversions by 35% in a month!','','filtered_html'),('node','mt_testimonial',0,51,51,'und',0,'Tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.','','filtered_html'),('node','blog',0,52,52,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>\r\n\r\n<h3 class=\"description-title\">Lorem ipsum dolor sit amed consetatur</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>','','full_html'),('node','blog',0,53,53,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>\r\n\r\n<h3 class=\"description-title\">Lorem ipsum dolor sit amed consetatur</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>','','full_html'),('node','blog',0,54,54,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>\r\n\r\n<h3 class=\"description-title\">Lorem ipsum dolor sit amed consetatur</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>','','full_html'),('node','mt_showcase',0,56,56,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas <a href=\"#\">state of the art interfaces</a>. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Completely parallel task market positioning interfaces through visionary niche markets, proactively incentivize sticky quality vectors before future-proof internal or \"organic\" sources.</blockquote>\r\n<p>Seamlessly procrastinate proactive outsourcing whereas wireless products. Progressively leverage existing 24/365 experiences through timely initiatives. Quickly facilitate frictionless ROI rather than equity invested solutions.</p>\r\n\r\n<h3 class=\"description-title\">Showcase specifics</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>','','filtered_html'),('node','page',0,57,57,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','','filtered_html'),('node','page',0,58,58,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','','filtered_html');
/*!40000 ALTER TABLE `field_data_body` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_comment_body`
--

DROP TABLE IF EXISTS `field_data_comment_body`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_comment_body` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `comment_body_value` longtext,
  `comment_body_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `comment_body_format` (`comment_body_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 1 (comment_body)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_comment_body`
--

LOCK TABLES `field_data_comment_body` WRITE;
/*!40000 ALTER TABLE `field_data_comment_body` DISABLE KEYS */;
INSERT INTO `field_data_comment_body` VALUES ('comment','comment_node_blog',0,1,1,'und',0,'Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.','filtered_html'),('comment','comment_node_blog',0,2,2,'und',0,'Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.','filtered_html'),('comment','comment_node_blog',0,3,3,'und',0,'Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. ','full_html'),('comment','comment_node_blog',0,5,5,'und',0,'Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Nullam id dolor id nibh ultricies vehicula ut id elit. ','filtered_html'),('comment','comment_node_mt_showcase',0,6,6,'und',0,'Fusce interdum hendrerit mauris porttitor scelerisque. Praesent facilisis augue tortor, a dapibus neque ultricies a. Phasellus mollis, purus eu consequat convallis, arcu urna pharetra mauris, non pulvinar lorem mauris quis purus. Donec ullamcorper mauris lorem, id molestie mauris fringilla non.','filtered_html'),('comment','comment_node_mt_showcase',0,7,7,'und',0,'Vestibulum in nisl leo. Nam interdum ac libero id pulvinar. Nam a leo vulputate nisl convallis viverra. Donec felis lorem, rhoncus nec placerat at, ullamcorper eget nisl. Suspendisse gravida volutpat lacus, sed iaculis nunc hendrerit id. Aenean id diam ornare, accumsan ligula in, vestibulum urna. Pellentesque id augue in ante semper cursus id vel neque. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.','filtered_html'),('comment','comment_node_mt_showcase',0,8,8,'und',0,'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam porta faucibus leo, quis rhoncus sem vehicula sed. Duis sit amet cursus mauris, et interdum sem. Curabitur eu nisl luctus, convallis arcu non, laoreet velit. Suspendisse accumsan magna vitae erat cursus tempus. ','filtered_html'),('comment','comment_node_mt_showcase',0,9,9,'und',0,'Nam porta faucibus leo, quis rhoncus sem vehicula sed. Duis sit amet cursus mauris, et interdum sem. Curabitur eu nisl luctus, convallis arcu non, laoreet velit. Suspendisse accumsan magna vitae erat cursus tempus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. ','filtered_html'),('comment','comment_node_mt_showcase',0,10,10,'und',0,'Proin ut viverra orci. Fusce interdum hendrerit mauris porttitor scelerisque. Praesent facilisis augue tortor, a dapibus neque ultricies a. Phasellus mollis, purus eu consequat convallis, arcu urna pharetra mauris, non pulvinar lorem mauris quis purus. Donec ullamcorper mauris lorem, id molestie mauris fringilla non.','filtered_html');
/*!40000 ALTER TABLE `field_data_comment_body` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_image`
--

DROP TABLE IF EXISTS `field_data_field_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_image` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_image_fid` int(10) unsigned DEFAULT NULL COMMENT 'The file_managed.fid being referenced in this field.',
  `field_image_alt` varchar(512) DEFAULT NULL COMMENT 'Alternative image text, for the image’s ’alt’ attribute.',
  `field_image_title` varchar(1024) DEFAULT NULL COMMENT 'Image title text, for the image’s ’title’ attribute.',
  `field_image_width` int(10) unsigned DEFAULT NULL COMMENT 'The width of the image in pixels.',
  `field_image_height` int(10) unsigned DEFAULT NULL COMMENT 'The height of the image in pixels.',
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_image_fid` (`field_image_fid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 4 (field_image)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_image`
--

LOCK TABLES `field_data_field_image` WRITE;
/*!40000 ALTER TABLE `field_data_field_image` DISABLE KEYS */;
INSERT INTO `field_data_field_image` VALUES ('node','blog',0,13,13,'und',0,277,'','The title of the caption',750,500),('node','blog',0,14,14,'und',0,275,'','Caption of the image Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.',750,500),('node','blog',0,14,14,'und',1,287,'','Title of Second Slide',750,500),('node','blog',0,15,15,'und',0,276,'lorem ipsum dolor sit amet','The title of the caption',750,500),('node','mt_showcase',0,16,16,'und',0,259,'','',750,500),('node','mt_showcase',0,17,17,'und',0,260,'','',750,500),('node','mt_showcase',0,18,18,'und',0,261,'','',750,500),('node','mt_showcase',0,19,19,'und',0,256,'lorem ipsum dolor sit amet','And this is the caption of the image',750,500),('node','mt_showcase',0,20,20,'und',0,253,'lorem ipsum dolor sit amet','The title of the caption',750,500),('node','mt_showcase',0,20,20,'und',1,254,'Imperatives rather than value-added potentialities.','Title of Second Slide',750,500),('node','mt_showcase',0,20,20,'und',2,255,'Communicate adaptive imperatives rather than value-added potentialities.','Title of Image',750,500),('node','mt_showcase',0,21,21,'und',0,257,'lorem ipsum dolor sit amet','The title of the caption',750,500),('node','blog',0,52,52,'und',0,299,'','And this is the caption of the image which clarifies that quickly extend top-line opportunities for leveraged bandwidth.',750,500),('node','blog',0,53,53,'und',0,281,'lorem ipsum dolor sit amet','And this is the caption of the image which clarifies that quickly extend top-line opportunities for leveraged bandwidth.',750,500),('node','blog',0,54,54,'und',0,282,'lorem ipsum dolor sit amet','The title of the caption',750,500),('node','mt_showcase',0,56,56,'und',0,296,'','',750,500);
/*!40000 ALTER TABLE `field_data_field_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_banner_image`
--

DROP TABLE IF EXISTS `field_data_field_mt_banner_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_banner_image` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_banner_image_fid` int(10) unsigned DEFAULT NULL COMMENT 'The file_managed.fid being referenced in this field.',
  `field_mt_banner_image_alt` varchar(512) DEFAULT NULL COMMENT 'Alternative image text, for the image’s ’alt’ attribute.',
  `field_mt_banner_image_title` varchar(1024) DEFAULT NULL COMMENT 'Image title text, for the image’s ’title’ attribute.',
  `field_mt_banner_image_width` int(10) unsigned DEFAULT NULL COMMENT 'The width of the image in pixels.',
  `field_mt_banner_image_height` int(10) unsigned DEFAULT NULL COMMENT 'The height of the image in pixels.',
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_banner_image_fid` (`field_mt_banner_image_fid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 35 (field_mt_banner_image)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_banner_image`
--

LOCK TABLES `field_data_field_mt_banner_image` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_banner_image` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_banner_image` VALUES ('node','mt_service',0,10,10,'und',0,210,'','Synergistically streamline prospective content whereas turnkey web services.',1920,630),('node','mt_service',0,10,10,'und',1,211,'','Efficiently formulate enabled processes with granular processes.',1920,630),('node','page',0,55,55,'und',0,283,'','Be it for a question, an idea or even a concern, don’t hesitate to contact us.',1920,630),('node','page',0,55,55,'und',1,284,'','Be it for a question, an idea or even a concern, don’t hesitate to contact us.',1920,630);
/*!40000 ALTER TABLE `field_data_field_mt_banner_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_company_description`
--

DROP TABLE IF EXISTS `field_data_field_mt_company_description`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_company_description` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_company_description_value` longtext,
  `field_mt_company_description_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_company_description_format` (`field_mt_company_description_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 73 (field_mt_company_description)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_company_description`
--

LOCK TABLES `field_data_field_mt_company_description` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_company_description` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_company_description` VALUES ('node','mt_showcase',0,16,16,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.','filtered_html'),('node','mt_showcase',0,17,17,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.','filtered_html'),('node','mt_showcase',0,18,18,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.','filtered_html'),('node','mt_showcase',0,19,19,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodtempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodoconsequat.','filtered_html'),('node','mt_showcase',0,20,20,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.','filtered_html'),('node','mt_showcase',0,21,21,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.','filtered_html'),('node','mt_showcase',0,56,56,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.','filtered_html');
/*!40000 ALTER TABLE `field_data_field_mt_company_description` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_company_logo`
--

DROP TABLE IF EXISTS `field_data_field_mt_company_logo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_company_logo` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_company_logo_fid` int(10) unsigned DEFAULT NULL COMMENT 'The file_managed.fid being referenced in this field.',
  `field_mt_company_logo_alt` varchar(512) DEFAULT NULL COMMENT 'Alternative image text, for the image’s ’alt’ attribute.',
  `field_mt_company_logo_title` varchar(1024) DEFAULT NULL COMMENT 'Image title text, for the image’s ’title’ attribute.',
  `field_mt_company_logo_width` int(10) unsigned DEFAULT NULL COMMENT 'The width of the image in pixels.',
  `field_mt_company_logo_height` int(10) unsigned DEFAULT NULL COMMENT 'The height of the image in pixels.',
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_company_logo_fid` (`field_mt_company_logo_fid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 72 (field_mt_company_logo)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_company_logo`
--

LOCK TABLES `field_data_field_mt_company_logo` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_company_logo` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_company_logo` VALUES ('node','mt_showcase',0,16,16,'und',0,304,'','',158,33),('node','mt_showcase',0,17,17,'und',0,305,'','',140,54),('node','mt_showcase',0,18,18,'und',0,306,'','',141,49),('node','mt_showcase',0,19,19,'und',0,308,'','',140,44),('node','mt_showcase',0,20,20,'und',0,301,'','',141,68),('node','mt_showcase',0,21,21,'und',0,303,'','',141,49),('node','mt_showcase',0,56,56,'und',0,307,'','',141,49);
/*!40000 ALTER TABLE `field_data_field_mt_company_logo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_company_title`
--

DROP TABLE IF EXISTS `field_data_field_mt_company_title`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_company_title` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_company_title_value` varchar(255) DEFAULT NULL,
  `field_mt_company_title_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_company_title_format` (`field_mt_company_title_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 71 (field_mt_company_title)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_company_title`
--

LOCK TABLES `field_data_field_mt_company_title` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_company_title` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_company_title` VALUES ('node','mt_showcase',0,16,16,'und',0,'Company D','filtered_html'),('node','mt_showcase',0,17,17,'und',0,'Company E','filtered_html'),('node','mt_showcase',0,18,18,'und',0,'Company B','filtered_html'),('node','mt_showcase',0,19,19,'und',0,'Company B','filtered_html'),('node','mt_showcase',0,20,20,'und',0,'Company A','filtered_html'),('node','mt_showcase',0,21,21,'und',0,'Company C','filtered_html'),('node','mt_showcase',0,56,56,'und',0,'Company G','filtered_html');
/*!40000 ALTER TABLE `field_data_field_mt_company_title` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_content_bottom`
--

DROP TABLE IF EXISTS `field_data_field_mt_content_bottom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_content_bottom` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_content_bottom_value` longtext,
  `field_mt_content_bottom_summary` longtext,
  `field_mt_content_bottom_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_content_bottom_format` (`field_mt_content_bottom_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 70 (field_mt_content_bottom)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_content_bottom`
--

LOCK TABLES `field_data_field_mt_content_bottom` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_content_bottom` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_content_bottom` VALUES ('node','mt_service',0,10,10,'und',0,'<h5>Service description</h5>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<th>Service</th>\r\n<th>Quantity</th>\r\n<th>Price</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Lorem ipsum dolor sit amet</td>\r\n<td>24 pieces in stock</td>\r\n<td>$189.00</td>			\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Praesent luctus fermentum tincidunt</td>\r\n<td>132 pieces in stock</td>\r\n<td>$379.00</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Morbi venenatis, ligula ut tincidunt</td>\r\n<td>1231 pieces in stock</td>\r\n<td>$1089.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Duis vitae odio ac ante faucibus faucibus</td>\r\n<td>978 pieces in stock</td>\r\n<td>$620.00</td>\r\n</tr>		\r\n</tbody>\r\n</table>\r\n<a href=\"#\" class=\"more\">Get free trial</a>\r\n','','full_html'),('node','mt_service',0,11,11,'und',0,'<h5 class=\"description-title\">Service Description</h5>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<th>Service</th>\r\n<th>Quantity</th>\r\n<th>Price</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Lorem ipsum dolor sit amet</td>\r\n<td>24 pieces in stock</td>\r\n<td>$189.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Praesent luctus fermentum tincidunt</td>\r\n<td>132 pieces in stock</td>\r\n<td>$379.00</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Morbi venenatis, ligula ut tincidunt</td>\r\n<td>1231 pieces in stock</td>\r\n<td>$1089.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Duis vitae odio ac ante faucibus faucibus</td>\r\n<td>978 pieces in stock</td>\r\n<td>$620.00</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<a href=\"#\" class=\"more\">Get free trial</a>','','full_html'),('node','mt_service',0,12,12,'und',0,'<h5 class=\"description-title\">Service Description</h5>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<th>Service</th>\r\n<th>Quantity</th>\r\n<th>Price</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Lorem ipsum dolor sit amet</td>\r\n<td>24 pieces in stock</td>\r\n<td>$189.00</td>			\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Praesent luctus fermentum tincidunt</td>\r\n<td>132 pieces in stock</td>\r\n<td>$379.00</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Morbi venenatis, ligula ut tincidunt</td>\r\n<td>1231 pieces in stock</td>\r\n<td>$1089.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Duis vitae odio ac ante faucibus faucibus</td>\r\n<td>978 pieces in stock</td>\r\n<td>$620.00</td>\r\n</tr>		\r\n</tbody>\r\n</table>\r\n<a href=\"#\" class=\"more\">Get free trial</a>\r\n','','full_html'),('node','mt_service',0,25,25,'und',0,'<h5 class=\"description-title\">Service Description</h5>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<th>Service</th>\r\n<th>Quantity</th>\r\n<th>Price</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Lorem ipsum dolor sit amet</td>\r\n<td>24 pieces in stock</td>\r\n<td>$189.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Praesent luctus fermentum tincidunt</td>\r\n<td>132 pieces in stock</td>\r\n<td>$379.00</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Morbi venenatis, ligula ut tincidunt</td>\r\n<td>1231 pieces in stock</td>\r\n<td>$1089.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Duis vitae odio ac ante faucibus faucibus</td>\r\n<td>978 pieces in stock</td>\r\n<td>$620.00</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<a href=\"#\" class=\"more\">Get free trial</a>','','full_html'),('node','mt_product',0,34,34,'und',0,'<h5 class=\"description-title\">Service Description</h5>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<th>Service</th>\r\n<th>Quantity</th>\r\n<th>Price</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Lorem ipsum dolor sit amet</td>\r\n<td>24 pieces in stock</td>\r\n<td>$189.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Praesent luctus fermentum tincidunt</td>\r\n<td>132 pieces in stock</td>\r\n<td>$379.00</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Morbi venenatis, ligula ut tincidunt</td>\r\n<td>1231 pieces in stock</td>\r\n<td>$1089.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Duis vitae odio ac ante faucibus faucibus</td>\r\n<td>978 pieces in stock</td>\r\n<td>$620.00</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<a href=\"#\" class=\"more\">Get free trial</a>','','full_html'),('node','mt_product',0,42,42,'und',0,'<h5 class=\"description-title\">Service Description</h5>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<th>Service</th>\r\n<th>Quantity</th>\r\n<th>Price</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Lorem ipsum dolor sit amet</td>\r\n<td>24 pieces in stock</td>\r\n<td>$189.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Praesent luctus fermentum tincidunt</td>\r\n<td>132 pieces in stock</td>\r\n<td>$379.00</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Morbi venenatis, ligula ut tincidunt</td>\r\n<td>1231 pieces in stock</td>\r\n<td>$1089.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Duis vitae odio ac ante faucibus faucibus</td>\r\n<td>978 pieces in stock</td>\r\n<td>$620.00</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<a href=\"#\" class=\"more\">Get free trial</a>','','full_html'),('node','mt_product',0,43,43,'und',0,'<h5 class=\"description-title\">Service Description</h5>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<th>Service</th>\r\n<th>Quantity</th>\r\n<th>Price</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Lorem ipsum dolor sit amet</td>\r\n<td>24 pieces in stock</td>\r\n<td>$189.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Praesent luctus fermentum tincidunt</td>\r\n<td>132 pieces in stock</td>\r\n<td>$379.00</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Morbi venenatis, ligula ut tincidunt</td>\r\n<td>1231 pieces in stock</td>\r\n<td>$1089.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Duis vitae odio ac ante faucibus faucibus</td>\r\n<td>978 pieces in stock</td>\r\n<td>$620.00</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<a href=\"#\" class=\"more\">Get free trial</a>','','full_html'),('node','mt_service',0,45,45,'und',0,'<h5 class=\"description-title\">Service Description</h5>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<th>Service</th>\r\n<th>Quantity</th>\r\n<th>Price</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Lorem ipsum dolor sit amet</td>\r\n<td>24 pieces in stock</td>\r\n<td>$189.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Praesent luctus fermentum tincidunt</td>\r\n<td>132 pieces in stock</td>\r\n<td>$379.00</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Morbi venenatis, ligula ut tincidunt</td>\r\n<td>1231 pieces in stock</td>\r\n<td>$1089.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Duis vitae odio ac ante faucibus faucibus</td>\r\n<td>978 pieces in stock</td>\r\n<td>$620.00</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<a href=\"#\" class=\"more\">Get free trial</a>','','full_html'),('node','mt_service',0,46,46,'und',0,'<h5 class=\"description-title\">Service Description</h5>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<th>Service</th>\r\n<th>Quantity</th>\r\n<th>Price</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Lorem ipsum dolor sit amet</td>\r\n<td>24 pieces in stock</td>\r\n<td>$189.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Praesent luctus fermentum tincidunt</td>\r\n<td>132 pieces in stock</td>\r\n<td>$379.00</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Morbi venenatis, ligula ut tincidunt</td>\r\n<td>1231 pieces in stock</td>\r\n<td>$1089.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Duis vitae odio ac ante faucibus faucibus</td>\r\n<td>978 pieces in stock</td>\r\n<td>$620.00</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<a href=\"#\" class=\"more\">Get free trial</a>','','full_html'),('node','mt_product',0,47,47,'und',0,'<h5 class=\"description-title\">Service Description</h5>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<th>Service</th>\r\n<th>Quantity</th>\r\n<th>Price</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Lorem ipsum dolor sit amet</td>\r\n<td>24 pieces in stock</td>\r\n<td>$189.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Praesent luctus fermentum tincidunt</td>\r\n<td>132 pieces in stock</td>\r\n<td>$379.00</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Morbi venenatis, ligula ut tincidunt</td>\r\n<td>1231 pieces in stock</td>\r\n<td>$1089.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Duis vitae odio ac ante faucibus faucibus</td>\r\n<td>978 pieces in stock</td>\r\n<td>$620.00</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<a href=\"#\" class=\"more\">Get free trial</a>','','full_html'),('node','mt_product',0,48,48,'und',0,'<h5 class=\"description-title\">Service Description</h5>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<th>Service</th>\r\n<th>Quantity</th>\r\n<th>Price</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Lorem ipsum dolor sit amet</td>\r\n<td>24 pieces in stock</td>\r\n<td>$189.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Praesent luctus fermentum tincidunt</td>\r\n<td>132 pieces in stock</td>\r\n<td>$379.00</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Morbi venenatis, ligula ut tincidunt</td>\r\n<td>1231 pieces in stock</td>\r\n<td>$1089.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Duis vitae odio ac ante faucibus faucibus</td>\r\n<td>978 pieces in stock</td>\r\n<td>$620.00</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<a href=\"#\" class=\"more\">Get free trial</a>','','full_html');
/*!40000 ALTER TABLE `field_data_field_mt_content_bottom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_facebook_account`
--

DROP TABLE IF EXISTS `field_data_field_mt_facebook_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_facebook_account` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_facebook_account_value` varchar(255) DEFAULT NULL,
  `field_mt_facebook_account_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_facebook_account_format` (`field_mt_facebook_account_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 39 (field_mt_facebook_account)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_facebook_account`
--

LOCK TABLES `field_data_field_mt_facebook_account` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_facebook_account` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_facebook_account` VALUES ('node','mt_team_member',0,36,36,'und',0,'https://www.facebook.com',NULL),('node','mt_team_member',0,37,37,'und',0,'https://www.facebook.com',NULL),('node','mt_team_member',0,38,38,'und',0,'https://www.facebook.com',NULL),('node','mt_team_member',0,39,39,'und',0,'https://www.facebook.com',NULL),('node','mt_team_member',0,40,40,'und',0,'https://www.facebook.com',NULL);
/*!40000 ALTER TABLE `field_data_field_mt_facebook_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_feature_body`
--

DROP TABLE IF EXISTS `field_data_field_mt_feature_body`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_feature_body` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_feature_body_value` longtext,
  `field_mt_feature_body_summary` longtext,
  `field_mt_feature_body_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_feature_body_format` (`field_mt_feature_body_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 51 (field_mt_feature_body)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_feature_body`
--

LOCK TABLES `field_data_field_mt_feature_body` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_feature_body` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_feature_body` VALUES ('field_collection_item','field_mt_special_feature',0,32,32,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing exercitation. ','','filtered_html'),('field_collection_item','field_mt_special_feature',0,33,33,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing exercitation dolor sit amet. ','','filtered_html'),('field_collection_item','field_mt_special_feature',0,34,34,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing exercitation ipsum dolor sit amet. ','','filtered_html'),('field_collection_item','field_mt_special_feature',0,36,36,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing exercitation. ','','filtered_html'),('field_collection_item','field_mt_special_feature',0,37,37,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing exercitation dolor sit amet. ','','filtered_html'),('field_collection_item','field_mt_special_feature',0,38,38,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing exercitation ipsum dolor sit amet. ','','filtered_html'),('field_collection_item','field_mt_standard_feature',0,39,39,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing exercitation.','','filtered_html'),('field_collection_item','field_mt_standard_feature',0,40,40,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing exercitation.','','filtered_html'),('field_collection_item','field_mt_standard_feature',0,41,41,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing exercitation.','','filtered_html'),('field_collection_item','field_mt_standard_feature',0,42,42,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing exercitation.','','filtered_html'),('field_collection_item','field_mt_standard_feature',0,43,43,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing exercitation. ','','filtered_html'),('field_collection_item','field_mt_standard_feature',0,44,44,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing exercitation. ','','filtered_html'),('field_collection_item','field_mt_standard_feature',0,45,45,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing exercitation. ','','filtered_html'),('field_collection_item','field_mt_standard_feature',0,46,46,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing exercitation. ','','filtered_html');
/*!40000 ALTER TABLE `field_data_field_mt_feature_body` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_feature_font_awesome`
--

DROP TABLE IF EXISTS `field_data_field_mt_feature_font_awesome`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_feature_font_awesome` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_feature_font_awesome_value` varchar(255) DEFAULT NULL,
  `field_mt_feature_font_awesome_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_feature_font_awesome_format` (`field_mt_feature_font_awesome_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 53 (field_mt_feature_font_awesome)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_feature_font_awesome`
--

LOCK TABLES `field_data_field_mt_feature_font_awesome` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_feature_font_awesome` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_feature_font_awesome` VALUES ('field_collection_item','field_mt_special_feature',0,32,32,'und',0,'fa-users',NULL),('field_collection_item','field_mt_special_feature',0,33,33,'und',0,'fa-globe',NULL),('field_collection_item','field_mt_special_feature',0,34,34,'und',0,'fa-diamond',NULL),('field_collection_item','field_mt_special_feature',0,36,36,'und',0,'fa-users',NULL),('field_collection_item','field_mt_special_feature',0,37,37,'und',0,'fa-globe',NULL),('field_collection_item','field_mt_special_feature',0,38,38,'und',0,'fa-diamond',NULL);
/*!40000 ALTER TABLE `field_data_field_mt_feature_font_awesome` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_feature_image`
--

DROP TABLE IF EXISTS `field_data_field_mt_feature_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_feature_image` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_feature_image_fid` int(10) unsigned DEFAULT NULL COMMENT 'The file_managed.fid being referenced in this field.',
  `field_mt_feature_image_alt` varchar(512) DEFAULT NULL COMMENT 'Alternative image text, for the image’s ’alt’ attribute.',
  `field_mt_feature_image_title` varchar(1024) DEFAULT NULL COMMENT 'Image title text, for the image’s ’title’ attribute.',
  `field_mt_feature_image_width` int(10) unsigned DEFAULT NULL COMMENT 'The width of the image in pixels.',
  `field_mt_feature_image_height` int(10) unsigned DEFAULT NULL COMMENT 'The height of the image in pixels.',
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_feature_image_fid` (`field_mt_feature_image_fid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 52 (field_mt_feature_image)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_feature_image`
--

LOCK TABLES `field_data_field_mt_feature_image` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_feature_image` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_feature_image` VALUES ('field_collection_item','field_mt_standard_feature',0,39,39,'und',0,218,'','',750,500),('field_collection_item','field_mt_standard_feature',0,40,40,'und',0,219,'','',750,500),('field_collection_item','field_mt_standard_feature',0,41,41,'und',0,220,'','',750,500),('field_collection_item','field_mt_standard_feature',0,42,42,'und',0,221,'','',750,500),('field_collection_item','field_mt_standard_feature',0,43,43,'und',0,222,'','',750,500),('field_collection_item','field_mt_standard_feature',0,44,44,'und',0,223,'','',750,500),('field_collection_item','field_mt_standard_feature',0,45,45,'und',0,224,'','',750,500),('field_collection_item','field_mt_standard_feature',0,46,46,'und',0,225,'','',750,500);
/*!40000 ALTER TABLE `field_data_field_mt_feature_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_feature_subtitle`
--

DROP TABLE IF EXISTS `field_data_field_mt_feature_subtitle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_feature_subtitle` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_feature_subtitle_value` longtext,
  `field_mt_feature_subtitle_summary` longtext,
  `field_mt_feature_subtitle_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_feature_subtitle_format` (`field_mt_feature_subtitle_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 49 (field_mt_feature_subtitle)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_feature_subtitle`
--

LOCK TABLES `field_data_field_mt_feature_subtitle` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_feature_subtitle` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_feature_subtitle` VALUES ('field_collection_item','field_mt_special_feature',0,32,32,'und',0,'Happy clients','','filtered_html'),('field_collection_item','field_mt_special_feature',0,33,33,'und',0,'Offices worldwide','','filtered_html'),('field_collection_item','field_mt_special_feature',0,34,34,'und',0,'Finished projects','','filtered_html'),('field_collection_item','field_mt_special_feature',0,36,36,'und',0,'Happy clients','','filtered_html'),('field_collection_item','field_mt_special_feature',0,37,37,'und',0,'Offices worldwide','','filtered_html'),('field_collection_item','field_mt_special_feature',0,38,38,'und',0,'Finished projects','','filtered_html');
/*!40000 ALTER TABLE `field_data_field_mt_feature_subtitle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_feature_title`
--

DROP TABLE IF EXISTS `field_data_field_mt_feature_title`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_feature_title` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_feature_title_value` varchar(255) DEFAULT NULL,
  `field_mt_feature_title_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_feature_title_format` (`field_mt_feature_title_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 48 (field_mt_feature_title)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_feature_title`
--

LOCK TABLES `field_data_field_mt_feature_title` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_feature_title` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_feature_title` VALUES ('field_collection_item','field_mt_special_feature',0,32,32,'und',0,'122K',NULL),('field_collection_item','field_mt_special_feature',0,33,33,'und',0,'13',NULL),('field_collection_item','field_mt_special_feature',0,34,34,'und',0,'100+',NULL),('field_collection_item','field_mt_special_feature',0,36,36,'und',0,'122K',NULL),('field_collection_item','field_mt_special_feature',0,37,37,'und',0,'13',NULL),('field_collection_item','field_mt_special_feature',0,38,38,'und',0,'100+',NULL),('field_collection_item','field_mt_standard_feature',0,39,39,'und',0,'Feature one',NULL),('field_collection_item','field_mt_standard_feature',0,40,40,'und',0,'Feature Two',NULL),('field_collection_item','field_mt_standard_feature',0,41,41,'und',0,'Feature Three',NULL),('field_collection_item','field_mt_standard_feature',0,42,42,'und',0,'Feature Four',NULL),('field_collection_item','field_mt_standard_feature',0,43,43,'und',0,'Feature one',NULL),('field_collection_item','field_mt_standard_feature',0,44,44,'und',0,'Feature Two',NULL),('field_collection_item','field_mt_standard_feature',0,45,45,'und',0,'Feature Three',NULL),('field_collection_item','field_mt_standard_feature',0,46,46,'und',0,'Feature Four',NULL);
/*!40000 ALTER TABLE `field_data_field_mt_feature_title` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_font_awesome_classes`
--

DROP TABLE IF EXISTS `field_data_field_mt_font_awesome_classes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_font_awesome_classes` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_font_awesome_classes_value` varchar(255) DEFAULT NULL,
  `field_mt_font_awesome_classes_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_font_awesome_classes_format` (`field_mt_font_awesome_classes_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 31 (field_mt_font_awesome_classes)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_font_awesome_classes`
--

LOCK TABLES `field_data_field_mt_font_awesome_classes` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_font_awesome_classes` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_font_awesome_classes` VALUES ('node','mt_benefit',0,29,29,'und',0,'fa-space-shuttle',NULL),('node','mt_benefit',0,30,30,'und',0,'fa-star',NULL),('node','mt_benefit',0,31,31,'und',0,'fa-users',NULL);
/*!40000 ALTER TABLE `field_data_field_mt_font_awesome_classes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_highlight`
--

DROP TABLE IF EXISTS `field_data_field_mt_highlight`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_highlight` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_highlight_value` int(11) DEFAULT NULL COMMENT 'The field collection item id.',
  `field_mt_highlight_revision_id` int(11) DEFAULT NULL COMMENT 'The field collection item revision id.',
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_highlight_revision_id` (`field_mt_highlight_revision_id`),
  KEY `field_mt_highlight_value` (`field_mt_highlight_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 43 (field_mt_highlight)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_highlight`
--

LOCK TABLES `field_data_field_mt_highlight` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_highlight` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_highlight` VALUES ('node','mt_service',0,10,10,'und',0,26,26),('node','mt_service',0,10,10,'und',1,27,27),('node','mt_service',0,10,10,'und',2,28,28),('node','mt_service',0,11,11,'und',0,47,47),('node','mt_service',0,12,12,'und',0,48,48),('node','mt_service',0,25,25,'und',0,51,51),('node','mt_product',0,34,34,'und',0,54,54),('node','mt_product',0,42,42,'und',0,53,53),('node','mt_product',0,43,43,'und',0,52,52),('node','mt_product',0,44,44,'und',0,29,29),('node','mt_product',0,44,44,'und',1,30,30),('node','mt_product',0,44,44,'und',2,31,31),('node','mt_service',0,45,45,'und',0,49,49),('node','mt_service',0,46,46,'und',0,50,50),('node','mt_product',0,47,47,'und',0,55,55),('node','mt_product',0,48,48,'und',0,56,56);
/*!40000 ALTER TABLE `field_data_field_mt_highlight` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_highlight_body`
--

DROP TABLE IF EXISTS `field_data_field_mt_highlight_body`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_highlight_body` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_highlight_body_value` longtext,
  `field_mt_highlight_body_summary` longtext,
  `field_mt_highlight_body_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_highlight_body_format` (`field_mt_highlight_body_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 56 (field_mt_highlight_body)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_highlight_body`
--

LOCK TABLES `field_data_field_mt_highlight_body` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_highlight_body` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_highlight_body` VALUES ('field_collection_item','field_mt_highlight',0,26,26,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html'),('field_collection_item','field_mt_highlight',0,27,27,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html'),('field_collection_item','field_mt_highlight',0,28,28,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html'),('field_collection_item','field_mt_highlight',0,29,29,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html'),('field_collection_item','field_mt_highlight',0,30,30,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html'),('field_collection_item','field_mt_highlight',0,31,31,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html'),('field_collection_item','field_mt_highlight',0,47,47,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html'),('field_collection_item','field_mt_highlight',0,48,48,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html'),('field_collection_item','field_mt_highlight',0,49,49,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html'),('field_collection_item','field_mt_highlight',0,50,50,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html'),('field_collection_item','field_mt_highlight',0,51,51,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html'),('field_collection_item','field_mt_highlight',0,52,52,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html'),('field_collection_item','field_mt_highlight',0,53,53,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html'),('field_collection_item','field_mt_highlight',0,54,54,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html'),('field_collection_item','field_mt_highlight',0,55,55,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html'),('field_collection_item','field_mt_highlight',0,56,56,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html');
/*!40000 ALTER TABLE `field_data_field_mt_highlight_body` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_highlight_image`
--

DROP TABLE IF EXISTS `field_data_field_mt_highlight_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_highlight_image` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_highlight_image_fid` int(10) unsigned DEFAULT NULL COMMENT 'The file_managed.fid being referenced in this field.',
  `field_mt_highlight_image_alt` varchar(512) DEFAULT NULL COMMENT 'Alternative image text, for the image’s ’alt’ attribute.',
  `field_mt_highlight_image_title` varchar(1024) DEFAULT NULL COMMENT 'Image title text, for the image’s ’title’ attribute.',
  `field_mt_highlight_image_width` int(10) unsigned DEFAULT NULL COMMENT 'The width of the image in pixels.',
  `field_mt_highlight_image_height` int(10) unsigned DEFAULT NULL COMMENT 'The height of the image in pixels.',
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_highlight_image_fid` (`field_mt_highlight_image_fid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 45 (field_mt_highlight_image)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_highlight_image`
--

LOCK TABLES `field_data_field_mt_highlight_image` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_highlight_image` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_highlight_image` VALUES ('field_collection_item','field_mt_highlight',0,26,26,'und',0,239,'','',750,500),('field_collection_item','field_mt_highlight',0,27,27,'und',0,240,'','',750,500),('field_collection_item','field_mt_highlight',0,28,28,'und',0,241,'','',750,500),('field_collection_item','field_mt_highlight',0,29,29,'und',0,242,'','',750,500),('field_collection_item','field_mt_highlight',0,30,30,'und',0,243,'','',750,500),('field_collection_item','field_mt_highlight',0,31,31,'und',0,244,'','',750,500),('field_collection_item','field_mt_highlight',0,47,47,'und',0,245,'','',750,500),('field_collection_item','field_mt_highlight',0,48,48,'und',0,246,'','',750,500),('field_collection_item','field_mt_highlight',0,49,49,'und',0,247,'','',750,500),('field_collection_item','field_mt_highlight',0,50,50,'und',0,248,'','',750,500),('field_collection_item','field_mt_highlight',0,51,51,'und',0,298,'','',750,500),('field_collection_item','field_mt_highlight',0,52,52,'und',0,250,'','',750,500),('field_collection_item','field_mt_highlight',0,53,53,'und',0,251,'','',750,500),('field_collection_item','field_mt_highlight',0,54,54,'und',0,300,'','',750,500),('field_collection_item','field_mt_highlight',0,55,55,'und',0,262,'','',750,500),('field_collection_item','field_mt_highlight',0,56,56,'und',0,263,'','',750,500);
/*!40000 ALTER TABLE `field_data_field_mt_highlight_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_highlight_link`
--

DROP TABLE IF EXISTS `field_data_field_mt_highlight_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_highlight_link` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_highlight_link_url` varchar(2048) DEFAULT NULL,
  `field_mt_highlight_link_title` varchar(255) DEFAULT NULL,
  `field_mt_highlight_link_attributes` mediumtext,
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 57 (field_mt_highlight_link)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_highlight_link`
--

LOCK TABLES `field_data_field_mt_highlight_link` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_highlight_link` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_highlight_link` VALUES ('field_collection_item','field_mt_highlight',0,26,26,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}'),('field_collection_item','field_mt_highlight',0,27,27,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}'),('field_collection_item','field_mt_highlight',0,28,28,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}'),('field_collection_item','field_mt_highlight',0,29,29,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}'),('field_collection_item','field_mt_highlight',0,30,30,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}'),('field_collection_item','field_mt_highlight',0,31,31,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}'),('field_collection_item','field_mt_highlight',0,47,47,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}'),('field_collection_item','field_mt_highlight',0,48,48,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}'),('field_collection_item','field_mt_highlight',0,49,49,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}'),('field_collection_item','field_mt_highlight',0,50,50,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}'),('field_collection_item','field_mt_highlight',0,51,51,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}'),('field_collection_item','field_mt_highlight',0,52,52,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}'),('field_collection_item','field_mt_highlight',0,53,53,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}'),('field_collection_item','field_mt_highlight',0,54,54,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}'),('field_collection_item','field_mt_highlight',0,55,55,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}'),('field_collection_item','field_mt_highlight',0,56,56,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}');
/*!40000 ALTER TABLE `field_data_field_mt_highlight_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_highlight_title`
--

DROP TABLE IF EXISTS `field_data_field_mt_highlight_title`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_highlight_title` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_highlight_title_value` varchar(255) DEFAULT NULL,
  `field_mt_highlight_title_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_highlight_title_format` (`field_mt_highlight_title_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 55 (field_mt_highlight_title)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_highlight_title`
--

LOCK TABLES `field_data_field_mt_highlight_title` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_highlight_title` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_highlight_title` VALUES ('field_collection_item','field_mt_highlight',0,26,26,'und',0,'Name of first highlight',NULL),('field_collection_item','field_mt_highlight',0,27,27,'und',0,'Name of second highlight',NULL),('field_collection_item','field_mt_highlight',0,28,28,'und',0,'Name of third highlight',NULL),('field_collection_item','field_mt_highlight',0,29,29,'und',0,'Name of first highlight',NULL),('field_collection_item','field_mt_highlight',0,30,30,'und',0,'Name of second highlight',NULL),('field_collection_item','field_mt_highlight',0,31,31,'und',0,'Name of third highlight',NULL),('field_collection_item','field_mt_highlight',0,47,47,'und',0,'Name of first highlight',NULL),('field_collection_item','field_mt_highlight',0,48,48,'und',0,'Name of first highlight',NULL),('field_collection_item','field_mt_highlight',0,49,49,'und',0,'Name of first highlight',NULL),('field_collection_item','field_mt_highlight',0,50,50,'und',0,'Name of first highlight',NULL),('field_collection_item','field_mt_highlight',0,51,51,'und',0,'Name of first highlight',NULL),('field_collection_item','field_mt_highlight',0,52,52,'und',0,'Name of first highlight',NULL),('field_collection_item','field_mt_highlight',0,53,53,'und',0,'Name of first highlight',NULL),('field_collection_item','field_mt_highlight',0,54,54,'und',0,'Name of first highlight',NULL),('field_collection_item','field_mt_highlight',0,55,55,'und',0,'Name of first highlight',NULL),('field_collection_item','field_mt_highlight',0,56,56,'und',0,'Name of first highlight',NULL);
/*!40000 ALTER TABLE `field_data_field_mt_highlight_title` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_landscape_image`
--

DROP TABLE IF EXISTS `field_data_field_mt_landscape_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_landscape_image` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_landscape_image_fid` int(10) unsigned DEFAULT NULL COMMENT 'The file_managed.fid being referenced in this field.',
  `field_mt_landscape_image_alt` varchar(512) DEFAULT NULL COMMENT 'Alternative image text, for the image’s ’alt’ attribute.',
  `field_mt_landscape_image_title` varchar(1024) DEFAULT NULL COMMENT 'Image title text, for the image’s ’title’ attribute.',
  `field_mt_landscape_image_width` int(10) unsigned DEFAULT NULL COMMENT 'The width of the image in pixels.',
  `field_mt_landscape_image_height` int(10) unsigned DEFAULT NULL COMMENT 'The height of the image in pixels.',
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_landscape_image_fid` (`field_mt_landscape_image_fid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 66 (field_mt_landscape_image)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_landscape_image`
--

LOCK TABLES `field_data_field_mt_landscape_image` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_landscape_image` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_landscape_image` VALUES ('node','blog',0,13,13,'und',0,278,'','',750,354),('node','blog',0,52,52,'und',0,280,'','',750,354);
/*!40000 ALTER TABLE `field_data_field_mt_landscape_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_linkedin_account`
--

DROP TABLE IF EXISTS `field_data_field_mt_linkedin_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_linkedin_account` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_linkedin_account_value` varchar(255) DEFAULT NULL,
  `field_mt_linkedin_account_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_linkedin_account_format` (`field_mt_linkedin_account_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 41 (field_mt_linkedin_account)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_linkedin_account`
--

LOCK TABLES `field_data_field_mt_linkedin_account` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_linkedin_account` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_linkedin_account` VALUES ('node','mt_team_member',0,36,36,'und',0,'https://www.linkedin.com',NULL),('node','mt_team_member',0,37,37,'und',0,'https://www.linkedin.com',NULL),('node','mt_team_member',0,38,38,'und',0,'https://www.linkedin.com',NULL),('node','mt_team_member',0,39,39,'und',0,'https://www.linkedin.com',NULL),('node','mt_team_member',0,40,40,'und',0,'https://www.linkedin.com',NULL);
/*!40000 ALTER TABLE `field_data_field_mt_linkedin_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_member_photo`
--

DROP TABLE IF EXISTS `field_data_field_mt_member_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_member_photo` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_member_photo_fid` int(10) unsigned DEFAULT NULL COMMENT 'The file_managed.fid being referenced in this field.',
  `field_mt_member_photo_alt` varchar(512) DEFAULT NULL COMMENT 'Alternative image text, for the image’s ’alt’ attribute.',
  `field_mt_member_photo_title` varchar(1024) DEFAULT NULL COMMENT 'Image title text, for the image’s ’title’ attribute.',
  `field_mt_member_photo_width` int(10) unsigned DEFAULT NULL COMMENT 'The width of the image in pixels.',
  `field_mt_member_photo_height` int(10) unsigned DEFAULT NULL COMMENT 'The height of the image in pixels.',
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_member_photo_fid` (`field_mt_member_photo_fid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 38 (field_mt_member_photo)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_member_photo`
--

LOCK TABLES `field_data_field_mt_member_photo` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_member_photo` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_member_photo` VALUES ('node','mt_team_member',0,36,36,'und',0,191,'','',750,750),('node','mt_team_member',0,37,37,'und',0,192,'','',750,750),('node','mt_team_member',0,38,38,'und',0,193,'','',750,750),('node','mt_team_member',0,39,39,'und',0,194,'','',750,750),('node','mt_team_member',0,40,40,'und',0,195,'','',750,750);
/*!40000 ALTER TABLE `field_data_field_mt_member_photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_most_popular`
--

DROP TABLE IF EXISTS `field_data_field_mt_most_popular`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_most_popular` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_most_popular_value` int(11) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_most_popular_value` (`field_mt_most_popular_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 69 (field_mt_most_popular)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_most_popular`
--

LOCK TABLES `field_data_field_mt_most_popular` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_most_popular` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_most_popular` VALUES ('node','mt_product',0,34,34,'und',0,0),('node','mt_product',0,42,42,'und',0,0),('node','mt_product',0,43,43,'und',0,1),('node','mt_product',0,44,44,'und',0,0),('node','mt_product',0,47,47,'und',0,0),('node','mt_product',0,48,48,'und',0,0);
/*!40000 ALTER TABLE `field_data_field_mt_most_popular` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_portrait_image`
--

DROP TABLE IF EXISTS `field_data_field_mt_portrait_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_portrait_image` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_portrait_image_fid` int(10) unsigned DEFAULT NULL COMMENT 'The file_managed.fid being referenced in this field.',
  `field_mt_portrait_image_alt` varchar(512) DEFAULT NULL COMMENT 'Alternative image text, for the image’s ’alt’ attribute.',
  `field_mt_portrait_image_title` varchar(1024) DEFAULT NULL COMMENT 'Image title text, for the image’s ’title’ attribute.',
  `field_mt_portrait_image_width` int(10) unsigned DEFAULT NULL COMMENT 'The width of the image in pixels.',
  `field_mt_portrait_image_height` int(10) unsigned DEFAULT NULL COMMENT 'The height of the image in pixels.',
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_portrait_image_fid` (`field_mt_portrait_image_fid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 60 (field_mt_portrait_image)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_portrait_image`
--

LOCK TABLES `field_data_field_mt_portrait_image` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_portrait_image` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_portrait_image` VALUES ('node','mt_service',0,10,10,'und',0,232,'','',750,1000),('node','mt_service',0,11,11,'und',0,234,'','',750,1000),('node','mt_showcase',0,21,21,'und',0,258,'','',750,1000),('node','mt_product',0,42,42,'und',0,265,'','',750,1000),('node','mt_product',0,44,44,'und',0,288,'','',750,1000);
/*!40000 ALTER TABLE `field_data_field_mt_portrait_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_price`
--

DROP TABLE IF EXISTS `field_data_field_mt_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_price` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_price_value` varchar(255) DEFAULT NULL,
  `field_mt_price_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_price_format` (`field_mt_price_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 68 (field_mt_price)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_price`
--

LOCK TABLES `field_data_field_mt_price` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_price` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_price` VALUES ('node','mt_product',0,42,42,'und',0,'$39.00',NULL),('node','mt_product',0,43,43,'und',0,'$29.00',NULL),('node','mt_product',0,44,44,'und',0,'$12.00',NULL);
/*!40000 ALTER TABLE `field_data_field_mt_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_pricing_table_item`
--

DROP TABLE IF EXISTS `field_data_field_mt_pricing_table_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_pricing_table_item` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_pricing_table_item_value` varchar(255) DEFAULT NULL,
  `field_mt_pricing_table_item_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_pricing_table_item_format` (`field_mt_pricing_table_item_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 67 (field_mt_pricing_table_item)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_pricing_table_item`
--

LOCK TABLES `field_data_field_mt_pricing_table_item` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_pricing_table_item` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_pricing_table_item` VALUES ('node','mt_product',0,42,42,'und',0,'Account Balance',NULL),('node','mt_product',0,42,42,'und',1,'Online Interface',NULL),('node','mt_product',0,42,42,'und',2,'Weekly Reports',NULL),('node','mt_product',0,42,42,'und',3,'Email support',NULL),('node','mt_product',0,43,43,'und',0,'Account Balance',NULL),('node','mt_product',0,43,43,'und',1,'Online Interface',NULL),('node','mt_product',0,43,43,'und',2,'Investment Monitoring',NULL),('node','mt_product',0,43,43,'und',3,'Weekly Reports',NULL),('node','mt_product',0,43,43,'und',4,'One-on-one consultation',NULL),('node','mt_product',0,44,44,'und',0,'Account Balance',NULL),('node','mt_product',0,44,44,'und',1,'Online Interface',NULL),('node','mt_product',0,44,44,'und',2,'Weekly Reports',NULL);
/*!40000 ALTER TABLE `field_data_field_mt_pricing_table_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_product_tags`
--

DROP TABLE IF EXISTS `field_data_field_mt_product_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_product_tags` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_product_tags_tid` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_product_tags_tid` (`field_mt_product_tags_tid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 63 (field_mt_product_tags)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_product_tags`
--

LOCK TABLES `field_data_field_mt_product_tags` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_product_tags` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_product_tags` VALUES ('node','mt_product',0,34,34,'und',0,26),('node','mt_product',0,42,42,'und',0,25),('node','mt_product',0,43,43,'und',0,24),('node','mt_product',0,44,44,'und',0,23),('node','mt_product',0,47,47,'und',0,24),('node','mt_product',0,48,48,'und',0,25);
/*!40000 ALTER TABLE `field_data_field_mt_product_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_promoted_on_slideshow`
--

DROP TABLE IF EXISTS `field_data_field_mt_promoted_on_slideshow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_promoted_on_slideshow` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_promoted_on_slideshow_value` int(11) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_promoted_on_slideshow_value` (`field_mt_promoted_on_slideshow_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 33 (field_mt_promoted_on_slideshow)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_promoted_on_slideshow`
--

LOCK TABLES `field_data_field_mt_promoted_on_slideshow` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_promoted_on_slideshow` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_promoted_on_slideshow` VALUES ('node','page',0,1,1,'und',0,0),('node','page',0,2,2,'und',0,0),('node','mt_service',0,10,10,'und',0,1),('node','mt_service',0,11,11,'und',0,0),('node','mt_service',0,12,12,'und',0,0),('node','blog',0,13,13,'und',0,0),('node','blog',0,14,14,'und',0,0),('node','blog',0,15,15,'und',0,0),('node','mt_showcase',0,16,16,'und',0,0),('node','mt_showcase',0,17,17,'und',0,0),('node','mt_showcase',0,18,18,'und',0,0),('node','mt_showcase',0,19,19,'und',0,0),('node','mt_showcase',0,20,20,'und',0,1),('node','mt_showcase',0,21,21,'und',0,0),('node','webform',0,22,22,'und',0,0),('node','mt_service',0,25,25,'und',0,0),('node','mt_benefit',0,29,29,'und',0,0),('node','mt_benefit',0,30,30,'und',0,0),('node','mt_benefit',0,31,31,'und',0,0),('node','webform',0,32,32,'und',0,0),('node','mt_product',0,34,34,'und',0,0),('node','mt_product',0,42,42,'und',0,0),('node','mt_product',0,43,43,'und',0,0),('node','mt_product',0,44,44,'und',0,1),('node','mt_service',0,45,45,'und',0,0),('node','mt_service',0,46,46,'und',0,0),('node','mt_product',0,47,47,'und',0,0),('node','mt_product',0,48,48,'und',0,0),('node','blog',0,52,52,'und',0,0),('node','blog',0,53,53,'und',0,0),('node','blog',0,54,54,'und',0,0),('node','page',0,55,55,'und',0,0),('node','mt_showcase',0,56,56,'und',0,0),('node','page',0,57,57,'und',0,0),('node','page',0,58,58,'und',0,0);
/*!40000 ALTER TABLE `field_data_field_mt_promoted_on_slideshow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_service_tags`
--

DROP TABLE IF EXISTS `field_data_field_mt_service_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_service_tags` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_service_tags_tid` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_service_tags_tid` (`field_mt_service_tags_tid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 61 (field_mt_service_tags)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_service_tags`
--

LOCK TABLES `field_data_field_mt_service_tags` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_service_tags` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_service_tags` VALUES ('node','mt_service',0,10,10,'und',0,19),('node','mt_service',0,11,11,'und',0,21),('node','mt_service',0,12,12,'und',0,21),('node','mt_service',0,25,25,'und',0,22),('node','mt_service',0,45,45,'und',0,20),('node','mt_service',0,46,46,'und',0,20);
/*!40000 ALTER TABLE `field_data_field_mt_service_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_showcase_tags`
--

DROP TABLE IF EXISTS `field_data_field_mt_showcase_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_showcase_tags` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_showcase_tags_tid` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_showcase_tags_tid` (`field_mt_showcase_tags_tid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 37 (field_mt_showcase_tags)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_showcase_tags`
--

LOCK TABLES `field_data_field_mt_showcase_tags` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_showcase_tags` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_showcase_tags` VALUES ('node','mt_showcase',0,16,16,'und',0,15),('node','mt_showcase',0,17,17,'und',0,15),('node','mt_showcase',0,18,18,'und',0,16),('node','mt_showcase',0,19,19,'und',0,17),('node','mt_showcase',0,20,20,'und',0,15),('node','mt_showcase',0,20,20,'und',1,27),('node','mt_showcase',0,21,21,'und',0,16),('node','mt_showcase',0,56,56,'und',0,35);
/*!40000 ALTER TABLE `field_data_field_mt_showcase_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_slideshow_image`
--

DROP TABLE IF EXISTS `field_data_field_mt_slideshow_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_slideshow_image` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_slideshow_image_fid` int(10) unsigned DEFAULT NULL COMMENT 'The file_managed.fid being referenced in this field.',
  `field_mt_slideshow_image_alt` varchar(512) DEFAULT NULL COMMENT 'Alternative image text, for the image’s ’alt’ attribute.',
  `field_mt_slideshow_image_title` varchar(1024) DEFAULT NULL COMMENT 'Image title text, for the image’s ’title’ attribute.',
  `field_mt_slideshow_image_width` int(10) unsigned DEFAULT NULL COMMENT 'The width of the image in pixels.',
  `field_mt_slideshow_image_height` int(10) unsigned DEFAULT NULL COMMENT 'The height of the image in pixels.',
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_slideshow_image_fid` (`field_mt_slideshow_image_fid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 32 (field_mt_slideshow_image)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_slideshow_image`
--

LOCK TABLES `field_data_field_mt_slideshow_image` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_slideshow_image` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_slideshow_image` VALUES ('node','mt_service',0,10,10,'und',0,196,'','',1920,630),('node','mt_showcase',0,20,20,'und',0,198,'','',1920,630),('node','mt_product',0,44,44,'und',0,197,'','',1920,630);
/*!40000 ALTER TABLE `field_data_field_mt_slideshow_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_slideshow_path`
--

DROP TABLE IF EXISTS `field_data_field_mt_slideshow_path`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_slideshow_path` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_slideshow_path_value` varchar(255) DEFAULT NULL,
  `field_mt_slideshow_path_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_slideshow_path_format` (`field_mt_slideshow_path_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 34 (field_mt_slideshow_path)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_slideshow_path`
--

LOCK TABLES `field_data_field_mt_slideshow_path` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_slideshow_path` DISABLE KEYS */;
/*!40000 ALTER TABLE `field_data_field_mt_slideshow_path` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_special_feature`
--

DROP TABLE IF EXISTS `field_data_field_mt_special_feature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_special_feature` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_special_feature_value` int(11) DEFAULT NULL COMMENT 'The field collection item id.',
  `field_mt_special_feature_revision_id` int(11) DEFAULT NULL COMMENT 'The field collection item revision id.',
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_special_feature_revision_id` (`field_mt_special_feature_revision_id`),
  KEY `field_mt_special_feature_value` (`field_mt_special_feature_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 46 (field_mt_special_feature)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_special_feature`
--

LOCK TABLES `field_data_field_mt_special_feature` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_special_feature` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_special_feature` VALUES ('node','mt_service',0,10,10,'und',0,32,32),('node','mt_service',0,10,10,'und',1,33,33),('node','mt_service',0,10,10,'und',2,34,34),('node','mt_product',0,44,44,'und',0,36,36),('node','mt_product',0,44,44,'und',1,37,37),('node','mt_product',0,44,44,'und',2,38,38);
/*!40000 ALTER TABLE `field_data_field_mt_special_feature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_standard_feature`
--

DROP TABLE IF EXISTS `field_data_field_mt_standard_feature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_standard_feature` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_standard_feature_value` int(11) DEFAULT NULL COMMENT 'The field collection item id.',
  `field_mt_standard_feature_revision_id` int(11) DEFAULT NULL COMMENT 'The field collection item revision id.',
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_standard_feature_revision_id` (`field_mt_standard_feature_revision_id`),
  KEY `field_mt_standard_feature_value` (`field_mt_standard_feature_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 54 (field_mt_standard_feature)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_standard_feature`
--

LOCK TABLES `field_data_field_mt_standard_feature` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_standard_feature` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_standard_feature` VALUES ('node','mt_service',0,10,10,'und',0,39,39),('node','mt_service',0,10,10,'und',1,40,40),('node','mt_service',0,10,10,'und',2,41,41),('node','mt_service',0,10,10,'und',3,42,42),('node','mt_product',0,44,44,'und',0,43,43),('node','mt_product',0,44,44,'und',1,44,44),('node','mt_product',0,44,44,'und',2,45,45),('node','mt_product',0,44,44,'und',3,46,46);
/*!40000 ALTER TABLE `field_data_field_mt_standard_feature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_subheader_body`
--

DROP TABLE IF EXISTS `field_data_field_mt_subheader_body`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_subheader_body` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_subheader_body_value` longtext,
  `field_mt_subheader_body_summary` longtext,
  `field_mt_subheader_body_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_subheader_body_format` (`field_mt_subheader_body_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 30 (field_mt_subheader_body)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_subheader_body`
--

LOCK TABLES `field_data_field_mt_subheader_body` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_subheader_body` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_subheader_body` VALUES ('node','mt_service',0,10,10,'und',0,'Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies appropriately communicate.','','filtered_html'),('node','webform',0,22,22,'und',0,'<p>We respect your privacy 100%, so the information that you provide will remain strictly confidential.</p>','','full_html'),('node','page',0,35,35,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing <br> tempor incididunt ut labore et dolore aliqua.','','filtered_html'),('node','mt_product',0,44,44,'und',0,'Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies appropriately communicate.','','filtered_html'),('node','page',0,55,55,'und',0,'Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies appropriately communicate.','','filtered_html');
/*!40000 ALTER TABLE `field_data_field_mt_subheader_body` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_subtitle`
--

DROP TABLE IF EXISTS `field_data_field_mt_subtitle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_subtitle` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_subtitle_value` longtext,
  `field_mt_subtitle_summary` longtext,
  `field_mt_subtitle_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_subtitle_format` (`field_mt_subtitle_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 36 (field_mt_subtitle)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_subtitle`
--

LOCK TABLES `field_data_field_mt_subtitle` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_subtitle` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_subtitle` VALUES ('node','mt_testimonial',0,3,3,'und',0,'Marketing specialist','','filtered_html'),('node','mt_testimonial',0,24,24,'und',0,'Commercial director','','filtered_html'),('node','mt_benefit',0,29,29,'und',0,'100+','','filtered_html'),('node','mt_benefit',0,30,30,'und',0,'13','','filtered_html'),('node','mt_benefit',0,31,31,'und',0,'122k','',NULL),('node','mt_testimonial',0,49,49,'und',0,'Business Solutions','','filtered_html'),('node','mt_testimonial',0,50,50,'und',0,'Business Solutions','','filtered_html'),('node','mt_testimonial',0,51,51,'und',0,'Lorem sit amet','','filtered_html');
/*!40000 ALTER TABLE `field_data_field_mt_subtitle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_testimonial_image`
--

DROP TABLE IF EXISTS `field_data_field_mt_testimonial_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_testimonial_image` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_testimonial_image_fid` int(10) unsigned DEFAULT NULL COMMENT 'The file_managed.fid being referenced in this field.',
  `field_mt_testimonial_image_alt` varchar(512) DEFAULT NULL COMMENT 'Alternative image text, for the image’s ’alt’ attribute.',
  `field_mt_testimonial_image_title` varchar(1024) DEFAULT NULL COMMENT 'Image title text, for the image’s ’title’ attribute.',
  `field_mt_testimonial_image_width` int(10) unsigned DEFAULT NULL COMMENT 'The width of the image in pixels.',
  `field_mt_testimonial_image_height` int(10) unsigned DEFAULT NULL COMMENT 'The height of the image in pixels.',
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_testimonial_image_fid` (`field_mt_testimonial_image_fid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 42 (field_mt_testimonial_image)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_testimonial_image`
--

LOCK TABLES `field_data_field_mt_testimonial_image` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_testimonial_image` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_testimonial_image` VALUES ('node','mt_testimonial',0,3,3,'und',0,190,'','',100,100),('node','mt_testimonial',0,24,24,'und',0,188,'','',100,100),('node','mt_testimonial',0,49,49,'und',0,266,'','',100,100),('node','mt_testimonial',0,50,50,'und',0,267,'','',100,100),('node','mt_testimonial',0,51,51,'und',0,268,'','',100,100);
/*!40000 ALTER TABLE `field_data_field_mt_testimonial_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_mt_twitter_account`
--

DROP TABLE IF EXISTS `field_data_field_mt_twitter_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_mt_twitter_account` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_twitter_account_value` varchar(255) DEFAULT NULL,
  `field_mt_twitter_account_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_twitter_account_format` (`field_mt_twitter_account_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 40 (field_mt_twitter_account)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_mt_twitter_account`
--

LOCK TABLES `field_data_field_mt_twitter_account` WRITE;
/*!40000 ALTER TABLE `field_data_field_mt_twitter_account` DISABLE KEYS */;
INSERT INTO `field_data_field_mt_twitter_account` VALUES ('node','mt_team_member',0,36,36,'und',0,'https://twitter.com/',NULL),('node','mt_team_member',0,37,37,'und',0,'https://twitter.com/',NULL),('node','mt_team_member',0,38,38,'und',0,'https://twitter.com/',NULL),('node','mt_team_member',0,39,39,'und',0,'https://twitter.com/',NULL),('node','mt_team_member',0,40,40,'und',0,'https://twitter.com/',NULL);
/*!40000 ALTER TABLE `field_data_field_mt_twitter_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_data_field_tags`
--

DROP TABLE IF EXISTS `field_data_field_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_data_field_tags` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_tags_tid` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_tags_tid` (`field_tags_tid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 3 (field_tags)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_data_field_tags`
--

LOCK TABLES `field_data_field_tags` WRITE;
/*!40000 ALTER TABLE `field_data_field_tags` DISABLE KEYS */;
INSERT INTO `field_data_field_tags` VALUES ('node','blog',0,13,13,'und',0,12),('node','blog',0,13,13,'und',1,13),('node','blog',0,13,13,'und',2,14),('node','blog',0,14,14,'und',0,12),('node','blog',0,14,14,'und',1,13),('node','blog',0,15,15,'und',0,10),('node','blog',0,15,15,'und',1,11),('node','blog',0,52,52,'und',0,13),('node','blog',0,53,53,'und',0,13),('node','blog',0,53,53,'und',1,10),('node','blog',0,54,54,'und',0,11),('node','blog',0,54,54,'und',1,13);
/*!40000 ALTER TABLE `field_data_field_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_body`
--

DROP TABLE IF EXISTS `field_revision_body`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_body` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `body_value` longtext,
  `body_summary` longtext,
  `body_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `body_format` (`body_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 2 (body)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_body`
--

LOCK TABLES `field_revision_body` WRITE;
/*!40000 ALTER TABLE `field_revision_body` DISABLE KEYS */;
INSERT INTO `field_revision_body` VALUES ('node','page',0,1,1,'und',0,'<p class=\"large\">Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies appropriately communicate.</p> \r\n<h2>Heading 2</h2>\r\n<p>\r\nDonec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer <a href=\"#\">posuere erat a ante</a> venenatis dapibus posuere velit aliquet.\r\n</p>\r\n<h2><a href=\"#\">Linked Heading 2</a></h2>\r\n<p>\r\nDonec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer <a href=\"#\">posuere erat a ante</a> venenatis dapibus posuere velit aliquet.\r\n</p>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Sit, esse, quo distinctio dolores magni reprehenderit id est at fugiat veritatis fugit dignissimos sed ut facere molestias illo impedit. Tempora, iure!\r\n</p>\r\n<h3>Heading 3</h3>\r\n<p>\r\nDonec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer <a href=\"#\">posuere erat a ante</a> venenatis dapibus posuere velit aliquet.\r\n</p>\r\n<h4>Heading 4</h4>\r\n<p>\r\nDonec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer <a href=\"#\">posuere erat a ante</a> venenatis dapibus posuere velit aliquet.\r\n</p>\r\n<h5>Heading 5</h5>\r\n<p>\r\nDonec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer <a href=\"#\">posuere erat a ante</a> venenatis dapibus posuere velit aliquet.\r\n</p>\r\n<blockquote>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima at dolores culpa, consectetur nihil, voluptate quaerat, provident.</p>\r\n<footer>Johnny Trulove <cite title=\"Source Title\">Commercial Director</cite></footer>\r\n</blockquote>\r\n<h3>Messages</h3>\r\n<div class=\"messages status\">\r\nSample status message. Page <em><strong>Typography</strong></em> has been updated.\r\n</div>\r\n<div class=\"messages error\">\r\nSample error message. There is a security update available for your version of Drupal. To ensure the security of your server, you should update immediately! See the available updates page for more information.\r\n</div>\r\n<div class=\"messages warning\">\r\nSample warning message. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\r\n</div>\r\n<br>\r\n<h3>Paragraph With Links</h3>\r\n<p>\r\nLorem ipsum dolor sit amet, <a href=\"#\">consectetuer adipiscing</a> elit. Donec odio. Quisque volutpat mattis eros. <a href=\"#\">Nullam malesuada</a> erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.\r\n</p>\r\n<h3>Ordered List</h3>\r\n<ol>\r\n<li>\r\nThis is a sample Ordered List.\r\n</li>\r\n<li>\r\nLorem ipsum dolor sit amet consectetuer.\r\n</li>\r\n<li>\r\nCongue Quisque augue elit dolor.\r\n<ol>\r\n<li>\r\nSomething goes here.\r\n</li>\r\n<li>\r\nAnd another here\r\n</li>\r\n</ol>\r\n</li>\r\n<li>\r\nCongue Quisque augue elit dolor nibh.\r\n</li>\r\n</ol>\r\n\r\n<h3>Unordered List</h3>\r\n<ul>\r\n<li>\r\nThis is a sample <strong>Unordered List</strong>.\r\n</li>\r\n<li>\r\nCondimentum quis.\r\n</li>\r\n<li>\r\nCongue Quisque augue elit dolor.\r\n<ul>\r\n<li>\r\nSomething goes here.\r\n</li>\r\n<li>\r\nAnd another here\r\n<ul>\r\n<li>\r\nSomething here as well\r\n</li>\r\n<li>\r\nSomething here as well\r\n</li>\r\n<li>\r\nSomething here as well\r\n</li>\r\n</ul>\r\n</li>\r\n<li>\r\nThen one more\r\n</li>\r\n</ul>\r\n</li>\r\n<li>\r\nNunc cursus sem et pretium sapien eget.\r\n</li>\r\n</ul>\r\n\r\n<h3>Fieldset</h3>\r\n<fieldset><legend>Account information</legend></fieldset>\r\n\r\n<br>\r\n<h3>Buttons &amp; Text fields</h3>\r\n<a href=\"#\" class=\"more\">Find out more</a>\r\n<h4>Text Fields</h4>\r\n<form action=\"#\">\r\n<div class=\"form-item form-type-textfield\">\r\n<input type=\"text\" class=\"form-text\" name=\"subscribe\" value=\"Your email address\" onfocus=\"if (this.value == \'Your email address\') {this.value = \'\';}\" onblur=\"if (this.value == \'\') {this.value = \'Your email address\';}\">\r\n</div>\r\n<div class=\"form-actions\">\r\n<input value=\"Subscribe\" type=\"submit\" id=\"edit-submit-3\" name=\"subscribe\" class=\"form-submit\">\r\n</div>\r\n</form>\r\n<form action=\"#\" class=\"container-inline\">\r\n<div class=\"form-item form-type-textfield\">\r\n<input type=\"text\" class=\"form-text\" name=\"subscribe\" value=\"Inline form items\" onfocus=\"if (this.value == \'Your email address\') {this.value = \'\';}\" onblur=\"if (this.value == \'\') {this.value = \'Inline form items\';}\"></div>\r\n<div class=\"form-actions\">\r\n<input value=\"Subscribe\" type=\"submit\" id=\"edit-submit-4\" name=\"subscribe\" class=\"form-submit\">\r\n</div>\r\n</form>\r\n<h4>Select Dropdown</h4>\r\n<select name=\"select_field\" class=\"form-select\"><option value=\"All\" selected=\"selected\">- Any -</option><option value=\"6\">Apartment</option><option value=\"7\">Family House</option><option value=\"8\">Office</option></select>\r\n<br>\r\n<h3>Table</h3>\r\n<table>\r\n<tr>\r\n<th>Header 1</th>\r\n<th>Header 2</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>row 1, cell 1</td>\r\n<td>row 1, cell 2</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>row 2, cell 1</td>\r\n<td>row 2, cell 2</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>row 3, cell 1</td>\r\n<td>row 3, cell 2</td>\r\n</tr>\r\n</table>\r\n\r\n<h4>Responsive Table</h4>\r\n<div class=\"table-responsive\">\r\n<table>\r\n<tr>\r\n<th>Header 1</th>\r\n<th>Header 2</th>\r\n<th>Header 3</th>\r\n<th>Header 4</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>row 1, cell 1</td>\r\n<td>row 1, cell 2</td>\r\n<td>row 1, cell 3</td>\r\n<td>row 1, cell 4</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>row 2, cell 1</td>\r\n<td>row 2, cell 2</td>\r\n<td>row 2, cell 3</td>\r\n<td>row 2, cell 4</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>row 3, cell 1</td>\r\n<td>row 3, cell 2</td>\r\n<td>row 3, cell 3</td>\r\n<td>row 3, cell 4</td>\r\n</tr>\r\n</table>\r\n</div>','','full_html'),('node','page',0,2,2,'und',0,'<div class=\"subheader\">\r\n<p class=\"large\">Completely drive standardized initiatives with principle-centered ROI. Progressively aggregate emerging content rather than leveraged bandwidth <a href=\"#\">and tech-enabled innovative materials</a> with a touch of uniqueness.</p>\r\n</div>\r\n\r\n<div class=\"row\">\r\n\r\n<div class=\"col-md-4 col-sm-6\">\r\n<div class=\"team-member clearfix\">\r\n<a href=\"<?php print base_path();?>contact-us\" class=\"overlayed\">\r\n<span class=\"overlay\"><i class=\"fa fa-link\"></i></span>\r\n<img src=\"<?php print base_path();?>sites/default/files/about-1.jpg\" alt=\"\" />\r\n</a>\r\n<h3><a href=\"<?php print base_path();?>contact-us\">Lorem Ipsum</a></h3>\r\n<p class=\"subtitle\">Founder &amp; CEO</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n<ul class=\"contact-info\">\r\n<li class=\"phone\"><i class=\"fa fa-phone\"><span class=\"sr-only\">phone</span></i> +1 212-582-8102</li>\r\n<li class=\"email\"><i class=\"fa fa-envelope\"><span class=\"sr-only\">email</span></i> <a href=\"mailto:lorem.ipsum@levelplus.com\">lorem.ipsum@levelplus.com</a></li>\r\n</ul>\r\n<ul class=\"social-bookmarks text-center\">\r\n<li class=\"facebook\"><a href=\"http://www.facebook.com/morethan.just.themes/\"><i class=\"fa fa-facebook\"><span class=\"sr-only\">facebook</span></i></a></li>\r\n<li class=\"twitter\"><a href=\"http://twitter.com/morethanthemes/\"><i class=\"fa fa-twitter\"><span class=\"sr-only\">twitter</span></i></a></li>\r\n<li class=\"googleplus\"><a href=\"https://plus.google.com/118354321025436191714/posts\"><i class=\"fa fa-google-plus\"><span class=\"sr-only\">google plus</span></i></a></li>    \r\n<li class=\"linkedin\"><a href=\"http://www.linkedin.com/company/more-than-themes/\"><i class=\"fa fa-linkedin\"><span class=\"sr-only\">linkedin</span></i></a></li>\r\n</ul>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-4 col-sm-6\">\r\n<div class=\"team-member clearfix\">\r\n<a href=\"<?php print base_path();?>contact-us\" class=\"overlayed\">\r\n<span class=\"overlay\"><i class=\"fa fa-link\"></i></span>\r\n<img src=\"<?php print base_path();?>sites/default/files/about-2.jpg\" alt=\"\" />\r\n</a>\r\n<h3><a href=\"<?php print base_path();?>contact-us\">Lorem Ipsum</a></h3>\r\n<p class=\"subtitle\">Chief Operating Officer</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n<ul class=\"contact-info\">\r\n<li class=\"phone\"><i class=\"fa fa-phone\"><span class=\"sr-only\">phone</span></i> +1 212-582-8102</li>\r\n<li class=\"email\"><i class=\"fa fa-envelope\"><span class=\"sr-only\">email</span></i> <a href=\"mailto:lorem.ipsum@levelplus.com\">lorem.ipsum@levelplus.com</a></li>\r\n</ul>\r\n<ul class=\"social-bookmarks text-center\">\r\n<li class=\"facebook\"><a href=\"http://www.facebook.com/morethan.just.themes/\"><i class=\"fa fa-facebook\"><span class=\"sr-only\">facebook</span></i></a></li>\r\n<li class=\"twitter\"><a href=\"http://twitter.com/morethanthemes/\"><i class=\"fa fa-twitter\"><span class=\"sr-only\">twitter</span></i></a></li>\r\n<li class=\"googleplus\"><a href=\"https://plus.google.com/118354321025436191714/posts\"><i class=\"fa fa-google-plus\"><span class=\"sr-only\">google plus</span></i></a></li>\r\n<li class=\"linkedin\"><a href=\"http://www.linkedin.com/company/more-than-themes/\"><i class=\"fa fa-linkedin\"><span class=\"sr-only\">linkedin</span></i></a></li>\r\n</ul>\r\n</div>\r\n</div>\r\n\r\n<div class=\"clearfix visible-sm\"></div>\r\n\r\n<div class=\"col-md-4 col-sm-6\">\r\n<div class=\"team-member clearfix\">\r\n<a href=\"<?php print base_path();?>contact-us\" class=\"overlayed\">\r\n<span class=\"overlay\"><i class=\"fa fa-link\"></i></span>\r\n<img src=\"<?php print base_path();?>sites/default/files/about-3.jpg\" alt=\"\" />\r\n</a>\r\n<h3><a href=\"<?php print base_path();?>contact-us\">Lorem Ipsum</a></h3>\r\n<p class=\"subtitle\">Chief Financial Officer</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n<ul class=\"contact-info\">\r\n<li class=\"phone\"><i class=\"fa fa-phone\"><span class=\"sr-only\">phone</span></i> +1 212-582-8102</li>\r\n<li class=\"email\"><i class=\"fa fa-envelope\"><span class=\"sr-only\">email</span></i> <a href=\"mailto:lorem.ipsum@levelplus.com\">lorem.ipsum@levelplus.com</a></li>\r\n</ul>\r\n<ul class=\"social-bookmarks text-center\">\r\n<li class=\"facebook\"><a href=\"http://www.facebook.com/morethan.just.themes/\"><i class=\"fa fa-facebook\"><span class=\"sr-only\">facebook</span></i></a></li>\r\n<li class=\"twitter\"><a href=\"http://twitter.com/morethanthemes/\"><i class=\"fa fa-twitter\"><span class=\"sr-only\">twitter</span></i></a></li>\r\n<li class=\"googleplus\"><a href=\"https://plus.google.com/118354321025436191714/posts\"><i class=\"fa fa-google-plus\"><span class=\"sr-only\">google plus</span></i></a></li>\r\n<li class=\"linkedin\"><a href=\"http://www.linkedin.com/company/more-than-themes/\"><i class=\"fa fa-linkedin\"><span class=\"sr-only\">linkedin</span></i></a></li>\r\n</ul>\r\n</div>\r\n</div>\r\n\r\n<div class=\"clearfix visible-md visible-lg\"></div>\r\n\r\n<div class=\"col-md-4 col-sm-6\">\r\n<div class=\"team-member clearfix\">\r\n<a href=\"<?php print base_path();?>contact-us\" class=\"overlayed\">\r\n<span class=\"overlay\"><i class=\"fa fa-link\"></i></span>\r\n<img src=\"<?php print base_path();?>sites/default/files/about-4.jpg\" alt=\"\" />\r\n</a>\r\n<h3><a href=\"<?php print base_path();?>contact-us\">Lorem Ipsum</a></h3>\r\n<p class=\"subtitle\">Chief Financial Officer</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n<ul class=\"contact-info\">\r\n<li class=\"phone\"><i class=\"fa fa-phone\"><span class=\"sr-only\">phone</span></i> +1 212-582-8102</li>\r\n<li class=\"email\"><i class=\"fa fa-envelope\"><span class=\"sr-only\">email</span></i> <a href=\"mailto:lorem.ipsum@levelplus.com\">lorem.ipsum@levelplus.com</a></li>\r\n</ul>\r\n<ul class=\"social-bookmarks text-center\">\r\n<li class=\"facebook\"><a href=\"http://www.facebook.com/morethan.just.themes/\"><i class=\"fa fa-facebook\"><span class=\"sr-only\">facebook</span></i></a></li>\r\n<li class=\"twitter\"><a href=\"http://twitter.com/morethanthemes/\"><i class=\"fa fa-twitter\"><span class=\"sr-only\">twitter</span></i></a></li>\r\n<li class=\"googleplus\"><a href=\"https://plus.google.com/118354321025436191714/posts\"><i class=\"fa fa-google-plus\"><span class=\"sr-only\">google plus</span></i></a></li>\r\n<li class=\"linkedin\"><a href=\"http://www.linkedin.com/company/more-than-themes/\"><i class=\"fa fa-linkedin\"><span class=\"sr-only\">linkedin</span></i></a></li>\r\n</ul>\r\n</div>\r\n</div>\r\n\r\n<div class=\"clearfix visible-sm\"></div>\r\n\r\n<div class=\"col-md-4 col-sm-6\">\r\n<div class=\"team-member clearfix\">\r\n<a href=\"<?php print base_path();?>contact-us\" class=\"overlayed\">\r\n<span class=\"overlay\"><i class=\"fa fa-link\"></i></span>\r\n<img src=\"<?php print base_path();?>sites/default/files/about-5.jpg\" alt=\"\" />\r\n</a>\r\n<h3><a href=\"<?php print base_path();?>contact-us\">Lorem Ipsum</a></h3>\r\n<p class=\"subtitle\">Chief Financial Officer</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n<ul class=\"contact-info\">\r\n<li class=\"phone\"><i class=\"fa fa-phone\"><span class=\"sr-only\">phone</span></i> +1 212-582-8102</li>\r\n<li class=\"email\"><i class=\"fa fa-envelope\"><span class=\"sr-only\">email</span></i> <a href=\"mailto:lorem.ipsum@levelplus.com\">lorem.ipsum@levelplus.com</a></li>\r\n</ul>\r\n<ul class=\"social-bookmarks text-center\">\r\n<li class=\"facebook\"><a href=\"http://www.facebook.com/morethan.just.themes/\"><i class=\"fa fa-facebook\"><span class=\"sr-only\">facebook</span></i></a></li>\r\n<li class=\"twitter\"><a href=\"http://twitter.com/morethanthemes/\"><i class=\"fa fa-twitter\"><span class=\"sr-only\">twitter</span></i></a></li>\r\n<li class=\"googleplus\"><a href=\"https://plus.google.com/118354321025436191714/posts\"><i class=\"fa fa-google-plus\"><span class=\"sr-only\">google plus</span></i></a></li>\r\n<li class=\"linkedin\"><a href=\"http://www.linkedin.com/company/more-than-themes/\"><i class=\"fa fa-linkedin\"><span class=\"sr-only\">linkedin</span></i></a></li>\r\n</ul>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-4 col-sm-6\">\r\n<div class=\"team-member clearfix\">\r\n<a href=\"<?php print base_path();?>contact-us\" class=\"overlayed\">\r\n<span class=\"overlay\"><i class=\"fa fa-link\"></i></span>\r\n<img src=\"<?php print base_path();?>sites/default/files/about-6.jpg\" alt=\"\" />\r\n</a>\r\n<h3><a href=\"<?php print base_path();?>contact-us\">Lorem Ipsum</a></h3>\r\n<p class=\"subtitle\">Chief Financial Officer</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n<ul class=\"contact-info\">\r\n<li class=\"phone\"><i class=\"fa fa-phone\"><span class=\"sr-only\">phone</span></i> +1 212-582-8102</li>\r\n<li class=\"email\"><i class=\"fa fa-envelope\"><span class=\"sr-only\">email</span></i> <a href=\"mailto:lorem.ipsum@levelplus.com\">lorem.ipsum@levelplus.com</a></li>\r\n</ul>\r\n<ul class=\"social-bookmarks text-center\">\r\n<li class=\"facebook\"><a href=\"http://www.facebook.com/morethan.just.themes/\"><i class=\"fa fa-facebook\"><span class=\"sr-only\">facebook</span></i></a></li>\r\n<li class=\"twitter\"><a href=\"http://twitter.com/morethanthemes/\"><i class=\"fa fa-twitter\"><span class=\"sr-only\">twitter</span></i></a></li>\r\n<li class=\"googleplus\"><a href=\"https://plus.google.com/118354321025436191714/posts\"><i class=\"fa fa-google-plus\"><span class=\"sr-only\">google plus</span></i></a></li>\r\n<li class=\"linkedin\"><a href=\"http://www.linkedin.com/company/more-than-themes/\"><i class=\"fa fa-linkedin\"><span class=\"sr-only\">linkedin</span></i></a></li>\r\n</ul>\r\n</div>\r\n</div>\r\n\r\n</div>','','php_code'),('node','mt_testimonial',0,3,3,'und',0,'Excepteur sint occaecat cupidatat non proident. Lorem ipsum dolor sit amet, consectetur.','','filtered_html'),('node','page',0,8,8,'und',0,'\r\n<h1>Columns</h1>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-6\">\r\n<h4>One Half</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>\r\n</div> \r\n\r\n<div class=\"col-md-6\"> \r\n<h4>One Half</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>\r\n</div> \r\n</div>\r\n<pre style=\"margin-bottom:40px;\">\r\n&lt;div class=\"row\"&gt;\r\n\r\n&lt;div class=\"col-md-6\"&gt;\r\n&lt;h4&gt;One Half&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-6\"&gt; \r\n&lt;h4&gt;One Half&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;/div&gt;\r\n</pre>\r\n<br/>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\"> \r\n<h4>One Third</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis.</p>\r\n</div> \r\n\r\n<div class=\"col-md-4\"> \r\n<h4>One Third</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis.</p>\r\n</div> \r\n\r\n<div class=\"col-md-4\"> \r\n<h4>One Third</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis.</p>\r\n</div> \r\n</div>\r\n\r\n<pre style=\"margin-bottom:40px;\">\r\n&lt;div class=\"row\"&gt;\r\n\r\n&lt;div class=\"col-md-4\"&gt; \r\n&lt;h4&gt;One Third&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui..&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-4\"&gt; \r\n&lt;h4&gt;One Third&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui..&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-4\"&gt; \r\n&lt;h4&gt;One Third&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui..&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;/div&gt;\r\n</pre>\r\n<br/>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-3\"> \r\n<h4>One Fourth</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut.</p>\r\n</div> \r\n\r\n<div class=\"col-md-3\"> \r\n<h4>One Fourth</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut.</p>\r\n</div> \r\n\r\n<div class=\"col-md-3\"> \r\n<h4>One Fourth</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut.</p>\r\n</div>\r\n\r\n<div class=\"col-md-3\"> \r\n<h4>One Fourth</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut.</p>\r\n</div>\r\n</div>\r\n\r\n<pre style=\"margin-bottom:40px;\">\r\n&lt;div class=\"row\"&gt;\r\n\r\n&lt;div class=\"col-md-3\"&gt; \r\n&lt;h4&gt;One Fourth&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui..&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-3\"&gt; \r\n&lt;h4&gt;One Fourth&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui..&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-3\"&gt; \r\n&lt;h4&gt;One Fourth&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui..&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=\"col-md-3\"&gt; \r\n&lt;h4&gt;One Fourth&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui..&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;/div&gt;\r\n</pre>\r\n<br/>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-4\"> \r\n<h4>One Third</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut. Lorem ipsum dolor sit amet, consectetur adipisicing.</p>\r\n</div> \r\n\r\n<div class=\"col-md-8\"> \r\n<h4>Two Thirds</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint, doloribus, tempora, numquam repellendus maiores facere a atque reiciendis voluptatibus hic veritatis ratione reprehenderit.</p>\r\n</div> \r\n</div>\r\n\r\n<pre style=\"margin-bottom:40px;\">\r\n&lt;div class=\"row\"&gt;\r\n\r\n&lt;div class=\"col-md-4\"&gt; \r\n&lt;h4&gt;One Third&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-8\"&gt; \r\n&lt;h4&gt;Two Thirds&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;/div&gt;\r\n</pre>\r\n<br/>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-8\"> \r\n<h4>Two Thirds</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint, doloribus, tempora, numquam repellendus maiores facere a atque reiciendis voluptatibus hic veritatis ratione reprehenderit.</p>\r\n</div> \r\n\r\n<div class=\"col-md-4\"> \r\n<h4>One Third</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut. Lorem ipsum dolor sit amet, consectetur adipisicing.</p>\r\n</div> \r\n</div>\r\n\r\n<pre style=\"margin-bottom:40px;\">\r\n&lt;div class=\"row\"&gt;\r\n\r\n&lt;div class=\"col-md-8\"&gt; \r\n&lt;h4&gt;Two Thirds&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-4\"&gt; \r\n&lt;h4&gt;One Third/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;/div&gt;\r\n</pre>\r\n<br/>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-3\"> \r\n<h4>One Fourth</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut.</p>\r\n</div> \r\n\r\n<div class=\"col-md-9\"> \r\n<h4>Three Fourths</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint, doloribus, tempora, numquam repellendus maiores facere a atque reiciendis voluptatibus hic veritatis ratione reprehenderit.</p>\r\n</div> \r\n</div>\r\n\r\n<pre style=\"margin-bottom:40px;\">\r\n&lt;div class=\"row\"&gt;\r\n\r\n&lt;div class=\"col-md-3\"&gt; \r\n&lt;h4&gt;One Fourth&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-9\"&gt; \r\n&lt;h4&gt;Three Fourths&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;/div&gt;\r\n</pre>\r\n<br/>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-9\"> \r\n<h4>Three Fourths</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint, doloribus, tempora, numquam repellendus maiores facere a atque reiciendis voluptatibus hic veritatis ratione reprehenderit.</p>\r\n</div> \r\n\r\n<div class=\"col-md-3\"> \r\n<h4>One Fourth</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut.</p>\r\n</div> \r\n</div>\r\n\r\n<pre style=\"margin-bottom:40px;\">\r\n&lt;div class=\"row\"&gt;\r\n\r\n&lt;div class=\"col-md-9\"&gt; \r\n&lt;h4&gt;Three Fourths&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=\"col-md-3\"&gt; \r\n&lt;h4&gt;One Fourth&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;/div&gt;\r\n</pre>\r\n<br/>','','full_html'),('node','page',0,9,9,'und',0,'\r\n<h1>Columns</h1>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-6\">\r\n<h4>One Half</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>\r\n</div> \r\n\r\n<div class=\"col-md-6\"> \r\n<h4>One Half</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>\r\n</div> \r\n</div>\r\n<pre style=\"margin-bottom:40px;\">\r\n&lt;div class=\"row\"&gt;\r\n\r\n&lt;div class=\"col-md-6\"&gt;\r\n&lt;h4&gt;One Half&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-6\"&gt; \r\n&lt;h4&gt;One Half&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;/div&gt;\r\n</pre>\r\n<br/>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\"> \r\n<h4>One Third</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis.</p>\r\n</div> \r\n\r\n<div class=\"col-md-4\"> \r\n<h4>One Third</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis.</p>\r\n</div> \r\n\r\n<div class=\"col-md-4\"> \r\n<h4>One Third</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis.</p>\r\n</div> \r\n</div>\r\n\r\n<pre style=\"margin-bottom:40px;\">\r\n&lt;div class=\"row\"&gt;\r\n\r\n&lt;div class=\"col-md-4\"&gt; \r\n&lt;h4&gt;One Third&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui..&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-4\"&gt; \r\n&lt;h4&gt;One Third&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui..&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-4\"&gt; \r\n&lt;h4&gt;One Third&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui..&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;/div&gt;\r\n</pre>\r\n<br/>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-3\"> \r\n<h4>One Fourth</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut.</p>\r\n</div> \r\n\r\n<div class=\"col-md-3\"> \r\n<h4>One Fourth</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut.</p>\r\n</div> \r\n\r\n<div class=\"col-md-3\"> \r\n<h4>One Fourth</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut.</p>\r\n</div>\r\n\r\n<div class=\"col-md-3\"> \r\n<h4>One Fourth</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut.</p>\r\n</div>\r\n</div>\r\n\r\n<pre style=\"margin-bottom:40px;\">\r\n&lt;div class=\"row\"&gt;\r\n\r\n&lt;div class=\"col-md-3\"&gt; \r\n&lt;h4&gt;One Fourth&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui..&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-3\"&gt; \r\n&lt;h4&gt;One Fourth&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui..&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-3\"&gt; \r\n&lt;h4&gt;One Fourth&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui..&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=\"col-md-3\"&gt; \r\n&lt;h4&gt;One Fourth&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui..&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;/div&gt;\r\n</pre>\r\n<br/>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-4\"> \r\n<h4>One Third</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut. Lorem ipsum dolor sit amet, consectetur adipisicing.</p>\r\n</div> \r\n\r\n<div class=\"col-md-8\"> \r\n<h4>Two Thirds</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint, doloribus, tempora, numquam repellendus maiores facere a atque reiciendis voluptatibus hic veritatis ratione reprehenderit.</p>\r\n</div> \r\n</div>\r\n\r\n<pre style=\"margin-bottom:40px;\">\r\n&lt;div class=\"row\"&gt;\r\n\r\n&lt;div class=\"col-md-4\"&gt; \r\n&lt;h4&gt;One Third&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-8\"&gt; \r\n&lt;h4&gt;Two Thirds&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;/div&gt;\r\n</pre>\r\n<br/>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-8\"> \r\n<h4>Two Thirds</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint, doloribus, tempora, numquam repellendus maiores facere a atque reiciendis voluptatibus hic veritatis ratione reprehenderit.</p>\r\n</div> \r\n\r\n<div class=\"col-md-4\"> \r\n<h4>One Third</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut. Lorem ipsum dolor sit amet, consectetur adipisicing.</p>\r\n</div> \r\n</div>\r\n\r\n<pre style=\"margin-bottom:40px;\">\r\n&lt;div class=\"row\"&gt;\r\n\r\n&lt;div class=\"col-md-8\"&gt; \r\n&lt;h4&gt;Two Thirds&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-4\"&gt; \r\n&lt;h4&gt;One Third/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;/div&gt;\r\n</pre>\r\n<br/>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-3\"> \r\n<h4>One Fourth</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut.</p>\r\n</div> \r\n\r\n<div class=\"col-md-9\"> \r\n<h4>Three Fourths</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint, doloribus, tempora, numquam repellendus maiores facere a atque reiciendis voluptatibus hic veritatis ratione reprehenderit.</p>\r\n</div> \r\n</div>\r\n\r\n<pre style=\"margin-bottom:40px;\">\r\n&lt;div class=\"row\"&gt;\r\n\r\n&lt;div class=\"col-md-3\"&gt; \r\n&lt;h4&gt;One Fourth&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;div class=\"col-md-9\"&gt; \r\n&lt;h4&gt;Three Fourths&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;/div&gt;\r\n</pre>\r\n<br/>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-9\"> \r\n<h4>Three Fourths</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint, doloribus, tempora, numquam repellendus maiores facere a atque reiciendis voluptatibus hic veritatis ratione reprehenderit.</p>\r\n</div> \r\n\r\n<div class=\"col-md-3\"> \r\n<h4>One Fourth</h4>\r\n<p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut.</p>\r\n</div> \r\n</div>\r\n\r\n<pre style=\"margin-bottom:40px;\">\r\n&lt;div class=\"row\"&gt;\r\n\r\n&lt;div class=\"col-md-9\"&gt; \r\n&lt;h4&gt;Three Fourths&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=\"col-md-3\"&gt; \r\n&lt;h4&gt;One Fourth&lt;/h4&gt;\r\n&lt;p&gt;Donec sed odio dui...&lt;/p&gt;\r\n&lt;/div&gt; \r\n\r\n&lt;/div&gt;\r\n</pre>\r\n<br/>','','full_html'),('node','mt_service',0,10,10,'und',0,'<h2>Heading 2</h2>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>\r\n','<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  ','full_html'),('node','mt_service',0,11,11,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>','','full_html'),('node','mt_service',0,12,12,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>','','full_html'),('node','blog',0,13,13,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>\r\n\r\n<h3 class=\"description-title\">Lorem ipsum dolor sit amed consetatur</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>','','full_html'),('node','blog',0,14,14,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>\r\n\r\n<h3 class=\"description-title\">Lorem ipsum dolor sit amed consetatur</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>','','full_html'),('node','blog',0,15,15,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>\r\n\r\n<h3 class=\"description-title\">Lorem ipsum dolor sit amed consetatur</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>','','full_html'),('node','mt_showcase',0,16,16,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas <a href=\"#\">state of the art interfaces</a>. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Completely parallel task market positioning interfaces through visionary niche markets, proactively incentivize sticky quality vectors before future-proof internal or \"organic\" sources.</blockquote>\r\n<p>Seamlessly procrastinate proactive outsourcing whereas wireless products. Progressively leverage existing 24/365 experiences through timely initiatives. Quickly facilitate frictionless ROI rather than equity invested solutions.</p>\r\n\r\n<h3 class=\"description-title\">Showcase specifics</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>','','full_html'),('node','mt_showcase',0,17,17,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas <a href=\"#\">state of the art interfaces</a>. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Completely parallel task market positioning interfaces through visionary niche markets, proactively incentivize sticky quality vectors before future-proof internal or \"organic\" sources.</blockquote>\r\n<p>Seamlessly procrastinate proactive outsourcing whereas wireless products. Progressively leverage existing 24/365 experiences through timely initiatives. Quickly facilitate frictionless ROI rather than equity invested solutions.</p>\r\n\r\n<h3 class=\"description-title\">Showcase specifics</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>','','full_html'),('node','mt_showcase',0,18,18,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas <a href=\"#\">state of the art interfaces</a>. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Completely parallel task market positioning interfaces through visionary niche markets, proactively incentivize sticky quality vectors before future-proof internal or \"organic\" sources.</blockquote>\r\n<p>Seamlessly procrastinate proactive outsourcing whereas wireless products. Progressively leverage existing 24/365 experiences through timely initiatives. Quickly facilitate frictionless ROI rather than equity invested solutions.</p>\r\n\r\n<h3 class=\"description-title\">Showcase specifics</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>','','full_html'),('node','mt_showcase',0,19,19,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas <a href=\"#\">state of the art interfaces</a>. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Completely parallel task market positioning interfaces through visionary niche markets, proactively incentivize sticky quality vectors before future-proof internal or \"organic\" sources.</blockquote>\r\n<p>Seamlessly procrastinate proactive outsourcing whereas wireless products. Progressively leverage existing 24/365 experiences through timely initiatives. Quickly facilitate frictionless ROI rather than equity invested solutions.</p>\r\n\r\n<h3 class=\"description-title\">Showcase specifics</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>','','full_html'),('node','mt_showcase',0,20,20,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas <a href=\"#\">state of the art interfaces</a>. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Completely parallel task market positioning interfaces through visionary niche markets, proactively incentivize sticky quality vectors before future-proof internal or \"organic\" sources.</blockquote>\r\n<p>Seamlessly procrastinate proactive outsourcing whereas wireless products. Progressively leverage existing 24/365 experiences through timely initiatives. Quickly facilitate frictionless ROI rather than equity invested solutions.</p>\r\n\r\n<h3 class=\"description-title\">Showcase specifics</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>','','full_html'),('node','mt_showcase',0,21,21,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas <a href=\"#\">state of the art interfaces</a>. Quickly enable prospective technology rather than open-source technologies. Seamlessly procrastinate proactive outsourcing whereas wireless products. Progressively leverage existing 24/365 experiences through timely initiatives.</p>  \r\n<blockquote>Completely parallel task market positioning interfaces through visionary niche markets, proactively incentivize sticky quality vectors before future-proof internal or \"organic\" sources.</blockquote>\r\n<p>Seamlessly procrastinate proactive outsourcing whereas wireless products. Progressively leverage existing 24/365 experiences through timely initiatives. Quickly facilitate frictionless ROI rather than equity invested solutions.</p>\r\n\r\n<h3 class=\"description-title\">Showcase specifics</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>','','full_html'),('node','mt_testimonial',0,24,24,'und',0,'Excepteur sint occaecat cupidatat non proident. Lorem ipsum dolor sit amet, consectetur.','','filtered_html'),('node','mt_service',0,25,25,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>','','full_html'),('node','page',0,26,26,'und',0,'<h2 id=\"brands\">Brands</h2>\r\n<ul class=\"brands\">\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-apple\"><span class=\"sr-only\">apple</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-android\"><span class=\"sr-only\">android</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-github\"><span class=\"sr-only\">github</span></i></a>\r\n</li>                        \r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-windows\"><span class=\"sr-only\">windows</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-linux\"><span class=\"sr-only\">linux</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-skype\"><span class=\"sr-only\">skype</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-btc\"><span class=\"sr-only\">btc</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-css3\"><span class=\"sr-only\">css3</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-html5\"><span class=\"sr-only\">html5</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-bitbucket\"><span class=\"sr-only\">bitbucket</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-maxcdn\"><span class=\"sr-only\">maxcdn</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-dropbox\"><span class=\"sr-only\">dropbox</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-facebook\"><span class=\"sr-only\">facebook</span></i></a>\r\n</li>\r\n<li>\r\n<a href=\"#\"><i class=\"fa fa-twitter\"><span class=\"sr-only\">twitter</span></i></a>\r\n</li>\r\n</ul>\r\n<pre>\r\n&lt;ul class=\"brands\"&gt;\r\n\r\n  &lt;li&gt;\r\n    &lt;a href=\"#\"&gt;&lt;i class=\"fa fa-apple\"&gt;&lt;span class=\"sr-only\"&gt;apple&lt;/span&gt;&lt;/i&gt;&lt;/a&gt;\r\n  &lt;/li&gt;\r\n  &lt;li&gt;\r\n    &lt;a href=\"#\"&gt;&lt;i class=\"fa fa-android\"&gt;&lt;span class=\"sr-only\"&gt;android&lt;/span&gt;&lt;/i&gt;&lt;/a&gt;\r\n  &lt;/li&gt;\r\n  &lt;li&gt;\r\n    &lt;a href=\"#\"&gt;&lt;i class=\"fa fa-github\"&gt;&lt;span class=\"sr-only\"&gt;github&lt;/span&gt;&lt;/i&gt;&lt;/a&gt;\r\n  &lt;/li&gt;\r\n  &lt;li&gt;\r\n    &lt;a href=\"#\"&gt;&lt;i class=\"fa fa-windows\"&gt;&lt;span class=\"sr-only\"&gt;windows&lt;/span&gt;&lt;/i&gt;&lt;/a&gt;\r\n  &lt;/li&gt;\r\n  &lt;li&gt;\r\n    &lt;a href=\"#\"&gt;&lt;i class=\"fa fa-linux\"&gt;&lt;span class=\"sr-only\"&gt;linux&lt;/span&gt;&lt;/i&gt;&lt;/a&gt;\r\n  &lt;/li&gt;\r\n  &lt;li&gt;\r\n    &lt;a href=\"#\"&gt;&lt;i class=\"fa fa-skype\"&gt;&lt;span class=\"sr-only\"&gt;skype&lt;/span&gt;&lt;/i&gt;&lt;/a&gt;\r\n  &lt;/li&gt;\r\n  &lt;li&gt;\r\n    &lt;a href=\"#\"&gt;&lt;i class=\"fa fa-btc\"&gt;&lt;span class=\"sr-only\"&gt;btc&lt;/span&gt;&lt;/i&gt;&lt;/a&gt;\r\n  &lt;/li&gt;\r\n  &lt;li&gt;\r\n    &lt;a href=\"#\"&gt;&lt;i class=\"fa fa-css3\"&gt;&lt;span class=\"sr-only\"&gt;css3&lt;/span&gt;&lt;/i&gt;&lt;/a&gt;\r\n  &lt;/li&gt;\r\n  &lt;li&gt;\r\n    &lt;a href=\"#\"&gt;&lt;i class=\"fa fa-html5\"&gt;&lt;span class=\"sr-only\"&gt;html5&lt;/span&gt;&lt;/i&gt;&lt;/a&gt;\r\n  &lt;/li&gt;\r\n  &lt;li&gt;\r\n    &lt;a href=\"#\"&gt;&lt;i class=\"fa fa-bitbucket\"&gt;&lt;span class=\"sr-only\"&gt;bitbucket&lt;/span&gt;&lt;/i&gt;&lt;/a&gt;\r\n  &lt;/li&gt;\r\n  &lt;li&gt;\r\n    &lt;a href=\"#\"&gt;&lt;i class=\"fa fa-maxcdn\"&gt;&lt;span class=\"sr-only\"&gt;maxcdn&lt;/span&gt;&lt;/i&gt;&lt;/a&gt;\r\n  &lt;/li&gt;\r\n  &lt;li&gt;\r\n    &lt;a href=\"#\"&gt;&lt;i class=\"fa fa-dropbox\"&gt;&lt;span class=\"sr-only\"&gt;dropbox&lt;/span&gt;&lt;/i&gt;&lt;/a&gt;\r\n  &lt;/li&gt;\r\n  &lt;li&gt;\r\n    &lt;a href=\"#\"&gt;&lt;i class=\"fa fa-facebook\"&gt;&lt;span class=\"sr-only\"&gt;facebook&lt;/span&gt;&lt;/i&gt;&lt;/a&gt;\r\n  &lt;/li&gt;\r\n  &lt;li&gt;\r\n    &lt;a href=\"#\"&gt;&lt;i class=\"fa fa-twitter\"&gt;&lt;span class=\"sr-only\"&gt;twitter&lt;/span&gt;&lt;/i&gt;&lt;/a&gt;\r\n  &lt;/li&gt;\r\n\r\n&lt;/ul&gt;\r\n</pre>\r\n<hr>\r\n<br>\r\n<h2 id=\"tabs\">Tabs</h2>\r\n<div role=\"tabpanel\">\r\n\r\n<!-- Nav tabs -->\r\n<ul class=\"nav nav-tabs\" role=\"tablist\">\r\n<li role=\"presentation\" class=\"active\"><a href=\"#home\" aria-controls=\"home\" role=\"tab\" data-toggle=\"tab\">Home</a></li>\r\n<li role=\"presentation\"><a href=\"#profile\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Profile</a></li>\r\n<li role=\"presentation\"><a href=\"#messages\" aria-controls=\"messages\" role=\"tab\" data-toggle=\"tab\">Messages</a></li>\r\n</ul>\r\n\r\n<!-- Tab panes -->\r\n<div class=\"tab-content\">\r\n<div role=\"tabpanel\" class=\"tab-pane fade in active\" id=\"home\"><p><strong>Home</strong> ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis, exercitationem, quaerat veniam repudiandae illo ratione eaque omnis obcaecati quidem distinctio sapiente quo assumenda amet cumque nobis nulla qui dolore autem.</p></div>\r\n<div role=\"tabpanel\" class=\"tab-pane fade\" id=\"profile\"><p><strong>Profile</strong> ipsum dolor sit amet, consectetur adipisicing elit. Ut odio facere minima porro quis. Maiores eius quibusdam et in corrupti necessitatibus consequatur debitis laudantium hic.</p></div>\r\n<div role=\"tabpanel\" class=\"tab-pane fade\" id=\"messages\"><p><strong>Messages</strong> ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis, optio error consectetur ullam porro eligendi mollitia odio numquam aut cumque. Sed, possimus recusandae itaque laboriosam nesciunt voluptates veniam aspernatur voluptate eaque ratione totam ipsa optio aliquam incidunt dolorum amet illum.</p></div>\r\n</div>\r\n\r\n</div>\r\n\r\n<pre>\r\n&lt;div role=\"tabpanel\"&gt;\r\n\r\n&lt;!-- Nav tabs --&gt;\r\n&lt;ul class=\"nav nav-tabs\" role=\"tablist\"&gt;\r\n  &lt;li role=\"presentation\" class=\"active\"&gt;\r\n    &lt;a href=\"#home\" aria-controls=\"home\" role=\"tab\" data-toggle=\"tab\"&gt;Home&lt;/a&gt;\r\n  &lt;/li&gt;\r\n  &lt;li role=\"presentation\"&gt;\r\n    &lt;a href=\"#profile\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\"&gt;Profile&lt;/a&gt;\r\n  &lt;/li&gt;\r\n  &lt;li role=\"presentation\"&gt;\r\n    &lt;a href=\"#messages\" aria-controls=\"messages\" role=\"tab\" data-toggle=\"tab\"&gt;Messages&lt;/a&gt;\r\n  &lt;/li&gt;\r\n&lt;/ul&gt;\r\n\r\n&lt;!-- Tab panes --&gt;\r\n&lt;div class=\"tab-content\"&gt;\r\n  &lt;div role=\"tabpanel\" class=\"tab-pane fade in active\" id=\"home\"&gt;\r\n    &lt;p&gt;&lt;strong&gt;Home&lt;/strong&gt; ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis, exercitationem, quaerat veniam repudiandae illo ratione eaque omnis obcaecati quidem distinctio sapiente quo assumenda amet cumque nobis nulla qui dolore autem.&lt;/p&gt;\r\n  &lt;/div&gt;\r\n  &lt;div role=\"tabpanel\" class=\"tab-pane fade\" id=\"profile\"&gt;\r\n    &lt;p&gt;&lt;strong&gt;Profile&lt;/strong&gt; ipsum dolor sit amet, consectetur adipisicing elit. Ut odio facere minima porro quis. Maiores eius quibusdam et in corrupti necessitatibus consequatur debitis laudantium hic.&lt;/p&gt;\r\n  &lt;/div&gt;\r\n  &lt;div role=\"tabpanel\" class=\"tab-pane fade\" id=\"messages\"&gt;\r\n    &lt;p&gt;&lt;strong&gt;Messages&lt;/strong&gt; ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis, optio error consectetur ullam porro eligendi mollitia odio numquam aut cumque. Sed, possimus recusandae itaque laboriosam nesciunt voluptates veniam aspernatur voluptate eaque ratione totam ipsa optio aliquam incidunt dolorum amet illum.&lt;/p&gt;\r\n  &lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;/div&gt;\r\n</pre>\r\n\r\n<hr>\r\n<br>\r\n<h2 id=\"accordion\">Accordion</h2>\r\n<div class=\"panel-group\" id=\"accordion-example\" role=\"tablist\" aria-multiselectable=\"true\">\r\n<div class=\"panel panel-default\">\r\n<div class=\"panel-heading\" role=\"tab\" id=\"headingOne\">\r\n<h4 class=\"panel-title\"><a data-toggle=\"collapse\" data-parent=\"#accordion-example\" href=\"#collapseOne\" aria-expanded=\"true\" aria-controls=\"collapseOne\"><i class=\"fa fa-home\"><span class=\"sr-only\">home</span></i> Home</a></h4>\r\n</div>\r\n<div id=\"collapseOne\" class=\"panel-collapse collapse in\" role=\"tabpanel\" aria-labelledby=\"headingOne\">\r\n<div class=\"panel-body\">\r\nAnim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven\'t heard of them accusamus labore sustainable VHS.\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"panel panel-default\">\r\n<div class=\"panel-heading\" role=\"tab\" id=\"headingTwo\">\r\n<h4 class=\"panel-title\"><a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion-example\" href=\"#collapseTwo\" aria-expanded=\"false\" aria-controls=\"collapseTwo\"><i class=\"fa fa-cog\"><span class=\"sr-only\">configuration</span></i> Configure</a></h4>\r\n</div>\r\n<div id=\"collapseTwo\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingTwo\">\r\n<div class=\"panel-body\">\r\nAnim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven\'t heard of them accusamus labore sustainable VHS.\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"panel panel-default\">\r\n<div class=\"panel-heading\" role=\"tab\" id=\"headingThree\">\r\n<h4 class=\"panel-title\"><a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion-example\" href=\"#collapseThree\" aria-expanded=\"false\" aria-controls=\"collapseThree\"><i class=\"fa fa-cloud-download\"><span class=\"sr-only\">cloud download</span></i> Download</a></h4>\r\n</div>\r\n<div id=\"collapseThree\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingThree\">\r\n<div class=\"panel-body\">\r\nAnim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven\'t heard of them accusamus labore sustainable VHS.\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<pre>\r\n&lt;div class=\"panel-group\" id=\"accordion-example\" role=\"tablist\" aria-multiselectable=\"true\"&gt;\r\n  &lt;div class=\"panel panel-default\"&gt;\r\n    &lt;div class=\"panel-heading\" role=\"tab\" id=\"headingOne\"&gt;\r\n      &lt;h4 class=\"panel-title\"&gt;\r\n        &lt;a data-toggle=\"collapse\" data-parent=\"#accordion-example\" href=\"#collapseOne\" aria-expanded=\"true\" aria-controls=\"collapseOne\"&gt;\r\n          &lt;i class=\"fa fa-home\"&gt;&lt;span class=\"sr-only\"&gt;home&lt;/span&gt;&lt;/i&gt; Home\r\n        &lt;/a&gt;\r\n      &lt;/h4&gt;\r\n    &lt;/div&gt;\r\n    &lt;div id=\"collapseOne\" class=\"panel-collapse collapse in\" role=\"tabpanel\" aria-labelledby=\"headingOne\"&gt;\r\n      &lt;div class=\"panel-body\"&gt;...&lt;/div&gt;\r\n    &lt;/div&gt;\r\n  &lt;/div&gt;\r\n  &lt;div class=\"panel panel-default\"&gt;\r\n    &lt;div class=\"panel-heading\" role=\"tab\" id=\"headingTwo\"&gt;\r\n      &lt;h4 class=\"panel-title\"&gt;\r\n        &lt;a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion-example\" href=\"#collapseTwo\" aria-expanded=\"false\" aria-controls=\"collapseTwo\"&gt;\r\n          &lt;i class=\"fa fa-cog\"&gt;&lt;span class=\"sr-only\"&gt;configuration&lt;/span&gt;&lt;/i&gt; Configure\r\n        &lt;/a&gt;\r\n      &lt;/h4&gt;\r\n    &lt;/div&gt;\r\n    &lt;div id=\"collapseTwo\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingTwo\"&gt;\r\n      &lt;div class=\"panel-body\"&gt;...&lt;/div&gt;\r\n    &lt;/div&gt;\r\n  &lt;/div&gt;\r\n  &lt;div class=\"panel panel-default\"&gt;\r\n    &lt;div class=\"panel-heading\" role=\"tab\" id=\"headingThree\"&gt;\r\n      &lt;h4 class=\"panel-title\"&gt;\r\n        &lt;a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion-example\" href=\"#collapseThree\" aria-expanded=\"false\" aria-controls=\"collapseThree\"&gt;\r\n          &lt;i class=\"fa fa-cloud-download\"&gt;&lt;span class=\"sr-only\"&gt;cloud download&lt;/span&gt;&lt;/i&gt; Download\r\n        &lt;/a&gt;\r\n        &lt;/h4&gt;\r\n    &lt;/div&gt;\r\n    &lt;div id=\"collapseThree\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingThree\"&gt;\r\n      &lt;div class=\"panel-body\"&gt;...&lt;/div&gt;\r\n    &lt;/div&gt;\r\n  &lt;/div&gt;\r\n&lt;/div&gt;\r\n</pre>\r\n<hr>\r\n<br>\r\n<h2 id=\"buttons\">Buttons</h2>\r\n<div>\r\n<a href=\"#\" class=\"more\">Read more</a>\r\n</div>\r\n<pre>\r\n&lt;a href=\"#\" class=\"more\"&gt;Read more&lt;/a&gt;\r\n</pre>\r\n<hr>\r\n<br>\r\n<h2>Buttons with click feedback effect</h2>\r\n<div>\r\n<button class=\"more cbutton-effect\">Click me</button>\r\n</div>\r\n<pre>\r\n&lt;button href=\"#\" class=\"more cbutton-effect\"&gt;Click me&lt;/button&gt;\r\n</pre>\r\n\r\n<hr>\r\n<br>\r\n<h2 id=\"progressbars\">Progress Bars</h2>\r\n<h5>78% Complete (default)</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 78%\">\r\n<span class=\"sr-only\">78% Complete (success)</span>\r\n</div>\r\n</div>\r\n<h5>40% Complete (success)</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-success\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 40%\">\r\n<span class=\"sr-only\">40% Complete (success)</span>\r\n</div>\r\n</div>\r\n<h5>20% Complete (info)</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-info\" role=\"progressbar\" aria-valuenow=\"20\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 20%\">\r\n<span class=\"sr-only\">20% Complete</span>\r\n</div>\r\n</div>\r\n<h5>60% Complete (warning)</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-warning\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 60%\">\r\n<span class=\"sr-only\">60% Complete (warning)</span>\r\n</div>\r\n</div>\r\n<h5>80% Complete (danger)</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-danger\" role=\"progressbar\" aria-valuenow=\"80\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 80%\">\r\n<span class=\"sr-only\">80% Complete</span>\r\n</div>\r\n</div>\r\n<h5>Results</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-success\" style=\"width: 40%\">\r\n<span class=\"sr-only\">35% A</span>\r\n</div>\r\n<div class=\"progress-bar progress-bar-info\" style=\"width: 30%\">\r\n<span class=\"sr-only\">20% B</span>\r\n</div>\r\n<div class=\"progress-bar progress-bar-warning\" style=\"width: 20%\">\r\n<span class=\"sr-only\">20% C</span>\r\n</div>\r\n<div class=\"progress-bar progress-bar-danger\" style=\"width: 10%\">\r\n<span class=\"sr-only\">10% D</span>\r\n</div>\r\n</div>\r\n\r\n<pre>\r\n&lt;h5>78% Complete (default)&lt;/h5&gt;\r\n&lt;div class=\"progress\"&gt;\r\n  &lt;div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 78%\"&gt;\r\n    &lt;span class=\"sr-only\"&gt;78% Complete (success)&lt;/span&gt;\r\n  &lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;h5>40% Complete (success)&lt;/h5&gt;\r\n&lt;div class=\"progress\"&gt;\r\n  &lt;div class=\"progress-bar progress-bar-success\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 40%\"&gt;\r\n    &lt;span class=\"sr-only\"&gt;40% Complete (success)&lt;/span&gt;\r\n  &lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;h5&gt;20% Complete (info)&lt;/h5&gt;\r\n&lt;div class=\"progress\"&gt;\r\n  &lt;div class=\"progress-bar progress-bar-info\" role=\"progressbar\" aria-valuenow=\"20\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 20%\"&gt;\r\n    &lt;span class=\"sr-only\"&gt;20% Complete&lt;/span&gt;\r\n  &lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;h5&gt;60% Complete (warning)&lt;/h5&gt;\r\n&lt;div class=\"progress\"&gt;\r\n  &lt;div class=\"progress-bar progress-bar-warning\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 60%\"&gt;\r\n    &lt;span class=\"sr-only\"&gt;60% Complete (warning)&lt;/span&gt;\r\n  &lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;h5&gt;80% Complete (danger)&lt;/h5&gt;\r\n&lt;div class=\"progress\"&gt;\r\n  &lt;div class=\"progress-bar progress-bar-danger\" role=\"progressbar\" aria-valuenow=\"80\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 80%\"&gt;\r\n    &lt;span class=\"sr-only\"&gt;80% Complete&lt;/span&gt;\r\n  &lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;h5&gt;Results&lt;/h5&gt;\r\n&lt;div class=\"progress\"&gt;\r\n  &lt;div class=\"progress-bar progress-bar-success\" style=\"width: 40%\"&gt;\r\n    &lt;span class=\"sr-only\"&gt;35% A&lt;/span&gt;\r\n  &lt;/div&gt;\r\n  &lt;div class=\"progress-bar progress-bar-info\" style=\"width: 30%\"&gt;\r\n    &lt;span class=\"sr-only\"&gt;20% B&lt;/span&gt;\r\n  &lt;/div&gt;\r\n  &lt;div class=\"progress-bar progress-bar-warning\" style=\"width: 20%\"&gt;\r\n    &lt;span class=\"sr-only\"&gt;20% C&lt;/span&gt;\r\n  &lt;/div&gt;\r\n  &lt;div class=\"progress-bar progress-bar-danger\" style=\"width: 10%\"&gt;\r\n    &lt;span class=\"sr-only\"&gt;10% D&lt;/span&gt;\r\n  &lt;/div&gt;\r\n&lt;/div&gt;\r\n</pre>\r\n<hr>\r\n<div class=\"alert alert-info\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>Check all available Font Awesome icons at <a  target=\"_blank\" href=\"http://fortawesome.github.io/Font-Awesome/icons/\" class=\"alert-link\">http://fortawesome.github.io/Font-Awesome/icons/</a></div>','','full_html'),('node','mt_benefit',0,29,29,'und',0,'<p>Ipsum, sed, odio, fugit, dolores atque sint odit doloremque expedita architecto commodi quas eum voluptas autem reprehenderit molestiae sunt cumque pariatur iure quod veritatis molestias quis facere eaque aliquid porro officia debitis accusamus eligendi itaque doloribus!</p>  \r\n<blockquote>Quasi, quaerat atque natus laudantium consectetur ducimus repellat deleniti amet sunt tenetur modi sit aperiam cumque dignissimos possimus? Ipsum, sed, odio, fugit, dolores atque sint odit doloremque expedita architecto commodi quas</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>\r\n\r\n<h3 class=\"description-title\">Lorem ipsum dolor sit amed consetatur</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<th>Service</th>\r\n<th>Quantity</th>\r\n<th>Price</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Lorem ipsum dolor sit amet</td>\r\n<td>24 pieces in stock</td>\r\n<td><strike>$279.00</strike> $189.00</td>			\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Praesent luctus fermentum tincidunt</td>\r\n<td>132 pieces in stock</td>\r\n<td><strike>$453.00</strike> $379.00</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Morbi venenatis, ligula ut tincidunt</td>\r\n<td>1231 pieces in stock</td>\r\n<td><strike>$1193.00</strike> $1089.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Duis vitae odio ac ante faucibus faucibus</td>\r\n<td>978 pieces in stock</td>\r\n<td><strike>$687.00</strike> $620.00</td>\r\n</tr>		\r\n</tbody>\r\n</table>\r\n<a href=\"#\" class=\"more\">Get free trial</a>\r\n','','full_html'),('node','mt_benefit',0,30,30,'und',0,'<p>Ipsum, sed, odio, fugit, dolores atque sint odit doloremque expedita architecto commodi quas eum voluptas autem reprehenderit molestiae sunt cumque pariatur iure quod veritatis molestias quis facere eaque aliquid porro officia debitis accusamus eligendi itaque doloribus!</p>  \r\n<blockquote>Quasi, quaerat atque natus laudantium consectetur ducimus repellat deleniti amet sunt tenetur modi sit aperiam cumque dignissimos possimus? Ipsum, sed, odio, fugit, dolores atque sint odit doloremque expedita architecto commodi quas</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>\r\n\r\n<h3 class=\"description-title\">Lorem ipsum dolor sit amed consetatur</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<th>Service</th>\r\n<th>Quantity</th>\r\n<th>Price</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Lorem ipsum dolor sit amet</td>\r\n<td>24 pieces in stock</td>\r\n<td><strike>$279.00</strike> $189.00</td>			\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Praesent luctus fermentum tincidunt</td>\r\n<td>132 pieces in stock</td>\r\n<td><strike>$453.00</strike> $379.00</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Morbi venenatis, ligula ut tincidunt</td>\r\n<td>1231 pieces in stock</td>\r\n<td><strike>$1193.00</strike> $1089.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Duis vitae odio ac ante faucibus faucibus</td>\r\n<td>978 pieces in stock</td>\r\n<td><strike>$687.00</strike> $620.00</td>\r\n</tr>		\r\n</tbody>\r\n</table>','','full_html'),('node','mt_benefit',0,31,31,'und',0,'<p>Ipsum, sed, odio, fugit, dolores atque sint odit doloremque expedita architecto commodi quas eum voluptas autem reprehenderit molestiae sunt cumque pariatur iure quod veritatis molestias quis facere eaque aliquid porro officia debitis accusamus eligendi itaque doloribus!</p>  \r\n<blockquote>Quasi, quaerat atque natus laudantium consectetur ducimus repellat deleniti amet sunt tenetur modi sit aperiam cumque dignissimos possimus? Ipsum, sed, odio, fugit, dolores atque sint odit doloremque expedita architecto commodi quas</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>\r\n\r\n<h3 class=\"description-title\">Lorem ipsum dolor sit amed consetatur</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<th>Service</th>\r\n<th>Quantity</th>\r\n<th>Price</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Lorem ipsum dolor sit amet</td>\r\n<td>24 pieces in stock</td>\r\n<td><strike>$279.00</strike> $189.00</td>			\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Praesent luctus fermentum tincidunt</td>\r\n<td>132 pieces in stock</td>\r\n<td><strike>$453.00</strike> $379.00</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Morbi venenatis, ligula ut tincidunt</td>\r\n<td>1231 pieces in stock</td>\r\n<td><strike>$1193.00</strike> $1089.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Duis vitae odio ac ante faucibus faucibus</td>\r\n<td>978 pieces in stock</td>\r\n<td><strike>$687.00</strike> $620.00</td>\r\n</tr>		\r\n</tbody>\r\n</table>','','full_html'),('node','webform',0,32,32,'und',0,'We respect your privacy 100%, so the information that you provide will remain strictly confidential. Nevertheless, a copy of your message might be stored in our records as a database entry for archival purposes.','','filtered_html'),('node','page',0,33,33,'und',0,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas interdum mi et facilisis rutrum. Vivamus lobortis aliquam est vel facilisis. Duis eu diam eu mi eleifend ornare ut hendrerit tortor. Quisque nisi nisl, adipiscing sed laoreet ac, vestibulum ac ligula. Fusce elementum, turpis nec facilisis facilisis, ante orci bibendum augue, vel ultricies risus leo eu lacus.</p>\r\n','','filtered_html'),('node','mt_product',0,34,34,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>','','full_html'),('node','mt_team_member',0,36,36,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n<h3>Skills</h3>\r\n\r\n<h5>90% Drupal</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 90%\">\r\n<span class=\"sr-only\">90% Drupal</span>\r\n</div>\r\n</div>\r\n\r\n<h5>70% jQuery</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 70%\">\r\n<span class=\"sr-only\">70% Drupal</span>\r\n</div>\r\n</div>\r\n\r\n<h5>80% CSS</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 80%\">\r\n<span class=\"sr-only\">80% Drupal</span>\r\n</div>\r\n</div>\r\n\r\n<h5>85% HTML</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 85%\">\r\n<span class=\"sr-only\">85% Drupal</span>\r\n</div>\r\n</div>','','full_html'),('node','mt_team_member',0,37,37,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n<h3>Skills</h3>\r\n\r\n<h5>90% Drupal</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 90%\">\r\n<span class=\"sr-only\">90% Drupal</span>\r\n</div>\r\n</div>\r\n\r\n<h5>70% jQuery</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 70%\">\r\n<span class=\"sr-only\">70% Drupal</span>\r\n</div>\r\n</div>\r\n\r\n<h5>80% CSS</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 80%\">\r\n<span class=\"sr-only\">80% Drupal</span>\r\n</div>\r\n</div>\r\n\r\n<h5>85% HTML</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 85%\">\r\n<span class=\"sr-only\">85% Drupal</span>\r\n</div>\r\n</div>','','full_html'),('node','mt_team_member',0,38,38,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n<h3>Skills</h3>\r\n\r\n<h5>90% Drupal</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 90%\">\r\n<span class=\"sr-only\">90% Drupal</span>\r\n</div>\r\n</div>\r\n\r\n<h5>70% jQuery</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 70%\">\r\n<span class=\"sr-only\">70% Drupal</span>\r\n</div>\r\n</div>\r\n\r\n<h5>80% CSS</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 80%\">\r\n<span class=\"sr-only\">80% Drupal</span>\r\n</div>\r\n</div>\r\n\r\n<h5>85% HTML</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 85%\">\r\n<span class=\"sr-only\">85% Drupal</span>\r\n</div>\r\n</div>','','full_html'),('node','mt_team_member',0,39,39,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n<h3>Skills</h3>\r\n\r\n<h5>90% Drupal</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 90%\">\r\n<span class=\"sr-only\">90% Drupal</span>\r\n</div>\r\n</div>\r\n\r\n<h5>70% jQuery</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 70%\">\r\n<span class=\"sr-only\">70% Drupal</span>\r\n</div>\r\n</div>\r\n\r\n<h5>80% CSS</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 80%\">\r\n<span class=\"sr-only\">80% Drupal</span>\r\n</div>\r\n</div>\r\n\r\n<h5>85% HTML</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 85%\">\r\n<span class=\"sr-only\">85% Drupal</span>\r\n</div>\r\n</div>','','full_html'),('node','mt_team_member',0,40,40,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n<h3>Skills</h3>\r\n\r\n<h5>90% Drupal</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 90%\">\r\n<span class=\"sr-only\">90% Drupal</span>\r\n</div>\r\n</div>\r\n\r\n<h5>70% jQuery</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 70%\">\r\n<span class=\"sr-only\">70% Drupal</span>\r\n</div>\r\n</div>\r\n\r\n<h5>80% CSS</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 80%\">\r\n<span class=\"sr-only\">80% Drupal</span>\r\n</div>\r\n</div>\r\n\r\n<h5>85% HTML</h5>\r\n<div class=\"progress\">\r\n<div class=\"progress-bar progress-bar-default\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 85%\">\r\n<span class=\"sr-only\">85% Drupal</span>\r\n</div>\r\n</div>','','full_html'),('node','page',0,41,41,'und',0,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas interdum mi et facilisis rutrum. Vivamus lobortis aliquam est vel facilisis. Duis eu diam eu mi eleifend ornare ut hendrerit tortor. Quisque nisi nisl, adipiscing sed laoreet ac, vestibulum ac ligula. Fusce elementum, turpis nec facilisis facilisis, ante orci bibendum augue, vel ultricies risus leo eu lacus.</p>','','filtered_html'),('node','mt_product',0,42,42,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>','','full_html'),('node','mt_product',0,43,43,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>\r\n','','full_html'),('node','mt_product',0,44,44,'und',0,'','Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies appropriately communicate.','filtered_html'),('node','mt_service',0,45,45,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>','','full_html'),('node','mt_service',0,46,46,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>','','full_html'),('node','mt_product',0,47,47,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>','','full_html'),('node','mt_product',0,48,48,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>\r\n','','full_html'),('node','mt_testimonial',0,49,49,'und',0,'Using their services we were never let down! Et dolore magna aliqua.','','filtered_html'),('node','mt_testimonial',0,50,50,'und',0,'Using their services we have increased conversions by 35% in a month!','','filtered_html'),('node','mt_testimonial',0,51,51,'und',0,'Tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.','','filtered_html'),('node','blog',0,52,52,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>\r\n\r\n<h3 class=\"description-title\">Lorem ipsum dolor sit amed consetatur</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>','','full_html'),('node','blog',0,53,53,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>\r\n\r\n<h3 class=\"description-title\">Lorem ipsum dolor sit amed consetatur</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>','','full_html'),('node','blog',0,54,54,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities.</blockquote>\r\n<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise <a href=\"#\">with competitive technologies</a>. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. Quickly enable prospective technology rather than open-source technologies.</p>\r\n\r\n<h3 class=\"description-title\">Lorem ipsum dolor sit amed consetatur</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>','','full_html'),('node','mt_showcase',0,56,56,'und',0,'<p>Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies. Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas <a href=\"#\">state of the art interfaces</a>. Quickly enable prospective technology rather than open-source technologies.</p>  \r\n<blockquote>Completely parallel task market positioning interfaces through visionary niche markets, proactively incentivize sticky quality vectors before future-proof internal or \"organic\" sources.</blockquote>\r\n<p>Seamlessly procrastinate proactive outsourcing whereas wireless products. Progressively leverage existing 24/365 experiences through timely initiatives. Quickly facilitate frictionless ROI rather than equity invested solutions.</p>\r\n\r\n<h3 class=\"description-title\">Showcase specifics</h3>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>','','filtered_html'),('node','page',0,57,57,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','','filtered_html'),('node','page',0,58,58,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','','filtered_html');
/*!40000 ALTER TABLE `field_revision_body` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_comment_body`
--

DROP TABLE IF EXISTS `field_revision_comment_body`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_comment_body` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `comment_body_value` longtext,
  `comment_body_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `comment_body_format` (`comment_body_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 1 (comment_body)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_comment_body`
--

LOCK TABLES `field_revision_comment_body` WRITE;
/*!40000 ALTER TABLE `field_revision_comment_body` DISABLE KEYS */;
INSERT INTO `field_revision_comment_body` VALUES ('comment','comment_node_blog',0,1,1,'und',0,'Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.','filtered_html'),('comment','comment_node_blog',0,2,2,'und',0,'Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.','filtered_html'),('comment','comment_node_blog',0,3,3,'und',0,'Appropriately communicate adaptive imperatives rather than value-added potentialities. Conveniently harness frictionless outsourcing whereas state of the art interfaces. ','full_html'),('comment','comment_node_blog',0,5,5,'und',0,'Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Nullam id dolor id nibh ultricies vehicula ut id elit. ','filtered_html'),('comment','comment_node_mt_showcase',0,6,6,'und',0,'Fusce interdum hendrerit mauris porttitor scelerisque. Praesent facilisis augue tortor, a dapibus neque ultricies a. Phasellus mollis, purus eu consequat convallis, arcu urna pharetra mauris, non pulvinar lorem mauris quis purus. Donec ullamcorper mauris lorem, id molestie mauris fringilla non.','filtered_html'),('comment','comment_node_mt_showcase',0,7,7,'und',0,'Vestibulum in nisl leo. Nam interdum ac libero id pulvinar. Nam a leo vulputate nisl convallis viverra. Donec felis lorem, rhoncus nec placerat at, ullamcorper eget nisl. Suspendisse gravida volutpat lacus, sed iaculis nunc hendrerit id. Aenean id diam ornare, accumsan ligula in, vestibulum urna. Pellentesque id augue in ante semper cursus id vel neque. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.','filtered_html'),('comment','comment_node_mt_showcase',0,8,8,'und',0,'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam porta faucibus leo, quis rhoncus sem vehicula sed. Duis sit amet cursus mauris, et interdum sem. Curabitur eu nisl luctus, convallis arcu non, laoreet velit. Suspendisse accumsan magna vitae erat cursus tempus. ','filtered_html'),('comment','comment_node_mt_showcase',0,9,9,'und',0,'Nam porta faucibus leo, quis rhoncus sem vehicula sed. Duis sit amet cursus mauris, et interdum sem. Curabitur eu nisl luctus, convallis arcu non, laoreet velit. Suspendisse accumsan magna vitae erat cursus tempus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. ','filtered_html'),('comment','comment_node_mt_showcase',0,10,10,'und',0,'Proin ut viverra orci. Fusce interdum hendrerit mauris porttitor scelerisque. Praesent facilisis augue tortor, a dapibus neque ultricies a. Phasellus mollis, purus eu consequat convallis, arcu urna pharetra mauris, non pulvinar lorem mauris quis purus. Donec ullamcorper mauris lorem, id molestie mauris fringilla non.','filtered_html');
/*!40000 ALTER TABLE `field_revision_comment_body` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_image`
--

DROP TABLE IF EXISTS `field_revision_field_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_image` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_image_fid` int(10) unsigned DEFAULT NULL COMMENT 'The file_managed.fid being referenced in this field.',
  `field_image_alt` varchar(512) DEFAULT NULL COMMENT 'Alternative image text, for the image’s ’alt’ attribute.',
  `field_image_title` varchar(1024) DEFAULT NULL COMMENT 'Image title text, for the image’s ’title’ attribute.',
  `field_image_width` int(10) unsigned DEFAULT NULL COMMENT 'The width of the image in pixels.',
  `field_image_height` int(10) unsigned DEFAULT NULL COMMENT 'The height of the image in pixels.',
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_image_fid` (`field_image_fid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 4 (field_image)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_image`
--

LOCK TABLES `field_revision_field_image` WRITE;
/*!40000 ALTER TABLE `field_revision_field_image` DISABLE KEYS */;
INSERT INTO `field_revision_field_image` VALUES ('node','blog',0,13,13,'und',0,277,'','The title of the caption',750,500),('node','blog',0,14,14,'und',0,275,'','Caption of the image Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.',750,500),('node','blog',0,14,14,'und',1,287,'','Title of Second Slide',750,500),('node','blog',0,15,15,'und',0,276,'lorem ipsum dolor sit amet','The title of the caption',750,500),('node','mt_showcase',0,16,16,'und',0,259,'','',750,500),('node','mt_showcase',0,17,17,'und',0,260,'','',750,500),('node','mt_showcase',0,18,18,'und',0,261,'','',750,500),('node','mt_showcase',0,19,19,'und',0,256,'lorem ipsum dolor sit amet','And this is the caption of the image',750,500),('node','mt_showcase',0,20,20,'und',0,253,'lorem ipsum dolor sit amet','The title of the caption',750,500),('node','mt_showcase',0,20,20,'und',1,254,'Imperatives rather than value-added potentialities.','Title of Second Slide',750,500),('node','mt_showcase',0,20,20,'und',2,255,'Communicate adaptive imperatives rather than value-added potentialities.','Title of Image',750,500),('node','mt_showcase',0,21,21,'und',0,257,'lorem ipsum dolor sit amet','The title of the caption',750,500),('node','blog',0,52,52,'und',0,299,'','And this is the caption of the image which clarifies that quickly extend top-line opportunities for leveraged bandwidth.',750,500),('node','blog',0,53,53,'und',0,281,'lorem ipsum dolor sit amet','And this is the caption of the image which clarifies that quickly extend top-line opportunities for leveraged bandwidth.',750,500),('node','blog',0,54,54,'und',0,282,'lorem ipsum dolor sit amet','The title of the caption',750,500),('node','mt_showcase',0,56,56,'und',0,296,'','',750,500);
/*!40000 ALTER TABLE `field_revision_field_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_banner_image`
--

DROP TABLE IF EXISTS `field_revision_field_mt_banner_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_banner_image` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_banner_image_fid` int(10) unsigned DEFAULT NULL COMMENT 'The file_managed.fid being referenced in this field.',
  `field_mt_banner_image_alt` varchar(512) DEFAULT NULL COMMENT 'Alternative image text, for the image’s ’alt’ attribute.',
  `field_mt_banner_image_title` varchar(1024) DEFAULT NULL COMMENT 'Image title text, for the image’s ’title’ attribute.',
  `field_mt_banner_image_width` int(10) unsigned DEFAULT NULL COMMENT 'The width of the image in pixels.',
  `field_mt_banner_image_height` int(10) unsigned DEFAULT NULL COMMENT 'The height of the image in pixels.',
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_banner_image_fid` (`field_mt_banner_image_fid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 35 (field_mt_banner...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_banner_image`
--

LOCK TABLES `field_revision_field_mt_banner_image` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_banner_image` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_banner_image` VALUES ('node','mt_service',0,10,10,'und',0,210,'','Synergistically streamline prospective content whereas turnkey web services.',1920,630),('node','mt_service',0,10,10,'und',1,211,'','Efficiently formulate enabled processes with granular processes.',1920,630),('node','page',0,55,55,'und',0,283,'','Be it for a question, an idea or even a concern, don’t hesitate to contact us.',1920,630),('node','page',0,55,55,'und',1,284,'','Be it for a question, an idea or even a concern, don’t hesitate to contact us.',1920,630);
/*!40000 ALTER TABLE `field_revision_field_mt_banner_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_company_description`
--

DROP TABLE IF EXISTS `field_revision_field_mt_company_description`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_company_description` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_company_description_value` longtext,
  `field_mt_company_description_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_company_description_format` (`field_mt_company_description_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 73 (field_mt_company...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_company_description`
--

LOCK TABLES `field_revision_field_mt_company_description` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_company_description` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_company_description` VALUES ('node','mt_showcase',0,16,16,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.','filtered_html'),('node','mt_showcase',0,17,17,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.','filtered_html'),('node','mt_showcase',0,18,18,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.','filtered_html'),('node','mt_showcase',0,19,19,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodtempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodoconsequat.','filtered_html'),('node','mt_showcase',0,20,20,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.','filtered_html'),('node','mt_showcase',0,21,21,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.','filtered_html'),('node','mt_showcase',0,56,56,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.','filtered_html');
/*!40000 ALTER TABLE `field_revision_field_mt_company_description` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_company_logo`
--

DROP TABLE IF EXISTS `field_revision_field_mt_company_logo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_company_logo` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_company_logo_fid` int(10) unsigned DEFAULT NULL COMMENT 'The file_managed.fid being referenced in this field.',
  `field_mt_company_logo_alt` varchar(512) DEFAULT NULL COMMENT 'Alternative image text, for the image’s ’alt’ attribute.',
  `field_mt_company_logo_title` varchar(1024) DEFAULT NULL COMMENT 'Image title text, for the image’s ’title’ attribute.',
  `field_mt_company_logo_width` int(10) unsigned DEFAULT NULL COMMENT 'The width of the image in pixels.',
  `field_mt_company_logo_height` int(10) unsigned DEFAULT NULL COMMENT 'The height of the image in pixels.',
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_company_logo_fid` (`field_mt_company_logo_fid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 72 (field_mt_company...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_company_logo`
--

LOCK TABLES `field_revision_field_mt_company_logo` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_company_logo` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_company_logo` VALUES ('node','mt_showcase',0,16,16,'und',0,304,'','',158,33),('node','mt_showcase',0,17,17,'und',0,305,'','',140,54),('node','mt_showcase',0,18,18,'und',0,306,'','',141,49),('node','mt_showcase',0,19,19,'und',0,308,'','',140,44),('node','mt_showcase',0,20,20,'und',0,301,'','',141,68),('node','mt_showcase',0,21,21,'und',0,303,'','',141,49),('node','mt_showcase',0,56,56,'und',0,307,'','',141,49);
/*!40000 ALTER TABLE `field_revision_field_mt_company_logo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_company_title`
--

DROP TABLE IF EXISTS `field_revision_field_mt_company_title`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_company_title` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_company_title_value` varchar(255) DEFAULT NULL,
  `field_mt_company_title_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_company_title_format` (`field_mt_company_title_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 71 (field_mt_company...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_company_title`
--

LOCK TABLES `field_revision_field_mt_company_title` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_company_title` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_company_title` VALUES ('node','mt_showcase',0,16,16,'und',0,'Company D','filtered_html'),('node','mt_showcase',0,17,17,'und',0,'Company E','filtered_html'),('node','mt_showcase',0,18,18,'und',0,'Company B','filtered_html'),('node','mt_showcase',0,19,19,'und',0,'Company B','filtered_html'),('node','mt_showcase',0,20,20,'und',0,'Company A','filtered_html'),('node','mt_showcase',0,21,21,'und',0,'Company C','filtered_html'),('node','mt_showcase',0,56,56,'und',0,'Company G','filtered_html');
/*!40000 ALTER TABLE `field_revision_field_mt_company_title` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_content_bottom`
--

DROP TABLE IF EXISTS `field_revision_field_mt_content_bottom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_content_bottom` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_content_bottom_value` longtext,
  `field_mt_content_bottom_summary` longtext,
  `field_mt_content_bottom_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_content_bottom_format` (`field_mt_content_bottom_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 70 (field_mt_content...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_content_bottom`
--

LOCK TABLES `field_revision_field_mt_content_bottom` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_content_bottom` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_content_bottom` VALUES ('node','mt_service',0,10,10,'und',0,'<h5>Service description</h5>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<th>Service</th>\r\n<th>Quantity</th>\r\n<th>Price</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Lorem ipsum dolor sit amet</td>\r\n<td>24 pieces in stock</td>\r\n<td>$189.00</td>			\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Praesent luctus fermentum tincidunt</td>\r\n<td>132 pieces in stock</td>\r\n<td>$379.00</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Morbi venenatis, ligula ut tincidunt</td>\r\n<td>1231 pieces in stock</td>\r\n<td>$1089.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Duis vitae odio ac ante faucibus faucibus</td>\r\n<td>978 pieces in stock</td>\r\n<td>$620.00</td>\r\n</tr>		\r\n</tbody>\r\n</table>\r\n<a href=\"#\" class=\"more\">Get free trial</a>\r\n','','full_html'),('node','mt_service',0,11,11,'und',0,'<h5 class=\"description-title\">Service Description</h5>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<th>Service</th>\r\n<th>Quantity</th>\r\n<th>Price</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Lorem ipsum dolor sit amet</td>\r\n<td>24 pieces in stock</td>\r\n<td>$189.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Praesent luctus fermentum tincidunt</td>\r\n<td>132 pieces in stock</td>\r\n<td>$379.00</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Morbi venenatis, ligula ut tincidunt</td>\r\n<td>1231 pieces in stock</td>\r\n<td>$1089.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Duis vitae odio ac ante faucibus faucibus</td>\r\n<td>978 pieces in stock</td>\r\n<td>$620.00</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<a href=\"#\" class=\"more\">Get free trial</a>','','full_html'),('node','mt_service',0,12,12,'und',0,'<h5 class=\"description-title\">Service Description</h5>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<th>Service</th>\r\n<th>Quantity</th>\r\n<th>Price</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Lorem ipsum dolor sit amet</td>\r\n<td>24 pieces in stock</td>\r\n<td>$189.00</td>			\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Praesent luctus fermentum tincidunt</td>\r\n<td>132 pieces in stock</td>\r\n<td>$379.00</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Morbi venenatis, ligula ut tincidunt</td>\r\n<td>1231 pieces in stock</td>\r\n<td>$1089.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Duis vitae odio ac ante faucibus faucibus</td>\r\n<td>978 pieces in stock</td>\r\n<td>$620.00</td>\r\n</tr>		\r\n</tbody>\r\n</table>\r\n<a href=\"#\" class=\"more\">Get free trial</a>\r\n','','full_html'),('node','mt_service',0,25,25,'und',0,'<h5 class=\"description-title\">Service Description</h5>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<th>Service</th>\r\n<th>Quantity</th>\r\n<th>Price</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Lorem ipsum dolor sit amet</td>\r\n<td>24 pieces in stock</td>\r\n<td>$189.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Praesent luctus fermentum tincidunt</td>\r\n<td>132 pieces in stock</td>\r\n<td>$379.00</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Morbi venenatis, ligula ut tincidunt</td>\r\n<td>1231 pieces in stock</td>\r\n<td>$1089.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Duis vitae odio ac ante faucibus faucibus</td>\r\n<td>978 pieces in stock</td>\r\n<td>$620.00</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<a href=\"#\" class=\"more\">Get free trial</a>','','full_html'),('node','mt_product',0,34,34,'und',0,'<h5 class=\"description-title\">Service Description</h5>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<th>Service</th>\r\n<th>Quantity</th>\r\n<th>Price</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Lorem ipsum dolor sit amet</td>\r\n<td>24 pieces in stock</td>\r\n<td>$189.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Praesent luctus fermentum tincidunt</td>\r\n<td>132 pieces in stock</td>\r\n<td>$379.00</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Morbi venenatis, ligula ut tincidunt</td>\r\n<td>1231 pieces in stock</td>\r\n<td>$1089.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Duis vitae odio ac ante faucibus faucibus</td>\r\n<td>978 pieces in stock</td>\r\n<td>$620.00</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<a href=\"#\" class=\"more\">Get free trial</a>','','full_html'),('node','mt_product',0,42,42,'und',0,'<h5 class=\"description-title\">Service Description</h5>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<th>Service</th>\r\n<th>Quantity</th>\r\n<th>Price</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Lorem ipsum dolor sit amet</td>\r\n<td>24 pieces in stock</td>\r\n<td>$189.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Praesent luctus fermentum tincidunt</td>\r\n<td>132 pieces in stock</td>\r\n<td>$379.00</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Morbi venenatis, ligula ut tincidunt</td>\r\n<td>1231 pieces in stock</td>\r\n<td>$1089.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Duis vitae odio ac ante faucibus faucibus</td>\r\n<td>978 pieces in stock</td>\r\n<td>$620.00</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<a href=\"#\" class=\"more\">Get free trial</a>','','full_html'),('node','mt_product',0,43,43,'und',0,'<h5 class=\"description-title\">Service Description</h5>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<th>Service</th>\r\n<th>Quantity</th>\r\n<th>Price</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Lorem ipsum dolor sit amet</td>\r\n<td>24 pieces in stock</td>\r\n<td>$189.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Praesent luctus fermentum tincidunt</td>\r\n<td>132 pieces in stock</td>\r\n<td>$379.00</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Morbi venenatis, ligula ut tincidunt</td>\r\n<td>1231 pieces in stock</td>\r\n<td>$1089.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Duis vitae odio ac ante faucibus faucibus</td>\r\n<td>978 pieces in stock</td>\r\n<td>$620.00</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<a href=\"#\" class=\"more\">Get free trial</a>','','full_html'),('node','mt_service',0,45,45,'und',0,'<h5 class=\"description-title\">Service Description</h5>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<th>Service</th>\r\n<th>Quantity</th>\r\n<th>Price</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Lorem ipsum dolor sit amet</td>\r\n<td>24 pieces in stock</td>\r\n<td>$189.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Praesent luctus fermentum tincidunt</td>\r\n<td>132 pieces in stock</td>\r\n<td>$379.00</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Morbi venenatis, ligula ut tincidunt</td>\r\n<td>1231 pieces in stock</td>\r\n<td>$1089.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Duis vitae odio ac ante faucibus faucibus</td>\r\n<td>978 pieces in stock</td>\r\n<td>$620.00</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<a href=\"#\" class=\"more\">Get free trial</a>','','full_html'),('node','mt_service',0,46,46,'und',0,'<h5 class=\"description-title\">Service Description</h5>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<th>Service</th>\r\n<th>Quantity</th>\r\n<th>Price</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Lorem ipsum dolor sit amet</td>\r\n<td>24 pieces in stock</td>\r\n<td>$189.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Praesent luctus fermentum tincidunt</td>\r\n<td>132 pieces in stock</td>\r\n<td>$379.00</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Morbi venenatis, ligula ut tincidunt</td>\r\n<td>1231 pieces in stock</td>\r\n<td>$1089.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Duis vitae odio ac ante faucibus faucibus</td>\r\n<td>978 pieces in stock</td>\r\n<td>$620.00</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<a href=\"#\" class=\"more\">Get free trial</a>','','full_html'),('node','mt_product',0,47,47,'und',0,'<h5 class=\"description-title\">Service Description</h5>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<th>Service</th>\r\n<th>Quantity</th>\r\n<th>Price</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Lorem ipsum dolor sit amet</td>\r\n<td>24 pieces in stock</td>\r\n<td>$189.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Praesent luctus fermentum tincidunt</td>\r\n<td>132 pieces in stock</td>\r\n<td>$379.00</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Morbi venenatis, ligula ut tincidunt</td>\r\n<td>1231 pieces in stock</td>\r\n<td>$1089.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Duis vitae odio ac ante faucibus faucibus</td>\r\n<td>978 pieces in stock</td>\r\n<td>$620.00</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<a href=\"#\" class=\"more\">Get free trial</a>','','full_html'),('node','mt_product',0,48,48,'und',0,'<h5 class=\"description-title\">Service Description</h5>\r\n<p>Compellingly <a href=\"#\">recaptiualize enterprise-wide web-readiness</a> for robust strategic theme areas. Synergistically reconceptualize user-centric functionalities via revolutionary strategic theme areas. Progressively transition out-of-the-box leadership skills rather than top-line and <a href=\"#\">this is a hover link</a>.</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<th>Service</th>\r\n<th>Quantity</th>\r\n<th>Price</th>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Lorem ipsum dolor sit amet</td>\r\n<td>24 pieces in stock</td>\r\n<td>$189.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Praesent luctus fermentum tincidunt</td>\r\n<td>132 pieces in stock</td>\r\n<td>$379.00</td>\r\n</tr>\r\n<tr class=\"odd\">\r\n<td>Morbi venenatis, ligula ut tincidunt</td>\r\n<td>1231 pieces in stock</td>\r\n<td>$1089.00</td>\r\n</tr>\r\n<tr class=\"even\">\r\n<td>Duis vitae odio ac ante faucibus faucibus</td>\r\n<td>978 pieces in stock</td>\r\n<td>$620.00</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<a href=\"#\" class=\"more\">Get free trial</a>','','full_html');
/*!40000 ALTER TABLE `field_revision_field_mt_content_bottom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_facebook_account`
--

DROP TABLE IF EXISTS `field_revision_field_mt_facebook_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_facebook_account` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_facebook_account_value` varchar(255) DEFAULT NULL,
  `field_mt_facebook_account_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_facebook_account_format` (`field_mt_facebook_account_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 39 (field_mt_facebook...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_facebook_account`
--

LOCK TABLES `field_revision_field_mt_facebook_account` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_facebook_account` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_facebook_account` VALUES ('node','mt_team_member',0,36,36,'und',0,'https://www.facebook.com',NULL),('node','mt_team_member',0,37,37,'und',0,'https://www.facebook.com',NULL),('node','mt_team_member',0,38,38,'und',0,'https://www.facebook.com',NULL),('node','mt_team_member',0,39,39,'und',0,'https://www.facebook.com',NULL),('node','mt_team_member',0,40,40,'und',0,'https://www.facebook.com',NULL);
/*!40000 ALTER TABLE `field_revision_field_mt_facebook_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_feature_body`
--

DROP TABLE IF EXISTS `field_revision_field_mt_feature_body`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_feature_body` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_feature_body_value` longtext,
  `field_mt_feature_body_summary` longtext,
  `field_mt_feature_body_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_feature_body_format` (`field_mt_feature_body_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 51 (field_mt_feature...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_feature_body`
--

LOCK TABLES `field_revision_field_mt_feature_body` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_feature_body` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_feature_body` VALUES ('field_collection_item','field_mt_special_feature',0,32,32,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing exercitation. ','','filtered_html'),('field_collection_item','field_mt_special_feature',0,33,33,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing exercitation dolor sit amet. ','','filtered_html'),('field_collection_item','field_mt_special_feature',0,34,34,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing exercitation ipsum dolor sit amet. ','','filtered_html'),('field_collection_item','field_mt_special_feature',0,36,36,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing exercitation. ','','filtered_html'),('field_collection_item','field_mt_special_feature',0,37,37,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing exercitation dolor sit amet. ','','filtered_html'),('field_collection_item','field_mt_special_feature',0,38,38,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing exercitation ipsum dolor sit amet. ','','filtered_html'),('field_collection_item','field_mt_standard_feature',0,39,39,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing exercitation.','','filtered_html'),('field_collection_item','field_mt_standard_feature',0,40,40,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing exercitation.','','filtered_html'),('field_collection_item','field_mt_standard_feature',0,41,41,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing exercitation.','','filtered_html'),('field_collection_item','field_mt_standard_feature',0,42,42,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing exercitation.','','filtered_html'),('field_collection_item','field_mt_standard_feature',0,43,43,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing exercitation. ','','filtered_html'),('field_collection_item','field_mt_standard_feature',0,44,44,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing exercitation. ','','filtered_html'),('field_collection_item','field_mt_standard_feature',0,45,45,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing exercitation. ','','filtered_html'),('field_collection_item','field_mt_standard_feature',0,46,46,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing exercitation. ','','filtered_html');
/*!40000 ALTER TABLE `field_revision_field_mt_feature_body` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_feature_font_awesome`
--

DROP TABLE IF EXISTS `field_revision_field_mt_feature_font_awesome`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_feature_font_awesome` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_feature_font_awesome_value` varchar(255) DEFAULT NULL,
  `field_mt_feature_font_awesome_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_feature_font_awesome_format` (`field_mt_feature_font_awesome_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 53 (field_mt_feature...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_feature_font_awesome`
--

LOCK TABLES `field_revision_field_mt_feature_font_awesome` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_feature_font_awesome` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_feature_font_awesome` VALUES ('field_collection_item','field_mt_special_feature',0,32,32,'und',0,'fa-users',NULL),('field_collection_item','field_mt_special_feature',0,33,33,'und',0,'fa-globe',NULL),('field_collection_item','field_mt_special_feature',0,34,34,'und',0,'fa-diamond',NULL),('field_collection_item','field_mt_special_feature',0,36,36,'und',0,'fa-users',NULL),('field_collection_item','field_mt_special_feature',0,37,37,'und',0,'fa-globe',NULL),('field_collection_item','field_mt_special_feature',0,38,38,'und',0,'fa-diamond',NULL);
/*!40000 ALTER TABLE `field_revision_field_mt_feature_font_awesome` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_feature_image`
--

DROP TABLE IF EXISTS `field_revision_field_mt_feature_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_feature_image` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_feature_image_fid` int(10) unsigned DEFAULT NULL COMMENT 'The file_managed.fid being referenced in this field.',
  `field_mt_feature_image_alt` varchar(512) DEFAULT NULL COMMENT 'Alternative image text, for the image’s ’alt’ attribute.',
  `field_mt_feature_image_title` varchar(1024) DEFAULT NULL COMMENT 'Image title text, for the image’s ’title’ attribute.',
  `field_mt_feature_image_width` int(10) unsigned DEFAULT NULL COMMENT 'The width of the image in pixels.',
  `field_mt_feature_image_height` int(10) unsigned DEFAULT NULL COMMENT 'The height of the image in pixels.',
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_feature_image_fid` (`field_mt_feature_image_fid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 52 (field_mt_feature...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_feature_image`
--

LOCK TABLES `field_revision_field_mt_feature_image` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_feature_image` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_feature_image` VALUES ('field_collection_item','field_mt_standard_feature',0,39,39,'und',0,218,'','',750,500),('field_collection_item','field_mt_standard_feature',0,40,40,'und',0,219,'','',750,500),('field_collection_item','field_mt_standard_feature',0,41,41,'und',0,220,'','',750,500),('field_collection_item','field_mt_standard_feature',0,42,42,'und',0,221,'','',750,500),('field_collection_item','field_mt_standard_feature',0,43,43,'und',0,222,'','',750,500),('field_collection_item','field_mt_standard_feature',0,44,44,'und',0,223,'','',750,500),('field_collection_item','field_mt_standard_feature',0,45,45,'und',0,224,'','',750,500),('field_collection_item','field_mt_standard_feature',0,46,46,'und',0,225,'','',750,500);
/*!40000 ALTER TABLE `field_revision_field_mt_feature_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_feature_subtitle`
--

DROP TABLE IF EXISTS `field_revision_field_mt_feature_subtitle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_feature_subtitle` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_feature_subtitle_value` longtext,
  `field_mt_feature_subtitle_summary` longtext,
  `field_mt_feature_subtitle_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_feature_subtitle_format` (`field_mt_feature_subtitle_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 49 (field_mt_feature...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_feature_subtitle`
--

LOCK TABLES `field_revision_field_mt_feature_subtitle` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_feature_subtitle` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_feature_subtitle` VALUES ('field_collection_item','field_mt_special_feature',0,32,32,'und',0,'Happy clients','','filtered_html'),('field_collection_item','field_mt_special_feature',0,33,33,'und',0,'Offices worldwide','','filtered_html'),('field_collection_item','field_mt_special_feature',0,34,34,'und',0,'Finished projects','','filtered_html'),('field_collection_item','field_mt_special_feature',0,36,36,'und',0,'Happy clients','','filtered_html'),('field_collection_item','field_mt_special_feature',0,37,37,'und',0,'Offices worldwide','','filtered_html'),('field_collection_item','field_mt_special_feature',0,38,38,'und',0,'Finished projects','','filtered_html');
/*!40000 ALTER TABLE `field_revision_field_mt_feature_subtitle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_feature_title`
--

DROP TABLE IF EXISTS `field_revision_field_mt_feature_title`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_feature_title` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_feature_title_value` varchar(255) DEFAULT NULL,
  `field_mt_feature_title_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_feature_title_format` (`field_mt_feature_title_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 48 (field_mt_feature...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_feature_title`
--

LOCK TABLES `field_revision_field_mt_feature_title` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_feature_title` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_feature_title` VALUES ('field_collection_item','field_mt_special_feature',0,32,32,'und',0,'122K',NULL),('field_collection_item','field_mt_special_feature',0,33,33,'und',0,'13',NULL),('field_collection_item','field_mt_special_feature',0,34,34,'und',0,'100+',NULL),('field_collection_item','field_mt_special_feature',0,36,36,'und',0,'122K',NULL),('field_collection_item','field_mt_special_feature',0,37,37,'und',0,'13',NULL),('field_collection_item','field_mt_special_feature',0,38,38,'und',0,'100+',NULL),('field_collection_item','field_mt_standard_feature',0,39,39,'und',0,'Feature one',NULL),('field_collection_item','field_mt_standard_feature',0,40,40,'und',0,'Feature Two',NULL),('field_collection_item','field_mt_standard_feature',0,41,41,'und',0,'Feature Three',NULL),('field_collection_item','field_mt_standard_feature',0,42,42,'und',0,'Feature Four',NULL),('field_collection_item','field_mt_standard_feature',0,43,43,'und',0,'Feature one',NULL),('field_collection_item','field_mt_standard_feature',0,44,44,'und',0,'Feature Two',NULL),('field_collection_item','field_mt_standard_feature',0,45,45,'und',0,'Feature Three',NULL),('field_collection_item','field_mt_standard_feature',0,46,46,'und',0,'Feature Four',NULL);
/*!40000 ALTER TABLE `field_revision_field_mt_feature_title` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_font_awesome_classes`
--

DROP TABLE IF EXISTS `field_revision_field_mt_font_awesome_classes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_font_awesome_classes` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_font_awesome_classes_value` varchar(255) DEFAULT NULL,
  `field_mt_font_awesome_classes_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_font_awesome_classes_format` (`field_mt_font_awesome_classes_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 31 (field_mt_font...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_font_awesome_classes`
--

LOCK TABLES `field_revision_field_mt_font_awesome_classes` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_font_awesome_classes` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_font_awesome_classes` VALUES ('node','mt_benefit',0,29,29,'und',0,'fa-space-shuttle',NULL),('node','mt_benefit',0,30,30,'und',0,'fa-star',NULL),('node','mt_benefit',0,31,31,'und',0,'fa-users',NULL);
/*!40000 ALTER TABLE `field_revision_field_mt_font_awesome_classes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_highlight`
--

DROP TABLE IF EXISTS `field_revision_field_mt_highlight`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_highlight` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_highlight_value` int(11) DEFAULT NULL COMMENT 'The field collection item id.',
  `field_mt_highlight_revision_id` int(11) DEFAULT NULL COMMENT 'The field collection item revision id.',
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_highlight_revision_id` (`field_mt_highlight_revision_id`),
  KEY `field_mt_highlight_value` (`field_mt_highlight_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 43 (field_mt_highlight)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_highlight`
--

LOCK TABLES `field_revision_field_mt_highlight` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_highlight` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_highlight` VALUES ('node','mt_service',0,10,10,'und',0,26,26),('node','mt_service',0,10,10,'und',1,27,27),('node','mt_service',0,10,10,'und',2,28,28),('node','mt_service',0,11,11,'und',0,47,47),('node','mt_service',0,12,12,'und',0,48,48),('node','mt_service',0,25,25,'und',0,51,51),('node','mt_product',0,34,34,'und',0,54,54),('node','mt_product',0,42,42,'und',0,53,53),('node','mt_product',0,43,43,'und',0,52,52),('node','mt_product',0,44,44,'und',0,29,29),('node','mt_product',0,44,44,'und',1,30,30),('node','mt_product',0,44,44,'und',2,31,31),('node','mt_service',0,45,45,'und',0,49,49),('node','mt_service',0,46,46,'und',0,50,50),('node','mt_product',0,47,47,'und',0,55,55),('node','mt_product',0,48,48,'und',0,56,56);
/*!40000 ALTER TABLE `field_revision_field_mt_highlight` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_highlight_body`
--

DROP TABLE IF EXISTS `field_revision_field_mt_highlight_body`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_highlight_body` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_highlight_body_value` longtext,
  `field_mt_highlight_body_summary` longtext,
  `field_mt_highlight_body_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_highlight_body_format` (`field_mt_highlight_body_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 56 (field_mt_highlight...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_highlight_body`
--

LOCK TABLES `field_revision_field_mt_highlight_body` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_highlight_body` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_highlight_body` VALUES ('field_collection_item','field_mt_highlight',0,26,26,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html'),('field_collection_item','field_mt_highlight',0,27,27,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html'),('field_collection_item','field_mt_highlight',0,28,28,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html'),('field_collection_item','field_mt_highlight',0,29,29,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html'),('field_collection_item','field_mt_highlight',0,30,30,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html'),('field_collection_item','field_mt_highlight',0,31,31,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html'),('field_collection_item','field_mt_highlight',0,47,47,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html'),('field_collection_item','field_mt_highlight',0,48,48,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html'),('field_collection_item','field_mt_highlight',0,49,49,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html'),('field_collection_item','field_mt_highlight',0,50,50,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html'),('field_collection_item','field_mt_highlight',0,51,51,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html'),('field_collection_item','field_mt_highlight',0,52,52,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html'),('field_collection_item','field_mt_highlight',0,53,53,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html'),('field_collection_item','field_mt_highlight',0,54,54,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html'),('field_collection_item','field_mt_highlight',0,55,55,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html'),('field_collection_item','field_mt_highlight',0,56,56,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','filtered_html');
/*!40000 ALTER TABLE `field_revision_field_mt_highlight_body` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_highlight_image`
--

DROP TABLE IF EXISTS `field_revision_field_mt_highlight_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_highlight_image` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_highlight_image_fid` int(10) unsigned DEFAULT NULL COMMENT 'The file_managed.fid being referenced in this field.',
  `field_mt_highlight_image_alt` varchar(512) DEFAULT NULL COMMENT 'Alternative image text, for the image’s ’alt’ attribute.',
  `field_mt_highlight_image_title` varchar(1024) DEFAULT NULL COMMENT 'Image title text, for the image’s ’title’ attribute.',
  `field_mt_highlight_image_width` int(10) unsigned DEFAULT NULL COMMENT 'The width of the image in pixels.',
  `field_mt_highlight_image_height` int(10) unsigned DEFAULT NULL COMMENT 'The height of the image in pixels.',
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_highlight_image_fid` (`field_mt_highlight_image_fid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 45 (field_mt_highlight...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_highlight_image`
--

LOCK TABLES `field_revision_field_mt_highlight_image` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_highlight_image` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_highlight_image` VALUES ('field_collection_item','field_mt_highlight',0,26,26,'und',0,239,'','',750,500),('field_collection_item','field_mt_highlight',0,27,27,'und',0,240,'','',750,500),('field_collection_item','field_mt_highlight',0,28,28,'und',0,241,'','',750,500),('field_collection_item','field_mt_highlight',0,29,29,'und',0,242,'','',750,500),('field_collection_item','field_mt_highlight',0,30,30,'und',0,243,'','',750,500),('field_collection_item','field_mt_highlight',0,31,31,'und',0,244,'','',750,500),('field_collection_item','field_mt_highlight',0,47,47,'und',0,245,'','',750,500),('field_collection_item','field_mt_highlight',0,48,48,'und',0,246,'','',750,500),('field_collection_item','field_mt_highlight',0,49,49,'und',0,247,'','',750,500),('field_collection_item','field_mt_highlight',0,50,50,'und',0,248,'','',750,500),('field_collection_item','field_mt_highlight',0,51,51,'und',0,298,'','',750,500),('field_collection_item','field_mt_highlight',0,52,52,'und',0,250,'','',750,500),('field_collection_item','field_mt_highlight',0,53,53,'und',0,251,'','',750,500),('field_collection_item','field_mt_highlight',0,54,54,'und',0,300,'','',750,500),('field_collection_item','field_mt_highlight',0,55,55,'und',0,262,'','',750,500),('field_collection_item','field_mt_highlight',0,56,56,'und',0,263,'','',750,500);
/*!40000 ALTER TABLE `field_revision_field_mt_highlight_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_highlight_link`
--

DROP TABLE IF EXISTS `field_revision_field_mt_highlight_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_highlight_link` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_highlight_link_url` varchar(2048) DEFAULT NULL,
  `field_mt_highlight_link_title` varchar(255) DEFAULT NULL,
  `field_mt_highlight_link_attributes` mediumtext,
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 57 (field_mt_highlight...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_highlight_link`
--

LOCK TABLES `field_revision_field_mt_highlight_link` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_highlight_link` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_highlight_link` VALUES ('field_collection_item','field_mt_highlight',0,26,26,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}'),('field_collection_item','field_mt_highlight',0,27,27,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}'),('field_collection_item','field_mt_highlight',0,28,28,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}'),('field_collection_item','field_mt_highlight',0,29,29,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}'),('field_collection_item','field_mt_highlight',0,30,30,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}'),('field_collection_item','field_mt_highlight',0,31,31,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}'),('field_collection_item','field_mt_highlight',0,47,47,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}'),('field_collection_item','field_mt_highlight',0,48,48,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}'),('field_collection_item','field_mt_highlight',0,49,49,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}'),('field_collection_item','field_mt_highlight',0,50,50,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}'),('field_collection_item','field_mt_highlight',0,51,51,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}'),('field_collection_item','field_mt_highlight',0,52,52,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}'),('field_collection_item','field_mt_highlight',0,53,53,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}'),('field_collection_item','field_mt_highlight',0,54,54,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}'),('field_collection_item','field_mt_highlight',0,55,55,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}'),('field_collection_item','field_mt_highlight',0,56,56,'und',0,'<front>','Read More','a:1:{s:6:\"target\";i:0;}');
/*!40000 ALTER TABLE `field_revision_field_mt_highlight_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_highlight_title`
--

DROP TABLE IF EXISTS `field_revision_field_mt_highlight_title`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_highlight_title` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_highlight_title_value` varchar(255) DEFAULT NULL,
  `field_mt_highlight_title_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_highlight_title_format` (`field_mt_highlight_title_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 55 (field_mt_highlight...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_highlight_title`
--

LOCK TABLES `field_revision_field_mt_highlight_title` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_highlight_title` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_highlight_title` VALUES ('field_collection_item','field_mt_highlight',0,26,26,'und',0,'Name of first highlight',NULL),('field_collection_item','field_mt_highlight',0,27,27,'und',0,'Name of second highlight',NULL),('field_collection_item','field_mt_highlight',0,28,28,'und',0,'Name of third highlight',NULL),('field_collection_item','field_mt_highlight',0,29,29,'und',0,'Name of first highlight',NULL),('field_collection_item','field_mt_highlight',0,30,30,'und',0,'Name of second highlight',NULL),('field_collection_item','field_mt_highlight',0,31,31,'und',0,'Name of third highlight',NULL),('field_collection_item','field_mt_highlight',0,47,47,'und',0,'Name of first highlight',NULL),('field_collection_item','field_mt_highlight',0,48,48,'und',0,'Name of first highlight',NULL),('field_collection_item','field_mt_highlight',0,49,49,'und',0,'Name of first highlight',NULL),('field_collection_item','field_mt_highlight',0,50,50,'und',0,'Name of first highlight',NULL),('field_collection_item','field_mt_highlight',0,51,51,'und',0,'Name of first highlight',NULL),('field_collection_item','field_mt_highlight',0,52,52,'und',0,'Name of first highlight',NULL),('field_collection_item','field_mt_highlight',0,53,53,'und',0,'Name of first highlight',NULL),('field_collection_item','field_mt_highlight',0,54,54,'und',0,'Name of first highlight',NULL),('field_collection_item','field_mt_highlight',0,55,55,'und',0,'Name of first highlight',NULL),('field_collection_item','field_mt_highlight',0,56,56,'und',0,'Name of first highlight',NULL);
/*!40000 ALTER TABLE `field_revision_field_mt_highlight_title` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_landscape_image`
--

DROP TABLE IF EXISTS `field_revision_field_mt_landscape_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_landscape_image` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_landscape_image_fid` int(10) unsigned DEFAULT NULL COMMENT 'The file_managed.fid being referenced in this field.',
  `field_mt_landscape_image_alt` varchar(512) DEFAULT NULL COMMENT 'Alternative image text, for the image’s ’alt’ attribute.',
  `field_mt_landscape_image_title` varchar(1024) DEFAULT NULL COMMENT 'Image title text, for the image’s ’title’ attribute.',
  `field_mt_landscape_image_width` int(10) unsigned DEFAULT NULL COMMENT 'The width of the image in pixels.',
  `field_mt_landscape_image_height` int(10) unsigned DEFAULT NULL COMMENT 'The height of the image in pixels.',
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_landscape_image_fid` (`field_mt_landscape_image_fid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 66 (field_mt_landscape...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_landscape_image`
--

LOCK TABLES `field_revision_field_mt_landscape_image` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_landscape_image` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_landscape_image` VALUES ('node','blog',0,13,13,'und',0,278,'','',750,354),('node','blog',0,52,52,'und',0,280,'','',750,354);
/*!40000 ALTER TABLE `field_revision_field_mt_landscape_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_linkedin_account`
--

DROP TABLE IF EXISTS `field_revision_field_mt_linkedin_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_linkedin_account` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_linkedin_account_value` varchar(255) DEFAULT NULL,
  `field_mt_linkedin_account_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_linkedin_account_format` (`field_mt_linkedin_account_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 41 (field_mt_linkedin...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_linkedin_account`
--

LOCK TABLES `field_revision_field_mt_linkedin_account` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_linkedin_account` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_linkedin_account` VALUES ('node','mt_team_member',0,36,36,'und',0,'https://www.linkedin.com',NULL),('node','mt_team_member',0,37,37,'und',0,'https://www.linkedin.com',NULL),('node','mt_team_member',0,38,38,'und',0,'https://www.linkedin.com',NULL),('node','mt_team_member',0,39,39,'und',0,'https://www.linkedin.com',NULL),('node','mt_team_member',0,40,40,'und',0,'https://www.linkedin.com',NULL);
/*!40000 ALTER TABLE `field_revision_field_mt_linkedin_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_member_photo`
--

DROP TABLE IF EXISTS `field_revision_field_mt_member_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_member_photo` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_member_photo_fid` int(10) unsigned DEFAULT NULL COMMENT 'The file_managed.fid being referenced in this field.',
  `field_mt_member_photo_alt` varchar(512) DEFAULT NULL COMMENT 'Alternative image text, for the image’s ’alt’ attribute.',
  `field_mt_member_photo_title` varchar(1024) DEFAULT NULL COMMENT 'Image title text, for the image’s ’title’ attribute.',
  `field_mt_member_photo_width` int(10) unsigned DEFAULT NULL COMMENT 'The width of the image in pixels.',
  `field_mt_member_photo_height` int(10) unsigned DEFAULT NULL COMMENT 'The height of the image in pixels.',
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_member_photo_fid` (`field_mt_member_photo_fid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 38 (field_mt_member...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_member_photo`
--

LOCK TABLES `field_revision_field_mt_member_photo` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_member_photo` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_member_photo` VALUES ('node','mt_team_member',0,36,36,'und',0,191,'','',750,750),('node','mt_team_member',0,37,37,'und',0,192,'','',750,750),('node','mt_team_member',0,38,38,'und',0,193,'','',750,750),('node','mt_team_member',0,39,39,'und',0,194,'','',750,750),('node','mt_team_member',0,40,40,'und',0,195,'','',750,750);
/*!40000 ALTER TABLE `field_revision_field_mt_member_photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_most_popular`
--

DROP TABLE IF EXISTS `field_revision_field_mt_most_popular`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_most_popular` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_most_popular_value` int(11) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_most_popular_value` (`field_mt_most_popular_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 69 (field_mt_most...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_most_popular`
--

LOCK TABLES `field_revision_field_mt_most_popular` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_most_popular` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_most_popular` VALUES ('node','mt_product',0,34,34,'und',0,0),('node','mt_product',0,42,42,'und',0,0),('node','mt_product',0,43,43,'und',0,1),('node','mt_product',0,44,44,'und',0,0),('node','mt_product',0,47,47,'und',0,0),('node','mt_product',0,48,48,'und',0,0);
/*!40000 ALTER TABLE `field_revision_field_mt_most_popular` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_portrait_image`
--

DROP TABLE IF EXISTS `field_revision_field_mt_portrait_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_portrait_image` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_portrait_image_fid` int(10) unsigned DEFAULT NULL COMMENT 'The file_managed.fid being referenced in this field.',
  `field_mt_portrait_image_alt` varchar(512) DEFAULT NULL COMMENT 'Alternative image text, for the image’s ’alt’ attribute.',
  `field_mt_portrait_image_title` varchar(1024) DEFAULT NULL COMMENT 'Image title text, for the image’s ’title’ attribute.',
  `field_mt_portrait_image_width` int(10) unsigned DEFAULT NULL COMMENT 'The width of the image in pixels.',
  `field_mt_portrait_image_height` int(10) unsigned DEFAULT NULL COMMENT 'The height of the image in pixels.',
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_portrait_image_fid` (`field_mt_portrait_image_fid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 60 (field_mt_portrait...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_portrait_image`
--

LOCK TABLES `field_revision_field_mt_portrait_image` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_portrait_image` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_portrait_image` VALUES ('node','mt_service',0,10,10,'und',0,232,'','',750,1000),('node','mt_service',0,11,11,'und',0,234,'','',750,1000),('node','mt_showcase',0,21,21,'und',0,258,'','',750,1000),('node','mt_product',0,42,42,'und',0,265,'','',750,1000),('node','mt_product',0,44,44,'und',0,288,'','',750,1000);
/*!40000 ALTER TABLE `field_revision_field_mt_portrait_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_price`
--

DROP TABLE IF EXISTS `field_revision_field_mt_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_price` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_price_value` varchar(255) DEFAULT NULL,
  `field_mt_price_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_price_format` (`field_mt_price_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 68 (field_mt_price)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_price`
--

LOCK TABLES `field_revision_field_mt_price` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_price` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_price` VALUES ('node','mt_product',0,42,42,'und',0,'$39.00',NULL),('node','mt_product',0,43,43,'und',0,'$29.00',NULL),('node','mt_product',0,44,44,'und',0,'$12.00',NULL);
/*!40000 ALTER TABLE `field_revision_field_mt_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_pricing_table_item`
--

DROP TABLE IF EXISTS `field_revision_field_mt_pricing_table_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_pricing_table_item` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_pricing_table_item_value` varchar(255) DEFAULT NULL,
  `field_mt_pricing_table_item_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_pricing_table_item_format` (`field_mt_pricing_table_item_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 67 (field_mt_pricing...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_pricing_table_item`
--

LOCK TABLES `field_revision_field_mt_pricing_table_item` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_pricing_table_item` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_pricing_table_item` VALUES ('node','mt_product',0,42,42,'und',0,'Account Balance',NULL),('node','mt_product',0,42,42,'und',1,'Online Interface',NULL),('node','mt_product',0,42,42,'und',2,'Weekly Reports',NULL),('node','mt_product',0,42,42,'und',3,'Email support',NULL),('node','mt_product',0,43,43,'und',0,'Account Balance',NULL),('node','mt_product',0,43,43,'und',1,'Online Interface',NULL),('node','mt_product',0,43,43,'und',2,'Investment Monitoring',NULL),('node','mt_product',0,43,43,'und',3,'Weekly Reports',NULL),('node','mt_product',0,43,43,'und',4,'One-on-one consultation',NULL),('node','mt_product',0,44,44,'und',0,'Account Balance',NULL),('node','mt_product',0,44,44,'und',1,'Online Interface',NULL),('node','mt_product',0,44,44,'und',2,'Weekly Reports',NULL);
/*!40000 ALTER TABLE `field_revision_field_mt_pricing_table_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_product_tags`
--

DROP TABLE IF EXISTS `field_revision_field_mt_product_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_product_tags` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_product_tags_tid` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_product_tags_tid` (`field_mt_product_tags_tid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 63 (field_mt_product...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_product_tags`
--

LOCK TABLES `field_revision_field_mt_product_tags` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_product_tags` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_product_tags` VALUES ('node','mt_product',0,34,34,'und',0,26),('node','mt_product',0,42,42,'und',0,25),('node','mt_product',0,43,43,'und',0,24),('node','mt_product',0,44,44,'und',0,23),('node','mt_product',0,47,47,'und',0,24),('node','mt_product',0,48,48,'und',0,25);
/*!40000 ALTER TABLE `field_revision_field_mt_product_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_promoted_on_slideshow`
--

DROP TABLE IF EXISTS `field_revision_field_mt_promoted_on_slideshow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_promoted_on_slideshow` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_promoted_on_slideshow_value` int(11) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_promoted_on_slideshow_value` (`field_mt_promoted_on_slideshow_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 33 (field_mt_promoted...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_promoted_on_slideshow`
--

LOCK TABLES `field_revision_field_mt_promoted_on_slideshow` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_promoted_on_slideshow` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_promoted_on_slideshow` VALUES ('node','page',0,1,1,'und',0,0),('node','page',0,2,2,'und',0,0),('node','mt_service',0,10,10,'und',0,1),('node','mt_service',0,11,11,'und',0,0),('node','mt_service',0,12,12,'und',0,0),('node','blog',0,13,13,'und',0,0),('node','blog',0,14,14,'und',0,0),('node','blog',0,15,15,'und',0,0),('node','mt_showcase',0,16,16,'und',0,0),('node','mt_showcase',0,17,17,'und',0,0),('node','mt_showcase',0,18,18,'und',0,0),('node','mt_showcase',0,19,19,'und',0,0),('node','mt_showcase',0,20,20,'und',0,1),('node','mt_showcase',0,21,21,'und',0,0),('node','webform',0,22,22,'und',0,0),('node','mt_service',0,25,25,'und',0,0),('node','mt_benefit',0,29,29,'und',0,0),('node','mt_benefit',0,30,30,'und',0,0),('node','mt_benefit',0,31,31,'und',0,0),('node','webform',0,32,32,'und',0,0),('node','mt_product',0,34,34,'und',0,0),('node','mt_product',0,42,42,'und',0,0),('node','mt_product',0,43,43,'und',0,0),('node','mt_product',0,44,44,'und',0,1),('node','mt_service',0,45,45,'und',0,0),('node','mt_service',0,46,46,'und',0,0),('node','mt_product',0,47,47,'und',0,0),('node','mt_product',0,48,48,'und',0,0),('node','blog',0,52,52,'und',0,0),('node','blog',0,53,53,'und',0,0),('node','blog',0,54,54,'und',0,0),('node','page',0,55,55,'und',0,0),('node','mt_showcase',0,56,56,'und',0,0),('node','page',0,57,57,'und',0,0),('node','page',0,58,58,'und',0,0);
/*!40000 ALTER TABLE `field_revision_field_mt_promoted_on_slideshow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_service_tags`
--

DROP TABLE IF EXISTS `field_revision_field_mt_service_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_service_tags` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_service_tags_tid` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_service_tags_tid` (`field_mt_service_tags_tid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 61 (field_mt_service...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_service_tags`
--

LOCK TABLES `field_revision_field_mt_service_tags` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_service_tags` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_service_tags` VALUES ('node','mt_service',0,10,10,'und',0,19),('node','mt_service',0,11,11,'und',0,21),('node','mt_service',0,12,12,'und',0,21),('node','mt_service',0,25,25,'und',0,22),('node','mt_service',0,45,45,'und',0,20),('node','mt_service',0,46,46,'und',0,20);
/*!40000 ALTER TABLE `field_revision_field_mt_service_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_showcase_tags`
--

DROP TABLE IF EXISTS `field_revision_field_mt_showcase_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_showcase_tags` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_showcase_tags_tid` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_showcase_tags_tid` (`field_mt_showcase_tags_tid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 37 (field_mt_showcase...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_showcase_tags`
--

LOCK TABLES `field_revision_field_mt_showcase_tags` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_showcase_tags` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_showcase_tags` VALUES ('node','mt_showcase',0,16,16,'und',0,15),('node','mt_showcase',0,17,17,'und',0,15),('node','mt_showcase',0,18,18,'und',0,16),('node','mt_showcase',0,19,19,'und',0,17),('node','mt_showcase',0,20,20,'und',0,15),('node','mt_showcase',0,20,20,'und',1,27),('node','mt_showcase',0,21,21,'und',0,16),('node','mt_showcase',0,56,56,'und',0,35);
/*!40000 ALTER TABLE `field_revision_field_mt_showcase_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_slideshow_image`
--

DROP TABLE IF EXISTS `field_revision_field_mt_slideshow_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_slideshow_image` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_slideshow_image_fid` int(10) unsigned DEFAULT NULL COMMENT 'The file_managed.fid being referenced in this field.',
  `field_mt_slideshow_image_alt` varchar(512) DEFAULT NULL COMMENT 'Alternative image text, for the image’s ’alt’ attribute.',
  `field_mt_slideshow_image_title` varchar(1024) DEFAULT NULL COMMENT 'Image title text, for the image’s ’title’ attribute.',
  `field_mt_slideshow_image_width` int(10) unsigned DEFAULT NULL COMMENT 'The width of the image in pixels.',
  `field_mt_slideshow_image_height` int(10) unsigned DEFAULT NULL COMMENT 'The height of the image in pixels.',
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_slideshow_image_fid` (`field_mt_slideshow_image_fid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 32 (field_mt_slideshow...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_slideshow_image`
--

LOCK TABLES `field_revision_field_mt_slideshow_image` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_slideshow_image` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_slideshow_image` VALUES ('node','mt_service',0,10,10,'und',0,196,'','',1920,630),('node','mt_showcase',0,20,20,'und',0,198,'','',1920,630),('node','mt_product',0,44,44,'und',0,197,'','',1920,630);
/*!40000 ALTER TABLE `field_revision_field_mt_slideshow_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_slideshow_path`
--

DROP TABLE IF EXISTS `field_revision_field_mt_slideshow_path`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_slideshow_path` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_slideshow_path_value` varchar(255) DEFAULT NULL,
  `field_mt_slideshow_path_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_slideshow_path_format` (`field_mt_slideshow_path_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 34 (field_mt_slideshow...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_slideshow_path`
--

LOCK TABLES `field_revision_field_mt_slideshow_path` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_slideshow_path` DISABLE KEYS */;
/*!40000 ALTER TABLE `field_revision_field_mt_slideshow_path` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_special_feature`
--

DROP TABLE IF EXISTS `field_revision_field_mt_special_feature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_special_feature` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_special_feature_value` int(11) DEFAULT NULL COMMENT 'The field collection item id.',
  `field_mt_special_feature_revision_id` int(11) DEFAULT NULL COMMENT 'The field collection item revision id.',
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_special_feature_revision_id` (`field_mt_special_feature_revision_id`),
  KEY `field_mt_special_feature_value` (`field_mt_special_feature_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 46 (field_mt_special...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_special_feature`
--

LOCK TABLES `field_revision_field_mt_special_feature` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_special_feature` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_special_feature` VALUES ('node','mt_service',0,10,10,'und',0,32,32),('node','mt_service',0,10,10,'und',1,33,33),('node','mt_service',0,10,10,'und',2,34,34),('node','mt_product',0,44,44,'und',0,36,36),('node','mt_product',0,44,44,'und',1,37,37),('node','mt_product',0,44,44,'und',2,38,38);
/*!40000 ALTER TABLE `field_revision_field_mt_special_feature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_standard_feature`
--

DROP TABLE IF EXISTS `field_revision_field_mt_standard_feature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_standard_feature` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_standard_feature_value` int(11) DEFAULT NULL COMMENT 'The field collection item id.',
  `field_mt_standard_feature_revision_id` int(11) DEFAULT NULL COMMENT 'The field collection item revision id.',
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_standard_feature_revision_id` (`field_mt_standard_feature_revision_id`),
  KEY `field_mt_standard_feature_value` (`field_mt_standard_feature_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 54 (field_mt_standard...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_standard_feature`
--

LOCK TABLES `field_revision_field_mt_standard_feature` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_standard_feature` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_standard_feature` VALUES ('node','mt_service',0,10,10,'und',0,39,39),('node','mt_service',0,10,10,'und',1,40,40),('node','mt_service',0,10,10,'und',2,41,41),('node','mt_service',0,10,10,'und',3,42,42),('node','mt_product',0,44,44,'und',0,43,43),('node','mt_product',0,44,44,'und',1,44,44),('node','mt_product',0,44,44,'und',2,45,45),('node','mt_product',0,44,44,'und',3,46,46);
/*!40000 ALTER TABLE `field_revision_field_mt_standard_feature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_subheader_body`
--

DROP TABLE IF EXISTS `field_revision_field_mt_subheader_body`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_subheader_body` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_subheader_body_value` longtext,
  `field_mt_subheader_body_summary` longtext,
  `field_mt_subheader_body_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_subheader_body_format` (`field_mt_subheader_body_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 30 (field_mt_subheader...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_subheader_body`
--

LOCK TABLES `field_revision_field_mt_subheader_body` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_subheader_body` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_subheader_body` VALUES ('node','mt_service',0,10,10,'und',0,'Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies appropriately communicate.','','filtered_html'),('node','webform',0,22,22,'und',0,'<p>We respect your privacy 100%, so the information that you provide will remain strictly confidential.</p>','','full_html'),('node','page',0,35,35,'und',0,'Lorem ipsum dolor sit amet, consectetur adipisicing <br> tempor incididunt ut labore et dolore aliqua.','','filtered_html'),('node','mt_product',0,44,44,'und',0,'Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies appropriately communicate.','','filtered_html'),('node','page',0,55,55,'und',0,'Phosfluorescently e-enable adaptive synergy for strategic quality vectors. Continually transform fully tested expertise with competitive technologies appropriately communicate.','','filtered_html');
/*!40000 ALTER TABLE `field_revision_field_mt_subheader_body` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_subtitle`
--

DROP TABLE IF EXISTS `field_revision_field_mt_subtitle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_subtitle` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_subtitle_value` longtext,
  `field_mt_subtitle_summary` longtext,
  `field_mt_subtitle_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_subtitle_format` (`field_mt_subtitle_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 36 (field_mt_subtitle)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_subtitle`
--

LOCK TABLES `field_revision_field_mt_subtitle` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_subtitle` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_subtitle` VALUES ('node','mt_testimonial',0,3,3,'und',0,'Marketing specialist','','filtered_html'),('node','mt_testimonial',0,24,24,'und',0,'Commercial director','','filtered_html'),('node','mt_benefit',0,29,29,'und',0,'100+','','filtered_html'),('node','mt_benefit',0,30,30,'und',0,'13','','filtered_html'),('node','mt_benefit',0,31,31,'und',0,'122k','',NULL),('node','mt_testimonial',0,49,49,'und',0,'Business Solutions','','filtered_html'),('node','mt_testimonial',0,50,50,'und',0,'Business Solutions','','filtered_html'),('node','mt_testimonial',0,51,51,'und',0,'Lorem sit amet','','filtered_html');
/*!40000 ALTER TABLE `field_revision_field_mt_subtitle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_testimonial_image`
--

DROP TABLE IF EXISTS `field_revision_field_mt_testimonial_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_testimonial_image` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_testimonial_image_fid` int(10) unsigned DEFAULT NULL COMMENT 'The file_managed.fid being referenced in this field.',
  `field_mt_testimonial_image_alt` varchar(512) DEFAULT NULL COMMENT 'Alternative image text, for the image’s ’alt’ attribute.',
  `field_mt_testimonial_image_title` varchar(1024) DEFAULT NULL COMMENT 'Image title text, for the image’s ’title’ attribute.',
  `field_mt_testimonial_image_width` int(10) unsigned DEFAULT NULL COMMENT 'The width of the image in pixels.',
  `field_mt_testimonial_image_height` int(10) unsigned DEFAULT NULL COMMENT 'The height of the image in pixels.',
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_testimonial_image_fid` (`field_mt_testimonial_image_fid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 42 (field_mt...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_testimonial_image`
--

LOCK TABLES `field_revision_field_mt_testimonial_image` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_testimonial_image` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_testimonial_image` VALUES ('node','mt_testimonial',0,3,3,'und',0,190,'','',100,100),('node','mt_testimonial',0,24,24,'und',0,188,'','',100,100),('node','mt_testimonial',0,49,49,'und',0,266,'','',100,100),('node','mt_testimonial',0,50,50,'und',0,267,'','',100,100),('node','mt_testimonial',0,51,51,'und',0,268,'','',100,100);
/*!40000 ALTER TABLE `field_revision_field_mt_testimonial_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_mt_twitter_account`
--

DROP TABLE IF EXISTS `field_revision_field_mt_twitter_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_mt_twitter_account` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_mt_twitter_account_value` varchar(255) DEFAULT NULL,
  `field_mt_twitter_account_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_mt_twitter_account_format` (`field_mt_twitter_account_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 40 (field_mt_twitter...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_mt_twitter_account`
--

LOCK TABLES `field_revision_field_mt_twitter_account` WRITE;
/*!40000 ALTER TABLE `field_revision_field_mt_twitter_account` DISABLE KEYS */;
INSERT INTO `field_revision_field_mt_twitter_account` VALUES ('node','mt_team_member',0,36,36,'und',0,'https://twitter.com/',NULL),('node','mt_team_member',0,37,37,'und',0,'https://twitter.com/',NULL),('node','mt_team_member',0,38,38,'und',0,'https://twitter.com/',NULL),('node','mt_team_member',0,39,39,'und',0,'https://twitter.com/',NULL),('node','mt_team_member',0,40,40,'und',0,'https://twitter.com/',NULL);
/*!40000 ALTER TABLE `field_revision_field_mt_twitter_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_revision_field_tags`
--

DROP TABLE IF EXISTS `field_revision_field_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_revision_field_tags` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned NOT NULL COMMENT 'The entity revision id this data is attached to',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_tags_tid` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`revision_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_tags_tid` (`field_tags_tid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revision archive storage for field 3 (field_tags)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_revision_field_tags`
--

LOCK TABLES `field_revision_field_tags` WRITE;
/*!40000 ALTER TABLE `field_revision_field_tags` DISABLE KEYS */;
INSERT INTO `field_revision_field_tags` VALUES ('node','blog',0,13,13,'und',0,12),('node','blog',0,13,13,'und',1,13),('node','blog',0,13,13,'und',2,14),('node','blog',0,14,14,'und',0,12),('node','blog',0,14,14,'und',1,13),('node','blog',0,15,15,'und',0,10),('node','blog',0,15,15,'und',1,11),('node','blog',0,52,52,'und',0,13),('node','blog',0,53,53,'und',0,13),('node','blog',0,53,53,'und',1,10),('node','blog',0,54,54,'und',0,11),('node','blog',0,54,54,'und',1,13);
/*!40000 ALTER TABLE `field_revision_field_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_managed`
--

DROP TABLE IF EXISTS `file_managed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_managed` (
  `fid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'File ID.',
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The users.uid of the user who is associated with the file.',
  `filename` varchar(255) NOT NULL DEFAULT '' COMMENT 'Name of the file with no path components. This may differ from the basename of the URI if the file is renamed to avoid overwriting an existing file.',
  `uri` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'The URI to access the file (either local or remote).',
  `filemime` varchar(255) NOT NULL DEFAULT '' COMMENT 'The file’s MIME type.',
  `filesize` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'The size of the file in bytes.',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A field indicating the status of the file. Two status are defined in core: temporary (0) and permanent (1). Temporary files older than DRUPAL_MAXIMUM_TEMP_FILE_AGE will be removed during a cron run.',
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'UNIX timestamp for when the file was added.',
  PRIMARY KEY (`fid`),
  UNIQUE KEY `uri` (`uri`),
  KEY `uid` (`uid`),
  KEY `status` (`status`),
  KEY `timestamp` (`timestamp`)
) ENGINE=InnoDB AUTO_INCREMENT=309 DEFAULT CHARSET=utf8 COMMENT='Stores information for uploaded files.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_managed`
--

LOCK TABLES `file_managed` WRITE;
/*!40000 ALTER TABLE `file_managed` DISABLE KEYS */;
INSERT INTO `file_managed` VALUES (134,1,'team-member-default.jpg','public://default_images/team-member-default.jpg','image/jpeg',12753,1,1423829773),(181,1,'team-member-default.jpg','public://default_images/team-member-default_0.jpg','image/jpeg',12753,1,1448635820),(188,1,'testimonial-2.jpg','public://testimonial-2.jpg','image/jpeg',11926,1,1448637455),(190,1,'testimonial-1.jpg','public://testimonial-1.jpg','image/jpeg',11926,1,1448640430),(191,1,'team-member-1.jpg','public://team-member-1.jpg','image/jpeg',12753,1,1448640527),(192,1,'team-member-2.jpg','public://team-member-2.jpg','image/jpeg',12753,1,1448640538),(193,1,'team-member-3.jpg','public://team-member-3.jpg','image/jpeg',12753,1,1448640547),(194,1,'team-member-4.jpg','public://team-member-4.jpg','image/jpeg',12753,1,1448640555),(195,1,'team-member-5.jpg','public://team-member-5.jpg','image/jpeg',12753,1,1448640563),(196,1,'slide1.jpg','public://slide1.jpg','image/jpeg',31921,1,1448647214),(197,1,'slide2.jpg','public://slide2.jpg','image/jpeg',28528,1,1448647624),(198,1,'slide3.jpg','public://slide3.jpg','image/jpeg',32417,1,1448647651),(210,1,'service-1-banner-1.jpg','public://service-1-banner-1.jpg','image/jpeg',29475,1,1448731171),(211,1,'service-1-banner-2.jpg','public://service-1-banner-2.jpg','image/jpeg',28316,1,1448731171),(218,1,'service-1-feature-1.jpg','public://service-1-feature-1.jpg','image/jpeg',17099,1,1448986950),(219,1,'service-1-feature-2.jpg','public://service-1-feature-2.jpg','image/jpeg',17099,1,1448986950),(220,1,'service-1-feature-3.jpg','public://service-1-feature-3.jpg','image/jpeg',17099,1,1448986950),(221,1,'service-1-feature-4.jpg','public://service-1-feature-4.jpg','image/jpeg',17099,1,1448986950),(222,1,'product-1-feature-1.jpg','public://product-1-feature-1.jpg','image/jpeg',17099,1,1448990332),(223,1,'product-1-feature-2.jpg','public://product-1-feature-2.jpg','image/jpeg',17099,1,1448990332),(224,1,'product-1-feature-3.jpg','public://product-1-feature-3.jpg','image/jpeg',17099,1,1448990332),(225,1,'product-1-feature-4.jpg','public://product-1-feature-4.jpg','image/jpeg',17099,1,1448990332),(232,1,'service-1.jpg','public://service-1.jpg','image/jpeg',23505,1,1449090616),(234,1,'service-3.jpg','public://service-3.jpg','image/jpeg',23505,1,1449090745),(239,1,'service-1-highlight-1.jpg','public://service-1-highlight-1.jpg','image/jpeg',17189,1,1449092692),(240,1,'service-1-highlight-2.jpg','public://service-1-highlight-2.jpg','image/jpeg',17189,1,1449092692),(241,1,'service-1-highlight-3.jpg','public://service-1-highlight-3.jpg','image/jpeg',17189,1,1449092692),(242,1,'product-1-highlight-1.jpg','public://product-1-highlight-1.jpg','image/jpeg',17189,1,1449093136),(243,1,'product-1-highlight-2.jpg','public://product-1-highlight-2.jpg','image/jpeg',17189,1,1449093136),(244,1,'product-1-highlight-3.jpg','public://product-1-highlight-3.jpg','image/jpeg',17189,1,1449093136),(245,1,'service-3-highlight-1.jpg','public://service-3-highlight-1.jpg','image/jpeg',17189,1,1449093243),(246,1,'service-2-highlight-1.jpg','public://service-2-highlight-1.jpg','image/jpeg',17189,1,1449094989),(247,1,'service-5-highlight-1.jpg','public://service-5-highlight-1.jpg','image/jpeg',17189,1,1449095143),(248,1,'service-6-highlight-1.jpg','public://service-6-highlight-1.jpg','image/jpeg',17189,1,1449095179),(250,1,'product-2-highlight-1.jpg','public://product-2-highlight-1.jpg','image/jpeg',17189,1,1449095451),(251,1,'product-3-highlight-1.jpg','public://product-3-highlight-1.jpg','image/jpeg',17189,1,1449095468),(253,1,'showcase-1.jpg','public://showcase-1.jpg','image/jpeg',17189,1,1449105065),(254,1,'showcase-1-2.jpg','public://showcase-1-2.jpg','image/jpeg',17189,1,1449105065),(255,1,'showcase-1-3.jpg','public://showcase-1-3.jpg','image/jpeg',17189,1,1449105065),(256,1,'showcase-2.jpg','public://showcase-2.jpg','image/jpeg',17189,1,1449105095),(257,1,'showcase-3.jpg','public://showcase-3.jpg','image/jpeg',17189,1,1449105149),(258,1,'showcase-3-portrait.jpg','public://showcase-3-portrait.jpg','image/jpeg',23505,1,1449105149),(259,1,'showcase-4.jpg','public://showcase-4.jpg','image/jpeg',17189,1,1449105177),(260,1,'showcase-5.jpg','public://showcase-5.jpg','image/jpeg',17189,1,1449105193),(261,1,'showcase-6.jpg','public://showcase-6.jpg','image/jpeg',17189,1,1449105215),(262,1,'product-5-highlight-1.jpg','public://product-5-highlight-1.jpg','image/jpeg',17189,1,1449107071),(263,1,'product-6-highlight-1.jpg','public://product-6-highlight-1.jpg','image/jpeg',17189,1,1449107138),(265,1,'product-3.jpg','public://product-3.jpg','image/jpeg',23505,1,1449107239),(266,1,'testimonial-3.jpg','public://testimonial-3.jpg','image/jpeg',11926,1,1449575526),(267,1,'testimonial-4.jpg','public://testimonial-4.jpg','image/jpeg',11926,1,1449575588),(268,1,'testimonial-5.jpg','public://testimonial-5.jpg','image/jpeg',11926,1,1449575654),(275,1,'blogpost-1.jpg','public://blogpost-1.jpg','image/jpeg',17189,1,1449692065),(276,1,'blogpost-2.jpg','public://blogpost-2.jpg','image/jpeg',17189,1,1449692093),(277,1,'blogpost-3.jpg','public://blogpost-3.jpg','image/jpeg',17189,1,1449692124),(278,1,'blogpost-3-landscape.jpg','public://blogpost-3-landscape.jpg','image/jpeg',15610,1,1449693402),(280,1,'blogpost-4-landscape.jpg','public://blogpost-4-landscape.jpg','image/jpeg',15610,1,1449694488),(281,1,'blogpost-5.jpg','public://blogpost-5.jpg','image/jpeg',17189,1,1449694558),(282,1,'blogpost-6.jpg','public://blogpost-6.jpg','image/jpeg',17189,1,1449694609),(283,1,'contact-1-banner-1.jpg','public://contact-1-banner-1.jpg','image/jpeg',32724,1,1449869579),(284,1,'contact-1-banner-2.jpg','public://contact-1-banner-2.jpg','image/jpeg',25197,1,1449869579),(287,1,'blogpost-1-2.jpg','public://blogpost-1-2.jpg','image/jpeg',17189,1,1450262426),(288,1,'product-1.jpg','public://product-1.jpg','image/jpeg',23505,1,1450264155),(289,1,'logo.svg','public://logos/main/logo.svg','image/svg+xml',827,1,1450736090),(290,1,'logo.svg','public://logos/main/logo_0.svg','image/svg+xml',5946,1,1451485857),(296,1,'showcase-7.jpg','public://showcase-7_0.jpg','image/jpeg',17189,1,1453195241),(298,1,'service-4-highlight-1.jpg','public://service-4-highlight-1.jpg','image/jpeg',17189,1,1458825881),(299,1,'blogpost-4.jpg','public://blogpost-4.jpg','image/jpeg',17189,1,1458825998),(300,1,'product-4-highlight-1.jpg','public://product-4-highlight-1.jpg','image/jpeg',17189,1,1458826089),(301,1,'logo-1.png','public://logo-1.png','image/png',3062,1,1458846573),(303,1,'logo-3.png','public://logo-3.png','image/png',2667,1,1458846598),(304,1,'logo-4.png','public://logo-4.png','image/png',3148,1,1458846615),(305,1,'logo-5.png','public://logo-5.png','image/png',2758,1,1458846629),(306,1,'logo-6.png','public://logo-6.png','image/png',2667,1,1458846641),(307,1,'logo-7.png','public://logo-7.png','image/png',2667,1,1458846652),(308,1,'logo-2.png','public://logo-2.png','image/png',2684,1,1458849136);
/*!40000 ALTER TABLE `file_managed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_usage`
--

DROP TABLE IF EXISTS `file_usage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_usage` (
  `fid` int(10) unsigned NOT NULL COMMENT 'File ID.',
  `module` varchar(255) NOT NULL DEFAULT '' COMMENT 'The name of the module that is using the file.',
  `type` varchar(64) NOT NULL DEFAULT '' COMMENT 'The name of the object type in which the file is used.',
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The primary key of the object using the file.',
  `count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The number of times this file is used by this object.',
  PRIMARY KEY (`fid`,`type`,`id`,`module`),
  KEY `type_id` (`type`,`id`),
  KEY `fid_count` (`fid`,`count`),
  KEY `fid_module` (`fid`,`module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Track where a file is used.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_usage`
--

LOCK TABLES `file_usage` WRITE;
/*!40000 ALTER TABLE `file_usage` DISABLE KEYS */;
INSERT INTO `file_usage` VALUES (181,'image','default_image',38,1),(188,'file','node',24,1),(190,'file','node',3,1),(191,'file','node',36,1),(192,'file','node',37,1),(193,'file','node',38,1),(194,'file','node',39,1),(195,'file','node',40,1),(196,'file','node',10,1),(197,'file','node',44,1),(198,'file','node',20,1),(210,'file','node',10,1),(211,'file','node',10,1),(218,'file','field_collection_item',39,1),(219,'file','field_collection_item',40,1),(220,'file','field_collection_item',41,1),(221,'file','field_collection_item',42,1),(222,'file','field_collection_item',43,1),(223,'file','field_collection_item',44,1),(224,'file','field_collection_item',45,1),(225,'file','field_collection_item',46,1),(232,'file','node',10,1),(234,'file','node',11,1),(239,'file','field_collection_item',26,1),(240,'file','field_collection_item',27,1),(241,'file','field_collection_item',28,1),(242,'file','field_collection_item',29,1),(243,'file','field_collection_item',30,1),(244,'file','field_collection_item',31,1),(245,'file','field_collection_item',47,1),(246,'file','field_collection_item',48,1),(247,'file','field_collection_item',49,1),(248,'file','field_collection_item',50,1),(250,'file','field_collection_item',52,1),(251,'file','field_collection_item',53,1),(253,'file','node',20,1),(254,'file','node',20,1),(255,'file','node',20,1),(256,'file','node',19,1),(257,'file','node',21,1),(258,'file','node',21,1),(259,'file','node',16,1),(260,'file','node',17,1),(261,'file','node',18,1),(262,'file','field_collection_item',55,1),(263,'file','field_collection_item',56,1),(265,'file','node',42,1),(266,'file','node',49,1),(267,'file','node',50,1),(268,'file','node',51,1),(275,'file','node',14,1),(276,'file','node',15,1),(277,'file','node',13,1),(278,'file','node',13,1),(280,'file','node',52,1),(281,'file','node',53,1),(282,'file','node',54,1),(283,'file','node',55,1),(284,'file','node',55,1),(287,'file','node',14,1),(288,'file','node',44,1),(289,'levelplus','theme',1,1),(290,'levelplus','theme',1,1),(296,'file','node',56,1),(298,'file','field_collection_item',51,1),(299,'file','node',52,1),(300,'file','field_collection_item',54,1),(301,'file','node',20,1),(303,'file','node',21,1),(304,'file','node',16,1),(305,'file','node',17,1),(306,'file','node',18,1),(307,'file','node',56,1),(308,'file','node',19,1);
/*!40000 ALTER TABLE `file_usage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filter`
--

DROP TABLE IF EXISTS `filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filter` (
  `format` varchar(255) NOT NULL COMMENT 'Foreign key: The filter_format.format to which this filter is assigned.',
  `module` varchar(64) NOT NULL DEFAULT '' COMMENT 'The origin module of the filter.',
  `name` varchar(32) NOT NULL DEFAULT '' COMMENT 'Name of the filter being referenced.',
  `weight` int(11) NOT NULL DEFAULT '0' COMMENT 'Weight of filter within format.',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT 'Filter enabled status. (1 = enabled, 0 = disabled)',
  `settings` longblob COMMENT 'A serialized array of name value pairs that store the filter settings for the specific format.',
  PRIMARY KEY (`format`,`name`),
  KEY `list` (`weight`,`module`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table that maps filters (HTML corrector) to text formats ...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filter`
--

LOCK TABLES `filter` WRITE;
/*!40000 ALTER TABLE `filter` DISABLE KEYS */;
INSERT INTO `filter` VALUES ('filtered_html','filter','filter_autop',2,1,'a:0:{}'),('filtered_html','filter','filter_html',1,1,'a:3:{s:12:\"allowed_html\";s:74:\"<a> <em> <strong> <cite> <blockquote> <code> <ul> <ol> <li> <dl> <dt> <dd>\";s:16:\"filter_html_help\";i:1;s:20:\"filter_html_nofollow\";i:0;}'),('filtered_html','filter','filter_htmlcorrector',10,1,'a:0:{}'),('filtered_html','filter','filter_html_escape',-10,0,'a:0:{}'),('filtered_html','filter','filter_url',0,1,'a:1:{s:17:\"filter_url_length\";i:72;}'),('full_html','filter','filter_autop',1,1,'a:0:{}'),('full_html','filter','filter_html',-10,0,'a:3:{s:12:\"allowed_html\";s:74:\"<a> <em> <strong> <cite> <blockquote> <code> <ul> <ol> <li> <dl> <dt> <dd>\";s:16:\"filter_html_help\";i:1;s:20:\"filter_html_nofollow\";i:0;}'),('full_html','filter','filter_htmlcorrector',10,1,'a:0:{}'),('full_html','filter','filter_html_escape',-10,0,'a:0:{}'),('full_html','filter','filter_url',0,1,'a:1:{s:17:\"filter_url_length\";i:72;}'),('php_code','filter','filter_autop',0,0,'a:0:{}'),('php_code','filter','filter_html',-10,0,'a:3:{s:12:\"allowed_html\";s:74:\"<a> <em> <strong> <cite> <blockquote> <code> <ul> <ol> <li> <dl> <dt> <dd>\";s:16:\"filter_html_help\";i:1;s:20:\"filter_html_nofollow\";i:0;}'),('php_code','filter','filter_htmlcorrector',10,0,'a:0:{}'),('php_code','filter','filter_html_escape',-10,0,'a:0:{}'),('php_code','filter','filter_url',0,0,'a:1:{s:17:\"filter_url_length\";i:72;}'),('php_code','php','php_code',0,1,'a:0:{}'),('plain_text','filter','filter_autop',2,1,'a:0:{}'),('plain_text','filter','filter_html',-10,0,'a:3:{s:12:\"allowed_html\";s:74:\"<a> <em> <strong> <cite> <blockquote> <code> <ul> <ol> <li> <dl> <dt> <dd>\";s:16:\"filter_html_help\";i:1;s:20:\"filter_html_nofollow\";i:0;}'),('plain_text','filter','filter_htmlcorrector',10,0,'a:0:{}'),('plain_text','filter','filter_html_escape',0,1,'a:0:{}'),('plain_text','filter','filter_url',1,1,'a:1:{s:17:\"filter_url_length\";i:72;}');
/*!40000 ALTER TABLE `filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filter_format`
--

DROP TABLE IF EXISTS `filter_format`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filter_format` (
  `format` varchar(255) NOT NULL COMMENT 'Primary Key: Unique machine name of the format.',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT 'Name of the text format (Filtered HTML).',
  `cache` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Flag to indicate whether format is cacheable. (1 = cacheable, 0 = not cacheable)',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT 'The status of the text format. (1 = enabled, 0 = disabled)',
  `weight` int(11) NOT NULL DEFAULT '0' COMMENT 'Weight of text format to use when listing.',
  PRIMARY KEY (`format`),
  UNIQUE KEY `name` (`name`),
  KEY `status_weight` (`status`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores text formats: custom groupings of filters, such as...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filter_format`
--

LOCK TABLES `filter_format` WRITE;
/*!40000 ALTER TABLE `filter_format` DISABLE KEYS */;
INSERT INTO `filter_format` VALUES ('filtered_html','Filtered HTML',1,1,0),('full_html','Full HTML',1,1,1),('php_code','PHP code',0,1,11),('plain_text','Plain text',1,1,10);
/*!40000 ALTER TABLE `filter_format` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image_effects`
--

DROP TABLE IF EXISTS `image_effects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image_effects` (
  `ieid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier for an image effect.',
  `isid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The image_styles.isid for an image style.',
  `weight` int(11) NOT NULL DEFAULT '0' COMMENT 'The weight of the effect in the style.',
  `name` varchar(255) NOT NULL COMMENT 'The unique name of the effect to be executed.',
  `data` longblob NOT NULL COMMENT 'The configuration data for the effect.',
  PRIMARY KEY (`ieid`),
  KEY `isid` (`isid`),
  KEY `weight` (`weight`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COMMENT='Stores configuration options for image effects.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image_effects`
--

LOCK TABLES `image_effects` WRITE;
/*!40000 ALTER TABLE `image_effects` DISABLE KEYS */;
INSERT INTO `image_effects` VALUES (5,4,2,'image_scale_and_crop','a:2:{s:5:\"width\";s:3:\"750\";s:6:\"height\";s:3:\"500\";}'),(8,6,1,'image_scale_and_crop','a:2:{s:5:\"width\";s:3:\"200\";s:6:\"height\";s:3:\"133\";}'),(12,10,1,'image_scale_and_crop','a:2:{s:5:\"width\";s:2:\"55\";s:6:\"height\";s:2:\"55\";}'),(17,11,1,'image_scale_and_crop','a:2:{s:5:\"width\";s:3:\"350\";s:6:\"height\";s:3:\"233\";}'),(20,12,1,'image_scale_and_crop','a:2:{s:5:\"width\";s:4:\"1140\";s:6:\"height\";s:3:\"630\";}'),(21,13,1,'image_scale_and_crop','a:2:{s:5:\"width\";s:3:\"100\";s:6:\"height\";s:3:\"100\";}'),(23,15,1,'image_scale_and_crop','a:2:{s:5:\"width\";s:3:\"750\";s:6:\"height\";s:3:\"750\";}'),(25,17,0,'image_scale','a:3:{s:5:\"width\";i:100;s:6:\"height\";i:100;s:7:\"upscale\";i:1;}'),(26,18,1,'image_scale_and_crop','a:2:{s:5:\"width\";s:3:\"750\";s:6:\"height\";s:4:\"1000\";}'),(28,19,1,'image_scale','a:3:{s:5:\"width\";s:3:\"205\";s:6:\"height\";s:0:\"\";s:7:\"upscale\";i:0;}'),(29,20,1,'image_scale_and_crop','a:2:{s:5:\"width\";s:3:\"750\";s:6:\"height\";s:3:\"354\";}');
/*!40000 ALTER TABLE `image_effects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image_styles`
--

DROP TABLE IF EXISTS `image_styles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image_styles` (
  `isid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier for an image style.',
  `name` varchar(255) NOT NULL COMMENT 'The style name.',
  `label` varchar(255) NOT NULL DEFAULT '' COMMENT 'The style administrative name.',
  PRIMARY KEY (`isid`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='Stores configuration options for image styles.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image_styles`
--

LOCK TABLES `image_styles` WRITE;
/*!40000 ALTER TABLE `image_styles` DISABLE KEYS */;
INSERT INTO `image_styles` VALUES (4,'large','Large (750x500)'),(6,'small','Small (200x133)'),(7,'slideshow','Slideshow (Fullscreen)'),(10,'user-picture','User-picture (55x55)'),(11,'medium','Medium (350x233)'),(12,'slideshow_boxed','Slideshow Boxed Width (1140x630)'),(13,'icon_style','Icon Style (100x100)'),(15,'square','Square (750x750)'),(17,'thumbnail','Thumbnail (100x100)'),(18,'mt_portrait_image','Portrait Image (750x1000)'),(19,'company_logo','Company logo'),(20,'landscape','Landscape (750x354)');
/*!40000 ALTER TABLE `image_styles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_custom`
--

DROP TABLE IF EXISTS `menu_custom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_custom` (
  `menu_name` varchar(32) NOT NULL DEFAULT '' COMMENT 'Primary Key: Unique key for menu. This is used as a block delta so length is 32.',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT 'Menu title; displayed at top of block.',
  `description` text COMMENT 'Menu description.',
  PRIMARY KEY (`menu_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Holds definitions for top-level custom menus (for example...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_custom`
--

LOCK TABLES `menu_custom` WRITE;
/*!40000 ALTER TABLE `menu_custom` DISABLE KEYS */;
INSERT INTO `menu_custom` VALUES ('main-menu','Main menu','The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.'),('management','Management','The <em>Management</em> menu contains links for administrative tasks.'),('menu-footer-bottom-menu','Footer bottom menu',''),('menu-footer-menu','Footer Menu',''),('navigation','Navigation','The <em>Navigation</em> menu contains links intended for site visitors. Links are added to the <em>Navigation</em> menu automatically by some modules.'),('user-menu','User menu','The <em>User</em> menu contains links related to the user\'s account, as well as the \'Log out\' link.');
/*!40000 ALTER TABLE `menu_custom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_links`
--

DROP TABLE IF EXISTS `menu_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_links` (
  `menu_name` varchar(32) NOT NULL DEFAULT '' COMMENT 'The menu name. All links with the same menu name (such as ’navigation’) are part of the same menu.',
  `mlid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The menu link ID (mlid) is the integer primary key.',
  `plid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The parent link ID (plid) is the mlid of the link above in the hierarchy, or zero if the link is at the top level in its menu.',
  `link_path` varchar(255) NOT NULL DEFAULT '' COMMENT 'The Drupal path or external path this link points to.',
  `router_path` varchar(255) NOT NULL DEFAULT '' COMMENT 'For links corresponding to a Drupal path (external = 0), this connects the link to a menu_router.path for joins.',
  `link_title` varchar(255) NOT NULL DEFAULT '' COMMENT 'The text displayed for the link, which may be modified by a title callback stored in menu_router.',
  `options` blob COMMENT 'A serialized array of options to be passed to the url() or l() function, such as a query string or HTML attributes.',
  `module` varchar(255) NOT NULL DEFAULT 'system' COMMENT 'The name of the module that generated this link.',
  `hidden` smallint(6) NOT NULL DEFAULT '0' COMMENT 'A flag for whether the link should be rendered in menus. (1 = a disabled menu item that may be shown on admin screens, -1 = a menu callback, 0 = a normal, visible link)',
  `external` smallint(6) NOT NULL DEFAULT '0' COMMENT 'A flag to indicate if the link points to a full URL starting with a protocol, like http:// (1 = external, 0 = internal).',
  `has_children` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Flag indicating whether any links have this link as a parent (1 = children exist, 0 = no children).',
  `expanded` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Flag for whether this link should be rendered as expanded in menus - expanded links always have their child links displayed, instead of only when the link is in the active trail (1 = expanded, 0 = not expanded)',
  `weight` int(11) NOT NULL DEFAULT '0' COMMENT 'Link weight among links in the same menu at the same depth.',
  `depth` smallint(6) NOT NULL DEFAULT '0' COMMENT 'The depth relative to the top level. A link with plid == 0 will have depth == 1.',
  `customized` smallint(6) NOT NULL DEFAULT '0' COMMENT 'A flag to indicate that the user has manually created or edited the link (1 = customized, 0 = not customized).',
  `p1` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The first mlid in the materialized path. If N = depth, then pN must equal the mlid. If depth > 1 then p(N-1) must equal the plid. All pX where X > depth must equal zero. The columns p1 .. p9 are also called the parents.',
  `p2` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The second mlid in the materialized path. See p1.',
  `p3` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The third mlid in the materialized path. See p1.',
  `p4` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The fourth mlid in the materialized path. See p1.',
  `p5` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The fifth mlid in the materialized path. See p1.',
  `p6` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The sixth mlid in the materialized path. See p1.',
  `p7` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The seventh mlid in the materialized path. See p1.',
  `p8` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The eighth mlid in the materialized path. See p1.',
  `p9` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The ninth mlid in the materialized path. See p1.',
  `updated` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Flag that indicates that this link was generated during the update from Drupal 5.',
  PRIMARY KEY (`mlid`),
  KEY `path_menu` (`link_path`(128),`menu_name`),
  KEY `menu_plid_expand_child` (`menu_name`,`plid`,`expanded`,`has_children`),
  KEY `menu_parents` (`menu_name`,`p1`,`p2`,`p3`,`p4`,`p5`,`p6`,`p7`,`p8`,`p9`),
  KEY `router_path` (`router_path`(128))
) ENGINE=InnoDB AUTO_INCREMENT=1867 DEFAULT CHARSET=utf8 COMMENT='Contains the individual links within a menu.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_links`
--

LOCK TABLES `menu_links` WRITE;
/*!40000 ALTER TABLE `menu_links` DISABLE KEYS */;
INSERT INTO `menu_links` VALUES ('management',1,0,'admin','admin','Administration','a:0:{}','system',0,0,1,0,9,1,0,1,0,0,0,0,0,0,0,0,0),('user-menu',2,0,'user','user','User account','a:1:{s:5:\"alter\";b:1;}','system',0,0,0,0,-10,1,0,2,0,0,0,0,0,0,0,0,0),('navigation',4,0,'filter/tips','filter/tips','Compose tips','a:0:{}','system',1,0,1,0,0,1,0,4,0,0,0,0,0,0,0,0,0),('navigation',5,0,'node/%','node/%','','a:0:{}','system',0,0,0,0,0,1,0,5,0,0,0,0,0,0,0,0,0),('navigation',6,0,'node/add','node/add','Add content','a:0:{}','system',0,0,1,0,0,1,0,6,0,0,0,0,0,0,0,0,0),('management',7,1,'admin/appearance','admin/appearance','Appearance','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:33:\"Select and configure your themes.\";}}','system',0,0,0,0,-6,2,0,1,7,0,0,0,0,0,0,0,0),('management',8,1,'admin/config','admin/config','Configuration','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:20:\"Administer settings.\";}}','system',0,0,1,0,0,2,0,1,8,0,0,0,0,0,0,0,0),('management',9,1,'admin/content','admin/content','Content','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:32:\"Administer content and comments.\";}}','system',0,0,1,0,-10,2,0,1,9,0,0,0,0,0,0,0,0),('user-menu',10,2,'user/register','user/register','Create new account','a:0:{}','system',-1,0,0,0,0,2,0,2,10,0,0,0,0,0,0,0,0),('management',11,1,'admin/dashboard','admin/dashboard','Dashboard','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:34:\"View and customize your dashboard.\";}}','system',0,0,0,0,-15,2,0,1,11,0,0,0,0,0,0,0,0),('management',12,1,'admin/help','admin/help','Help','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:48:\"Reference for usage, configuration, and modules.\";}}','system',0,0,0,0,9,2,0,1,12,0,0,0,0,0,0,0,0),('management',13,1,'admin/index','admin/index','Index','a:0:{}','system',-1,0,0,0,-18,2,0,1,13,0,0,0,0,0,0,0,0),('user-menu',14,2,'user/login','user/login','Log in','a:0:{}','system',-1,0,0,0,0,2,0,2,14,0,0,0,0,0,0,0,0),('user-menu',15,0,'user/logout','user/logout','Log out','a:0:{}','system',0,0,0,0,10,1,0,15,0,0,0,0,0,0,0,0,0),('management',16,1,'admin/modules','admin/modules','Modules','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:26:\"Extend site functionality.\";}}','system',0,0,0,0,-2,2,0,1,16,0,0,0,0,0,0,0,0),('navigation',17,0,'user/%','user/%','My account','a:0:{}','system',0,0,1,0,0,1,0,17,0,0,0,0,0,0,0,0,0),('management',18,1,'admin/people','admin/people','People','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:45:\"Manage user accounts, roles, and permissions.\";}}','system',0,0,0,0,-4,2,0,1,18,0,0,0,0,0,0,0,0),('management',19,1,'admin/reports','admin/reports','Reports','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:34:\"View reports, updates, and errors.\";}}','system',0,0,1,0,5,2,0,1,19,0,0,0,0,0,0,0,0),('user-menu',20,2,'user/password','user/password','Request new password','a:0:{}','system',-1,0,0,0,0,2,0,2,20,0,0,0,0,0,0,0,0),('management',21,1,'admin/structure','admin/structure','Structure','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:45:\"Administer blocks, content types, menus, etc.\";}}','system',0,0,1,0,-8,2,0,1,21,0,0,0,0,0,0,0,0),('management',22,1,'admin/tasks','admin/tasks','Tasks','a:0:{}','system',-1,0,0,0,-20,2,0,1,22,0,0,0,0,0,0,0,0),('navigation',27,0,'taxonomy/term/%','taxonomy/term/%','Taxonomy term','a:0:{}','system',0,0,0,0,0,1,0,27,0,0,0,0,0,0,0,0,0),('management',29,18,'admin/people/create','admin/people/create','Add user','a:0:{}','system',-1,0,0,0,0,3,0,1,18,29,0,0,0,0,0,0,0),('management',30,21,'admin/structure/block','admin/structure/block','Blocks','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:79:\"Configure what block content appears in your site\'s sidebars and other regions.\";}}','system',0,0,1,0,0,3,0,1,21,30,0,0,0,0,0,0,0),('navigation',31,17,'user/%/cancel','user/%/cancel','Cancel account','a:0:{}','system',0,0,1,0,0,2,0,17,31,0,0,0,0,0,0,0,0),('management',33,11,'admin/dashboard/configure','admin/dashboard/configure','Configure available dashboard blocks','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:53:\"Configure which blocks can be shown on the dashboard.\";}}','system',-1,0,0,0,0,3,0,1,11,33,0,0,0,0,0,0,0),('management',34,9,'admin/content/node','admin/content/node','Content','a:0:{}','system',-1,0,0,0,-10,3,0,1,9,34,0,0,0,0,0,0,0),('management',35,8,'admin/config/content','admin/config/content','Content authoring','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:53:\"Settings related to formatting and authoring content.\";}}','system',0,0,1,0,-15,3,0,1,8,35,0,0,0,0,0,0,0),('management',36,21,'admin/structure/types','admin/structure/types','Content types','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:92:\"Manage content types, including default status, front page promotion, comment settings, etc.\";}}','system',0,0,1,0,0,3,0,1,21,36,0,0,0,0,0,0,0),('management',37,11,'admin/dashboard/customize','admin/dashboard/customize','Customize dashboard','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:25:\"Customize your dashboard.\";}}','system',-1,0,0,0,0,3,0,1,11,37,0,0,0,0,0,0,0),('navigation',38,5,'node/%/delete','node/%/delete','Delete','a:0:{}','system',-1,0,0,0,1,2,0,5,38,0,0,0,0,0,0,0,0),('management',39,8,'admin/config/development','admin/config/development','Development','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:18:\"Development tools.\";}}','system',0,0,1,0,-10,3,0,1,8,39,0,0,0,0,0,0,0),('navigation',40,17,'user/%/edit','user/%/edit','Edit','a:0:{}','system',-1,0,0,0,0,2,0,17,40,0,0,0,0,0,0,0,0),('navigation',41,5,'node/%/edit','node/%/edit','Edit','a:0:{}','system',-1,0,0,0,0,2,0,5,41,0,0,0,0,0,0,0,0),('management',42,19,'admin/reports/fields','admin/reports/fields','Field list','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:39:\"Overview of fields on all entity types.\";}}','system',0,0,0,0,0,3,0,1,19,42,0,0,0,0,0,0,0),('management',43,7,'admin/appearance/list','admin/appearance/list','List','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:31:\"Select and configure your theme\";}}','system',-1,0,0,0,-1,3,0,1,7,43,0,0,0,0,0,0,0),('management',44,16,'admin/modules/list','admin/modules/list','List','a:0:{}','system',-1,0,0,0,0,3,0,1,16,44,0,0,0,0,0,0,0),('management',45,18,'admin/people/people','admin/people/people','List','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:50:\"Find and manage people interacting with your site.\";}}','system',-1,0,0,0,-10,3,0,1,18,45,0,0,0,0,0,0,0),('management',46,8,'admin/config/media','admin/config/media','Media','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:12:\"Media tools.\";}}','system',0,0,1,0,-10,3,0,1,8,46,0,0,0,0,0,0,0),('management',47,21,'admin/structure/menu','admin/structure/menu','Menus','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:86:\"Add new menus to your site, edit existing menus, and rename and reorganize menu links.\";}}','system',0,0,1,0,0,3,0,1,21,47,0,0,0,0,0,0,0),('management',48,8,'admin/config/people','admin/config/people','People','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:24:\"Configure user accounts.\";}}','system',0,0,1,0,-20,3,0,1,8,48,0,0,0,0,0,0,0),('management',49,18,'admin/people/permissions','admin/people/permissions','Permissions','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:64:\"Determine access to features by selecting permissions for roles.\";}}','system',-1,0,0,0,0,3,0,1,18,49,0,0,0,0,0,0,0),('management',50,19,'admin/reports/dblog','admin/reports/dblog','Recent log messages','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:43:\"View events that have recently been logged.\";}}','system',0,0,0,0,-1,3,0,1,19,50,0,0,0,0,0,0,0),('management',51,8,'admin/config/regional','admin/config/regional','Regional and language','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:48:\"Regional settings, localization and translation.\";}}','system',0,0,1,0,-5,3,0,1,8,51,0,0,0,0,0,0,0),('navigation',52,5,'node/%/revisions','node/%/revisions','Revisions','a:0:{}','system',-1,0,1,0,2,2,0,5,52,0,0,0,0,0,0,0,0),('management',53,8,'admin/config/search','admin/config/search','Search and metadata','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:36:\"Local site search, metadata and SEO.\";}}','system',0,0,1,0,-10,3,0,1,8,53,0,0,0,0,0,0,0),('management',54,7,'admin/appearance/settings','admin/appearance/settings','Settings','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:46:\"Configure default and theme specific settings.\";}}','system',-1,0,0,0,20,3,0,1,7,54,0,0,0,0,0,0,0),('management',55,19,'admin/reports/status','admin/reports/status','Status report','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:74:\"Get a status report about your site\'s operation and any detected problems.\";}}','system',0,0,0,0,-60,3,0,1,19,55,0,0,0,0,0,0,0),('management',56,8,'admin/config/system','admin/config/system','System','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:37:\"General system related configuration.\";}}','system',0,0,1,0,-20,3,0,1,8,56,0,0,0,0,0,0,0),('management',57,21,'admin/structure/taxonomy','admin/structure/taxonomy','Taxonomy','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:67:\"Manage tagging, categorization, and classification of your content.\";}}','system',0,0,1,0,0,3,0,1,21,57,0,0,0,0,0,0,0),('management',58,19,'admin/reports/access-denied','admin/reports/access-denied','Top \'access denied\' errors','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:35:\"View \'access denied\' errors (403s).\";}}','system',0,0,0,0,0,3,0,1,19,58,0,0,0,0,0,0,0),('management',59,19,'admin/reports/page-not-found','admin/reports/page-not-found','Top \'page not found\' errors','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:36:\"View \'page not found\' errors (404s).\";}}','system',0,0,0,0,0,3,0,1,19,59,0,0,0,0,0,0,0),('management',60,16,'admin/modules/uninstall','admin/modules/uninstall','Uninstall','a:0:{}','system',-1,0,0,0,20,3,0,1,16,60,0,0,0,0,0,0,0),('management',61,8,'admin/config/user-interface','admin/config/user-interface','User interface','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:38:\"Tools that enhance the user interface.\";}}','system',0,0,1,0,-15,3,0,1,8,61,0,0,0,0,0,0,0),('navigation',62,5,'node/%/view','node/%/view','View','a:0:{}','system',-1,0,0,0,-10,2,0,5,62,0,0,0,0,0,0,0,0),('navigation',63,17,'user/%/view','user/%/view','View','a:0:{}','system',-1,0,0,0,-10,2,0,17,63,0,0,0,0,0,0,0,0),('management',64,8,'admin/config/services','admin/config/services','Web services','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:30:\"Tools related to web services.\";}}','system',0,0,1,0,0,3,0,1,8,64,0,0,0,0,0,0,0),('management',65,8,'admin/config/workflow','admin/config/workflow','Workflow','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:43:\"Content workflow, editorial workflow tools.\";}}','system',0,0,0,0,5,3,0,1,8,65,0,0,0,0,0,0,0),('management',66,12,'admin/help/block','admin/help/block','block','a:0:{}','system',-1,0,0,0,0,3,0,1,12,66,0,0,0,0,0,0,0),('management',67,12,'admin/help/color','admin/help/color','color','a:0:{}','system',-1,0,0,0,0,3,0,1,12,67,0,0,0,0,0,0,0),('management',69,12,'admin/help/contextual','admin/help/contextual','contextual','a:0:{}','system',-1,0,0,0,0,3,0,1,12,69,0,0,0,0,0,0,0),('management',70,12,'admin/help/dashboard','admin/help/dashboard','dashboard','a:0:{}','system',-1,0,0,0,0,3,0,1,12,70,0,0,0,0,0,0,0),('management',71,12,'admin/help/dblog','admin/help/dblog','dblog','a:0:{}','system',-1,0,0,0,0,3,0,1,12,71,0,0,0,0,0,0,0),('management',72,12,'admin/help/field','admin/help/field','field','a:0:{}','system',-1,0,0,0,0,3,0,1,12,72,0,0,0,0,0,0,0),('management',73,12,'admin/help/field_sql_storage','admin/help/field_sql_storage','field_sql_storage','a:0:{}','system',-1,0,0,0,0,3,0,1,12,73,0,0,0,0,0,0,0),('management',74,12,'admin/help/field_ui','admin/help/field_ui','field_ui','a:0:{}','system',-1,0,0,0,0,3,0,1,12,74,0,0,0,0,0,0,0),('management',75,12,'admin/help/file','admin/help/file','file','a:0:{}','system',-1,0,0,0,0,3,0,1,12,75,0,0,0,0,0,0,0),('management',76,12,'admin/help/filter','admin/help/filter','filter','a:0:{}','system',-1,0,0,0,0,3,0,1,12,76,0,0,0,0,0,0,0),('management',77,12,'admin/help/help','admin/help/help','help','a:0:{}','system',-1,0,0,0,0,3,0,1,12,77,0,0,0,0,0,0,0),('management',78,12,'admin/help/image','admin/help/image','image','a:0:{}','system',-1,0,0,0,0,3,0,1,12,78,0,0,0,0,0,0,0),('management',79,12,'admin/help/list','admin/help/list','list','a:0:{}','system',-1,0,0,0,0,3,0,1,12,79,0,0,0,0,0,0,0),('management',80,12,'admin/help/menu','admin/help/menu','menu','a:0:{}','system',-1,0,0,0,0,3,0,1,12,80,0,0,0,0,0,0,0),('management',81,12,'admin/help/node','admin/help/node','node','a:0:{}','system',-1,0,0,0,0,3,0,1,12,81,0,0,0,0,0,0,0),('management',82,12,'admin/help/options','admin/help/options','options','a:0:{}','system',-1,0,0,0,0,3,0,1,12,82,0,0,0,0,0,0,0),('management',83,12,'admin/help/system','admin/help/system','system','a:0:{}','system',-1,0,0,0,0,3,0,1,12,83,0,0,0,0,0,0,0),('management',84,12,'admin/help/taxonomy','admin/help/taxonomy','taxonomy','a:0:{}','system',-1,0,0,0,0,3,0,1,12,84,0,0,0,0,0,0,0),('management',85,12,'admin/help/text','admin/help/text','text','a:0:{}','system',-1,0,0,0,0,3,0,1,12,85,0,0,0,0,0,0,0),('management',86,12,'admin/help/user','admin/help/user','user','a:0:{}','system',-1,0,0,0,0,3,0,1,12,86,0,0,0,0,0,0,0),('navigation',87,27,'taxonomy/term/%/edit','taxonomy/term/%/edit','Edit','a:0:{}','system',-1,0,0,0,10,2,0,27,87,0,0,0,0,0,0,0,0),('navigation',88,27,'taxonomy/term/%/view','taxonomy/term/%/view','View','a:0:{}','system',-1,0,0,0,0,2,0,27,88,0,0,0,0,0,0,0,0),('management',89,48,'admin/config/people/accounts','admin/config/people/accounts','Account settings','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:109:\"Configure default behavior of users, including registration requirements, e-mails, fields, and user pictures.\";}}','system',0,0,0,0,-10,4,0,1,8,48,89,0,0,0,0,0,0),('management',90,56,'admin/config/system/actions','admin/config/system/actions','Actions','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:41:\"Manage the actions defined for your site.\";}}','system',0,0,1,0,0,4,0,1,8,56,90,0,0,0,0,0,0),('management',91,30,'admin/structure/block/add','admin/structure/block/add','Add block','a:0:{}','system',-1,0,0,0,0,4,0,1,21,30,91,0,0,0,0,0,0),('management',92,36,'admin/structure/types/add','admin/structure/types/add','Add content type','a:0:{}','system',-1,0,0,0,0,4,0,1,21,36,92,0,0,0,0,0,0),('management',93,47,'admin/structure/menu/add','admin/structure/menu/add','Add menu','a:0:{}','system',-1,0,0,0,0,4,0,1,21,47,93,0,0,0,0,0,0),('management',94,57,'admin/structure/taxonomy/add','admin/structure/taxonomy/add','Add vocabulary','a:0:{}','system',-1,0,0,0,0,4,0,1,21,57,94,0,0,0,0,0,0),('management',95,54,'admin/appearance/settings/bartik','admin/appearance/settings/bartik','Bartik','a:0:{}','system',-1,0,0,0,0,4,0,1,7,54,95,0,0,0,0,0,0),('management',96,53,'admin/config/search/clean-urls','admin/config/search/clean-urls','Clean URLs','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:43:\"Enable or disable clean URLs for your site.\";}}','system',0,0,0,0,5,4,0,1,8,53,96,0,0,0,0,0,0),('management',97,56,'admin/config/system/cron','admin/config/system/cron','Cron','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:40:\"Manage automatic site maintenance tasks.\";}}','system',0,0,0,0,20,4,0,1,8,56,97,0,0,0,0,0,0),('management',98,51,'admin/config/regional/date-time','admin/config/regional/date-time','Date and time','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:44:\"Configure display formats for date and time.\";}}','system',0,0,0,0,-15,4,0,1,8,51,98,0,0,0,0,0,0),('management',99,19,'admin/reports/event/%','admin/reports/event/%','Details','a:0:{}','system',0,0,0,0,0,3,0,1,19,99,0,0,0,0,0,0,0),('management',100,46,'admin/config/media/file-system','admin/config/media/file-system','File system','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:68:\"Tell Drupal where to store uploaded files and how they are accessed.\";}}','system',0,0,0,0,-10,4,0,1,8,46,100,0,0,0,0,0,0),('management',101,54,'admin/appearance/settings/garland','admin/appearance/settings/garland','Garland','a:0:{}','system',-1,0,0,0,0,4,0,1,7,54,101,0,0,0,0,0,0),('management',102,54,'admin/appearance/settings/global','admin/appearance/settings/global','Global settings','a:0:{}','system',-1,0,0,0,-1,4,0,1,7,54,102,0,0,0,0,0,0),('management',103,48,'admin/config/people/ip-blocking','admin/config/people/ip-blocking','IP address blocking','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:28:\"Manage blocked IP addresses.\";}}','system',0,0,1,0,10,4,0,1,8,48,103,0,0,0,0,0,0),('management',104,46,'admin/config/media/image-styles','admin/config/media/image-styles','Image styles','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:78:\"Configure styles that can be used for resizing or adjusting images on display.\";}}','system',0,0,1,0,0,4,0,1,8,46,104,0,0,0,0,0,0),('management',105,46,'admin/config/media/image-toolkit','admin/config/media/image-toolkit','Image toolkit','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:74:\"Choose which image toolkit to use if you have installed optional toolkits.\";}}','system',0,0,0,0,20,4,0,1,8,46,105,0,0,0,0,0,0),('management',106,36,'admin/structure/types/list','admin/structure/types/list','List','a:0:{}','system',-1,0,0,0,-10,4,0,1,21,36,106,0,0,0,0,0,0),('management',107,44,'admin/modules/list/confirm','admin/modules/list/confirm','List','a:0:{}','system',-1,0,0,0,0,4,0,1,16,44,107,0,0,0,0,0,0),('management',108,57,'admin/structure/taxonomy/list','admin/structure/taxonomy/list','List','a:0:{}','system',-1,0,0,0,-10,4,0,1,21,57,108,0,0,0,0,0,0),('management',109,47,'admin/structure/menu/list','admin/structure/menu/list','List menus','a:0:{}','system',-1,0,0,0,-10,4,0,1,21,47,109,0,0,0,0,0,0),('management',110,39,'admin/config/development/logging','admin/config/development/logging','Logging and errors','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:154:\"Settings for logging and alerts modules. Various modules can route Drupal\'s system events to different destinations, such as syslog, database, email, etc.\";}}','system',0,0,0,0,-15,4,0,1,8,39,110,0,0,0,0,0,0),('management',111,39,'admin/config/development/maintenance','admin/config/development/maintenance','Maintenance mode','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:62:\"Take the site offline for maintenance or bring it back online.\";}}','system',0,0,0,0,-10,4,0,1,8,39,111,0,0,0,0,0,0),('management',112,39,'admin/config/development/performance','admin/config/development/performance','Performance','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:101:\"Enable or disable page caching for anonymous users and set CSS and JS bandwidth optimization options.\";}}','system',0,0,0,0,-20,4,0,1,8,39,112,0,0,0,0,0,0),('management',113,49,'admin/people/permissions/list','admin/people/permissions/list','Permissions','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:64:\"Determine access to features by selecting permissions for roles.\";}}','system',-1,0,0,0,-8,4,0,1,18,49,113,0,0,0,0,0,0),('management',115,64,'admin/config/services/rss-publishing','admin/config/services/rss-publishing','RSS publishing','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:114:\"Configure the site description, the number of items per feed and whether feeds should be titles/teasers/full-text.\";}}','system',0,0,0,0,0,4,0,1,8,64,115,0,0,0,0,0,0),('management',116,51,'admin/config/regional/settings','admin/config/regional/settings','Regional settings','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:54:\"Settings for the site\'s default time zone and country.\";}}','system',0,0,0,0,-20,4,0,1,8,51,116,0,0,0,0,0,0),('management',117,49,'admin/people/permissions/roles','admin/people/permissions/roles','Roles','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:30:\"List, edit, or add user roles.\";}}','system',-1,0,1,0,-5,4,0,1,18,49,117,0,0,0,0,0,0),('management',118,47,'admin/structure/menu/settings','admin/structure/menu/settings','Settings','a:0:{}','system',-1,0,0,0,5,4,0,1,21,47,118,0,0,0,0,0,0),('management',119,54,'admin/appearance/settings/seven','admin/appearance/settings/seven','Seven','a:0:{}','system',-1,0,0,0,0,4,0,1,7,54,119,0,0,0,0,0,0),('management',120,56,'admin/config/system/site-information','admin/config/system/site-information','Site information','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:104:\"Change site name, e-mail address, slogan, default front page, and number of posts per page, error pages.\";}}','system',0,0,0,0,-20,4,0,1,8,56,120,0,0,0,0,0,0),('management',121,54,'admin/appearance/settings/stark','admin/appearance/settings/stark','Stark','a:0:{}','system',-1,0,0,0,0,4,0,1,7,54,121,0,0,0,0,0,0),('management',122,35,'admin/config/content/formats','admin/config/content/formats','Text formats','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:127:\"Configure how content input by users is filtered, including allowed HTML tags. Also allows enabling of module-provided filters.\";}}','system',0,0,1,0,0,4,0,1,8,35,122,0,0,0,0,0,0),('management',123,60,'admin/modules/uninstall/confirm','admin/modules/uninstall/confirm','Uninstall','a:0:{}','system',-1,0,0,0,0,4,0,1,16,60,123,0,0,0,0,0,0),('management',124,57,'admin/structure/taxonomy/%','admin/structure/taxonomy/%','','a:0:{}','system',0,0,0,0,0,4,0,1,21,57,124,0,0,0,0,0,0),('navigation',126,40,'user/%/edit/account','user/%/edit/account','Account','a:0:{}','system',-1,0,0,0,0,3,0,17,40,126,0,0,0,0,0,0,0),('management',127,122,'admin/config/content/formats/%','admin/config/content/formats/%','','a:0:{}','system',0,0,1,0,0,5,0,1,8,35,122,127,0,0,0,0,0),('management',128,104,'admin/config/media/image-styles/add','admin/config/media/image-styles/add','Add style','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:22:\"Add a new image style.\";}}','system',-1,0,0,0,2,5,0,1,8,46,104,128,0,0,0,0,0),('management',129,124,'admin/structure/taxonomy/%/add','admin/structure/taxonomy/%/add','Add term','a:0:{}','system',-1,0,0,0,0,5,0,1,21,57,124,129,0,0,0,0,0),('management',130,122,'admin/config/content/formats/add','admin/config/content/formats/add','Add text format','a:0:{}','system',-1,0,0,0,1,5,0,1,8,35,122,130,0,0,0,0,0),('management',131,30,'admin/structure/block/list/bartik','admin/structure/block/list/bartik','Bartik','a:0:{}','system',-1,0,0,0,0,4,0,1,21,30,131,0,0,0,0,0,0),('management',132,90,'admin/config/system/actions/configure','admin/config/system/actions/configure','Configure an advanced action','a:0:{}','system',-1,0,0,0,0,5,0,1,8,56,90,132,0,0,0,0,0),('management',133,47,'admin/structure/menu/manage/%','admin/structure/menu/manage/%','Customize menu','a:0:{}','system',0,0,1,0,0,4,0,1,21,47,133,0,0,0,0,0,0),('management',134,124,'admin/structure/taxonomy/%/edit','admin/structure/taxonomy/%/edit','Edit','a:0:{}','system',-1,0,0,0,-10,5,0,1,21,57,124,134,0,0,0,0,0),('management',135,36,'admin/structure/types/manage/%','admin/structure/types/manage/%','Edit content type','a:0:{}','system',0,0,1,0,0,4,0,1,21,36,135,0,0,0,0,0,0),('management',136,98,'admin/config/regional/date-time/formats','admin/config/regional/date-time/formats','Formats','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:51:\"Configure display format strings for date and time.\";}}','system',-1,0,1,0,-9,5,0,1,8,51,98,136,0,0,0,0,0),('management',137,30,'admin/structure/block/list/garland','admin/structure/block/list/garland','Garland','a:0:{}','system',-1,0,0,0,0,4,0,1,21,30,137,0,0,0,0,0,0),('management',138,124,'admin/structure/taxonomy/%/list','admin/structure/taxonomy/%/list','List','a:0:{}','system',-1,0,0,0,-20,5,0,1,21,57,124,138,0,0,0,0,0),('management',139,122,'admin/config/content/formats/list','admin/config/content/formats/list','List','a:0:{}','system',-1,0,0,0,0,5,0,1,8,35,122,139,0,0,0,0,0),('management',140,104,'admin/config/media/image-styles/list','admin/config/media/image-styles/list','List','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:42:\"List the current image styles on the site.\";}}','system',-1,0,0,0,1,5,0,1,8,46,104,140,0,0,0,0,0),('management',141,90,'admin/config/system/actions/manage','admin/config/system/actions/manage','Manage actions','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:41:\"Manage the actions defined for your site.\";}}','system',-1,0,0,0,-2,5,0,1,8,56,90,141,0,0,0,0,0),('management',142,89,'admin/config/people/accounts/settings','admin/config/people/accounts/settings','Settings','a:0:{}','system',-1,0,0,0,-10,5,0,1,8,48,89,142,0,0,0,0,0),('management',143,30,'admin/structure/block/list/seven','admin/structure/block/list/seven','Seven','a:0:{}','system',-1,0,0,0,0,4,0,1,21,30,143,0,0,0,0,0,0),('management',144,30,'admin/structure/block/list/stark','admin/structure/block/list/stark','Stark','a:0:{}','system',-1,0,0,0,0,4,0,1,21,30,144,0,0,0,0,0,0),('management',145,98,'admin/config/regional/date-time/types','admin/config/regional/date-time/types','Types','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:44:\"Configure display formats for date and time.\";}}','system',-1,0,1,0,-10,5,0,1,8,51,98,145,0,0,0,0,0),('navigation',146,52,'node/%/revisions/%/delete','node/%/revisions/%/delete','Delete earlier revision','a:0:{}','system',0,0,0,0,0,3,0,5,52,146,0,0,0,0,0,0,0),('navigation',147,52,'node/%/revisions/%/revert','node/%/revisions/%/revert','Revert to earlier revision','a:0:{}','system',0,0,0,0,0,3,0,5,52,147,0,0,0,0,0,0,0),('navigation',148,52,'node/%/revisions/%/view','node/%/revisions/%/view','Revisions','a:0:{}','system',0,0,0,0,0,3,0,5,52,148,0,0,0,0,0,0,0),('management',151,144,'admin/structure/block/list/stark/add','admin/structure/block/list/stark/add','Add block','a:0:{}','system',-1,0,0,0,0,5,0,1,21,30,144,151,0,0,0,0,0),('management',152,145,'admin/config/regional/date-time/types/add','admin/config/regional/date-time/types/add','Add date type','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:18:\"Add new date type.\";}}','system',-1,0,0,0,-10,6,0,1,8,51,98,145,152,0,0,0,0),('management',153,136,'admin/config/regional/date-time/formats/add','admin/config/regional/date-time/formats/add','Add format','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:43:\"Allow users to add additional date formats.\";}}','system',-1,0,0,0,-10,6,0,1,8,51,98,136,153,0,0,0,0),('management',154,133,'admin/structure/menu/manage/%/add','admin/structure/menu/manage/%/add','Add link','a:0:{}','system',-1,0,0,0,0,5,0,1,21,47,133,154,0,0,0,0,0),('management',155,30,'admin/structure/block/manage/%/%','admin/structure/block/manage/%/%','Configure block','a:0:{}','system',0,0,0,0,0,4,0,1,21,30,155,0,0,0,0,0,0),('navigation',156,31,'user/%/cancel/confirm/%/%','user/%/cancel/confirm/%/%','Confirm account cancellation','a:0:{}','system',0,0,0,0,0,3,0,17,31,156,0,0,0,0,0,0,0),('management',157,135,'admin/structure/types/manage/%/delete','admin/structure/types/manage/%/delete','Delete','a:0:{}','system',0,0,0,0,0,5,0,1,21,36,135,157,0,0,0,0,0),('management',158,103,'admin/config/people/ip-blocking/delete/%','admin/config/people/ip-blocking/delete/%','Delete IP address','a:0:{}','system',0,0,0,0,0,5,0,1,8,48,103,158,0,0,0,0,0),('management',159,90,'admin/config/system/actions/delete/%','admin/config/system/actions/delete/%','Delete action','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:17:\"Delete an action.\";}}','system',0,0,0,0,0,5,0,1,8,56,90,159,0,0,0,0,0),('management',160,133,'admin/structure/menu/manage/%/delete','admin/structure/menu/manage/%/delete','Delete menu','a:0:{}','system',0,0,0,0,0,5,0,1,21,47,133,160,0,0,0,0,0),('management',161,47,'admin/structure/menu/item/%/delete','admin/structure/menu/item/%/delete','Delete menu link','a:0:{}','system',0,0,0,0,0,4,0,1,21,47,161,0,0,0,0,0,0),('management',162,117,'admin/people/permissions/roles/delete/%','admin/people/permissions/roles/delete/%','Delete role','a:0:{}','system',0,0,0,0,0,5,0,1,18,49,117,162,0,0,0,0,0),('management',163,127,'admin/config/content/formats/%/disable','admin/config/content/formats/%/disable','Disable text format','a:0:{}','system',0,0,0,0,0,6,0,1,8,35,122,127,163,0,0,0,0),('management',164,135,'admin/structure/types/manage/%/edit','admin/structure/types/manage/%/edit','Edit','a:0:{}','system',-1,0,0,0,0,5,0,1,21,36,135,164,0,0,0,0,0),('management',165,133,'admin/structure/menu/manage/%/edit','admin/structure/menu/manage/%/edit','Edit menu','a:0:{}','system',-1,0,0,0,0,5,0,1,21,47,133,165,0,0,0,0,0),('management',166,47,'admin/structure/menu/item/%/edit','admin/structure/menu/item/%/edit','Edit menu link','a:0:{}','system',0,0,0,0,0,4,0,1,21,47,166,0,0,0,0,0,0),('management',167,117,'admin/people/permissions/roles/edit/%','admin/people/permissions/roles/edit/%','Edit role','a:0:{}','system',0,0,0,0,0,5,0,1,18,49,117,167,0,0,0,0,0),('management',168,104,'admin/config/media/image-styles/edit/%','admin/config/media/image-styles/edit/%','Edit style','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:25:\"Configure an image style.\";}}','system',0,0,1,0,0,5,0,1,8,46,104,168,0,0,0,0,0),('management',169,133,'admin/structure/menu/manage/%/list','admin/structure/menu/manage/%/list','List links','a:0:{}','system',-1,0,0,0,-10,5,0,1,21,47,133,169,0,0,0,0,0),('management',170,47,'admin/structure/menu/item/%/reset','admin/structure/menu/item/%/reset','Reset menu link','a:0:{}','system',0,0,0,0,0,4,0,1,21,47,170,0,0,0,0,0,0),('management',171,104,'admin/config/media/image-styles/delete/%','admin/config/media/image-styles/delete/%','Delete style','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:22:\"Delete an image style.\";}}','system',0,0,0,0,0,5,0,1,8,46,104,171,0,0,0,0,0),('management',172,104,'admin/config/media/image-styles/revert/%','admin/config/media/image-styles/revert/%','Revert style','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:22:\"Revert an image style.\";}}','system',0,0,0,0,0,5,0,1,8,46,104,172,0,0,0,0,0),('management',175,155,'admin/structure/block/manage/%/%/configure','admin/structure/block/manage/%/%/configure','Configure block','a:0:{}','system',-1,0,0,0,0,5,0,1,21,30,155,175,0,0,0,0,0),('management',176,155,'admin/structure/block/manage/%/%/delete','admin/structure/block/manage/%/%/delete','Delete block','a:0:{}','system',-1,0,0,0,0,5,0,1,21,30,155,176,0,0,0,0,0),('management',177,136,'admin/config/regional/date-time/formats/%/delete','admin/config/regional/date-time/formats/%/delete','Delete date format','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:47:\"Allow users to delete a configured date format.\";}}','system',0,0,0,0,0,6,0,1,8,51,98,136,177,0,0,0,0),('management',178,145,'admin/config/regional/date-time/types/%/delete','admin/config/regional/date-time/types/%/delete','Delete date type','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:45:\"Allow users to delete a configured date type.\";}}','system',0,0,0,0,0,6,0,1,8,51,98,145,178,0,0,0,0),('management',179,136,'admin/config/regional/date-time/formats/%/edit','admin/config/regional/date-time/formats/%/edit','Edit date format','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:45:\"Allow users to edit a configured date format.\";}}','system',0,0,0,0,0,6,0,1,8,51,98,136,179,0,0,0,0),('management',180,168,'admin/config/media/image-styles/edit/%/add/%','admin/config/media/image-styles/edit/%/add/%','Add image effect','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:28:\"Add a new effect to a style.\";}}','system',0,0,0,0,0,6,0,1,8,46,104,168,180,0,0,0,0),('management',181,168,'admin/config/media/image-styles/edit/%/effects/%','admin/config/media/image-styles/edit/%/effects/%','Edit image effect','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:39:\"Edit an existing effect within a style.\";}}','system',0,0,1,0,0,6,0,1,8,46,104,168,181,0,0,0,0),('management',182,181,'admin/config/media/image-styles/edit/%/effects/%/delete','admin/config/media/image-styles/edit/%/effects/%/delete','Delete image effect','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:39:\"Delete an existing effect from a style.\";}}','system',0,0,0,0,0,7,0,1,8,46,104,168,181,182,0,0,0),('management',183,47,'admin/structure/menu/manage/main-menu','admin/structure/menu/manage/%','Main menu','a:0:{}','menu',0,0,0,0,0,4,0,1,21,47,183,0,0,0,0,0,0),('management',184,47,'admin/structure/menu/manage/management','admin/structure/menu/manage/%','Management','a:0:{}','menu',0,0,0,0,0,4,0,1,21,47,184,0,0,0,0,0,0),('management',185,47,'admin/structure/menu/manage/navigation','admin/structure/menu/manage/%','Navigation','a:0:{}','menu',0,0,0,0,0,4,0,1,21,47,185,0,0,0,0,0,0),('management',186,47,'admin/structure/menu/manage/user-menu','admin/structure/menu/manage/%','User menu','a:0:{}','menu',0,0,0,0,0,4,0,1,21,47,186,0,0,0,0,0,0),('navigation',191,17,'user/%/shortcuts','user/%/shortcuts','Shortcuts','a:0:{}','system',-1,0,0,0,0,2,0,17,191,0,0,0,0,0,0,0,0),('management',194,12,'admin/help/number','admin/help/number','number','a:0:{}','system',-1,0,0,0,0,3,0,1,12,194,0,0,0,0,0,0,0),('management',195,12,'admin/help/overlay','admin/help/overlay','overlay','a:0:{}','system',-1,0,0,0,0,3,0,1,12,195,0,0,0,0,0,0,0),('management',196,12,'admin/help/path','admin/help/path','path','a:0:{}','system',-1,0,0,0,0,3,0,1,12,196,0,0,0,0,0,0,0),('management',197,12,'admin/help/rdf','admin/help/rdf','rdf','a:0:{}','system',-1,0,0,0,0,3,0,1,12,197,0,0,0,0,0,0,0),('management',199,12,'admin/help/shortcut','admin/help/shortcut','shortcut','a:0:{}','system',-1,0,0,0,0,3,0,1,12,199,0,0,0,0,0,0,0),('management',201,61,'admin/config/user-interface/shortcut','admin/config/user-interface/shortcut','Shortcuts','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:29:\"Add and modify shortcut sets.\";}}','system',0,0,1,0,0,4,0,1,8,61,201,0,0,0,0,0,0),('management',202,53,'admin/config/search/path','admin/config/search/path','URL aliases','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:46:\"Change your site\'s URL paths by aliasing them.\";}}','system',0,0,1,0,-5,4,0,1,8,53,202,0,0,0,0,0,0),('management',203,202,'admin/config/search/path/add','admin/config/search/path/add','Add alias','a:0:{}','system',-1,0,0,0,0,5,0,1,8,53,202,203,0,0,0,0,0),('management',204,201,'admin/config/user-interface/shortcut/add-set','admin/config/user-interface/shortcut/add-set','Add shortcut set','a:0:{}','system',-1,0,0,0,0,5,0,1,8,61,201,204,0,0,0,0,0),('management',206,201,'admin/config/user-interface/shortcut/%','admin/config/user-interface/shortcut/%','Edit shortcuts','a:0:{}','system',0,0,1,0,0,5,0,1,8,61,201,206,0,0,0,0,0),('management',207,202,'admin/config/search/path/list','admin/config/search/path/list','List','a:0:{}','system',-1,0,0,0,-10,5,0,1,8,53,202,207,0,0,0,0,0),('management',208,206,'admin/config/user-interface/shortcut/%/add-link','admin/config/user-interface/shortcut/%/add-link','Add shortcut','a:0:{}','system',-1,0,0,0,0,6,0,1,8,61,201,206,208,0,0,0,0),('management',209,202,'admin/config/search/path/delete/%','admin/config/search/path/delete/%','Delete alias','a:0:{}','system',0,0,0,0,0,5,0,1,8,53,202,209,0,0,0,0,0),('management',210,206,'admin/config/user-interface/shortcut/%/delete','admin/config/user-interface/shortcut/%/delete','Delete shortcut set','a:0:{}','system',0,0,0,0,0,6,0,1,8,61,201,206,210,0,0,0,0),('management',211,202,'admin/config/search/path/edit/%','admin/config/search/path/edit/%','Edit alias','a:0:{}','system',0,0,0,0,0,5,0,1,8,53,202,211,0,0,0,0,0),('management',212,206,'admin/config/user-interface/shortcut/%/edit','admin/config/user-interface/shortcut/%/edit','Edit set name','a:0:{}','system',-1,0,0,0,10,6,0,1,8,61,201,206,212,0,0,0,0),('management',213,201,'admin/config/user-interface/shortcut/link/%','admin/config/user-interface/shortcut/link/%','Edit shortcut','a:0:{}','system',0,0,1,0,0,5,0,1,8,61,201,213,0,0,0,0,0),('management',214,206,'admin/config/user-interface/shortcut/%/links','admin/config/user-interface/shortcut/%/links','List links','a:0:{}','system',-1,0,0,0,0,6,0,1,8,61,201,206,214,0,0,0,0),('management',215,213,'admin/config/user-interface/shortcut/link/%/delete','admin/config/user-interface/shortcut/link/%/delete','Delete shortcut','a:0:{}','system',0,0,0,0,0,6,0,1,8,61,201,213,215,0,0,0,0),('shortcut-set-1',216,0,'node/add','node/add','Add content','a:0:{}','menu',0,0,0,0,-20,1,0,216,0,0,0,0,0,0,0,0,0),('shortcut-set-1',217,0,'admin/content','admin/content','Find content','a:0:{}','menu',0,0,0,0,-19,1,0,217,0,0,0,0,0,0,0,0,0),('main-menu',218,0,'products','products','Products','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,1,0,-50,1,1,218,0,0,0,0,0,0,0,0,0),('navigation',219,6,'node/add/article','node/add/article','Article','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:89:\"Use <em>articles</em> for time-sensitive content like news, press releases or blog posts.\";}}','system',0,0,0,0,0,2,0,6,219,0,0,0,0,0,0,0,0),('navigation',220,6,'node/add/page','node/add/page','Basic page','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:77:\"Use <em>basic pages</em> for your static content, such as an \'About us\' page.\";}}','system',0,0,0,0,0,2,0,6,220,0,0,0,0,0,0,0,0),('management',221,12,'admin/help/toolbar','admin/help/toolbar','toolbar','a:0:{}','system',-1,0,0,0,0,3,0,1,12,221,0,0,0,0,0,0,0),('management',260,19,'admin/reports/updates','admin/reports/updates','Available updates','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:82:\"Get a status report about available updates for your installed modules and themes.\";}}','system',0,0,0,0,-50,3,0,1,19,260,0,0,0,0,0,0,0),('management',261,7,'admin/appearance/install','admin/appearance/install','Install new theme','a:0:{}','system',-1,0,0,0,25,3,0,1,7,261,0,0,0,0,0,0,0),('management',262,16,'admin/modules/update','admin/modules/update','Update','a:0:{}','system',-1,0,0,0,10,3,0,1,16,262,0,0,0,0,0,0,0),('management',263,16,'admin/modules/install','admin/modules/install','Install new module','a:0:{}','system',-1,0,0,0,25,3,0,1,16,263,0,0,0,0,0,0,0),('management',264,7,'admin/appearance/update','admin/appearance/update','Update','a:0:{}','system',-1,0,0,0,10,3,0,1,7,264,0,0,0,0,0,0,0),('management',265,12,'admin/help/update','admin/help/update','update','a:0:{}','system',-1,0,0,0,0,3,0,1,12,265,0,0,0,0,0,0,0),('management',266,260,'admin/reports/updates/install','admin/reports/updates/install','Install new module or theme','a:0:{}','system',-1,0,0,0,25,4,0,1,19,260,266,0,0,0,0,0,0),('management',267,260,'admin/reports/updates/update','admin/reports/updates/update','Update','a:0:{}','system',-1,0,0,0,10,4,0,1,19,260,267,0,0,0,0,0,0),('management',268,260,'admin/reports/updates/list','admin/reports/updates/list','List','a:0:{}','system',-1,0,0,0,0,4,0,1,19,260,268,0,0,0,0,0,0),('management',269,260,'admin/reports/updates/settings','admin/reports/updates/settings','Settings','a:0:{}','system',-1,0,0,0,50,4,0,1,19,260,269,0,0,0,0,0,0),('main-menu',312,1264,'node/2','node/%','About us - Alternative','a:1:{s:10:\"attributes\";a:0:{}}','menu',0,0,0,0,-50,2,1,1264,312,0,0,0,0,0,0,0,0),('management',314,47,'admin/structure/menu/manage/menu-footer-bottom-menu','admin/structure/menu/manage/%','Footer bottom menu','a:0:{}','menu',0,0,0,0,0,4,0,1,21,47,314,0,0,0,0,0,0),('menu-footer-bottom-menu',315,0,'blog','blog','Blog','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-46,1,1,315,0,0,0,0,0,0,0,0,0),('menu-footer-bottom-menu',316,0,'node/22','node/%','Contact','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-45,1,1,316,0,0,0,0,0,0,0,0,0),('main-menu',327,0,'contact','contact','Contact','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-45,1,1,327,0,0,0,0,0,0,0,0,0),('navigation',328,0,'blog','blog','Blogs','a:0:{}','system',1,0,1,0,0,1,0,328,0,0,0,0,0,0,0,0,0),('navigation',329,328,'blog/%','blog/%','My blog','a:0:{}','system',0,0,0,0,0,2,0,328,329,0,0,0,0,0,0,0,0),('navigation',330,6,'node/add/blog','node/add/blog','Blog entry','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:58:\"Use for multi-user blogs. Every user gets a personal blog.\";}}','system',0,0,0,0,0,2,0,6,330,0,0,0,0,0,0,0,0),('management',331,12,'admin/help/blog','admin/help/blog','blog','a:0:{}','system',-1,0,0,0,0,3,0,1,12,331,0,0,0,0,0,0,0),('main-menu',332,1808,'blog','blog','Blog - Default','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-50,3,1,895,1808,332,0,0,0,0,0,0,0),('navigation',333,6,'node/add/mt-testimonial','node/add/mt-testimonial','Testimonial','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:233:\"The Testimonial entry is perfect to showcase the words of love from your users and customers. Use it to easily publish testimonials on your site. A special View Block and a View Page are already configured to show them off perfectly.\";}}','system',0,0,0,0,0,2,0,6,333,0,0,0,0,0,0,0,0),('management',334,21,'admin/structure/views','admin/structure/views','Views','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:35:\"Manage customized lists of content.\";}}','system',0,0,1,0,0,3,0,1,21,334,0,0,0,0,0,0,0),('management',335,19,'admin/reports/views-plugins','admin/reports/views-plugins','Views plugins','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:38:\"Overview of plugins used in all views.\";}}','system',0,0,0,0,0,3,0,1,19,335,0,0,0,0,0,0,0),('management',336,334,'admin/structure/views/add','admin/structure/views/add','Add new view','a:0:{}','system',-1,0,0,0,0,4,0,1,21,334,336,0,0,0,0,0,0),('management',337,334,'admin/structure/views/add-template','admin/structure/views/add-template','Add view from template','a:0:{}','system',-1,0,0,0,0,4,0,1,21,334,337,0,0,0,0,0,0),('management',338,334,'admin/structure/views/import','admin/structure/views/import','Import','a:0:{}','system',-1,0,0,0,0,4,0,1,21,334,338,0,0,0,0,0,0),('management',339,334,'admin/structure/views/list','admin/structure/views/list','List','a:0:{}','system',-1,0,0,0,-10,4,0,1,21,334,339,0,0,0,0,0,0),('management',340,42,'admin/reports/fields/list','admin/reports/fields/list','List','a:0:{}','system',-1,0,0,0,-10,4,0,1,19,42,340,0,0,0,0,0,0),('management',341,334,'admin/structure/views/settings','admin/structure/views/settings','Settings','a:0:{}','system',-1,0,0,0,0,4,0,1,21,334,341,0,0,0,0,0,0),('management',342,42,'admin/reports/fields/views-fields','admin/reports/fields/views-fields','Used in views','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:37:\"Overview of fields used in all views.\";}}','system',-1,0,0,0,0,4,0,1,19,42,342,0,0,0,0,0,0),('management',343,341,'admin/structure/views/settings/advanced','admin/structure/views/settings/advanced','Advanced','a:0:{}','system',-1,0,0,0,1,5,0,1,21,334,341,343,0,0,0,0,0),('management',344,341,'admin/structure/views/settings/basic','admin/structure/views/settings/basic','Basic','a:0:{}','system',-1,0,0,0,0,5,0,1,21,334,341,344,0,0,0,0,0),('management',345,334,'admin/structure/views/view/%','admin/structure/views/view/%','','a:0:{}','system',0,0,0,0,0,4,0,1,21,334,345,0,0,0,0,0,0),('management',346,345,'admin/structure/views/view/%/break-lock','admin/structure/views/view/%/break-lock','Break lock','a:0:{}','system',-1,0,0,0,0,5,0,1,21,334,345,346,0,0,0,0,0),('management',347,345,'admin/structure/views/view/%/edit','admin/structure/views/view/%/edit','Edit view','a:0:{}','system',-1,0,0,0,-10,5,0,1,21,334,345,347,0,0,0,0,0),('management',348,345,'admin/structure/views/view/%/clone','admin/structure/views/view/%/clone','Clone','a:0:{}','system',-1,0,0,0,0,5,0,1,21,334,345,348,0,0,0,0,0),('management',349,345,'admin/structure/views/view/%/delete','admin/structure/views/view/%/delete','Delete','a:0:{}','system',-1,0,0,0,0,5,0,1,21,334,345,349,0,0,0,0,0),('management',350,345,'admin/structure/views/view/%/export','admin/structure/views/view/%/export','Export','a:0:{}','system',-1,0,0,0,0,5,0,1,21,334,345,350,0,0,0,0,0),('management',351,345,'admin/structure/views/view/%/revert','admin/structure/views/view/%/revert','Revert','a:0:{}','system',-1,0,0,0,0,5,0,1,21,334,345,351,0,0,0,0,0),('management',352,334,'admin/structure/views/nojs/preview/%/%','admin/structure/views/nojs/preview/%/%','','a:0:{}','system',0,0,0,0,0,4,0,1,21,334,352,0,0,0,0,0,0),('management',353,345,'admin/structure/views/view/%/preview/%','admin/structure/views/view/%/preview/%','','a:0:{}','system',-1,0,0,0,0,5,0,1,21,334,345,353,0,0,0,0,0),('management',354,334,'admin/structure/views/ajax/preview/%/%','admin/structure/views/ajax/preview/%/%','','a:0:{}','system',0,0,0,0,0,4,0,1,21,334,354,0,0,0,0,0,0),('navigation',356,6,'node/add/mt-slideshow-entry','node/add/mt-slideshow-entry','Slideshow entry','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:268:\"A Slideshow entry is ideal for creating commercial banners as well as messages for your website. Use it to promote any page of your website or URL into the front page slide show. It can carry a title, a teaser and an image linking to an internal path or external link.\";}}','system',0,0,0,0,0,2,0,6,356,0,0,0,0,0,0,0,0),('management',358,12,'admin/help/superfish','admin/help/superfish','superfish','a:0:{}','system',-1,0,0,0,0,3,0,1,12,358,0,0,0,0,0,0,0),('management',359,61,'admin/config/user-interface/superfish','admin/config/user-interface/superfish','Superfish','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:25:\"Configure Superfish Menus\";}}','system',0,0,0,0,0,4,0,1,8,61,359,0,0,0,0,0,0),('navigation',365,6,'node/add/mt-service','node/add/mt-service','Service','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:530:\"A Service post is ideal for creating and displaying services. It has the option to be displayed into front page slide show. It provides the ability to attach an image which is automatically adjusted to fit into the websites layout. What’s particularly cool about a Service post, is that it provides the ability to attach a series of images, thumbnails of which are automatically created and adjusted to fit into the website layout. Service post enables free tagging (just like labels), taking the bests from the taxonomy system.\";}}','system',0,0,0,0,0,2,0,6,365,0,0,0,0,0,0,0,0),('main-menu',366,0,'services','services','Services','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,1,0,-48,1,1,366,0,0,0,0,0,0,0,0,0),('main-menu',405,844,'node/8','node/%','Grid - No Sidebar','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:17:\"Grid - No sidebar\";}}','menu',0,0,0,0,-49,3,1,895,844,405,0,0,0,0,0,0,0),('main-menu',406,844,'node/9','node/%','Grid - With Sidebar','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:19:\"Grid (with sidebar)\";}}','menu',0,0,0,0,-48,3,1,895,844,406,0,0,0,0,0,0,0),('main-menu',407,895,'node/1','node/%','Typography','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:10:\"Typography\";}}','menu',0,0,0,0,-44,2,1,895,407,0,0,0,0,0,0,0,0),('main-menu',408,1657,'services','services','Services - 4 Columns','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:13:\"First Service\";}}','menu',0,0,0,0,-48,3,1,366,1657,408,0,0,0,0,0,0,0),('main-menu',409,1657,'services3','services3','Services - 3 Columns','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:20:\"Services (3 columns)\";}}','menu',0,0,1,0,-49,3,1,366,1657,409,0,0,0,0,0,0,0),('management',411,12,'admin/help/php','admin/help/php','php','a:0:{}','system',-1,0,0,0,0,3,0,1,12,411,0,0,0,0,0,0,0),('navigation',412,6,'node/add/mt-showcase','node/add/mt-showcase','Showcase','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:534:\"A Showcase post is ideal for creating and displaying showcases. It has the option to be displayed into front page slide show. It provides the ability to attach an image which is automatically adjusted to fit into the websites layout. What’s particularly cool about a Showcase post, is that it provides the ability to attach a series of images, thumbnails of which are automatically created and adjusted to fit into the website layout. Showcase post enables free tagging (just like labels), taking the bests from the taxonomy system.\";}}','system',0,0,0,0,0,2,0,6,412,0,0,0,0,0,0,0,0),('navigation',415,5,'node/%/webform-results','node/%/webform-results','Results','a:0:{}','system',-1,0,0,0,2,2,0,5,415,0,0,0,0,0,0,0,0),('navigation',416,6,'node/add/webform','node/add/webform','Webform','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:138:\"Create a new form or questionnaire accessible to users. Submission results and statistics are recorded and accessible to privileged users.\";}}','system',0,0,0,0,0,2,0,6,416,0,0,0,0,0,0,0,0),('navigation',417,5,'node/%/webform','node/%/webform','Webform','a:0:{}','system',-1,0,0,0,1,2,0,5,417,0,0,0,0,0,0,0,0),('management',418,9,'admin/content/webform','admin/content/webform','Webforms','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:54:\"View and edit all the available webforms on your site.\";}}','system',-1,0,0,0,0,3,0,1,9,418,0,0,0,0,0,0,0),('management',419,12,'admin/help/webform','admin/help/webform','webform','a:0:{}','system',-1,0,0,0,0,3,0,1,12,419,0,0,0,0,0,0,0),('navigation',420,415,'node/%/webform-results/analysis','node/%/webform-results/analysis','Analysis','a:0:{}','system',-1,0,0,0,5,3,0,5,415,420,0,0,0,0,0,0,0),('navigation',421,415,'node/%/webform-results/clear','node/%/webform-results/clear','Clear','a:0:{}','system',-1,0,0,0,8,3,0,5,415,421,0,0,0,0,0,0,0),('navigation',422,415,'node/%/webform-results/download','node/%/webform-results/download','Download','a:0:{}','system',-1,0,0,0,7,3,0,5,415,422,0,0,0,0,0,0,0),('navigation',423,417,'node/%/webform/emails','node/%/webform/emails','E-mails','a:0:{}','system',-1,0,0,0,4,3,0,5,417,423,0,0,0,0,0,0,0),('navigation',424,417,'node/%/webform/components','node/%/webform/components','Form components','a:0:{}','system',-1,0,0,0,0,3,0,5,417,424,0,0,0,0,0,0,0),('navigation',425,417,'node/%/webform/configure','node/%/webform/configure','Form settings','a:0:{}','system',-1,0,0,0,5,3,0,5,417,425,0,0,0,0,0,0,0),('navigation',426,415,'node/%/webform-results/submissions','node/%/webform-results/submissions','Submissions','a:0:{}','system',-1,0,0,0,4,3,0,5,415,426,0,0,0,0,0,0,0),('navigation',427,415,'node/%/webform-results/table','node/%/webform-results/table','Table','a:0:{}','system',-1,0,0,0,6,3,0,5,415,427,0,0,0,0,0,0,0),('management',428,35,'admin/config/content/webform','admin/config/content/webform','Webform settings','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:46:\"Global configuration of webform functionality.\";}}','system',0,0,0,0,0,4,0,1,8,35,428,0,0,0,0,0,0),('navigation',429,424,'node/%/webform/components/%','node/%/webform/components/%','','a:0:{}','system',-1,0,0,0,0,4,0,5,417,424,429,0,0,0,0,0,0),('navigation',430,5,'node/%/submission/%/delete','node/%/submission/%/delete','Delete','a:0:{}','system',-1,0,0,0,2,2,0,5,430,0,0,0,0,0,0,0,0),('navigation',431,5,'node/%/submission/%/edit','node/%/submission/%/edit','Edit','a:0:{}','system',-1,0,0,0,1,2,0,5,431,0,0,0,0,0,0,0,0),('navigation',432,5,'node/%/submission/%/view','node/%/submission/%/view','View','a:0:{}','system',-1,0,0,0,0,2,0,5,432,0,0,0,0,0,0,0,0),('navigation',433,429,'node/%/webform/components/%/delete','node/%/webform/components/%/delete','','a:0:{}','system',-1,0,0,0,0,5,0,5,417,424,429,433,0,0,0,0,0),('navigation',434,429,'node/%/webform/components/%/clone','node/%/webform/components/%/clone','','a:0:{}','system',-1,0,0,0,0,5,0,5,417,424,429,434,0,0,0,0,0),('main-menu',442,0,'node/55','node/%','Contact','a:1:{s:10:\"attributes\";a:0:{}}','menu',0,0,1,0,-45,1,1,442,0,0,0,0,0,0,0,0,0),('management',444,21,'admin/structure/bulk-export','admin/structure/bulk-export','Bulk Exporter','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:57:\"Bulk-export multiple CTools-handled data objects to code.\";}}','system',0,0,0,0,0,3,0,1,21,444,0,0,0,0,0,0,0),('menu-footer-bottom-menu',524,0,'node/2','node/%','About us','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-48,1,1,524,0,0,0,0,0,0,0,0,0),('menu-footer-bottom-menu',525,0,'services','services','Services','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-50,1,1,525,0,0,0,0,0,0,0,0,0),('menu-footer-bottom-menu',526,0,'showcase','showcase','Showcase','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-49,1,1,526,0,0,0,0,0,0,0,0,0),('main-menu',527,409,'node/10','node/%','First Service','a:1:{s:10:\"attributes\";a:0:{}}','menu',0,0,0,0,-50,4,1,366,1657,409,527,0,0,0,0,0,0),('main-menu',528,409,'node/12','node/%','Second Service','a:1:{s:10:\"attributes\";a:0:{}}','menu',0,0,1,0,-49,4,1,366,1657,409,528,0,0,0,0,0,0),('main-menu',529,409,'node/11','node/%','Third Service','a:1:{s:10:\"attributes\";a:0:{}}','menu',0,0,0,0,-48,4,1,366,1657,409,529,0,0,0,0,0,0),('main-menu',530,0,'showcase','showcase','Showcase','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,1,0,-46,1,1,530,0,0,0,0,0,0,0,0,0),('main-menu',531,1658,'showcase2','showcase2','Showcase - 2 Columns','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-50,3,1,530,1658,531,0,0,0,0,0,0,0),('main-menu',532,530,'showcase3','showcase3','Showcase (3 columns)','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-49,2,1,530,532,0,0,0,0,0,0,0,0),('main-menu',533,1658,'showcase4','showcase4','Showcase - 4 Columns','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-48,3,1,530,1658,533,0,0,0,0,0,0,0),('management',691,12,'admin/help/jquery_update','admin/help/jquery_update','jquery_update','a:0:{}','system',-1,0,0,0,0,3,0,1,12,691,0,0,0,0,0,0,0),('management',692,39,'admin/config/development/jquery_update','admin/config/development/jquery_update','jQuery update','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:83:\"Configure settings related to the jQuery upgrade, the library path and compression.\";}}','system',0,0,0,0,0,4,0,1,8,39,692,0,0,0,0,0,0),('main-menu',693,528,'node/10','node/%','Service X','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-49,5,1,366,1657,409,528,693,0,0,0,0,0),('management',694,64,'admin/config/services/oauth','admin/config/services/oauth','OAuth','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:18:\"Settings for OAuth\";}}','system',0,0,0,0,0,4,0,1,8,64,694,0,0,0,0,0,0),('management',695,64,'admin/config/services/twitter','admin/config/services/twitter','Twitter','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:30:\"Twitter accounts and settings.\";}}','system',0,0,0,0,0,4,0,1,8,64,695,0,0,0,0,0,0),('navigation',696,40,'user/%/edit/twitter','user/%/edit/twitter','Twitter accounts','a:0:{}','system',-1,0,0,0,10,3,0,17,40,696,0,0,0,0,0,0,0),('management',697,694,'admin/config/services/oauth/settings','admin/config/services/oauth/settings','Settings','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:18:\"Settings for OAuth\";}}','system',-1,0,0,0,0,5,0,1,8,64,694,697,0,0,0,0,0),('management',698,695,'admin/config/services/twitter/settings','admin/config/services/twitter/settings','Settings','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:17:\"Twitter settings.\";}}','system',-1,0,0,0,0,5,0,1,8,64,695,698,0,0,0,0,0),('management',699,695,'admin/config/services/twitter/default','admin/config/services/twitter/default','Twitter','a:0:{}','system',-1,0,0,0,0,5,0,1,8,64,695,699,0,0,0,0,0),('main-menu',700,1658,'showcase','showcase','Showcase - 3 Columns','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-49,3,1,530,1658,700,0,0,0,0,0,0,0),('navigation',831,0,'search','search','Search','a:0:{}','system',1,0,0,0,0,1,0,831,0,0,0,0,0,0,0,0,0),('navigation',832,831,'search/node','search/node','Content','a:0:{}','system',-1,0,0,0,-10,2,0,831,832,0,0,0,0,0,0,0,0),('navigation',833,831,'search/user','search/user','Users','a:0:{}','system',-1,0,0,0,0,2,0,831,833,0,0,0,0,0,0,0,0),('navigation',834,832,'search/node/%','search/node/%','Content','a:0:{}','system',-1,0,0,0,0,3,0,831,832,834,0,0,0,0,0,0,0),('management',835,19,'admin/reports/search','admin/reports/search','Top search phrases','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:33:\"View most popular search phrases.\";}}','system',0,0,0,0,0,3,0,1,19,835,0,0,0,0,0,0,0),('navigation',836,833,'search/user/%','search/user/%','Users','a:0:{}','system',-1,0,0,0,0,3,0,831,833,836,0,0,0,0,0,0,0),('management',837,12,'admin/help/search','admin/help/search','search','a:0:{}','system',-1,0,0,0,0,3,0,1,12,837,0,0,0,0,0,0,0),('management',838,53,'admin/config/search/settings','admin/config/search/settings','Search settings','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:67:\"Configure relevance settings for search and other indexing options.\";}}','system',0,0,0,0,-10,4,0,1,8,53,838,0,0,0,0,0,0),('management',839,838,'admin/config/search/settings/reindex','admin/config/search/settings/reindex','Clear index','a:0:{}','system',-1,0,0,0,0,5,0,1,8,53,838,839,0,0,0,0,0),('main-menu',842,895,'node/26','node/%','Shortcodes','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:10:\"Shortcodes\";}}','menu',0,0,1,0,-47,2,1,895,842,0,0,0,0,0,0,0,0),('main-menu',843,1657,'services2','services2','Services - 2 Columns','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-50,3,1,366,1657,843,0,0,0,0,0,0,0),('main-menu',844,895,'node/8','node/%','Responsive Grid','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:15:\"Responsive Grid\";}}','menu',0,0,1,0,-45,2,1,895,844,0,0,0,0,0,0,0,0),('main-menu',845,895,'node/10','node/%','Pages','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:5:\"Pages\";}}','menu',0,0,1,0,-46,2,1,895,845,0,0,0,0,0,0,0,0),('main-menu',846,845,'node/25','node/%','Full Width','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:10:\"Full Width\";}}','menu',0,0,0,0,-50,3,1,895,845,846,0,0,0,0,0,0,0),('main-menu',847,845,'node/12','node/%','One Sidebar','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:11:\"One Sidebar\";}}','menu',0,0,0,0,-49,3,1,895,845,847,0,0,0,0,0,0,0),('main-menu',848,845,'node/11','node/%','Two Sidebars','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:12:\"Two Sidebars\";}}','menu',0,0,0,0,-48,3,1,895,845,848,0,0,0,0,0,0,0),('main-menu',850,842,'node/26','node/%','Tabs','a:2:{s:8:\"fragment\";s:4:\"tabs\";s:10:\"attributes\";a:1:{s:5:\"title\";s:4:\"Tabs\";}}','menu',0,0,0,0,-48,3,1,895,842,850,0,0,0,0,0,0,0),('main-menu',851,842,'node/26','node/%','Accordion','a:2:{s:8:\"fragment\";s:9:\"accordion\";s:10:\"attributes\";a:1:{s:5:\"title\";s:9:\"Accordion\";}}','menu',0,0,0,0,-47,3,1,895,842,851,0,0,0,0,0,0,0),('main-menu',852,842,'node/26','node/%','Brands','a:2:{s:8:\"fragment\";s:6:\"brands\";s:10:\"attributes\";a:1:{s:5:\"title\";s:6:\"Brands\";}}','menu',0,0,0,0,-49,3,1,895,842,852,0,0,0,0,0,0,0),('main-menu',853,842,'node/26','node/%','Progress Bars','a:2:{s:8:\"fragment\";s:12:\"progressbars\";s:10:\"attributes\";a:1:{s:5:\"title\";s:13:\"Progress Bars\";}}','menu',0,0,0,0,-45,3,1,895,842,853,0,0,0,0,0,0,0),('main-menu',854,845,'node/15','node/%','Page with Comments','a:2:{s:8:\"fragment\";s:8:\"comments\";s:10:\"attributes\";a:1:{s:5:\"title\";s:18:\"Page with Comments\";}}','menu',0,0,0,0,-47,3,1,895,845,854,0,0,0,0,0,0,0),('main-menu',855,842,'node/26','node/%','Buttons','a:2:{s:8:\"fragment\";s:7:\"buttons\";s:10:\"attributes\";a:1:{s:5:\"title\";s:7:\"Buttons\";}}','menu',0,0,0,0,-46,3,1,895,842,855,0,0,0,0,0,0,0),('main-menu',895,0,'node/26','node/%','Features','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:8:\"Features\";}}','menu',0,0,1,0,-49,1,1,895,0,0,0,0,0,0,0,0,0),('menu-footer-bottom-menu',896,0,'node/26','node/%','Features','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:8:\"Features\";}}','menu',0,0,0,0,-47,1,1,896,0,0,0,0,0,0,0,0,0),('navigation',897,423,'node/%/webform/emails/%','node/%/webform/emails/%','','a:0:{}','system',-1,0,0,0,0,4,0,5,417,423,897,0,0,0,0,0,0),('navigation',898,897,'node/%/webform/emails/%/delete','node/%/webform/emails/%/delete','','a:0:{}','system',-1,0,0,0,0,5,0,5,417,423,897,898,0,0,0,0,0),('management',900,137,'admin/structure/block/list/garland/add','admin/structure/block/list/garland/add','Add block','a:0:{}','system',-1,0,0,0,0,5,0,1,21,30,137,900,0,0,0,0,0),('management',913,21,'admin/structure/quicktabs','admin/structure/quicktabs','Quicktabs','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:32:\"Create blocks of tabbed content.\";}}','system',0,0,1,0,0,3,0,1,21,913,0,0,0,0,0,0,0),('management',914,12,'admin/help/quicktabs','admin/help/quicktabs','quicktabs','a:0:{}','system',-1,0,0,0,0,3,0,1,12,914,0,0,0,0,0,0,0),('management',915,913,'admin/structure/quicktabs/add','admin/structure/quicktabs/add','Add Quicktabs Instance','a:0:{}','system',-1,0,0,0,0,4,0,1,21,913,915,0,0,0,0,0,0),('management',916,913,'admin/structure/quicktabs/list','admin/structure/quicktabs/list','List quicktabs','a:0:{}','system',-1,0,0,0,0,4,0,1,21,913,916,0,0,0,0,0,0),('management',917,913,'admin/structure/quicktabs/manage/%','admin/structure/quicktabs/manage/%','Edit quicktab','a:0:{}','system',0,0,0,0,0,4,0,1,21,913,917,0,0,0,0,0,0),('management',918,917,'admin/structure/quicktabs/manage/%/clone','admin/structure/quicktabs/manage/%/clone','Clone quicktab','a:0:{}','system',-1,0,0,0,0,5,0,1,21,913,917,918,0,0,0,0,0),('management',919,917,'admin/structure/quicktabs/manage/%/delete','admin/structure/quicktabs/manage/%/delete','Delete quicktab','a:0:{}','system',-1,0,0,0,0,5,0,1,21,913,917,919,0,0,0,0,0),('management',920,917,'admin/structure/quicktabs/manage/%/edit','admin/structure/quicktabs/manage/%/edit','Edit quicktab','a:0:{}','system',-1,0,0,0,0,5,0,1,21,913,917,920,0,0,0,0,0),('management',921,917,'admin/structure/quicktabs/manage/%/export','admin/structure/quicktabs/manage/%/export','Export','a:0:{}','system',-1,0,0,0,0,5,0,1,21,913,917,921,0,0,0,0,0),('management',926,143,'admin/structure/block/list/seven/add','admin/structure/block/list/seven/add','Add block','a:0:{}','system',-1,0,0,0,0,5,0,1,21,30,143,926,0,0,0,0,0),('main-menu',927,409,'node/25','node/%','Fourth Service','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,0,4,1,366,1657,409,927,0,0,0,0,0,0),('navigation',929,6,'node/add/mt-benefit','node/add/mt-benefit','Benefit','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:595:\"A \"Benefit\" post is ideal for creating and displaying Benefits of your offerings to clients. A Benefit can be displayed in the front-page slideshow. It also comes with the capability of attaching an image which is automatically adjusted to fit into the website layout. What’s particularly cool about a Benefit post, is that it provides the ability to attach a series of images, thumbnails of which are automatically created and adjusted to fit into the website layout. Lastly, a Benefit post enables free tagging (just like labels), helping you make the most from the built-in taxonomy system.\";}}','system',0,0,0,0,0,2,0,6,929,0,0,0,0,0,0,0,0),('main-menu',930,1658,'showcase-isotope','showcase-isotope','Showcase - Isotope','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-47,3,1,530,1658,930,0,0,0,0,0,0,0),('main-menu',1008,442,'node/22','node/%','Contact alternative','a:1:{s:10:\"attributes\";a:0:{}}','menu',0,0,0,0,-50,2,1,442,1008,0,0,0,0,0,0,0,0),('main-menu',1009,1809,'node/33','node/%','Slideshow - Boxed Width','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-49,3,1,895,1809,1009,0,0,0,0,0,0,0),('management',1168,131,'admin/structure/block/list/bartik/add','admin/structure/block/list/bartik/add','Add block','a:0:{}','system',-1,0,0,0,0,5,0,1,21,30,131,1168,0,0,0,0,0),('navigation',1245,6,'node/add/mt-product','node/add/mt-product','Product','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:513:\"A Product post is ideal for creating and showcasing your products. It comes with the option to be featured at the front page slide show. It also provides the ability to attach one main product image, which is automatically adjusted to fit into the website layout. Further to that, what’s a particularly great time-saver about a Product post, is that it provides the ability to attach a series of images, thumbnails of which are automatically created and adjusted to fit into the website layout, wherever needed.\";}}','system',0,0,0,0,0,2,0,6,1245,0,0,0,0,0,0,0,0),('management',1246,21,'admin/structure/field-collections','admin/structure/field-collections','Field collections','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:35:\"Manage fields on field collections.\";}}','system',0,0,1,0,0,3,0,1,21,1246,0,0,0,0,0,0,0),('management',1247,12,'admin/help/field_collection','admin/help/field_collection','field_collection','a:0:{}','system',-1,0,0,0,0,3,0,1,12,1247,0,0,0,0,0,0,0),('main-menu',1264,0,'node/35','node/%','About Us','a:0:{}','menu',0,0,1,0,-47,1,1,1264,0,0,0,0,0,0,0,0,0),('navigation',1265,6,'node/add/mt-team-member','node/add/mt-team-member','Team Member','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:394:\"The Team member post is ideal for creating a page for each of your team members. Along with a description of your team member’s position, responsibilities and qualifications, it provides the ability to attach an image, which is automatically adjusted to fit into the website’s layout. And what’s great about Team member posts, is that they are all collected under the “About us” page.\";}}','system',0,0,0,0,0,2,0,6,1265,0,0,0,0,0,0,0,0),('main-menu',1314,1809,'node/41','node/%','Slideshow - Full Screen','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-50,3,1,895,1809,1314,0,0,0,0,0,0,0),('main-menu',1315,1663,'products2','products2','Products - 2 Columns','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-50,3,1,218,1663,1315,0,0,0,0,0,0,0),('main-menu',1316,1663,'products3','products3','Products - 3 Columns','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,1,0,-49,3,1,218,1663,1316,0,0,0,0,0,0,0),('main-menu',1317,1663,'products','products','Products - 4 Columns','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-48,3,1,218,1663,1317,0,0,0,0,0,0,0),('main-menu',1373,1316,'node/44','node/%','First Product','a:1:{s:10:\"attributes\";a:0:{}}','menu',0,0,0,0,-50,4,1,218,1663,1316,1373,0,0,0,0,0,0),('main-menu',1374,1316,'node/43','node/%','Second Product','a:1:{s:10:\"attributes\";a:0:{}}','menu',0,0,1,0,-49,4,1,218,1663,1316,1374,0,0,0,0,0,0),('main-menu',1375,1316,'node/42','node/%','Third Product','a:1:{s:10:\"attributes\";a:0:{}}','menu',0,0,0,0,-48,4,1,218,1663,1316,1375,0,0,0,0,0,0),('main-menu',1376,1316,'node/34','node/%','Fourth Product','a:1:{s:10:\"attributes\";a:0:{}}','menu',0,0,0,0,-47,4,1,218,1663,1316,1376,0,0,0,0,0,0),('main-menu',1377,1374,'node/44','node/%','Product X','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,0,5,1,218,1663,1316,1374,1377,0,0,0,0,0),('main-menu',1378,1264,'node/35','node/%','Team Members','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,1,0,0,2,1,1264,1378,0,0,0,0,0,0,0,0),('main-menu',1379,1378,'node/40','node/%','First Team Member','a:1:{s:10:\"attributes\";a:0:{}}','menu',0,0,0,0,-50,3,1,1264,1378,1379,0,0,0,0,0,0,0),('main-menu',1380,1378,'node/39','node/%','Second Team Member','a:1:{s:10:\"attributes\";a:0:{}}','menu',0,0,0,0,-49,3,1,1264,1378,1380,0,0,0,0,0,0,0),('main-menu',1381,1378,'node/38','node/%','Third Team Member','a:1:{s:10:\"attributes\";a:0:{}}','menu',0,0,0,0,-48,3,1,1264,1378,1381,0,0,0,0,0,0,0),('main-menu',1382,1378,'node/37','node/%','Fourth Team Member','a:1:{s:10:\"attributes\";a:0:{}}','menu',0,0,0,0,-47,3,1,1264,1378,1382,0,0,0,0,0,0,0),('main-menu',1479,842,'node/26','node/%','Circular Progress Bars','a:2:{s:8:\"fragment\";s:12:\"circularbars\";s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,0,3,1,895,842,1479,0,0,0,0,0,0,0),('management',1576,54,'admin/appearance/settings/levelplus','admin/appearance/settings/levelplus','Level+','a:0:{}','system',-1,0,0,0,0,4,0,1,7,54,1576,0,0,0,0,0,0),('management',1577,30,'admin/structure/block/list/levelplus','admin/structure/block/list/levelplus','Level+','a:0:{}','system',-1,0,0,0,-10,4,0,1,21,30,1577,0,0,0,0,0,0),('navigation',1628,4,'filter/tips/%','filter/tips/%','Compose tips','a:0:{}','system',0,0,0,0,0,2,0,4,1628,0,0,0,0,0,0,0,0),('management',1629,47,'admin/structure/menu/manage/menu-footer-menu','admin/structure/menu/manage/%','Footer Menu','a:0:{}','menu',0,0,0,0,0,4,0,1,21,47,1629,0,0,0,0,0,0),('menu-footer-menu',1630,0,'node/35','node/%','About Us','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-50,1,1,1630,0,0,0,0,0,0,0,0,0),('menu-footer-menu',1631,0,'blog','blog','Blog and News','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-48,1,1,1631,0,0,0,0,0,0,0,0,0),('menu-footer-menu',1632,0,'node/22','node/%','Contact Us','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,1,0,-49,1,1,1632,0,0,0,0,0,0,0,0,0),('menu-footer-menu',1633,0,'node/26','node/%','Features','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-47,1,1,1633,0,0,0,0,0,0,0,0,0),('navigation',1634,0,'field-collection/field-mt-highlight/%','field-collection/field-mt-highlight/%','','a:0:{}','system',0,0,1,0,0,1,0,1634,0,0,0,0,0,0,0,0,0),('navigation',1635,1634,'field-collection/field-mt-highlight/%/delete','field-collection/field-mt-highlight/%/delete','Delete','a:0:{}','system',-1,0,0,0,0,2,0,1634,1635,0,0,0,0,0,0,0,0),('navigation',1636,1634,'field-collection/field-mt-highlight/%/edit','field-collection/field-mt-highlight/%/edit','Edit','a:0:{}','system',-1,0,0,0,0,2,0,1634,1636,0,0,0,0,0,0,0,0),('navigation',1637,1634,'field-collection/field-mt-highlight/%/view','field-collection/field-mt-highlight/%/view','View','a:0:{}','system',-1,0,0,0,-10,2,0,1634,1637,0,0,0,0,0,0,0,0),('navigation',1638,0,'field-collection/field-mt-highlight/add/%/%','field-collection/field-mt-highlight/add/%/%','','a:0:{}','system',0,0,0,0,0,1,0,1638,0,0,0,0,0,0,0,0,0),('navigation',1639,1634,'field-collection/field-mt-highlight/%/revisions/%','field-collection/field-mt-highlight/%/revisions/%','','a:0:{}','system',0,0,0,0,0,2,0,1634,1639,0,0,0,0,0,0,0,0),('navigation',1640,0,'field-collection/field-mt-special-feature/%','field-collection/field-mt-special-feature/%','','a:0:{}','system',0,0,1,0,0,1,0,1640,0,0,0,0,0,0,0,0,0),('navigation',1641,1640,'field-collection/field-mt-special-feature/%/delete','field-collection/field-mt-special-feature/%/delete','Delete','a:0:{}','system',-1,0,0,0,0,2,0,1640,1641,0,0,0,0,0,0,0,0),('navigation',1642,1640,'field-collection/field-mt-special-feature/%/edit','field-collection/field-mt-special-feature/%/edit','Edit','a:0:{}','system',-1,0,0,0,0,2,0,1640,1642,0,0,0,0,0,0,0,0),('navigation',1643,1640,'field-collection/field-mt-special-feature/%/view','field-collection/field-mt-special-feature/%/view','View','a:0:{}','system',-1,0,0,0,-10,2,0,1640,1643,0,0,0,0,0,0,0,0),('navigation',1644,0,'field-collection/field-mt-special-feature/add/%/%','field-collection/field-mt-special-feature/add/%/%','','a:0:{}','system',0,0,0,0,0,1,0,1644,0,0,0,0,0,0,0,0,0),('navigation',1645,1640,'field-collection/field-mt-special-feature/%/revisions/%','field-collection/field-mt-special-feature/%/revisions/%','','a:0:{}','system',0,0,0,0,0,2,0,1640,1645,0,0,0,0,0,0,0,0),('navigation',1646,0,'field-collection/field-mt-standard-feature/%','field-collection/field-mt-standard-feature/%','','a:0:{}','system',0,0,1,0,0,1,0,1646,0,0,0,0,0,0,0,0,0),('navigation',1647,1646,'field-collection/field-mt-standard-feature/%/delete','field-collection/field-mt-standard-feature/%/delete','Delete','a:0:{}','system',-1,0,0,0,0,2,0,1646,1647,0,0,0,0,0,0,0,0),('navigation',1648,1646,'field-collection/field-mt-standard-feature/%/edit','field-collection/field-mt-standard-feature/%/edit','Edit','a:0:{}','system',-1,0,0,0,0,2,0,1646,1648,0,0,0,0,0,0,0,0),('navigation',1649,1646,'field-collection/field-mt-standard-feature/%/view','field-collection/field-mt-standard-feature/%/view','View','a:0:{}','system',-1,0,0,0,-10,2,0,1646,1649,0,0,0,0,0,0,0,0),('navigation',1650,0,'field-collection/field-mt-standard-feature/add/%/%','field-collection/field-mt-standard-feature/add/%/%','','a:0:{}','system',0,0,0,0,0,1,0,1650,0,0,0,0,0,0,0,0,0),('navigation',1651,1646,'field-collection/field-mt-standard-feature/%/revisions/%','field-collection/field-mt-standard-feature/%/revisions/%','','a:0:{}','system',0,0,0,0,0,2,0,1646,1651,0,0,0,0,0,0,0,0),('main-menu',1652,366,'services-masonry','services-masonry','Services Masonry','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,0,2,1,366,1652,0,0,0,0,0,0,0,0),('main-menu',1653,1654,'services-masonry-col-3','services-masonry-col-3','Services - 3 Columns','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-49,3,1,366,1654,1653,0,0,0,0,0,0,0),('main-menu',1654,366,'services-masonry-col-3','services-masonry-col-3','Services Masonry','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,1,0,-50,2,1,366,1654,0,0,0,0,0,0,0,0),('main-menu',1655,1654,'services-masonry-col-2','services-masonry-col-2','Services - 2 Columns','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-50,3,1,366,1654,1655,0,0,0,0,0,0,0),('main-menu',1656,1654,'services-masonry-col-4','services-masonry-col-4','Services - 4 Columns','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-48,3,1,366,1654,1656,0,0,0,0,0,0,0),('main-menu',1657,366,'services','services','Services Grid','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,1,0,-48,2,1,366,1657,0,0,0,0,0,0,0,0),('main-menu',1658,530,'showcase','showcase','Showcases Grid','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,1,0,-49,2,1,530,1658,0,0,0,0,0,0,0,0),('main-menu',1659,530,'showcases-masonry-col-4','showcases-masonry-col-4','Showcases Masonry','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,1,0,-50,2,1,530,1659,0,0,0,0,0,0,0,0),('main-menu',1660,1659,'showcases-masonry-col-2','showcases-masonry-col-2','Showcases - 2 Columns','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-50,3,1,530,1659,1660,0,0,0,0,0,0,0),('main-menu',1661,1659,'showcases-masonry-col-3','showcases-masonry-col-3','Showcases - 3 Columns','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-49,3,1,530,1659,1661,0,0,0,0,0,0,0),('main-menu',1662,1659,'showcases-masonry-col-4','showcases-masonry-col-4','Showcases - 4 Columns','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-48,3,1,530,1659,1662,0,0,0,0,0,0,0),('main-menu',1663,218,'products','products','Products Grid','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,1,0,-49,2,1,218,1663,0,0,0,0,0,0,0,0),('main-menu',1664,218,'products-masonry-col-4','products-masonry-col-4','Products Masonry','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,1,0,-50,2,1,218,1664,0,0,0,0,0,0,0,0),('main-menu',1665,1664,'products-masonry-col-2','products-masonry-col-2','Products - Columns 2','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-50,3,1,218,1664,1665,0,0,0,0,0,0,0),('main-menu',1666,1664,'products-masonry-col-3','products-masonry-col-3','Products - Columns 3','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-49,3,1,218,1664,1666,0,0,0,0,0,0,0),('main-menu',1667,1664,'products-masonry-col-4','products-masonry-col-4','Products - Columns 4','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-48,3,1,218,1664,1667,0,0,0,0,0,0,0),('main-menu',1668,1664,'products-masonry-isotope','products-masonry-isotope','Products - Isotope','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,0,3,1,218,1664,1668,0,0,0,0,0,0,0),('main-menu',1669,1654,'services-masonry-isotope','services-masonry-isotope','Services - Isotope','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,0,3,1,366,1654,1669,0,0,0,0,0,0,0),('main-menu',1670,1659,'showcases-masonry-isotope','showcases-masonry-isotope','Showcases - Isotope','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,0,3,1,530,1659,1670,0,0,0,0,0,0,0),('navigation',1671,0,'comment/%','comment/%','Comment permalink','a:0:{}','system',0,0,1,0,0,1,0,1671,0,0,0,0,0,0,0,0,0),('navigation',1672,0,'comment/reply/%','comment/reply/%','Add new comment','a:0:{}','system',0,0,0,0,0,1,0,1672,0,0,0,0,0,0,0,0,0),('navigation',1673,1671,'comment/%/approve','comment/%/approve','Approve','a:0:{}','system',0,0,0,0,1,2,0,1671,1673,0,0,0,0,0,0,0,0),('navigation',1674,1671,'comment/%/delete','comment/%/delete','Delete','a:0:{}','system',-1,0,0,0,2,2,0,1671,1674,0,0,0,0,0,0,0,0),('navigation',1675,1671,'comment/%/edit','comment/%/edit','Edit','a:0:{}','system',-1,0,0,0,0,2,0,1671,1675,0,0,0,0,0,0,0,0),('navigation',1676,1671,'comment/%/view','comment/%/view','View comment','a:0:{}','system',-1,0,0,0,-10,2,0,1671,1676,0,0,0,0,0,0,0,0),('management',1677,9,'admin/content/comment','admin/content/comment','Comments','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:59:\"List and edit site comments and the comment approval queue.\";}}','system',0,0,0,0,0,3,0,1,9,1677,0,0,0,0,0,0,0),('management',1678,12,'admin/help/comment','admin/help/comment','comment','a:0:{}','system',-1,0,0,0,0,3,0,1,12,1678,0,0,0,0,0,0,0),('management',1679,1677,'admin/content/comment/new','admin/content/comment/new','Published comments','a:0:{}','system',-1,0,0,0,-10,4,0,1,9,1677,1679,0,0,0,0,0,0),('management',1680,1677,'admin/content/comment/approval','admin/content/comment/approval','Unapproved comments','a:0:{}','system',-1,0,0,0,0,4,0,1,9,1677,1680,0,0,0,0,0,0),('management',1681,135,'admin/structure/types/manage/%/comment/display','admin/structure/types/manage/%/comment/display','Comment display','a:0:{}','system',-1,0,0,0,4,5,0,1,21,36,135,1681,0,0,0,0,0),('management',1682,135,'admin/structure/types/manage/%/comment/fields','admin/structure/types/manage/%/comment/fields','Comment fields','a:0:{}','system',-1,0,1,0,3,5,0,1,21,36,135,1682,0,0,0,0,0),('management',1690,19,'admin/reports/hits','admin/reports/hits','Recent hits','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:43:\"View pages that have recently been visited.\";}}','system',0,0,0,0,0,3,0,1,19,1690,0,0,0,0,0,0,0),('management',1691,19,'admin/reports/pages','admin/reports/pages','Top pages','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:41:\"View pages that have been hit frequently.\";}}','system',0,0,0,0,1,3,0,1,19,1691,0,0,0,0,0,0,0),('management',1692,19,'admin/reports/referrers','admin/reports/referrers','Top referrers','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:19:\"View top referrers.\";}}','system',0,0,0,0,0,3,0,1,19,1692,0,0,0,0,0,0,0),('management',1693,19,'admin/reports/visitors','admin/reports/visitors','Top visitors','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:34:\"View visitors that hit many pages.\";}}','system',0,0,0,0,2,3,0,1,19,1693,0,0,0,0,0,0,0),('navigation',1694,5,'node/%/track','node/%/track','Track','a:0:{}','system',-1,0,0,0,2,2,0,5,1694,0,0,0,0,0,0,0,0),('management',1695,12,'admin/help/statistics','admin/help/statistics','statistics','a:0:{}','system',-1,0,0,0,0,3,0,1,12,1695,0,0,0,0,0,0,0),('management',1696,56,'admin/config/system/statistics','admin/config/system/statistics','Statistics','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:68:\"Control details about what and how your site logs access statistics.\";}}','system',0,0,0,0,-15,4,0,1,8,56,1696,0,0,0,0,0,0),('management',1697,19,'admin/reports/access/%','admin/reports/access/%','Details','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:16:\"View access log.\";}}','system',0,0,0,0,0,3,0,1,19,1697,0,0,0,0,0,0,0),('navigation',1698,17,'user/%/track/navigation','user/%/track/navigation','Track page visits','a:0:{}','system',-1,0,0,0,2,2,0,17,1698,0,0,0,0,0,0,0,0),('main-menu',1699,1808,'alternative-blog','alternative-blog','Blog - Alternative 1','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-49,3,1,895,1808,1699,0,0,0,0,0,0,0),('main-menu',1700,1808,'posts-col-3','posts-col-3','Blog - Alternative 2','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,1,0,-48,3,1,895,1808,1700,0,0,0,0,0,0,0),('main-menu',1701,1700,'posts-col-2','posts-col-2','2 Columns','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,0,4,1,895,1808,1700,1701,0,0,0,0,0,0),('main-menu',1702,1700,'posts-col-3','posts-col-3','3 Columns','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,0,4,1,895,1808,1700,1702,0,0,0,0,0,0),('main-menu',1703,1700,'posts-col-4','posts-col-4','4 Columns','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,0,4,1,895,1808,1700,1703,0,0,0,0,0,0),('main-menu',1704,442,'node/32','node/%','Contact alternative 2','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,0,2,1,442,1704,0,0,0,0,0,0,0,0),('menu-footer-menu',1705,1632,'node/22','node/%','Contact Us Alternative','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,0,2,1,1632,1705,0,0,0,0,0,0,0,0),('menu-footer-menu',1706,1632,'node/32','node/%','Contact Us Alternative 2','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,0,2,1,1632,1706,0,0,0,0,0,0,0,0),('main-menu',1707,530,'photo-gallery','photo-gallery','Photo Gallery','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,0,2,1,530,1707,0,0,0,0,0,0,0,0),('navigation',1756,417,'node/%/webform/conditionals','node/%/webform/conditionals','Conditionals','a:0:{}','system',-1,0,0,0,1,3,0,5,417,1756,0,0,0,0,0,0,0),('navigation',1757,420,'node/%/webform-results/analysis/%','node/%/webform-results/analysis/%','Analysis','a:0:{}','system',-1,0,0,0,0,4,0,5,415,420,1757,0,0,0,0,0,0),('navigation',1758,1757,'node/%/webform-results/analysis/%/more','node/%/webform-results/analysis/%/more','In-depth analysis','a:0:{}','system',-1,0,0,0,0,5,0,5,415,420,1757,1758,0,0,0,0,0),('navigation',1759,897,'node/%/webform/emails/%/clone','node/%/webform/emails/%/clone','','a:0:{}','system',-1,0,0,0,0,5,0,5,417,423,897,1759,0,0,0,0,0),('main-menu',1808,895,'blog','blog','Blog','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,1,0,-50,2,1,895,1808,0,0,0,0,0,0,0,0),('main-menu',1809,895,'node/41','node/%','Slideshows','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,1,0,-48,2,1,895,1809,0,0,0,0,0,0,0,0),('main-menu',1810,1809,'<front>','','Slideshow - Full Width','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,1,0,0,0,3,1,895,1809,1810,0,0,0,0,0,0,0),('main-menu',1811,895,'<front>','','Headers','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,1,1,0,-49,2,1,895,1811,0,0,0,0,0,0,0,0),('main-menu',1812,1811,'<front>','','Header - Default','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,1,0,0,-50,3,1,895,1811,1812,0,0,0,0,0,0,0),('main-menu',1813,1811,'node/57','node/%','Header - Alternative 2','a:1:{s:10:\"attributes\";a:0:{}}','menu',0,0,0,0,1,3,1,895,1811,1813,0,0,0,0,0,0,0),('main-menu',1814,1811,'node/58','node/%','Header - Alternative 1','a:0:{}','menu',0,0,0,0,0,3,0,895,1811,1814,0,0,0,0,0,0,0),('main-menu',1815,366,'services-masonry-alt-col-2','services-masonry-alt-col-2','Services Masonry 2','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:0:\"\";}}','menu',0,0,0,0,-49,2,1,366,1815,0,0,0,0,0,0,0,0),('management',1816,19,'admin/reports/libraries','admin/reports/libraries','Libraries','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:48:\"An overview of libraries installed on this site.\";}}','system',0,0,1,0,0,3,0,1,19,1816,0,0,0,0,0,0,0),('management',1817,12,'admin/help/libraries','admin/help/libraries','libraries','a:0:{}','system',-1,0,0,0,0,3,0,1,12,1817,0,0,0,0,0,0,0),('management',1818,1816,'admin/reports/libraries/%','admin/reports/libraries/%','Library status report','a:1:{s:10:\"attributes\";a:1:{s:5:\"title\";s:36:\"Status overview for a single library\";}}','system',0,0,0,0,0,4,0,1,19,1816,1818,0,0,0,0,0,0),('management',1819,1246,'admin/structure/field-collections/%','admin/structure/field-collections/%','Manage fields','a:0:{}','system',0,0,0,0,1,4,0,1,21,1246,1819,0,0,0,0,0,0),('management',1820,124,'admin/structure/taxonomy/%/display','admin/structure/taxonomy/%/display','Manage display','a:0:{}','system',-1,0,0,0,2,5,0,1,21,57,124,1820,0,0,0,0,0),('management',1821,89,'admin/config/people/accounts/display','admin/config/people/accounts/display','Manage display','a:0:{}','system',-1,0,0,0,2,5,0,1,8,48,89,1821,0,0,0,0,0),('management',1822,1819,'admin/structure/field-collections/%/display','admin/structure/field-collections/%/display','Manage display','a:0:{}','system',-1,0,0,0,2,5,0,1,21,1246,1819,1822,0,0,0,0,0),('management',1823,1819,'admin/structure/field-collections/%/fields','admin/structure/field-collections/%/fields','Manage fields','a:0:{}','system',-1,0,1,0,1,5,0,1,21,1246,1819,1823,0,0,0,0,0),('management',1824,124,'admin/structure/taxonomy/%/fields','admin/structure/taxonomy/%/fields','Manage fields','a:0:{}','system',-1,0,1,0,1,5,0,1,21,57,124,1824,0,0,0,0,0),('management',1825,89,'admin/config/people/accounts/fields','admin/config/people/accounts/fields','Manage fields','a:0:{}','system',-1,0,1,0,1,5,0,1,8,48,89,1825,0,0,0,0,0),('management',1826,1822,'admin/structure/field-collections/%/display/default','admin/structure/field-collections/%/display/default','Default','a:0:{}','system',-1,0,0,0,-10,6,0,1,21,1246,1819,1822,1826,0,0,0,0),('management',1827,1820,'admin/structure/taxonomy/%/display/default','admin/structure/taxonomy/%/display/default','Default','a:0:{}','system',-1,0,0,0,-10,6,0,1,21,57,124,1820,1827,0,0,0,0),('management',1828,1821,'admin/config/people/accounts/display/default','admin/config/people/accounts/display/default','Default','a:0:{}','system',-1,0,0,0,-10,6,0,1,8,48,89,1821,1828,0,0,0,0),('management',1829,1822,'admin/structure/field-collections/%/display/full','admin/structure/field-collections/%/display/full','Full content','a:0:{}','system',-1,0,0,0,0,6,0,1,21,1246,1819,1822,1829,0,0,0,0),('management',1830,135,'admin/structure/types/manage/%/display','admin/structure/types/manage/%/display','Manage display','a:0:{}','system',-1,0,0,0,2,5,0,1,21,36,135,1830,0,0,0,0,0),('management',1831,135,'admin/structure/types/manage/%/fields','admin/structure/types/manage/%/fields','Manage fields','a:0:{}','system',-1,0,1,0,1,5,0,1,21,36,135,1831,0,0,0,0,0),('management',1832,1820,'admin/structure/taxonomy/%/display/full','admin/structure/taxonomy/%/display/full','Taxonomy term page','a:0:{}','system',-1,0,0,0,0,6,0,1,21,57,124,1820,1832,0,0,0,0),('management',1833,1821,'admin/config/people/accounts/display/full','admin/config/people/accounts/display/full','User account','a:0:{}','system',-1,0,0,0,0,6,0,1,8,48,89,1821,1833,0,0,0,0),('management',1834,1823,'admin/structure/field-collections/%/fields/%','admin/structure/field-collections/%/fields/%','','a:0:{}','system',0,0,0,0,0,6,0,1,21,1246,1819,1823,1834,0,0,0,0),('management',1835,1824,'admin/structure/taxonomy/%/fields/%','admin/structure/taxonomy/%/fields/%','','a:0:{}','system',0,0,0,0,0,6,0,1,21,57,124,1824,1835,0,0,0,0),('management',1836,1825,'admin/config/people/accounts/fields/%','admin/config/people/accounts/fields/%','','a:0:{}','system',0,0,0,0,0,6,0,1,8,48,89,1825,1836,0,0,0,0),('management',1837,1830,'admin/structure/types/manage/%/display/default','admin/structure/types/manage/%/display/default','Default','a:0:{}','system',-1,0,0,0,-10,6,0,1,21,36,135,1830,1837,0,0,0,0),('management',1838,1830,'admin/structure/types/manage/%/display/full','admin/structure/types/manage/%/display/full','Full content','a:0:{}','system',-1,0,0,0,0,6,0,1,21,36,135,1830,1838,0,0,0,0),('management',1839,1830,'admin/structure/types/manage/%/display/rss','admin/structure/types/manage/%/display/rss','RSS','a:0:{}','system',-1,0,0,0,2,6,0,1,21,36,135,1830,1839,0,0,0,0),('management',1840,1830,'admin/structure/types/manage/%/display/search_index','admin/structure/types/manage/%/display/search_index','Search index','a:0:{}','system',-1,0,0,0,3,6,0,1,21,36,135,1830,1840,0,0,0,0),('management',1841,1830,'admin/structure/types/manage/%/display/search_result','admin/structure/types/manage/%/display/search_result','Search result highlighting input','a:0:{}','system',-1,0,0,0,4,6,0,1,21,36,135,1830,1841,0,0,0,0),('management',1842,1830,'admin/structure/types/manage/%/display/teaser','admin/structure/types/manage/%/display/teaser','Teaser','a:0:{}','system',-1,0,0,0,1,6,0,1,21,36,135,1830,1842,0,0,0,0),('management',1843,1834,'admin/structure/field-collections/%/fields/%/delete','admin/structure/field-collections/%/fields/%/delete','Delete','a:0:{}','system',-1,0,0,0,10,7,0,1,21,1246,1819,1823,1834,1843,0,0,0),('management',1844,1834,'admin/structure/field-collections/%/fields/%/edit','admin/structure/field-collections/%/fields/%/edit','Edit','a:0:{}','system',-1,0,0,0,0,7,0,1,21,1246,1819,1823,1834,1844,0,0,0),('management',1845,1834,'admin/structure/field-collections/%/fields/%/field-settings','admin/structure/field-collections/%/fields/%/field-settings','Field settings','a:0:{}','system',-1,0,0,0,0,7,0,1,21,1246,1819,1823,1834,1845,0,0,0),('management',1846,1834,'admin/structure/field-collections/%/fields/%/widget-type','admin/structure/field-collections/%/fields/%/widget-type','Widget type','a:0:{}','system',-1,0,0,0,0,7,0,1,21,1246,1819,1823,1834,1846,0,0,0),('management',1847,1831,'admin/structure/types/manage/%/fields/%','admin/structure/types/manage/%/fields/%','','a:0:{}','system',0,0,0,0,0,6,0,1,21,36,135,1831,1847,0,0,0,0),('management',1848,1835,'admin/structure/taxonomy/%/fields/%/delete','admin/structure/taxonomy/%/fields/%/delete','Delete','a:0:{}','system',-1,0,0,0,10,7,0,1,21,57,124,1824,1835,1848,0,0,0),('management',1849,1835,'admin/structure/taxonomy/%/fields/%/edit','admin/structure/taxonomy/%/fields/%/edit','Edit','a:0:{}','system',-1,0,0,0,0,7,0,1,21,57,124,1824,1835,1849,0,0,0),('management',1850,1835,'admin/structure/taxonomy/%/fields/%/field-settings','admin/structure/taxonomy/%/fields/%/field-settings','Field settings','a:0:{}','system',-1,0,0,0,0,7,0,1,21,57,124,1824,1835,1850,0,0,0),('management',1851,1835,'admin/structure/taxonomy/%/fields/%/widget-type','admin/structure/taxonomy/%/fields/%/widget-type','Widget type','a:0:{}','system',-1,0,0,0,0,7,0,1,21,57,124,1824,1835,1851,0,0,0),('management',1852,1836,'admin/config/people/accounts/fields/%/delete','admin/config/people/accounts/fields/%/delete','Delete','a:0:{}','system',-1,0,0,0,10,7,0,1,8,48,89,1825,1836,1852,0,0,0),('management',1853,1836,'admin/config/people/accounts/fields/%/edit','admin/config/people/accounts/fields/%/edit','Edit','a:0:{}','system',-1,0,0,0,0,7,0,1,8,48,89,1825,1836,1853,0,0,0),('management',1854,1836,'admin/config/people/accounts/fields/%/field-settings','admin/config/people/accounts/fields/%/field-settings','Field settings','a:0:{}','system',-1,0,0,0,0,7,0,1,8,48,89,1825,1836,1854,0,0,0),('management',1855,1836,'admin/config/people/accounts/fields/%/widget-type','admin/config/people/accounts/fields/%/widget-type','Widget type','a:0:{}','system',-1,0,0,0,0,7,0,1,8,48,89,1825,1836,1855,0,0,0),('management',1856,1681,'admin/structure/types/manage/%/comment/display/default','admin/structure/types/manage/%/comment/display/default','Default','a:0:{}','system',-1,0,0,0,-10,6,0,1,21,36,135,1681,1856,0,0,0,0),('management',1857,1681,'admin/structure/types/manage/%/comment/display/full','admin/structure/types/manage/%/comment/display/full','Full comment','a:0:{}','system',-1,0,0,0,0,6,0,1,21,36,135,1681,1857,0,0,0,0),('management',1858,1847,'admin/structure/types/manage/%/fields/%/delete','admin/structure/types/manage/%/fields/%/delete','Delete','a:0:{}','system',-1,0,0,0,10,7,0,1,21,36,135,1831,1847,1858,0,0,0),('management',1859,1847,'admin/structure/types/manage/%/fields/%/edit','admin/structure/types/manage/%/fields/%/edit','Edit','a:0:{}','system',-1,0,0,0,0,7,0,1,21,36,135,1831,1847,1859,0,0,0),('management',1860,1682,'admin/structure/types/manage/%/comment/fields/%','admin/structure/types/manage/%/comment/fields/%','','a:0:{}','system',0,0,0,0,0,6,0,1,21,36,135,1682,1860,0,0,0,0),('management',1861,1847,'admin/structure/types/manage/%/fields/%/field-settings','admin/structure/types/manage/%/fields/%/field-settings','Field settings','a:0:{}','system',-1,0,0,0,0,7,0,1,21,36,135,1831,1847,1861,0,0,0),('management',1862,1847,'admin/structure/types/manage/%/fields/%/widget-type','admin/structure/types/manage/%/fields/%/widget-type','Widget type','a:0:{}','system',-1,0,0,0,0,7,0,1,21,36,135,1831,1847,1862,0,0,0),('management',1863,1860,'admin/structure/types/manage/%/comment/fields/%/delete','admin/structure/types/manage/%/comment/fields/%/delete','Delete','a:0:{}','system',-1,0,0,0,10,7,0,1,21,36,135,1682,1860,1863,0,0,0),('management',1864,1860,'admin/structure/types/manage/%/comment/fields/%/edit','admin/structure/types/manage/%/comment/fields/%/edit','Edit','a:0:{}','system',-1,0,0,0,0,7,0,1,21,36,135,1682,1860,1864,0,0,0),('management',1865,1860,'admin/structure/types/manage/%/comment/fields/%/field-settings','admin/structure/types/manage/%/comment/fields/%/field-settings','Field settings','a:0:{}','system',-1,0,0,0,0,7,0,1,21,36,135,1682,1860,1865,0,0,0),('management',1866,1860,'admin/structure/types/manage/%/comment/fields/%/widget-type','admin/structure/types/manage/%/comment/fields/%/widget-type','Widget type','a:0:{}','system',-1,0,0,0,0,7,0,1,21,36,135,1682,1860,1866,0,0,0);
/*!40000 ALTER TABLE `menu_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `node`
--

DROP TABLE IF EXISTS `node`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `node` (
  `nid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier for a node.',
  `vid` int(10) unsigned DEFAULT NULL COMMENT 'The current node_revision.vid version identifier.',
  `type` varchar(32) NOT NULL DEFAULT '' COMMENT 'The node_type.type of this node.',
  `language` varchar(12) NOT NULL DEFAULT '' COMMENT 'The languages.language of this node.',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT 'The title of this node, always treated as non-markup plain text.',
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT 'The users.uid that owns this node; initially, this is the user that created it.',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT 'Boolean indicating whether the node is published (visible to non-administrators).',
  `created` int(11) NOT NULL DEFAULT '0' COMMENT 'The Unix timestamp when the node was created.',
  `changed` int(11) NOT NULL DEFAULT '0' COMMENT 'The Unix timestamp when the node was most recently saved.',
  `comment` int(11) NOT NULL DEFAULT '0' COMMENT 'Whether comments are allowed on this node: 0 = no, 1 = closed (read only), 2 = open (read/write).',
  `promote` int(11) NOT NULL DEFAULT '0' COMMENT 'Boolean indicating whether the node should be displayed on the front page.',
  `sticky` int(11) NOT NULL DEFAULT '0' COMMENT 'Boolean indicating whether the node should be displayed at the top of lists in which it appears.',
  `tnid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The translation set id for this node, which equals the node id of the source post in each set.',
  `translate` int(11) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this translation page needs to be updated.',
  PRIMARY KEY (`nid`),
  UNIQUE KEY `vid` (`vid`),
  KEY `node_changed` (`changed`),
  KEY `node_created` (`created`),
  KEY `node_frontpage` (`promote`,`status`,`sticky`,`created`),
  KEY `node_status_type` (`status`,`type`,`nid`),
  KEY `node_title_type` (`title`,`type`(4)),
  KEY `node_type` (`type`(4)),
  KEY `uid` (`uid`),
  KEY `tnid` (`tnid`),
  KEY `translate` (`translate`),
  KEY `language` (`language`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COMMENT='The base table for nodes.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `node`
--

LOCK TABLES `node` WRITE;
/*!40000 ALTER TABLE `node` DISABLE KEYS */;
INSERT INTO `node` VALUES (1,1,'page','und','Typography',1,1,1348839636,1459357351,1,0,0,0,0),(2,2,'page','und','About',1,1,1348856744,1459360270,1,0,0,0,0),(3,3,'mt_testimonial','und','John Smith',1,1,1349978186,1449573560,1,1,0,0,0),(8,8,'page','und','Responsive Grid',1,1,1354461699,1389354675,1,0,0,0,0),(9,9,'page','und','Responsive Grid',1,1,1354462099,1389354638,1,0,0,0,0),(10,10,'mt_service','und','First Service',1,1,1448635775,1459355727,1,1,0,0,0),(11,11,'mt_service','und','Third Service',1,1,1360075775,1459355994,1,1,0,0,0),(12,12,'mt_service','und','Second Service',1,1,1360334975,1459355817,1,1,0,0,0),(13,13,'blog','und','The secret to a successful online presence',1,1,1357311631,1449695602,2,1,0,0,0),(14,14,'blog','und','Announcing our brand new Service',1,1,1358523691,1450296561,2,1,0,0,0),(15,15,'blog','und','Phosfluore e-enable adaptive synergy for strategic quality vectors',1,1,1357400711,1449692093,2,1,0,0,0),(16,16,'mt_showcase','und','Fourth Showcase',1,1,1354618231,1458846615,2,1,0,0,0),(17,17,'mt_showcase','und','Fifth Showcase',1,1,1354608296,1458846629,2,1,0,0,0),(18,18,'mt_showcase','und','Sixth Showcase',1,1,1354604787,1458846641,2,1,0,0,0),(19,19,'mt_showcase','und','Second Showcase',1,1,1354630058,1458849136,2,1,0,0,0),(20,20,'mt_showcase','und','First Showcase',1,1,1370354997,1458848740,2,1,0,0,0),(21,21,'mt_showcase','und','Third Showcase',1,1,1354626661,1458846598,2,1,0,0,0),(22,22,'webform','und','Contact Us',1,1,1354804588,1458642091,1,0,0,0,0),(24,24,'mt_testimonial','und','Terry Smith',1,1,1359891193,1449573566,1,1,0,0,0),(25,25,'mt_service','und','Fourth Service',1,1,1359989375,1459356042,1,1,0,0,0),(26,26,'page','und','Shortcodes',1,1,1390290367,1458300433,1,0,0,0,0),(29,29,'mt_benefit','und','Finished projects',1,1,1394644156,1449065656,1,0,0,0,0),(30,30,'mt_benefit','und','Offices worldwide',1,1,1394644326,1449065634,1,0,0,0,0),(31,31,'mt_benefit','und','Happy clients',1,1,1394644700,1449840946,1,0,0,0,0),(32,32,'webform','und','Contact alternative',1,1,1396879025,1449916349,1,0,0,0,0),(33,33,'page','und','Boxed Width Slideshow',1,1,1396883952,1396886165,1,0,0,0,0),(34,34,'mt_product','und','Fourth Product',1,1,1423221928,1459356300,1,1,0,0,0),(35,35,'page','und','About Us',1,1,1423658176,1449574800,1,0,0,0,0),(36,36,'mt_team_member','und','John Smith',1,1,1423659946,1448640527,1,0,0,0,0),(37,37,'mt_team_member','und','Terry Smith',1,1,1423676420,1448640538,1,0,0,0,0),(38,38,'mt_team_member','und','John Doe',1,1,1423676688,1448640547,1,0,0,0,0),(39,39,'mt_team_member','und','Lorem Ipsum',1,1,1423676864,1448640555,1,0,0,0,0),(40,40,'mt_team_member','und','John Smith',1,1,1423676935,1448640563,1,0,0,0,0),(41,41,'page','und','Full Screen Slideshow',1,1,1423759408,1448646461,1,0,0,0,0),(42,42,'mt_product','und','Third Product',1,1,1424355328,1459356259,1,1,0,0,0),(43,43,'mt_product','und','Second Product',1,1,1424356003,1459356204,1,1,0,0,0),(44,44,'mt_product','und','First Product',1,1,1424356153,1459183847,1,1,0,0,0),(45,45,'mt_service','und','Fifth Service',1,1,1359902975,1459356082,1,1,0,0,0),(46,46,'mt_service','und','Sixth Service',1,1,1359816575,1459356125,1,1,0,0,0),(47,47,'mt_product','und','Fifth Product',1,1,1423135528,1459356346,1,1,0,0,0),(48,48,'mt_product','und','Sixth Product',1,1,1423049128,1459356378,1,1,0,0,0),(49,49,'mt_testimonial','und','Jane Smith',1,1,1449575526,1449579442,1,1,0,0,0),(50,50,'mt_testimonial','und','Jonathan Doe',1,1,1449575588,1449575588,1,1,0,0,0),(51,51,'mt_testimonial','und','Smith Doe',1,1,1449575654,1449575654,1,1,0,0,0),(52,52,'blog','und','Lorem ipsum dolor sit amet',1,1,1449694729,1458825998,2,1,0,0,0),(53,53,'blog','und','Ut enim ad minim veniam',1,1,1449694558,1449696584,2,1,0,0,0),(54,54,'blog','und','Duis aute irure dolor in reprehenderit',1,1,1449694609,1449694609,2,1,0,0,0),(55,55,'page','und','Contact Us',1,1,1449868624,1458642068,1,0,0,0,0),(56,56,'mt_showcase','und','Seventh Showcase',1,1,1352012787,1458846652,2,1,0,0,0),(57,57,'page','und','Header - Alternative 2',1,1,1458645891,1458649924,1,0,0,0,0),(58,58,'page','und','Header - Alternative 1',1,1,1458648797,1458649773,1,0,0,0,0);
/*!40000 ALTER TABLE `node` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `node_access`
--

DROP TABLE IF EXISTS `node_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `node_access` (
  `nid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The node.nid this record affects.',
  `gid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The grant ID a user must possess in the specified realm to gain this row’s privileges on the node.',
  `realm` varchar(255) NOT NULL DEFAULT '' COMMENT 'The realm in which the user must possess the grant ID. Each node access node can define one or more realms.',
  `grant_view` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Boolean indicating whether a user with the realm/grant pair can view this node.',
  `grant_update` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Boolean indicating whether a user with the realm/grant pair can edit this node.',
  `grant_delete` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Boolean indicating whether a user with the realm/grant pair can delete this node.',
  PRIMARY KEY (`nid`,`gid`,`realm`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Identifies which realm/grant pairs a user must possess in...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `node_access`
--

LOCK TABLES `node_access` WRITE;
/*!40000 ALTER TABLE `node_access` DISABLE KEYS */;
INSERT INTO `node_access` VALUES (0,0,'all',1,0,0);
/*!40000 ALTER TABLE `node_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `node_comment_statistics`
--

DROP TABLE IF EXISTS `node_comment_statistics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `node_comment_statistics` (
  `nid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The node.nid for which the statistics are compiled.',
  `cid` int(11) NOT NULL DEFAULT '0' COMMENT 'The comment.cid of the last comment.',
  `last_comment_timestamp` int(11) NOT NULL DEFAULT '0' COMMENT 'The Unix timestamp of the last comment that was posted within this node, from comment.changed.',
  `last_comment_name` varchar(60) DEFAULT NULL COMMENT 'The name of the latest author to post a comment on this node, from comment.name.',
  `last_comment_uid` int(11) NOT NULL DEFAULT '0' COMMENT 'The user ID of the latest author to post a comment on this node, from comment.uid.',
  `comment_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The total number of comments on this node.',
  PRIMARY KEY (`nid`),
  KEY `node_comment_timestamp` (`last_comment_timestamp`),
  KEY `comment_count` (`comment_count`),
  KEY `last_comment_uid` (`last_comment_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Maintains statistics of node and comments posts to show ...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `node_comment_statistics`
--

LOCK TABLES `node_comment_statistics` WRITE;
/*!40000 ALTER TABLE `node_comment_statistics` DISABLE KEYS */;
INSERT INTO `node_comment_statistics` VALUES (1,0,1348839636,NULL,1,0),(2,0,1348856744,NULL,1,0),(3,0,1349978186,NULL,1,0),(8,0,1354461699,NULL,1,0),(9,0,1354462099,NULL,1,0),(10,0,1354545695,NULL,1,0),(11,0,1354545771,NULL,1,0),(12,0,1354546127,NULL,1,0),(13,0,1354549204,NULL,1,0),(14,3,1389961594,'',1,1),(15,5,1390166287,'',1,3),(16,0,1354629031,NULL,1,0),(17,0,1354629896,NULL,1,0),(18,0,1354629987,NULL,1,0),(19,6,1394103065,'',1,1),(20,10,1394107117,'',1,3),(21,7,1394106998,'',1,1),(22,0,1354804588,NULL,1,0),(24,0,1359891193,NULL,1,0),(25,0,1389632354,NULL,1,0),(26,0,1390290367,NULL,1,0),(29,0,1394644156,NULL,1,0),(30,0,1394644326,NULL,1,0),(31,0,1394644700,NULL,1,0),(32,0,1396879025,NULL,1,0),(33,0,1396883952,NULL,1,0),(34,0,1423221928,NULL,1,0),(35,0,1423658176,NULL,1,0),(36,0,1423659946,NULL,1,0),(37,0,1423676420,NULL,1,0),(38,0,1423676688,NULL,1,0),(39,0,1423676864,NULL,1,0),(40,0,1423676935,NULL,1,0),(41,0,1423759408,NULL,1,0),(42,0,1424355328,NULL,1,0),(43,0,1424356003,NULL,1,0),(44,0,1424356153,NULL,1,0),(45,0,1449091929,NULL,1,0),(46,0,1449092088,NULL,1,0),(47,0,1449107071,NULL,1,0),(48,0,1449107138,NULL,1,0),(49,0,1449575526,NULL,1,0),(50,0,1449575588,NULL,1,0),(51,0,1449575654,NULL,1,0),(52,0,1449694488,NULL,1,0),(53,0,1449694558,NULL,1,0),(54,0,1449694609,NULL,1,0),(55,0,1449868624,NULL,1,0),(56,0,1453193867,NULL,1,0),(57,0,1458645891,NULL,1,0),(58,0,1458648797,NULL,1,0);
/*!40000 ALTER TABLE `node_comment_statistics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `node_revision`
--

DROP TABLE IF EXISTS `node_revision`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `node_revision` (
  `nid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The node this version belongs to.',
  `vid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier for this version.',
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT 'The users.uid that created this version.',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT 'The title of this version.',
  `log` longtext NOT NULL COMMENT 'The log entry explaining the changes in this version.',
  `timestamp` int(11) NOT NULL DEFAULT '0' COMMENT 'A Unix timestamp indicating when this version was created.',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT 'Boolean indicating whether the node (at the time of this revision) is published (visible to non-administrators).',
  `comment` int(11) NOT NULL DEFAULT '0' COMMENT 'Whether comments are allowed on this node (at the time of this revision): 0 = no, 1 = closed (read only), 2 = open (read/write).',
  `promote` int(11) NOT NULL DEFAULT '0' COMMENT 'Boolean indicating whether the node (at the time of this revision) should be displayed on the front page.',
  `sticky` int(11) NOT NULL DEFAULT '0' COMMENT 'Boolean indicating whether the node (at the time of this revision) should be displayed at the top of lists in which it appears.',
  PRIMARY KEY (`vid`),
  KEY `nid` (`nid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COMMENT='Stores information about each saved version of a node.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `node_revision`
--

LOCK TABLES `node_revision` WRITE;
/*!40000 ALTER TABLE `node_revision` DISABLE KEYS */;
INSERT INTO `node_revision` VALUES (1,1,1,'Typography','',1459357351,1,1,0,0),(2,2,1,'About','',1459360270,1,1,0,0),(3,3,1,'John Smith','',1449573560,1,1,1,0),(8,8,1,'Responsive Grid','',1389354675,1,1,0,0),(9,9,1,'Responsive Grid','',1389354638,1,1,0,0),(10,10,1,'First Service','',1459355727,1,1,1,0),(11,11,1,'Third Service','',1459355994,1,1,1,0),(12,12,1,'Second Service','',1459355817,1,1,1,0),(13,13,1,'The secret to a successful online presence','',1449695602,1,2,1,0),(14,14,1,'Announcing our brand new Service','',1450296561,1,2,1,0),(15,15,1,'Phosfluore e-enable adaptive synergy for strategic quality vectors','',1449692093,1,2,1,0),(16,16,1,'Fourth Showcase','',1458846615,1,2,1,0),(17,17,1,'Fifth Showcase','',1458846629,1,2,1,0),(18,18,1,'Sixth Showcase','',1458846641,1,2,1,0),(19,19,1,'Second Showcase','',1458849136,1,2,1,0),(20,20,1,'First Showcase','',1458848740,1,2,1,0),(21,21,1,'Third Showcase','',1458846598,1,2,1,0),(22,22,1,'Contact Us','',1458642091,1,1,0,0),(24,24,1,'Terry Smith','',1449573566,1,1,1,0),(25,25,1,'Fourth Service','',1459356042,1,1,1,0),(26,26,1,'Shortcodes','',1458300433,1,1,0,0),(29,29,1,'Finished projects','',1449065656,1,1,0,0),(30,30,1,'Offices worldwide','',1449065634,1,1,0,0),(31,31,1,'Happy clients','',1449840946,1,1,0,0),(32,32,1,'Contact alternative','',1449916349,1,1,0,0),(33,33,1,'Boxed Width Slideshow','',1396886165,1,1,0,0),(34,34,1,'Fourth Product','',1459356300,1,1,1,0),(35,35,1,'About Us','',1449574800,1,1,0,0),(36,36,1,'John Smith','',1448640527,1,1,0,0),(37,37,1,'Terry Smith','',1448640538,1,1,0,0),(38,38,1,'John Doe','',1448640547,1,1,0,0),(39,39,1,'Lorem Ipsum','',1448640555,1,1,0,0),(40,40,1,'John Smith','',1448640563,1,1,0,0),(41,41,1,'Full Screen Slideshow','',1448646461,1,1,0,0),(42,42,1,'Third Product','',1459356259,1,1,1,0),(43,43,1,'Second Product','',1459356204,1,1,1,0),(44,44,1,'First Product','',1459183847,1,1,1,0),(45,45,1,'Fifth Service','',1459356082,1,1,1,0),(46,46,1,'Sixth Service','',1459356125,1,1,1,0),(47,47,1,'Fifth Product','',1459356346,1,1,1,0),(48,48,1,'Sixth Product','',1459356378,1,1,1,0),(49,49,1,'Jane Smith','',1449579442,1,1,1,0),(50,50,1,'Jonathan Doe','',1449575588,1,1,1,0),(51,51,1,'Smith Doe','',1449575654,1,1,1,0),(52,52,1,'Lorem ipsum dolor sit amet','',1458825998,1,2,1,0),(53,53,1,'Ut enim ad minim veniam','',1449696584,1,2,1,0),(54,54,1,'Duis aute irure dolor in reprehenderit','',1449694609,1,2,1,0),(55,55,1,'Contact Us','',1458642068,1,1,0,0),(56,56,1,'Seventh Showcase','',1458846652,1,2,1,0),(57,57,1,'Header - Alternative 2','',1458649924,1,1,0,0),(58,58,1,'Header - Alternative 1','',1458649773,1,1,0,0);
/*!40000 ALTER TABLE `node_revision` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `node_type`
--

DROP TABLE IF EXISTS `node_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `node_type` (
  `type` varchar(32) NOT NULL COMMENT 'The machine-readable name of this type.',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT 'The human-readable name of this type.',
  `base` varchar(255) NOT NULL COMMENT 'The base string used to construct callbacks corresponding to this node type.',
  `module` varchar(255) NOT NULL COMMENT 'The module defining this node type.',
  `description` mediumtext NOT NULL COMMENT 'A brief description of this type.',
  `help` mediumtext NOT NULL COMMENT 'Help information shown to the user when creating a node of this type.',
  `has_title` tinyint(3) unsigned NOT NULL COMMENT 'Boolean indicating whether this type uses the node.title field.',
  `title_label` varchar(255) NOT NULL DEFAULT '' COMMENT 'The label displayed for the title field on the edit form.',
  `custom` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this type is defined by a module (FALSE) or by a user via Add content type (TRUE).',
  `modified` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this type has been modified by an administrator; currently not used in any way.',
  `locked` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether the administrator can change the machine name of this type.',
  `disabled` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether the node type is disabled.',
  `orig_type` varchar(255) NOT NULL DEFAULT '' COMMENT 'The original machine-readable name of this node type. This may be different from the current type name if the locked field is 0.',
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores information about all defined node types.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `node_type`
--

LOCK TABLES `node_type` WRITE;
/*!40000 ALTER TABLE `node_type` DISABLE KEYS */;
INSERT INTO `node_type` VALUES ('article','Article','node_content','node','Use <em>articles</em> for time-sensitive content like news, press releases or blog posts.','',1,'Title',1,1,0,0,'article'),('blog','Blog entry','blog','blog','Use for multi-user blogs. Every user gets a personal blog.','',1,'Title',0,1,1,0,'blog'),('mt_benefit','Benefit','node_content','node','A \"Benefit\" post is ideal for creating and displaying Benefits of your offerings to clients. A Benefit can be displayed in the front-page slideshow. It also comes with the capability of attaching an image which is automatically adjusted to fit into the website layout. What’s particularly cool about a Benefit post, is that it provides the ability to attach a series of images, thumbnails of which are automatically created and adjusted to fit into the website layout. Lastly, a Benefit post enables free tagging (just like labels), helping you make the most from the built-in taxonomy system.','',1,'Title',1,1,0,0,'mt_benefit'),('mt_product','Product','node_content','node','A Product post is ideal for creating and showcasing your products. It comes with the option to be featured at the front page slide show. It also provides the ability to attach one main product image, which is automatically adjusted to fit into the website layout. Further to that, what’s a particularly great time-saver about a Product post, is that it provides the ability to attach a series of images, thumbnails of which are automatically created and adjusted to fit into the website layout, wherever needed.','',1,'Title',1,1,0,0,'mt_product'),('mt_service','Service','node_content','node','A Service post is ideal for creating and displaying services. It has the option to be displayed into front page slide show. It provides the ability to attach an image which is automatically adjusted to fit into the websites layout. What’s particularly cool about a Service post, is that it provides the ability to attach a series of images, thumbnails of which are automatically created and adjusted to fit into the website layout. Service post enables free tagging (just like labels), taking the bests from the taxonomy system.','',1,'Title',1,1,0,0,'mt_service'),('mt_showcase','Showcase','node_content','node','A Showcase post is ideal for creating and displaying showcases. It has the option to be displayed into front page slide show. It provides the ability to attach an image which is automatically adjusted to fit into the websites layout. What’s particularly cool about a Showcase post, is that it provides the ability to attach a series of images, thumbnails of which are automatically created and adjusted to fit into the website layout. Showcase post enables free tagging (just like labels), taking the bests from the taxonomy system.','',1,'Title',1,1,0,0,'mt_showcase'),('mt_slideshow_entry','Slideshow entry','node_content','node','A Slideshow entry is ideal for creating commercial banners as well as messages for your website. Use it to promote any page of your website or URL into the front page slide show. It can carry a title, a teaser and an image linking to an internal path or external link.','',1,'Title',1,1,0,0,'mt_slideshow_entry'),('mt_team_member','Team Member','node_content','node','The Team member post is ideal for creating a page for each of your team members. Along with a description of your team member’s position, responsibilities and qualifications, it provides the ability to attach an image, which is automatically adjusted to fit into the website’s layout. And what’s great about Team member posts, is that they are all collected under the “About us” page.','',1,'Title',1,1,0,0,'mt_team_member'),('mt_testimonial','Testimonial','node_content','node','The Testimonial entry is perfect to showcase the words of love from your users and customers. Use it to easily publish testimonials on your site. A special View Block and a View Page are already configured to show them off perfectly.','',1,'Title',1,1,0,0,'mt_testimonial'),('page','Basic page','node_content','node','Use <em>basic pages</em> for your static content, such as an \'About us\' page.','',1,'Title',1,1,0,0,'page'),('webform','Webform','node_content','node','Create a new form or questionnaire accessible to users. Submission results and statistics are recorded and accessible to privileged users.','',1,'Title',1,1,0,0,'webform');
/*!40000 ALTER TABLE `node_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_common_consumer`
--

DROP TABLE IF EXISTS `oauth_common_consumer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_common_consumer` (
  `csid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary ID field for the table. Not used for anything except internal lookups.',
  `key_hash` char(40) NOT NULL COMMENT 'SHA1-hash of consumer_key.',
  `consumer_key` text NOT NULL COMMENT 'Consumer key.',
  `secret` text NOT NULL COMMENT 'Consumer secret.',
  `configuration` longtext NOT NULL COMMENT 'Consumer configuration',
  PRIMARY KEY (`csid`),
  KEY `key_hash` (`key_hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Keys and secrets for OAuth consumers, both those provided...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_common_consumer`
--

LOCK TABLES `oauth_common_consumer` WRITE;
/*!40000 ALTER TABLE `oauth_common_consumer` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_common_consumer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_common_context`
--

DROP TABLE IF EXISTS `oauth_common_context`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_common_context` (
  `cid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary ID field for the table. Not used for anything except internal lookups.',
  `name` varchar(32) NOT NULL COMMENT 'The computer-readable name of the context.',
  `title` varchar(100) NOT NULL COMMENT 'The localizable title of the authorization context.',
  `authorization_options` longtext NOT NULL COMMENT 'Authorization options.',
  `authorization_levels` longtext NOT NULL COMMENT 'Authorization levels for the context.',
  PRIMARY KEY (`cid`),
  UNIQUE KEY `context` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores contexts for OAuth common';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_common_context`
--

LOCK TABLES `oauth_common_context` WRITE;
/*!40000 ALTER TABLE `oauth_common_context` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_common_context` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_common_nonce`
--

DROP TABLE IF EXISTS `oauth_common_nonce`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_common_nonce` (
  `nonce` varchar(255) NOT NULL COMMENT 'The random string used on each request.',
  `timestamp` int(11) NOT NULL COMMENT 'The timestamp of the request.',
  `token_key` varchar(32) NOT NULL COMMENT 'Token key.',
  PRIMARY KEY (`nonce`),
  KEY `timekey` (`timestamp`,`token_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores timestamp against nonce for repeat attacks.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_common_nonce`
--

LOCK TABLES `oauth_common_nonce` WRITE;
/*!40000 ALTER TABLE `oauth_common_nonce` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_common_nonce` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_common_provider_consumer`
--

DROP TABLE IF EXISTS `oauth_common_provider_consumer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_common_provider_consumer` (
  `csid` int(10) unsigned DEFAULT '0' COMMENT 'The oauth_common_consumer.csid this data is related to.',
  `consumer_key` char(32) NOT NULL COMMENT 'Consumer key.',
  `created` int(11) NOT NULL DEFAULT '0' COMMENT 'The time that the consumer was created, as a Unix timestamp.',
  `changed` int(11) NOT NULL DEFAULT '0' COMMENT 'The last time the consumer was edited, as a Unix timestamp.',
  `uid` int(10) unsigned NOT NULL COMMENT 'The application owner.',
  `name` varchar(128) NOT NULL COMMENT 'The application name.',
  `context` varchar(32) NOT NULL DEFAULT '' COMMENT 'The application context.',
  `callback_url` varchar(255) NOT NULL COMMENT 'Callback url.',
  PRIMARY KEY (`consumer_key`),
  UNIQUE KEY `csid` (`csid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Additional data for OAuth consumers provided by this site.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_common_provider_consumer`
--

LOCK TABLES `oauth_common_provider_consumer` WRITE;
/*!40000 ALTER TABLE `oauth_common_provider_consumer` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_common_provider_consumer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_common_provider_token`
--

DROP TABLE IF EXISTS `oauth_common_provider_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_common_provider_token` (
  `tid` int(10) unsigned DEFAULT '0' COMMENT 'The oauth_common_token.tid this data is related to.',
  `token_key` char(32) NOT NULL COMMENT 'Token key.',
  `created` int(11) NOT NULL DEFAULT '0' COMMENT 'The time that the token was created, as a Unix timestamp.',
  `changed` int(11) NOT NULL DEFAULT '0' COMMENT 'The last time the token was edited, as a Unix timestamp.',
  `services` text COMMENT 'An array of services that the user allowed the consumer to access.',
  `authorized` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'In case its a request token, it checks if the user already authorized the consumer to get an access token.',
  PRIMARY KEY (`token_key`),
  UNIQUE KEY `tid` (`tid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Additional data for OAuth tokens provided by this site.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_common_provider_token`
--

LOCK TABLES `oauth_common_provider_token` WRITE;
/*!40000 ALTER TABLE `oauth_common_provider_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_common_provider_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_common_token`
--

DROP TABLE IF EXISTS `oauth_common_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_common_token` (
  `tid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary ID field for the table. Not used for anything except internal lookups.',
  `csid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The oauth_common_consumer.csid this token is related to.',
  `key_hash` char(40) NOT NULL COMMENT 'SHA1-hash of token_key.',
  `token_key` text NOT NULL COMMENT 'Token key.',
  `secret` text NOT NULL COMMENT 'Token secret.',
  `expires` int(11) NOT NULL DEFAULT '0' COMMENT 'The expiry time for the token, as a Unix timestamp.',
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Token type: request or access.',
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'User ID from user.uid.',
  PRIMARY KEY (`tid`),
  KEY `key_hash` (`key_hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tokens stored on behalf of providers or consumers for...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_common_token`
--

LOCK TABLES `oauth_common_token` WRITE;
/*!40000 ALTER TABLE `oauth_common_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_common_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `queue`
--

DROP TABLE IF EXISTS `queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `queue` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key: Unique item ID.',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT 'The queue name.',
  `data` longblob COMMENT 'The arbitrary data for the item.',
  `expire` int(11) NOT NULL DEFAULT '0' COMMENT 'Timestamp when the claim lease expires on the item.',
  `created` int(11) NOT NULL DEFAULT '0' COMMENT 'Timestamp when the item was created.',
  PRIMARY KEY (`item_id`),
  KEY `name_created` (`name`,`created`),
  KEY `expire` (`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='Stores items in queues.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `queue`
--

LOCK TABLES `queue` WRITE;
/*!40000 ALTER TABLE `queue` DISABLE KEYS */;
INSERT INTO `queue` VALUES (1,'update_fetch_tasks','a:8:{s:4:\"name\";s:6:\"drupal\";s:4:\"info\";a:6:{s:4:\"name\";s:5:\"Block\";s:7:\"package\";s:4:\"Core\";s:7:\"version\";s:4:\"7.52\";s:7:\"project\";s:6:\"drupal\";s:9:\"datestamp\";s:10:\"1479322922\";s:16:\"_info_file_ctime\";i:1480638546;}s:9:\"datestamp\";s:10:\"1479322922\";s:8:\"includes\";a:34:{s:5:\"block\";s:5:\"Block\";s:4:\"blog\";s:4:\"Blog\";s:5:\"color\";s:5:\"Color\";s:7:\"comment\";s:7:\"Comment\";s:10:\"contextual\";s:16:\"Contextual links\";s:9:\"dashboard\";s:9:\"Dashboard\";s:5:\"dblog\";s:16:\"Database logging\";s:5:\"field\";s:5:\"Field\";s:17:\"field_sql_storage\";s:17:\"Field SQL storage\";s:8:\"field_ui\";s:8:\"Field UI\";s:4:\"file\";s:4:\"File\";s:6:\"filter\";s:6:\"Filter\";s:4:\"help\";s:4:\"Help\";s:5:\"image\";s:5:\"Image\";s:4:\"list\";s:4:\"List\";s:4:\"menu\";s:4:\"Menu\";s:4:\"node\";s:4:\"Node\";s:6:\"number\";s:6:\"Number\";s:7:\"options\";s:7:\"Options\";s:7:\"overlay\";s:7:\"Overlay\";s:4:\"path\";s:4:\"Path\";s:3:\"php\";s:10:\"PHP filter\";s:3:\"rdf\";s:3:\"RDF\";s:6:\"search\";s:6:\"Search\";s:8:\"shortcut\";s:8:\"Shortcut\";s:10:\"statistics\";s:10:\"Statistics\";s:6:\"system\";s:6:\"System\";s:8:\"taxonomy\";s:8:\"Taxonomy\";s:4:\"text\";s:4:\"Text\";s:7:\"toolbar\";s:7:\"Toolbar\";s:6:\"update\";s:14:\"Update manager\";s:4:\"user\";s:4:\"User\";s:6:\"bartik\";s:6:\"Bartik\";s:5:\"seven\";s:5:\"Seven\";}s:12:\"project_type\";s:4:\"core\";s:14:\"project_status\";b:1;s:10:\"sub_themes\";a:0:{}s:11:\"base_themes\";a:0:{}}',0,1481055873),(2,'update_fetch_tasks','a:8:{s:4:\"name\";s:6:\"ctools\";s:4:\"info\";a:6:{s:4:\"name\";s:11:\"Bulk Export\";s:7:\"package\";s:16:\"Chaos tool suite\";s:7:\"version\";s:8:\"7.x-1.12\";s:7:\"project\";s:6:\"ctools\";s:9:\"datestamp\";s:10:\"1479787162\";s:16:\"_info_file_ctime\";i:1480638546;}s:9:\"datestamp\";s:10:\"1479787162\";s:8:\"includes\";a:2:{s:11:\"bulk_export\";s:11:\"Bulk Export\";s:6:\"ctools\";s:11:\"Chaos tools\";}s:12:\"project_type\";s:6:\"module\";s:14:\"project_status\";b:1;s:10:\"sub_themes\";a:0:{}s:11:\"base_themes\";a:0:{}}',0,1481055873),(3,'update_fetch_tasks','a:8:{s:4:\"name\";s:6:\"entity\";s:4:\"info\";a:6:{s:4:\"name\";s:10:\"Entity API\";s:7:\"version\";s:7:\"7.x-1.8\";s:7:\"project\";s:6:\"entity\";s:9:\"datestamp\";s:10:\"1474546503\";s:7:\"package\";s:5:\"Other\";s:16:\"_info_file_ctime\";i:1480638546;}s:9:\"datestamp\";s:10:\"1474546503\";s:8:\"includes\";a:1:{s:6:\"entity\";s:10:\"Entity API\";}s:12:\"project_type\";s:6:\"module\";s:14:\"project_status\";b:1;s:10:\"sub_themes\";a:0:{}s:11:\"base_themes\";a:0:{}}',0,1481055873),(4,'update_fetch_tasks','a:8:{s:4:\"name\";s:16:\"field_collection\";s:4:\"info\";a:6:{s:4:\"name\";s:16:\"Field collection\";s:7:\"package\";s:6:\"Fields\";s:7:\"version\";s:14:\"7.x-1.0-beta12\";s:7:\"project\";s:16:\"field_collection\";s:9:\"datestamp\";s:10:\"1479402861\";s:16:\"_info_file_ctime\";i:1480638546;}s:9:\"datestamp\";s:10:\"1479402861\";s:8:\"includes\";a:1:{s:16:\"field_collection\";s:16:\"Field collection\";}s:12:\"project_type\";s:6:\"module\";s:14:\"project_status\";b:1;s:10:\"sub_themes\";a:0:{}s:11:\"base_themes\";a:0:{}}',0,1481055873),(5,'update_fetch_tasks','a:8:{s:4:\"name\";s:13:\"jquery_update\";s:4:\"info\";a:6:{s:4:\"name\";s:13:\"jQuery Update\";s:7:\"package\";s:14:\"User interface\";s:7:\"version\";s:7:\"7.x-2.7\";s:7:\"project\";s:13:\"jquery_update\";s:9:\"datestamp\";s:10:\"1445379855\";s:16:\"_info_file_ctime\";i:1480638310;}s:9:\"datestamp\";s:10:\"1445379855\";s:8:\"includes\";a:1:{s:13:\"jquery_update\";s:13:\"jQuery Update\";}s:12:\"project_type\";s:6:\"module\";s:14:\"project_status\";b:1;s:10:\"sub_themes\";a:0:{}s:11:\"base_themes\";a:0:{}}',0,1481055873),(6,'update_fetch_tasks','a:8:{s:4:\"name\";s:9:\"libraries\";s:4:\"info\";a:6:{s:4:\"name\";s:9:\"Libraries\";s:7:\"version\";s:7:\"7.x-2.3\";s:7:\"project\";s:9:\"libraries\";s:9:\"datestamp\";s:10:\"1463077450\";s:7:\"package\";s:5:\"Other\";s:16:\"_info_file_ctime\";i:1480638310;}s:9:\"datestamp\";s:10:\"1463077450\";s:8:\"includes\";a:1:{s:9:\"libraries\";s:9:\"Libraries\";}s:12:\"project_type\";s:6:\"module\";s:14:\"project_status\";b:1;s:10:\"sub_themes\";a:0:{}s:11:\"base_themes\";a:0:{}}',0,1481055873),(7,'update_fetch_tasks','a:8:{s:4:\"name\";s:4:\"link\";s:4:\"info\";a:6:{s:4:\"name\";s:4:\"Link\";s:7:\"package\";s:6:\"Fields\";s:7:\"version\";s:7:\"7.x-1.4\";s:7:\"project\";s:4:\"link\";s:9:\"datestamp\";s:10:\"1452830642\";s:16:\"_info_file_ctime\";i:1480638310;}s:9:\"datestamp\";s:10:\"1452830642\";s:8:\"includes\";a:1:{s:4:\"link\";s:4:\"Link\";}s:12:\"project_type\";s:6:\"module\";s:14:\"project_status\";b:1;s:10:\"sub_themes\";a:0:{}s:11:\"base_themes\";a:0:{}}',0,1481055873),(8,'update_fetch_tasks','a:8:{s:4:\"name\";s:5:\"oauth\";s:4:\"info\";a:6:{s:4:\"name\";s:5:\"OAuth\";s:7:\"package\";s:5:\"OAuth\";s:7:\"version\";s:7:\"7.x-3.2\";s:7:\"project\";s:5:\"oauth\";s:9:\"datestamp\";s:10:\"1390561406\";s:16:\"_info_file_ctime\";i:1480638310;}s:9:\"datestamp\";s:10:\"1390561406\";s:8:\"includes\";a:1:{s:12:\"oauth_common\";s:5:\"OAuth\";}s:12:\"project_type\";s:6:\"module\";s:14:\"project_status\";b:1;s:10:\"sub_themes\";a:0:{}s:11:\"base_themes\";a:0:{}}',0,1481055873),(9,'update_fetch_tasks','a:8:{s:4:\"name\";s:9:\"quicktabs\";s:4:\"info\";a:6:{s:4:\"name\";s:9:\"Quicktabs\";s:7:\"version\";s:7:\"7.x-3.6\";s:7:\"project\";s:9:\"quicktabs\";s:9:\"datestamp\";s:10:\"1380731929\";s:7:\"package\";s:5:\"Other\";s:16:\"_info_file_ctime\";i:1480638310;}s:9:\"datestamp\";s:10:\"1380731929\";s:8:\"includes\";a:1:{s:9:\"quicktabs\";s:9:\"Quicktabs\";}s:12:\"project_type\";s:6:\"module\";s:14:\"project_status\";b:1;s:10:\"sub_themes\";a:0:{}s:11:\"base_themes\";a:0:{}}',0,1481055873),(10,'update_fetch_tasks','a:8:{s:4:\"name\";s:9:\"superfish\";s:4:\"info\";a:6:{s:4:\"name\";s:9:\"Superfish\";s:7:\"package\";s:14:\"User interface\";s:7:\"version\";s:7:\"7.x-2.0\";s:7:\"project\";s:9:\"superfish\";s:9:\"datestamp\";s:10:\"1448444942\";s:16:\"_info_file_ctime\";i:1480638310;}s:9:\"datestamp\";s:10:\"1448444942\";s:8:\"includes\";a:1:{s:9:\"superfish\";s:9:\"Superfish\";}s:12:\"project_type\";s:6:\"module\";s:14:\"project_status\";b:1;s:10:\"sub_themes\";a:0:{}s:11:\"base_themes\";a:0:{}}',0,1481055873),(11,'update_fetch_tasks','a:8:{s:4:\"name\";s:7:\"twitter\";s:4:\"info\";a:6:{s:4:\"name\";s:7:\"Twitter\";s:7:\"version\";s:8:\"7.x-5.11\";s:7:\"project\";s:7:\"twitter\";s:9:\"datestamp\";s:10:\"1444046332\";s:7:\"package\";s:5:\"Other\";s:16:\"_info_file_ctime\";i:1480638310;}s:9:\"datestamp\";s:10:\"1444046332\";s:8:\"includes\";a:1:{s:7:\"twitter\";s:7:\"Twitter\";}s:12:\"project_type\";s:6:\"module\";s:14:\"project_status\";b:1;s:10:\"sub_themes\";a:0:{}s:11:\"base_themes\";a:0:{}}',0,1481055873),(12,'update_fetch_tasks','a:8:{s:4:\"name\";s:5:\"views\";s:4:\"info\";a:6:{s:4:\"name\";s:5:\"Views\";s:7:\"package\";s:5:\"Views\";s:7:\"version\";s:8:\"7.x-3.14\";s:7:\"project\";s:5:\"views\";s:9:\"datestamp\";s:10:\"1466019588\";s:16:\"_info_file_ctime\";i:1480638546;}s:9:\"datestamp\";s:10:\"1466019588\";s:8:\"includes\";a:2:{s:5:\"views\";s:5:\"Views\";s:8:\"views_ui\";s:8:\"Views UI\";}s:12:\"project_type\";s:6:\"module\";s:14:\"project_status\";b:1;s:10:\"sub_themes\";a:0:{}s:11:\"base_themes\";a:0:{}}',0,1481055873),(13,'update_fetch_tasks','a:8:{s:4:\"name\";s:7:\"webform\";s:4:\"info\";a:6:{s:4:\"name\";s:7:\"Webform\";s:7:\"package\";s:7:\"Webform\";s:7:\"version\";s:8:\"7.x-4.14\";s:7:\"project\";s:7:\"webform\";s:9:\"datestamp\";s:10:\"1472386754\";s:16:\"_info_file_ctime\";i:1480638546;}s:9:\"datestamp\";s:10:\"1472386754\";s:8:\"includes\";a:1:{s:7:\"webform\";s:7:\"Webform\";}s:12:\"project_type\";s:6:\"module\";s:14:\"project_status\";b:1;s:10:\"sub_themes\";a:0:{}s:11:\"base_themes\";a:0:{}}',0,1481055873);
/*!40000 ALTER TABLE `queue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quicktabs`
--

DROP TABLE IF EXISTS `quicktabs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quicktabs` (
  `machine_name` varchar(255) NOT NULL COMMENT 'The primary identifier for a qt block.',
  `ajax` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Whether this is an ajax views block.',
  `hide_empty_tabs` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Whether this tabset hides empty tabs.',
  `default_tab` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Default tab.',
  `title` varchar(255) NOT NULL COMMENT 'The title of this quicktabs block.',
  `tabs` mediumtext NOT NULL COMMENT 'A serialized array of the contents of this qt block.',
  `renderer` varchar(255) NOT NULL COMMENT 'The rendering mechanism.',
  `style` varchar(255) NOT NULL COMMENT 'The tab style.',
  `options` mediumtext COMMENT 'A serialized array of the options for this qt instance.',
  PRIMARY KEY (`machine_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='The quicktabs table.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quicktabs`
--

LOCK TABLES `quicktabs` WRITE;
/*!40000 ALTER TABLE `quicktabs` DISABLE KEYS */;
INSERT INTO `quicktabs` VALUES ('sidebar_tabs',0,0,0,'Tabbed Block','a:2:{i:0;a:5:{s:3:\"bid\";s:31:\"views_delta_mt_services-block_2\";s:10:\"hide_title\";i:1;s:5:\"title\";s:8:\"Services\";s:6:\"weight\";s:4:\"-100\";s:4:\"type\";s:5:\"block\";}i:1;a:5:{s:3:\"bid\";s:35:\"views_delta_mt_latest_posts-block_1\";s:10:\"hide_title\";i:1;s:5:\"title\";s:9:\"Blogposts\";s:6:\"weight\";s:3:\"-99\";s:4:\"type\";s:5:\"block\";}}','quicktabs','nostyle','a:0:{}');
/*!40000 ALTER TABLE `quicktabs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rdf_mapping`
--

DROP TABLE IF EXISTS `rdf_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rdf_mapping` (
  `type` varchar(128) NOT NULL COMMENT 'The name of the entity type a mapping applies to (node, user, comment, etc.).',
  `bundle` varchar(128) NOT NULL COMMENT 'The name of the bundle a mapping applies to.',
  `mapping` longblob COMMENT 'The serialized mapping of the bundle type and fields to RDF terms.',
  PRIMARY KEY (`type`,`bundle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores custom RDF mappings for user defined content types...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rdf_mapping`
--

LOCK TABLES `rdf_mapping` WRITE;
/*!40000 ALTER TABLE `rdf_mapping` DISABLE KEYS */;
INSERT INTO `rdf_mapping` VALUES ('node','article','a:11:{s:11:\"field_image\";a:2:{s:10:\"predicates\";a:2:{i:0;s:8:\"og:image\";i:1;s:12:\"rdfs:seeAlso\";}s:4:\"type\";s:3:\"rel\";}s:10:\"field_tags\";a:2:{s:10:\"predicates\";a:1:{i:0;s:10:\"dc:subject\";}s:4:\"type\";s:3:\"rel\";}s:7:\"rdftype\";a:2:{i:0;s:9:\"sioc:Item\";i:1;s:13:\"foaf:Document\";}s:5:\"title\";a:1:{s:10:\"predicates\";a:1:{i:0;s:8:\"dc:title\";}}s:7:\"created\";a:3:{s:10:\"predicates\";a:2:{i:0;s:7:\"dc:date\";i:1;s:10:\"dc:created\";}s:8:\"datatype\";s:12:\"xsd:dateTime\";s:8:\"callback\";s:12:\"date_iso8601\";}s:7:\"changed\";a:3:{s:10:\"predicates\";a:1:{i:0;s:11:\"dc:modified\";}s:8:\"datatype\";s:12:\"xsd:dateTime\";s:8:\"callback\";s:12:\"date_iso8601\";}s:4:\"body\";a:1:{s:10:\"predicates\";a:1:{i:0;s:15:\"content:encoded\";}}s:3:\"uid\";a:2:{s:10:\"predicates\";a:1:{i:0;s:16:\"sioc:has_creator\";}s:4:\"type\";s:3:\"rel\";}s:4:\"name\";a:1:{s:10:\"predicates\";a:1:{i:0;s:9:\"foaf:name\";}}s:13:\"comment_count\";a:2:{s:10:\"predicates\";a:1:{i:0;s:16:\"sioc:num_replies\";}s:8:\"datatype\";s:11:\"xsd:integer\";}s:13:\"last_activity\";a:3:{s:10:\"predicates\";a:1:{i:0;s:23:\"sioc:last_activity_date\";}s:8:\"datatype\";s:12:\"xsd:dateTime\";s:8:\"callback\";s:12:\"date_iso8601\";}}'),('node','blog','a:9:{s:7:\"rdftype\";a:2:{i:0;s:9:\"sioc:Post\";i:1;s:14:\"sioct:BlogPost\";}s:5:\"title\";a:1:{s:10:\"predicates\";a:1:{i:0;s:8:\"dc:title\";}}s:7:\"created\";a:3:{s:10:\"predicates\";a:2:{i:0;s:7:\"dc:date\";i:1;s:10:\"dc:created\";}s:8:\"datatype\";s:12:\"xsd:dateTime\";s:8:\"callback\";s:12:\"date_iso8601\";}s:7:\"changed\";a:3:{s:10:\"predicates\";a:1:{i:0;s:11:\"dc:modified\";}s:8:\"datatype\";s:12:\"xsd:dateTime\";s:8:\"callback\";s:12:\"date_iso8601\";}s:4:\"body\";a:1:{s:10:\"predicates\";a:1:{i:0;s:15:\"content:encoded\";}}s:3:\"uid\";a:2:{s:10:\"predicates\";a:1:{i:0;s:16:\"sioc:has_creator\";}s:4:\"type\";s:3:\"rel\";}s:4:\"name\";a:1:{s:10:\"predicates\";a:1:{i:0;s:9:\"foaf:name\";}}s:13:\"comment_count\";a:2:{s:10:\"predicates\";a:1:{i:0;s:16:\"sioc:num_replies\";}s:8:\"datatype\";s:11:\"xsd:integer\";}s:13:\"last_activity\";a:3:{s:10:\"predicates\";a:1:{i:0;s:23:\"sioc:last_activity_date\";}s:8:\"datatype\";s:12:\"xsd:dateTime\";s:8:\"callback\";s:12:\"date_iso8601\";}}'),('node','page','a:9:{s:7:\"rdftype\";a:1:{i:0;s:13:\"foaf:Document\";}s:5:\"title\";a:1:{s:10:\"predicates\";a:1:{i:0;s:8:\"dc:title\";}}s:7:\"created\";a:3:{s:10:\"predicates\";a:2:{i:0;s:7:\"dc:date\";i:1;s:10:\"dc:created\";}s:8:\"datatype\";s:12:\"xsd:dateTime\";s:8:\"callback\";s:12:\"date_iso8601\";}s:7:\"changed\";a:3:{s:10:\"predicates\";a:1:{i:0;s:11:\"dc:modified\";}s:8:\"datatype\";s:12:\"xsd:dateTime\";s:8:\"callback\";s:12:\"date_iso8601\";}s:4:\"body\";a:1:{s:10:\"predicates\";a:1:{i:0;s:15:\"content:encoded\";}}s:3:\"uid\";a:2:{s:10:\"predicates\";a:1:{i:0;s:16:\"sioc:has_creator\";}s:4:\"type\";s:3:\"rel\";}s:4:\"name\";a:1:{s:10:\"predicates\";a:1:{i:0;s:9:\"foaf:name\";}}s:13:\"comment_count\";a:2:{s:10:\"predicates\";a:1:{i:0;s:16:\"sioc:num_replies\";}s:8:\"datatype\";s:11:\"xsd:integer\";}s:13:\"last_activity\";a:3:{s:10:\"predicates\";a:1:{i:0;s:23:\"sioc:last_activity_date\";}s:8:\"datatype\";s:12:\"xsd:dateTime\";s:8:\"callback\";s:12:\"date_iso8601\";}}');
/*!40000 ALTER TABLE `rdf_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `rid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key: Unique role ID.',
  `name` varchar(64) NOT NULL DEFAULT '' COMMENT 'Unique role name.',
  `weight` int(11) NOT NULL DEFAULT '0' COMMENT 'The weight of this role in listings and the user interface.',
  PRIMARY KEY (`rid`),
  UNIQUE KEY `name` (`name`),
  KEY `name_weight` (`name`,`weight`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Stores user roles.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (3,'administrator',2),(1,'anonymous user',0),(2,'authenticated user',1);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_permission`
--

DROP TABLE IF EXISTS `role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_permission` (
  `rid` int(10) unsigned NOT NULL COMMENT 'Foreign Key: role.rid.',
  `permission` varchar(128) NOT NULL DEFAULT '' COMMENT 'A single permission granted to the role identified by rid.',
  `module` varchar(255) NOT NULL DEFAULT '' COMMENT 'The module declaring the permission.',
  PRIMARY KEY (`rid`,`permission`),
  KEY `permission` (`permission`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the permissions assigned to user roles.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_permission`
--

LOCK TABLES `role_permission` WRITE;
/*!40000 ALTER TABLE `role_permission` DISABLE KEYS */;
INSERT INTO `role_permission` VALUES (1,'access comments','comment'),(1,'access content','node'),(1,'access site-wide contact form','contact'),(1,'search content','search'),(1,'use text format filtered_html','filter'),(1,'view post access counter','statistics'),(2,'access comments','comment'),(2,'access content','node'),(2,'access overlay','overlay'),(2,'access site-wide contact form','contact'),(2,'access toolbar','toolbar'),(2,'access user contact forms','contact'),(2,'edit webform components','webform'),(2,'post comments','comment'),(2,'search content','search'),(2,'skip comment approval','comment'),(2,'use advanced search','search'),(2,'use text format filtered_html','filter'),(2,'view the administration theme','system'),(3,'access administration pages','system'),(3,'access all views','views'),(3,'access all webform results','webform'),(3,'access comments','comment'),(3,'access content','node'),(3,'access content overview','node'),(3,'access contextual links','contextual'),(3,'access dashboard','dashboard'),(3,'access overlay','overlay'),(3,'access own authorizations','oauth_common'),(3,'access own consumers','oauth_common'),(3,'access own webform results','webform'),(3,'access own webform submissions','webform'),(3,'access site in maintenance mode','system'),(3,'access site reports','system'),(3,'access site-wide contact form','contact'),(3,'access statistics','statistics'),(3,'access toolbar','toolbar'),(3,'access user contact forms','contact'),(3,'access user profiles','user'),(3,'add authenticated twitter accounts','twitter'),(3,'add twitter accounts','twitter'),(3,'administer actions','system'),(3,'administer blocks','block'),(3,'administer comments','comment'),(3,'administer consumers','oauth_common'),(3,'administer contact forms','contact'),(3,'administer content types','node'),(3,'administer field collections','field_collection'),(3,'administer fields','field'),(3,'administer filters','filter'),(3,'administer image styles','image'),(3,'administer menu','menu'),(3,'administer modules','system'),(3,'administer nodes','node'),(3,'administer oauth','oauth_common'),(3,'administer permissions','user'),(3,'administer quicktabs','quicktabs'),(3,'administer search','search'),(3,'administer shortcuts','shortcut'),(3,'administer site configuration','system'),(3,'administer software updates','system'),(3,'administer statistics','statistics'),(3,'administer taxonomy','taxonomy'),(3,'administer themes','system'),(3,'administer twitter','twitter'),(3,'administer twitter accounts','twitter'),(3,'administer url aliases','path'),(3,'administer users','user'),(3,'administer views','views'),(3,'block IP addresses','system'),(3,'bypass node access','node'),(3,'cancel account','user'),(3,'change own username','user'),(3,'create article content','node'),(3,'create page content','node'),(3,'create url aliases','path'),(3,'customize shortcut links','shortcut'),(3,'delete all webform submissions','webform'),(3,'delete any article content','node'),(3,'delete any page content','node'),(3,'delete own article content','node'),(3,'delete own page content','node'),(3,'delete own webform submissions','webform'),(3,'delete revisions','node'),(3,'delete terms in 1','taxonomy'),(3,'edit all webform submissions','webform'),(3,'edit any article content','node'),(3,'edit any page content','node'),(3,'edit own article content','node'),(3,'edit own comments','comment'),(3,'edit own page content','node'),(3,'edit own webform submissions','webform'),(3,'edit terms in 1','taxonomy'),(3,'oauth authorize any consumers','oauth_common'),(3,'oauth register any consumers','oauth_common'),(3,'post comments','comment'),(3,'revert revisions','node'),(3,'search content','search'),(3,'select account cancellation method','user'),(3,'skip comment approval','comment'),(3,'switch shortcut sets','shortcut'),(3,'use advanced search','search'),(3,'use bulk exporter','bulk_export'),(3,'use PHP for settings','php'),(3,'use text format filtered_html','filter'),(3,'use text format full_html','filter'),(3,'view own unpublished content','node'),(3,'view post access counter','statistics'),(3,'view revisions','node'),(3,'view the administration theme','system');
/*!40000 ALTER TABLE `role_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `semaphore`
--

DROP TABLE IF EXISTS `semaphore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `semaphore` (
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT 'Primary Key: Unique name.',
  `value` varchar(255) NOT NULL DEFAULT '' COMMENT 'A value for the semaphore.',
  `expire` double NOT NULL COMMENT 'A Unix timestamp with microseconds indicating when the semaphore should expire.',
  PRIMARY KEY (`name`),
  KEY `value` (`value`),
  KEY `expire` (`expire`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table for holding semaphores, locks, flags, etc. that...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `semaphore`
--

LOCK TABLES `semaphore` WRITE;
/*!40000 ALTER TABLE `semaphore` DISABLE KEYS */;
/*!40000 ALTER TABLE `semaphore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sequences`
--

DROP TABLE IF EXISTS `sequences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sequences` (
  `value` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The value of the sequence.',
  PRIMARY KEY (`value`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COMMENT='Stores IDs.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sequences`
--

LOCK TABLES `sequences` WRITE;
/*!40000 ALTER TABLE `sequences` DISABLE KEYS */;
INSERT INTO `sequences` VALUES (65);
/*!40000 ALTER TABLE `sequences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shortcut_set`
--

DROP TABLE IF EXISTS `shortcut_set`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shortcut_set` (
  `set_name` varchar(32) NOT NULL DEFAULT '' COMMENT 'Primary Key: The menu_links.menu_name under which the set’s links are stored.',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT 'The title of the set.',
  PRIMARY KEY (`set_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores information about sets of shortcuts links.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shortcut_set`
--

LOCK TABLES `shortcut_set` WRITE;
/*!40000 ALTER TABLE `shortcut_set` DISABLE KEYS */;
INSERT INTO `shortcut_set` VALUES ('shortcut-set-1','Default');
/*!40000 ALTER TABLE `shortcut_set` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taxonomy_index`
--

DROP TABLE IF EXISTS `taxonomy_index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taxonomy_index` (
  `nid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The node.nid this record tracks.',
  `tid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The term ID.',
  `sticky` tinyint(4) DEFAULT '0' COMMENT 'Boolean indicating whether the node is sticky.',
  `created` int(11) NOT NULL DEFAULT '0' COMMENT 'The Unix timestamp when the node was created.',
  KEY `term_node` (`tid`,`sticky`,`created`),
  KEY `nid` (`nid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Maintains denormalized information about node/term...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taxonomy_index`
--

LOCK TABLES `taxonomy_index` WRITE;
/*!40000 ALTER TABLE `taxonomy_index` DISABLE KEYS */;
INSERT INTO `taxonomy_index` VALUES (15,10,0,1357400711),(15,11,0,1357400711),(54,11,0,1449694609),(54,13,0,1449694609),(13,12,0,1357311631),(13,13,0,1357311631),(13,14,0,1357311631),(53,13,0,1449694558),(53,10,0,1449694558),(14,12,0,1358523691),(14,13,0,1358523691),(52,13,0,1449694729),(21,16,0,1354626661),(16,15,0,1354618231),(17,15,0,1354608296),(18,16,0,1354604787),(56,35,0,1352012787),(20,15,0,1370354997),(20,27,0,1370354997),(19,17,0,1354630058),(44,23,0,1424356153),(10,19,0,1448635775),(12,21,0,1360334975),(11,21,0,1360075775),(25,22,0,1359989375),(45,20,0,1359902975),(46,20,0,1359816575),(43,24,0,1424356003),(42,25,0,1424355328),(34,26,0,1423221928),(47,24,0,1423135528),(48,25,0,1423049128);
/*!40000 ALTER TABLE `taxonomy_index` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taxonomy_term_data`
--

DROP TABLE IF EXISTS `taxonomy_term_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taxonomy_term_data` (
  `tid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key: Unique term ID.',
  `vid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The taxonomy_vocabulary.vid of the vocabulary to which the term is assigned.',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT 'The term name.',
  `description` longtext COMMENT 'A description of the term.',
  `format` varchar(255) DEFAULT NULL COMMENT 'The filter_format.format of the description.',
  `weight` int(11) NOT NULL DEFAULT '0' COMMENT 'The weight of this term in relation to other terms.',
  PRIMARY KEY (`tid`),
  KEY `taxonomy_tree` (`vid`,`weight`,`name`),
  KEY `vid_name` (`vid`,`name`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COMMENT='Stores term information.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taxonomy_term_data`
--

LOCK TABLES `taxonomy_term_data` WRITE;
/*!40000 ALTER TABLE `taxonomy_term_data` DISABLE KEYS */;
INSERT INTO `taxonomy_term_data` VALUES (10,1,'Tag 1','','filtered_html',0),(11,1,'Longer Tag 2','','filtered_html',0),(12,1,'Very Long Tag 3','','filtered_html',0),(13,1,'Long Tag 4','','filtered_html',0),(14,1,'Tag 5','','filtered_html',0),(15,2,'finance','','filtered_html',0),(16,2,'software','','filtered_html',0),(17,2,'finance energy','','filtered_html',0),(18,2,'trade','','filtered_html',0),(19,3,'Service Category 1','','filtered_html',0),(20,3,'Service Category 2',NULL,NULL,0),(21,3,'Service Category 3',NULL,NULL,0),(22,3,'Service Category 4',NULL,NULL,0),(23,4,'Product Tag 1',NULL,NULL,0),(24,4,'Product Tag 2',NULL,NULL,0),(25,4,'Product Tag 3',NULL,NULL,0),(26,4,'Product Tag 4',NULL,NULL,0),(27,2,'consulting',NULL,NULL,0),(35,2,'energy',NULL,NULL,0);
/*!40000 ALTER TABLE `taxonomy_term_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taxonomy_term_hierarchy`
--

DROP TABLE IF EXISTS `taxonomy_term_hierarchy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taxonomy_term_hierarchy` (
  `tid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Primary Key: The taxonomy_term_data.tid of the term.',
  `parent` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Primary Key: The taxonomy_term_data.tid of the term’s parent. 0 indicates no parent.',
  PRIMARY KEY (`tid`,`parent`),
  KEY `parent` (`parent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the hierarchical relationship between terms.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taxonomy_term_hierarchy`
--

LOCK TABLES `taxonomy_term_hierarchy` WRITE;
/*!40000 ALTER TABLE `taxonomy_term_hierarchy` DISABLE KEYS */;
INSERT INTO `taxonomy_term_hierarchy` VALUES (10,0),(11,0),(12,0),(13,0),(14,0),(15,0),(16,0),(17,0),(18,0),(19,0),(20,0),(21,0),(22,0),(23,0),(24,0),(25,0),(26,0),(27,0),(35,0);
/*!40000 ALTER TABLE `taxonomy_term_hierarchy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taxonomy_vocabulary`
--

DROP TABLE IF EXISTS `taxonomy_vocabulary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taxonomy_vocabulary` (
  `vid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key: Unique vocabulary ID.',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT 'Name of the vocabulary.',
  `machine_name` varchar(255) NOT NULL DEFAULT '' COMMENT 'The vocabulary machine name.',
  `description` longtext COMMENT 'Description of the vocabulary.',
  `hierarchy` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'The type of hierarchy allowed within the vocabulary. (0 = disabled, 1 = single, 2 = multiple)',
  `module` varchar(255) NOT NULL DEFAULT '' COMMENT 'The module which created the vocabulary.',
  `weight` int(11) NOT NULL DEFAULT '0' COMMENT 'The weight of this vocabulary in relation to other vocabularies.',
  PRIMARY KEY (`vid`),
  UNIQUE KEY `machine_name` (`machine_name`),
  KEY `list` (`weight`,`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Stores vocabulary information.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taxonomy_vocabulary`
--

LOCK TABLES `taxonomy_vocabulary` WRITE;
/*!40000 ALTER TABLE `taxonomy_vocabulary` DISABLE KEYS */;
INSERT INTO `taxonomy_vocabulary` VALUES (1,'Tags','tags','Use tags to group articles on similar topics into categories.',0,'taxonomy',0),(2,'Showcase Tags','showcase_tags','',0,'taxonomy',0),(3,'Service Tags','service_tags','',0,'taxonomy',0),(4,'Product Tags','mt_product_tags','',0,'taxonomy',0);
/*!40000 ALTER TABLE `taxonomy_vocabulary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `twitter`
--

DROP TABLE IF EXISTS `twitter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twitter` (
  `twitter_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Unique identifier for each twitter post.',
  `screen_name` varchar(255) NOT NULL DEFAULT '' COMMENT 'Screen Name of the twitter_account user.',
  `created_at` varchar(64) NOT NULL DEFAULT '' COMMENT 'Date and time the twitter post was created.',
  `created_time` int(11) NOT NULL COMMENT 'A duplicate of twitter.created_at in UNIX timestamp format.',
  `text` blob COMMENT 'The text of the twitter post.',
  `source` varchar(255) DEFAULT NULL COMMENT 'The application that created the twitter post.',
  `in_reply_to_status_id` bigint(20) unsigned DEFAULT '0' COMMENT 'Unique identifier of a status this twitter post was replying to.',
  `in_reply_to_user_id` bigint(20) unsigned DEFAULT '0' COMMENT 'Unique identifier for the twitter_account this post was replying to.',
  `in_reply_to_screen_name` varchar(255) DEFAULT NULL COMMENT 'Screen name of the twitter user this post was replying to.',
  `truncated` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Boolean flag indicating whether the twitter status was cut off to fit in the 140 character limit.',
  `entities` longblob COMMENT 'A serialized array of twitter post entities.',
  PRIMARY KEY (`twitter_id`),
  KEY `screen_name` (`screen_name`),
  KEY `created_time` (`created_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores individual Twitter posts.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `twitter`
--

LOCK TABLES `twitter` WRITE;
/*!40000 ALTER TABLE `twitter` DISABLE KEYS */;
/*!40000 ALTER TABLE `twitter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `twitter_account`
--

DROP TABLE IF EXISTS `twitter_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twitter_account` (
  `twitter_uid` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'The unique identifier of the twitter_account.',
  `host` varchar(255) DEFAULT NULL COMMENT 'The host for this account can be a laconi.ca instance',
  `screen_name` varchar(255) DEFAULT NULL COMMENT 'The unique login name of the twitter_account user.',
  `oauth_token` varchar(64) DEFAULT NULL COMMENT 'The token_key for oauth-based access.',
  `oauth_token_secret` varchar(64) DEFAULT NULL COMMENT 'The token_secret for oauth-based access.',
  `name` varchar(64) NOT NULL DEFAULT '' COMMENT 'The full name of the twitter_account user.',
  `description` varchar(255) DEFAULT NULL COMMENT 'The description/biography associated with the twitter_account.',
  `location` varchar(255) DEFAULT NULL COMMENT 'The location of the twitter_account’s owner.',
  `followers_count` int(11) NOT NULL DEFAULT '0' COMMENT 'The number of users following this twitter_account.',
  `friends_count` int(11) NOT NULL DEFAULT '0' COMMENT 'The number of users this twitter_account is following.',
  `statuses_count` int(11) NOT NULL DEFAULT '0' COMMENT 'The total number of status updates performed by a user, excluding direct messages sent.',
  `favourites_count` int(11) NOT NULL DEFAULT '0' COMMENT 'The  number of statuses a user has marked as favorite.',
  `url` varchar(255) DEFAULT NULL COMMENT 'The url of the twitter_account’s home page.',
  `profile_image_url` varchar(255) DEFAULT NULL COMMENT 'The url of the twitter_account’s profile image.',
  `protected` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Boolean flag indicating whether the twitter_account’s posts are publicly accessible.',
  `profile_background_color` varchar(6) NOT NULL DEFAULT '' COMMENT 'hex RGB value for a user’s background color',
  `profile_text_color` varchar(6) NOT NULL DEFAULT '' COMMENT 'hex RGB value for a user’s text color',
  `profile_link_color` varchar(6) NOT NULL DEFAULT '' COMMENT 'hex RGB value for a user’s link color',
  `profile_sidebar_fill_color` varchar(6) NOT NULL DEFAULT '' COMMENT 'hex RGB value for a user’s sidebar color',
  `profile_sidebar_border_color` varchar(6) NOT NULL DEFAULT '' COMMENT 'hex RGB value for a user’s border color',
  `profile_background_image_url` varchar(255) DEFAULT NULL COMMENT 'The url of the twitter_account’s profile image.',
  `profile_background_tile` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'Boolean indicating if a user’s background is tiled.',
  `verified` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'Indicates if a user is verified.',
  `created_at` varchar(64) NOT NULL DEFAULT '' COMMENT 'Date and time the twitter_account was created.',
  `created_time` int(11) NOT NULL COMMENT 'A duplicate of twitter_account.created_at in UNIX timestamp format.',
  `utc_offset` int(11) NOT NULL COMMENT 'A duplicate of twitter_account.created_at in UNIX timestamp format.',
  `import` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Boolean flag indicating whether the twitter_user’s posts should be pulled in by the site.',
  `mentions` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Boolean flag indicating whether the twitter_user’s mentions should be pulled in by the site.',
  `last_refresh` int(11) NOT NULL DEFAULT '0' COMMENT 'A UNIX timestamp marking the date Twitter statuses were last fetched on.',
  `is_global` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Boolean flag indicating if this account is available for global use.',
  `uid` int(10) unsigned DEFAULT '0' COMMENT 'The uid of the user who added this Twitter account.',
  PRIMARY KEY (`twitter_uid`),
  KEY `screen_name` (`screen_name`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores information on specific Twitter user accounts.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `twitter_account`
--

LOCK TABLES `twitter_account` WRITE;
/*!40000 ALTER TABLE `twitter_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `twitter_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `url_alias`
--

DROP TABLE IF EXISTS `url_alias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `url_alias` (
  `pid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'A unique path alias identifier.',
  `source` varchar(255) NOT NULL DEFAULT '' COMMENT 'The Drupal path this alias is for; e.g. node/12.',
  `alias` varchar(255) NOT NULL DEFAULT '' COMMENT 'The alias for this path; e.g. title-of-the-story.',
  `language` varchar(12) NOT NULL DEFAULT '' COMMENT 'The language this alias is for; if ’und’, the alias will be used for unknown languages. Each Drupal path can have an alias for each supported language.',
  PRIMARY KEY (`pid`),
  KEY `alias_language_pid` (`alias`,`language`,`pid`),
  KEY `source_language_pid` (`source`,`language`,`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='A list of URL aliases for Drupal paths; a user may visit...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `url_alias`
--

LOCK TABLES `url_alias` WRITE;
/*!40000 ALTER TABLE `url_alias` DISABLE KEYS */;
INSERT INTO `url_alias` VALUES (2,'node/22','contact-us-alternative','und'),(3,'node/32','contact-us-alternative-2','und'),(4,'node/55','contact-us','und');
/*!40000 ALTER TABLE `url_alias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `variable`
--

DROP TABLE IF EXISTS `variable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `variable` (
  `name` varchar(128) NOT NULL DEFAULT '' COMMENT 'The name of the variable.',
  `value` longblob NOT NULL COMMENT 'The value of the variable.',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Named variable/value pairs created by Drupal core or any...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `variable`
--

LOCK TABLES `variable` WRITE;
/*!40000 ALTER TABLE `variable` DISABLE KEYS */;
INSERT INTO `variable` VALUES ('additional_settings__active_tab_blog','s:12:\"edit-display\";'),('additional_settings__active_tab_mt_benefit','s:13:\"edit-workflow\";'),('additional_settings__active_tab_mt_product','s:15:\"edit-submission\";'),('additional_settings__active_tab_mt_service','s:12:\"edit-display\";'),('additional_settings__active_tab_mt_showcase','s:12:\"edit-display\";'),('additional_settings__active_tab_mt_slideshow_entry','s:9:\"edit-menu\";'),('additional_settings__active_tab_mt_team_member','s:15:\"edit-submission\";'),('additional_settings__active_tab_mt_testimonial','s:13:\"edit-workflow\";'),('additional_settings__active_tab_page','s:12:\"edit-comment\";'),('additional_settings__active_tab_webform','s:12:\"edit-comment\";'),('admin_theme','s:5:\"seven\";'),('anonymous','s:9:\"Anonymous\";'),('block_cache','i:0;'),('blog_block_count','s:2:\"10\";'),('cache','i:0;'),('cache_class_cache_ctools_css','s:14:\"CToolsCssCache\";'),('cache_lifetime','s:1:\"0\";'),('clean_url','i:0;'),('comment_anonymous_blog','i:0;'),('comment_anonymous_mt_benefit','i:0;'),('comment_anonymous_mt_product','i:0;'),('comment_anonymous_mt_service','i:0;'),('comment_anonymous_mt_showcase','i:0;'),('comment_anonymous_mt_slideshow_entry','i:0;'),('comment_anonymous_mt_team_member','i:0;'),('comment_anonymous_mt_testimonial','i:0;'),('comment_anonymous_page','i:0;'),('comment_anonymous_webform','i:0;'),('comment_block_count','i:10;'),('comment_blog','s:1:\"2\";'),('comment_default_mode_blog','i:1;'),('comment_default_mode_mt_benefit','i:1;'),('comment_default_mode_mt_product','i:1;'),('comment_default_mode_mt_service','i:1;'),('comment_default_mode_mt_showcase','i:1;'),('comment_default_mode_mt_slideshow_entry','i:1;'),('comment_default_mode_mt_team_member','i:1;'),('comment_default_mode_mt_testimonial','i:1;'),('comment_default_mode_page','i:1;'),('comment_default_mode_webform','i:1;'),('comment_default_per_page_blog','s:2:\"50\";'),('comment_default_per_page_mt_benefit','s:2:\"50\";'),('comment_default_per_page_mt_product','s:2:\"50\";'),('comment_default_per_page_mt_service','s:2:\"50\";'),('comment_default_per_page_mt_showcase','s:2:\"50\";'),('comment_default_per_page_mt_slideshow_entry','s:2:\"50\";'),('comment_default_per_page_mt_team_member','s:2:\"50\";'),('comment_default_per_page_mt_testimonial','s:2:\"50\";'),('comment_default_per_page_page','s:2:\"50\";'),('comment_default_per_page_webform','s:2:\"50\";'),('comment_form_location_blog','i:1;'),('comment_form_location_mt_benefit','i:1;'),('comment_form_location_mt_product','i:1;'),('comment_form_location_mt_service','i:1;'),('comment_form_location_mt_showcase','i:1;'),('comment_form_location_mt_slideshow_entry','i:1;'),('comment_form_location_mt_team_member','i:1;'),('comment_form_location_mt_testimonial','i:1;'),('comment_form_location_page','i:1;'),('comment_form_location_webform','i:1;'),('comment_mt_benefit','s:1:\"1\";'),('comment_mt_product','s:1:\"1\";'),('comment_mt_service','s:1:\"1\";'),('comment_mt_showcase','s:1:\"2\";'),('comment_mt_slideshow_entry','s:1:\"1\";'),('comment_mt_team_member','s:1:\"1\";'),('comment_mt_testimonial','s:1:\"1\";'),('comment_page','s:1:\"1\";'),('comment_preview_blog','s:1:\"1\";'),('comment_preview_mt_benefit','s:1:\"1\";'),('comment_preview_mt_product','s:1:\"1\";'),('comment_preview_mt_service','s:1:\"1\";'),('comment_preview_mt_showcase','s:1:\"1\";'),('comment_preview_mt_slideshow_entry','s:1:\"1\";'),('comment_preview_mt_team_member','s:1:\"1\";'),('comment_preview_mt_testimonial','s:1:\"1\";'),('comment_preview_page','s:1:\"1\";'),('comment_preview_webform','s:1:\"1\";'),('comment_subject_field_blog','i:1;'),('comment_subject_field_mt_benefit','i:1;'),('comment_subject_field_mt_product','i:1;'),('comment_subject_field_mt_service','i:1;'),('comment_subject_field_mt_showcase','i:1;'),('comment_subject_field_mt_slideshow_entry','i:1;'),('comment_subject_field_mt_team_member','i:1;'),('comment_subject_field_mt_testimonial','i:1;'),('comment_subject_field_page','i:1;'),('comment_subject_field_webform','i:1;'),('comment_webform','s:1:\"1\";'),('contact_default_status','i:1;'),('cron_key','s:43:\"EwyZ8mxwnOlP4LO_-xzLsQfk6yaq-AUoUy5sOVkYQHc\";'),('cron_last','i:1481054270;'),('css_js_query_string','s:6:\"ohuses\";'),('ctools_last_cron','i:1481040341;'),('date_default_timezone','s:13:\"Europe/Athens\";'),('default_nodes_main','s:1:\"2\";'),('drupal_http_request_fails','b:0;'),('drupal_private_key','s:43:\"5KM7yOggrH3nTk3-g054breoH6-qjVuESWKCISHm33o\";'),('email__active_tab','s:24:\"edit-email-admin-created\";'),('entity_cache_tables_created','N;'),('field_bundle_settings_comment__comment_node_mt_testimonial','a:2:{s:10:\"view_modes\";a:1:{s:4:\"full\";a:1:{s:15:\"custom_settings\";b:0;}}s:12:\"extra_fields\";a:2:{s:4:\"form\";a:0:{}s:7:\"display\";a:0:{}}}'),('field_bundle_settings_field_collection_item__field_mt_highlight','a:2:{s:10:\"view_modes\";a:1:{s:4:\"full\";a:1:{s:15:\"custom_settings\";b:0;}}s:12:\"extra_fields\";a:2:{s:4:\"form\";a:0:{}s:7:\"display\";a:0:{}}}'),('field_bundle_settings_field_collection_item__field_mt_special_feature','a:2:{s:10:\"view_modes\";a:1:{s:4:\"full\";a:1:{s:15:\"custom_settings\";b:0;}}s:12:\"extra_fields\";a:2:{s:4:\"form\";a:0:{}s:7:\"display\";a:0:{}}}'),('field_bundle_settings_field_collection_item__field_mt_standard_feature','a:2:{s:10:\"view_modes\";a:1:{s:4:\"full\";a:1:{s:15:\"custom_settings\";b:0;}}s:12:\"extra_fields\";a:2:{s:4:\"form\";a:0:{}s:7:\"display\";a:0:{}}}'),('field_bundle_settings_node__article','a:2:{s:10:\"view_modes\";a:5:{s:6:\"teaser\";a:1:{s:15:\"custom_settings\";b:1;}s:4:\"full\";a:1:{s:15:\"custom_settings\";b:0;}s:3:\"rss\";a:1:{s:15:\"custom_settings\";b:0;}s:12:\"search_index\";a:1:{s:15:\"custom_settings\";b:0;}s:13:\"search_result\";a:1:{s:15:\"custom_settings\";b:0;}}s:12:\"extra_fields\";a:2:{s:4:\"form\";a:1:{s:5:\"title\";a:1:{s:6:\"weight\";s:1:\"0\";}}s:7:\"display\";a:0:{}}}'),('field_bundle_settings_node__blog','a:2:{s:10:\"view_modes\";a:5:{s:6:\"teaser\";a:1:{s:15:\"custom_settings\";b:1;}s:4:\"full\";a:1:{s:15:\"custom_settings\";b:0;}s:3:\"rss\";a:1:{s:15:\"custom_settings\";b:0;}s:12:\"search_index\";a:1:{s:15:\"custom_settings\";b:0;}s:13:\"search_result\";a:1:{s:15:\"custom_settings\";b:0;}}s:12:\"extra_fields\";a:2:{s:4:\"form\";a:1:{s:5:\"title\";a:1:{s:6:\"weight\";s:1:\"0\";}}s:7:\"display\";a:0:{}}}'),('field_bundle_settings_node__mt_benefit','a:2:{s:10:\"view_modes\";a:5:{s:6:\"teaser\";a:1:{s:15:\"custom_settings\";b:1;}s:4:\"full\";a:1:{s:15:\"custom_settings\";b:0;}s:3:\"rss\";a:1:{s:15:\"custom_settings\";b:0;}s:12:\"search_index\";a:1:{s:15:\"custom_settings\";b:0;}s:13:\"search_result\";a:1:{s:15:\"custom_settings\";b:0;}}s:12:\"extra_fields\";a:2:{s:4:\"form\";a:1:{s:5:\"title\";a:1:{s:6:\"weight\";s:1:\"0\";}}s:7:\"display\";a:0:{}}}'),('field_bundle_settings_node__mt_product','a:2:{s:10:\"view_modes\";a:5:{s:6:\"teaser\";a:1:{s:15:\"custom_settings\";b:1;}s:4:\"full\";a:1:{s:15:\"custom_settings\";b:0;}s:3:\"rss\";a:1:{s:15:\"custom_settings\";b:0;}s:12:\"search_index\";a:1:{s:15:\"custom_settings\";b:0;}s:13:\"search_result\";a:1:{s:15:\"custom_settings\";b:0;}}s:12:\"extra_fields\";a:2:{s:4:\"form\";a:1:{s:5:\"title\";a:1:{s:6:\"weight\";s:1:\"0\";}}s:7:\"display\";a:0:{}}}'),('field_bundle_settings_node__mt_service','a:2:{s:10:\"view_modes\";a:5:{s:6:\"teaser\";a:1:{s:15:\"custom_settings\";b:1;}s:4:\"full\";a:1:{s:15:\"custom_settings\";b:0;}s:3:\"rss\";a:1:{s:15:\"custom_settings\";b:0;}s:12:\"search_index\";a:1:{s:15:\"custom_settings\";b:0;}s:13:\"search_result\";a:1:{s:15:\"custom_settings\";b:0;}}s:12:\"extra_fields\";a:2:{s:4:\"form\";a:1:{s:5:\"title\";a:1:{s:6:\"weight\";s:1:\"0\";}}s:7:\"display\";a:0:{}}}'),('field_bundle_settings_node__mt_showcase','a:2:{s:10:\"view_modes\";a:5:{s:6:\"teaser\";a:1:{s:15:\"custom_settings\";b:1;}s:4:\"full\";a:1:{s:15:\"custom_settings\";b:0;}s:3:\"rss\";a:1:{s:15:\"custom_settings\";b:0;}s:12:\"search_index\";a:1:{s:15:\"custom_settings\";b:0;}s:13:\"search_result\";a:1:{s:15:\"custom_settings\";b:0;}}s:12:\"extra_fields\";a:2:{s:4:\"form\";a:1:{s:5:\"title\";a:1:{s:6:\"weight\";s:1:\"0\";}}s:7:\"display\";a:0:{}}}'),('field_bundle_settings_node__mt_slideshow_entry','a:2:{s:10:\"view_modes\";a:5:{s:6:\"teaser\";a:1:{s:15:\"custom_settings\";b:1;}s:4:\"full\";a:1:{s:15:\"custom_settings\";b:0;}s:3:\"rss\";a:1:{s:15:\"custom_settings\";b:0;}s:12:\"search_index\";a:1:{s:15:\"custom_settings\";b:0;}s:13:\"search_result\";a:1:{s:15:\"custom_settings\";b:0;}}s:12:\"extra_fields\";a:2:{s:4:\"form\";a:1:{s:5:\"title\";a:1:{s:6:\"weight\";s:1:\"0\";}}s:7:\"display\";a:0:{}}}'),('field_bundle_settings_node__mt_team_member','a:2:{s:10:\"view_modes\";a:5:{s:6:\"teaser\";a:1:{s:15:\"custom_settings\";b:1;}s:4:\"full\";a:1:{s:15:\"custom_settings\";b:0;}s:3:\"rss\";a:1:{s:15:\"custom_settings\";b:0;}s:12:\"search_index\";a:1:{s:15:\"custom_settings\";b:0;}s:13:\"search_result\";a:1:{s:15:\"custom_settings\";b:0;}}s:12:\"extra_fields\";a:2:{s:4:\"form\";a:1:{s:5:\"title\";a:1:{s:6:\"weight\";s:1:\"0\";}}s:7:\"display\";a:0:{}}}'),('field_bundle_settings_node__mt_testimonial','a:2:{s:10:\"view_modes\";a:5:{s:6:\"teaser\";a:1:{s:15:\"custom_settings\";b:1;}s:4:\"full\";a:1:{s:15:\"custom_settings\";b:0;}s:3:\"rss\";a:1:{s:15:\"custom_settings\";b:0;}s:12:\"search_index\";a:1:{s:15:\"custom_settings\";b:0;}s:13:\"search_result\";a:1:{s:15:\"custom_settings\";b:0;}}s:12:\"extra_fields\";a:2:{s:4:\"form\";a:1:{s:5:\"title\";a:1:{s:6:\"weight\";s:1:\"0\";}}s:7:\"display\";a:0:{}}}'),('field_bundle_settings_node__page','a:2:{s:10:\"view_modes\";a:5:{s:6:\"teaser\";a:1:{s:15:\"custom_settings\";b:1;}s:4:\"full\";a:1:{s:15:\"custom_settings\";b:0;}s:3:\"rss\";a:1:{s:15:\"custom_settings\";b:0;}s:12:\"search_index\";a:1:{s:15:\"custom_settings\";b:0;}s:13:\"search_result\";a:1:{s:15:\"custom_settings\";b:0;}}s:12:\"extra_fields\";a:2:{s:4:\"form\";a:1:{s:5:\"title\";a:1:{s:6:\"weight\";s:1:\"0\";}}s:7:\"display\";a:0:{}}}'),('field_bundle_settings_node__webform','a:2:{s:10:\"view_modes\";a:5:{s:6:\"teaser\";a:1:{s:15:\"custom_settings\";b:1;}s:4:\"full\";a:1:{s:15:\"custom_settings\";b:0;}s:3:\"rss\";a:1:{s:15:\"custom_settings\";b:0;}s:12:\"search_index\";a:1:{s:15:\"custom_settings\";b:0;}s:13:\"search_result\";a:1:{s:15:\"custom_settings\";b:0;}}s:12:\"extra_fields\";a:2:{s:4:\"form\";a:1:{s:5:\"title\";a:1:{s:6:\"weight\";s:1:\"0\";}}s:7:\"display\";a:1:{s:7:\"webform\";a:2:{s:7:\"default\";a:2:{s:6:\"weight\";s:1:\"2\";s:7:\"visible\";b:1;}s:6:\"teaser\";a:2:{s:6:\"weight\";s:2:\"10\";s:7:\"visible\";b:1;}}}}}'),('file_default_scheme','s:6:\"public\";'),('file_private_path','s:0:\"\";'),('file_public_path','s:19:\"sites/default/files\";'),('file_temporary_path','s:23:\"sites/default/files/tmp\";'),('filter_fallback_format','s:10:\"plain_text\";'),('image_toolkit','s:2:\"gd\";'),('install_profile','s:8:\"standard\";'),('install_task','s:4:\"done\";'),('install_time','i:1348838576;'),('jquery_update_compression_type','s:3:\"min\";'),('jquery_update_jquery_admin_version','s:3:\"1.7\";'),('jquery_update_jquery_cdn','s:4:\"none\";'),('jquery_update_jquery_version','s:4:\"1.10\";'),('maintenance_mode','b:0;'),('maintenance_mode_message','s:94:\"LEVEL+ is currently under maintenance. We should be back shortly. Thank you for your patience.\";'),('menu_default_active_menus','a:6:{i:0;s:23:\"menu-footer-bottom-menu\";i:2;s:9:\"main-menu\";i:3;s:10:\"management\";i:4;s:10:\"navigation\";i:5;s:9:\"user-menu\";i:6;s:16:\"menu-footer-menu\";}'),('menu_expanded','a:0:{}'),('menu_masks','a:39:{i:0;i:501;i:1;i:493;i:2;i:250;i:3;i:247;i:4;i:246;i:5;i:245;i:6;i:125;i:7;i:124;i:8;i:123;i:9;i:122;i:10;i:121;i:11;i:117;i:12;i:63;i:13;i:62;i:14;i:61;i:15;i:60;i:16;i:59;i:17;i:58;i:18;i:45;i:19;i:44;i:20;i:31;i:21;i:30;i:22;i:29;i:23;i:28;i:24;i:26;i:25;i:24;i:26;i:22;i:27;i:21;i:28;i:15;i:29;i:14;i:30;i:13;i:31;i:11;i:32;i:10;i:33;i:7;i:34;i:6;i:35;i:5;i:36;i:3;i:37;i:2;i:38;i:1;}'),('menu_options_blog','a:1:{i:0;s:9:\"main-menu\";}'),('menu_options_mt_benefit','a:1:{i:0;s:9:\"main-menu\";}'),('menu_options_mt_product','a:1:{i:0;s:9:\"main-menu\";}'),('menu_options_mt_service','a:1:{i:0;s:9:\"main-menu\";}'),('menu_options_mt_showcase','a:1:{i:0;s:9:\"main-menu\";}'),('menu_options_mt_slideshow_entry','a:0:{}'),('menu_options_mt_team_member','a:1:{i:0;s:9:\"main-menu\";}'),('menu_options_mt_testimonial','a:0:{}'),('menu_options_page','a:1:{i:0;s:9:\"main-menu\";}'),('menu_options_webform','a:1:{i:0;s:9:\"main-menu\";}'),('menu_parent_blog','s:11:\"main-menu:0\";'),('menu_parent_mt_benefit','s:11:\"main-menu:0\";'),('menu_parent_mt_product','s:11:\"main-menu:0\";'),('menu_parent_mt_service','s:11:\"main-menu:0\";'),('menu_parent_mt_showcase','s:11:\"main-menu:0\";'),('menu_parent_mt_slideshow_entry','s:11:\"main-menu:0\";'),('menu_parent_mt_team_member','s:11:\"main-menu:0\";'),('menu_parent_mt_testimonial','s:11:\"main-menu:0\";'),('menu_parent_page','s:11:\"main-menu:0\";'),('menu_parent_webform','s:11:\"main-menu:0\";'),('mtt_twitter_account','s:14:\"morethanthemes\";'),('mtt_twitter_interval','s:1:\"5\";'),('mtt_twitter_title','s:19:\"Latest from Twitter\";'),('mtt_twitter_widget_type','s:7:\"profile\";'),('node_admin_theme','i:1;'),('node_cron_last','s:10:\"1458649773\";'),('node_options_blog','a:2:{i:0;s:6:\"status\";i:1;s:7:\"promote\";}'),('node_options_mt_benefit','a:2:{i:0;s:6:\"status\";i:1;s:7:\"promote\";}'),('node_options_mt_product','a:2:{i:0;s:6:\"status\";i:1;s:7:\"promote\";}'),('node_options_mt_service','a:2:{i:0;s:6:\"status\";i:1;s:7:\"promote\";}'),('node_options_mt_showcase','a:2:{i:0;s:6:\"status\";i:1;s:7:\"promote\";}'),('node_options_mt_slideshow_entry','a:1:{i:0;s:6:\"status\";}'),('node_options_mt_team_member','a:1:{i:0;s:6:\"status\";}'),('node_options_mt_testimonial','a:2:{i:0;s:6:\"status\";i:1;s:7:\"promote\";}'),('node_options_page','a:1:{i:0;s:6:\"status\";}'),('node_options_webform','a:1:{i:0;s:6:\"status\";}'),('node_preview_blog','s:1:\"1\";'),('node_preview_mt_benefit','s:1:\"1\";'),('node_preview_mt_product','s:1:\"1\";'),('node_preview_mt_service','s:1:\"1\";'),('node_preview_mt_showcase','s:1:\"1\";'),('node_preview_mt_slideshow_entry','s:1:\"1\";'),('node_preview_mt_team_member','s:1:\"1\";'),('node_preview_mt_testimonial','s:1:\"1\";'),('node_preview_page','s:1:\"1\";'),('node_preview_webform','s:1:\"1\";'),('node_recent_block_count','s:2:\"10\";'),('node_submitted_blog','i:1;'),('node_submitted_mt_benefit','i:0;'),('node_submitted_mt_product','i:0;'),('node_submitted_mt_service','i:0;'),('node_submitted_mt_showcase','i:1;'),('node_submitted_mt_slideshow_entry','i:0;'),('node_submitted_mt_team_member','i:0;'),('node_submitted_mt_testimonial','i:0;'),('node_submitted_page','i:0;'),('node_submitted_webform','i:0;'),('page_cache_maximum_age','s:1:\"0\";'),('page_compression','i:1;'),('path_alias_whitelist','a:1:{s:4:\"node\";b:1;}'),('preprocess_css','i:0;'),('preprocess_js','i:0;'),('save_continue_mt_benefit','s:19:\"Save and add fields\";'),('save_continue_mt_product','s:19:\"Save and add fields\";'),('save_continue_mt_service','s:19:\"Save and add fields\";'),('save_continue_mt_showcase','s:19:\"Save and add fields\";'),('save_continue_mt_slideshow_entry','s:19:\"Save and add fields\";'),('save_continue_mt_team_member','s:19:\"Save and add fields\";'),('save_continue_mt_testimonial','s:19:\"Save and add fields\";'),('site_403','s:0:\"\";'),('site_404','s:0:\"\";'),('site_default_country','s:2:\"GR\";'),('site_frontpage','s:4:\"node\";'),('site_mail','s:32:\"dev+levelplus@morethanthemes.com\";'),('site_name','s:6:\"LEVEL+\";'),('site_slogan','s:22:\"LEVEL UP YOUR BUSINESS\";'),('statistics_count_content_views','i:1;'),('statistics_count_content_views_ajax','i:0;'),('statistics_day_timestamp','i:1481040340;'),('statistics_enable_access_log','i:0;'),('statistics_flush_accesslog_timer','s:6:\"259200\";'),('superfish_amw_1','i:0;'),('superfish_amw_2','i:0;'),('superfish_arrow_1','i:0;'),('superfish_arrow_2','i:0;'),('superfish_bgf_1','i:0;'),('superfish_bgf_2','i:0;'),('superfish_clone_parent_1','i:0;'),('superfish_clone_parent_2','i:0;'),('superfish_delay_1','s:3:\"200\";'),('superfish_delay_2','s:3:\"800\";'),('superfish_depth_1','s:2:\"-1\";'),('superfish_depth_2','s:2:\"-1\";'),('superfish_dfirstlast_1','i:0;'),('superfish_dfirstlast_2','i:0;'),('superfish_dzebra_1','i:0;'),('superfish_dzebra_2','i:0;'),('superfish_expanded_1','i:0;'),('superfish_expanded_2','i:0;'),('superfish_firstlast_1','i:1;'),('superfish_firstlast_2','i:1;'),('superfish_hhldescription_1','i:0;'),('superfish_hhldescription_2','i:0;'),('superfish_hid_1','i:0;'),('superfish_hid_2','i:1;'),('superfish_hlclass_1','s:0:\"\";'),('superfish_hlclass_2','s:0:\"\";'),('superfish_hldescription_1','i:0;'),('superfish_hldescription_2','i:0;'),('superfish_hldexclude_1','s:0:\"\";'),('superfish_hldexclude_2','s:0:\"\";'),('superfish_hldmenus_1','s:0:\"\";'),('superfish_hldmenus_2','s:0:\"\";'),('superfish_itemcounter_1','i:1;'),('superfish_itemcounter_2','i:1;'),('superfish_itemcount_1','i:0;'),('superfish_itemcount_2','i:1;'),('superfish_itemdepth_1','i:1;'),('superfish_itemdepth_2','i:1;'),('superfish_liclass_1','s:0:\"\";'),('superfish_liclass_2','s:0:\"\";'),('superfish_maxwidth_1','s:2:\"27\";'),('superfish_maxwidth_2','s:2:\"27\";'),('superfish_mcdepth_1','s:1:\"1\";'),('superfish_mcdepth_2','s:1:\"1\";'),('superfish_mcexclude_1','s:0:\"\";'),('superfish_mcexclude_2','s:0:\"\";'),('superfish_mclevels_1','s:1:\"1\";'),('superfish_mclevels_2','s:1:\"1\";'),('superfish_menu_1','s:11:\"main-menu:0\";'),('superfish_menu_2','s:11:\"main-menu:0\";'),('superfish_minwidth_1','s:2:\"15\";'),('superfish_minwidth_2','s:2:\"12\";'),('superfish_multicolumn_1','i:0;'),('superfish_multicolumn_2','i:0;'),('superfish_name_1','s:11:\"Superfish 1\";'),('superfish_name_2','s:11:\"Superfish 2\";'),('superfish_number','s:1:\"2\";'),('superfish_pathclass_1','s:12:\"active-trail\";'),('superfish_pathclass_2','s:12:\"active-trail\";'),('superfish_pathcss_1','s:0:\"\";'),('superfish_pathcss_2','s:0:\"\";'),('superfish_pathlevels_1','s:1:\"1\";'),('superfish_pathlevels_2','s:1:\"1\";'),('superfish_shadow_1','i:0;'),('superfish_shadow_2','i:0;'),('superfish_slide_1','s:4:\"none\";'),('superfish_slide_2','s:8:\"vertical\";'),('superfish_slp','s:344:\"sites/all/libraries/superfish/jquery.hoverIntent.minified.js\r\nsites/all/libraries/superfish/jquery.bgiframe.min.js\r\nsites/all/libraries/superfish/superfish.js\r\nsites/all/libraries/superfish/supersubs.js\r\nsites/all/libraries/superfish/supposition.js\r\nsites/all/libraries/superfish/sftouchscreen.js\r\nsites/all/libraries/superfish/sfsmallscreen.js\";'),('superfish_smallabt_1','s:1:\"1\";'),('superfish_smallabt_2','s:1:\"1\";'),('superfish_smallact_1','s:1:\"1\";'),('superfish_smallact_2','s:1:\"1\";'),('superfish_smallamt_1','s:0:\"\";'),('superfish_smallamt_2','s:0:\"\";'),('superfish_smallasa_1','i:0;'),('superfish_smallasa_2','i:0;'),('superfish_smallbpu_1','s:2:\"px\";'),('superfish_smallbpu_2','s:2:\"px\";'),('superfish_smallbp_1','s:3:\"753\";'),('superfish_smallbp_2','s:3:\"768\";'),('superfish_smallchc_1','i:0;'),('superfish_smallchc_2','i:0;'),('superfish_smallcmc_1','i:0;'),('superfish_smallcmc_2','i:0;'),('superfish_smallech_1','s:0:\"\";'),('superfish_smallech_2','s:0:\"\";'),('superfish_smallecm_1','s:0:\"\";'),('superfish_smallecm_2','s:0:\"\";'),('superfish_smallich_1','s:0:\"\";'),('superfish_smallich_2','s:0:\"\";'),('superfish_smallicm_1','s:0:\"\";'),('superfish_smallicm_2','s:0:\"\";'),('superfish_smallset_1','s:0:\"\";'),('superfish_smallset_2','s:0:\"\";'),('superfish_smallual_1','s:0:\"\";'),('superfish_smallual_2','s:0:\"\";'),('superfish_smalluam_1','s:1:\"0\";'),('superfish_smalluam_2','s:1:\"0\";'),('superfish_smallua_1','s:1:\"0\";'),('superfish_smallua_2','s:1:\"0\";'),('superfish_small_1','s:1:\"0\";'),('superfish_small_2','s:1:\"0\";'),('superfish_speed_1','s:4:\"fast\";'),('superfish_speed_2','s:4:\"fast\";'),('superfish_spp_1','i:1;'),('superfish_spp_2','i:1;'),('superfish_style_1','s:4:\"none\";'),('superfish_style_2','s:4:\"none\";'),('superfish_supersubs_1','i:1;'),('superfish_supersubs_2','i:1;'),('superfish_supersubs_emm_1','i:0;'),('superfish_supersubs_emm_2','i:0;'),('superfish_touchbh_1','s:1:\"2\";'),('superfish_touchbh_2','s:1:\"2\";'),('superfish_touchbpu_1','s:2:\"px\";'),('superfish_touchbpu_2','s:2:\"px\";'),('superfish_touchbp_1','s:3:\"758\";'),('superfish_touchbp_2','s:3:\"768\";'),('superfish_touchdh_1','i:0;'),('superfish_touchdh_2','i:0;'),('superfish_touchual_1','s:19:\"iPhone*Android*iPad\";'),('superfish_touchual_2','s:0:\"\";'),('superfish_touchuam_1','s:1:\"0\";'),('superfish_touchuam_2','s:1:\"0\";'),('superfish_touchua_1','s:1:\"1\";'),('superfish_touchua_2','s:1:\"0\";'),('superfish_touch_1','s:1:\"0\";'),('superfish_touch_2','s:1:\"0\";'),('superfish_type_1','s:10:\"horizontal\";'),('superfish_type_2','s:10:\"horizontal\";'),('superfish_ulclass_1','s:0:\"\";'),('superfish_ulclass_2','s:0:\"\";'),('superfish_use_item_theme_1','i:1;'),('superfish_use_item_theme_2','i:1;'),('superfish_use_link_theme_1','i:1;'),('superfish_use_link_theme_2','i:1;'),('superfish_wraphlt_1','s:0:\"\";'),('superfish_wraphlt_2','s:0:\"\";'),('superfish_wraphl_1','s:0:\"\";'),('superfish_wraphl_2','s:0:\"\";'),('superfish_wrapmul_1','s:0:\"\";'),('superfish_wrapmul_2','s:0:\"\";'),('superfish_wrapul_1','s:0:\"\";'),('superfish_wrapul_2','s:0:\"\";'),('superfish_zebra_1','i:1;'),('superfish_zebra_2','i:1;'),('theme_corporateplus_settings','a:63:{s:11:\"toggle_logo\";i:1;s:11:\"toggle_name\";i:1;s:13:\"toggle_slogan\";i:1;s:24:\"toggle_node_user_picture\";i:0;s:27:\"toggle_comment_user_picture\";i:1;s:32:\"toggle_comment_user_verification\";i:1;s:14:\"toggle_favicon\";i:1;s:16:\"toggle_main_menu\";i:1;s:21:\"toggle_secondary_menu\";i:1;s:12:\"default_logo\";i:1;s:9:\"logo_path\";s:0:\"\";s:11:\"logo_upload\";s:0:\"\";s:15:\"default_favicon\";i:1;s:12:\"favicon_path\";s:0:\"\";s:14:\"favicon_upload\";s:0:\"\";s:18:\"breadcrumb_display\";i:1;s:20:\"breadcrumb_separator\";s:14:\"fa-angle-right\";s:17:\"scrolltop_display\";i:1;s:23:\"frontpage_content_print\";i:0;s:20:\"bootstrap_js_include\";i:1;s:26:\"easy_pie_charts_js_include\";i:1;s:12:\"color_scheme\";s:7:\"default\";s:10:\"form_style\";s:12:\"form-style-1\";s:11:\"layout_mode\";s:4:\"wide\";s:12:\"fixed_header\";i:1;s:24:\"transparent_header_state\";i:1;s:26:\"transparent_header_opacity\";s:2:\"85\";s:21:\"offcanvas_layout_side\";s:19:\"navmenu-fixed-right\";s:20:\"sitename_font_family\";s:6:\"sff-33\";s:18:\"slogan_font_family\";s:7:\"slff-33\";s:20:\"headings_font_family\";s:6:\"hff-33\";s:21:\"paragraph_font_family\";s:5:\"pff-7\";s:24:\"rs_slideshow_full_effect\";s:6:\"random\";s:29:\"rs_slideshow_full_effect_time\";s:2:\"10\";s:29:\"rs_slideshow_fullwidth_effect\";s:6:\"random\";s:34:\"rs_slideshow_fullwidth_effect_time\";s:2:\"10\";s:37:\"rs_slideshow_fullwidth_initial_height\";s:3:\"540\";s:25:\"rs_slideshow_boxed_effect\";s:6:\"random\";s:30:\"rs_slideshow_boxed_effect_time\";s:2:\"10\";s:33:\"rs_slideshow_boxed_initial_height\";s:3:\"540\";s:28:\"rs_slideshow_internal_effect\";s:4:\"fade\";s:33:\"rs_slideshow_internal_effect_time\";s:2:\"10\";s:27:\"rs_slideshow_initial_height\";s:3:\"540\";s:28:\"rs_slideshow_caption_opacity\";s:2:\"30\";s:28:\"testimonial_slideshow_effect\";s:5:\"slide\";s:33:\"testimonial_slideshow_effect_time\";s:1:\"5\";s:11:\"owl_include\";i:1;s:12:\"owl_autoplay\";i:0;s:15:\"owl_effect_time\";s:1:\"5\";s:17:\"owl_team_autoplay\";i:0;s:20:\"owl_team_effect_time\";s:1:\"5\";s:31:\"responsive_multilevelmenu_state\";i:1;s:14:\"parallax_state\";i:1;s:14:\"video_bg_state\";i:1;s:29:\"parallax_and_video_bg_opacity\";s:2:\"65\";s:13:\"google_map_js\";i:1;s:19:\"google_map_latitude\";s:9:\"40.726576\";s:20:\"google_map_longitude\";s:10:\"-74.046822\";s:15:\"google_map_zoom\";s:2:\"13\";s:17:\"google_map_canvas\";s:10:\"map-canvas\";s:18:\"isotope_js_include\";i:1;s:14:\"isotope_layout\";s:7:\"masonry\";s:16:\"tabs__active_tab\";s:17:\"edit-layout-modes\";}'),('theme_default','s:9:\"levelplus\";'),('theme_finance_settings','a:40:{s:11:\"toggle_logo\";i:1;s:11:\"toggle_name\";i:1;s:13:\"toggle_slogan\";i:1;s:24:\"toggle_node_user_picture\";i:0;s:27:\"toggle_comment_user_picture\";i:1;s:32:\"toggle_comment_user_verification\";i:1;s:14:\"toggle_favicon\";i:1;s:16:\"toggle_main_menu\";i:1;s:21:\"toggle_secondary_menu\";i:1;s:12:\"default_logo\";i:1;s:9:\"logo_path\";s:0:\"\";s:11:\"logo_upload\";s:0:\"\";s:15:\"default_favicon\";i:1;s:12:\"favicon_path\";s:0:\"\";s:14:\"favicon_upload\";s:0:\"\";s:18:\"breadcrumb_display\";i:1;s:17:\"scrolltop_display\";i:1;s:23:\"frontpage_content_print\";i:0;s:20:\"bootstrap_js_include\";i:1;s:12:\"color_scheme\";s:7:\"default\";s:21:\"internal_banner_cover\";i:1;s:20:\"sitename_font_family\";s:5:\"sff-7\";s:18:\"slogan_font_family\";s:6:\"slff-7\";s:20:\"headings_font_family\";s:5:\"hff-7\";s:21:\"paragraph_font_family\";s:5:\"pff-7\";s:16:\"slideshow_effect\";s:4:\"fade\";s:21:\"slideshow_effect_time\";s:2:\"10\";s:15:\"slideshow_cover\";i:1;s:21:\"responsive_menu_state\";i:0;s:27:\"responsive_menu_switchwidth\";s:3:\"760\";s:29:\"responsive_menu_topoptiontext\";s:13:\"Select a page\";s:25:\"responsive_menu_optgroups\";i:1;s:22:\"responsive_menu_nested\";i:1;s:13:\"google_map_js\";i:1;s:19:\"google_map_latitude\";s:9:\"40.726576\";s:20:\"google_map_longitude\";s:10:\"-74.046822\";s:15:\"google_map_zoom\";s:2:\"13\";s:15:\"google_map_text\";s:10:\"Map on/off\";s:17:\"google_map_canvas\";s:10:\"map-canvas\";s:16:\"tabs__active_tab\";s:19:\"edit-basic-settings\";}'),('theme_levelplus_settings','a:113:{s:11:\"toggle_logo\";i:1;s:11:\"toggle_name\";i:1;s:13:\"toggle_slogan\";i:1;s:24:\"toggle_node_user_picture\";i:1;s:27:\"toggle_comment_user_picture\";i:1;s:32:\"toggle_comment_user_verification\";i:1;s:14:\"toggle_favicon\";i:1;s:16:\"toggle_main_menu\";i:1;s:21:\"toggle_secondary_menu\";i:1;s:12:\"default_logo\";i:1;s:9:\"logo_path\";s:0:\"\";s:11:\"logo_upload\";s:0:\"\";s:15:\"default_favicon\";i:1;s:12:\"favicon_path\";s:0:\"\";s:14:\"favicon_upload\";s:0:\"\";s:18:\"breadcrumb_display\";i:1;s:20:\"breadcrumb_separator\";s:14:\"fa-angle-right\";s:17:\"scrolltop_display\";i:1;s:23:\"frontpage_content_print\";i:0;s:20:\"bootstrap_js_include\";i:1;s:21:\"alternative_logo_path\";s:0:\"\";s:20:\"alternative_logo_fid\";s:3:\"290\";s:27:\"alternative_logo_inner_path\";s:0:\"\";s:26:\"alternative_logo_inner_fid\";i:0;s:12:\"color_scheme\";s:4:\"blue\";s:10:\"form_style\";s:12:\"form-style-1\";s:16:\"animations_state\";i:1;s:23:\"header_layout_container\";s:15:\"container-fluid\";s:12:\"fixed_header\";i:1;s:24:\"transparent_header_state\";i:0;s:26:\"transparent_header_opacity\";s:2:\"85\";s:28:\"content_top_layout_container\";s:9:\"container\";s:28:\"content_top_background_color\";s:12:\"white-region\";s:28:\"content_top_animation_effect\";s:12:\"no-animation\";s:29:\"main_content_animation_effect\";s:12:\"no-animation\";s:31:\"content_bottom_background_color\";s:12:\"white-region\";s:31:\"content_bottom_animation_effect\";s:12:\"no-animation\";s:29:\"featured_top_layout_container\";s:15:\"container-fluid\";s:29:\"featured_top_background_color\";s:12:\"white-region\";s:29:\"featured_top_animation_effect\";s:12:\"no-animation\";s:25:\"featured_layout_container\";s:9:\"container\";s:25:\"featured_background_color\";s:17:\"light-gray-region\";s:25:\"featured_animation_effect\";s:12:\"no-animation\";s:32:\"featured_bottom_layout_container\";s:9:\"container\";s:32:\"featured_bottom_background_color\";s:12:\"white-region\";s:32:\"featured_bottom_animation_effect\";s:12:\"no-animation\";s:28:\"highlighted_layout_container\";s:9:\"container\";s:28:\"highlighted_background_color\";s:17:\"light-gray-region\";s:28:\"highlighted_animation_effect\";s:12:\"no-animation\";s:35:\"highlighted_bottom_layout_container\";s:9:\"container\";s:35:\"highlighted_bottom_background_color\";s:12:\"white-region\";s:35:\"highlighted_bottom_animation_effect\";s:8:\"fadeInUp\";s:27:\"footer_top_background_color\";s:7:\"bicolor\";s:27:\"footer_top_animation_effect\";s:6:\"fadeIn\";s:23:\"footer_layout_container\";s:9:\"container\";s:23:\"footer_background_color\";s:12:\"white-region\";s:23:\"footer_animation_effect\";s:6:\"fadeIn\";s:26:\"subfooter_layout_container\";s:9:\"container\";s:26:\"subfooter_background_color\";s:17:\"light-gray-region\";s:12:\"reading_time\";i:1;s:13:\"post_progress\";i:1;s:18:\"affix_admin_height\";s:2:\"64\";s:24:\"affix_fixedHeader_height\";s:2:\"70\";s:11:\"layout_mode\";s:4:\"wide\";s:20:\"sitename_font_family\";s:5:\"sff-5\";s:18:\"slogan_font_family\";s:6:\"slff-5\";s:20:\"headings_font_family\";s:5:\"hff-5\";s:21:\"paragraph_font_family\";s:5:\"pff-5\";s:24:\"rs_slideshow_full_effect\";s:6:\"random\";s:29:\"rs_slideshow_full_effect_time\";s:2:\"10\";s:34:\"rs_slideshow_full_bullets_position\";s:4:\"left\";s:29:\"rs_slideshow_fullwidth_effect\";s:6:\"random\";s:34:\"rs_slideshow_fullwidth_effect_time\";s:2:\"10\";s:37:\"rs_slideshow_fullwidth_initial_height\";s:3:\"630\";s:39:\"rs_slideshow_fullwidth_bullets_position\";s:6:\"center\";s:25:\"rs_slideshow_boxed_effect\";s:6:\"random\";s:30:\"rs_slideshow_boxed_effect_time\";s:2:\"10\";s:33:\"rs_slideshow_boxed_initial_height\";s:3:\"630\";s:35:\"rs_slideshow_boxed_bullets_position\";s:6:\"center\";s:28:\"rs_slideshow_internal_effect\";s:6:\"random\";s:33:\"rs_slideshow_internal_effect_time\";s:2:\"10\";s:36:\"rs_slideshow_internal_initial_height\";s:3:\"630\";s:38:\"rs_slideshow_internal_bullets_position\";s:6:\"center\";s:28:\"rs_slideshow_caption_opacity\";s:2:\"40\";s:28:\"testimonial_slideshow_effect\";s:5:\"slide\";s:33:\"testimonial_slideshow_effect_time\";s:1:\"5\";s:31:\"in_page_images_slideshow_effect\";s:5:\"slide\";s:11:\"owl_include\";i:1;s:12:\"owl_autoplay\";i:0;s:15:\"owl_effect_time\";s:1:\"5\";s:17:\"owl_team_autoplay\";i:0;s:20:\"owl_team_effect_time\";s:1:\"5\";s:25:\"owl_testimonials_autoplay\";i:1;s:28:\"owl_testimonials_effect_time\";s:1:\"5\";s:18:\"owl_logos_autoplay\";i:1;s:21:\"owl_logos_effect_time\";s:1:\"5\";s:21:\"responsive_menu_state\";i:1;s:38:\"responsive_menu_header_top_switchwidth\";s:3:\"767\";s:40:\"responsive_menu_header_first_switchwidth\";s:4:\"5000\";s:40:\"responsive_menu_header_third_switchwidth\";s:3:\"767\";s:27:\"parallax_and_video_bg_state\";s:4:\"none\";s:29:\"parallax_and_video_bg_opacity\";s:2:\"75\";s:14:\"google_map_key\";s:39:\"AIzaSyDyL-4KyWw_3hp-0jmZLGcJ3zMWkkJ44VI\";s:13:\"google_map_js\";i:1;s:19:\"google_map_latitude\";s:9:\"40.726576\";s:20:\"google_map_longitude\";s:10:\"-74.046822\";s:15:\"google_map_zoom\";s:2:\"13\";s:17:\"google_map_canvas\";s:10:\"map-canvas\";s:15:\"google_map_show\";s:8:\"Show Map\";s:15:\"google_map_hide\";s:8:\"Hide Map\";s:18:\"isotope_js_include\";i:1;s:14:\"isotope_layout\";s:7:\"masonry\";s:16:\"tabs__active_tab\";s:26:\"edit-parallax-and-video-bg\";}'),('theme_marketsquare_settings','a:41:{s:11:\"toggle_logo\";i:1;s:11:\"toggle_name\";i:1;s:13:\"toggle_slogan\";i:1;s:24:\"toggle_node_user_picture\";i:0;s:27:\"toggle_comment_user_picture\";i:1;s:32:\"toggle_comment_user_verification\";i:1;s:14:\"toggle_favicon\";i:1;s:16:\"toggle_main_menu\";i:1;s:21:\"toggle_secondary_menu\";i:1;s:12:\"default_logo\";i:1;s:9:\"logo_path\";s:0:\"\";s:11:\"logo_upload\";s:0:\"\";s:15:\"default_favicon\";i:1;s:12:\"favicon_path\";s:0:\"\";s:14:\"favicon_upload\";s:0:\"\";s:18:\"breadcrumb_display\";i:1;s:20:\"breadcrumb_separator\";s:2:\"»\";s:12:\"fixed_header\";i:1;s:17:\"scrolltop_display\";i:1;s:23:\"frontpage_content_print\";i:0;s:12:\"color_scheme\";s:7:\"default\";s:13:\"smart_borders\";i:1;s:20:\"sitename_font_family\";s:6:\"sff-17\";s:18:\"slogan_font_family\";s:6:\"slff-1\";s:20:\"headings_font_family\";s:5:\"hff-1\";s:21:\"paragraph_font_family\";s:5:\"pff-1\";s:16:\"slideshow_effect\";s:4:\"fade\";s:21:\"slideshow_effect_time\";s:2:\"10\";s:21:\"responsive_menu_state\";i:0;s:27:\"responsive_menu_switchwidth\";s:3:\"760\";s:29:\"responsive_menu_topoptiontext\";s:13:\"Select a page\";s:25:\"responsive_menu_optgroups\";i:1;s:22:\"responsive_menu_nested\";i:1;s:13:\"google_map_js\";i:1;s:19:\"google_map_latitude\";s:9:\"40.726576\";s:20:\"google_map_longitude\";s:10:\"-74.046822\";s:15:\"google_map_zoom\";s:2:\"13\";s:15:\"google_map_show\";s:8:\"Show Map\";s:15:\"google_map_hide\";s:8:\"Hide Map\";s:17:\"google_map_canvas\";s:10:\"map-canvas\";s:16:\"tabs__active_tab\";s:14:\"edit-looknfeel\";}'),('theme_mobileplus_settings','a:41:{s:11:\"toggle_logo\";i:1;s:11:\"toggle_name\";i:1;s:13:\"toggle_slogan\";i:1;s:24:\"toggle_node_user_picture\";i:1;s:27:\"toggle_comment_user_picture\";i:1;s:32:\"toggle_comment_user_verification\";i:1;s:14:\"toggle_favicon\";i:1;s:16:\"toggle_main_menu\";i:1;s:21:\"toggle_secondary_menu\";i:1;s:12:\"default_logo\";i:1;s:9:\"logo_path\";s:0:\"\";s:11:\"logo_upload\";s:0:\"\";s:15:\"default_favicon\";i:1;s:12:\"favicon_path\";s:0:\"\";s:14:\"favicon_upload\";s:0:\"\";s:18:\"breadcrumb_display\";i:1;s:17:\"scrolltop_display\";i:1;s:23:\"frontpage_content_print\";i:0;s:20:\"bootstrap_js_include\";i:1;s:12:\"color_scheme\";s:7:\"default\";s:13:\"caption_style\";s:7:\"style-1\";s:20:\"sitename_font_family\";s:5:\"sff-7\";s:18:\"slogan_font_family\";s:6:\"slff-7\";s:20:\"headings_font_family\";s:5:\"hff-7\";s:21:\"paragraph_font_family\";s:5:\"pff-7\";s:16:\"slideshow_effect\";s:4:\"fade\";s:21:\"slideshow_effect_time\";s:2:\"10\";s:28:\"testimonial_slideshow_effect\";s:5:\"slide\";s:33:\"testimonial_slideshow_effect_time\";s:1:\"5\";s:21:\"responsive_menu_state\";i:0;s:27:\"responsive_menu_switchwidth\";s:3:\"760\";s:29:\"responsive_menu_topoptiontext\";s:13:\"Select a page\";s:25:\"responsive_menu_optgroups\";i:1;s:22:\"responsive_menu_nested\";i:1;s:13:\"google_map_js\";i:1;s:19:\"google_map_latitude\";s:9:\"40.726576\";s:20:\"google_map_longitude\";s:10:\"-74.046822\";s:15:\"google_map_zoom\";s:2:\"13\";s:15:\"google_map_text\";s:10:\"Map on/off\";s:17:\"google_map_canvas\";s:10:\"map-canvas\";s:16:\"tabs__active_tab\";s:14:\"edit-looknfeel\";}'),('theme_socialstyle_settings','a:32:{s:11:\"toggle_logo\";i:0;s:11:\"toggle_name\";i:1;s:13:\"toggle_slogan\";i:1;s:24:\"toggle_node_user_picture\";i:0;s:27:\"toggle_comment_user_picture\";i:1;s:32:\"toggle_comment_user_verification\";i:1;s:14:\"toggle_favicon\";i:1;s:16:\"toggle_main_menu\";i:1;s:21:\"toggle_secondary_menu\";i:1;s:12:\"default_logo\";i:1;s:9:\"logo_path\";s:0:\"\";s:11:\"logo_upload\";s:0:\"\";s:15:\"default_favicon\";i:1;s:12:\"favicon_path\";s:0:\"\";s:14:\"favicon_upload\";s:0:\"\";s:18:\"breadcrumb_display\";i:1;s:20:\"breadcrumb_separator\";s:2:\"»\";s:17:\"scrolltop_display\";i:1;s:12:\"color_scheme\";s:7:\"default\";s:20:\"sitename_font_family\";s:5:\"sff-4\";s:18:\"slogan_font_family\";s:6:\"slff-2\";s:20:\"headings_font_family\";s:5:\"hff-2\";s:29:\"slideshow_heading_font_family\";s:6:\"shff-4\";s:21:\"paragraph_font_family\";s:5:\"pff-2\";s:16:\"slideshow_effect\";s:4:\"fade\";s:21:\"slideshow_effect_time\";s:2:\"10\";s:21:\"responsive_menu_state\";i:1;s:27:\"responsive_menu_switchwidth\";s:3:\"760\";s:29:\"responsive_menu_topoptiontext\";s:13:\"Select a page\";s:25:\"responsive_menu_optgroups\";i:1;s:22:\"responsive_menu_nested\";i:1;s:16:\"tabs__active_tab\";s:20:\"edit-responsive-menu\";}'),('theme_startupgrowth_settings','a:46:{s:11:\"toggle_logo\";i:1;s:11:\"toggle_name\";i:1;s:13:\"toggle_slogan\";i:1;s:24:\"toggle_node_user_picture\";i:0;s:27:\"toggle_comment_user_picture\";i:1;s:32:\"toggle_comment_user_verification\";i:1;s:14:\"toggle_favicon\";i:1;s:16:\"toggle_main_menu\";i:1;s:21:\"toggle_secondary_menu\";i:1;s:12:\"default_logo\";i:1;s:9:\"logo_path\";s:0:\"\";s:11:\"logo_upload\";s:0:\"\";s:15:\"default_favicon\";i:1;s:12:\"favicon_path\";s:0:\"\";s:14:\"favicon_upload\";s:0:\"\";s:18:\"breadcrumb_display\";i:1;s:12:\"fixed_header\";i:1;s:17:\"scrolltop_display\";i:1;s:23:\"frontpage_content_print\";i:0;s:20:\"bootstrap_js_include\";i:1;s:12:\"color_scheme\";s:7:\"default\";s:10:\"form_style\";s:12:\"form-style-1\";s:20:\"sitename_font_family\";s:5:\"sff-7\";s:18:\"slogan_font_family\";s:6:\"slff-7\";s:20:\"headings_font_family\";s:5:\"hff-7\";s:21:\"paragraph_font_family\";s:5:\"pff-7\";s:24:\"rs_slideshow_full_effect\";s:6:\"random\";s:29:\"rs_slideshow_full_effect_time\";s:2:\"10\";s:25:\"rs_slideshow_boxed_effect\";s:6:\"random\";s:30:\"rs_slideshow_boxed_effect_time\";s:2:\"10\";s:31:\"responsive_multilevelmenu_state\";i:1;s:21:\"responsive_menu_state\";i:0;s:27:\"responsive_menu_switchwidth\";s:3:\"760\";s:29:\"responsive_menu_topoptiontext\";s:13:\"Select a page\";s:25:\"responsive_menu_optgroups\";i:1;s:22:\"responsive_menu_nested\";i:1;s:14:\"parallax_state\";i:1;s:16:\"parallax_opacity\";s:2:\"80\";s:13:\"google_map_js\";i:1;s:19:\"google_map_latitude\";s:9:\"40.726576\";s:20:\"google_map_longitude\";s:10:\"-74.046822\";s:15:\"google_map_zoom\";s:2:\"13\";s:17:\"google_map_canvas\";s:10:\"map-canvas\";s:18:\"isotope_js_include\";i:1;s:14:\"isotope_layout\";s:8:\"vertical\";s:16:\"tabs__active_tab\";s:12:\"edit-isotope\";}'),('theme_successinc_settings','a:34:{s:11:\"toggle_logo\";i:0;s:11:\"toggle_name\";i:1;s:13:\"toggle_slogan\";i:1;s:24:\"toggle_node_user_picture\";i:0;s:27:\"toggle_comment_user_picture\";i:1;s:32:\"toggle_comment_user_verification\";i:1;s:14:\"toggle_favicon\";i:1;s:16:\"toggle_main_menu\";i:1;s:21:\"toggle_secondary_menu\";i:1;s:12:\"default_logo\";i:1;s:9:\"logo_path\";s:0:\"\";s:11:\"logo_upload\";s:0:\"\";s:15:\"default_favicon\";i:1;s:12:\"favicon_path\";s:0:\"\";s:14:\"favicon_upload\";s:0:\"\";s:18:\"breadcrumb_display\";i:1;s:20:\"breadcrumb_separator\";s:2:\"»\";s:17:\"scrolltop_display\";i:1;s:17:\"frontpage_emulate\";i:0;s:23:\"frontpage_content_print\";i:0;s:12:\"color_scheme\";s:7:\"default\";s:20:\"sitename_font_family\";s:5:\"sff-6\";s:18:\"slogan_font_family\";s:6:\"slff-1\";s:20:\"headings_font_family\";s:5:\"hff-6\";s:21:\"paragraph_font_family\";s:5:\"pff-1\";s:16:\"slideshow_effect\";s:5:\"slide\";s:26:\"slideshow_effect_direction\";s:10:\"horizontal\";s:21:\"slideshow_effect_time\";s:2:\"10\";s:21:\"responsive_menu_state\";i:1;s:27:\"responsive_menu_switchwidth\";s:3:\"760\";s:29:\"responsive_menu_topoptiontext\";s:13:\"Select a page\";s:25:\"responsive_menu_optgroups\";i:1;s:22:\"responsive_menu_nested\";i:1;s:16:\"tabs__active_tab\";s:20:\"edit-responsive-menu\";}'),('twitter_align','s:4:\"none\";'),('twitter_api','s:23:\"https://api.twitter.com\";'),('twitter_consumer_key','s:0:\"\";'),('twitter_consumer_secret','s:0:\"\";'),('twitter_conversation','i:0;'),('twitter_expire','s:7:\"2592000\";'),('twitter_host','s:18:\"http://twitter.com\";'),('twitter_import','i:1;'),('twitter_media','i:0;'),('twitter_oauth_callback_url','s:13:\"twitter/oauth\";'),('twitter_search','s:20:\"https://twitter.com/\";'),('twitter_tinyurl','s:18:\"http://tinyurl.com\";'),('twitter_use_default_views','i:1;'),('update_check_disabled','i:0;'),('update_check_frequency','s:1:\"1\";'),('update_last_check','i:1481054269;'),('update_last_email_notification','i:1463894890;'),('update_notification_threshold','s:3:\"all\";'),('update_notify_emails','a:1:{i:0;s:32:\"dev+levelplus@morethanthemes.com\";}'),('user_admin_role','s:1:\"3\";'),('user_cancel_method','s:17:\"user_cancel_block\";'),('user_email_verification','i:1;'),('user_mail_cancel_confirm_body','s:381:\"[user:name],\r\n\r\nA request to cancel your account has been made at [site:name].\r\n\r\nYou may now cancel your account on [site:url-brief] by clicking this link or copying and pasting it into your browser:\r\n\r\n[user:cancel-url]\r\n\r\nNOTE: The cancellation of your account is not reversible.\r\n\r\nThis link expires in one day and nothing will happen if it is not used.\r\n\r\n--  [site:name] team\";'),('user_mail_cancel_confirm_subject','s:59:\"Account cancellation request for [user:name] at [site:name]\";'),('user_mail_password_reset_body','s:407:\"[user:name],\r\n\r\nA request to reset the password for your account has been made at [site:name].\r\n\r\nYou may now log in by clicking this link or copying and pasting it to your browser:\r\n\r\n[user:one-time-login-url]\r\n\r\nThis link can only be used once to log in and will lead you to a page where you can set your password. It expires after one day and nothing will happen if it\'s not used.\r\n\r\n--  [site:name] team\";'),('user_mail_password_reset_subject','s:60:\"Replacement login information for [user:name] at [site:name]\";'),('user_mail_register_admin_created_body','s:476:\"[user:name],\r\n\r\nA site administrator at [site:name] has created an account for you. You may now log in by clicking this link or copying and pasting it to your browser:\r\n\r\n[user:one-time-login-url]\r\n\r\nThis link can only be used once to log in and will lead you to a page where you can set your password.\r\n\r\nAfter setting your password, you will be able to log in at [site:login-url] in the future using:\r\n\r\nusername: [user:name]\r\npassword: Your password\r\n\r\n--  [site:name] team\";'),('user_mail_register_admin_created_subject','s:58:\"An administrator created an account for you at [site:name]\";'),('user_mail_register_no_approval_required_body','s:450:\"[user:name],\r\n\r\nThank you for registering at [site:name]. You may now log in by clicking this link or copying and pasting it to your browser:\r\n\r\n[user:one-time-login-url]\r\n\r\nThis link can only be used once to log in and will lead you to a page where you can set your password.\r\n\r\nAfter setting your password, you will be able to log in at [site:login-url] in the future using:\r\n\r\nusername: [user:name]\r\npassword: Your password\r\n\r\n--  [site:name] team\";'),('user_mail_register_no_approval_required_subject','s:46:\"Account details for [user:name] at [site:name]\";'),('user_mail_register_pending_approval_body','s:287:\"[user:name],\r\n\r\nThank you for registering at [site:name]. Your application for an account is currently pending approval. Once it has been approved, you will receive another e-mail containing information about how to log in, set your password, and other details.\r\n\r\n\r\n--  [site:name] team\";'),('user_mail_register_pending_approval_subject','s:71:\"Account details for [user:name] at [site:name] (pending admin approval)\";'),('user_mail_status_activated_body','s:461:\"[user:name],\r\n\r\nYour account at [site:name] has been activated.\r\n\r\nYou may now log in by clicking this link or copying and pasting it into your browser:\r\n\r\n[user:one-time-login-url]\r\n\r\nThis link can only be used once to log in and will lead you to a page where you can set your password.\r\n\r\nAfter setting your password, you will be able to log in at [site:login-url] in the future using:\r\n\r\nusername: [user:name]\r\npassword: Your password\r\n\r\n--  [site:name] team\";'),('user_mail_status_activated_notify','i:1;'),('user_mail_status_activated_subject','s:57:\"Account details for [user:name] at [site:name] (approved)\";'),('user_mail_status_blocked_body','s:85:\"[user:name],\r\n\r\nYour account on [site:name] has been blocked.\r\n\r\n--  [site:name] team\";'),('user_mail_status_blocked_notify','i:0;'),('user_mail_status_blocked_subject','s:56:\"Account details for [user:name] at [site:name] (blocked)\";'),('user_mail_status_canceled_body','s:86:\"[user:name],\r\n\r\nYour account on [site:name] has been canceled.\r\n\r\n--  [site:name] team\";'),('user_mail_status_canceled_notify','i:0;'),('user_mail_status_canceled_subject','s:57:\"Account details for [user:name] at [site:name] (canceled)\";'),('user_pictures','i:1;'),('user_picture_default','s:39:\"sites/default/files/pictures/avatar.jpg\";'),('user_picture_dimensions','s:9:\"1024x1024\";'),('user_picture_file_size','s:3:\"800\";'),('user_picture_guidelines','s:0:\"\";'),('user_picture_path','s:8:\"pictures\";'),('user_picture_style','s:9:\"thumbnail\";'),('user_register','s:1:\"0\";'),('user_signatures','i:1;'),('views_block_hashes','a:7:{s:32:\"aa3f00e073c92c80741f86293eda5d80\";s:32:\"mt_slideshow_full_screen-block_1\";s:32:\"a26e7917a24898060fdea541f9450e29\";s:33:\"mt_showcases_masonry_layout-block\";s:32:\"73c8aff58320e4b69ece62d7b7b00718\";s:35:\"mt_showcases_masonry_layout-block_1\";s:32:\"571d9e8bb2857ab7f6b2a7793775feeb\";s:35:\"mt_showcases_masonry_layout-block_2\";s:32:\"a637dbca9ccd602ac4d792ee4e0e0d77\";s:32:\"mt_products_masonry_layout-block\";s:32:\"dfb25af03546316b01f5050d3de25bab\";s:34:\"mt_products_masonry_layout-block_1\";s:32:\"40df75d14cc1bdcd2c14915afb87ffdf\";s:34:\"mt_products_masonry_layout-block_2\";}'),('views_defaults','a:2:{s:13:\"taxonomy_term\";b:1;s:6:\"tweets\";b:1;}'),('webform_blocks','a:2:{s:15:\"client-block-22\";a:2:{s:7:\"display\";s:4:\"form\";s:11:\"pages_block\";i:0;}s:15:\"client-block-32\";a:2:{s:7:\"display\";s:4:\"form\";s:11:\"pages_block\";i:0;}}'),('webform_email_address_individual','i:1;'),('webform_email_html_capable','b:0;'),('webform_node_webform','b:1;');
/*!40000 ALTER TABLE `variable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webform`
--

DROP TABLE IF EXISTS `webform`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webform` (
  `nid` int(10) unsigned NOT NULL COMMENT 'The node identifier of a webform.',
  `confirmation` text NOT NULL COMMENT 'The confirmation message or URL displayed to the user after submitting a form.',
  `confirmation_format` varchar(255) DEFAULT NULL COMMENT 'The filter_format.format of the confirmation message.',
  `redirect_url` varchar(2048) DEFAULT '<confirmation>' COMMENT 'The URL a user is redirected to after submitting a form.',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Boolean value of a webform for open (1) or closed (0).',
  `block` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Boolean value for whether this form be available as a block.',
  `allow_draft` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Boolean value for whether submissions to this form be saved as a draft.',
  `auto_save` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Boolean value for whether submissions to this form should be auto-saved between pages.',
  `submit_notice` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Boolean value for whether to show or hide the previous submissions notification.',
  `submit_text` varchar(255) DEFAULT NULL COMMENT 'The title of the submit button on the form.',
  `submit_limit` tinyint(4) NOT NULL DEFAULT '-1' COMMENT 'The number of submissions a single user is allowed to submit within an interval. -1 is unlimited.',
  `submit_interval` int(11) NOT NULL DEFAULT '-1' COMMENT 'The amount of time in seconds that must pass before a user can submit another submission within the set limit.',
  `total_submit_limit` int(11) NOT NULL DEFAULT '-1' COMMENT 'The total number of submissions allowed within an interval. -1 is unlimited.',
  `total_submit_interval` int(11) NOT NULL DEFAULT '-1' COMMENT 'The amount of time in seconds that must pass before another submission can be submitted within the set limit.',
  `progressbar_bar` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Boolean value indicating if the bar should be shown as part of the progress bar.',
  `progressbar_page_number` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Boolean value indicating if the page number should be shown as part of the progress bar.',
  `progressbar_percent` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Boolean value indicating if the percentage complete should be shown as part of the progress bar.',
  `progressbar_pagebreak_labels` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Boolean value indicating if the pagebreak labels should be included as part of the progress bar.',
  `progressbar_include_confirmation` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Boolean value indicating if the confirmation page should count as a page in the progress bar.',
  `progressbar_label_first` varchar(255) DEFAULT NULL COMMENT 'Label for the first page of the progress bar.',
  `progressbar_label_confirmation` varchar(255) DEFAULT NULL COMMENT 'Label for the last page of the progress bar.',
  `preview` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Boolean value indicating if this form includes a page for previewing the submission.',
  `preview_next_button_label` varchar(255) DEFAULT NULL COMMENT 'The text for the button that will proceed to the preview page.',
  `preview_prev_button_label` varchar(255) DEFAULT NULL COMMENT 'The text for the button to go backwards from the preview page.',
  `preview_title` varchar(255) DEFAULT NULL COMMENT 'The title of the preview page, as used by the progress bar.',
  `preview_message` text NOT NULL COMMENT 'Text shown on the preview page of the form.',
  `preview_message_format` varchar(255) DEFAULT NULL COMMENT 'The filter_format.format of the preview page message.',
  `preview_excluded_components` text NOT NULL COMMENT 'Comma-separated list of component IDs that should not be included in this form’s confirmation page.',
  `next_serial` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'The serial number to give to the next submission to this webform.',
  `confidential` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Boolean value for whether to anonymize submissions.',
  PRIMARY KEY (`nid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table for storing additional properties for webform nodes.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webform`
--

LOCK TABLES `webform` WRITE;
/*!40000 ALTER TABLE `webform` DISABLE KEYS */;
INSERT INTO `webform` VALUES (22,'','filtered_html','<confirmation>',1,1,0,0,1,'',-1,-1,-1,-1,0,0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,'',NULL,'',2,0),(32,'','filtered_html','<confirmation>',1,1,0,0,1,'',-1,-1,-1,-1,0,0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,'',NULL,'',1,0);
/*!40000 ALTER TABLE `webform` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webform_component`
--

DROP TABLE IF EXISTS `webform_component`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webform_component` (
  `nid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The node identifier of a webform.',
  `cid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'The identifier for this component within this node, starts at 0 for each node.',
  `pid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'If this component has a parent fieldset, the cid of that component.',
  `form_key` varchar(128) DEFAULT NULL COMMENT 'When the form is displayed and processed, this key can be used to reference the results.',
  `name` text NOT NULL,
  `type` varchar(16) DEFAULT NULL COMMENT 'The field type of this component (textfield, select, hidden, etc.).',
  `value` text NOT NULL COMMENT 'The default value of the component when displayed to the end-user.',
  `extra` text NOT NULL COMMENT 'Additional information unique to the display or processing of this component.',
  `required` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Boolean flag for if this component is required.',
  `weight` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Determines the position of this component in the form.',
  PRIMARY KEY (`nid`,`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores information about components for webform nodes.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webform_component`
--

LOCK TABLES `webform_component` WRITE;
/*!40000 ALTER TABLE `webform_component` DISABLE KEYS */;
INSERT INTO `webform_component` VALUES (22,1,0,'name','Your name','textfield','','a:10:{s:13:\"title_display\";s:6:\"before\";s:7:\"private\";i:0;s:8:\"disabled\";i:0;s:6:\"unique\";i:0;s:5:\"width\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:12:\"field_prefix\";s:0:\"\";s:12:\"field_suffix\";s:0:\"\";s:11:\"description\";s:0:\"\";s:10:\"attributes\";a:0:{}}',1,5),(22,2,0,'email','Your email','email','','a:7:{s:13:\"title_display\";s:6:\"before\";s:7:\"private\";i:0;s:8:\"disabled\";i:0;s:6:\"unique\";i:0;s:5:\"width\";s:0:\"\";s:11:\"description\";s:0:\"\";s:10:\"attributes\";a:0:{}}',1,6),(22,3,0,'subject','Subject','textfield','','a:10:{s:13:\"title_display\";s:6:\"before\";s:7:\"private\";i:0;s:8:\"disabled\";i:0;s:6:\"unique\";i:0;s:5:\"width\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:12:\"field_prefix\";s:0:\"\";s:12:\"field_suffix\";s:0:\"\";s:11:\"description\";s:0:\"\";s:10:\"attributes\";a:0:{}}',1,7),(22,4,0,'message','Message','textarea','','a:8:{s:13:\"title_display\";i:0;s:7:\"private\";i:0;s:4:\"rows\";s:2:\"12\";s:9:\"resizable\";i:1;s:8:\"disabled\";i:0;s:4:\"cols\";s:0:\"\";s:11:\"description\";s:0:\"\";s:10:\"attributes\";a:0:{}}',1,8),(32,1,0,'name_alt','Your name','textfield','','a:10:{s:13:\"title_display\";s:6:\"before\";s:7:\"private\";i:0;s:8:\"disabled\";i:0;s:6:\"unique\";i:0;s:5:\"width\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:12:\"field_prefix\";s:0:\"\";s:12:\"field_suffix\";s:0:\"\";s:11:\"description\";s:0:\"\";s:10:\"attributes\";a:0:{}}',1,5),(32,2,0,'email_alt','Your email','email','','a:7:{s:13:\"title_display\";s:6:\"before\";s:7:\"private\";i:0;s:8:\"disabled\";i:0;s:6:\"unique\";i:0;s:5:\"width\";s:0:\"\";s:11:\"description\";s:0:\"\";s:10:\"attributes\";a:0:{}}',1,6),(32,3,0,'subject_alt','Subject','textfield','','a:10:{s:13:\"title_display\";s:6:\"before\";s:7:\"private\";i:0;s:8:\"disabled\";i:0;s:6:\"unique\";i:0;s:5:\"width\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:12:\"field_prefix\";s:0:\"\";s:12:\"field_suffix\";s:0:\"\";s:11:\"description\";s:0:\"\";s:10:\"attributes\";a:0:{}}',1,7),(32,4,0,'message','Message','textarea','','a:8:{s:13:\"title_display\";i:0;s:7:\"private\";i:0;s:4:\"rows\";s:2:\"12\";s:9:\"resizable\";i:1;s:8:\"disabled\";i:0;s:4:\"cols\";s:0:\"\";s:11:\"description\";s:0:\"\";s:10:\"attributes\";a:0:{}}',1,8),(32,5,0,'markup','Markup','markup','<h2>Write to us</h2>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>','a:2:{s:6:\"format\";s:9:\"full_html\";s:7:\"private\";i:0;}',0,4);
/*!40000 ALTER TABLE `webform_component` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webform_conditional`
--

DROP TABLE IF EXISTS `webform_conditional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webform_conditional` (
  `nid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The node identifier of a webform.',
  `rgid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'The rule group identifier for this group of rules.',
  `andor` varchar(128) DEFAULT NULL COMMENT 'Whether to AND or OR the actions in this group. All actions within the same crid should have the same andor value.',
  `weight` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Determines the position of this conditional compared to others.',
  PRIMARY KEY (`nid`,`rgid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Holds information about conditional logic.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webform_conditional`
--

LOCK TABLES `webform_conditional` WRITE;
/*!40000 ALTER TABLE `webform_conditional` DISABLE KEYS */;
/*!40000 ALTER TABLE `webform_conditional` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webform_conditional_actions`
--

DROP TABLE IF EXISTS `webform_conditional_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webform_conditional_actions` (
  `nid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The node identifier of a webform.',
  `rgid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'The rule group identifier for this group of rules.',
  `aid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'The rule identifier for this conditional action.',
  `target_type` varchar(128) DEFAULT NULL COMMENT 'The type of target to be affected. Currently always "component". Indicates what type of ID the "target" column contains.',
  `target` varchar(128) DEFAULT NULL COMMENT 'The ID of the target to be affected. Typically a component ID.',
  `invert` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'If inverted, execute when rule(s) are false.',
  `action` varchar(128) DEFAULT NULL COMMENT 'The action to be performed on the target.',
  `argument` text COMMENT 'Optional argument for action.',
  PRIMARY KEY (`nid`,`rgid`,`aid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Holds information about conditional actions.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webform_conditional_actions`
--

LOCK TABLES `webform_conditional_actions` WRITE;
/*!40000 ALTER TABLE `webform_conditional_actions` DISABLE KEYS */;
/*!40000 ALTER TABLE `webform_conditional_actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webform_conditional_rules`
--

DROP TABLE IF EXISTS `webform_conditional_rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webform_conditional_rules` (
  `nid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The node identifier of a webform.',
  `rgid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'The rule group identifier for this group of rules.',
  `rid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'The rule identifier for this conditional rule.',
  `source_type` varchar(128) DEFAULT NULL COMMENT 'The type of source on which the conditional is based. Currently always "component". Indicates what type of ID the "source" column contains.',
  `source` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'The component ID being used in this condition.',
  `operator` varchar(128) DEFAULT NULL COMMENT 'Which operator (equal, contains, starts with, etc.) should be used for this comparison between the source and value?',
  `value` text COMMENT 'The value to be compared with source.',
  PRIMARY KEY (`nid`,`rgid`,`rid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Holds information about conditional logic.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webform_conditional_rules`
--

LOCK TABLES `webform_conditional_rules` WRITE;
/*!40000 ALTER TABLE `webform_conditional_rules` DISABLE KEYS */;
/*!40000 ALTER TABLE `webform_conditional_rules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webform_emails`
--

DROP TABLE IF EXISTS `webform_emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webform_emails` (
  `nid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The node identifier of a webform.',
  `eid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'The e-mail identifier for this row’s settings.',
  `email` text COMMENT 'The e-mail address that will be sent to upon submission. This may be an e-mail address, the special key "default" or a numeric value. If a numeric value is used, the value of a component will be substituted on submission.',
  `subject` text COMMENT 'The e-mail subject that will be used. This may be a string, the special key "default" or a numeric value. If a numeric value is used, the value of a component will be substituted on submission.',
  `from_name` text COMMENT 'The e-mail "from" name that will be used. This may be a string, the special key "default" or a numeric value. If a numeric value is used, the value of a component will be substituted on submission.',
  `from_address` text COMMENT 'The e-mail "from" e-mail address that will be used. This may be a string, the special key "default" or a numeric value. If a numeric value is used, the value of a component will be substituted on submission.',
  `template` text COMMENT 'A template that will be used for the sent e-mail. This may be a string or the special key "default", which will use the template provided by the theming layer.',
  `excluded_components` text NOT NULL COMMENT 'A list of components that will not be included in the %email_values token. A list of CIDs separated by commas.',
  `html` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Determines if the e-mail will be sent in an HTML format. Requires Mime Mail module.',
  `attachments` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Determines if the e-mail will include file attachments. Requires Mime Mail module.',
  `extra` text NOT NULL COMMENT 'A serialized array of additional options for the e-mail configuration, including excluded components and value mapping for the TO and FROM addresses for select lists.',
  `exclude_empty` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Determines if the e-mail will include component with an empty value.',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT 'Whether this email is enabled.',
  PRIMARY KEY (`nid`,`eid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Holds information regarding e-mails that should be sent...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webform_emails`
--

LOCK TABLES `webform_emails` WRITE;
/*!40000 ALTER TABLE `webform_emails` DISABLE KEYS */;
/*!40000 ALTER TABLE `webform_emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webform_last_download`
--

DROP TABLE IF EXISTS `webform_last_download`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webform_last_download` (
  `nid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The node identifier of a webform.',
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The user identifier.',
  `sid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The last downloaded submission number.',
  `requested` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Timestamp of last download request.',
  PRIMARY KEY (`nid`,`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores last submission number per user download.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webform_last_download`
--

LOCK TABLES `webform_last_download` WRITE;
/*!40000 ALTER TABLE `webform_last_download` DISABLE KEYS */;
/*!40000 ALTER TABLE `webform_last_download` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webform_roles`
--

DROP TABLE IF EXISTS `webform_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webform_roles` (
  `nid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The node identifier of a webform.',
  `rid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The role identifier.',
  PRIMARY KEY (`nid`,`rid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Holds access information regarding which roles are...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webform_roles`
--

LOCK TABLES `webform_roles` WRITE;
/*!40000 ALTER TABLE `webform_roles` DISABLE KEYS */;
INSERT INTO `webform_roles` VALUES (22,1),(22,2),(32,1),(32,2);
/*!40000 ALTER TABLE `webform_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webform_submissions`
--

DROP TABLE IF EXISTS `webform_submissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webform_submissions` (
  `sid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The unique identifier for this submission.',
  `nid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The node identifier of a webform.',
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The id of the user that completed this submission.',
  `is_draft` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Is this a draft of the submission?',
  `submitted` int(11) NOT NULL DEFAULT '0' COMMENT 'Timestamp of when the form was submitted.',
  `remote_addr` varchar(128) DEFAULT NULL COMMENT 'The IP address of the user that submitted the form.',
  `serial` int(10) unsigned NOT NULL COMMENT 'The serial number of this submission.',
  `completed` int(11) NOT NULL DEFAULT '0' COMMENT 'Timestamp when the form was submitted as complete (not draft).',
  `modified` int(11) NOT NULL DEFAULT '0' COMMENT 'Timestamp when the form was last saved (complete or draft).',
  `highest_valid_page` smallint(6) NOT NULL DEFAULT '0' COMMENT 'For drafts, the highest validated page number.',
  PRIMARY KEY (`sid`),
  UNIQUE KEY `sid_nid` (`sid`,`nid`),
  UNIQUE KEY `nid_serial` (`nid`,`serial`),
  KEY `nid_uid_sid` (`nid`,`uid`,`sid`),
  KEY `nid_sid` (`nid`,`sid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Holds general information about submissions outside of...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webform_submissions`
--

LOCK TABLES `webform_submissions` WRITE;
/*!40000 ALTER TABLE `webform_submissions` DISABLE KEYS */;
INSERT INTO `webform_submissions` VALUES (1,22,0,0,1355999473,'127.0.0.1',1,1355999473,1355999473,0);
/*!40000 ALTER TABLE `webform_submissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webform_submitted_data`
--

DROP TABLE IF EXISTS `webform_submitted_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webform_submitted_data` (
  `nid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The node identifier of a webform.',
  `sid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The unique identifier for this submission.',
  `cid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'The identifier for this component within this node, starts at 0 for each node.',
  `no` varchar(128) NOT NULL DEFAULT '0' COMMENT 'Usually this value is 0, but if a field has multiple values (such as a time or date), it may require multiple rows in the database.',
  `data` mediumtext NOT NULL COMMENT 'The submitted value of this field, may be serialized for some components.',
  PRIMARY KEY (`nid`,`sid`,`cid`,`no`),
  KEY `nid` (`nid`),
  KEY `sid_nid` (`sid`,`nid`),
  KEY `data` (`data`(64))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores all submitted field data for webform submissions.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webform_submitted_data`
--

LOCK TABLES `webform_submitted_data` WRITE;
/*!40000 ALTER TABLE `webform_submitted_data` DISABLE KEYS */;
INSERT INTO `webform_submitted_data` VALUES (22,1,1,'0','Test'),(22,1,2,'0','george@morethanthemes.com'),(22,1,3,'0','Test'),(22,1,4,'0','Message');
/*!40000 ALTER TABLE `webform_submitted_data` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-08  8:23:30
